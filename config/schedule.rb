every 1.day, at: '2:00 am' do
  rake "update_tables:delete_unmatched_tables"
end

every :sunday, at: '12:00 am' do
  rake "elasticsearch:create_indices"
end
