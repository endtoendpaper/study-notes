require 'sidekiq/web'

Rails.application.routes.draw do
  root "static_pages#home"
  get '/404', to: 'errors#not_found'
  get  "/about", to: "static_pages#about"
  get  "/help", to: "static_pages#help"
  get  "/user-management", to: "static_pages#user_management"
  get  "/search", to: "static_pages#search"
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks', registrations: "registrations" }
  devise_scope :user do
    delete "delete_avatar" => "registrations"
  end
  authenticate :user, lambda { |u| u.admin? } do
    mount Sidekiq::Web => '/sidekiq'
  end
  resources :site_settings, only: [:edit, :update], :path => "settings" do
    member do
      delete :delete_default_avatar
      delete :delete_logo
      delete :delete_favicon
      delete :delete_apple_touch_icon
    end
  end
  resources :help_sections, only: [:new, :create, :edit, :update, :destroy], :path => "documentation"
  resources :support_issues, only: [:index, :show, :new, :create, :update], :path => "support" do
    resources :support_replies, only: [:create], :path => "reply"
    resources :support_comments, only: [:create], :path => "comment"
    resources :support_notes, only: [:create], :path => "note"
  end
  resources :chats, only: [:create, :index] do
    resources :messages, only: [:create]
    collection do
      post :update_index
      get '/join_user_chat/:user_id/', to: 'chats#join_user_chat', :as => :join_user_chat
      get '/join_group_chat/:group_id/', to: 'chats#join_group_chat', :as => :join_group_chat
      get :search
    end
    member do
      post :update_show
      post :pin
      post :unpin
      post :hide
      post :clear_unread_messages
      post :load_more_messages
    end
  end
  resources :relationships, only: [:create, :destroy]
  resources :tables, only: [:show, :create, :update]
  get '/edit_collaborations/:record_id/:record_type', to: 'collaborations#edit_collaborations', :as => :edit_collaborations
  get '/:type/:record_id/collaboration_logs', type: /(calendars|decks|notes)/, to: 'collaborations#collaboration_logs', :as => :collaboration_logs
  patch '/change_owner/:record_id/:record_type', to: 'collaborations#change_owner', :as => :change_owner
  patch '/add_collaborator/:record_id/:record_type', to: 'collaborations#add_collaborator', :as => :add_collaborator
  patch '/remove_collaborator/:record_id/:record_type', to: 'collaborations#remove_collaborator', :as => :remove_collaborator
  patch '/add_group/:record_id/:record_type', to: 'collaborations#add_group', :as => :add_group
  patch '/remove_group/:record_id/:record_type', to: 'collaborations#remove_group', :as => :remove_group
  resources :tags, only: [:show] do
    collection do
      get '/edit_item_list/:record_id/:record_type', to: 'tags#edit_item_list', :as => :edit_item_list
      patch '/add/:record_id/:record_type', to: 'tags#add', :as => :add
      patch '/remove/:record_id/:record_type', to: 'tags#remove', :as => :remove
      get '/get_id/:tag_name', to: 'tags#get_id', :as => :get_id
      get :search
    end
  end
  resources :stars, only: []  do
    collection do
      patch '/add/:record_id/:record_type', to: 'stars#add', :as => :add
      patch '/remove/:record_id/:record_type', to: 'stars#remove', :as => :remove
    end
  end
  get '/:type/:record_id/stargazers', type: /(calendars|decks|notes)/, to: 'stars#stargazers', :as => :stargazers
  resources :calendars do
    resources :events, only: [:show, :new, :create, :edit, :update, :destroy]
    resources :recurring_events, only: [:show, :new, :create, :edit, :update, :destroy]
    resources :recurring_events do
      patch :add_exception
    end
    get :by_day
    post :update_events_for_tz
  end
  get '/calendar', to: 'calendars#user_calendar', :as => :user_calendar
  get '/calendar/by_day', to: 'calendars#user_calendar_by_day', :as => :user_calendar_by_day
  resources :decks do
    resources :cards do
      patch '/update_position/:display_position', to: 'cards#update_position', :as => :update_position
    end
    post :shuffle
  end
  resources :notes
  resources :groups do
    collection do
      get '/get_id/:group_name', to: 'groups#get_id', :as => :get_id
      get :search
    end
    patch :change_owner
    patch :add_member
    patch :remove_member
    get :edit_members
    get :show_members, :path => "members"
    get ':type', type: /(calendars|decks|notes)/, action: 'show_items', :as => :show_items
    get :membership_logs
    delete :delete_avatar
  end
  resources :users, only: [:index, :show, :new, :edit, :update, :destroy] do
    collection do
      get '/get_id/:username', to: 'users#get_id', :as => :get_id
      get :search
    end
    member do
      delete :delete_avatar
      resources :support_issues, only: [:index], :path => "support"
      get :following
      get :followers
      get :show_groups, :path => "groups"
      get :show_stars, :path => "stars"
      get ':type', type: /(calendars|decks|notes)/, action: 'show_items', :as => :show_items
      get :bulk_edit_ownership
      post :bulk_transfer_ownership
    end
  end
end
