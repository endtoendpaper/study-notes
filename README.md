# Ruby on Rails App for Creating Calendars, Flashcards, and Sticky Notes
This application is for creating calendars, flashcards, and sticky notes that can be displayed publicly, internally to logged in users, or privately to the user that created them. Users may collaborate on study materials by creating groups and giving them edit/view permissions to chosen resources, specifying individual collaborators for each resource, or by following/chatting with other users. Finally, users can organize their items and search for new items through tags.

The Devise gem provides user registration and user sessions. Built on top of Devise is an admin interface for managing users. Users can set a password or sign in through their Google account with OAuth. Users may select a username and avatar for public display along with some site preferences including the timezone and if other users can see who they are following.

In the site settings, admins can decide if users are allowed to sign up for the application or if they need to be created through the admin interface only. Admins can select a default avatar image, logo, and site name from the site settings.

A basic support issue tracker can be enabled to allow users to submit feedback and issues to admins. Admins can reply, add internal notes, and resolve these issues.

Admins can also disable follow and chat functionality for users.

Dark mode is available for users.

In the development environment, admins can login as other confirmed users.

Source code: https://gitlab.com/endtoendpaper/study-notes

## Development Environment

```sh
git clone git@gitlab.com:endtoendpaper/study-notes.git
cd study-notes
cp .env.tmpl .env
```

Rename databases in config/database.yml if desired. Mailer is turned off in development. To use the mailer, change config.action_mailer.perform_deliveries to true in config/environments/development.rb and add the SENDMAIL_USERNAME and SENDMAIL_PASSWORDS to your .env variables as instructed below.

Fix the variables in .env
* ADMIN_USERNAME, ADMIN_EMAIL and ADMIN_PS are **required** to seed the database with the first admin account. The password can be changed within the app and more admins and users can be created after startup.
* Leave the POSTGRES_USER and POSTGRES_PASSWORD to the postgres image default in dev.
* OPTIONAL in DEV: GOOGLE_CLIENT_ID and GOOGLE_CLIENT_SECRET are your OAuth credientials https://developers.google.com/workspace/guides/create-credentials#oauth-client-id
   * Authorized JavaScript Origin Path for DEV: http://localhost:3000
   * Authorized redirect URI path for DEV: http://localhost:3000/users/auth/google_oauth2/callback
* OPTIONAL in DEV: SENDMAIL_USERNAME and SENDMAIL_PASSWORD are your gmail account and application password for your mailer https://support.google.com/accounts/answer/185833?hl=en

```sh
ADMIN_USERNAME="admin"
ADMIN_EMAIL="AdminEmail@gmail.com"
ADMIN_PW="adminPW"
POSTGRES_USER=postgres
POSTGRES_PASSWORD=password
GOOGLE_CLIENT_ID="XXXXXX"
GOOGLE_CLIENT_SECRET="XXXXXX"
SENDMAIL_USERNAME="OAuthEmail@gmail.com"
SENDMAIL_PASSWORD="ApplicationPassword"
REDIS_URL="redis://redis:6379"
```

```sh
docker compose up
docker compose exec web rails db:create db:migrate db:seed elasticsearch:create_indices
```

Access the app from this link: http://localhost:3000

If you want to seed the database with fake test data, run the following rake task (this will delete all users except the original admin already seeded).

```sh
docker compose exec web rails db:seed:fake_data
```

## Test Environment

Run tests with rspec:

```sh
docker compose run -e "RAILS_ENV=test" --rm web rspec
```

Preview mailers in development:
* http://localhost:3000/rails/mailers/admins_mailer
* http://localhost:3000/rails/mailers/user_mailer

## Production Environment - Docker Swarm

### Gather Environment Variables for .env.production and .env.production.local

```sh
git clone git@gitlab.com:endtoendpaper/study-notes.git
cd study-notes
cp .env.production.tmpl .env.production
cp .env.production.local.tmpl .env.production.local
docker compose up
docker compose exec web rails secret # copy value for .env.production file below
```

Fix the variables in .env.production
* ADMIN_USERNAME, ADMIN_EMAIL and ADMIN_PS are **required** to seed the database with the first admin account. The password can be changed within the app and more admins and users can be created after startup.
* GOOGLE_CLIENT_ID and GOOGLE_CLIENT_SECRET are your OAuth credientials https://developers.google.com/workspace/guides/create-credentials#oauth-client-id
   * Authorized JavaScript Origin Path for DEV: http://hostname:3000
   * Authorized redirect URI path for DEV: http://hostname:3000/users/auth/google_oauth2/callback
* SENDMAIL_USERNAME and SENDMAIL_PASSWORD are your gmail account and application password for your mailer https://support.google.com/accounts/answer/185833?hl=en
* AWS_* variables are for using amazon storage in production. https://docs.aws.amazon.com/AmazonS3/latest/userguide/creating-bucket.html
* HOST is the default URL for your site (e.g. my_domain.com, myhost:3000)
* SECRET_KEY_BASE is the key generated above

```sh
ADMIN_USERNAME=AdminUsername
ADMIN_EMAIL=AdminEmail@gmail.com
ADMIN_PW=adminPW
GOOGLE_CLIENT_ID=XXXXXX
GOOGLE_CLIENT_SECRET=XXXXXX
SENDMAIL_USERNAME=OAuthEmail@gmail.com
SENDMAIL_PASSWORD=ApplicationPassword
AWS_ACCESS_KEY_ID="XXXXXX"
AWS_SECRET_ACCESS_KEY="XXXXXX"
AWS_BUCKET="XXXXXX"
AWS_REGION="us-west-1"
HOST="IP_OR_DNS_ALIAS"
RAILS_ENV=production
SECRET_KEY_BASE="XXXXX"
REDIS_URL="redis://redis:6379"
```

Fix the variables in .env.production.local
* Change database user and password from the default. Don't add quotes to the strings or the postgres image can't read it.

```sh
POSTGRES_DB=study_notes
POSTGRES_USER=postgres
POSTGRES_PASSWORD=password
```

### Build Production Image and Push to Docker Hub

Skip this step if using public endtoendpaper/study-notes images. Otherwise, replace endtoendpaper with your own Docker username in the commands below and in the docker-stack.yml file.

```sh
docker build -f Dockerfile -t endtoendpaper/study-notes:latest .
docker login
docker push endtoendpaper/study-notes:latest
```

### Setup Docker Swarm Ubuntu DigitalOcean Instance

Follow instructions here: https://gitlab.com/endtoendpaper/ubuntu-setup-for-docker-swarm/-/blob/main/README.md

### Remotely Connect to Swarm

```sh
docker context create --docker host=ssh://myuser@myremote studynotes
```

### Deploy to DigitalOcean Docker Swarm

```sh
docker --context studynotes stack deploy -c docker-stack.yml studynotes
```

The db_migrator container is set to wait 5 minutes before running its migration command. After that, access the app via the IP address or dns_alias.

To check on status of stack:
```sh
docker --context studynotes ps
docker --context studynotes stack ps studynotes
docker --context studynotes stack services studynotes
```

If the deploy fails and the above commands show the error "no such image found," try cleaning up unused images, containers, and networks with docker system prune then redeploy. This will reclaim space on your machine.
```sh
docker --context studynotes system prune
```

View logs of web app:
```sh
docker --context studynotes service logs studynotes_web
```

To scale up web app (add containers):
```sh
docker --context studynotes service studynotes_web=3
```

### Configure AWS Bucket

You’ll need to edit cross-origin resource sharing (CORS) in your aws bucket for action text to work. The configuration should look something like:

```sh
[
  {
    "AllowedHeaders": [
      "*"
    ],
    "AllowedMethods": [
      "PUT"
    ],
    "AllowedOrigins": [
      "https://hostname*"
    ],
    "ExposeHeaders": [
      "Origin",
      "Content-Type",
      "Content-MD5",
      "Content-Disposition"
    ],
    "MaxAgeSeconds": 3600
  }
]
```

## Modify CSS/JS

app/assets/stylesheets/application.bootstrap.scss
* Change SASS variables above the bootstrap file imports
* Add custom css and import custom scss files below the bootstrap file imports

Color variables that change for dark mode are in two places
* Light mode color variable versions are in app/assets/stylesheets/application.bootstrap.scss :root {} section
* Dark mode color variable versions app/views/layouts/application.html.erb :root {} section

app/javascript/application.js
* Add custom JS or import custom JS files after the bootstrap imports OR add a new stimulus controller following the instrutions in app/javascript/controllers/index.js

## Open Rails Console

### Development

```sh
docker compose exec web bash
rails c
```

### Production in Docker Swarm

```sh
docker --context studynotes ps
docker --context studynotes exec -it CONTAINER_ID bash #use container id from above command
rails c
```

## Create New User With Rails Console

```sh
rails c
user = user = User.new(:username => 'admin', :admin => false, :email => 'NewEmail@gmail.com', :password => 'userpass', :password_confirmation => 'userpass')
user.skip_confirmation! #optional
user.save!
```

## Make User Admin With Rails Console

```sh
rails c
a = User.find_by_email("MyEmail@gmail.com")
a.update(admin: true)
```
