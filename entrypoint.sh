#!/bin/bash
set -e

# Remove a potentially pre-existing server.pid for Rails.
rm -f /app/tmp/pids/server.pid

# Verify node_modules are up to date
yarn install --silent

# Verify gems are up to date
if ! bundle check > /dev/null; then
  echo "Gems dependencies are out of date. Installing..."
  bundle install
fi

# Then exec the container's main process (what's set as CMD in the Dockerfile).
exec "$@"
