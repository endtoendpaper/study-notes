require 'rails_helper'

RSpec.describe Chat, type: :model do
  before(:each) do
    @chat_group = FactoryBot.create(:chat, :group)
    @chat_dm = FactoryBot.create(:chat, :dm)
  end

  it 'is valid with valid data' do
    expect(@chat_group).to be_valid
    expect(@chat_dm).to be_valid
  end

  it "it is invalid with both a group and collaborators" do
    @chat_group.collaborate(FactoryBot.create(:user))
    expect(@chat_group).to_not be_valid
    @chat_dm.add_group(FactoryBot.create(:group))
    expect(@chat_dm).to_not be_valid
  end

  it "updates participants unread msgs before destroy" do
    message = FactoryBot.create(:message, chat_id: @chat_group.id)
    @chat_group.participants.each do |p|
      expect(p.unread_messages).to eq(1)
    end
    participants = @chat_group.participants
    @chat_group.destroy
    participants.each do |p|
      expect(p.unread_messages).to eq(0)
    end
  end
end
