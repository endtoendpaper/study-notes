require 'rails_helper'

RSpec.describe GroupAssignment, type: :model do
  before(:each) do
    @deck_group = FactoryBot.create(:group_assignment, :to_deck)
    @note_group = FactoryBot.create(:group_assignment, :to_note)
  end

  it 'is valid with valid data' do
    expect(@deck_group).to be_valid
  end

  it "requires a group_id" do
    @deck_group.group_id = nil
    expect(@deck_group).to_not be_valid
  end

  it "requires a deck_id" do
    @deck_group.record_id = nil
    expect(@deck_group).to_not be_valid
  end

  it 'is valid with valid data' do
    expect(@note_group).to be_valid
  end

  it "requires a group_id" do
    @note_group.group_id = nil
    expect(@note_group).to_not be_valid
  end

  it "requires a note_id" do
    @note_group.record_id = nil
    expect(@note_group).to_not be_valid
  end

  it "deletes associated chats when deleted" do
    new_group = FactoryBot.create(:group)
    chat = FactoryBot.create(:chat)
    chat.add_group(new_group)
    expect do
      new_group.destroy
    end.to change(Chat, :count).by(-1)
  end
end
