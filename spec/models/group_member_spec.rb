require 'rails_helper'

RSpec.describe GroupMember, type: :model do
  before(:each) do
    @group_member = FactoryBot.create(:group_member)
  end

  it 'is valid with valid data' do
    expect(@group_member).to be_valid
  end

  it "requires a group_id" do
    @group_member.group_id = nil
    expect(@group_member).to_not be_valid
  end

  it "requires a user_id" do
    @group_member.user_id = nil
    expect(@group_member).to_not be_valid
  end
end
