require 'rails_helper'

RSpec.describe Note, type: :model do
  it 'is valid with valid data' do
    note = Note.new
    note.user_id = FactoryBot.create(:user, :confirmed).id
    note.title = Faker::Lorem.sentence(word_count: 6)
    note.text = Faker::Lorem.paragraphs(number: 4)
    note.visibility = 0
    expect(note).to be_valid
  end

  it 'is invalid without a user' do
    note = Note.new
    note.title = Faker::Lorem.sentence(word_count: 6)
    note.text = Faker::Lorem.paragraphs(number: 4)
    note.visibility = 0
    expect(note).to_not be_valid
  end

  it 'is invalid without a title' do
    note = Note.new
    note.user_id = FactoryBot.create(:user, :confirmed).id
    note.text = Faker::Lorem.paragraphs(number: 4)
    note.visibility = 0
    expect(note).to_not be_valid
  end

  it 'is invalid without text' do
    note = Note.new
    note.user_id = FactoryBot.create(:user, :confirmed).id
    note.title = Faker::Lorem.sentence(word_count: 6)
    note.visibility = 0
    expect(note).to_not be_valid
  end

  it 'is invalid without visibility' do
    note = Note.new
    note.user_id = FactoryBot.create(:user, :confirmed).id
    note.title = Faker::Lorem.sentence(word_count: 6)
    note.text = Faker::Lorem.paragraphs(number: 4)
    expect(note).to_not be_valid
  end

  it 'is invalid if visibility is less than zero' do
    note = Note.new
    note.user_id = FactoryBot.create(:user, :confirmed).id
    note.title = Faker::Lorem.sentence(word_count: 6)
    note.text = Faker::Lorem.paragraphs(number: 4)
    note.visibility = -1
    expect(note).to_not be_valid
  end

  it 'is invalid if visibility is greater than two' do
    note = Note.new
    note.user_id = FactoryBot.create(:user, :confirmed).id
    note.title = Faker::Lorem.sentence(word_count: 6)
    note.text = Faker::Lorem.paragraphs(number: 4)
    note.visibility = 3
    expect(note).to_not be_valid
  end

  it 'returns tag count' do
    note = FactoryBot.create(:note, :private, :five_tags)
    expect(note.tags_count).to eq(5)
  end

  it "stars and unstars a note" do
    user1 = FactoryBot.create(:user)
    note = FactoryBot.create(:note, :private, user_id: user1.id)
    expect(note.stargazer?(user1)).to be false
    expect(note.star_count).to eq(0)
    note.add_star(user1)
    expect(note.stargazer?(user1)).to be true
    expect(note.star_count).to eq(1)
    note.unstar(user1)
    expect(note.stargazer?(user1)).to be false
    expect(note.star_count).to eq(0)
  end

  it "collaborates and uncollaborates a note" do
    user1 = FactoryBot.create(:user)
    note = FactoryBot.create(:note, :private, user_id: user1.id)
    expect(note.collaborater?(user1)).to be false
    expect(note.collaborators_count).to eq(0)
    note.collaborate(user1, note.user)
    expect(note.collaborater?(user1)).to be true
    expect(note.collaborators_count).to eq(1)
    note.uncollaborate(user1, note.user)
    expect(note.collaborater?(user1)).to be false
    expect(note.collaborators_count).to eq(0)
  end

  it "adds and removes a group from a note" do
    note = FactoryBot.create(:note, :private)
    group = FactoryBot.create(:group)
    expect(note.group?(note)).to be false
    expect(note.groups_count).to eq(0)
    note.add_group(group, note.user)
    expect(note.group?(group)).to be true
    expect(note.groups_count).to eq(1)
    note.remove_group(group, note.user)
    expect(note.group?(group)).to be false
    expect(note.groups_count).to eq(0)
  end
end
