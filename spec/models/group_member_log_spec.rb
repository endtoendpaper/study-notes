require 'rails_helper'

RSpec.describe GroupMemberLog, type: :model do
  it 'is created for :changed_owner_to when new group is created' do
    expect do
      FactoryBot.create(:group)
    end.to change(GroupMemberLog, :count).by(1)
    expect(GroupMemberLog.last.action).to eq("changed_owner_to")
  end

  it 'is created for :added_member when member is added' do
    group = FactoryBot.create(:group)
    user = FactoryBot.create(:user, :confirmed)
    expect do
      group.add_member(user, group.user)
    end.to change(GroupMemberLog, :count).by(1)
    expect(GroupMemberLog.last.action).to eq("added_member")
  end

  it 'is created for :removed_member when member is removed' do
    group = FactoryBot.create(:group)
    user = FactoryBot.create(:user, :confirmed)
    group.add_member(user, group.user)
    expect do
      group.remove_member(user, group.user)
    end.to change(GroupMemberLog, :count).by(1)
    expect(GroupMemberLog.last.action).to eq("removed_member")
  end

  it 'is deleted when group is deleted' do
    group = FactoryBot.create(:group)
    user = FactoryBot.create(:user, :confirmed)
    group.add_member(user, group.user)
    group.remove_member(user, group.user)
    expect(GroupMemberLog.count).to eq(3)
    expect do
      group.destroy
    end.to change(GroupMemberLog, :count).by(-3)
  end

  it 'has a nullified user when user is deleted' do
    group = FactoryBot.create(:group)
    user1 = FactoryBot.create(:user, :confirmed)
    user2 = FactoryBot.create(:user, :confirmed)
    group.add_member(user1, group.user)
    group.add_member(user2, user1)
    expect do
      user1.destroy
    end.to change(GroupMemberLog, :count).by(0)
    expect(GroupMemberLog.last.user).to be_nil
    expect(GroupMemberLog.last.user_handle).to_not be_nil
  end

  it 'has a nullified member when user/member is deleted' do
    group = FactoryBot.create(:group)
    user1 = FactoryBot.create(:user, :confirmed)
    user2 = FactoryBot.create(:user, :confirmed)
    group.add_member(user1, group.user)
    group.add_member(user2, user1)
    expect do
      user2.destroy
    end.to change(GroupMemberLog, :count).by(0)
    expect(GroupMemberLog.last.member).to be_nil
    expect(GroupMemberLog.last.member_handle).to_not be_nil
  end

  it 'sets the user_handle when created' do
    log = FactoryBot.create(:group_member_log)
    expect(GroupMemberLog.last.user_handle).to eq(log.user.handle)
  end

  it 'sets the member_handle when created' do
    log = FactoryBot.create(:group_member_log)
    expect(GroupMemberLog.last.member_handle).to eq(log.member.handle)
  end
end
