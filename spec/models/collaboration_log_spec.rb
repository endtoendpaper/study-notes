require 'rails_helper'

RSpec.describe CollaborationLog, type: :model do
  it 'is created for :changed_owner_to when new deck is created' do
    expect do
      FactoryBot.create(:deck, :private)
    end.to change(CollaborationLog, :count).by(1)
    expect(CollaborationLog.last.action).to eq("changed_owner_to")
  end

  it 'is created for :changed_owner_to when new note is created' do
    expect do
      FactoryBot.create(:note, :internal)
    end.to change(CollaborationLog, :count).by(1)
    expect(CollaborationLog.last.action).to eq("changed_owner_to")
  end

  it 'is created for :added_collaborator when collaborator is added' do
    note = FactoryBot.create(:note, :public)
    user = FactoryBot.create(:user, :confirmed)
    expect do
      note.collaborate(user, note.user)
    end.to change(CollaborationLog, :count).by(1)
    expect(CollaborationLog.last.action).to eq("added_collaborator")
  end

  it 'is created for :removed_collaborator when collaborator is removed' do
    deck = FactoryBot.create(:deck, :public)
    user = FactoryBot.create(:user, :confirmed)
    deck.collaborate(user, deck.user)
    expect do
      deck.uncollaborate(user, deck.user)
    end.to change(CollaborationLog, :count).by(1)
    expect(CollaborationLog.last.action).to eq("removed_collaborator")
  end

  it 'is created for :added_group when group is added' do
    deck = FactoryBot.create(:deck, :public)
    group = FactoryBot.create(:group)
    expect do
      deck.add_group(group, deck.user)
    end.to change(CollaborationLog, :count).by(1)
    expect(CollaborationLog.last.action).to eq("added_group")
  end

  it 'is created for :removed_group when group is removed' do
    note = FactoryBot.create(:note, :public)
    group = FactoryBot.create(:group)
    note.add_group(group, note.user)
    expect do
      note.remove_group(group, note.user)
    end.to change(CollaborationLog, :count).by(1)
    expect(CollaborationLog.last.action).to eq("removed_group")
  end

  it 'is deleted when deck is deleted' do
    log = FactoryBot.create(:collaboration_log, :log_deck, :log_collaborator)
    expect(CollaborationLog.count).to eq(2)
    expect do
      log.item.destroy
    end.to change(CollaborationLog, :count).by(-2)
  end

  it 'is deleted when note is deleted' do
    log = FactoryBot.create(:collaboration_log, :log_note, :log_collaborator)
    expect(CollaborationLog.count).to eq(2)
    expect do
      log.item.destroy
    end.to change(CollaborationLog, :count).by(-2)
  end

  it 'has a nullified user when user is deleted' do
    log = FactoryBot.create(:collaboration_log, :log_note, :log_collaborator)
    expect(CollaborationLog.count).to eq(2)
    expect do
      log.user.destroy
    end.to change(CollaborationLog, :count).by(0)
    expect(CollaborationLog.last.user).to be_nil
    expect(CollaborationLog.last.user_handle).to_not be_nil
  end

  it 'has a nullified collaborator when group is deleted' do
    log = FactoryBot.create(:collaboration_log, :log_note, :log_group)
    expect(CollaborationLog.count).to eq(2)
    expect do
      log.collaboration.destroy
    end.to change(CollaborationLog, :count).by(0)
    expect(CollaborationLog.last.collaboration).to be_nil
    expect(CollaborationLog.last.collaboration_name).to_not be_nil
  end

  it 'has a nullified collaborator when user/collaborator is deleted' do
    log = FactoryBot.create(:collaboration_log, :log_note, :log_collaborator)
    expect(CollaborationLog.count).to eq(2)
    expect do
      log.collaboration.destroy
    end.to change(CollaborationLog, :count).by(0)
    expect(CollaborationLog.last.collaboration).to be_nil
    expect(CollaborationLog.last.collaboration_name).to_not be_nil
  end

  it 'sets the user_handle when created' do
    log = FactoryBot.create(:collaboration_log, :log_note, :log_group)
    expect(CollaborationLog.last.collaboration_name).to eq(log.collaboration.name)
    log2 = FactoryBot.create(:collaboration_log, :log_note, :log_collaborator)
    expect(CollaborationLog.last.collaboration_name).to eq(log2.collaboration.handle)
  end

  it 'sets the collaborator_name when created' do
    log = FactoryBot.create(:collaboration_log, :log_note, :log_group)
    expect(CollaborationLog.last.user_handle).to eq(log.user.handle)
  end

  it 'does not add an entry when a dm chat is created' do
    expect do
      FactoryBot.create(:chat, :dm)
    end.to change(CollaborationLog, :count).by(0)
  end

  it 'does not add an entry when a group chat is created' do
    expect do
      FactoryBot.create(:chat, :group)
    end.to change(CollaborationLog, :count).by(0)
  end
end
