require 'rails_helper'

RSpec.describe Message, type: :model do
  before(:each) do
    @message = FactoryBot.create(:message, :dm)
  end

  it 'is valid with valid data' do
    expect(@message).to be_valid
  end

  it "requires a user_id" do
    @message.user_id = nil
    expect(@message).to_not be_valid
  end

  it "requires a chat_id" do
    @message.chat_id = nil
    expect(@message).to_not be_valid
  end

  it 'is invalid without content' do
    @message.content = nil
    expect(@message).to_not be_valid
  end

  it "updates chat's last message after create" do
    expect(@message.chat.last_message).to eq(@message.created_at)
  end

  it "updates recipent participants unread messages" do
    @message.chat.participants.each do |p|
      cp = ChatPreference.find_by(user: p, chat: @message.chat)
      expect(cp.unread_messages).to eq(1)
      expect(p.unread_messages).to eq(1)
    end
    user = @message.chat.participants.sample
    FactoryBot.create(:message, chat_id: @message.chat.id, user_id: user.id)
    @message.chat.participants.each do |p|
      cp = ChatPreference.find_by(user: p, chat: @message.chat)
      expect(cp.unread_messages).to eq(2) unless p = user
      expect(p.unread_messages).to eq(1) unless p = user
    end
  end
end
