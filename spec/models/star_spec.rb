require 'rails_helper'

RSpec.describe Star, type: :model do
  before(:each) do
    @deck_star = FactoryBot.create(:star, :for_deck)
    @note_star = FactoryBot.create(:star, :for_note)
  end

  it 'is valid with valid data' do
    expect(@deck_star).to be_valid
  end

  it "requires a user_id" do
    @deck_star.user_id = nil
    expect(@deck_star).to_not be_valid
  end

  it "requires a user_id" do
    @deck_star.user_id = nil
    expect(@deck_star).to_not be_valid
  end

  it 'is valid with valid data' do
    expect(@note_star).to be_valid
  end

  it "requires a user_id" do
    @note_star.user_id = nil
    expect(@note_star).to_not be_valid
  end

  it "requires a user_id" do
    @note_star.user_id = nil
    expect(@note_star).to_not be_valid
  end
end
