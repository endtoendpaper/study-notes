require 'rails_helper'

RSpec.describe Card, type: :model do
  it 'is valid with valid data' do
    card = Card.new
    card.deck_id = FactoryBot.create(:deck, :internal).id
    card.front = Faker::Lorem.sentence(word_count: 10)
    card.back = Faker::Lorem.paragraphs(number: 4)
    expect(card).to be_valid
  end

  it 'is invalid without a back' do
    card = Card.new
    card.deck_id = FactoryBot.create(:deck, :internal).id
    card.front = Faker::Lorem.sentence(word_count: 10)
    expect(card).to_not be_valid
  end

  it 'is invalid without a front' do
    card = Card.new
    card.deck_id = FactoryBot.create(:deck, :internal).id
    card.back = Faker::Lorem.paragraphs(number: 4)
    expect(card).to_not be_valid
  end

  it 'is invalid without a deck' do
    card = Card.new
    card.front = Faker::Lorem.sentence(word_count: 10)
    card.back = Faker::Lorem.paragraphs(number: 4)
    expect(card).to_not be_valid
  end
end
