require 'rails_helper'

RSpec.describe Collaboration, type: :model do
  before(:each) do
    @deck_collaboration = FactoryBot.create(:collaboration, :on_deck)
    @note_collaboration = FactoryBot.create(:collaboration, :on_note)
  end

  it 'is valid with valid data' do
    expect(@deck_collaboration).to be_valid
  end

  it "requires a user_id" do
    @deck_collaboration.user_id = nil
    expect(@deck_collaboration).to_not be_valid
  end

  it "requires a record_id" do
    @deck_collaboration.record_id = nil
    expect(@deck_collaboration).to_not be_valid
  end

  it 'is valid with valid data' do
    expect(@note_collaboration).to be_valid
  end

  it "requires a user_id" do
    @note_collaboration.user_id = nil
    expect(@note_collaboration).to_not be_valid
  end

  it "requires a record_id" do
    @note_collaboration.record_id = nil
    expect(@note_collaboration).to_not be_valid
  end

  it "deletes associated chats when deleted" do
    chat = FactoryBot.create(:chat)
    user1 = User.ordered_by_id.first
    user2 = FactoryBot.create(:user, :confirmed)
    chat.collaborate(user1)
    chat.collaborate(user2)
    expect do
      user1.destroy
    end.to change(Chat, :count).by(-1)
  end
end
