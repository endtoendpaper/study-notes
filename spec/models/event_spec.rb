require 'rails_helper'

RSpec.describe Event, type: :model do
  it 'is valid with valid data' do
    event = Event.new
    event.calendar_id = FactoryBot.create(:calendar, :internal).id
    event.summary = Faker::Lorem.sentence(word_count: 10)
    event.start = DateTime.now
    event.end = DateTime.now + 1.hour
    expect(event).to be_valid
  end

  it 'is invalid without a calendar' do
    event = Event.new
    event.summary = Faker::Lorem.sentence(word_count: 10)
    event.start = DateTime.now
    event.end = DateTime.now + 1.hour
    expect(event).to_not be_valid
  end

  it 'is invalid without a summary' do
    event = Event.new
    event.calendar_id = FactoryBot.create(:calendar, :internal).id
    event.start = DateTime.now
    event.end = DateTime.now + 1.hour
    expect(event).to_not be_valid
  end

  it 'is invalid without a start' do
    event = Event.new
    event.calendar_id = FactoryBot.create(:calendar, :internal).id
    event.summary = Faker::Lorem.sentence(word_count: 10)
    event.end = DateTime.now + 1.hour
    expect(event).to_not be_valid
  end

  it 'is invalid without an end' do
    event = Event.new
    event.calendar_id = FactoryBot.create(:calendar, :internal).id
    event.summary = Faker::Lorem.sentence(word_count: 10)
    event.start = DateTime.now
    expect(event).to_not be_valid
  end

  it 'is invalid if it starts after it ends' do
    event = Event.new
    event.calendar_id = FactoryBot.create(:calendar, :internal).id
    event.summary = Faker::Lorem.sentence(word_count: 10)
    event.start = DateTime.now + 5.minutes
    event.end = DateTime.now
    expect(event).to_not be_valid
  end
end
