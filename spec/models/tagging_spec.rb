require 'rails_helper'

RSpec.describe Tagging, type: :model do
  before(:each) do
    @deck_tag = FactoryBot.create(:tagging, :tag_deck)
    @note_tag = FactoryBot.create(:tagging, :tag_note)
  end

  it 'is valid with valid data' do
    expect(@deck_tag).to be_valid
  end

  it "requires a tag_id" do
    @deck_tag.tag_id = nil
    expect(@deck_tag).to_not be_valid
  end

  it "requires a record" do
    @deck_tag.record = nil
    expect(@deck_tag).to_not be_valid
  end

  it 'is valid with valid data' do
    expect(@note_tag).to be_valid
  end

  it "requires a tag" do
    @note_tag.tag_id = nil
    expect(@note_tag).to_not be_valid
  end

  it "requires a record" do
    @note_tag.record = nil
    expect(@note_tag).to_not be_valid
  end
end
