require 'rails_helper'

RSpec.describe Deck, type: :model do
  it 'is valid with valid data' do
    deck = Deck.new
    deck.user_id = FactoryBot.create(:user, :confirmed).id
    deck.title = Faker::Lorem.sentence(word_count: 10)
    deck.description = Faker::Lorem.paragraphs(number: 4)
    deck.visibility = 0
    expect(deck).to be_valid
  end

  it 'is invalid without a user' do
    deck = Deck.new
    deck.title = Faker::Lorem.sentence(word_count: 10)
    deck.description = Faker::Lorem.paragraphs(number: 4)
    deck.visibility = 0
    expect(deck).to_not be_valid
  end

  it 'is invalid without a title' do
    deck = Deck.new
    deck.user_id = FactoryBot.create(:user, :confirmed).id
    deck.description = Faker::Lorem.paragraphs(number: 4)
    deck.visibility = 0
    expect(deck).to_not be_valid
  end

  it 'is valid without a description' do
    deck = Deck.new
    deck.user_id = FactoryBot.create(:user, :confirmed).id
    deck.title = Faker::Lorem.sentence(word_count: 10)
    deck.visibility = 0
    expect(deck).to be_valid
  end

  it 'is is invalid without visibility' do
    deck = Deck.new
    deck.user_id = FactoryBot.create(:user, :confirmed).id
    deck.title = Faker::Lorem.sentence(word_count: 10)
    deck.description = Faker::Lorem.paragraphs(number: 4)
    expect(deck).to_not be_valid
  end

  it 'it is invalid if visibility is less than zero' do
    deck = Deck.new
    deck.user_id = FactoryBot.create(:user, :confirmed).id
    deck.title = Faker::Lorem.sentence(word_count: 10)
    deck.description = Faker::Lorem.paragraphs(number: 4)
    deck.visibility = -1
    expect(deck).to_not be_valid
  end

  it 'it is invalid if visibility is greater than two' do
    deck = Deck.new
    deck.user_id = FactoryBot.create(:user, :confirmed).id
    deck.title = Faker::Lorem.sentence(word_count: 10)
    deck.description = Faker::Lorem.paragraphs(number: 4)
    deck.visibility = 3
    expect(deck).to_not be_valid
  end

  it 'is valid with no cards' do
    deck = Deck.new
    deck.user_id = FactoryBot.create(:user, :confirmed).id
    deck.title = Faker::Lorem.sentence(word_count: 10)
    deck.description = Faker::Lorem.paragraphs(number: 4)
    deck.visibility = 0
    expect(deck).to be_valid
  end

  it 'it deletes its cards if it is deleted' do
    deck = Deck.new
    deck.user_id = FactoryBot.create(:user, :confirmed).id
    deck.title = Faker::Lorem.sentence(word_count: 10)
    deck.description = Faker::Lorem.paragraphs(number: 4)
    deck.visibility = 0
    deck.save
    card1 = deck.cards.create(front: Faker::Lorem.paragraphs(number: 4),
                              back: Faker::Lorem.paragraphs(number: 4))
    card2 = deck.cards.create(front: Faker::Lorem.paragraphs(number: 4),
                              back: Faker::Lorem.paragraphs(number: 4))
    expect(deck.cards.count).to eq(2)
    expect do
      deck.destroy
    end.to change(Card, :count).by(-2)
  end

  it 'returns tag count' do
    deck = FactoryBot.create(:deck, :private, :five_tags)
    expect(deck.tags_count).to eq(5)
  end

  it "stars and unstars a deck" do
    user1 = FactoryBot.create(:user)
    deck = FactoryBot.create(:deck, :private, user_id: user1.id)
    expect(deck.stargazer?(user1)).to be false
    expect(deck.star_count).to eq(0)
    deck.add_star(user1)
    expect(deck.stargazer?(user1)).to be true
    expect(deck.star_count).to eq(1)
    deck.unstar(user1)
    expect(deck.stargazer?(user1)).to be false
    expect(deck.star_count).to eq(0)
  end

  it "collaborates and uncollaborates a deck" do
    user1 = FactoryBot.create(:user)
    deck = FactoryBot.create(:deck, :private, user_id: user1.id)
    expect(deck.collaborater?(user1)).to be false
    expect(deck.collaborators_count).to eq(0)
    deck.collaborate(user1, deck.user)
    expect(deck.collaborater?(user1)).to be true
    expect(deck.collaborators_count).to eq(1)
    deck.uncollaborate(user1, deck.user)
    expect(deck.collaborater?(user1)).to be false
    expect(deck.collaborators_count).to eq(0)
  end

  it "adds and removes a group from a deck" do
    deck = FactoryBot.create(:deck, :private)
    group = FactoryBot.create(:group)
    expect(deck.group?(group)).to be false
    expect(deck.groups_count).to eq(0)
    deck.add_group(group, deck.user)
    expect(deck.group?(group)).to be true
    expect(deck.groups_count).to eq(1)
    deck.remove_group(group, deck.user)
    expect(deck.group?(group)).to be false
    expect(deck.groups_count).to eq(0)
  end
end
