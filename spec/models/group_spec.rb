require 'rails_helper'

RSpec.describe Group, type: :model do
  before(:each) do
    @group = FactoryBot.create(:group)
  end

  it 'is valid with valid data' do
    expect(@group).to be_valid
  end

  it "requires a name" do
    @group.name = nil
    expect(@group).to_not be_valid
  end

  it "it should have a name < 51 char" do
    @group.name = 'a' * 101
    expect(@group).to_not be_valid
  end

  it "requires a user_id" do
    @group.user_id = nil
    expect(@group).to_not be_valid
  end

  it "creates a new chat preference when adding a member" do
    user = FactoryBot.create(:user, :confirmed)
    chat = FactoryBot.create(:chat)
    chat.add_group(@group)
    @group.all_group_members.each do |member|
      ChatPreference.create(user: member, chat: chat)
    end
    expect do
      @group.add_member(user, @group.user)
    end.to change(ChatPreference, :count).by(1)
  end

  it "deletes chat preference when removing a member" do
    user = FactoryBot.create(:user, :confirmed)
    chat = FactoryBot.create(:chat)
    chat.add_group(@group)
    @group.all_group_members.each do |member|
      ChatPreference.create(user: member, chat: chat)
    end
    @group.add_member(user, @group.user)
    expect do
      @group.remove_member(user, @group.user)
    end.to change(ChatPreference, :count).by(-1)
  end

  it "creates new ChatPreferences for all members when find_or_create_chat is called" do
    user1 = FactoryBot.create(:user, :confirmed)
    user2 = FactoryBot.create(:user, :confirmed)
    @group.add_member(user1, @group.user)
    @group.add_member(user2, @group.user)
    expect do
      @group.find_or_create_chat
    end.to change(ChatPreference, :count).by(3)
  end
end
