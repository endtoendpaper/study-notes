require 'rails_helper'

RSpec.describe Tag, type: :model do
  it 'is valid with valid data' do
    tag = Tag.new
    tag.name = 'my-tag'
    expect(tag).to be_valid
  end

  it 'is invalid without a name' do
    tag = Tag.new
    expect(tag).to_not be_valid
  end

  it 'is invalid if the name has spaces' do
    tag = Tag.new
    tag.name = 'my tag'
    expect(tag).to_not be_valid
  end

  it 'is invalid if the name is greater than 100 char' do
    tag = Tag.new
    tag.name = 'a' * 101
    expect(tag).to_not be_valid
  end
end
