require 'rails_helper'

RSpec.describe ChatPreference, type: :model do
  before(:each) do
    user_chat = FactoryBot.create(:chat, :dm)
    @user_chat_preference = ChatPreference.find_by(user: user_chat.participants.sample, chat: user_chat)
    group_chat = FactoryBot.create(:chat, :group)
    @group_chat_preference = ChatPreference.find_by(user: group_chat.participants.sample, chat: group_chat)
  end

  it 'is valid with valid data' do
    expect(@user_chat_preference).to be_valid
  end

  it "requires a user_id" do
    @user_chat_preference.user_id = nil
    expect(@user_chat_preference).to_not be_valid
  end

  it "requires a chat_id" do
    @user_chat_preference.chat_id = nil
    expect(@user_chat_preference).to_not be_valid
  end

  it 'is valid with valid data' do
    expect(@group_chat_preference).to be_valid
  end

  it "requires a user_id" do
    @group_chat_preference.user_id = nil
    expect(@group_chat_preference).to_not be_valid
  end

  it "requires a chat_id" do
    @group_chat_preference.chat_id = nil
    expect(@group_chat_preference).to_not be_valid
  end
end
