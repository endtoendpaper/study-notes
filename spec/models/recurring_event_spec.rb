require 'rails_helper'

RSpec.describe RecurringEvent, type: :model do
  it 'is valid with valid data' do
    recurring_event = RecurringEvent.new
    recurring_event.calendar_id = FactoryBot.create(:calendar, :internal).id
    recurring_event.summary = Faker::Lorem.sentence(word_count: 10)
    recurring_event.start = DateTime.now
    recurring_event.end = DateTime.now + 1.hour
    recurring_event.rrule = "FREQ=DAILY;INTERVAL=2;COUNT=20"
    expect(recurring_event).to be_valid
  end

  it 'is invalid without an rrule' do
    recurring_event = RecurringEvent.new
    recurring_event.calendar_id = FactoryBot.create(:calendar, :internal).id
    recurring_event.summary = Faker::Lorem.sentence(word_count: 10)
    recurring_event.start = DateTime.now
    recurring_event.end = DateTime.now + 1.hour
    expect(recurring_event).to_not be_valid
  end

  it 'is invalid without a calendar' do
    recurring_event = RecurringEvent.new
    recurring_event.summary = Faker::Lorem.sentence(word_count: 10)
    recurring_event.start = DateTime.now
    recurring_event.end = DateTime.now + 1.hour
    recurring_event.rrule = "FREQ=DAILY;INTERVAL=2;COUNT=20"
    expect(recurring_event).to_not be_valid
  end

  it 'is invalid without a summary' do
    recurring_event = RecurringEvent.new
    recurring_event.calendar_id = FactoryBot.create(:calendar, :internal).id
    recurring_event.start = DateTime.now
    recurring_event.end = DateTime.now + 1.hour
    recurring_event.rrule = "FREQ=DAILY;INTERVAL=2;COUNT=20"
    expect(recurring_event).to_not be_valid
  end

  it 'is invalid without a start' do
    recurring_event = RecurringEvent.new
    recurring_event.calendar_id = FactoryBot.create(:calendar, :internal).id
    recurring_event.summary = Faker::Lorem.sentence(word_count: 10)
    recurring_event.end = DateTime.now + 1.hour
    recurring_event.rrule = "FREQ=DAILY;INTERVAL=2;COUNT=20"
    expect(recurring_event).to_not be_valid
  end

  it 'is invalid without an end' do
    recurring_event = RecurringEvent.new
    recurring_event.calendar_id = FactoryBot.create(:calendar, :internal).id
    recurring_event.summary = Faker::Lorem.sentence(word_count: 10)
    recurring_event.start = DateTime.now
    recurring_event.rrule = "FREQ=DAILY;INTERVAL=2;COUNT=20"
    expect(recurring_event).to_not be_valid
  end

  it 'is invalid if it starts after it ends' do
    recurring_event = RecurringEvent.new
    recurring_event.calendar_id = FactoryBot.create(:calendar, :internal).id
    recurring_event.summary = Faker::Lorem.sentence(word_count: 10)
    recurring_event.start = DateTime.now + 5.minutes
    recurring_event.end = DateTime.now
    recurring_event.rrule = "FREQ=DAILY;INTERVAL=2;COUNT=20"
    expect(recurring_event).to_not be_valid
  end

  it 'sets recurrence_end when saving' do
    recurring_event = RecurringEvent.new
    recurring_event.calendar_id = FactoryBot.create(:calendar, :internal).id
    recurring_event.summary = Faker::Lorem.sentence(word_count: 10)
    recurring_event.start = DateTime.now
    recurring_event.end = DateTime.now + 1.hour
    recurring_event.rrule = "FREQ=DAILY;INTERVAL=2;COUNT=20"
    recurring_event.save
    expect(recurring_event.reload.recurrence_end).to_not be_nil
  end
end
