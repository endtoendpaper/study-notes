require 'rails_helper'

RSpec.describe Calendar, type: :model do
  it 'is valid with valid data' do
    calendar = Calendar.new
    calendar.user_id = FactoryBot.create(:user, :confirmed).id
    calendar.title = Faker::Lorem.sentence(word_count: 10)
    calendar.description = Faker::Lorem.paragraphs(number: 4)
    calendar.visibility = 0
    expect(calendar).to be_valid
  end

  it 'is invalid without a user' do
    calendar = Calendar.new
    calendar.title = Faker::Lorem.sentence(word_count: 10)
    calendar.description = Faker::Lorem.paragraphs(number: 4)
    calendar.visibility = 0
    expect(calendar).to_not be_valid
  end

  it 'is invalid without a title' do
    calendar = Calendar.new
    calendar.user_id = FactoryBot.create(:user, :confirmed).id
    calendar.description = Faker::Lorem.paragraphs(number: 4)
    calendar.visibility = 0
    expect(calendar).to_not be_valid
  end

  it 'is invalid with title longer than 255' do
    calendar = Calendar.new
    calendar.user_id = FactoryBot.create(:user, :confirmed).id
    calendar.title = 'a' * 256
    calendar.description = Faker::Lorem.paragraphs(number: 4)
    calendar.visibility = 0
    expect(calendar).to_not be_valid
  end

  it 'is valid without a description' do
    calendar = Calendar.new
    calendar.user_id = FactoryBot.create(:user, :confirmed).id
    calendar.title = Faker::Lorem.sentence(word_count: 10)
    calendar.visibility = 0
    expect(calendar).to be_valid
  end

  it 'is is invalid without visibility' do
    calendar = Calendar.new
    calendar.user_id = FactoryBot.create(:user, :confirmed).id
    calendar.title = Faker::Lorem.sentence(word_count: 10)
    calendar.description = Faker::Lorem.paragraphs(number: 4)
    expect(calendar).to_not be_valid
  end

  it 'it is invalid if visibility is less than zero' do
    calendar = Calendar.new
    calendar.user_id = FactoryBot.create(:user, :confirmed).id
    calendar.title = Faker::Lorem.sentence(word_count: 10)
    calendar.description = Faker::Lorem.paragraphs(number: 4)
    calendar.visibility = -1
    expect(calendar).to_not be_valid
  end

  it 'it is invalid if visibility is greater than two' do
    calendar = Calendar.new
    calendar.user_id = FactoryBot.create(:user, :confirmed).id
    calendar.title = Faker::Lorem.sentence(word_count: 10)
    calendar.description = Faker::Lorem.paragraphs(number: 4)
    calendar.visibility = 3
    expect(calendar).to_not be_valid
  end

  it 'it deletes its events if it is deleted' do
    calendar = FactoryBot.create(:calendar, :internal, :thirty_events)
    expect(calendar.events.count).to eq(30)
    expect do
      calendar.reload.destroy
    end.to change(Event, :count).by(-30)
  end

  it 'returns tag count' do
    calendar = FactoryBot.create(:calendar, :private, :five_tags)
    expect(calendar.tags_count).to eq(5)
  end

  it "stars and unstars a calendar" do
    user1 = FactoryBot.create(:user)
    calendar = FactoryBot.create(:calendar, :private, user_id: user1.id)
    expect(calendar.stargazer?(user1)).to be false
    expect(calendar.star_count).to eq(0)
    calendar.add_star(user1)
    expect(calendar.stargazer?(user1)).to be true
    expect(calendar.star_count).to eq(1)
    calendar.unstar(user1)
    expect(calendar.stargazer?(user1)).to be false
    expect(calendar.star_count).to eq(0)
  end

  it "collaborates and uncollaborates a calendar" do
    user1 = FactoryBot.create(:user)
    calendar = FactoryBot.create(:calendar, :private, user_id: user1.id)
    expect(calendar.collaborater?(user1)).to be false
    expect(calendar.collaborators_count).to eq(0)
    calendar.collaborate(user1, calendar.user)
    expect(calendar.collaborater?(user1)).to be true
    expect(calendar.collaborators_count).to eq(1)
    calendar.uncollaborate(user1, calendar.user)
    expect(calendar.collaborater?(user1)).to be false
    expect(calendar.collaborators_count).to eq(0)
  end

  it "adds and removes a group from a calendar" do
    calendar = FactoryBot.create(:calendar, :private)
    group = FactoryBot.create(:group)
    expect(calendar.group?(group)).to be false
    expect(calendar.groups_count).to eq(0)
    calendar.add_group(group, calendar.user)
    expect(calendar.group?(group)).to be true
    expect(calendar.groups_count).to eq(1)
    calendar.remove_group(group, calendar.user)
    expect(calendar.group?(group)).to be false
    expect(calendar.groups_count).to eq(0)
  end

  it "generate_calendar_grid returns an array of dates a multiple of 7" do
    date = Faker::Date.between(from: '2014-09-23', to: DateTime.now + 2.years)
    calendar = FactoryBot.create(:calendar, :private)
    dates = Calendar.generate_calendar_grid(date.year, date.month)
    expect(dates.flatten.count % 7).to eq(0)
    expect(dates.flatten.first.class).to eq(Date)
  end

  it "get_events_for returns only events within that time period" do
    date = Faker::Date.between(from: '2014-09-23', to: DateTime.now + 2.years)
    prev_date = date - 1.month - 6.days
    next_date = date + 1.month + 6.days
    calendar = FactoryBot.create(:calendar, :private)
    event = FactoryBot.create(:event, calendar_id: calendar.id, start: date + 2.hours, end: date + 3.hours)
    prev_event = FactoryBot.create(:event, calendar_id: calendar.id, start: prev_date + 2.hours, end: prev_date + 3.hours)
    next_event = FactoryBot.create(:event, calendar_id: calendar.id, start: next_date + 2.hours, end: next_date + 3.hours)
    dates = (Calendar.generate_calendar_grid( date.year, date.month)).flatten
    date_events = calendar.get_events_for dates, 'UTC'
    expect(date_events[date].present?).to eq(true)
    expect(date_events[date]).to eq([event])
    expect(date_events[prev_date].present?).to eq(false)
    expect(date_events[next_date].present?).to eq(false)
  end

  it "get_events_for returns only n events if limit is set to n" do
    date = Faker::Date.between(from: '2014-09-23', to: DateTime.now + 2.years)
    prev_date = date - 1.month - 6.days
    next_date = date + 1.month + 6.days
    calendar = FactoryBot.create(:calendar, :private)
    10.times do
      FactoryBot.create(:event, calendar_id: calendar.id, start: date + 2.hours, end: date + 3.hours)
    end
    dates = (Calendar.generate_calendar_grid(date.year, date.month)).flatten
    date_events = calendar.get_events_for dates, 'UTC', 5
    expect(date_events[date].present?).to eq(true)
    expect(date_events[date].count).to eq(5)
  end
end
