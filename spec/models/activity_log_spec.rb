require 'rails_helper'

RSpec.describe ActivityLog, type: :model do
  it "deletes a user's older activity logs and keeps the latest 50" do
    user_id = FactoryBot.create(:user, :confirmed).id
    2.times do
      FactoryBot.create(:activity_log, :follow_activity, user_id: user_id)
      FactoryBot.create(:activity_log, :unfollow_activity, user_id: user_id)
      FactoryBot.create(:activity_log, :starred_private_activity, user_id: user_id)
      FactoryBot.create(:activity_log, :unstarred_private_activity, user_id: user_id)
      FactoryBot.create(:activity_log, :tagged_private_activity, user_id: user_id)
      FactoryBot.create(:activity_log, :untagged_private_activity, user_id: user_id)
      FactoryBot.create(:activity_log, :created_private_item_activity, user_id: user_id)
      FactoryBot.create(:activity_log, :edited_private_item_activity, user_id: user_id)
      FactoryBot.create(:activity_log, :created_private_subitem_activity, user_id: user_id)
      FactoryBot.create(:activity_log, :edited_private_subitem_activity, user_id: user_id)
      FactoryBot.create(:activity_log, :starred_internal_activity, user_id: user_id)
      FactoryBot.create(:activity_log, :unstarred_internal_activity, user_id: user_id)
      FactoryBot.create(:activity_log, :tagged_internal_activity, user_id: user_id)
      FactoryBot.create(:activity_log, :untagged_internal_activity, user_id: user_id)
      FactoryBot.create(:activity_log, :created_internal_item_activity, user_id: user_id)
      FactoryBot.create(:activity_log, :edited_internal_item_activity, user_id: user_id)
      FactoryBot.create(:activity_log, :created_internal_subitem_activity, user_id: user_id)
      FactoryBot.create(:activity_log, :edited_internal_subitem_activity, user_id: user_id)
      FactoryBot.create(:activity_log, :starred_public_activity, user_id: user_id)
      FactoryBot.create(:activity_log, :unstarred_public_activity, user_id: user_id)
      FactoryBot.create(:activity_log, :tagged_public_activity, user_id: user_id)
      FactoryBot.create(:activity_log, :untagged_public_activity, user_id: user_id)
      FactoryBot.create(:activity_log, :created_public_item_activity, user_id: user_id)
      FactoryBot.create(:activity_log, :edited_public_item_activity, user_id: user_id)
      FactoryBot.create(:activity_log, :created_public_subitem_activity, user_id: user_id)
    end
    expect do
      FactoryBot.create(:activity_log, :edited_public_subitem_activity, user_id: user_id)
    end.to change(ActivityLog, :count).by(0)
    expect do
      FactoryBot.create(:activity_log, :edited_public_subitem_activity)
    end.to change(ActivityLog, :count).by(1)
  end

  it "is deleted when user is deleted" do
    log = FactoryBot.create(:activity_log, :edited_public_subitem_activity)
    expect do
      log.user.destroy
    end.to change(ActivityLog, :count).by(-1)
  end

  it "is deleted when record is deleted" do
    log1 = FactoryBot.create(:activity_log, :edited_public_subitem_activity)
    log2 = FactoryBot.create(:activity_log, :follow_activity)
    expect do
      log1.record.destroy
      log2.record.destroy
    end.to change(ActivityLog, :count).by(-2)
  end
end
