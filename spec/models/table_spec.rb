require 'rails_helper'

RSpec.describe Table, type: :model do
  before(:each) do
    @table = FactoryBot.create(:table, :card, :record_saved)
    @card = FactoryBot.create(:card, :private)
  end

  it 'is valid with valid data' do
    expect(@table).to be_valid
  end

  it 'is valid without a record' do
    @table.record_id = nil
    @table.record_type = nil
    expect(@table).to be_valid
  end

  it "delete unmatched tables created over 24 hours ago" do
    FactoryBot.create(:table, :record_saved, :updated_30_hr_ago, record: @card)
    FactoryBot.create(:table, :record_saved, :updated_30_hr_ago, record: @card)
    FactoryBot.create(:table, :updated_30_hr_ago, record: @card)
    FactoryBot.create(:table, :updated_30_hr_ago, record: @card)
    FactoryBot.create(:table, :updated_30_hr_ago)
    FactoryBot.create(:table, :updated_30_hr_ago)
    expect(Table.count).to eq(7)
    Table.delete_unmatched_tables
    expect(Table.count).to eq(3)
    FactoryBot.create(:table, :record_saved, :updated_3_day_ago, record: @card)
    FactoryBot.create(:table, :record_saved, :updated_3_day_ago, record: @card)
    FactoryBot.create(:table, :updated_3_day_ago, record: @card)
    FactoryBot.create(:table, :updated_3_day_ago, record: @card)
    FactoryBot.create(:table, :updated_3_day_ago)
    FactoryBot.create(:table, :updated_3_day_ago)
    expect(Table.count).to eq(9)
    Table.delete_unmatched_tables
    expect(Table.count).to eq(5)
    FactoryBot.create(:table, :record_saved, record: @card)
    FactoryBot.create(:table, :record_saved, record: @card)
    FactoryBot.create(:table, record: @card)
    FactoryBot.create(:table, record: @card)
    FactoryBot.create(:table)
    FactoryBot.create(:table)
    expect(Table.count).to eq(11)
    Table.delete_unmatched_tables
    expect(Table.count).to eq(11)
  end

  it "deletes all unmatched tables" do
    FactoryBot.create(:table, :record_saved, :updated_30_hr_ago, record: @card)
    FactoryBot.create(:table, :record_saved, :updated_30_hr_ago, record: @card)
    FactoryBot.create(:table, :updated_30_hr_ago, record: @card)
    FactoryBot.create(:table, :updated_30_hr_ago, record: @card)
    FactoryBot.create(:table, :updated_30_hr_ago)
    FactoryBot.create(:table, :updated_30_hr_ago)
    expect(Table.count).to eq(7)
    Table.delete_all_unmatched_tables
    expect(Table.count).to eq(3)
    FactoryBot.create(:table, :record_saved, :updated_3_day_ago, record: @card)
    FactoryBot.create(:table, :record_saved, :updated_3_day_ago, record: @card)
    FactoryBot.create(:table, :updated_3_day_ago, record: @card)
    FactoryBot.create(:table, :updated_3_day_ago, record: @card)
    FactoryBot.create(:table, :updated_3_day_ago)
    FactoryBot.create(:table, :updated_3_day_ago)
    expect(Table.count).to eq(9)
    Table.delete_all_unmatched_tables
    expect(Table.count).to eq(5)
    FactoryBot.create(:table, :record_saved, record: @card)
    FactoryBot.create(:table, :record_saved, record: @card)
    FactoryBot.create(:table, record: @card)
    FactoryBot.create(:table, record: @card)
    FactoryBot.create(:table)
    FactoryBot.create(:table)
    expect(Table.count).to eq(11)
    Table.delete_all_unmatched_tables
    expect(Table.count).to eq(7)
  end
end
