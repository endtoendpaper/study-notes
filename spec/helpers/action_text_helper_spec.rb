# spec/helpers/action_text_helper_spec.rb
require 'rails_helper'

RSpec.describe ActionTextHelper, type: :helper do
  describe '#truncate_actiontext_html' do
    let(:attachment_html) do
      <<~HTML
        <action-text-attachment content-type="image/jpeg" width="192" height="192">
          <figure class="attachment attachment--preview attachment--jpg">
            <img src="http://localhost:3000/sample_image.jpg" alt="Sample Image">
            <figcaption class="attachment__caption">
              <span class="attachment__download">
                <a href="http://localhost:3000/sample_image.jpg">sample_image.jpg</a>
              </span>
              <span class="attachment__size">1.44 KB</span>
            </figcaption>
          </figure>
        </action-text-attachment>
      HTML
    end

    let(:long_html) do
      <<~HTML
        <p>This is a long text that will be truncated.</p>
        <p>Here is an image:</p>
        #{attachment_html}
        <p>And here is some more text that exceeds the maximum length limit.</p>
      HTML
    end

    let(:read_more_path) { '/path/to/full/content' }

    it 'truncates the HTML content and appends a "read more" link' do
      truncated_html = truncate_actiontext_html(long_html, max_length: 50, read_more_path: read_more_path)

      expect(truncated_html).to include('This is a long text that will be truncated.')
      expect(truncated_html).to include('…read more')
      expect(truncated_html).to include("<a href=\"/path/to/full/content\" class=\" \">…read more</a>")
      expect(truncated_html).not_to include('And here is some more text that exceeds the maximum length limit.')
    end

    it 'includes action text attachments in the output' do
      truncated_html = truncate_actiontext_html(long_html, max_length: 100, read_more_path: read_more_path)

      expect(truncated_html).to include('<action-text-attachment')
      expect(truncated_html).to include('sample_image.jpg')
      expect(truncated_html).to include('http://localhost:3000/sample_image.jpg')
    end
  end
end
