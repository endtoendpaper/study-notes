FactoryBot.define do
  factory :calendar do
    user_id { FactoryBot.create(:user, :confirmed).id }
    sequence(:title) { |n| "#{Faker::Movie.title} #{n}" }
    background_color { Faker::Color.hex_color }
    text_color { Faker::Color.hex_color }
    description { Faker::TvShows::TheExpanse.quote }

    trait :private do
      visibility { 0 }
    end

    trait :internal do
      visibility { 1 }
    end

    trait :public do
      visibility { 2 }
    end

    trait :one_event do
      after(:create) do |calendar|
        create(
          :event,
          calendar_id: calendar.id
        )
      end
    end

    trait :one_tag do
      after(:create) do |calendar|
        calendar.tags << create(
          :tag
        )
      end
    end

    trait :five_tags do
      after(:create) do |calendar|
        5.times do
          calendar.tags << create(
            :tag
          )
        end
      end
    end

    trait :two_events do
      after(:create) do |calendar|
        create(
            :event,
            :all_day,
            calendar_id: calendar.id
          )
        create(
          :event,
          :all_day,
          calendar_id: calendar.id
        )
      end
    end

    trait :thirty_events do
      after(:create) do |calendar|
        20.times do
          create(
            :event,
            calendar_id: calendar.id
          )
        end
        10.times do
          create(
            :event,
            :all_day,
            calendar_id: calendar.id
          )
        end
      end
    end

    trait :two_recurring_events do
      after(:create) do |calendar|
        create(
          :recurring_event,
          calendar_id: calendar.id
        )
        create(
          :recurring_event,
          :all_day,
          calendar_id: calendar.id
        )
      end
    end

    trait :six_recurring_events do
      after(:create) do |calendar|
        3.times do
          create(
            :recurring_event,
            calendar_id: calendar.id
          )
        end
        3.times do
          create(
            :recurring_event,
            :all_day,
            calendar_id: calendar.id
          )
        end
      end
    end
  end
end
