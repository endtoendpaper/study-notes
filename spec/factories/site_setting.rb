FactoryBot.define do
  factory :siteSetting do
    site_name { Faker::Team.name }

    trait :no_registrations do
      users_can_join { false }
    end

    trait :allow_registrations do
      users_can_join { true }
    end

    trait :default_avatar do
      default_avatar { Rack::Test::UploadedFile.new('./spec/factories/images/default-avatar.jpg', 'image/jpeg') }
    end

    trait :logo do
      logo { Rack::Test::UploadedFile.new('./spec/factories/images/logo.png', 'image/jpeg') }
    end
  end
end
