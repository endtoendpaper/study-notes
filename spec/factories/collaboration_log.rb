FactoryBot.define do
  factory :collaboration_log do
    user_id { FactoryBot.create(:user, :confirmed).id }
  end

  trait :log_deck do
    item_id { FactoryBot.create(:deck, :public).id }
    item_type { 'Deck' }
  end

  trait :log_note do
    item_id { FactoryBot.create(:note, :public).id }
    item_type { 'Note' }
  end

  trait :log_collaborator do
    collaboration_id { FactoryBot.create(:user, :confirmed).id }
    collaboration_type { 'User' }
    action { :added_collaborator }
  end

  trait :log_group do
    collaboration_id { FactoryBot.create(:group).id }
    collaboration_type { 'Group' }
    action { :added_group }
  end
end
