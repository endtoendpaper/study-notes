FactoryBot.define do
  factory :recurring_event do
    sequence(:summary) { |n| "Recurring #{Faker::Esport.event} #{n}" }
    location { Faker::Address.full_address }
    start { Faker::Time.between_dates(from: Date.today - 70, to: Date.today + 70, period: :all) }
    self.end { Date.today + 71 }
    description { Faker::TvShows::MichaelScott.quote }
    rrule { ["FREQ=YEARLY;BYMONTH=9;BYMONTHDAY=4;COUNT=50",
      "FREQ=YEARLY;BYMONTH=6;BYDAY=TU;BYSETPOS=3;COUNT=3",
      "FREQ=MONTHLY;BYMONTHDAY=1;INTERVAL=1;COUNT=4",
      "FREQ=MONTHLY;INTERVAL=2;BYDAY=WE;BYSETPOS=2;COUNT=7",
      "FREQ=MONTHLY;INTERVAL=1;BYDAY=TH;BYSETPOS=-1;UNTIL=#{(Date.today + 12.months).strftime('%Y%m%d')}",
      "FREQ=WEEKLY;INTERVAL=2;BYDAY=MO,WE,FR;UNTIL=#{(Date.today + 12.months).strftime('%Y%m%d')}",
      "FREQ=WEEKLY;INTERVAL=1;BYDAY=TH;COUNT=7",
      "FREQ=DAILY;INTERVAL=3;COUNT=10",
      "FREQ=DAILY;INTERVAL=2;COUNT=20",
      "FREQ=WEEKLY;INTERVAL=1;BYDAY=TU,FR;UNTIL=#{(Date.today + 12.months).strftime('%Y%m%d')}",
      "FREQ=DAILY;INTERVAL=3;UNTIL=#{(Date.today + 9.months).strftime('%Y%m%d')}",
      "FREQ=MONTHLY;INTERVAL=1;BYDAY=TU;BYSETPOS=2;UNTIL=#{(Date.today + 6.months).strftime('%Y%m%d')}"].shuffle.first
    }

    trait :private do
      calendar_id { FactoryBot.create(:calendar, :private).id }
    end

    trait :internal do
      calendar_id { FactoryBot.create(:calendar, :internal).id }
    end

    trait :public do
      calendar_id { FactoryBot.create(:calendar, :public).id }
    end

    trait :all_day do
      all_day { true }
    end

    after(:create) do |event|
      rand = 1 + rand(26)
      event.update(end: event.start + rand.hours)
    end
  end
end
