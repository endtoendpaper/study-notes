FactoryBot.define do
  factory :tagging do
    tag_id { FactoryBot.create(:tag).id }
  end

  trait :tag_deck do
    record_id { FactoryBot.create(:deck, :public).id }
    record_type { 'Deck' }
  end

  trait :tag_note do
    record_id { FactoryBot.create(:note, :public).id }
    record_type { 'Note' }
  end
end
