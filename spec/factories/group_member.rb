FactoryBot.define do
  factory :group_member do
    group_id { FactoryBot.create(:group).id }
    user_id { FactoryBot.create(:user, :confirmed).id }
  end
end
