FactoryBot.define do
  factory :event do
    sequence(:summary) { |n| "#{Faker::Esport.event} #{n}" }
    location { Faker::Address.full_address }
    start { Faker::Time.between_dates(from: Date.today - 70, to: Date.today + 70, period: :all) }
    self.end { Date.today + 71 }
    description { Faker::Movies::PrincessBride.quote }

    trait :private do
      calendar_id { FactoryBot.create(:calendar, :private).id }
    end

    trait :internal do
      calendar_id { FactoryBot.create(:calendar, :internal).id }
    end

    trait :public do
      calendar_id { FactoryBot.create(:calendar, :public).id }
    end

    trait :all_day do
      all_day { true }
    end

    after(:create) do |event|
      rand = 1 + rand(26)
      event.update(end: event.start + rand.hours)
    end
  end
end
