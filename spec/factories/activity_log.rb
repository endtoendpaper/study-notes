FactoryBot.define do
  factory :activity_log do
    user_id { FactoryBot.create(:user, :confirmed).id }
  end

  trait :follow_activity do
    record_id { FactoryBot.create(:user, :confirmed).id }
    record_type { 'User' }
    action { :followed }
  end

  trait :unfollow_activity do
    record_id { FactoryBot.create(:user, :confirmed).id }
    record_type { 'User' }
    action { :unfollowed }
  end

  trait :starred_private_activity do
    record_id { FactoryBot.create(:deck, :private).id }
    record_type { 'Deck' }
    action { :starred }
  end

  trait :unstarred_private_activity do
    record_id { FactoryBot.create(:note, :private).id }
    record_type { 'Note' }
    action { :unstarred }
  end

  trait :tagged_private_activity do
    record_id { FactoryBot.create(:deck, :private).id }
    record_type { 'Deck' }
    action { :tagged }
    details { 'with myTag' }
  end

  trait :untagged_private_activity do
    record_id { FactoryBot.create(:note, :private).id }
    record_type { 'Note' }
    action { :untagged }
    details { 'with myTag' }
  end

  trait :created_private_item_activity do
    record_id { FactoryBot.create(:deck, :private).id }
    record_type { 'Deck' }
    action { :created }
  end

  trait :edited_private_item_activity do
    record_id { FactoryBot.create(:note, :private).id }
    record_type { 'Note' }
    action { :edited }
  end

  trait :created_private_subitem_activity do
    record_id { FactoryBot.create(:card, :private).id }
    record_type { 'Card' }
    action { :created }
  end

  trait :edited_private_subitem_activity do
    record_id { FactoryBot.create(:card, :private).id }
    record_type { 'Card' }
    action { :edited }
  end

  trait :starred_internal_activity do
    record_id { FactoryBot.create(:deck, :internal).id }
    record_type { 'Deck' }
    action { :starred }
  end

  trait :unstarred_internal_activity do
    record_id { FactoryBot.create(:note, :internal).id }
    record_type { 'Note' }
    action { :unstarred }
  end

  trait :tagged_internal_activity do
    record_id { FactoryBot.create(:deck, :internal).id }
    record_type { 'Deck' }
    action { :tagged }
  end

  trait :untagged_internal_activity do
    record_id { FactoryBot.create(:note, :internal).id }
    record_type { 'Note' }
    action { :untagged }
    details { 'with myTag' }
  end

  trait :created_internal_item_activity do
    record_id { FactoryBot.create(:deck, :internal).id }
    record_type { 'Deck' }
    action { :created }
  end

  trait :edited_internal_item_activity do
    record_id { FactoryBot.create(:note, :internal).id }
    record_type { 'Note' }
    action { :edited }
  end

  trait :created_internal_subitem_activity do
    record_id { FactoryBot.create(:card, :internal).id }
    record_type { 'Card' }
    action { :created }
  end

  trait :edited_internal_subitem_activity do
    record_id { FactoryBot.create(:card, :internal).id }
    record_type { 'Card' }
    action { :edited }
  end

  trait :starred_public_activity do
    record_id { FactoryBot.create(:deck, :public).id }
    record_type { 'Deck' }
    action { :starred }
  end

  trait :unstarred_public_activity do
    record_id { FactoryBot.create(:note, :public).id }
    record_type { 'Note' }
    action { :unstarred }
  end

  trait :tagged_public_activity do
    record_id { FactoryBot.create(:deck, :public).id }
    record_type { 'Deck' }
    action { :tagged }
    details { 'with myTag' }
  end

  trait :untagged_public_activity do
    record_id { FactoryBot.create(:note, :public).id }
    record_type { 'Note' }
    action { :untagged }
    details { 'with myTag' }
  end

  trait :created_public_item_activity do
    record_id { FactoryBot.create(:deck, :public).id }
    record_type { 'Deck' }
    action { :created }
  end

  trait :edited_public_item_activity do
    record_id { FactoryBot.create(:note, :public).id }
    record_type { 'Note' }
    action { :edited }
  end

  trait :created_public_subitem_activity do
    record_id { FactoryBot.create(:card, :public).id }
    record_type { 'Card' }
    action { :created }
  end

  trait :edited_public_subitem_activity do
    record_id { FactoryBot.create(:card, :public).id }
    record_type { 'Card' }
    action { :edited }
  end
end
