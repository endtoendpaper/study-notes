FactoryBot.define do
  factory :message do
    content { Faker::TvShows::VentureBros.quote }
    user_id { FactoryBot.create(:user, :confirmed).id }
    user_username { 'temp_username' }

    trait :group do
      chat_id { FactoryBot.create(:chat, :group).id }
    end

    trait :dm do
      chat_id { FactoryBot.create(:chat, :dm).id }
    end

    after(:create) do |message|
      unless message.chat.participants.include?(message.user)
        new_user = message.chat.participants.sample
        message.update(user: new_user, user_username: new_user.username)
      end
    end
  end
end
