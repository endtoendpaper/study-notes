FactoryBot.define do
  factory :group do
    sequence(:name) { |n| "#{Faker::Movies::StarWars.call_squadron} #{n}" }
    user_id { FactoryBot.create(:user, :confirmed).id }
    description { Faker::Movies::StarWars.quote }

    trait :avatar do
      avatar { Rack::Test::UploadedFile
               .new('spec/factories/images/crying-cat.jpeg', 'image/jpeg') }
    end
  end
end
