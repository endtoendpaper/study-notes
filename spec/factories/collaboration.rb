FactoryBot.define do
  factory :collaboration do
    user_id { FactoryBot.create(:user, :confirmed).id }
  end

  trait :on_deck do
    record_id { FactoryBot.create(:deck, :public).id }
    record_type { 'Deck' }
  end

  trait :on_note do
    record_id { FactoryBot.create(:note, :public).id }
    record_type { 'Note' }
  end
end
