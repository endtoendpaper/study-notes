FactoryBot.define do
  factory :group_assignment do
    group_id { FactoryBot.create(:group).id }
  end

  trait :to_deck do
    record_id { FactoryBot.create(:deck, :public).id }
    record_type { 'Deck' }
  end

  trait :to_note do
    record_id { FactoryBot.create(:note, :public).id }
    record_type { 'Note' }
  end
end
