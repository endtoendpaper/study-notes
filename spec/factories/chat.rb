FactoryBot.define do
  factory :chat do
    trait :dm do
      after(:create) do |chat|
        @user1 = FactoryBot.create(:user, :confirmed)
        @user2 = FactoryBot.create(:user, :confirmed)
        chat.collaborate(@user1)
        chat.collaborate(@user2)
        ChatPreference.create(user: @user1, chat: chat)
        ChatPreference.create(user: @user2, chat: chat)
      end
    end

    trait :group do
      after(:create) do |chat|
        @group = FactoryBot.create(:group)
        @group.add_member(FactoryBot.create(:user, :confirmed), @group.user)
        @group.add_member(FactoryBot.create(:user, :confirmed), @group.user)
        chat.add_group(@group, @group.user)
        @group.all_group_members.each do |member|
          ChatPreference.create(user: member, chat: chat)
        end
      end
    end

    trait :one_msg do
      after(:create) do |chat|
        @user = chat.participants.sample
        create(
          :message,
          chat_id: chat.id,
          user_id: @user.id,
          user_username: @user.username
        )
      end
    end

    trait :two_hundred_msgs do
      after(:create) do |chat|
        200.times do
          @user = chat.participants.sample
          create(
            :message,
            chat_id: chat.id,
            user_id: @user.id,
            user_username: @user.username
          )
        end
      end
    end

    trait :dm_with_first_user do
      after(:create) do |chat|
        @user1 = User.ordered_by_id.first
        @user2 = FactoryBot.create(:user, :confirmed)
        chat.collaborate(@user1)
        chat.collaborate(@user2)
        ChatPreference.create(user: @user1, chat: chat)
        ChatPreference.create(user: @user2, chat: chat)
      end
    end

    trait :group_with_first_user do
      after(:create) do |chat|
        @group = FactoryBot.create(:group)
        @group.add_member(User.ordered_by_id.first, @group.user)
        @group.add_member(FactoryBot.create(:user, :confirmed), @group.user)
        chat.add_group(@group)
        @group.all_group_members.each do |member|
          ChatPreference.create(user: member, chat: chat)
        end
      end
    end

    trait :dm_with_first_user_pinned do
      after(:create) do |chat|
        @user1 = User.ordered_by_id.first
        @user2 = FactoryBot.create(:user, :confirmed)
        chat.collaborate(@user1)
        chat.collaborate(@user2)
        ChatPreference.create(user: @user1, chat: chat, pin: true)
        ChatPreference.create(user: @user2, chat: chat)
      end
    end

    trait :group_with_first_user_pinned do
      after(:create) do |chat|
        @group = FactoryBot.create(:group)
        @group.add_member(User.ordered_by_id.first, @group.user)
        @group.add_member(FactoryBot.create(:user, :confirmed), @group.user)
        chat.add_group(@group)
        @group.all_group_members.each do |member|
          ChatPreference.create(user: member, chat: chat, pin: true)
        end
      end
    end
  end
end
