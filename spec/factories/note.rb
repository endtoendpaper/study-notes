FactoryBot.define do
  factory :note do
    user_id { FactoryBot.create(:user, :confirmed).id }
    sequence(:title) { |n| "#{Faker::Book.title} #{n}" }
    text { Faker::HTML.paragraph(sentence_count: 20, supplemental: true, random_sentences_to_add: 10, exclude_words: nil) }

    trait :private do
      visibility { 0 }
    end

    trait :internal do
      visibility { 1 }
    end

    trait :public do
      visibility { 2 }
    end

    trait :one_tag do
      after(:create) do |note|
        note.tags << create(
          :tag
        )
      end
    end

    trait :five_tags do
      after(:create) do |note|
        5.times do
          note.tags << create(
            :tag
          )
        end
      end
    end
  end
end
