FactoryBot.define do
  factory :card do
    front { Faker::Lorem.sentence(word_count: 2, supplemental: true, random_words_to_add: 5) }
    back { Faker::Lorem.paragraph(sentence_count: 1, supplemental: false, random_sentences_to_add: 5) }
  end

  trait :private do
    deck_id { FactoryBot.create(:deck, :private).id }
  end

  trait :internal do
    deck_id { FactoryBot.create(:deck, :internal).id }
  end

  trait :public do
    deck_id { FactoryBot.create(:deck, :public).id }
  end
end
