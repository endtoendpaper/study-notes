FactoryBot.define do
  factory :star do
    user_id { FactoryBot.create(:user, :confirmed).id }
  end

  trait :for_deck do
    record_id { FactoryBot.create(:deck, :public).id }
    record_type { 'Deck' }
  end

  trait :for_note do
    record_id { FactoryBot.create(:note, :public).id }
    record_type { 'Note' }
  end
end
