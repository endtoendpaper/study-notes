FactoryBot.define do
  factory :tag do
    sequence(:name) { |n| "#{Faker::Books::Lovecraft.word}-#{n}" }
  end
end
