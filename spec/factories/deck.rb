FactoryBot.define do
  factory :deck do
    user_id { FactoryBot.create(:user, :confirmed).id }
    sequence(:title) { |n| "#{Faker::DcComics.title} #{n}" }
    description { Faker::Movies::HitchhikersGuideToTheGalaxy.quote }

    trait :private do
      visibility { 0 }
    end

    trait :internal do
      visibility { 1 }
    end

    trait :public do
      visibility { 2 }
    end

    trait :one_card do
      after(:create) do |deck|
        create(
          :card,
          deck_id: deck.id
        )
      end
    end

    trait :one_tag do
      after(:create) do |deck|
        deck.tags << create(
          :tag
        )
      end
    end

    trait :five_tags do
      after(:create) do |deck|
        5.times do
          deck.tags << create(
            :tag
          )
        end
      end
    end

    trait :two_cards do
      after(:create) do |deck|
        create(
          :card,
          deck_id: deck.id
        )
        create(
          :card,
          deck_id: deck.id
        )
      end
    end

    trait :thirty_cards do
      after(:create) do |deck|
        30.times do
          create(
            :card,
            deck_id: deck.id
          )
        end
      end
    end
  end
end
