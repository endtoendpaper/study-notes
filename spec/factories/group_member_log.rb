FactoryBot.define do
  factory :group_member_log do
    user_id { FactoryBot.create(:user, :confirmed).id }
    group_id {  FactoryBot.create(:group).id }
    member_id { FactoryBot.create(:user, :confirmed).id }
    action { :added_member }
  end
end
