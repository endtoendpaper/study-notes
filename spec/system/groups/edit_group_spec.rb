require 'rails_helper'

describe 'User edits group', type: :system do
  let(:name) { Faker::Team.name }
  let(:description) { Faker::Lorem.sentence(word_count: 5) }

  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group, user_id: @owner.id)
    @group.add_member(@group_member, @group.user)
  end

  scenario 'as owner with valid data' do
    sign_in @owner
    visit group_path(@group.reload)
    click_link 'Edit group'
    expect(page).to have_button('Delete Group')
    fill_in 'group_name', with: name
    fill_in 'group_description', with: description
    click_button 'Submit'
    expect(page).to have_content('Group was successfully updated.')
    expect(page).to have_content(name)
    expect(page).to have_content(description)
    expect(current_path).to include('/groups/' + name.gsub(" ", "%20"))
  end

  scenario "as admin editing user's group with valid data" do
    sign_in @admin
    visit group_path(@group.reload)
    click_link 'Edit group'
    expect(page).to have_button('Delete Group')
    fill_in 'group_name', with: name
    fill_in 'group_description', with: description
    click_button 'Submit'
    expect(page).to have_content('Group was successfully updated.')
    expect(page).to have_content(name)
    expect(page).to have_content(description)
    expect(current_path).to include('/groups/' + name.gsub(" ", "%20"))
  end

  scenario 'as owner with invalid data', js: true do
    sign_in @owner
    visit group_path(@group.reload)
    click_link 'Edit group'
    expect(page).to have_button('Delete Group')
    fill_in 'group_name', with: 'a' * 101
    fill_in 'group_description', with: description
    click_button 'Submit'
    expect(current_path).to include('/groups/' + @group.name.gsub(" ", "%20") + '/edit')
    expect(page).to have_content('1 error prohibited this group from being saved:')
    expect(page).to have_content("Name is too long")
  end

  scenario 'as group member with valid data' do
    sign_in @group_member
    visit group_path(@group.reload)
    click_link 'Edit group'
    expect(page).to_not have_button('Delete Group')
    fill_in 'group_name', with: name
    fill_in 'group_description', with: description
    click_button 'Submit'
    expect(page).to have_content('Group was successfully updated.')
    expect(page).to have_content(name)
    expect(page).to have_content(description)
    expect(current_path).to include('/groups/' + name.gsub(" ", "%20"))
  end
end
