require 'rails_helper'

describe 'User views group index', type: :system do
  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @group1 = FactoryBot.create(:group, user_id: @owner.id)
    @group2 = FactoryBot.create(:group)
    @group2.add_member(@owner, @group2.reload.user)
  end

  scenario 'admin', js: true do
    sign_in @admin
    visit show_groups_user_path(@owner)
    expect(page).to have_link(@owner.handle)
    expect(page).to have_content('2 groups')
    expect(page).to have_link(@group1.name)
    expect(page).to have_link(@group2.name)
    expect(page).to have_content('1 member')
    expect(page).to have_content('2 members')
    expect(page).to have_content(@group1.description)
    expect(page).to have_content(@group2.description)
    expect(page).to_not have_link('Edit members')
    expect(page).to_not have_link('Edit group')
    expect(page).to have_content('Show owned groups')
    page.check('view_owned')
    expect(page).to have_link(@group1.name)
    expect(page).to_not have_link(@group2.name)
  end

  scenario "as a group owner", js: true do
    sign_in @owner
    visit show_groups_user_path(@owner)
    expect(page).to have_link(@owner.handle)
    expect(page).to have_content('2 groups')
    expect(page).to have_link(@group1.name)
    expect(page).to have_link(@group2.name)
    expect(page).to have_content('1 member')
    expect(page).to have_content('2 members')
    expect(page).to have_content(@group1.description)
    expect(page).to have_content(@group2.description)
    expect(page).to_not have_link('Edit members')
    expect(page).to_not have_link('Edit group')
    expect(page).to have_content('Show owned groups')
    page.check('view_owned')
    expect(page).to have_link(@group1.name)
    expect(page).to_not have_link(@group2.name)
  end

  scenario 'as a user' do
    sign_in @user
    visit show_groups_user_path(@owner)
    expect(page).to have_link(@owner.handle)
    expect(page).to have_content('2 groups')
    expect(page).to have_link(@group1.name)
    expect(page).to have_link(@group2.name)
    expect(page).to have_content('1 member')
    expect(page).to have_content('2 members')
    expect(page).to have_content(@group1.description)
    expect(page).to have_content(@group2.description)
    expect(page).to_not have_link('Edit members')
    expect(page).to_not have_link('Edit group')
    expect(page).to_not have_content('Show owned groups')
  end

  scenario 'as a user with 6 groups and pagination' do
    sign_in @user
    4.times do
      FactoryBot.create(:group, user_id: @owner.id)
    end
    visit show_groups_user_path(@owner)
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end
end
