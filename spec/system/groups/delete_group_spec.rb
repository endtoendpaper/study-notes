require 'rails_helper'

describe 'Deletes group', type: :system do
  before(:each) do
    @user1 = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @group = FactoryBot.create(:group, user_id: @user1.id)
  end

  scenario 'as user deleting own deck', js: true do
    sign_in @user1
    visit group_path(@group.name)
    click_link 'Edit group'
    click_button 'Delete Group'
    page.accept_alert
    expect(current_path).to include('/group')
    expect(page).to have_content('Group was successfully deleted.')
    visit groups_path
    expect(page).to_not have_content(@group.name)
  end

  scenario "as admin deleting user's deck", js: true do
    sign_in @user1
    visit group_path(@group.name)
    click_link 'Edit group'
    click_button 'Delete Group'
    page.accept_alert
    expect(current_path).to include('/groups')
    expect(page).to have_content('Group was successfully deleted.')
    visit groups_path
    expect(page).to_not have_content(@group.name)
  end
end
