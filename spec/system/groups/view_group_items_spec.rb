require 'rails_helper'

describe 'Views group items', type: :system do
  before(:each) do
    reset_elasticsearch_indices

    @user1 = FactoryBot.create(:user, :confirmed)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group_owner = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)

    @group = FactoryBot.create(:group, user_id: @group_owner.id)
    @group.add_member(@group_member, @group.user)

    UserItems.item_list.each do |item|
      instance_variable_set("@#{item.singularize}_public1", FactoryBot.create(item.singularize, :public))
      instance_variable_set("@#{item.singularize}_internal1", FactoryBot.create(item.singularize, :internal))
      instance_variable_set("@#{item.singularize}_private1", FactoryBot.create(item.singularize, :private))

      instance_variable_set("@#{item.singularize}_public2", FactoryBot.create(item.singularize, :public))
      instance_variable_set("@#{item.singularize}_internal2", FactoryBot.create(item.singularize, :internal))
      instance_variable_set("@#{item.singularize}_private2", FactoryBot.create(item.singularize, :private))

      instance_variable_get("@#{item.singularize}_public1").add_group(@group, instance_variable_get("@#{item.singularize}_public1").user)
      instance_variable_get("@#{item.singularize}_internal1").add_group(@group, instance_variable_get("@#{item.singularize}_internal1").user)
      instance_variable_get("@#{item.singularize}_private1").add_group(@group, instance_variable_get("@#{item.singularize}_private1").user)

      instance_variable_get("@#{item.singularize}_public2").add_group(@group, instance_variable_get("@#{item.singularize}_public1").user)
      instance_variable_get("@#{item.singularize}_internal2").add_group(@group, instance_variable_get("@#{item.singularize}_internal1").user)
      instance_variable_get("@#{item.singularize}_private2").add_group(@group, instance_variable_get("@#{item.singularize}_private1").user)
    end

    reindex_elasticsearch_data
  end

  scenario 'as admin' do
    sign_in @admin
    UserItems.item_list.each do |item|
      visit group_show_items_path(@group, item)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public1").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal1").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_private1").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public2").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal2").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_private2").title)
    end
  end

  scenario 'as group member' do
    sign_in @group_member
    UserItems.item_list.each do |item|
      visit group_show_items_path(@group, item)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public1").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal1").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_private1").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public2").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal2").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_private2").title)
    end
  end

  scenario 'as group owner' do
    sign_in @group_owner
    UserItems.item_list.each do |item|
      visit group_show_items_path(@group, item)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public1").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal1").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_private1").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public2").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal2").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_private2").title)
    end
  end

  scenario 'as user', js: true do
    sign_in @user1
    UserItems.item_list.each do |item|
      visit group_show_items_path(@group, item)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public1").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal1").title)
      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_private1").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public2").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal2").title)
      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_private2").title)

      item1 = instance_variable_get("@#{item.singularize}_private1").collaborate(@user1, @admin)
      item2 = instance_variable_get("@#{item.singularize}_private2").update(user_id: @user1.id)
      UserItems.class(item).import
      UserItems.class(item).__elasticsearch__.refresh_index!

      visit group_show_items_path(@group, item)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public1").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal1").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_private1").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public2").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal2").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_private2").title)
    end
  end
end
