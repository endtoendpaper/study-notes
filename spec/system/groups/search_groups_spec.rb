require 'rails_helper'

describe 'User searches groups', type: :system do
  before(:each) do
    reset_elasticsearch_indices

    @user = FactoryBot.create(:user, :confirmed)
    @group1 = FactoryBot.create(:group, user_id: @user.id, name: "lizard people 3")
    @group2 = FactoryBot.create(:group, name: "tin men 2")
    @group3 = FactoryBot.create(:group, name: "dance kids 5")
    @group2.add_member(@user, @group2.user)

    reindex_elasticsearch_data
  end

  scenario 'by group name' do
    sign_in @user
    visit groups_path
    expect(page).to have_link(@group1.name)
    expect(page).to have_link(@group2.name)
    expect(page).to have_link(@group3.name)
    fill_in('search', with: @group1.name)
    click_button 'Search'
    expect(page).to have_link(@group1.name)
    expect(page).to_not have_link(@group2.name)
    expect(page).to_not have_link(@group3.name)
    fill_in('search', with: 'birbs')
    click_button 'Search'
    expect(page).to_not have_link(@group1.name)
    expect(page).to_not have_link(@group2.name)
    expect(page).to_not have_link(@group3.name)
    click_link 'Clear'
    expect(page).to have_link(@group1.name)
    expect(page).to have_link(@group2.name)
    expect(page).to have_link(@group3.name)
  end

  scenario 'by toggling their groups', js: true do
    sign_in @user
    visit groups_path
    expect(page).to have_link(@group1.name)
    expect(page).to have_link(@group2.name)
    expect(page).to have_link(@group3.name)
    page.check('view_my_groups')
    expect(page).to have_link(@group1.name)
    expect(page).to have_link(@group2.name)
    expect(page).to_not have_link(@group3.name)
    page.uncheck('view_my_groups')
    expect(page).to have_link(@group1.name)
    expect(page).to have_link(@group2.name)
    expect(page).to have_link(@group3.name)
  end
end
