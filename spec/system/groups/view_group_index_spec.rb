require 'rails_helper'

describe 'User views group index', type: :system do
  let(:name) { Faker::Lorem.sentence(word_count: 6) }
  let(:description) { Faker::Lorem.sentence(word_count: 5) }

  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group1 = FactoryBot.create(:group, user_id: @owner.id)
    @group2 = FactoryBot.create(:group)
    @group3 = FactoryBot.create(:group)
    @group1.add_member(@group_member, @group1.user)
  end

  scenario 'admin' do
    sign_in @admin
    visit groups_path
    expect(page).to have_link(@group1.name)
    expect(page).to have_link(@group2.name)
    expect(page).to have_link(@group3.name)
    expect(page).to have_link('1 member', minimum: 2)
    expect(page).to have_link('2 members', minimum: 1)
    expect(page).to have_content(@group1.description)
    expect(page).to have_content(@group2.description)
    expect(page).to have_content(@group3.description)
    expect(page).to_not have_link('Edit members')
    expect(page).to_not have_link('Edit group')
  end

  scenario "as a group owner" do
    sign_in @owner
    visit groups_path
    expect(page).to have_link(@group1.name)
    expect(page).to have_link(@group2.name)
    expect(page).to have_link(@group3.name)
    expect(page).to have_link('1 member', minimum: 2)
    expect(page).to have_link('2 members', minimum: 1)
    expect(page).to have_content(@group1.description)
    expect(page).to have_content(@group2.description)
    expect(page).to have_content(@group3.description)
    expect(page).to_not have_link('Edit members')
    expect(page).to_not have_link('Edit group')
  end

  scenario 'as a group member' do
    sign_in @group_member
    visit groups_path
    expect(page).to have_link(@group1.name)
    expect(page).to have_link(@group2.name)
    expect(page).to have_link(@group3.name)
    expect(page).to have_link('1 member', minimum: 2)
    expect(page).to have_link('2 members', minimum: 1)
    expect(page).to have_content(@group1.description)
    expect(page).to have_content(@group2.description)
    expect(page).to have_content(@group3.description)
    expect(page).to_not have_link('Edit members')
    expect(page).to_not have_link('Edit group')
  end

  scenario 'as a user' do
    sign_in @user
    visit groups_path
    expect(page).to have_link(@group1.name)
    expect(page).to have_link(@group2.name)
    expect(page).to have_link(@group3.name)
    expect(page).to have_link('1 member', minimum: 2)
    expect(page).to have_link('2 members', minimum: 1)
    expect(page).to have_content(@group1.description)
    expect(page).to have_content(@group2.description)
    expect(page).to have_content(@group3.description)
    expect(page).to_not have_link('Edit members')
    expect(page).to_not have_link('Edit group')
  end

  scenario 'as a user with 6 groups and pagination' do
    sign_in @user
    3.times do
      FactoryBot.create(:group)
    end
    visit groups_path
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end
end
