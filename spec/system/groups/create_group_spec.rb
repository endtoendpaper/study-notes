require 'rails_helper'

describe 'User creates a group', type: :system do
  let(:name) { Faker::Team.name }
  let(:description) { Faker::Lorem.sentence(word_count: 25) }

  before(:each) do
    @user = FactoryBot.create(:user, :confirmed)
  end

  scenario 'with valid data' do
    sign_in @user
    visit new_group_path
    fill_in 'group_name', with: name
    fill_in 'group_description', with: description
    click_button 'Submit'
    expect(page).to have_content('Group was successfully created.')
    expect(page).to have_content(name)
    expect(page).to have_content(description)
  end

  scenario 'with invalid data', js: true do
    sign_in @user
    visit new_group_path
    fill_in 'group_description', with: description
    click_button 'Submit'
    expect(page).to have_content('1 error prohibited this group from being saved:')
    expect(page).to have_content("Name can't be blank")
    expect(current_path).to eq('/groups/new')
  end
end
