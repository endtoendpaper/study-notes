require 'rails_helper'

describe 'User group membership logs', type: :system do
  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @owner = FactoryBot.create(:user, :confirmed)
    @new_owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @group_member1 = FactoryBot.create(:user, :confirmed)
    @group_member2 = FactoryBot.create(:user, :confirmed)
    @group_member3 = FactoryBot.create(:user, :confirmed)
    @group_member4 = FactoryBot.create(:user, :confirmed)
    @group_member5 = FactoryBot.create(:user, :confirmed)
    @group_member6 = FactoryBot.create(:user, :confirmed)
    @group_member7 = FactoryBot.create(:user, :confirmed)
    @group_member8 = FactoryBot.create(:user, :confirmed)
    @group_member9 = FactoryBot.create(:user, :confirmed)
    @group_member10 = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group, user_id: @owner.id)
    @group.add_member(@group_member1, @group.user)
    @group.add_member(@group_member2, @group_member1)
    @group.add_member(@group_member3, @group_member2)
    @group.add_member(@group_member4, @group.user)
    @group.add_member(@group_member5, @group.user)
    @group.add_member(@group_member6, @group.user)
    @group.add_member(@group_member7, @group.user)
    @group.add_member(@group_member8, @group.user)
    @group.add_member(@group_member9, @group.user)
    @group.add_member(@group_member10, @group.user)
    @group.remove_member(@group_member2, @group_member2)
    @group.remove_member(@group_member3, @group_member1)
    @group_member2.destroy
    @group.change_owner(@new_owner, @group.user)
  end

  scenario 'admin' do
    sign_in @admin
    visit group_path(@group)
    click_link 'Membership Log'
    expect(page).to have_selector('h1', text: @group.name + ': Membership Log')
    expect(page).to have_content("#{@owner.handle} changed owner to #{@new_owner.handle} on")
    expect(page).to have_content("[Deleted User Account](#{@group_member2.handle}) removed member [Deleted User Account](#{@group_member2.handle}) on")
    expect(page).to have_content("#{@group_member1.handle} removed member #{@group_member3.handle} on")
    expect(page).to have_content("#{@owner.handle} added member #{@group_member10.handle} on")
    expect(page).to have_content("#{@owner.handle} added member #{@group_member9.handle} on")
    expect(page).to have_content("#{@owner.handle} added member #{@group_member8.handle} on")
    expect(page).to have_content("#{@owner.handle} added member #{@group_member7.handle} on")
    expect(page).to have_content("#{@owner.handle} added member #{@group_member6.handle} on")
    expect(page).to have_content("#{@owner.handle} added member #{@group_member5.handle} on")
    expect(page).to have_content("#{@owner.handle} added member #{@group_member4.handle} on")
    expect(page).to have_content("[Deleted User Account](#{@group_member2.handle}) added member #{@group_member3.handle} on")
    expect(page).to have_content("#{@group_member1.handle} added member [Deleted User Account](#{@group_member2.handle}) on")
    click_link '2'
    expect(page).to have_content("#{@owner.handle} added member #{@group_member1.handle} on")
    expect(page).to have_content("#{@owner.handle} changed owner to #{@owner.handle} on")
  end

  scenario "as a group owner" do
    sign_in @new_owner
    visit group_path(@group)
    click_link 'Membership Log'
    expect(page).to have_selector('h1', text: @group.name + ': Membership Log')
    expect(page).to have_content("#{@owner.handle} changed owner to #{@new_owner.handle} on")
    expect(page).to have_content("[Deleted User Account](#{@group_member2.handle}) removed member [Deleted User Account](#{@group_member2.handle}) on")
    expect(page).to have_content("#{@group_member1.handle} removed member #{@group_member3.handle} on")
    expect(page).to have_content("#{@owner.handle} added member #{@group_member10.handle} on")
    expect(page).to have_content("#{@owner.handle} added member #{@group_member9.handle} on")
    expect(page).to have_content("#{@owner.handle} added member #{@group_member8.handle} on")
    expect(page).to have_content("#{@owner.handle} added member #{@group_member7.handle} on")
    expect(page).to have_content("#{@owner.handle} added member #{@group_member6.handle} on")
    expect(page).to have_content("#{@owner.handle} added member #{@group_member5.handle} on")
    expect(page).to have_content("#{@owner.handle} added member #{@group_member4.handle} on")
    expect(page).to have_content("[Deleted User Account](#{@group_member2.handle}) added member #{@group_member3.handle} on")
    expect(page).to have_content("#{@group_member1.handle} added member [Deleted User Account](#{@group_member2.handle}) on")
    click_link '2'
    expect(page).to have_content("#{@owner.handle} added member #{@group_member1.handle} on")
    expect(page).to have_content("#{@owner.handle} changed owner to #{@owner.handle} on")
  end

  scenario 'as a group member' do
    sign_in @group_member1
    visit group_path(@group)
    click_link 'Membership Log'
    expect(page).to have_selector('h1', text: @group.name + ': Membership Log')
    expect(page).to have_content("#{@owner.handle} changed owner to #{@new_owner.handle} on")
    expect(page).to have_content("[Deleted User Account](#{@group_member2.handle}) removed member [Deleted User Account](#{@group_member2.handle}) on")
    expect(page).to have_content("#{@group_member1.handle} removed member #{@group_member3.handle} on")
    expect(page).to have_content("#{@owner.handle} added member #{@group_member10.handle} on")
    expect(page).to have_content("#{@owner.handle} added member #{@group_member9.handle} on")
    expect(page).to have_content("#{@owner.handle} added member #{@group_member8.handle} on")
    expect(page).to have_content("#{@owner.handle} added member #{@group_member7.handle} on")
    expect(page).to have_content("#{@owner.handle} added member #{@group_member6.handle} on")
    expect(page).to have_content("#{@owner.handle} added member #{@group_member5.handle} on")
    expect(page).to have_content("#{@owner.handle} added member #{@group_member4.handle} on")
    expect(page).to have_content("[Deleted User Account](#{@group_member2.handle}) added member #{@group_member3.handle} on")
    expect(page).to have_content("#{@group_member1.handle} added member [Deleted User Account](#{@group_member2.handle}) on")
    click_link '2'
    expect(page).to have_content("#{@owner.handle} added member #{@group_member1.handle} on")
    expect(page).to have_content("#{@owner.handle} changed owner to #{@owner.handle} on")
  end

  scenario 'as user' do
    sign_in @user
    visit group_path(@group)
    expect(page).to_not have_button('Membership Log')
  end
end
