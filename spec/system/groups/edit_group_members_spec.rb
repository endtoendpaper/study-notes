#add the owner as members too, make sure owner is only listed and counted once
require 'rails_helper'

describe 'User edits group members', type: :system do
  let(:name) { Faker::Lorem.sentence(word_count: 6) }
  let(:description) { Faker::Lorem.sentence(word_count: 5) }

  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group, user_id: @owner.id)
    @group.add_member(@group_member, @group.user)
    @new_owner = FactoryBot.create(:user, :confirmed)
    @new_group_member1 = FactoryBot.create(:user, :confirmed)
    @new_group_member2 = FactoryBot.create(:user, :confirmed)
  end

  scenario 'as owner on group show page', js: true do
    sign_in @owner
    visit group_path(@group)
    expect(page).to have_content('2 members')
    click_link 'Edit members'
    # add owner as member, count doesn't go up
    expect(page).to have_button(@group_member.handle)
    find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set(@new_group_member1.handle, wait: 10)
    click_button 'Add Member', wait: 10
    expect(page).to have_content('Member was successfully added.')
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set(@new_group_member2.handle, wait: 10)
    click_button 'Add Member', wait: 10
    expect(page).to have_content('Member was successfully added.')
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    expect(page).to have_button(@new_group_member2.handle)
    find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set(@owner.handle, wait: 10)
    click_button 'Add Member', wait: 10
    expect(page).to have_content('Member was successfully added.')
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    expect(page).to have_button(@new_group_member2.handle)
    expect(page).to have_button(@owner.handle)
    click_button @owner.handle, wait: 10
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    expect(page).to have_button(@new_group_member2.handle)
    expect(page).to_not have_button(@owner.handle)
    click_button @new_group_member1.handle, wait: 10
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member2.handle)
    expect(page).to_not have_button(@new_group_member1.handle)
    expect(page).to_not have_button(@owner.handle)
  end

  scenario 'as owner changing owner', js: true do
    sign_in @owner
    visit group_path(@group)
    expect(page).to have_content('2 members')
    click_link 'Edit members'
    #owner will lose access after changing
    find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[1]/div[2]/input[1]").set(@new_owner.handle, wait: 10)
    click_button 'Change Owner'
    page.accept_alert
    expect(page).to have_content('Owner was successfully updated.')
    expect(page).to_not have_content('Add member')
    expect(page).to_not have_content('Remove member')
  end

  scenario "as admin on group show page", js: true do
    sign_in @admin
    visit group_path(@group)
    expect(page).to have_content('2 members')
    click_link 'Edit members'
    # add owner as member, count doesn't go up
    expect(page).to have_button(@group_member.handle)
    find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set(@new_group_member1.handle, wait: 10)
    click_button 'Add Member', wait: 10
    expect(page).to have_content('Member was successfully added.')
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set(@new_group_member2.handle, wait: 10)
    click_button 'Add Member', wait: 10
    expect(page).to have_content('Member was successfully added.')
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    expect(page).to have_button(@new_group_member2.handle)
    find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set(@owner.handle, wait: 10)
    click_button 'Add Member', wait: 10
    expect(page).to have_content('Member was successfully added.')
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    expect(page).to have_button(@new_group_member2.handle)
    expect(page).to have_button(@owner.handle)
    click_button @owner.handle, wait: 10
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    expect(page).to have_button(@new_group_member2.handle)
    expect(page).to_not have_button(@owner.handle)
    click_button @new_group_member1.handle, wait: 10
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member2.handle)
    expect(page).to_not have_button(@new_group_member1.handle)
    expect(page).to_not have_button(@owner.handle)
  end

  scenario "as admin changing owner", js: true do
    sign_in @admin
    visit group_path(@group)
    expect(page).to have_content('2 members')
    click_link 'Edit members'
    #owner will lose access after changing
    find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[1]/div[2]/input[1]").set(@new_owner.handle, wait: 10)
    click_button 'Change Owner'
    page.accept_alert
    expect(page).to have_content('Owner was successfully updated.')
  end

  scenario 'as group member on group show page', js: true do
    sign_in @group_member
    visit group_path(@group)
    expect(page).to have_content('2 members')
    click_link 'Edit members'
    # add owner as member, count doesn't go up
    find('input.group-member', match: :first).set(@new_group_member1.handle, wait: 10)
    click_button 'Add Member', wait: 10
    expect(page).to have_content('Member was successfully added.')
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    find('input.group-member', match: :first).set(@new_group_member2.handle, wait: 10)
    click_button 'Add Member', wait: 10
    expect(page).to have_content('Member was successfully added.')
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    expect(page).to have_button(@new_group_member2.handle)
    find('input.group-member', match: :first).set(@owner.handle, wait: 10)
    click_button 'Add Member', wait: 10
    expect(page).to have_content('Member was successfully added.')
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    expect(page).to have_button(@new_group_member2.handle)
    expect(page).to have_button(@owner.handle)
    click_button @owner.handle, wait: 10
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    expect(page).to have_button(@new_group_member2.handle)
    expect(page).to_not have_button(@owner.handle)
    click_button @new_group_member1.handle, wait: 10
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member2.handle)
    expect(page).to_not have_button(@new_group_member1.handle)
    expect(page).to_not have_button(@owner.handle)
    expect(page).to_not have_button('Change Owner')
  end

  scenario 'as owner on group show_members page', js: true do
    sign_in @owner
    visit group_show_members_path(@group)
    expect(page).to have_content('2 members')
    click_link 'Edit members'
    # add owner as member, count doesn't go up
    expect(page).to have_button(@group_member.handle)
    find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set(@new_group_member1.handle, wait: 10)
    click_button 'Add Member', wait: 10
    expect(page).to have_content('Member was successfully added.')
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set(@new_group_member2.handle, wait: 10)
    click_button 'Add Member', wait: 10
    expect(page).to have_content('Member was successfully added.')
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    expect(page).to have_button(@new_group_member2.handle)
    find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set(@owner.handle, wait: 10)
    click_button 'Add Member', wait: 10
    expect(page).to have_content('Member was successfully added.')
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    expect(page).to have_button(@new_group_member2.handle)
    expect(page).to have_button(@owner.handle)
    click_button @owner.handle, wait: 10
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    expect(page).to have_button(@new_group_member2.handle)
    expect(page).to_not have_button(@owner.handle)
    click_button @new_group_member1.handle, wait: 10
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member2.handle)
    expect(page).to_not have_button(@new_group_member1.handle)
    expect(page).to_not have_button(@owner.handle)
  end

  scenario "as admin on group show_members page", js: true do
    sign_in @admin
    visit group_show_members_path(@group)
    expect(page).to have_content('2 members')
    click_link 'Edit members'
    # add owner as member, count doesn't go up
    expect(page).to have_button(@group_member.handle)
    find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set(@new_group_member1.handle, wait: 10)
    click_button 'Add Member', wait: 10
    expect(page).to have_content('Member was successfully added.')
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set(@new_group_member2.handle, wait: 10)
    click_button 'Add Member', wait: 10
    expect(page).to have_content('Member was successfully added.')
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    expect(page).to have_button(@new_group_member2.handle)
    find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set(@owner.handle, wait: 10)
    click_button 'Add Member', wait: 10
    expect(page).to have_content('Member was successfully added.')
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    expect(page).to have_button(@new_group_member2.handle)
    expect(page).to have_button(@owner.handle)
    click_button @owner.handle, wait: 10
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    expect(page).to have_button(@new_group_member2.handle)
    expect(page).to_not have_button(@owner.handle)
    click_button @new_group_member1.handle, wait: 10
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member2.handle)
    expect(page).to_not have_button(@new_group_member1.handle)
    expect(page).to_not have_button(@owner.handle)
  end

  scenario 'as group member on group_members show page', js: true do
    sign_in @group_member
    visit group_show_members_path(@group)
    expect(page).to have_content('2 members')
    click_link 'Edit members'
    # add owner as member, count doesn't go up
    expect(page).to have_button(@group_member.handle)
    find('input.group-member', match: :first).set(@new_group_member1.handle, wait: 10)
    click_button 'Add Member', wait: 10
    expect(page).to have_content('Member was successfully added.')
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    find('input.group-member', match: :first).set(@new_group_member2.handle, wait: 10)
    click_button 'Add Member', wait: 10
    expect(page).to have_content('Member was successfully added.')
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    expect(page).to have_button(@new_group_member2.handle)
    find('input.group-member', match: :first).set(@owner.handle, wait: 10)
    click_button 'Add Member', wait: 10
    expect(page).to have_content('Member was successfully added.')
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    expect(page).to have_button(@new_group_member2.handle)
    expect(page).to have_button(@owner.handle)
    click_button @owner.handle, wait: 10
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    expect(page).to have_button(@new_group_member2.handle)
    expect(page).to_not have_button(@owner.handle)
    click_button @new_group_member1.handle, wait: 10
    expect(page).to have_button(@group_member.handle)
    expect(page).to have_button(@new_group_member2.handle)
    expect(page).to_not have_button(@new_group_member1.handle)
    expect(page).to_not have_button(@owner.handle)
    expect(page).to_not have_button('Change Owner')
  end
end
