require 'rails_helper'

describe 'User views group', type: :system do
  let(:name) { Faker::Lorem.sentence(word_count: 6) }
  let(:description) { Faker::Lorem.sentence(word_count: 5) }

  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @group_member1 = FactoryBot.create(:user, :confirmed)
    @group_member2 = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group, user_id: @owner.id)
    @group.add_member(@group_member1, @group.user)
    @group.add_member(@group_member2, @group.user)
  end

  scenario 'admin' do
    sign_in @admin
    visit group_path(@group.reload)
    expect(page).to have_selector('h5', text: @group.name)
    expect(page).to have_link('3 members')
    expect(page).to have_content(@group.description)
    expect(page).to have_link('Edit members')
    expect(page).to have_link('Edit group')
  end

  scenario "as a group owner" do
    sign_in @owner
    visit group_path(@group.reload)
    expect(page).to have_selector('h5', text: @group.name)
    expect(page).to have_link('3 members')
    expect(page).to have_content(@group.description)
    expect(page).to have_link('Edit members')
    expect(page).to have_link('Edit group')
  end

  scenario 'as a group member' do
    sign_in @group_member1
    visit group_path(@group.reload)
    expect(page).to have_selector('h5', text: @group.name)
    expect(page).to have_link('3 members')
    expect(page).to have_content(@group.description)
    expect(page).to have_link('Edit members')
    expect(page).to have_link('Edit group')
  end

  scenario 'as a user' do
    sign_in @user
    visit group_path(@group.reload)
    expect(page).to have_selector('h5', text: @group.name)
    expect(page).to have_link('3 members')
    expect(page).to have_content(@group.description)
    expect(page).to_not have_link('Edit members')
    expect(page).to_not have_link('Edit group')
  end
end
