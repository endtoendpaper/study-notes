require 'rails_helper'

describe 'User views group members', type: :system do
  let(:name) { Faker::Lorem.sentence(word_count: 6) }
  let(:description) { Faker::Lorem.sentence(word_count: 5) }

  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @group_member1 = FactoryBot.create(:user, :confirmed)
    @group_member2 = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group, user_id: @owner.id)
    @group.add_member(@group_member1, @group.user)
    @group.add_member(@group_member2, @group.user)
  end

  scenario 'admin' do
    sign_in @admin
    visit group_show_members_path(@group)
    expect(page).to have_link(@group.name)
    expect(page).to have_link('3 members')
    expect(page).to have_link(@owner.handle)
    expect(page).to have_link(@group_member1.handle)
    expect(page).to have_link(@group_member2.handle)
    expect(page).to have_link('Edit members')
    expect(page).to have_link('Edit group')
  end

  scenario "as a group owner" do
    sign_in @owner
    visit group_show_members_path(@group)
    expect(page).to have_link(@group.name)
    expect(page).to have_link('3 members')
    expect(page).to have_link(@owner.handle)
    expect(page).to have_link(@group_member1.handle)
    expect(page).to have_link(@group_member2.handle)
    expect(page).to have_link('Edit members')
    expect(page).to have_link('Edit group')
  end

  scenario 'as a group member' do
    sign_in @group_member1
    visit group_show_members_path(@group)
    expect(page).to have_link(@group.name)
    expect(page).to have_link('3 members')
    expect(page).to have_link(@owner.handle)
    expect(page).to have_link(@group_member1.handle)
    expect(page).to have_link(@group_member2.handle)
    expect(page).to have_link('Edit members')
    expect(page).to have_link('Edit group')
  end

  scenario 'as a user' do
    sign_in @user
    visit group_show_members_path(@group)
    expect(page).to have_link(@group.name)
    expect(page).to have_link('3 members')
    expect(page).to have_link(@owner.handle)
    expect(page).to have_link(@group_member1.handle)
    expect(page).to have_link(@group_member2.handle)
    expect(page).to_not have_link('Edit members')
    expect(page).to_not have_link('Edit group')
  end

  scenario 'as a user with 6 group members and pagination' do
    sign_in @user
    4.times do
      @group.add_member(FactoryBot.create(:user, :confirmed), @group.user)
    end
    visit group_show_members_path(@group)
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end
end
