require 'rails_helper'

describe 'Edit item collaborations', type: :system do
  before(:each) do
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @group_member = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group)
    @new_owner = FactoryBot.create(:user, :confirmed)
    @new_collaborator1 = FactoryBot.create(:user, :confirmed)
    @new_collaborator2 = FactoryBot.create(:user, :confirmed)
    @new_group1 = FactoryBot.create(:group)
    @new_group2 = FactoryBot.create(:group)
    @group.add_member(@group_member, @group.user)

    UserItems.item_list.each do |item|
      item = instance_variable_set("@#{item.singularize}", FactoryBot.create(item.singularize, :private, user_id: @owner.id))
      item.collaborate(@collaborator, item.user)
      item.add_group(@group, item.user)
    end
  end

  scenario 'change owner as admin', js: true do
    sign_in @admin
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      click_link 'Edit collaborators'
      expect(page).to have_content(@owner.handle, minimum: 1)
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[1]/div[2]/input[1]").set(@new_owner.handle, wait: 10)
      click_button 'Change Owner'
      page.accept_alert
      expect(page).to have_content('Owner was successfully updated.')
      expect(page).to_not have_content(@owner.handle, minimum: 1)
      expect(page).to have_content(@new_owner.handle, minimum: 1)
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[1]/div[2]/input[1]").set('bad username', wait: 10)
      click_button 'Change Owner'
      page.accept_alert
      expect(page).to have_content('Username does not exist.')
      expect(page).to_not have_content(@owner.handle, minimum: 1)
      expect(page).to have_content(@new_owner.handle, minimum: 1)
      expect(page).to have_content(@collaborator.handle, minimum: 1)
    end
  end

  scenario 'add collaborators, remove collaborators as admin', js: true do
    sign_in @admin
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      click_link 'Edit collaborators'
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set('bad username', wait: 10)
      click_button 'Add Collaborator'
      expect(page).to have_content('Username does not exist.')
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set(@new_collaborator1.handle, wait: 10)
      click_button 'Add Collaborator'
      expect(page).to have_content('Collaborater was successfully added.')
      expect(page).to have_button(@new_collaborator1.handle)
      expect(page).to have_button(@collaborator.handle)
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set(@new_collaborator2.handle, wait: 10)
      click_button 'Add Collaborator'
      expect(page).to have_content('Collaborater was successfully added.')
      expect(page).to have_button(@new_collaborator1.handle)
      expect(page).to have_button(@collaborator.handle)
      expect(page).to have_button(@new_collaborator2.handle)
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set(@new_owner.handle, wait: 10)
      click_button 'Add Collaborator'
      expect(page).to have_content('Collaborater was successfully added.')
      expect(page).to have_button(@new_collaborator1.handle)
      expect(page).to have_button(@collaborator.handle)
      expect(page).to have_button(@new_collaborator1.handle)
      expect(page).to have_button(@new_owner.handle, minimum: 1)
      click_button @collaborator.handle
      expect(page).to_not have_button(@collaborator.handle)
      expect(page).to have_button(@new_collaborator1.handle)
      expect(page).to have_button(@new_collaborator2.handle)
      expect(page).to have_button(@new_owner.handle, minimum: 1)
      click_button @new_collaborator1.handle
      expect(page).to_not have_button(@new_collaborator1.handle)
      expect(page).to have_button(@new_collaborator2.handle)
      expect(page).to have_button(@new_collaborator2.handle)
      expect(page).to have_button(@new_owner.handle, minimum: 1)
      click_button @new_collaborator2.handle
      expect(page).to_not have_button(@new_collaborator2.handle)
      expect(page).to_not have_button(@new_collaborator1.handle)
      expect(page).to_not have_button(@new_collaborator1.handle)
      expect(page).to have_button(@new_owner.handle, minimum: 1)
      click_button @new_owner.handle
      expect(page).to_not have_button(@new_owner.handle)
    end
  end

  scenario 'add groups, remove groups as admin', js: true do
    sign_in @admin
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      click_link 'Edit collaborators'
      expect(page).to have_button(@group.name)
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[3]/div/input[1]").set(@new_group1.name, wait: 10)
      click_button 'Add Group'
      expect(page).to have_button(@group.name)
      expect(page).to have_button(@new_group1.name)
      expect(page).to have_content('Group was successfully added.')
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[3]/div/input[1]").set("Not a group", wait: 10)
      click_button 'Add Group'
      expect(page).to have_content('Group does not exist.')
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[3]/div/input[1]").set(@new_group2.name, wait: 10)
      click_button 'Add Group'
      expect(page).to have_button(@group.name)
      expect(page).to have_button(@new_group1.name)
      expect(page).to have_button(@new_group2.name)
      expect(page).to have_content('Group was successfully added.')
      click_button @new_group1.name
      expect(page).to_not have_button(@new_group1.name)
      expect(page).to have_button(@group.name)
      expect(page).to have_button(@new_group2.name)
      click_button @new_group2.name
      expect(page).to_not have_button(@new_group2.name)
      expect(page).to_not have_button(@new_group1.name)
      expect(page).to have_button(@group.name)
    end
  end

  scenario 'change owner as owner', js: true do
    sign_in @owner
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      click_link 'Edit collaborators'
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[1]/div[2]/input[1]").set('bad username', wait: 10)
      click_button 'Change Owner'
      page.accept_alert
      expect(page).to have_content('Username does not exist.')
      expect(page).to have_content(@collaborator.handle, minimum: 1)
    end
  end

   scenario 'add collaborators, remove collaborators as owner', js: true do
    sign_in @owner
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      click_link 'Edit collaborators'
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set('bad username', wait: 10)
      click_button 'Add Collaborator'
      expect(page).to have_content('Username does not exist.')
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set(@new_collaborator1.handle, wait: 10)
      click_button 'Add Collaborator'
      expect(page).to have_content('Collaborater was successfully added.')
      expect(page).to have_button(@new_collaborator1.handle)
      expect(page).to have_button(@collaborator.handle)
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set(@new_collaborator2.handle, wait: 10)
      click_button 'Add Collaborator'
      expect(page).to have_content('Collaborater was successfully added.')
      expect(page).to have_button(@collaborator.handle)
      expect(page).to have_button(@new_collaborator1.handle)
      expect(page).to have_button(@new_collaborator2.handle)
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set(@new_owner.handle, wait: 10)
      click_button 'Add Collaborator'
      expect(page).to have_content('Collaborater was successfully added.')
      expect(page).to have_button(@new_owner.handle)
      expect(page).to have_button(@collaborator.handle)
      expect(page).to have_button(@new_collaborator1.handle)
      expect(page).to have_button(@new_collaborator2.handle)
      click_button @collaborator.handle
      expect(page).to have_button(@new_owner.handle)
      expect(page).to have_button(@new_collaborator1.handle)
      expect(page).to have_button(@new_collaborator2.handle)
      expect(page).to_not have_button(@collaborator.handle)
      click_button @new_collaborator1.handle
      expect(page).to have_button(@new_owner.handle)
      expect(page).to have_button(@new_collaborator2.handle)
      expect(page).to_not have_button(@collaborator.handle)
      expect(page).to_not have_button(@new_collaborator1.handle)
      click_button @new_collaborator2.handle
      expect(page).to have_button(@new_owner.handle)
      expect(page).to_not have_button(@collaborator.handle)
      expect(page).to_not have_button(@new_collaborator1.handle)
      expect(page).to_not have_button(@new_collaborator2.handle)
      click_button @new_owner.handle
      expect(page).to_not have_button(@collaborator.handle)
      expect(page).to_not have_button(@new_collaborator1.handle)
      expect(page).to_not have_button(@new_collaborator2.handle)
      expect(page).to_not have_button(@new_owner.handle)
    end
   end

  scenario 'add groups, remove groups as owner', js: true do
    sign_in @owner
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      click_link 'Edit collaborators'
      expect(page).to have_button(@group.name)
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[3]/div/input[1]").set(@new_group1.name, wait: 10)
      click_button 'Add Group'
      expect(page).to have_button(@group.name)
      expect(page).to have_button(@new_group1.name)
      expect(page).to have_content('Group was successfully added.')
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[3]/div/input[1]").set("Not a group", wait: 10)
      click_button 'Add Group'
      expect(page).to have_content('Group does not exist.')
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[3]/div/input[1]").set(@new_group2.name, wait: 10)
      click_button 'Add Group'
      expect(page).to have_button(@group.name)
      expect(page).to have_button(@new_group1.name)
      expect(page).to have_button(@new_group2.name)
      expect(page).to have_content('Group was successfully added.')
      click_button @new_group1.name
      expect(page).to_not have_button(@new_group1.name)
      expect(page).to have_button(@group.name)
      expect(page).to have_button(@new_group2.name)
      click_button @new_group2.name
      expect(page).to_not have_button(@new_group2.name)
      expect(page).to_not have_button(@new_group1.name)
      expect(page).to have_button(@group.name)
      #owner will lose access after changing
      expect(page).to have_content(@owner.handle, minimum: 1)
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[1]/div[2]/input[1]").set(@new_owner.handle, wait: 10)
      click_button 'Change Owner'
      page.accept_alert
      expect(page).to have_content('Owner was successfully updated.')
      expect(page).to have_content(@new_owner.handle, minimum: 1)
      expect(page).to_not have_content(@owner.handle, minimum: 1)
      expect(page).to_not have_content('Add collaborator')
      expect(page).to_not have_content('Add group')
    end
  end

  scenario 'add collaborators, remove collaborators as collaborator', js: true do
    sign_in @collaborator
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      click_link 'Edit collaborators'
      expect(page).to have_content(@collaborator.handle, minimum: 1)
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[1]/div/input[1]").set('bad username')
      click_button 'Add Collaborator'
      expect(page).to have_content('Username does not exist.')
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[1]/div/input[1]").set(@new_collaborator1.handle, wait: 10)
      click_button 'Add Collaborator'
      expect(page).to have_content('Collaborater was successfully added.')
      expect(page).to have_button(@new_collaborator1.handle)
      expect(page).to have_button(@collaborator.handle)
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[1]/div/input[1]").set(@new_collaborator2.handle, wait: 10)
      click_button 'Add Collaborator'
      expect(page).to have_content('Collaborater was successfully added.')
      expect(page).to have_button(@new_collaborator1.handle)
      expect(page).to have_button(@collaborator.handle)
      expect(page).to have_button(@new_collaborator2.handle)
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[1]/div/input[1]").set(@new_owner.handle, wait: 10)
      click_button 'Add Collaborator'
      expect(page).to have_content('Collaborater was successfully added.')
      expect(page).to have_button(@new_collaborator1.handle)
      expect(page).to have_button(@collaborator.handle)
      expect(page).to have_button(@new_collaborator2.handle)
      expect(page).to have_button(@new_owner.handle)
      click_button @new_collaborator1.handle
      expect(page).to have_button(@collaborator.handle)
      expect(page).to have_button(@new_collaborator2.handle)
      expect(page).to have_button(@new_owner.handle)
      expect(page).to_not have_button(@new_collaborator1.handle)
      click_button @new_collaborator2.handle
      expect(page).to have_button(@collaborator.handle)
      expect(page).to have_button(@new_owner.handle)
      expect(page).to_not have_button(@new_collaborator1.handle)
      expect(page).to_not have_button(@new_collaborator2.handle)
      click_button @new_owner.handle
      expect(page).to have_button(@collaborator.handle)
      expect(page).to_not have_button(@new_collaborator1.handle)
      expect(page).to_not have_button(@new_collaborator2.handle)
      expect(page).to_not have_button(@new_owner.handle)
    end
  end

  scenario 'add groups, remove groups as collaborator', js: true do
    sign_in @collaborator
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      click_link 'Edit collaborators'
      expect(page).to have_button(@group.name)
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set(@new_group1.name, wait: 10)
      click_button 'Add Group'
      expect(page).to have_button(@group.name)
      expect(page).to have_button(@new_group1.name)
      expect(page).to have_content('Group was successfully added.')
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set("Not a group", wait: 10)
      click_button 'Add Group'
      expect(page).to have_content('Group does not exist.')
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set(@new_group2.name, wait: 10)
      click_button 'Add Group'
      expect(page).to have_button(@group.name)
      expect(page).to have_button(@new_group1.name)
      expect(page).to have_button(@new_group2.name)
      expect(page).to have_content('Group was successfully added.')
      click_button @new_group1.name
      expect(page).to_not have_button(@new_group1.name)
      expect(page).to have_button(@group.name)
      expect(page).to have_button(@new_group2.name)
      click_button @new_group2.name
      expect(page).to_not have_button(@new_group2.name)
      expect(page).to_not have_button(@new_group1.name)
      expect(page).to have_button(@group.name)
      #no owner form
      expect(page).to_not have_content('Change Owner')
    end
  end

  scenario 'add collaborators, remove collaborators as group member', js: true do
    sign_in @group_member
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      click_link 'Edit collaborators'
      expect(page).to have_content(@collaborator.handle, minimum: 1)
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[1]/div/input[1]").set('bad username')
      click_button 'Add Collaborator'
      expect(page).to have_content('Username does not exist.')
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[1]/div/input[1]").set(@new_collaborator1.handle, wait: 10)
      click_button 'Add Collaborator'
      expect(page).to have_content('Collaborater was successfully added.')
      expect(page).to have_button(@new_collaborator1.handle)
      expect(page).to have_button(@collaborator.handle)
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[1]/div/input[1]").set(@new_collaborator2.handle, wait: 10)
      click_button 'Add Collaborator'
      expect(page).to have_content('Collaborater was successfully added.')
      expect(page).to have_button(@new_collaborator1.handle)
      expect(page).to have_button(@collaborator.handle)
      expect(page).to have_button(@new_collaborator2.handle)
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[1]/div/input[1]").set(@new_owner.handle, wait: 10)
      click_button 'Add Collaborator'
      expect(page).to have_content('Collaborater was successfully added.')
      expect(page).to have_button(@new_collaborator1.handle)
      expect(page).to have_button(@collaborator.handle)
      expect(page).to have_button(@new_collaborator2.handle)
      expect(page).to have_button(@new_owner.handle)
      click_button @new_collaborator1.handle
      expect(page).to have_button(@collaborator.handle)
      expect(page).to have_button(@new_collaborator2.handle)
      expect(page).to have_button(@new_owner.handle)
      expect(page).to_not have_button(@new_collaborator1.handle)
      click_button @new_collaborator2.handle
      expect(page).to have_button(@collaborator.handle)
      expect(page).to have_button(@new_owner.handle)
      expect(page).to_not have_button(@new_collaborator1.handle)
      expect(page).to_not have_button(@new_collaborator2.handle)
      click_button @new_owner.handle
      expect(page).to have_button(@collaborator.handle)
      expect(page).to_not have_button(@new_collaborator1.handle)
      expect(page).to_not have_button(@new_collaborator2.handle)
      expect(page).to_not have_button(@new_owner.handle)
    end
  end

  scenario 'add groups, remove groups as group member', js: true do
    sign_in @group_member
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      click_link 'Edit collaborators'
      expect(page).to have_button(@group.name)
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set(@new_group1.name, wait: 10)
      click_button 'Add Group'
      expect(page).to have_button(@group.name)
      expect(page).to have_button(@new_group1.name)
      expect(page).to have_content('Group was successfully added.')
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set("Not a group", wait: 10)
      click_button 'Add Group'
      expect(page).to have_content('Group does not exist.')
      find(:xpath, "/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form[2]/div/input[1]").set(@new_group2.name, wait: 10)
      click_button 'Add Group'
      expect(page).to have_button(@group.name)
      expect(page).to have_button(@new_group1.name)
      expect(page).to have_button(@new_group2.name)
      expect(page).to have_content('Group was successfully added.')
      click_button @new_group1.name
      expect(page).to_not have_button(@new_group1.name)
      expect(page).to have_button(@group.name)
      expect(page).to have_button(@new_group2.name)
      click_button @new_group2.name
      expect(page).to_not have_button(@new_group2.name)
      expect(page).to_not have_button(@new_group1.name)
      expect(page).to have_button(@group.name)
      #no owner form
      expect(page).to_not have_content('Change Owner')
    end
  end
end
