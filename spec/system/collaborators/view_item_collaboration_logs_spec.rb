require 'rails_helper'

describe 'View item collaboration logs', type: :system do
  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @owner = FactoryBot.create(:user, :confirmed)
    @new_owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @group_owner = FactoryBot.create(:user, :confirmed)
    @group_member = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group, user_id: @group_owner.id)
    @group.add_member(@group_member, @group.user)
    @new_group1 = FactoryBot.create(:group)
    @new_group2 = FactoryBot.create(:group)
    @new_owner = FactoryBot.create(:user, :confirmed)
    @new_collaborator1 = FactoryBot.create(:user, :confirmed)
    @new_collaborator2 = FactoryBot.create(:user, :confirmed)
    @new_collaborator3 = FactoryBot.create(:user, :confirmed)

    UserItems.item_list.each do |item|
      item = instance_variable_set("@#{item.singularize}", FactoryBot.create(item.singularize, :public, user_id: @owner.id))
      item.add_group(@group, item.user)
      item.collaborate(@collaborator, item.user)
      item.collaborate(@new_collaborator1, item.user)
      item.collaborate(@new_collaborator2, @new_collaborator1)
      item.collaborate(@new_collaborator3, item.user)
      item.uncollaborate(@new_collaborator1, item.user)
      item.uncollaborate(@new_collaborator2, item.user)
      item.add_group(@new_group1, item.user)
      item.add_group(@new_group2, item.user)
      item.remove_group(@new_group1, @group_member)
      item.remove_group(@new_group2, item.user)
      item.change_owner(@new_owner, @owner)
    end

    @new_group2.destroy
    @new_collaborator1.destroy
  end

  scenario 'as admin' do
    sign_in @admin
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      click_link 'Collaboration Log'
      expect(page).to have_selector('h1', text: instance_variable_get("@#{item.singularize}").title + ': Collaboration Log')
      expect(page).to have_content("#{@owner.handle} changed owner to #{@new_owner.handle} on")
      expect(page).to have_content("#{@owner.handle} removed group [Deleted Group](#{@new_group2.name}) on")
      expect(page).to have_content("#{@group_member.handle} removed group #{@new_group1.name} on")
      expect(page).to have_content("#{@owner.handle} added group [Deleted Group](#{@new_group2.name}) on")
      expect(page).to have_content("#{@owner.handle} added group #{@new_group1.name} on")
      expect(page).to have_content("#{@owner.handle} removed collaborator #{@new_collaborator2.handle} on")
      expect(page).to have_content("#{@owner.handle} removed collaborator [Deleted User Account](#{@new_collaborator1.handle}) on")
      expect(page).to have_content("#{@owner.handle} added collaborator #{@new_collaborator3.handle} on")
      expect(page).to have_content("[Deleted User Account](#{@new_collaborator1.handle}) added collaborator #{@new_collaborator2.handle} on")
      expect(page).to have_content("#{@owner.handle} added collaborator [Deleted User Account](#{@new_collaborator1.handle}) on")
      expect(page).to have_content("#{@owner.handle} added group #{@group.name} on")
      expect(page).to have_content("#{@owner.handle} added collaborator #{@collaborator.handle} on")
      click_link '2'
      expect(page).to have_content("#{@owner.handle} changed owner to #{@owner.handle} on")
    end
  end

  scenario 'as owner' do
    sign_in @new_owner
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      click_link 'Collaboration Log'
      expect(page).to have_selector('h1', text: instance_variable_get("@#{item.singularize}").title + ': Collaboration Log')
      expect(page).to have_content("#{@owner.handle} changed owner to #{@new_owner.handle} on")
      expect(page).to have_content("#{@owner.handle} removed group [Deleted Group](#{@new_group2.name}) on")
      expect(page).to have_content("#{@group_member.handle} removed group #{@new_group1.name} on")
      expect(page).to have_content("#{@owner.handle} added group [Deleted Group](#{@new_group2.name}) on")
      expect(page).to have_content("#{@owner.handle} added group #{@new_group1.name} on")
      expect(page).to have_content("#{@owner.handle} removed collaborator #{@new_collaborator2.handle} on")
      expect(page).to have_content("#{@owner.handle} removed collaborator [Deleted User Account](#{@new_collaborator1.handle}) on")
      expect(page).to have_content("#{@owner.handle} added collaborator #{@new_collaborator3.handle} on")
      expect(page).to have_content("[Deleted User Account](#{@new_collaborator1.handle}) added collaborator #{@new_collaborator2.handle} on")
      expect(page).to have_content("#{@owner.handle} added collaborator [Deleted User Account](#{@new_collaborator1.handle}) on")
      expect(page).to have_content("#{@owner.handle} added group #{@group.name} on")
      expect(page).to have_content("#{@owner.handle} added collaborator #{@collaborator.handle} on")
      click_link '2'
      expect(page).to have_content("#{@owner.handle} changed owner to #{@owner.handle} on")
    end
  end

  scenario 'as collaborator' do
    sign_in @collaborator
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      click_link 'Collaboration Log'
      expect(page).to have_selector('h1', text: instance_variable_get("@#{item.singularize}").title + ': Collaboration Log')
      expect(page).to have_content("#{@owner.handle} changed owner to #{@new_owner.handle} on")
      expect(page).to have_content("#{@owner.handle} removed group [Deleted Group](#{@new_group2.name}) on")
      expect(page).to have_content("#{@group_member.handle} removed group #{@new_group1.name} on")
      expect(page).to have_content("#{@owner.handle} added group [Deleted Group](#{@new_group2.name}) on")
      expect(page).to have_content("#{@owner.handle} added group #{@new_group1.name} on")
      expect(page).to have_content("#{@owner.handle} removed collaborator #{@new_collaborator2.handle} on")
      expect(page).to have_content("#{@owner.handle} removed collaborator [Deleted User Account](#{@new_collaborator1.handle}) on")
      expect(page).to have_content("#{@owner.handle} added collaborator #{@new_collaborator3.handle} on")
      expect(page).to have_content("[Deleted User Account](#{@new_collaborator1.handle}) added collaborator #{@new_collaborator2.handle} on")
      expect(page).to have_content("#{@owner.handle} added collaborator [Deleted User Account](#{@new_collaborator1.handle}) on")
      expect(page).to have_content("#{@owner.handle} added group #{@group.name} on")
      expect(page).to have_content("#{@owner.handle} added collaborator #{@collaborator.handle} on")
      click_link '2'
      expect(page).to have_content("#{@owner.handle} changed owner to #{@owner.handle} on")
    end
  end

  scenario 'as group owner' do
    sign_in @group_owner
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      click_link 'Collaboration Log'
      expect(page).to have_selector('h1', text: instance_variable_get("@#{item.singularize}").title + ': Collaboration Log')
      expect(page).to have_content("#{@owner.handle} changed owner to #{@new_owner.handle} on")
      expect(page).to have_content("#{@owner.handle} removed group [Deleted Group](#{@new_group2.name}) on")
      expect(page).to have_content("#{@group_member.handle} removed group #{@new_group1.name} on")
      expect(page).to have_content("#{@owner.handle} added group [Deleted Group](#{@new_group2.name}) on")
      expect(page).to have_content("#{@owner.handle} added group #{@new_group1.name} on")
      expect(page).to have_content("#{@owner.handle} removed collaborator #{@new_collaborator2.handle} on")
      expect(page).to have_content("#{@owner.handle} removed collaborator [Deleted User Account](#{@new_collaborator1.handle}) on")
      expect(page).to have_content("#{@owner.handle} added collaborator #{@new_collaborator3.handle} on")
      expect(page).to have_content("[Deleted User Account](#{@new_collaborator1.handle}) added collaborator #{@new_collaborator2.handle} on")
      expect(page).to have_content("#{@owner.handle} added collaborator [Deleted User Account](#{@new_collaborator1.handle}) on")
      expect(page).to have_content("#{@owner.handle} added group #{@group.name} on")
      expect(page).to have_content("#{@owner.handle} added collaborator #{@collaborator.handle} on")
      click_link '2'
      expect(page).to have_content("#{@owner.handle} changed owner to #{@owner.handle} on")
    end
  end

  scenario 'as group member' do
    sign_in @group_member
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      click_link 'Collaboration Log'
      expect(page).to have_selector('h1', text: instance_variable_get("@#{item.singularize}").title + ': Collaboration Log')
      expect(page).to have_content("#{@owner.handle} changed owner to #{@new_owner.handle} on")
      expect(page).to have_content("#{@owner.handle} removed group [Deleted Group](#{@new_group2.name}) on")
      expect(page).to have_content("#{@group_member.handle} removed group #{@new_group1.name} on")
      expect(page).to have_content("#{@owner.handle} added group [Deleted Group](#{@new_group2.name}) on")
      expect(page).to have_content("#{@owner.handle} added group #{@new_group1.name} on")
      expect(page).to have_content("#{@owner.handle} removed collaborator #{@new_collaborator2.handle} on")
      expect(page).to have_content("#{@owner.handle} removed collaborator [Deleted User Account](#{@new_collaborator1.handle}) on")
      expect(page).to have_content("#{@owner.handle} added collaborator #{@new_collaborator3.handle} on")
      expect(page).to have_content("[Deleted User Account](#{@new_collaborator1.handle}) added collaborator #{@new_collaborator2.handle} on")
      expect(page).to have_content("#{@owner.handle} added collaborator [Deleted User Account](#{@new_collaborator1.handle}) on")
      expect(page).to have_content("#{@owner.handle} added group #{@group.name} on")
      expect(page).to have_content("#{@owner.handle} added collaborator #{@collaborator.handle} on")
      click_link '2'
      expect(page).to have_content("#{@owner.handle} changed owner to #{@owner.handle} on")
    end
  end

  scenario 'as user' do
    sign_in @user
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      expect(page).to_not have_button('Collaboration Log')
    end
  end

  scenario 'not logged in' do
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      expect(page).to_not have_button('Collaboration Log')
    end
  end
end
