require 'rails_helper'

describe 'View deck collaborators', type: :system do
  before(:each) do
    reset_elasticsearch_indices

    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @group_member = FactoryBot.create(:user, :confirmed)
    @collaborator1 = FactoryBot.create(:user, :confirmed)
    @collaborator2 = FactoryBot.create(:user, :confirmed)
    @collaborator3 = FactoryBot.create(:user, :confirmed)
    @group1 = FactoryBot.create(:group)
    @group2 = FactoryBot.create(:group)
    @group1.add_member(@group_member, @group1.user)

    UserItems.item_list.each do |item|
      item = instance_variable_set("@#{item.singularize}", FactoryBot.create(item.singularize, :public, user_id: @owner.id))
      item.collaborate(@collaborator1, item.user)
      item.collaborate(@collaborator2, item.user)
      item.collaborate(@collaborator3, item.user)
      item.collaborate(@owner, item.user)
      item.add_group(@group1, item.user)
      item.add_group(@group2, item.user)
    end

    reindex_elasticsearch_data
  end

  scenario 'As admin on deck show page' do
    #add the owner and admin as collaborators too, make sure owner is only listed once
    sign_in @admin
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      expect(page).to have_link(@owner.handle, minimum: 1)
      expect(page).to have_link(@collaborator1.handle, minimum: 1)
      expect(page).to have_link(@collaborator2.handle, minimum: 1)
      expect(page).to have_link(@collaborator3.handle, minimum: 1)
      expect(page).to have_link(@group1.name, minimum: 1)
      expect(page).to have_link(@group2.name, minimum: 1)
      expect(page).to have_link('Edit collaborators')
    end
  end

  scenario 'As owner on deck show page' do
    #add the owner and admin as collaborators too, make sure owner is only listed once
    sign_in @admin
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      expect(page).to have_link(@owner.handle, minimum: 1)
      expect(page).to have_link(@collaborator1.handle, minimum: 1)
      expect(page).to have_link(@collaborator2.handle, minimum: 1)
      expect(page).to have_link(@collaborator3.handle, minimum: 1)
      expect(page).to have_link(@group1.name, minimum: 1)
      expect(page).to have_link(@group2.name, minimum: 1)
      expect(page).to have_link('Edit collaborators')
    end
  end

  scenario 'As collaborator on deck show page' do
    #add the owner and admin as collaborators too, make sure owner is only listed once
    sign_in @collaborator1
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      expect(page).to have_link(@owner.handle, minimum: 1)
      expect(page).to have_link(@collaborator1.handle, minimum: 1)
      expect(page).to have_link(@collaborator2.handle, minimum: 1)
      expect(page).to have_link(@collaborator3.handle, minimum: 1)
      expect(page).to have_link(@group1.name, minimum: 1)
      expect(page).to have_link(@group2.name, minimum: 1)
      expect(page).to have_link('Edit collaborators')
    end
  end

  scenario 'As group member on deck show page' do
    #add the owner and admin as collaborators too, make sure owner is only listed once
    sign_in @group_member
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      expect(page).to have_link(@owner.handle, minimum: 1)
      expect(page).to have_link(@collaborator1.handle, minimum: 1)
      expect(page).to have_link(@collaborator2.handle, minimum: 1)
      expect(page).to have_link(@collaborator3.handle, minimum: 1)
      expect(page).to have_link(@group1.name, minimum: 1)
      expect(page).to have_link(@group2.name, minimum: 1)
      expect(page).to have_link('Edit collaborators')
    end
  end

  scenario 'As user on home' do
    #add the owner and admin as collaborators too, make sure owner is only listed once
    sign_in @user
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      expect(page).to have_link(@owner.handle, minimum: 1)
      expect(page).to have_link(@collaborator1.handle, minimum: 1)
      expect(page).to have_link(@collaborator2.handle, minimum: 1)
      expect(page).to have_link(@collaborator3.handle, minimum: 1)
      expect(page).to have_link(@group1.name, minimum: 1)
      expect(page).to have_link(@group2.name, minimum: 1)
      expect(page).to_not have_link('Edit collaborators')
    end
  end

  scenario 'Not logged in on show page' do
    #add the owner and admin as collaborators too, make sure owner is only listed once
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      expect(page).to have_link(@owner.handle, minimum: 1)
      expect(page).to_not have_link(@collaborator1.handle, minimum: 1)
      expect(page).to_not have_link(@collaborator2.handle, minimum: 1)
      expect(page).to_not have_link(@collaborator3.handle, minimum: 1)
      expect(page).to_not have_link(@group1.name, minimum: 1)
      expect(page).to_not have_link(@group2.name, minimum: 1)
      expect(page).to_not have_link('Edit collaborators')
    end
  end

  scenario 'As admin on home page' do
    #add the owner and admin as collaborators too, make sure owner is only listed once
    sign_in @admin
    visit root_path
    expect(page).to have_link(@owner.handle, minimum: 1)
    expect(page).to_not have_link(@collaborator1.handle, minimum: 1)
    expect(page).to_not have_link(@collaborator2.handle, minimum: 1)
    expect(page).to_not have_link(@collaborator3.handle, minimum: 1)
    expect(page).to_not have_link(@group1.name, minimum: 1)
    expect(page).to_not have_link(@group2.name, minimum: 1)
    expect(page).to_not have_link('Edit collaborators')
  end

  scenario 'As owner on home page' do
    #add the owner and admin as collaborators too, make sure owner is only listed once
    sign_in @owner
    visit root_path
    expect(page).to have_link(@owner.handle, minimum: 1)
    expect(page).to_not have_link(@collaborator1.handle, minimum: 1)
    expect(page).to_not have_link(@collaborator2.handle, minimum: 1)
    expect(page).to_not have_link(@collaborator3.handle, minimum: 1)
    expect(page).to_not have_link(@group1.name, minimum: 1)
    expect(page).to_not have_link(@group2.name, minimum: 1)
    expect(page).to_not have_link('Edit collaborators')
  end

  scenario 'As collaborator on home page' do
    #add the owner and admin as collaborators too, make sure owner is only listed once
    sign_in @collaborator1
    visit root_path
    expect(page).to have_link(@owner.handle, minimum: 1)
    expect(page).to_not have_link(@collaborator1.handle, minimum: 1)
    expect(page).to_not have_link(@collaborator2.handle, minimum: 1)
    expect(page).to_not have_link(@collaborator3.handle, minimum: 1)
    expect(page).to_not have_link(@group1.name, minimum: 1)
    expect(page).to_not have_link(@group2.name, minimum: 1)
    expect(page).to_not have_link('Edit collaborators')
  end

  scenario 'As group member on home page' do
    #add the owner and admin as collaborators too, make sure owner is only listed once
    sign_in @group_member
    visit root_path
    expect(page).to have_link(@owner.handle, minimum: 1)
    expect(page).to_not have_link(@collaborator1.handle, minimum: 1)
    expect(page).to_not have_link(@collaborator2.handle, minimum: 1)
    expect(page).to_not have_link(@collaborator3.handle, minimum: 1)
    expect(page).to_not have_link(@group1.name, minimum: 1)
    expect(page).to_not have_link(@group2.name, minimum: 1)
    expect(page).to_not have_link('Edit collaborators')
  end

  scenario 'As user on home' do
    #add the owner and admin as collaborators too, make sure owner is only listed once
    sign_in @user
    visit root_path
    expect(page).to have_link(@owner.handle, minimum: 1)
    expect(page).to_not have_link(@collaborator1.handle, minimum: 1)
    expect(page).to_not have_link(@collaborator2.handle, minimum: 1)
    expect(page).to_not have_link(@collaborator3.handle, minimum: 1)
    expect(page).to_not have_link(@group1.name, minimum: 1)
    expect(page).to_not have_link(@group2.name, minimum: 1)
    expect(page).to_not have_link('Edit collaborators')
  end

  scenario 'Not logged in on home page' do
    #add the owner and admin as collaborators too, make sure owner is only listed once
    visit root_path
    expect(page).to have_link(@owner.handle, minimum: 1)
    expect(page).to_not have_link(@collaborator1.handle, minimum: 1)
    expect(page).to_not have_link(@collaborator2.handle, minimum: 1)
    expect(page).to_not have_link(@collaborator3.handle, minimum: 1)
    expect(page).to_not have_link(@group1.name, minimum: 1)
    expect(page).to_not have_link(@group2.name, minimum: 1)
    expect(page).to_not have_link('Edit collaborators')
  end
end
