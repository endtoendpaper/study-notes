require 'rails_helper'

describe 'Deletes event', type: :system do
  before(:each) do
    reset_elasticsearch_indices

    @owner = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)

    @calendar_private = FactoryBot.create(:calendar, :private, user_id: @owner.id)

    @date = DateTime.current - 1.month
    @date = DateTime.new(@date.year, @date.month, 15, 7, 10, 0, '+0')

    @event = @calendar_private.events.create(
        summary: Faker::Lorem.sentence(word_count: 2, supplemental: true, random_words_to_add: 5),
        start: @date, end: @date + 30.minutes,
        location: Faker::Address.full_address,
        description: Faker::Lorem.paragraph(sentence_count: 1, supplemental: false, random_sentences_to_add: 5)
      )

    reindex_elasticsearch_data
  end

  scenario 'from monthly show as admin', js: true do
    sign_in @admin
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@event.summary)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link @event.summary, match: :first
    click_link 'Edit event'
    click_button 'Delete Event'
    page.accept_alert
    expect(page).to have_content('Event was successfully deleted.')
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    expect(page).to_not have_content(@event.summary)
  end

  scenario 'from monthly show as owner', js: true do
    sign_in @owner
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@event.summary)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link @event.summary, match: :first
    click_link 'Edit event'
    click_button 'Delete Event'
    page.accept_alert
    expect(page).to have_content('Event was successfully deleted.')
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    expect(page).to_not have_content(@event.summary)
  end

  scenario 'from by_day show as admin', js: true do
    sign_in @admin
    date = @date.in_time_zone(@admin.time_zone)
    visit calendar_by_day_path(@calendar_private.id, year: date.year, month: date.month, day: date.day)
    expect(page).to have_content(@event.summary)
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    click_link 'Edit event'
    click_button 'Delete Event'
    page.accept_alert
    expect(page).to have_content('Event was successfully deleted.')
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    expect(page).to_not have_content(@event.summary)
  end

  scenario 'from by_day show as owner', js: true do
    sign_in @owner
    date = @date.in_time_zone(@owner.time_zone)
    visit calendar_by_day_path(@calendar_private.id, year: date.year, month: date.month, day: date.day)
    expect(page).to have_content(@event.summary)
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    click_link 'Edit event'
    click_button 'Delete Event'
    page.accept_alert
    expect(page).to have_content('Event was successfully deleted.')
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    expect(page).to_not have_content(@event.summary)
  end

  scenario 'from monthly all my events show as owner', js: true do
    sign_in @owner
    date = @date.in_time_zone(@owner.time_zone)
    visit user_calendar_path(year: date.year, month: date.month, day: date.day)
    expect(page).to have_content(@event.summary)
    expect(page).to have_content("All Calendar Events")
    expect(page).to have_content(date.strftime("%B") + " " + date.strftime("%Y"))
    click_link @event.summary, match: :first
    click_link 'Edit event'
    click_button 'Delete Event'
    page.accept_alert
    expect(page).to have_content('Event was successfully deleted.')
    expect(page).to have_content("All Calendar Events")
    expect(page).to have_content(date.strftime("%B") + " " + date.strftime("%Y"))
    expect(page).to_not have_content(@event.summary)
  end

  scenario 'from by_day all_my_events show as owner', js: true do
    sign_in @owner
    date = @date.in_time_zone(@owner.time_zone)
    visit user_calendar_by_day_path(@calendar_private.id, year: date.year, month: date.month, day: date.day)
    expect(page).to have_content(@event.summary)
    expect(page).to have_content("All Calendar Events")
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    click_link 'Edit event'
    click_button 'Delete Event'
    page.accept_alert
    expect(page).to have_content('Event was successfully deleted.')
    expect(page).to have_content("All Calendar Events")
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    expect(page).to_not have_content(@event.summary)
  end
end
