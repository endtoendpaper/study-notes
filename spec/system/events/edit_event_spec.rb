require 'rails_helper'

describe 'Edits event', type: :system do
  let(:summary) { Faker::Lorem.sentence(word_count: 6) }
  let(:description) { Faker::Lorem.paragraph(sentence_count: 1, supplemental: false, random_sentences_to_add: 5) }
  let(:location) { Faker::Address.full_address }

  before(:each) do
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group_owner = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group, user_id: @group_owner.id)
    @group.add_member(@group_member, @group.user)

    @calendar_private = FactoryBot.create(:calendar, :private, user_id: @owner.id)
    @calendar_private.collaborate(@collaborator, @calendar_private.user)
    @calendar_private.add_group(@group, @calendar_private.user)

    @date = DateTime.current - 2.months
    @date = DateTime.new(@date.year, @date.month, 15, 7, 10, 0, '+0')

    @event = @calendar_private.events.create(
        summary: "Event-#{Time.current.to_i}",
        start: @date,
        end: @date + 30.minutes,
        location: Faker::Address.full_address,
        description: Faker::Lorem.paragraph(sentence_count: 1, supplemental: false, random_sentences_to_add: 5)
      )
  end

  scenario 'with valid data as admin', js: true do
    sign_in @admin
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link @event.summary, match: :first
    click_link 'Edit event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link 'Show event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link 'Edit event'
    fill_in 'event_summary', with: "My new summary!"
    click_button 'Submit'
    using_wait_time 5 do
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
      expect(page).to have_content("Event was successfully updated.")
      expect(page).to_not have_content("(all day)")
      expect(page).to have_content("My new summary!", minimum: 2)
    end
  end

  scenario 'with valid data as owner', js: true do
    sign_in @owner
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link @event.summary, match: :first
    click_link 'Edit event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link 'Show event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link 'Edit event'
    fill_in 'event_summary', with: "My new summary!"
    click_button 'Submit'
    using_wait_time 5 do
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
      expect(page).to have_content("Event was successfully updated.")
      expect(page).to_not have_content("(all day)")
      expect(page).to have_content("My new summary!", minimum: 2)
    end
  end

  scenario 'with valid data as collaborator', js: true do
    sign_in @collaborator
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link @event.summary, match: :first
    click_link 'Edit event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link 'Show event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link 'Edit event'
    fill_in 'event_summary', with: "My new summary!"
    click_button 'Submit'
    using_wait_time 5 do
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
      expect(page).to have_content("Event was successfully updated.")
      expect(page).to_not have_content("(all day)")
      expect(page).to have_content("My new summary!", minimum: 2)
    end
  end

  scenario 'with valid data as group member', js: true do
    sign_in @group_member
    date = @date.in_time_zone(@group_member.time_zone)
    visit calendar_by_day_path(@calendar_private.id, year: date.year, month: date.month, day: date.day)
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    click_link 'Edit event'
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    fill_in 'event_summary', with: "My new summary!"
    click_button 'Submit'
    using_wait_time 5 do
      expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
      expect(page).to have_content("Event was successfully updated.")
      expect(page).to_not have_content("(all day)")
      expect(page).to have_content("My new summary!", minimum: 2)
    end
  end

  scenario 'with valid data as group owner', js: true do
    sign_in @group_owner
    date = @date.in_time_zone(@group_owner.time_zone)
    visit calendar_by_day_path(@calendar_private.id, year: date.year, month: date.month, day: date.day)
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    click_link 'Edit event'
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    fill_in 'event_summary', with: "My new summary!"
    click_button 'Submit'
    using_wait_time 5 do
      expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
      expect(page).to have_content("Event was successfully updated.")
      expect(page).to_not have_content("(all day)")
      expect(page).to have_content("My new summary!", minimum: 2)
    end
  end

  scenario 'all day as admin', js: true do
    sign_in @admin
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link @event.summary, match: :first
    click_link 'Edit event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    page.check('event_all_day')
    click_button 'Submit'
    using_wait_time 5 do
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
      expect(page).to have_content("Event was successfully updated.")
      expect(page).to have_content("(all day)")
    end
  end

  scenario 'all day as owner', js: true do
    sign_in @owner
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link @event.summary, match: :first
    click_link 'Edit event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    page.check('event_all_day')
    click_button 'Submit'
    using_wait_time 5 do
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
      expect(page).to have_content("Event was successfully updated.")
      expect(page).to have_content("(all day)")
    end
  end

  scenario 'all day as collaborator', js: true do
    sign_in @collaborator
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link @event.summary, match: :first
    click_link 'Edit event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    page.check('event_all_day')
    click_button 'Submit'
    using_wait_time 5 do
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
      expect(page).to have_content("Event was successfully updated.")
      expect(page).to have_content("(all day)")
    end
  end

  scenario 'all day as group member', js: true do
    sign_in @group_member
    date = @date.in_time_zone(@group_member.time_zone)
    visit calendar_by_day_path(@calendar_private.id, year: date.year, month: date.month, day: date.day)
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    expect(page).to_not have_content("(all day)")
    click_link 'Edit event'
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    page.check('event_all_day')
    click_button 'Submit'
    using_wait_time 5 do
      expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
      expect(page).to have_content("Event was successfully updated.")
      expect(page).to have_content("(all day)")
    end
  end

  scenario 'all day as group owner', js: true do
    sign_in @group_owner
    date = @date.in_time_zone(@group_owner.time_zone)
    visit calendar_by_day_path(@calendar_private.id, year: date.year, month: date.month, day: date.day)
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    expect(page).to_not have_content("(all day)")
    click_link 'Edit event'
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    page.check('event_all_day')
    click_button 'Submit'
    using_wait_time 5 do
      expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
      expect(page).to have_content("Event was successfully updated.")
      expect(page).to have_content("(all day)")
    end
  end

  scenario 'with invalid data as admin', js: true do
    sign_in @admin
    date = @date.in_time_zone(@admin.time_zone)
    visit calendar_by_day_path(@calendar_private.id, year: date.year, month: date.month, day: date.day)
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    expect(page).to_not have_content("(all day)")
    click_link 'Edit event'
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    fill_in 'event_summary', with: ''
    click_button 'Submit'
    using_wait_time 5 do
      expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
      expect(page).to have_content("1 error prohibited this event from being saved:")
      expect(page).to have_content("Summary can't be blank")
    end
  end

  scenario 'with invalid data as owner', js: true do
    sign_in @owner
    date = @date
    visit calendar_by_day_path(@calendar_private.id, year: date.year, month: date.month, day: date.day)
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    expect(page).to_not have_content("(all day)")
    click_link 'Edit event'
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    fill_in 'event_summary', with: ''
    click_button 'Submit'
    using_wait_time 5 do
      expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
      expect(page).to have_content("1 error prohibited this event from being saved:")
      expect(page).to have_content("Summary can't be blank")
    end
  end

  scenario 'with invalid data as collaborator', js: true do
    sign_in @collaborator
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link @event.summary, match: :first
    click_link 'Edit event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    fill_in 'event_summary', with: ''
    click_button 'Submit'
    using_wait_time 5 do
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
      expect(page).to have_content("1 error prohibited this event from being saved:")
      expect(page).to have_content("Summary can't be blank")
    end
  end

  scenario 'with invalid data as group member', js: true do
    sign_in @group_member
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link @event.summary, match: :first
    click_link 'Edit event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    fill_in 'event_summary', with: ''
    click_button 'Submit'
    using_wait_time 5 do
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
      expect(page).to have_content("1 error prohibited this event from being saved:")
      expect(page).to have_content("Summary can't be blank")
    end
  end

  scenario 'with invalid data as group owner', js: true do
    sign_in @group_owner
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link @event.summary, match: :first
    click_link 'Edit event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    fill_in 'event_summary', with: ''
    click_button 'Submit'
    using_wait_time 5 do
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
      expect(page).to have_content("1 error prohibited this event from being saved:")
      expect(page).to have_content("Summary can't be blank")
    end
  end

  scenario 'with invalid data as owner', js: true do
    sign_in @owner
    visit user_calendar_path(year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    expect(page).to_not have_content("(all day)")
    expect(page).to have_content('All Calendar Events')
    click_link @event.summary, match: :first
    click_link 'Edit event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    fill_in 'event_summary', with: ''
    click_button 'Submit'
    using_wait_time 5 do
      expect(page).to have_content('All Calendar Events')
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
      expect(page).to have_content("1 error prohibited this event from being saved:")
      expect(page).to have_content("Summary can't be blank")
    end
  end

  scenario 'with invalid data as collaborator', js: true do
    sign_in @collaborator
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    visit user_calendar_path(year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    expect(page).to have_content('All Calendar Events')
    click_link @event.summary, match: :first
    click_link 'Edit event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    fill_in 'event_summary', with: ''
    click_button 'Submit'
    using_wait_time 5 do
      expect(page).to have_content('All Calendar Events')
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
      expect(page).to have_content("1 error prohibited this event from being saved:")
      expect(page).to have_content("Summary can't be blank")
    end
  end
end
