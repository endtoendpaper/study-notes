require 'rails_helper'

describe 'User creates a card', type: :system do
  let(:front) { Faker::Lorem.sentence(word_count: 15) }
  let(:back) { Faker::Lorem.sentence(word_count: 50) }

  before(:each) do
    @user = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :admin, :confirmed)
    @deck_public1 = FactoryBot.create(:deck, :public, user_id: @user.id)
  end

  scenario 'with valid data as user', js: true do
    sign_in @user
    visit deck_path(@deck_public1.id)
    click_link 'Add card'
    fill_in_trix_editor 'card_front_trix_input_card', with: front
    fill_in_trix_editor 'card_back_trix_input_card', with: back
    click_button 'Submit'
    expect(page).to have_content('Card was successfully created.')
    expect(current_path).to include('/decks/' + @deck_public1.id.to_s + '/' + 'cards/')
    expect(page).to have_content(front)
    expect(page).to_not have_content(back)
  end

  scenario 'with invalid data as user', js: true do
    sign_in @user
    visit deck_path(@deck_public1.id)
    click_link 'Add card'
    fill_in_trix_editor 'card_front_trix_input_card', with: front
    click_button 'Submit'
    expect(current_path).to include('/decks/' + @deck_public1.id.to_s + '/' + 'cards/')
    expect(page).to have_content('1 error prohibited this card from being saved:')
    expect(page).to have_content("Back can't be blank")
    expect(current_path).to eq('/decks/' + @deck_public1.id.to_s + '/cards/new')
  end

  scenario 'with valid data as admin', js: true do
    sign_in @admin
    visit deck_path(@deck_public1.id)
    click_link 'Add card'
    fill_in_trix_editor 'card_front_trix_input_card', with: front
    fill_in_trix_editor 'card_back_trix_input_card', with: back
    click_button 'Submit'
    expect(page).to have_content('Card was successfully created.')
    expect(current_path).to include('/decks/' + @deck_public1.id.to_s + '/' + 'cards/')
    expect(page).to have_content(front)
    expect(page).to_not have_content(back)
  end

  scenario 'with invalid data as admin', js: true do
    sign_in @admin
    visit deck_path(@deck_public1.id)
    click_link 'Add card'
    fill_in_trix_editor 'card_front_trix_input_card', with: front
    click_button 'Submit'
    expect(current_path).to include('/decks/' + @deck_public1.id.to_s + '/' + 'cards/')
    expect(page).to have_content('1 error prohibited this card from being saved:')
    expect(page).to have_content("Back can't be blank")
    expect(current_path).to eq('/decks/' + @deck_public1.id.to_s + '/cards/new')
  end
end
