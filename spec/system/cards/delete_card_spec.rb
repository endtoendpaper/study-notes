require 'rails_helper'

describe 'Deletes card', type: :system do
  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @user = FactoryBot.create(:user, :confirmed)
    @deck_public1 = FactoryBot.create(:deck, :public, user_id: @user.id)
    @card = FactoryBot.create(:card, deck_id: @deck_public1.id)
    @group_member = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group)
    @deck_public1.collaborate(@collaborator, @deck_public1.user)
    @deck_public1.add_group(@group, @deck_public1.user)
    @group.add_member(@group_member, @group.user)
  end

  scenario 'as user deleting own card', js: true do
    sign_in @user
    visit deck_card_path(@deck_public1.id, @card.id)
    click_link 'Edit card'
    click_button 'Delete Card'
    page.accept_alert
    expect(current_path).to include('/decks/' + @deck_public1.id.to_s + '/cards')
    expect(page).to have_content('Card was successfully deleted.')
    visit deck_cards_path(@deck_public1.id)
    expect(page).to_not have_content(@card.front)
  end

  scenario "as admin deleting user's card", js: true do
    sign_in @admin
    visit deck_card_path(@deck_public1.id, @card.id)
    click_link 'Edit card'
    click_button 'Delete Card'
    page.accept_alert
    expect(current_path).to include('/decks/' + @deck_public1.id.to_s + '/cards')
    expect(page).to have_content('Card was successfully deleted.')
    visit deck_cards_path(@deck_public1.id)
    expect(page).to_not have_content(@card.front)
  end

  scenario "as collaborator deleting user's card", js: true do
    sign_in @collaborator
    visit deck_card_path(@deck_public1.id, @card.id)
    click_link 'Edit card'
    expect(page).to_not have_button('Delete Card')
  end

  scenario "as group member deleting user's card", js: true do
    sign_in @group_member
    visit deck_card_path(@deck_public1.id, @card.id)
    click_link 'Edit card'
    expect(page).to_not have_button('Delete Card')
  end
end
