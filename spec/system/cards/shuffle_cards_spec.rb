require 'rails_helper'

describe 'Shuffle cards', type: :system do
  before(:each) do
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @deck_public = FactoryBot.create(:deck, :public, user_id: @owner.id)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group_owner = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group, user_id: @group_owner.id)
    @deck_public.collaborate(@collaborator, @deck_public.user)
    @deck_public.add_group(@group, @deck_public.user)
    @group.add_member(@group_member, @group.user)

    14.times do |i|
      instance_variable_set("@card#{i}", FactoryBot.create(:card, front: "question#{i}.", back: "answer#{i}.",
        deck_id: @deck_public.id))
    end
  end

  scenario 'as admin' do
    sign_in @admin
    visit deck_cards_path(@deck_public.id)
    expect(page.body).to include(@card1.front.to_plain_text)
    expect(page).to have_link('Shuffle')
    click_link '2'
    click_link 'Shuffle'
    expect(page).to have_content('Deck was successfully shuffled.')
    expect(current_path).to include('/decks/' + @deck_public.id.to_s + '/cards')
  end

  scenario 'as deck owner' do
    sign_in @owner
    visit deck_cards_path(@deck_public.id)
    expect(page.body).to include(@card1.front.to_plain_text)
    expect(page).to have_link('Shuffle')
    click_link '2'
    click_link 'Shuffle'
    expect(page).to have_content('Deck was successfully shuffled.')
    expect(current_path).to include('/decks/' + @deck_public.id.to_s + '/cards')
  end

  scenario 'as deck collaborator' do
    sign_in @collaborator
    visit deck_cards_path(@deck_public.id)
    expect(page.body).to include(@card1.front.to_plain_text)
    expect(page).to have_link('Shuffle')
    click_link '2'
    click_link 'Shuffle'
    expect(page).to have_content('Deck was successfully shuffled.')
    expect(current_path).to include('/decks/' + @deck_public.id.to_s + '/cards')
  end

  scenario 'as group member' do
    sign_in @group_member
    visit deck_cards_path(@deck_public.id)
    expect(page.body).to include(@card1.front.to_plain_text)
    expect(page).to have_link('Shuffle')
    click_link '2'
    click_link 'Shuffle'
    expect(page).to have_content('Deck was successfully shuffled.')
    expect(current_path).to include('/decks/' + @deck_public.id.to_s + '/cards')
  end

  scenario 'as group owner' do
    sign_in @group_owner
    visit deck_cards_path(@deck_public.id)
    expect(page.body).to include(@card1.front.to_plain_text)
    expect(page).to have_link('Shuffle')
    click_link '2'
    click_link 'Shuffle'
    expect(page).to have_content('Deck was successfully shuffled.')
    expect(current_path).to include('/decks/' + @deck_public.id.to_s + '/cards')
  end

  scenario 'as user' do
    sign_in @user
    visit deck_cards_path(@deck_public.id)
    expect(page.body).to include(@card1.front.to_plain_text)
    expect(page).to_not have_link('Shuffle')
    click_link '2'
    expect(page).to_not have_link('Shuffle')
  end

  scenario 'not logged in' do
    visit deck_cards_path(@deck_public.id)
    expect(page.body).to include(@card1.front.to_plain_text)
    expect(page).to_not have_link('Shuffle')
    click_link '2'
    expect(page).to_not have_link('Shuffle')
  end
end
