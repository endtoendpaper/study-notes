require 'rails_helper'

describe 'Views cards', type: :system do
  before(:each) do
    @user1 = FactoryBot.create(:user, :confirmed)
    @user2 = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @deck_private = FactoryBot.create(:deck, :private, :one_card, user_id: @user1.id)
    @deck_internal = FactoryBot.create(:deck, :internal, :two_cards, user_id: @user1.id)
    @deck_public = FactoryBot.create(:deck, :public, :thirty_cards, user_id: @user1.id)
    @group_member = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group)
    @deck_private.collaborate(@collaborator, @deck_private.user)
    @deck_private.add_group(@group, @deck_private.user)
    @group.add_member(@group_member, @group.user)
  end

  scenario 'on index page there is pagination as admin' do
    sign_in @admin
    visit deck_cards_path(@deck_public.id)
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end

  scenario 'on index page there is pagination as owner user' do
    sign_in @user1
    visit deck_cards_path(@deck_public.id)
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end

  scenario 'on index page there is pagination as user' do
    sign_in @user2
    visit deck_cards_path(@deck_public.id)
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end

  scenario 'on index page there is pagination not logged in' do
    visit deck_cards_path(@deck_public.id)
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end

  scenario 'on index page for private deck as admin' do
    sign_in @admin
    visit deck_path(@deck_private.id)
    expect(page).to have_link('List cards')
    visit deck_cards_path(@deck_private.id)
    expect(page).to have_content('Private')
    expect(page).to_not have_content('Internal')
    expect(page).to_not have_content('Public')
    expect(page).to have_link('Back to deck')
    expect(page).to have_link('Edit card')
    expect(page).to have_link('Add card')
    expect(page).to have_selector('#front')
  end

  scenario 'on index page for internal deck as admin' do
    sign_in @admin
    visit deck_path(@deck_internal.id)
    expect(page).to have_link('List cards')
    visit deck_cards_path(@deck_internal.id)
    expect(page).to_not have_content('Private')
    expect(page).to have_content('Internal')
    expect(page).to_not have_content('Public')
    expect(page).to have_link('Back to deck')
    expect(page).to have_link('Edit card')
    expect(page).to have_link('Add card')
    expect(page).to have_selector('#front')
  end

  scenario 'on index page for public deck as admin' do
    sign_in @admin
    visit deck_path(@deck_public.id)
    expect(page).to have_link('List cards')
    visit deck_cards_path(@deck_public.id)
    expect(page).to_not have_content('Private')
    expect(page).to_not have_content('Internal')
    expect(page).to have_content('Public')
    expect(page).to have_link('Back to deck')
    expect(page).to have_link('Edit card')
    expect(page).to have_link('Add card')
    expect(page).to have_selector('#front')
  end

  scenario 'on index page for private deck as owner user' do
    sign_in @user1
    visit deck_path(@deck_private.id)
    expect(page).to have_link('List cards')
    visit deck_cards_path(@deck_private.id)
    expect(page).to have_content('Private')
    expect(page).to_not have_content('Internal')
    expect(page).to_not have_content('Public')
    expect(page).to have_link('Back to deck')
    expect(page).to have_link('Edit card')
    expect(page).to have_link('Add card')
    expect(page).to have_selector('#front')
  end

  scenario 'on index page for internal deck as owner user' do
    sign_in @user1
    visit deck_path(@deck_internal.id)
    click_link 'List cards'
    expect(page).to_not have_content('Private')
    expect(page).to have_content('Internal')
    expect(page).to_not have_content('Public')
    expect(page).to have_link('Back to deck')
    expect(page).to have_link('Edit card')
    expect(page).to have_link('Add card')
    expect(page).to have_selector('#front')
  end

  scenario 'on index page for public deck as owner user' do
    sign_in @user1
    visit deck_path(@deck_public.id)
    click_link 'List cards'
    expect(page).to_not have_content('Private')
    expect(page).to_not have_content('Internal')
    expect(page).to have_content('Public')
    expect(page).to have_link('Back to deck')
    expect(page).to have_link('Edit card')
    expect(page).to have_link('Add card')
    expect(page).to have_selector('#front')
  end

  scenario 'on index page for internal deck as user' do
    sign_in @user2
    visit deck_path(@deck_internal.id)
    expect(page).to_not have_link('List cards')
    visit deck_cards_path(@deck_internal.id)
    expect(page).to_not have_content('Private')
    expect(page).to_not have_content('Internal')
    expect(page).to_not have_content('Public')
    expect(page).to have_link('Back to deck')
    expect(page).to_not have_link('Edit card')
    expect(page).to_not have_link('Add card')
    expect(page).to have_selector('#front')
  end

  scenario 'on index page for public deck as user' do
    sign_in @user2
    visit deck_path(@deck_public.id)
    expect(page).to_not have_link('List cards')
    visit deck_cards_path(@deck_public.id)
    expect(page).to_not have_content('Private')
    expect(page).to_not have_content('Internal')
    expect(page).to_not have_content('Public')
    expect(page).to have_link('Back to deck')
    expect(page).to_not have_link('Edit card')
    expect(page).to_not have_link('Add card')
    expect(page).to have_selector('#front')
    expect(page).to have_selector('#back')
  end

  scenario 'on index page for public deck not logged in' do
    visit deck_path(@deck_public.id)
    expect(page).to_not have_link('List cards')
    visit deck_cards_path(@deck_public.id)
    expect(page).to_not have_content('Private')
    expect(page).to_not have_content('Internal')
    expect(page).to_not have_content('Public')
    expect(page).to have_link('Back to deck')
    expect(page).to_not have_link('Edit card')
    expect(page).to_not have_link('Add card')
    expect(page).to have_selector('#front')
    expect(page).to have_selector('#back')
  end

  scenario 'walking through private deck as admin', js: true do
    sign_in @admin
    visit deck_path(@deck_private.id)
    click_link 'Study card'
    expect(page).to have_content('Private')
    expect(page).to have_link('Back to deck')
    expect(page).to have_link('Edit card')
    expect(page).to have_link('Add card')
    expect(html).to_not include('bi bi-caret-right-fill')
    expect(html).to_not include('bi bi-caret-left-fill')
    expect(page).to have_selector('#front')
    expect(page).to_not have_selector('#back')
    find('#flipcard').click
    expect(page).to have_selector('#back')
  end

  scenario 'walking through internal deck as admin', js: true do
    sign_in @admin
    visit deck_path(@deck_internal.id)
    click_link 'Study 2 cards'
    expect(page).to have_content('Internal')
    expect(page).to have_link('Back to deck')
    expect(page).to have_link('Edit card')
    expect(page).to have_link('Add card')
    expect(html).to include('bi bi-caret-right-fill')
    expect(html).to_not include('bi bi-caret-left-fill')
    expect(page).to have_selector('#front')
    expect(page).to_not have_selector('#back')
    find('#flipcard').click
    expect(page).to have_selector('#back')
    click_on(class: 'btn-next')
    expect(page).to have_content('Internal')
    expect(page).to have_link('Back to deck')
    expect(page).to have_link('Edit card')
    expect(page).to have_link('Add card')
    expect(page).to have_selector('.btn-secondary', count: 1)
    expect(page).to have_selector('#front')
    expect(page).to_not have_selector('#back')
    find('#flipcard').click
    expect(page).to have_selector('#back')
    click_on(class: 'btn-prev')
    expect(page).to have_content('Internal')
    expect(page).to have_link('Back to deck')
    expect(page).to have_link('Edit card')
    expect(page).to have_link('Add card')
    expect(page).to have_selector('#front')
    expect(page).to_not have_selector('#back')
    find('#flipcard').click
    expect(page).to have_selector('#back')
    expect(page).to have_selector('.btn-secondary', count: 1)
  end

  scenario 'walking through public deck as admin', js: true do
    sign_in @admin
    visit deck_path(@deck_public.id)
    click_link 'Study 30 cards'
    expect(page).to have_content('Public')
    expect(page).to have_link('Back to deck')
    expect(page).to have_link('Edit card')
    expect(page).to have_link('Add card')
    expect(page).to have_selector('.btn-secondary', count: 1)
    expect(page).to have_selector('#front')
    expect(page).to_not have_selector('#back')
    find('#flipcard').click
    expect(page).to have_selector('#back')
    click_on(class: 'btn-next')
    28.times do
      expect(page).to have_content('Public')
      expect(page).to have_link('Back to deck')
      expect(page).to have_link('Edit card')
      expect(page).to have_link('Add card')
      expect(page).to have_selector('.btn-secondary', count: 2)
      expect(page).to have_selector('#front')
      expect(page).to_not have_selector('#back')
      find('#flipcard').click
      expect(page).to have_selector('#back')
      click_on(class: 'btn-next')
    end
    expect(page).to have_content('Public')
    expect(page).to have_link('Back to deck')
    expect(page).to have_link('Edit card')
    expect(page).to have_link('Add card')
    expect(page).to have_selector('.btn-secondary', count: 1)
    expect(page).to have_selector('#front')
    expect(page).to_not have_selector('#back')
    find('#flipcard').click
    expect(page).to have_selector('#back')
    click_on(class: 'btn-prev')
  end

  scenario 'walking through private deck as owner user', js: true do
    sign_in @user1
    visit deck_path(@deck_private.id)
    click_link 'Study card'
    expect(page).to have_content('Private')
    expect(page).to have_link('Back to deck')
    expect(page).to have_link('Edit card')
    expect(page).to have_link('Add card')
    expect(html).to_not include('bi bi-caret-right-fill')
    expect(html).to_not include('bi bi-caret-left-fill')
    expect(page).to have_selector('#front')
    expect(page).to_not have_selector('#back')
    find('#flipcard').click
    expect(page).to have_selector('#back')
  end

  scenario 'walking through internal deck as owner user', js: true do
    sign_in @user1
    visit deck_path(@deck_internal.id)
    click_link 'Study 2 cards'
    expect(page).to have_content('Internal')
    expect(page).to have_link('Back to deck')
    expect(page).to have_link('Edit card')
    expect(page).to have_link('Add card')
    expect(page).to have_selector('.btn-secondary', count: 1)
    expect(page).to have_selector('#front')
    expect(page).to_not have_selector('#back')
    find('#flipcard').click
    expect(page).to have_selector('#back')
    click_on(class: 'btn-next')
    expect(page).to have_content('Internal')
    expect(page).to have_link('Back to deck')
    expect(page).to have_link('Edit card')
    expect(page).to have_link('Add card')
    expect(page).to have_selector('.btn-secondary', count: 1)
    expect(page).to have_selector('#front')
    expect(page).to_not have_selector('#back')
    find('#flipcard').click
    expect(page).to have_selector('#back')
    click_on(class: 'btn-prev')
    expect(page).to have_content('Internal')
    expect(page).to have_link('Back to deck')
    expect(page).to have_link('Edit card')
    expect(page).to have_link('Add card')
    expect(page).to have_selector('#front')
    expect(page).to_not have_selector('#back')
    find('#flipcard').click
    expect(page).to have_selector('#back')
    expect(page).to have_selector('.btn-secondary', count: 1)
  end

  scenario 'walking through public deck as owner user', js: true do
    sign_in @user1
    visit deck_path(@deck_public.id)
    click_link 'Study 30 cards'
    expect(page).to have_content('Public')
    expect(page).to have_link('Back to deck')
    expect(page).to have_link('Edit card')
    expect(page).to have_link('Add card')
    expect(page).to have_selector('.btn-secondary', count: 1)
    expect(page).to have_selector('#front')
    expect(page).to_not have_selector('#back')
    find('#flipcard').click
    expect(page).to have_selector('#back')
    click_on(class: 'btn-next')
    28.times do
      expect(page).to have_content('Public')
      expect(page).to have_link('Back to deck')
      expect(page).to have_link('Edit card')
      expect(page).to have_link('Add card')
      expect(page).to have_selector('.btn-secondary', count: 2)
      expect(page).to have_selector('#front')
      expect(page).to_not have_selector('#back')
      find('#flipcard').click
      expect(page).to have_selector('#back')
      click_on(class: 'btn-next')
    end
    expect(page).to have_content('Public')
    expect(page).to have_link('Back to deck')
    expect(page).to have_link('Edit card')
    expect(page).to have_link('Add card')
    expect(page).to have_selector('.btn-secondary', count: 1)
    expect(page).to have_selector('#front')
    expect(page).to_not have_selector('#back')
    find('#flipcard').click
    expect(page).to have_selector('#back')
    click_on(class: 'btn-prev')
  end

  scenario 'walking through private deck as collaborator', js: true do
    sign_in @collaborator
    visit deck_path(@deck_private.id)
    click_link 'Study card'
    expect(page).to have_content('Private')
    expect(page).to have_link('Back to deck')
    expect(page).to have_link('Edit card')
    expect(page).to have_link('Add card')
    expect(html).to_not include('bi bi-caret-right-fill')
    expect(html).to_not include('bi bi-caret-left-fill')
    expect(page).to have_selector('#front')
    expect(page).to_not have_selector('#back')
    find('#flipcard').click
    expect(page).to have_selector('#back')
  end

  scenario 'walking through private deck as group member', js: true do
    sign_in @group_member
    visit deck_path(@deck_private.id)
    click_link 'Study card'
    expect(page).to have_content('Private')
    expect(page).to have_link('Back to deck')
    expect(page).to have_link('Edit card')
    expect(page).to have_link('Add card')
    expect(html).to_not include('bi bi-caret-right-fill')
    expect(html).to_not include('bi bi-caret-left-fill')
    expect(page).to have_selector('#front')
    expect(page).to_not have_selector('#back')
    find('#flipcard').click
    expect(page).to have_selector('#back')
  end

  scenario 'walking through internal deck as user', js: true do
    sign_in @user2
    visit deck_path(@deck_internal.id)
    click_link 'Study 2 cards'
    expect(page).to_not have_content('Internal')
    expect(page).to have_link('Back to deck')
    expect(page).to_not have_link('Edit card')
    expect(page).to_not have_link('Add card')
    expect(page).to have_selector('.btn-secondary', count: 1)
    expect(page).to have_selector('#front')
    expect(page).to_not have_selector('#back')
    find('#flipcard').click
    expect(page).to have_selector('#back')
    click_on(class: 'btn-next')
    expect(page).to_not have_content('Internal')
    expect(page).to have_link('Back to deck')
    expect(page).to_not have_link('Edit card')
    expect(page).to_not have_link('Add card')
    expect(page).to have_selector('.btn-secondary', count: 1)
    expect(page).to have_selector('#front')
    expect(page).to_not have_selector('#back')
    find('#flipcard').click
    expect(page).to have_selector('#back')
    click_on(class: 'btn-prev')
    expect(page).to_not have_content('Internal')
    expect(page).to have_link('Back to deck')
    expect(page).to_not have_link('Edit card')
    expect(page).to_not have_link('Add card')
    expect(page).to have_selector('#front')
    expect(page).to_not have_selector('#back')
    find('#flipcard').click
    expect(page).to have_selector('#back')
    expect(page).to have_selector('.btn-secondary', count: 1)
  end

  scenario 'walking through public deck as user', js: true do
    sign_in @user2
    visit deck_path(@deck_public.id)
    click_link 'Study 30 cards'
    expect(page).to_not have_content('Public')
    expect(page).to have_link('Back to deck')
    expect(page).to_not have_link('Edit card')
    expect(page).to_not have_link('Add card')
    expect(page).to have_selector('.btn-secondary', count: 1)
    expect(page).to have_selector('#front')
    expect(page).to_not have_selector('#back')
    find('#flipcard').click
    expect(page).to have_selector('#back')
    click_on(class: 'btn-next')
    28.times do
      expect(page).to_not have_content('Public')
      expect(page).to have_link('Back to deck')
      expect(page).to_not have_link('Edit card')
      expect(page).to_not have_link('Add card')
      expect(page).to have_selector('.btn-secondary', count: 2)
      expect(page).to have_selector('#front')
      expect(page).to_not have_selector('#back')
      find('#flipcard').click
      expect(page).to have_selector('#back')
      click_on(class: 'btn-next')
    end
    expect(page).to_not have_content('Public')
    expect(page).to have_link('Back to deck')
    expect(page).to_not have_link('Edit card')
    expect(page).to_not have_link('Add card')
    expect(page).to have_selector('.btn-secondary', count: 1)
    expect(page).to have_selector('#front')
    expect(page).to_not have_selector('#back')
    find('#flipcard').click
    expect(page).to have_selector('#back')
    click_on(class: 'btn-prev')
  end

  scenario 'walking through public deck not logged in', js: true do
    visit deck_path(@deck_public.id)
    click_link 'Study 30 cards'
    expect(page).to_not have_content('Public')
    expect(page).to have_link('Back to deck')
    expect(page).to_not have_link('Edit card')
    expect(page).to_not have_link('Add card')
    expect(page).to have_selector('.btn-secondary', count: 1)
    expect(page).to have_selector('#front')
    expect(page).to_not have_selector('#back')
    find('#flipcard').click
    expect(page).to have_selector('#back')
    click_on(class: 'btn-next')
    28.times do
      expect(page).to_not have_content('Public')
      expect(page).to have_link('Back to deck')
      expect(page).to_not have_link('Edit card')
      expect(page).to_not have_link('Add card')
      expect(page).to have_selector('.btn-secondary', count: 2)
      expect(page).to have_selector('#front')
      expect(page).to_not have_selector('#back')
      find('#flipcard').click
      expect(page).to have_selector('#back')
      click_on(class: 'btn-next')
    end
    expect(page).to_not have_content('Public')
    expect(page).to have_link('Back to deck')
    expect(page).to_not have_link('Edit card')
    expect(page).to_not have_link('Add card')
    expect(page).to have_selector('.btn-secondary', count: 1)
    expect(page).to have_selector('#front')
    expect(page).to_not have_selector('#back')
    find('#flipcard').click
    expect(page).to have_selector('#back')
    click_on(class: 'btn-prev')
  end
end
