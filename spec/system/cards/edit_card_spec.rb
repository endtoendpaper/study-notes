require 'rails_helper'

describe 'User edits card', type: :system do
  let(:front) { Faker::Lorem.sentence(word_count: 15) }
  let(:back) { Faker::Lorem.sentence(word_count: 50) }

  before(:each) do
    @user = FactoryBot.create(:user, :confirmed)
    @deck_public1 = FactoryBot.create(:deck, :public, user_id: @user.id)
    @card = FactoryBot.create(:card, deck_id: @deck_public1.id)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @group_member = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group)
    @deck_public1.collaborate(@collaborator, @deck_public1.user)
    @deck_public1.add_group(@group, @deck_public1.user)
    @group.add_member(@group_member, @group.user)
  end

  scenario 'as user with valid data', js: true do
    sign_in @user
    visit deck_card_path(@deck_public1.id, @card.id)
    click_link 'Edit card'
    fill_in_trix_editor 'card_front_trix_input_card_' + @card.id.to_s, with: front
    fill_in_trix_editor 'card_back_trix_input_card_' + @card.id.to_s, with: back
    click_button 'Submit'
    expect(current_path).to include('/decks/' + @deck_public1.id.to_s + '/cards/' + @card.id.to_s)
    expect(page).to have_content('Card was successfully updated.')
    expect(page).to_not have_content(@deck_public1.title)
    expect(page).to_not have_content(@deck_public1.description)
    expect(page).to have_content(front)
    expect(page).to_not have_content(back)
  end

  scenario "as admin editing user's deck with valid data", js: true do
    sign_in @admin
    visit deck_card_path(@deck_public1.id, @card.id)
    click_link 'Edit card'
    fill_in_trix_editor 'card_front_trix_input_card_' + @card.id.to_s, with: front
    fill_in_trix_editor 'card_back_trix_input_card_' + @card.id.to_s, with: back
    click_button 'Submit'
    expect(current_path).to include('/decks/' + @deck_public1.id.to_s + '/cards/' + @card.id.to_s)
    expect(page).to have_content('Card was successfully updated.')
    expect(page).to_not have_content(@deck_public1.title)
    expect(page).to_not have_content(@deck_public1.description)
    expect(page).to have_content(front)
    expect(page).to_not have_content(back)
  end

  scenario 'as user with invalid data', js: true do
    sign_in @user
    visit deck_card_path(@deck_public1.id, @card.id)
    click_link 'Edit card'
    fill_in_trix_editor 'card_front_trix_input_card_' + @card.id.to_s, with: front
    clear_trix_editor 'card_back_trix_input_card_' + @card.id.to_s
    click_button 'Submit'
    expect(current_path).to include('/decks/' + @deck_public1.id.to_s + '/cards/' + @card.id.to_s)
    expect(page).to have_content('1 error prohibited this card from being saved:')
    expect(page).to have_content("Back can't be blank")
  end

  scenario "as admin editing user's deck with invalid data", js: true do
    sign_in @admin
    visit deck_card_path(@deck_public1.id, @card.id)
    click_link 'Edit card'
    fill_in_trix_editor 'card_front_trix_input_card_' + @card.id.to_s, with: front
    clear_trix_editor 'card_back_trix_input_card_' + @card.id.to_s
    click_button 'Submit'
    expect(current_path).to include('/decks/' + @deck_public1.id.to_s + '/cards/' + @card.id.to_s)
    expect(page).to have_content('1 error prohibited this card from being saved:')
    expect(page).to have_content("Back can't be blank")
  end

  scenario 'as collaborator with valid data', js: true do
    sign_in @collaborator
    visit deck_card_path(@deck_public1.id, @card.id)
    click_link 'Edit card'
    fill_in_trix_editor 'card_front_trix_input_card_' + @card.id.to_s, with: front
    fill_in_trix_editor 'card_back_trix_input_card_' + @card.id.to_s, with: back
    click_button 'Submit'
    expect(current_path).to include('/decks/' + @deck_public1.id.to_s + '/cards/' + @card.id.to_s)
    expect(page).to have_content('Card was successfully updated.')
    expect(page).to_not have_content(@deck_public1.title)
    expect(page).to_not have_content(@deck_public1.description)
    expect(page).to have_content(front)
    expect(page).to_not have_content(back)
  end

  scenario 'as group member with valid data', js: true do
    sign_in @group_member
    visit deck_card_path(@deck_public1.id, @card.id)
    click_link 'Edit card'
    fill_in_trix_editor 'card_front_trix_input_card_' + @card.id.to_s, with: front
    fill_in_trix_editor 'card_back_trix_input_card_' + @card.id.to_s, with: back
    click_button 'Submit'
    expect(current_path).to include('/decks/' + @deck_public1.id.to_s + '/cards/' + @card.id.to_s)
    expect(page).to have_content('Card was successfully updated.')
    expect(page).to_not have_content(@deck_public1.title)
    expect(page).to_not have_content(@deck_public1.description)
    expect(page).to have_content(front)
    expect(page).to_not have_content(back)
  end
end
