require 'rails_helper'

describe 'Move card positions', type: :system do
  before(:each) do
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @deck_public = FactoryBot.create(:deck, :public, user_id: @owner.id)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group_owner = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group, user_id: @group_owner.id)
    @deck_public.collaborate(@collaborator, @deck_public.user)
    @deck_public.add_group(@group, @deck_public.user)
    @group.add_member(@group_member, @group.user)

    14.times do |i|
      instance_variable_set("@card#{i}", FactoryBot.create(:card, front: "question#{i}.", back: "answer#{i}.",
        deck_id: @deck_public.id))
    end
  end

  scenario 'as admin', js: true do
    sign_in @admin
    visit deck_cards_path(@deck_public.id)

    expect(page).to have_content(@card1.front.to_plain_text)
    expect(page).to have_css('i.bi-arrow-up', minimum: 1)
    expect(page).to have_css('i.bi-arrow-down', minimum: 1)

    click_link '2'
    expect(page).to have_content(@card12.front.to_plain_text)
    expect(page).to have_content(@card13.front.to_plain_text)

    using_wait_time 5 do
      find(:xpath, '/html/body/main/div/div/div/div[1]/div/span/div/div[1]/div/div[1]/div[1]/form[1]/button').click
    end
    expect(page).to have_content(@card13.front.to_plain_text)
    expect(page).to have_content(@card11.front.to_plain_text)
    expect(page).not_to have_content(@card12.front.to_plain_text)

    using_wait_time 5 do
      find(:xpath, '/html/body/main/div/div/div/div[1]/div/span/div/div[2]/div/div[1]/div[1]/form[2]/button').click
    end
    expect(page).to have_content(@card12.front.to_plain_text)
    expect(page).to have_content(@card11.front.to_plain_text)
    expect(page).not_to have_content(@card13.front.to_plain_text)
  end

  scenario 'as deck owner', js: true do
    sign_in @owner
    visit deck_cards_path(@deck_public.id)

    expect(page).to have_content(@card1.front.to_plain_text)
    expect(page).to have_css('i.bi-arrow-up', minimum: 1)
    expect(page).to have_css('i.bi-arrow-down', minimum: 1)

    click_link '2'
    expect(page).to have_content(@card12.front.to_plain_text)
    expect(page).to have_content(@card13.front.to_plain_text)

    using_wait_time 5 do
      find(:xpath, '/html/body/main/div/div/div/div[1]/div/span/div/div[1]/div/div[1]/div[1]/form[1]/button').click
    end
    expect(page).to have_content(@card13.front.to_plain_text)
    expect(page).to have_content(@card11.front.to_plain_text)
    expect(page).not_to have_content(@card12.front.to_plain_text)

    using_wait_time 5 do
      find(:xpath, '/html/body/main/div/div/div/div[1]/div/span/div/div[2]/div/div[1]/div[1]/form[2]/button').click
    end
    expect(page).to have_content(@card12.front.to_plain_text)
    expect(page).to have_content(@card11.front.to_plain_text)
    expect(page).not_to have_content(@card13.front.to_plain_text)
  end

  scenario 'as deck collaborator', js: true do
    sign_in @collaborator
    visit deck_cards_path(@deck_public.id)

    expect(page).to have_content(@card1.front.to_plain_text)
    expect(page).to have_css('i.bi-arrow-up', minimum: 1)
    expect(page).to have_css('i.bi-arrow-down', minimum: 1)

    click_link '2'
    expect(page).to have_content(@card12.front.to_plain_text)
    expect(page).to have_content(@card13.front.to_plain_text)

    using_wait_time 5 do
      find(:xpath, '/html/body/main/div/div/div/div[1]/div/span/div/div[1]/div/div[1]/div[1]/form[1]/button').click
    end
    expect(page).to have_content(@card13.front.to_plain_text)
    expect(page).to have_content(@card11.front.to_plain_text)
    expect(page).not_to have_content(@card12.front.to_plain_text)

    using_wait_time 5 do
      find(:xpath, '/html/body/main/div/div/div/div[1]/div/span/div/div[2]/div/div[1]/div[1]/form[2]/button').click
    end
    expect(page).to have_content(@card12.front.to_plain_text)
    expect(page).to have_content(@card11.front.to_plain_text)
    expect(page).not_to have_content(@card13.front.to_plain_text)
  end

  scenario 'as group member', js: true do
    sign_in @group_member
    visit deck_cards_path(@deck_public.id)

    expect(page).to have_content(@card1.front.to_plain_text)
    expect(page).to have_css('i.bi-arrow-up', minimum: 1)
    expect(page).to have_css('i.bi-arrow-down', minimum: 1)

    click_link '2'
    expect(page).to have_content(@card12.front.to_plain_text)
    expect(page).to have_content(@card13.front.to_plain_text)

    using_wait_time 5 do
      find(:xpath, '/html/body/main/div/div/div/div[1]/div/span/div/div[1]/div/div[1]/div[1]/form[1]/button').click
    end
    expect(page).to have_content(@card13.front.to_plain_text)
    expect(page).to have_content(@card11.front.to_plain_text)
    expect(page).not_to have_content(@card12.front.to_plain_text)

    using_wait_time 5 do
      find(:xpath, '/html/body/main/div/div/div/div[1]/div/span/div/div[2]/div/div[1]/div[1]/form[2]/button').click
    end
    expect(page).to have_content(@card12.front.to_plain_text)
    expect(page).to have_content(@card11.front.to_plain_text)
    expect(page).not_to have_content(@card13.front.to_plain_text)
  end

  scenario 'as group owner', js: true do
    sign_in @group_owner
    visit deck_cards_path(@deck_public.id)

    expect(page).to have_content(@card1.front.to_plain_text)
    expect(page).to have_css('i.bi-arrow-up', minimum: 1)
    expect(page).to have_css('i.bi-arrow-down', minimum: 1)

    click_link '2'
    expect(page).to have_content(@card12.front.to_plain_text)
    expect(page).to have_content(@card13.front.to_plain_text)

    using_wait_time 5 do
      find(:xpath, '/html/body/main/div/div/div/div[1]/div/span/div/div[1]/div/div[1]/div[1]/form[1]/button').click
    end
    expect(page).to have_content(@card13.front.to_plain_text)
    expect(page).to have_content(@card11.front.to_plain_text)
    expect(page).not_to have_content(@card12.front.to_plain_text)

    using_wait_time 5 do
      find(:xpath, '/html/body/main/div/div/div/div[1]/div/span/div/div[2]/div/div[1]/div[1]/form[2]/button').click
    end
    expect(page).to have_content(@card12.front.to_plain_text)
    expect(page).to have_content(@card11.front.to_plain_text)
    expect(page).not_to have_content(@card13.front.to_plain_text)
  end

  scenario 'as user' do
    sign_in @user
    visit deck_cards_path(@deck_public.id)
    expect(page).to_not have_css('i.bi-arrow-up', minimum: 1)
    expect(page).to_not have_css('i.bi-arrow-down', minimum: 1)
  end

  scenario 'not logged in' do
    visit deck_cards_path(@deck_public.id)
    expect(page.body).to include(@card1.front.to_plain_text)
    expect(page).to_not have_css('i.bi-arrow-up', minimum: 1)
    expect(page).to_not have_css('i.bi-arrow-down', minimum: 1)
  end
end
