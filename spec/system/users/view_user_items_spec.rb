require 'rails_helper'

describe 'Views user decks', type: :system do
  before(:each) do
    reset_elasticsearch_indices

    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @group1 = FactoryBot.create(:group)
    @group1.add_member(@owner, @group1.user)
    @group2 = FactoryBot.create(:group, user_id: @owner.id)

    UserItems.item_list.each do |item|
      instance_variable_set("@#{item.singularize}_public_own", FactoryBot.create(item.singularize, :public, user_id: @owner.id))
      instance_variable_set("@#{item.singularize}_internal_own", FactoryBot.create(item.singularize, :internal, user_id: @owner.id))
      instance_variable_set("@#{item.singularize}_private_own", FactoryBot.create(item.singularize, :private, user_id: @owner.id))

      instance_variable_set("@#{item.singularize}_public_colb", FactoryBot.create(item.singularize, :public))
      instance_variable_set("@#{item.singularize}_internal_colb", FactoryBot.create(item.singularize, :internal))
      instance_variable_set("@#{item.singularize}_private_colb", FactoryBot.create(item.singularize, :private))

      instance_variable_get("@#{item.singularize}_public_colb").collaborate(@owner, instance_variable_get("@#{item.singularize}_public_colb").user)
      instance_variable_get("@#{item.singularize}_internal_colb").collaborate(@owner, instance_variable_get("@#{item.singularize}_internal_colb").user)
      instance_variable_get("@#{item.singularize}_private_colb").collaborate(@owner, instance_variable_get("@#{item.singularize}_private_colb").user)

      instance_variable_set("@#{item.singularize}_public_group_own", FactoryBot.create(item.singularize, :public))
      instance_variable_set("@#{item.singularize}_internal_group_own", FactoryBot.create(item.singularize, :internal))
      instance_variable_set("@#{item.singularize}_private_group_own", FactoryBot.create(item.singularize, :private))

      instance_variable_get("@#{item.singularize}_public_group_own").add_group(@group2, instance_variable_get("@#{item.singularize}_public_group_own").user)
      instance_variable_get("@#{item.singularize}_internal_group_own").add_group(@group2, instance_variable_get("@#{item.singularize}_internal_group_own").user)
      instance_variable_get("@#{item.singularize}_private_group_own").add_group(@group2, instance_variable_get("@#{item.singularize}_private_group_own").user)

      instance_variable_set("@#{item.singularize}_public_group_memb", FactoryBot.create(item.singularize, :public))
      instance_variable_set("@#{item.singularize}_internal_group_memb", FactoryBot.create(item.singularize, :internal))
      instance_variable_set("@#{item.singularize}_private_group_memb", FactoryBot.create(item.singularize, :private))

      instance_variable_get("@#{item.singularize}_public_group_memb").add_group(@group1, instance_variable_get("@#{item.singularize}_public_group_memb").user)
      instance_variable_get("@#{item.singularize}_internal_group_memb").add_group(@group1, instance_variable_get("@#{item.singularize}_internal_group_memb").user)
      instance_variable_get("@#{item.singularize}_private_group_memb").add_group(@group1, instance_variable_get("@#{item.singularize}_private_group_memb").user)
    end

    reindex_elasticsearch_data
  end

  scenario 'as admin' do
    sign_in @admin
    UserItems.item_list.each do |item|
      visit '/users/' + @owner.username + "/#{item}"
      expect(page).to have_content("Show owned #{item}")
      page.check('view_owned')
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_private_group_memb").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal_group_memb").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public_group_memb").title)

      expect(page).to have_link(instance_variable_get("@#{item.singularize}_private_group_own").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal_group_own").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public_group_own").title)

      expect(page).to have_link(instance_variable_get("@#{item.singularize}_private_colb").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal_colb").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public_colb").title)

      expect(page).to have_link(instance_variable_get("@#{item.singularize}_private_own").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal_own").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public_own").title)
    end
  end

  scenario 'as owner' do
    sign_in @owner
    UserItems.item_list.each do |item|
      visit '/users/' + @owner.username + "/#{item}"
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_private_group_memb").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal_group_memb").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public_group_memb").title)

      expect(page).to have_link(instance_variable_get("@#{item.singularize}_private_group_own").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal_group_own").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public_group_own").title)

      expect(page).to have_link(instance_variable_get("@#{item.singularize}_private_colb").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal_colb").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public_colb").title)

      expect(page).to have_link(instance_variable_get("@#{item.singularize}_private_own").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal_own").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public_own").title)
    end
  end

  scenario 'as another user' do
    sign_in @user
    UserItems.item_list.each do |item|
      visit '/users/' + @owner.username + "/#{item}"
      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_private_group_memb").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal_group_memb").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public_group_memb").title)

      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_private_group_own").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal_group_own").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public_group_own").title)

      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_private_colb").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal_colb").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public_colb").title)

      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_private_own").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal_own").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public_own").title)

      instance_variable_get("@#{item.singularize}_private_colb").collaborate(@user, instance_variable_get("@#{item.singularize}_private_colb").user)
      instance_variable_get("@#{item.singularize}_private_group_own").update(user_id: @user.id)
      @group2.add_member(@user, @group2.user)
      @group1.add_member(@user, @group1.user)
      group3 = FactoryBot.create(:group, user_id: @user.id)
      instance_variable_get("@#{item.singularize}_private_own").add_group(group3, instance_variable_get("@#{item.singularize}_private_own").user)

      reindex_elasticsearch_data

      visit '/users/' + @owner.username + "/#{item}"

      expect(page).to have_link(instance_variable_get("@#{item.singularize}_private_group_memb").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal_group_memb").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public_group_memb").title)

      expect(page).to have_link(instance_variable_get("@#{item.singularize}_private_group_own").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal_group_own").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public_group_own").title)

      expect(page).to have_link(instance_variable_get("@#{item.singularize}_private_colb").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal_colb").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public_colb").title)

      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public_own").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal_own").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_private_own").title)

      @group2.remove_member(@user, @group2.user)
      @group1.remove_member(@user, @group1.user)

      reindex_elasticsearch_data
    end
  end

  scenario 'not logged in' do
    UserItems.item_list.each do |item|
      visit '/users/' + @owner.username + "/#{item}"
      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_private_group_memb").title)
      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_internal_group_memb").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public_group_memb").title)

      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_private_group_own").title)
      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_internal_group_own").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public_group_own").title)

      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_private_colb").title)
      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_internal_colb").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public_colb").title)

      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_private_own").title)
      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_internal_own").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public_own").title)
    end
  end
end

