require 'rails_helper'

describe 'Toggles dark mode', type: :system do
  before(:each) do
    @user = FactoryBot.create(:user, :confirmed)
  end

  scenario 'as user' do
    sign_in @user
    visit edit_user_registration_path
    expect(html).to_not include('bg-dark')
    expect(html).to_not include('text-white')
    page.check('Darkmode')
    click_button 'Update'
    expect(page).to have_content('Your account has been updated successfully.')
    expect(html).to include('bg-dark')
    expect(html).to include('text-white')
  end

  scenario 'as admin for a user' do
    admin = FactoryBot.create(:user, :confirmed, :admin)
    sign_in admin
    visit user_path(@user)
    expect(page).to have_content('Darkmode: false')
    click_link 'Edit user'
    page.check('Darkmode')
    click_button 'Update'
    expect(page).to have_content('User was successfully updated.')
    expect(page).to have_content('Darkmode: true')
    '/users/' + @user.username
  end
end
