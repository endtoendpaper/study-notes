require 'rails_helper'

describe 'Views users activity logs', type: :system do
  before(:each) do
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group_owner = FactoryBot.create(:user, :confirmed)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group, user_id: @group_owner.id)
    @group.add_member(@group_member, @group.user)
  end

  scenario 'as admin' do
    sign_in @admin

    UserItems.item_list.each do |item|
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      FactoryBot.create(:activity_log, :created_private_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :created_internal_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :created_public_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :edited_private_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :edited_internal_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :edited_public_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      visit user_path(@owner)
      expect(page).to have_content('Recent Activity')
      expect(page).to have_content("created #{item.singularize.humanize.titleize} on", minimum: 3)
      expect(page).to have_content("edited #{item.singularize.humanize.titleize} on", minimum: 3)
      item.singularize.camelize.constantize.destroy_all
    end

    UserItems.item_list.each do |item|
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      FactoryBot.create(:activity_log, :tagged_private_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :tagged_internal_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :tagged_public_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :untagged_private_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :untagged_internal_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :untagged_public_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      visit user_path(@owner)
      expect(page).to have_content('Recent Activity')
      expect(page).to have_content("tagged #{item.singularize.humanize.titleize} with myTag", minimum: 3)
      expect(page).to have_content("untagged #{item.singularize.humanize.titleize} with myTag", minimum: 3)
      item.singularize.camelize.constantize.destroy_all
    end

    UserItems.subitem_list.each do |item|
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      item1 = FactoryBot.create(:activity_log, :created_private_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private).id, record_type: item.singularize.camelize)
      item2 = FactoryBot.create(:activity_log, :created_internal_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal).id, record_type: item.singularize.camelize)
      item3 = FactoryBot.create(:activity_log, :created_public_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public).id, record_type: item.singularize.camelize)
      item4 = FactoryBot.create(:activity_log, :edited_private_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private).id, record_type: item.singularize.camelize)
      item5 = FactoryBot.create(:activity_log, :edited_internal_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal).id, record_type: item.singularize.camelize)
      item6 = FactoryBot.create(:activity_log, :edited_public_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public).id, record_type: item.singularize.camelize)
      item1.record.parent.update(user: @owner)
      item2.record.parent.update(user: @owner)
      item3.record.parent.update(user: @owner)
      item4.record.parent.update(user: @owner)
      item5.record.parent.update(user: @owner)
      item6.record.parent.update(user: @owner)
      visit user_path(@owner)
      expect(page).to have_content('Recent Activity')
      expect(page).to have_content("created #{item.singularize.humanize.titleize} on", minimum: 3)
      expect(page).to have_content("edited #{item.singularize.humanize.titleize} on", minimum: 3)
      item.singularize.camelize.constantize.destroy_all
    end
  end

  scenario 'as owner' do
    sign_in @owner
    visit user_path(@owner)
    expect(page).to have_content('Recent Activity')
    UserItems.item_list.each do |item|
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      FactoryBot.create(:activity_log, :created_private_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :created_internal_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :created_public_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :edited_private_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :edited_internal_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :edited_public_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      visit user_path(@owner)
      expect(page).to have_content('Recent Activity')
      expect(page).to have_content("created #{item.singularize.humanize.titleize} on", minimum: 3)
      expect(page).to have_content("edited #{item.singularize.humanize.titleize} on", minimum: 3)
      item.singularize.camelize.constantize.destroy_all
    end

    UserItems.item_list.each do |item|
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      FactoryBot.create(:activity_log, :tagged_private_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :tagged_internal_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :tagged_public_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :untagged_private_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :untagged_internal_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :untagged_public_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      visit user_path(@owner)
      expect(page).to have_content('Recent Activity')
      expect(page).to have_content("tagged #{item.singularize.humanize.titleize} with myTag", minimum: 3)
      expect(page).to have_content("untagged #{item.singularize.humanize.titleize} with myTag", minimum: 3)
      item.singularize.camelize.constantize.destroy_all
    end

    UserItems.subitem_list.each do |item|
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      item1 = FactoryBot.create(:activity_log, :created_private_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private).id, record_type: item.singularize.camelize)
      item2 = FactoryBot.create(:activity_log, :created_internal_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal).id, record_type: item.singularize.camelize)
      item3 = FactoryBot.create(:activity_log, :created_public_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public).id, record_type: item.singularize.camelize)
      item4 = FactoryBot.create(:activity_log, :edited_private_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private).id, record_type: item.singularize.camelize)
      item5 = FactoryBot.create(:activity_log, :edited_internal_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal).id, record_type: item.singularize.camelize)
      item6 = FactoryBot.create(:activity_log, :edited_public_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public).id, record_type: item.singularize.camelize)
      item1.record.parent.update(user: @owner)
      item2.record.parent.update(user: @owner)
      item3.record.parent.update(user: @owner)
      item4.record.parent.update(user: @owner)
      item5.record.parent.update(user: @owner)
      item6.record.parent.update(user: @owner)
      visit user_path(@owner)
      expect(page).to have_content('Recent Activity')
      expect(page).to have_content("created #{item.singularize.humanize.titleize} on", minimum: 3)
      expect(page).to have_content("edited #{item.singularize.humanize.titleize} on", minimum: 3)
      item.singularize.camelize.constantize.destroy_all
    end
  end

  scenario 'as item collaborator' do
    sign_in @collaborator
    visit user_path(@owner)
    UserItems.item_list.each do |item|
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      item1 = FactoryBot.create(:activity_log, :created_private_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item2 = FactoryBot.create(:activity_log, :created_internal_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item3 = FactoryBot.create(:activity_log, :created_public_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item4 = FactoryBot.create(:activity_log, :edited_private_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item5 = FactoryBot.create(:activity_log, :edited_internal_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item6 = FactoryBot.create(:activity_log, :edited_public_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item1.record.collaborate(@collaborator, @owner)
      item2.record.collaborate(@collaborator, @owner)
      item3.record.collaborate(@collaborator, @owner)
      item4.record.collaborate(@collaborator, @owner)
      item5.record.collaborate(@collaborator, @owner)
      item6.record.collaborate(@collaborator, @owner)
      visit user_path(@owner)
      expect(page).to have_content('Recent Activity')
      expect(page).to have_content("created #{item.singularize.humanize.titleize} on", minimum: 3)
      expect(page).to have_content("edited #{item.singularize.humanize.titleize} on", minimum: 3)
      item.singularize.camelize.constantize.destroy_all
    end

    UserItems.item_list.each do |item|
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      item1 = FactoryBot.create(:activity_log, :tagged_private_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item2 = FactoryBot.create(:activity_log, :tagged_internal_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item3 = FactoryBot.create(:activity_log, :tagged_public_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item4 = FactoryBot.create(:activity_log, :untagged_private_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item5 = FactoryBot.create(:activity_log, :untagged_internal_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item6 = FactoryBot.create(:activity_log, :untagged_public_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item1.record.collaborate(@collaborator, @owner)
      item2.record.collaborate(@collaborator, @owner)
      item3.record.collaborate(@collaborator, @owner)
      item4.record.collaborate(@collaborator, @owner)
      item5.record.collaborate(@collaborator, @owner)
      item6.record.collaborate(@collaborator, @owner)
      visit user_path(@owner)
      expect(page).to have_content('Recent Activity')
      expect(page).to have_content("tagged #{item.singularize.humanize.titleize} with myTag", minimum: 3)
      expect(page).to have_content("untagged #{item.singularize.humanize.titleize} with myTag", minimum: 3)
      item.singularize.camelize.constantize.destroy_all
    end

    UserItems.subitem_list.each do |item|
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      item1 = FactoryBot.create(:activity_log, :created_private_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private).id, record_type: item.singularize.camelize)
      item2 = FactoryBot.create(:activity_log, :created_internal_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal).id, record_type: item.singularize.camelize)
      item3 = FactoryBot.create(:activity_log, :created_public_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public).id, record_type: item.singularize.camelize)
      item4 = FactoryBot.create(:activity_log, :edited_private_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private).id, record_type: item.singularize.camelize)
      item5 = FactoryBot.create(:activity_log, :edited_internal_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal).id, record_type: item.singularize.camelize)
      item6 = FactoryBot.create(:activity_log, :edited_public_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public).id, record_type: item.singularize.camelize)
      item1.record.parent.update(user: @owner)
      item2.record.parent.update(user: @owner)
      item3.record.parent.update(user: @owner)
      item4.record.parent.update(user: @owner)
      item5.record.parent.update(user: @owner)
      item6.record.parent.update(user: @owner)
      item1.record.parent.collaborate(@collaborator, @owner)
      item2.record.parent.collaborate(@collaborator, @owner)
      item3.record.parent.collaborate(@collaborator, @owner)
      item4.record.parent.collaborate(@collaborator, @owner)
      item5.record.parent.collaborate(@collaborator, @owner)
      item6.record.parent.collaborate(@collaborator, @owner)
      visit user_path(@owner)
      expect(page).to have_content('Recent Activity')
      expect(page).to have_content("created #{item.singularize.humanize.titleize} on", minimum: 3)
      expect(page).to have_content("edited #{item.singularize.humanize.titleize} on", minimum: 3)
      item.singularize.camelize.constantize.destroy_all
    end
  end

  scenario 'as item group owner' do
    sign_in @group_owner
    visit user_path(@owner)
    UserItems.item_list.each do |item|
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      item1 = FactoryBot.create(:activity_log, :created_private_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item2 = FactoryBot.create(:activity_log, :created_internal_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item3 = FactoryBot.create(:activity_log, :created_public_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item4 = FactoryBot.create(:activity_log, :edited_private_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item5 = FactoryBot.create(:activity_log, :edited_internal_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item6 = FactoryBot.create(:activity_log, :edited_public_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item1.record.add_group(@group, @owner)
      item2.record.add_group(@group, @owner)
      item3.record.add_group(@group, @owner)
      item4.record.add_group(@group, @owner)
      item5.record.add_group(@group, @owner)
      item6.record.add_group(@group, @owner)
      visit user_path(@owner)
      expect(page).to have_content('Recent Activity')
      expect(page).to have_content("created #{item.singularize.humanize.titleize} on", minimum: 3)
      expect(page).to have_content("edited #{item.singularize.humanize.titleize} on", minimum: 3)
      item.singularize.camelize.constantize.destroy_all
    end

    UserItems.item_list.each do |item|
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      item1 = FactoryBot.create(:activity_log, :tagged_private_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item2 = FactoryBot.create(:activity_log, :tagged_internal_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item3 = FactoryBot.create(:activity_log, :tagged_public_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item4 = FactoryBot.create(:activity_log, :untagged_private_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item5 = FactoryBot.create(:activity_log, :untagged_internal_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item6 = FactoryBot.create(:activity_log, :untagged_public_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item1.record.add_group(@group, @owner)
      item2.record.add_group(@group, @owner)
      item3.record.add_group(@group, @owner)
      item4.record.add_group(@group, @owner)
      item5.record.add_group(@group, @owner)
      item6.record.add_group(@group, @owner)
      visit user_path(@owner)
      expect(page).to have_content('Recent Activity')
      expect(page).to have_content("tagged #{item.singularize.humanize.titleize} with myTag", minimum: 3)
      expect(page).to have_content("untagged #{item.singularize.humanize.titleize} with myTag", minimum: 3)
      item.singularize.camelize.constantize.destroy_all
    end

    UserItems.subitem_list.each do |item|
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      item1 = FactoryBot.create(:activity_log, :created_private_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private).id, record_type: item.singularize.camelize)
      item2 = FactoryBot.create(:activity_log, :created_internal_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal).id, record_type: item.singularize.camelize)
      item3 = FactoryBot.create(:activity_log, :created_public_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public).id, record_type: item.singularize.camelize)
      item4 = FactoryBot.create(:activity_log, :edited_private_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private).id, record_type: item.singularize.camelize)
      item5 = FactoryBot.create(:activity_log, :edited_internal_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal).id, record_type: item.singularize.camelize)
      item6 = FactoryBot.create(:activity_log, :edited_public_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public).id, record_type: item.singularize.camelize)
      item1.record.parent.update(user: @owner)
      item2.record.parent.update(user: @owner)
      item3.record.parent.update(user: @owner)
      item4.record.parent.update(user: @owner)
      item5.record.parent.update(user: @owner)
      item6.record.parent.update(user: @owner)
      item1.record.parent.add_group(@group, @owner)
      item2.record.parent.add_group(@group, @owner)
      item3.record.parent.add_group(@group, @owner)
      item4.record.parent.add_group(@group, @owner)
      item5.record.parent.add_group(@group, @owner)
      item6.record.parent.add_group(@group, @owner)
      visit user_path(@owner)
      expect(page).to have_content('Recent Activity')
      expect(page).to have_content("created #{item.singularize.humanize.titleize} on", minimum: 3)
      expect(page).to have_content("edited #{item.singularize.humanize.titleize} on", minimum: 3)
      item.singularize.camelize.constantize.destroy_all
    end
  end

  scenario 'as item group member' do
    sign_in @group_member
    UserItems.item_list.each do |item|
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      item1 = FactoryBot.create(:activity_log, :created_private_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item2 = FactoryBot.create(:activity_log, :created_internal_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item3 = FactoryBot.create(:activity_log, :created_public_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item4 = FactoryBot.create(:activity_log, :edited_private_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item5 = FactoryBot.create(:activity_log, :edited_internal_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item6 = FactoryBot.create(:activity_log, :edited_public_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item1.record.add_group(@group, @owner)
      item2.record.add_group(@group, @owner)
      item3.record.add_group(@group, @owner)
      item4.record.add_group(@group, @owner)
      item5.record.add_group(@group, @owner)
      item6.record.add_group(@group, @owner)
      visit user_path(@owner)
      expect(page).to have_content('Recent Activity')
      expect(page).to have_content("created #{item.singularize.humanize.titleize} on", minimum: 3)
      expect(page).to have_content("edited #{item.singularize.humanize.titleize} on", minimum: 3)
      item.singularize.camelize.constantize.destroy_all
    end

    UserItems.item_list.each do |item|
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      item1 = FactoryBot.create(:activity_log, :tagged_private_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item2 = FactoryBot.create(:activity_log, :tagged_internal_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item3 = FactoryBot.create(:activity_log, :tagged_public_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item4 = FactoryBot.create(:activity_log, :untagged_private_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item5 = FactoryBot.create(:activity_log, :untagged_internal_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item6 = FactoryBot.create(:activity_log, :untagged_public_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      item1.record.add_group(@group, @owner)
      item2.record.add_group(@group, @owner)
      item3.record.add_group(@group, @owner)
      item4.record.add_group(@group, @owner)
      item5.record.add_group(@group, @owner)
      item6.record.add_group(@group, @owner)
      visit user_path(@owner)
      expect(page).to have_content('Recent Activity')
      expect(page).to have_content("tagged #{item.singularize.humanize.titleize} with myTag", minimum: 3)
      expect(page).to have_content("untagged #{item.singularize.humanize.titleize} with myTag", minimum: 3)
      item.singularize.camelize.constantize.destroy_all
    end

    UserItems.subitem_list.each do |item|
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      item1 = FactoryBot.create(:activity_log, :created_private_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private).id, record_type: item.singularize.camelize)
      item2 = FactoryBot.create(:activity_log, :created_internal_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal).id, record_type: item.singularize.camelize)
      item3 = FactoryBot.create(:activity_log, :created_public_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public).id, record_type: item.singularize.camelize)
      item4 = FactoryBot.create(:activity_log, :edited_private_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private).id, record_type: item.singularize.camelize)
      item5 = FactoryBot.create(:activity_log, :edited_internal_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal).id, record_type: item.singularize.camelize)
      item6 = FactoryBot.create(:activity_log, :edited_public_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public).id, record_type: item.singularize.camelize)
      item1.record.parent.update(user: @owner)
      item2.record.parent.update(user: @owner)
      item3.record.parent.update(user: @owner)
      item4.record.parent.update(user: @owner)
      item5.record.parent.update(user: @owner)
      item6.record.parent.update(user: @owner)
      item1.record.parent.add_group(@group, @owner)
      item2.record.parent.add_group(@group, @owner)
      item3.record.parent.add_group(@group, @owner)
      item4.record.parent.add_group(@group, @owner)
      item5.record.parent.add_group(@group, @owner)
      item6.record.parent.add_group(@group, @owner)
      visit user_path(@owner)
      expect(page).to have_content('Recent Activity')
      expect(page).to have_content("created #{item.singularize.humanize.titleize} on", minimum: 3)
      expect(page).to have_content("edited #{item.singularize.humanize.titleize} on", minimum: 3)
      item.singularize.camelize.constantize.destroy_all
    end
  end

  scenario 'as user' do
    sign_in @user
    visit user_path(@owner)
    UserItems.item_list.each do |item|
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      FactoryBot.create(:activity_log, :created_private_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :created_internal_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :created_public_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :edited_private_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :edited_internal_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :edited_public_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      visit user_path(@owner)
      expect(page).to have_content('Recent Activity')
      expect(page).to have_content("created #{item.singularize.humanize.titleize} on", maximum: 2)
      expect(page).to have_content("edited #{item.singularize.humanize.titleize} on", maximum: 2)
      item.singularize.camelize.constantize.destroy_all
    end

    UserItems.item_list.each do |item|
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      FactoryBot.create(:activity_log, :tagged_private_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :tagged_internal_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :tagged_public_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :untagged_private_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :untagged_internal_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :untagged_public_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      visit user_path(@owner)
      expect(page).to have_content('Recent Activity')
      expect(page).to have_content("tagged #{item.singularize.humanize.titleize} with myTag", maximum: 4)
      expect(page).to have_content("untagged #{item.singularize.humanize.titleize} with myTag", maximum: 2)
      item.singularize.camelize.constantize.destroy_all
    end

    UserItems.subitem_list.each do |item|
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      item1 = FactoryBot.create(:activity_log, :created_private_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private).id, record_type: item.singularize.camelize)
      item2 = FactoryBot.create(:activity_log, :created_internal_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal).id, record_type: item.singularize.camelize)
      item3 = FactoryBot.create(:activity_log, :created_public_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public).id, record_type: item.singularize.camelize)
      item4 = FactoryBot.create(:activity_log, :edited_private_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private).id, record_type: item.singularize.camelize)
      item5 = FactoryBot.create(:activity_log, :edited_internal_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal).id, record_type: item.singularize.camelize)
      item6 = FactoryBot.create(:activity_log, :edited_public_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public).id, record_type: item.singularize.camelize)
      item1.record.parent.update(user: @owner)
      item2.record.parent.update(user: @owner)
      item3.record.parent.update(user: @owner)
      item4.record.parent.update(user: @owner)
      item5.record.parent.update(user: @owner)
      item6.record.parent.update(user: @owner)
      visit user_path(@owner)
      expect(page).to have_content('Recent Activity')
      expect(page).to have_content("created #{item.singularize.humanize.titleize} on", maximum: 2)
      expect(page).to have_content("edited #{item.singularize.humanize.titleize} on", maximum: 2)
      item.singularize.camelize.constantize.destroy_all
    end
  end

  scenario 'not logged in' do
    visit user_path(@owner)
    visit user_path(@owner)
    UserItems.item_list.each do |item|
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      FactoryBot.create(:activity_log, :created_private_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :created_internal_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :created_public_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :edited_private_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :edited_internal_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :edited_public_item_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      visit user_path(@owner)
      expect(page).to have_content('Recent Activity')
      expect(page).to have_content("created #{item.singularize.humanize.titleize} on", maximum: 1)
      expect(page).to have_content("edited #{item.singularize.humanize.titleize} on", maximum: 1)
      item.singularize.camelize.constantize.destroy_all
    end

    UserItems.item_list.each do |item|
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      FactoryBot.create(:activity_log, :tagged_private_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :tagged_internal_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :tagged_public_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :untagged_private_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :untagged_internal_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :untagged_public_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      visit user_path(@owner)
      expect(page).to have_content('Recent Activity')
      expect(page).to have_content("tagged #{item.singularize.humanize.titleize} with myTag", maximum: 2)
      expect(page).to have_content("untagged #{item.singularize.humanize.titleize} with myTag", maximum: 1)
      item.singularize.camelize.constantize.destroy_all
    end

    UserItems.subitem_list.each do |item|
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      item1 = FactoryBot.create(:activity_log, :created_private_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private).id, record_type: item.singularize.camelize)
      item2 = FactoryBot.create(:activity_log, :created_internal_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal).id, record_type: item.singularize.camelize)
      item3 = FactoryBot.create(:activity_log, :created_public_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public).id, record_type: item.singularize.camelize)
      item4 = FactoryBot.create(:activity_log, :edited_private_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private).id, record_type: item.singularize.camelize)
      item5 = FactoryBot.create(:activity_log, :edited_internal_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal).id, record_type: item.singularize.camelize)
      item6 = FactoryBot.create(:activity_log, :edited_public_subitem_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public).id, record_type: item.singularize.camelize)
      item1.record.parent.update(user: @owner)
      item2.record.parent.update(user: @owner)
      item3.record.parent.update(user: @owner)
      item4.record.parent.update(user: @owner)
      item5.record.parent.update(user: @owner)
      item6.record.parent.update(user: @owner)
      visit user_path(@owner)
      expect(page).to have_content('Recent Activity')
      expect(page).to have_content("created #{item.singularize.humanize.titleize} on", maximum: 1)
      expect(page).to have_content("edited #{item.singularize.humanize.titleize} on", maximum: 2)
      item.singularize.camelize.constantize.destroy_all
    end
  end

  scenario 'as admin when user hides following' do
    sign_in @admin
    @owner.update(hide_relationships: true)
    FactoryBot.create(:activity_log, :follow_activity, user_id: @owner.id)
    FactoryBot.create(:activity_log, :unfollow_activity, user_id: @owner.id)
    visit user_path(@owner)
    expect(page).to have_content("followed @")
    expect(page).to have_content("unfollowed @")
  end

  scenario 'as owner when user hides following' do
    sign_in @owner
    @owner.update(hide_relationships: true)
    FactoryBot.create(:activity_log, :follow_activity, user_id: @owner.id)
    FactoryBot.create(:activity_log, :unfollow_activity, user_id: @owner.id)
    visit user_path(@owner)
    expect(page).to have_content("followed @")
    expect(page).to have_content("unfollowed @")
  end

  scenario 'as user when user hides following' do
    sign_in @user
    FactoryBot.create(:activity_log, :follow_activity, user_id: @owner.id)
    FactoryBot.create(:activity_log, :unfollow_activity, user_id: @owner.id)
    visit user_path(@owner)
    expect(page).to have_content("followed @")
    expect(page).to have_content("unfollowed @")
    @owner.update(hide_relationships: true)
    visit user_path(@owner)
    expect(page).to_not have_content("followed @")
  end

  scenario 'not logged in when user hides following' do
    FactoryBot.create(:activity_log, :follow_activity, user_id: @owner.id)
    FactoryBot.create(:activity_log, :unfollow_activity, user_id: @owner.id)
    visit user_path(@owner)
    expect(page).to have_content("followed @")
    expect(page).to have_content("unfollowed @")
    @owner.update(hide_relationships: true)
    visit user_path(@owner)
    expect(page).to_not have_content("followed @")
  end

  scenario 'as admin when user hides stars' do
    sign_in @admin
    UserItems.item_list.each do |item|
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      FactoryBot.create(:activity_log, :starred_private_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :starred_internal_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :starred_public_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :unstarred_private_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :unstarred_internal_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :unstarred_public_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      @owner.update(hide_stars: true)
      visit user_path(@owner)
      expect(page).to have_content('Recent Activity')
      expect(page).to have_content("starred #{item.singularize.humanize.titleize} on", minimum: 3)
      expect(page).to have_content("unstarred #{item.singularize.humanize.titleize} on", minimum: 3)
      item.singularize.camelize.constantize.destroy_all
    end
  end

  scenario 'as owner when user hides stars' do
    sign_in @owner
    UserItems.item_list.each do |item|
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      FactoryBot.create(:activity_log, :starred_private_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :starred_internal_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :starred_public_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :unstarred_private_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :unstarred_internal_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :unstarred_public_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      @owner.update(hide_stars: true)
      visit user_path(@owner)
      expect(page).to have_content('Recent Activity')
      expect(page).to have_content("starred #{item.singularize.humanize.titleize} on", minimum: 3)
      expect(page).to have_content("unstarred #{item.singularize.humanize.titleize} on", minimum: 3)
      item.singularize.camelize.constantize.destroy_all
    end
  end

  scenario 'as user when user hides stars' do
    sign_in @user
    UserItems.item_list.each do |item|
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      FactoryBot.create(:activity_log, :starred_private_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :starred_internal_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :starred_public_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :unstarred_private_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :unstarred_internal_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :unstarred_public_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      visit user_path(@owner)
      expect(page).to have_content('Recent Activity')
      expect(page).to have_content("starred #{item.singularize.humanize.titleize} on", maximum: 4)
      expect(page).to have_content("unstarred #{item.singularize.humanize.titleize} on", maximum: 2)
      @owner.update(hide_stars: true)
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      expect(page).to_not have_content("starred #{item.singularize.humanize.titleize} on")
      expect(page).to_not have_content("unstarred #{item.singularize.humanize.titleize} on")
      item.singularize.camelize.constantize.destroy_all
    end
  end

  scenario 'not logged in when user hides stars' do
    @owner.update(hide_stars: true)
    UserItems.item_list.each do |item|
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      FactoryBot.create(:activity_log, :starred_private_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :starred_internal_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :starred_public_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :unstarred_private_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :private, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :unstarred_internal_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :internal, user_id: @owner.id).id, record_type: item.singularize.camelize)
      FactoryBot.create(:activity_log, :unstarred_public_activity, user_id: @owner.id, record_id: FactoryBot.create(item.singularize, :public, user_id: @owner.id).id, record_type: item.singularize.camelize)
      visit user_path(@owner)
      expect(page).to have_content('Recent Activity')
      expect(page).to have_content("starred #{item.singularize.humanize.titleize} on", maximum: 2)
      expect(page).to have_content("unstarred #{item.singularize.humanize.titleize} on", maximum: 1)
      @owner.update(hide_stars: true)
      visit user_path(@owner)
      expect(page).to have_content('No Recent Activity')
      expect(page).to_not have_content("starred #{item.singularize.humanize.titleize} on")
      expect(page).to_not have_content("unstarred #{item.singularize.humanize.titleize} on")
      item.singularize.camelize.constantize.destroy_all
    end
  end
end

