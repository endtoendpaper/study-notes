require 'rails_helper'

describe 'Toggle owned decks', type: :system do
  before(:each) do
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @group1 = FactoryBot.create(:group)
    @group1.add_member(@owner, @group1.user)
    @group2 = FactoryBot.create(:group, user_id: @owner.id)

    UserItems.item_list.each do |item|
      instance_variable_set("@#{item.singularize}_public_own", FactoryBot.create(item.singularize, :public, user_id: @owner.id))
      instance_variable_set("@#{item.singularize}_internal_own", FactoryBot.create(item.singularize, :internal, user_id: @owner.id))
      instance_variable_set("@#{item.singularize}_private_own", FactoryBot.create(item.singularize, :private, user_id: @owner.id))

      instance_variable_set("@#{item.singularize}_public_colb", FactoryBot.create(item.singularize, :public))
      instance_variable_set("@#{item.singularize}_internal_colb", FactoryBot.create(item.singularize, :internal))
      instance_variable_set("@#{item.singularize}_private_colb", FactoryBot.create(item.singularize, :private))

      instance_variable_get("@#{item.singularize}_public_colb").collaborate(@owner, instance_variable_get("@#{item.singularize}_public_colb").user)
      instance_variable_get("@#{item.singularize}_internal_colb").collaborate(@owner, instance_variable_get("@#{item.singularize}_internal_colb").user)
      instance_variable_get("@#{item.singularize}_private_colb").collaborate(@owner, instance_variable_get("@#{item.singularize}_private_colb").user)

      instance_variable_set("@#{item.singularize}_public_group_own", FactoryBot.create(item.singularize, :public))
      instance_variable_set("@#{item.singularize}_internal_group_own", FactoryBot.create(item.singularize, :internal))
      instance_variable_set("@#{item.singularize}_private_group_own", FactoryBot.create(item.singularize, :private))

      instance_variable_get("@#{item.singularize}_public_group_own").add_group(@group2, instance_variable_get("@#{item.singularize}_public_group_own").user)
      instance_variable_get("@#{item.singularize}_internal_group_own").add_group(@group2, instance_variable_get("@#{item.singularize}_internal_group_own").user)
      instance_variable_get("@#{item.singularize}_private_group_own").add_group(@group2, instance_variable_get("@#{item.singularize}_private_group_own").user)

      instance_variable_set("@#{item.singularize}_public_group_memb", FactoryBot.create(item.singularize, :public))
      instance_variable_set("@#{item.singularize}_internal_group_memb", FactoryBot.create(item.singularize, :internal))
      instance_variable_set("@#{item.singularize}_private_group_memb", FactoryBot.create(item.singularize, :private))

      instance_variable_get("@#{item.singularize}_public_group_memb").add_group(@group1, instance_variable_get("@#{item.singularize}_public_group_memb").user)
      instance_variable_get("@#{item.singularize}_internal_group_memb").add_group(@group1, instance_variable_get("@#{item.singularize}_internal_group_memb").user)
      instance_variable_get("@#{item.singularize}_private_group_memb").add_group(@group1, instance_variable_get("@#{item.singularize}_private_group_memb").user)
    end
  end

  scenario 'as admin', js: true do
    sign_in @admin
    UserItems.item_list.each do |item|
      visit '/users/' + @owner.username + "/#{item}"
      expect(page).to have_content("Show owned #{item}")
      page.check('view_owned')
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public_own").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal_own").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_private_own").title)

      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_public_colb").title)
      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_internal_colb").title)
      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_private_colb").title)

      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_public_group_own").title)
      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_internal_group_own").title)
      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_private_group_own").title)

      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_public_group_memb").title)
      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_internal_group_memb").title)
      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_private_group_memb").title)
    end
  end

  scenario 'as user', js: true do
    sign_in @owner
    UserItems.item_list.each do |item|
      visit '/users/' + @owner.username + "/#{item}"
      expect(page).to have_content("Show owned #{item}")
      page.check('view_owned')
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_public_own").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_internal_own").title)
      expect(page).to have_link(instance_variable_get("@#{item.singularize}_private_own").title)

      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_public_colb").title)
      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_internal_colb").title)
      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_private_colb").title)

      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_public_group_own").title)
      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_internal_group_own").title)
      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_private_group_own").title)

      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_public_group_memb").title)
      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_internal_group_memb").title)
      expect(page).to_not have_link(instance_variable_get("@#{item.singularize}_private_group_memb").title)
    end
  end

  scenario 'as another user' do
    sign_in @user
    UserItems.item_list.each do |item|
      visit '/users/' + @owner.username + "/#{item}"
      expect(page).to_not have_content("Show owned #{item}")
    end
  end

  scenario 'not logged in' do
    UserItems.item_list.each do |item|
      visit '/users/' + @owner.username + "/#{item}"
      expect(page).to_not have_content("Show owned #{item}")
    end
  end
end
