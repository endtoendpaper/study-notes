require 'rails_helper'

describe 'View user profile', type: :system do
  before(:each) do
    @user1 = FactoryBot.create(:user, :confirmed)
    @user2 = FactoryBot.create(:user, :confirmed)
    @user3 = FactoryBot.create(:user, :confirmed, :hide_relationships)
    @admin = FactoryBot.create(:user, :admin, :confirmed)
  end

  scenario 'as the user' do
    sign_in @user1
    visit user_path(@user1)
    expect(page).to have_link('Edit your profile')
    expect(page).to_not have_link('Edit user')
    expect(page).to have_content(@user1.handle)
    expect(page).to_not have_content('Email: ' + @user1.email)
    expect(page).to_not have_content('Darkmode:')
    expect(page).to_not have_content('Timezone:')
    expect(page).to_not have_content('Sign in count:')
    expect(page).to_not have_content('Current sign in:')
    expect(page).to_not have_content('Last sign in:')
    expect(page).to_not have_content('Current sign in IP:')
    expect(page).to_not have_content('Last sign in IP:')
    expect(page).to_not have_content('Confirmation sent at:')
    expect(page).to_not have_content('Confirmed at:')
    expect(page).to_not have_content('Unconfirmed email:')
    expect(page).to_not have_content('Failed login attemps:')
    expect(page).to_not have_content('Unlocked at:')
    expect(html).to_not include('<img src=')
  end

  scenario 'as another user' do
    sign_in @user2
    visit user_path(@user1)
    expect(page).to_not have_link('Edit your profile')
    expect(page).to_not have_link('Edit user')
    expect(page).to have_content(@user1.handle)
    expect(page).to_not have_content('Email: ' + @user1.email)
    expect(page).to_not have_content('Darkmode:')
    expect(page).to_not have_content('Timezone:')
    expect(page).to_not have_content('Sign in count:')
    expect(page).to_not have_content('Current sign in:')
    expect(page).to_not have_content('Last sign in:')
    expect(page).to_not have_content('Current sign in IP:')
    expect(page).to_not have_content('Last sign in IP:')
    expect(page).to_not have_content('Confirmation sent at:')
    expect(page).to_not have_content('Confirmed at:')
    expect(page).to_not have_content('Unconfirmed email:')
    expect(page).to_not have_content('Failed login attemps:')
    expect(page).to_not have_content('Unlocked at:')
    expect(html).to_not include('<img src=')
  end

  scenario 'as admin' do
    sign_in @admin
    visit user_path(@user1)
    expect(page).to_not have_link('Edit your profile')
    expect(page).to have_link('Edit user')
    expect(page).to have_content(@user1.handle)
    expect(page).to have_content('Email: ' + @user1.email)
    expect(page).to have_content('Darkmode:')
    expect(page).to have_content('Timezone:')
    expect(page).to have_content('Sign in count:')
    expect(page).to have_content('Current sign in:')
    expect(page).to have_content('Last sign in:')
    expect(page).to have_content('Current sign in IP:')
    expect(page).to have_content('Last sign in IP:')
    expect(page).to have_content('Confirmation sent at:')
    expect(page).to have_content('Confirmed at:')
    expect(page).to have_content('Unconfirmed email:')
    expect(page).to have_content('Failed login attemps:')
    expect(page).to have_content('Unlocked at:')
    expect(html).to_not include('<img src=')
  end

  scenario 'not logged in' do
    visit user_path(@user1)
    expect(page).to_not have_link('Edit your profile')
    expect(page).to_not have_link('Edit user')
    expect(page).to have_content(@user1.handle)
    expect(page).to_not have_content('Email: ' + @user1.email)
    expect(page).to_not have_content('Darkmode:')
    expect(page).to_not have_content('Timezone:')
    expect(page).to_not have_content('Sign in count:')
    expect(page).to_not have_content('Current sign in:')
    expect(page).to_not have_content('Last sign in:')
    expect(page).to_not have_content('Current sign in IP:')
    expect(page).to_not have_content('Last sign in IP:')
    expect(page).to_not have_content('Confirmation sent at:')
    expect(page).to_not have_content('Confirmed at:')
    expect(page).to_not have_content('Unconfirmed email:')
    expect(page).to_not have_content('Failed login attemps:')
    expect(page).to_not have_content('Unlocked at:')
    expect(html).to_not include('<img src=')
  end

  scenario 'when the site settings have default avatar' do
    SiteSetting.first.update(default_avatar: Rack::Test::UploadedFile.new('spec/factories/images/default-avatar.jpg'))
    visit user_path(@user1)
    expect(page).to_not have_link('Edit your profile')
    expect(page).to_not have_link('Edit user')
    expect(page).to have_content(@user1.handle)
    expect(html).to include('<img src=')
    expect(html).to include('default_avatar-')
  end

  scenario 'as admin if user is admin' do
    sign_in @user2
    visit user_path(@admin)
    expect(page).to_not have_content('Admin')
  end

  scenario 'as another user if user is admin' do
    sign_in @admin
    visit user_path(@admin)
    expect(page).to have_content('Admin')
  end

  scenario 'as user if allow follow is enabled' do
    SiteSetting.first.update(follow_users: true)
    sign_in @user1
    visit user_path(@user2)
    expect(page).to have_link('0 following')
    expect(page).to have_link('0 followers')
    expect(page).to have_button('Follow')
  end

  scenario 'as user if allow follow is disabled' do
    SiteSetting.first.update(follow_users: false)
    sign_in @user1
    visit user_path(@user2)
    expect(page).to_not have_link('0 following')
    expect(page).to_not have_link('0 followers')
    expect(page).to_not have_button('Follow')
  end

  scenario 'not logged in if allow follow is enabled' do
    SiteSetting.first.update(follow_users: true)
    visit user_path(@user2)
    expect(page).to have_link('0 following')
    expect(page).to have_link('0 followers')
    expect(page).to_not have_button('Follow')
  end

  scenario 'not logged in if allow follow is disabled' do
    SiteSetting.first.update(follow_users: false)
    visit user_path(@user2)
    expect(page).to_not have_link('0 following')
    expect(page).to_not have_link('0 followers')
    expect(page).to_not have_button('Follow')
  end

  scenario 'as user for own profile if their hide relationships is selected' do
    SiteSetting.first.update(follow_users: true)
    sign_in @user3
    visit user_path(@user3)
    expect(page).to have_link('0 following')
    expect(page).to have_link('0 followers')
    expect(page).to_not have_button('Follow')
  end

  scenario 'as admin for user who selected hide relationships' do
    SiteSetting.first.update(follow_users: true)
    sign_in @admin
    visit user_path(@user3)
    expect(page).to have_link('0 following')
    expect(page).to have_link('0 followers')
    expect(page).to have_button('Follow')
  end

  scenario 'as user for another user who selected hide relationships' do
    SiteSetting.first.update(follow_users: true)
    sign_in @user1
    visit user_path(@user3)
    expect(page).to_not have_link('0 following')
    expect(page).to_not have_link('0 followers')
    expect(page).to have_button('Follow')
  end

  scenario 'not logged in for user who selected hide relationships' do
    SiteSetting.first.update(follow_users: true)
    sign_in @user1
    expect(page).to_not have_link('0 following')
    expect(page).to_not have_link('0 followers')
    expect(page).to_not have_button('Follow')
  end
end
