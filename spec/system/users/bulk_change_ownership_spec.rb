require 'rails_helper'

describe 'Bulk change ownership', type: :system do
  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)

    FactoryBot.create(:group, user_id: @owner.id)
    FactoryBot.create(:group, user_id: @owner.id)

    UserItems.item_list.each do |item|
      private = FactoryBot.create(item.singularize, :private, user_id: @owner.id)
      internal = FactoryBot.create(item.singularize, :internal, user_id: @owner.id)
    end
  end

  scenario 'transfers items to @user as admin', js: true do
    sign_in @admin
    visit edit_user_path(@owner)
    click_link 'Bulk transfer groups and items'
    expect(page).to have_selector('h1', text: "Bulk Transfer Ownership of User #{@owner.handle}'s Items")
    fill_in('new_user', with: @user.username)

    page.check('groups')

    UserItems.item_list.each do |item|
      page.check(item)
    end

    click_button 'Transfer'
    page.accept_alert
    expect(current_path).to include('/users/' + @owner.username)
    expect(page).to have_content("Items were successfully transferred to #{@user.handle}.")

    UserItems.item_list.each do |item|
      expect(@owner.reload.send("#{item}").count).to eq(0)
      expect(@user.reload.send("#{item}").count).to eq(2)
    end
    expect(@owner.reload.groups.count).to eq(0)
    expect(@user.reload.groups.count).to eq(2)
  end

  scenario 'does not select options as admin', js: true do
    sign_in @admin
    visit edit_user_path(@owner)
    click_link 'Bulk transfer groups and items'
    expect(page).to have_selector('h1', text: "Bulk Transfer Ownership of User #{@owner.handle}'s Items")
    fill_in('new_user', with: @user.handle)
    click_button 'Transfer'
    page.accept_alert
    expect(current_path).to include('/users/' + @owner.username + '/bulk_edit_ownership')
    expect(page).to have_content("No options selected")
    UserItems.item_list.each do |item|
      expect(@owner.reload.send("#{item}").count).to eq(2)
      expect(@user.reload.send("#{item}").count).to eq(0)
    end
    expect(@owner.reload.groups.count).to eq(2)
    expect(@user.reload.groups.count).to eq(0)
  end

  scenario 'transfers to user that does not exist as admin', js: true do
    sign_in @admin
    visit edit_user_path(@owner)
    click_link 'Bulk transfer groups and items'
    expect(page).to have_selector('h1', text: "Bulk Transfer Ownership of User #{@owner.handle}'s Items")
    fill_in('new_user', with: "notauser")

    UserItems.item_list.each do |item|
      page.check(item)
    end

    page.check('groups')

    click_button 'Transfer'
    page.accept_alert
    expect(current_path).to include('/users/' + @owner.username + '/bulk_edit_ownership')
    expect(page).to have_content("Username does not exist.")

    UserItems.item_list.each do |item|
      expect(@owner.reload.send("#{item}").count).to eq(2)
      expect(@user.reload.send("#{item}").count).to eq(0)
    end
    expect(@owner.reload.groups.count).to eq(2)
    expect(@user.reload.groups.count).to eq(0)
  end

  scenario 'transfers to the user as admin', js: true do
    sign_in @admin
    visit edit_user_path(@owner)
    click_link 'Bulk transfer groups and items'
    expect(page).to have_selector('h1', text: "Bulk Transfer Ownership of User #{@owner.handle}'s Items")
    fill_in('new_user', with: @owner.username)
    page.check('groups')

    UserItems.item_list.each do |item|
      page.check(item)
    end

    click_button 'Transfer'
    page.accept_alert
    expect(current_path).to include('/users/' + @owner.username + '/bulk_edit_ownership')
    expect(page).to have_content("User already owns items.")
    UserItems.item_list.each do |item|
      expect(@owner.reload.send("#{item}").count).to eq(2)
    end
    expect(@owner.reload.groups.count).to eq(2)
  end

  scenario 'transfers items to @user as owner', js: true do
    sign_in @owner
    visit edit_user_registration_path
    click_link 'Bulk transfer groups and items'
    expect(page).to have_selector('h1', text: "Bulk Transfer Ownership of Your Items")
    fill_in('new_user', with: @user.username)

    page.check('groups')

    UserItems.item_list.each do |item|
      page.check(item)
    end

    click_button 'Transfer'
    page.accept_alert
    expect(current_path).to include('/users/' + @owner.username)
    expect(page).to have_content("Items were successfully transferred to #{@user.handle}.")

    UserItems.item_list.each do |item|
      expect(@owner.reload.send("#{item}").count).to eq(0)
      expect(@user.reload.send("#{item}").count).to eq(2)
    end
    expect(@owner.reload.groups.count).to eq(0)
    expect(@user.reload.groups.count).to eq(2)
  end

  scenario 'does not select options as owner', js: true do
    sign_in @owner
    visit edit_user_registration_path
    click_link 'Bulk transfer groups and items'
    expect(page).to have_selector('h1', text: "Bulk Transfer Ownership of Your Items")
    fill_in('new_user', with: @user.handle)
    click_button 'Transfer'
    page.accept_alert
    expect(current_path).to include('/users/' + @owner.username + '/bulk_edit_ownership')
    expect(page).to have_content("No options selected")
    UserItems.item_list.each do |item|
      expect(@owner.reload.send("#{item}").count).to eq(2)
      expect(@user.reload.send("#{item}").count).to eq(0)
    end
    expect(@owner.reload.groups.count).to eq(2)
    expect(@user.reload.groups.count).to eq(0)
  end

  scenario 'transfers to user that does not exist as owner', js: true do
    sign_in @owner
    visit edit_user_registration_path
    click_link 'Bulk transfer groups and items'
    expect(page).to have_selector('h1', text: "Bulk Transfer Ownership of Your Items")
    fill_in('new_user', with: "notauser")

    UserItems.item_list.each do |item|
      page.check(item)
    end

    page.check('groups')

    click_button 'Transfer'
    page.accept_alert
    expect(current_path).to include('/users/' + @owner.username + '/bulk_edit_ownership')
    expect(page).to have_content("Username does not exist.")

    UserItems.item_list.each do |item|
      expect(@owner.reload.send("#{item}").count).to eq(2)
      expect(@user.reload.send("#{item}").count).to eq(0)
    end
    expect(@owner.reload.groups.count).to eq(2)
    expect(@user.reload.groups.count).to eq(0)
  end

  scenario 'transfers to the owner as owner', js: true do
    sign_in @owner
    visit edit_user_registration_path
    click_link 'Bulk transfer groups and items'
    expect(page).to have_selector('h1', text: "Bulk Transfer Ownership of Your Items")
    fill_in('new_user', with: @owner.username)
    page.check('groups')

    UserItems.item_list.each do |item|
      page.check(item)
    end

    click_button 'Transfer'
    page.accept_alert
    expect(current_path).to include('/users/' + @owner.username + '/bulk_edit_ownership')
    expect(page).to have_content("User already owns items.")
    UserItems.item_list.each do |item|
      expect(@owner.reload.send("#{item}").count).to eq(2)
    end
    expect(@owner.reload.groups.count).to eq(2)
  end
end
