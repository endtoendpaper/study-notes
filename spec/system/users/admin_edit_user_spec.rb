require 'rails_helper'

describe 'Admin edits user', type: :system do
  let(:email) { Faker::Internet.email }
  let(:password) { Faker::Internet.password(min_length: 6) }
  let(:username) { Faker::Internet.username(specifier: 1..50, separators: ['-','_','0','123','9']) }

  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @user = FactoryBot.create(:user, :confirmed)
  end

  scenario 'and changes username' do
    sign_in @admin
    visit user_path(@user)
    click_link 'Edit user'
    prev_username = @user.username
    fill_in('user_username', with: 'my_new_name', fill_options: { clear: :backspace })
    click_button 'Update'
    expect(page).to have_content('@my_new_name')
    expect(page).to have_content('User was successfully updated.')
    expect(page).to_not have_content(prev_username)
    '/users/' + @user.username
    expect(@user.reload.admin).to be false
  end

  scenario 'and makes them admin' do
    sign_in @admin
    visit edit_user_path(@user)
    page.check('Admin')
    click_button 'Update'
    expect(page).to have_content('Admin')
    expect(page).to have_content('User was successfully updated.')
    '/users/' + @user.username
    expect(@user.reload.admin).to be true
  end

  scenario 'adding valid avatar' do
    sign_in @admin
    visit edit_user_path(@user)
    expect(page).to have_content('Avatar - Not set')
    attach_file('user_avatar', File.absolute_path('./spec/factories/images/crying-cat.jpeg'))
    click_button 'Update'
    expect(page).to have_content('User was successfully updated.')
    '/users/' + @user.username
    expect(html).to include('crying-cat.jpeg')
    expect(html).to include('<img src=')
  end

  scenario 'switching avatar' do
    user = FactoryBot.create(:user, :confirmed, :avatar)
    sign_in @admin
    visit edit_user_path(user)
    expect(page).to_not have_content('Avatar - Not set')
    attach_file('user_avatar', File.absolute_path('./spec/factories/images/surprised-cat.png'))
    click_button 'Update'
    expect(page).to have_content('User was successfully updated.')
    expect(current_path).to include('/users/' + user.username)
    expect(html).to include('surprised-cat.png')
    expect(html).to include('<img src=')
  end

  scenario 'removing avatar', js: true do
    user = FactoryBot.create(:user, :confirmed, :avatar)
    sign_in @admin
    visit edit_user_path(user)
    expect(html).to include('crying-cat.jpeg')
    expect(html).to include('<img src=')
    expect(page).to_not have_content('Avatar - Not set')
    click_link 'Remove avatar'
    page.accept_alert
    expect(current_path).to include('/users/' + user.reload.username + '/edit')
    expect(page).to have_content('Avatar - Not set')
  end

  scenario 'with invalid username', js: true do
    sign_in @admin
    visit edit_user_path(@user)
    prev_username = @user.username
    fill_in('user_username', with: 'bad name', fill_options: { clear: :backspace })
    click_button 'Update'
    expect(page).to have_content('1 error prohibited this user from being saved:')
    expect(page).to have_content('Username can only contain letters, numbers, dashes, or underscores')
    expect(current_path).to include('/users/' + @user.username + '/edit')
  end

  scenario 'with no username', js: true do
    sign_in @admin
    visit edit_user_path(@user)
    prev_username = @user.username
    fill_in('user_username', with: '', fill_options: { clear: :backspace })
    click_button 'Update'
    expect(page).to have_content('1 error prohibited this user from being saved:')
    expect(page).to have_content("Username can't be blank")
    expect(current_path).to include('/users/' + @user.username + '/edit')
  end

  scenario 'and destroys user', js: true do
    sign_in @admin
    visit edit_user_path(@user)
    click_button 'Delete User'
    page.accept_alert
    expect(current_path).to include('/users')
    expect(page).to have_content('User was successfully deleted.')
    visit @users_path
    expect(page).to_not have_content(@user.username)
  end
end
