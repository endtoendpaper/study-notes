require 'rails_helper'

describe 'Views user followers page', type: :system do
  before(:each) do
    @user1 = FactoryBot.create(:user, :confirmed, :avatar)
    @user2 = FactoryBot.create(:user, :confirmed)
    @user3 = FactoryBot.create(:user, :confirmed)
    @user4 = FactoryBot.create(:user, :confirmed, :hide_relationships)
    @user5 = FactoryBot.create(:user, :confirmed, :hide_relationships)
    @user6 = FactoryBot.create(:user, :confirmed, :hide_relationships)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @user2.follow(@user1)
    @user3.follow(@user1)
    @user4.follow(@user1)
    @user5.follow(@user1)
    @user6.follow(@user1)
  end

  scenario 'as admin' do
    sign_in @admin
    visit followers_user_path(@user1)
    expect(page).to have_link('0 following', minimum: 1)
    expect(page).to have_link('1 following', minimum: 5)
    expect(page).to have_link('5 followers', minimum: 1)
    expect(page).to have_link('0 followers', minimum: 5)
    expect(page).to_not have_button('Unfollow')
    expect(page).to have_button('Follow', minimum: 6)
    expect(page).to_not have_content('Email:')
    expect(page).to_not have_content(@user1.email)
    expect(page).to_not have_content(@user2.email)
  end

  scenario 'as user' do
    sign_in @user4
    visit followers_user_path(@user1)
    expect(page).to have_link('0 following', minimum: 1)
    expect(page).to have_link('1 following', minimum: 2)
    expect(page).to have_link('5 followers', minimum: 1)
    expect(page).to have_link('0 followers', minimum: 2)
    expect(page).to have_button('Unfollow')
    expect(page).to have_button('Follow', minimum: 2)
    expect(page).to_not have_content('Email:')
    expect(page).to_not have_content(@user1.email)
    expect(page).to_not have_content(@user2.email)
  end

  scenario 'sees pagination with >5 users' do
    FactoryBot.create(:user, :confirmed, :admin).follow(@user1)
    sign_in @admin
    visit followers_user_path(@user1)
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end
end
