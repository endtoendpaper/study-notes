require 'rails_helper'

describe 'User logs out', type: :system do
  scenario 'after logging in', js: true do
    user = FactoryBot.create(:user, :confirmed)
    sign_in user
    visit root_path
    expect(page).to have_css('.dropdown-toggle i.bi-person-circle', minimum: 1)
    find(:css, 'i.bi-person-circle').click
    click_link 'Logout'
    expect(page).to_not have_css('.dropdown-toggle i.bi-person-circle')
  end
end
