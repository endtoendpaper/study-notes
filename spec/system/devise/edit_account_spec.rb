require 'rails_helper'

describe 'User edits account', type: :system do
  let(:password) { Faker::Internet.password(min_length: 6) }
  let(:username) { Faker::Internet.username(specifier: 1..50, separators: ['-','_','0','123','9']) }

  scenario 'username with valid data' do
    user = FactoryBot.create(:user, :confirmed)
    sign_in user
    visit edit_user_registration_path
    expect(page).to have_selector('h1', text: 'Edit')
    username_before = user.username
    fill_in('user_username', with: username,
                             fill_options: { clear: :backspace })
    click_button 'Update'
    expect(page).to have_content('Your account has been updated successfully.')
    expect(current_path).to eq('/')
    expect(page).to have_css('.dropdown-toggle i.bi-person-circle', minimum: 1)
    user.reload
    expect(username_before).to_not eq(user.username)
    expect(user.username).to eq(username)
  end

  scenario 'password with valid data' do
    user = FactoryBot.create(:user, :confirmed, password: 'my_password')
    sign_in user
    visit edit_user_registration_path
    expect(page).to have_selector('h1', text: 'Edit')
    pw_before = user.password
    fill_in 'user_password', with: password
    fill_in 'user_password_confirmation', with: password
    fill_in 'user_current_password', with: 'my_password'
    expect do
      click_button 'Update'
    end.to change(Devise.mailer.deliveries, :count).by(1)
    expect(Devise.mailer.deliveries.last.to).to include(user.email)
    expect(Devise.mailer.deliveries.last.subject).to eq('Password Changed')
    expect(page).to have_content('Your account has been updated successfully.')
    expect(current_path).to eq('/')
    expect(page).to have_css('.dropdown-toggle i.bi-person-circle', minimum: 1)
    expect(Devise.mailer.deliveries.count).to eq 1
  end

  scenario 'password with mismatched password' do
    user = FactoryBot.create(:user, :confirmed, password: 'my_password')
    sign_in user
    visit edit_user_registration_path
    expect(page).to have_selector('h1', text: 'Edit')
    fill_in 'user_password', with: password
    fill_in 'user_password_confirmation', with: password + 'extra_char'
    fill_in 'user_current_password', with: 'my_password' #correct pw
    expect do
      click_button 'Update'
    end.to change(Devise.mailer.deliveries, :count).by(0)
    expect(page).to have_css('.dropdown-toggle i.bi-person-circle', minimum: 1)
    expect(page).to have_content('1 error prohibited this user from being saved:')
    expect(page).to have_content("Password confirmation doesn't match Password")
    expect(current_path).to eq('/users')
  end

  scenario 'password with incorrect current password' do
    user = FactoryBot.create(:user, :confirmed, password: 'my_password')
    sign_in user
    visit edit_user_registration_path
    expect(page).to have_selector('h1', text: 'Edit')
    fill_in 'user_password', with: password
    fill_in 'user_password_confirmation', with: password
    fill_in 'user_current_password', with: 'not_my_password'
    expect do
      click_button 'Update'
    end.to change(Devise.mailer.deliveries, :count).by(0)
    expect(page).to have_content('1 error prohibited this user from being saved:')
    expect(page).to have_content('Current password is invalid')
    expect(current_path).to eq('/users')
    expect(page).to have_css('.dropdown-toggle i.bi-person-circle', minimum: 1)
  end

  scenario 'and cancels account', js: true do
    user = FactoryBot.create(:user, :confirmed)
    user_email = user.email
    sign_in user
    visit edit_user_registration_path
    expect(page).to have_selector('h1', text: 'Edit')
    click_button 'Cancel My Account'
    page.accept_alert
    expect(page).to have_content('Bye! Your account has been successfully canceled. We hope to see you again soon.')
    expect(current_path).to eq('/')
    expect(page).to_not have_css('.dropdown-toggle i.bi-person-circle')
  end

  scenario 'without logging in' do
    visit edit_user_registration_path
    expect(page).to have_selector('h1', text: 'Login')
    expect(page).to have_content('You need to sign in or sign up before continuing.')
    expect(current_path).to eq('/users/sign_in')
    expect(page).to_not have_css('.dropdown-toggle i.bi-person-circle')
  end

  scenario 'adding valid avatar' do
    user = FactoryBot.create(:user, :confirmed)
    sign_in user
    visit edit_user_registration_path
    expect(page).to have_css('.dropdown-toggle i.bi-person-circle', minimum: 1)
    expect(page).to have_selector('h1', text: 'Edit')
    expect(html).to_not include('crying-cat.jpeg')
    expect(html).to_not include('<img src=')
    expect(page).to have_content('Avatar - Not set')
    attach_file('user_avatar', File.absolute_path('./spec/factories/images/crying-cat.jpeg'))
    click_button 'Update'
    expect(page).to have_content('Your account has been updated successfully.')
    expect(current_path).to eq('/')
    expect(page).to_not have_css('.dropdown-toggle i.bi-person-circle')
    expect(html).to include('crying-cat.jpeg')
    expect(html).to include('<img src=')
  end

  scenario 'removes avatar', js: true do
    user = FactoryBot.create(:user, :confirmed, :avatar)
    sign_in user
    visit edit_user_registration_path
    expect(html).to include('crying-cat.jpeg')
    expect(html).to include('<img src=')
    expect(page).to have_selector('h1', text: 'Edit')
    expect(page).to_not have_content('Avatar - Not set')
    click_link 'Remove avatar'
    page.accept_alert
    expect(current_path).to eq('/users/edit')
    expect(page).to have_css('.dropdown-toggle i.bi-person-circle', minimum: 1)
    expect(html).to_not include('crying-cat.jpeg')
    expect(html).to_not include('<img src=')
  end

  scenario 'switching avatar' do
    user = FactoryBot.create(:user, :confirmed, :avatar)
    sign_in user
    visit edit_user_registration_path
    expect(page).to have_selector('h1', text: 'Edit')
    expect(page).to_not have_content('Avatar - Not set')
    expect(html).to include('crying-cat.jpeg')
    expect(html).to include('<img src=')
    attach_file('user_avatar', File.absolute_path('./spec/factories/images/surprised-cat.png'))
    click_button 'Update'
    expect(page).to have_content('Your account has been updated successfully.')
    expect(current_path).to eq('/')
    expect(page).to_not have_css('.dropdown-toggle i.bi-person-circle')
    expect(html).to include('surprised-cat.png')
    expect(html).to include('<img src=')
  end

  scenario 'makes password visible', js: true do
    user = FactoryBot.create(:user, :confirmed)
    sign_in user
    visit edit_user_registration_path
    expect(page).to_not have_css('.bi-eye-slash', visible: true)
    expect(page).to have_css('.bi-eye', minimum: 3, visible: true)
    find(".bi-eye", match: :first).click
    expect(page).to have_css('.bi-eye-slash', maximum: 1, visible: true)
    expect(page).to have_css('.bi-eye', maximum: 2, visible: true)
    find(".bi-eye-slash", match: :first).click
    expect(page).to_not have_css('.bi-eye-slash', visible: true)
    expect(page).to have_css('.bi-eye', minimum: 3, visible: true)
  end
end
