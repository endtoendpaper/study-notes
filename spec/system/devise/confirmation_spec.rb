require 'rails_helper'

describe 'User confirms account', type: :system do
  scenario 'with valid data' do
    user = FactoryBot.create(:user)
    expect(Devise.mailer.deliveries.count).to eq 1
    expect do
      confirmation_token = user.send_confirmation_instructions
    end.to change(Devise.mailer.deliveries, :count).by(1)
    expect(Devise.mailer.deliveries.last.to).to include(user.email)
    expect(Devise.mailer.deliveries.last.subject).to eq('Confirmation instructions')
    expect(Devise.mailer.deliveries.last.body).to include(user.confirmation_token)
    url = user_confirmation_path
    visit url + '?confirmation_token=' + user.confirmation_token
    expect(current_path).to eq('/users/sign_in')
    expect(page).to have_selector('h1', text: 'Login')
    expect(page).to have_content('Your email address has been successfully confirmed.')
    expect(page).to have_button('Login')
    expect(page).to have_link('Forgot your password?')
    expect(page).to have_link("Didn't receive confirmation instructions?")
    expect(page).to have_link("Didn't receive unlock instructions?")
    expect(page).to have_button('Google')
    expect(page).to_not have_css('.dropdown-toggle i.bi-person-circle')
  end

  scenario 'over 3 days after confirmation email sent' do
    user = FactoryBot.create(:user)
    expect(Devise.mailer.deliveries.count).to eq 1
    expect do
      confirmation_token = user.send_confirmation_instructions
    end.to change(Devise.mailer.deliveries, :count).by(1)
    user.update(confirmation_sent_at: Time.current - 4.days)
    url = user_confirmation_path
    visit url + '?confirmation_token=' + user.confirmation_token
    expect(current_path).to eq('/users/confirmation')
    expect(page).to have_selector('h1', text: 'Resend Confirmation Instructions')
    expect(page).to have_content('Email needs to be confirmed within 3 days, please request a new one')
    expect(page).to have_button('Resend Confirmation Instructions')
    expect(page).to have_link('Forgot your password?')
    expect(page).to have_link("Didn't receive unlock instructions?")
    expect(page).to_not have_css('.dropdown-toggle i.bi-person-circle')
  end

  scenario 'with invalid confirmation_token' do
    user = FactoryBot.create(:user)
    expect(Devise.mailer.deliveries.count).to eq 1
    expect do
      confirmation_token = user.send_confirmation_instructions
    end.to change(Devise.mailer.deliveries, :count).by(1)
    user.update(confirmation_sent_at: Time.current - 4.days)
    url = user_confirmation_path
    visit url + '?confirmation_token=' + '1111111'
    expect(current_path).to eq('/users/confirmation')
    expect(page).to have_selector('h1', text: 'Resend Confirmation Instructions')
    expect(page).to have_content('1 error prohibited this user from being saved:')
    expect(page).to have_content('Confirmation token is invalid')
    expect(page).to have_link('Login')
    expect(page).to have_link('Forgot your password?')
    expect(page).to have_link("Didn't receive unlock instructions?")
    expect(page).to_not have_css('.dropdown-toggle i.bi-person-circle')
  end

  scenario 'without confirmation_token' do
    user = FactoryBot.create(:user)
    expect(Devise.mailer.deliveries.count).to eq 1
    expect do
      confirmation_token = user.send_confirmation_instructions
    end.to change(Devise.mailer.deliveries, :count).by(1)
    user.update(confirmation_sent_at: Time.current - 4.days)
    url = user_confirmation_path
    visit url + '?confirmation_token=' + ''
    expect(current_path).to eq('/users/confirmation')
    expect(page).to have_selector('h1', text: 'Resend Confirmation Instructions')
    expect(page).to have_content('1 error prohibited this user from being saved:')
    expect(page).to have_content("Confirmation token can't be blank")
    expect(page).to have_link('Login')
    expect(page).to have_link('Forgot your password?')
    expect(page).to have_link("Didn't receive unlock instructions?")
    expect(page).to_not have_css('.dropdown-toggle i.bi-person-circle')
  end
end
