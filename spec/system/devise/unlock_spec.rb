require 'rails_helper'

describe 'User unlocks account', type: :system do
  scenario 'after 1 hour' do
    user = FactoryBot.create(:user, :confirmed, :locked)
    user.update(locked_at: Time.zone.now - 1.hours)
    visit new_user_session_path
    fill_in 'user_login', with: user.email
    fill_in 'user_password', with: user.password
    click_button 'Login'
    expect(page).to have_content('Signed in successfully.')
    expect(current_path).to eq('/')
    expect(page).to have_css('.dropdown-toggle i.bi-person-circle', minimum: 1)
    expect(Devise.mailer.deliveries.count).to eq 0
  end

  scenario 'with valid unlock token' do
    user = FactoryBot.create(:user, :confirmed)
    expect(Devise.mailer.deliveries.count).to eq 0
    unlock_token = user.lock_access!
    user.reload
    expect(Devise.mailer.deliveries.count).to eq 1
    expect(Devise.mailer.deliveries.last.to).to include(user.email)
    expect(Devise.mailer.deliveries.last.subject).to eq('Unlock instructions')
    visit user_unlock_path(unlock_token: unlock_token)
    expect(page).to have_content('Your account has been unlocked successfully. Please sign in to continue.')
    expect(current_path).to eq('/users/sign_in')
    expect(page).to_not have_css('.dropdown-toggle i.bi-person-circle')
    expect(Devise.mailer.deliveries.count).to eq 1
  end

  scenario 'with invalid unlock token' do
    user = FactoryBot.create(:user, :confirmed)
    expect(Devise.mailer.deliveries.count).to eq 0
    unlock_token = user.lock_access!
    user.reload
    expect(Devise.mailer.deliveries.count).to eq 1
    expect(Devise.mailer.deliveries.last.to).to include(user.email)
    expect(Devise.mailer.deliveries.last.subject).to eq('Unlock instructions')
    visit user_unlock_path(unlock_token: '11111111')
    expect(page).to have_content('1 error prohibited this user from being saved:')
    expect(page).to have_content('Unlock token is invalid')
    expect(current_path).to eq('/users/unlock')
    expect(page).to_not have_css('.dropdown-toggle i.bi-person-circle')
    expect(Devise.mailer.deliveries.count).to eq 1
  end

  scenario 'with no unlock token' do
    user = FactoryBot.create(:user, :confirmed)
    expect(Devise.mailer.deliveries.count).to eq 0
    unlock_token = user.lock_access!
    user.reload
    expect(Devise.mailer.deliveries.last.to).to include(user.email)
    expect(Devise.mailer.deliveries.last.subject).to eq('Unlock instructions')
    visit user_unlock_path
    expect(page).to have_content('1 error prohibited this user from being saved:')
    expect(page).to have_content("Unlock token can't be blank")
    expect(current_path).to eq('/users/unlock')
    expect(page).to_not have_css('.dropdown-toggle i.bi-person-circle')
    expect(Devise.mailer.deliveries.count).to eq 1
  end

  scenario 'already signed in' do
    user = FactoryBot.create(:user, :confirmed)
    expect(Devise.mailer.deliveries.count).to eq 0
    sign_in user
    visit user_unlock_path
    expect(page).to have_content('You are already signed in.')
    expect(current_path).to eq('/')
    expect(page).to have_css('.dropdown-toggle i.bi-person-circle', minimum: 1)
    expect(Devise.mailer.deliveries.count).to eq 0
  end
end
