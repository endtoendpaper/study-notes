require 'rails_helper'

describe 'User signs in', type: :system do
  let(:email) { Faker::Internet.email }
  let(:password) { Faker::Internet.password(min_length: 6) }
  let(:username) { Faker::Internet.username(specifier: 1..50, separators: ['-','_','0','123','9']) }

  before(:each) do
    visit new_user_session_path
  end

  scenario 'with correct email credentials' do
    SiteSetting.first.update(users_can_join: true)
    expect(page).to have_selector('h1', text: 'Login')
    expect(page).to have_link('Sign up')
    expect(page).to have_link('Forgot your password?')
    expect(page).to have_link("Didn't receive confirmation instructions?")
    expect(page).to have_link("Didn't receive unlock instructions?")
    expect(page).to have_button('Google')
    user = FactoryBot.create(:user, :confirmed)
    fill_in 'user_login', with: user.email
    fill_in 'user_password', with: user.password
    click_button 'Login'
    expect(page).to have_content('Signed in successfully.')
    expect(current_path).to eq('/')
    expect(page).to have_css('.dropdown-toggle i.bi-person-circle', minimum: 1)
    expect(page).to_not have_css('.dropdown-toggle i.bi-gear-fill')
    expect(Devise.mailer.deliveries.count).to eq 0
  end

  scenario 'with correct username credentials' do
    SiteSetting.first.update(users_can_join: true)
    expect(page).to have_selector('h1', text: 'Login')
    expect(page).to have_link('Sign up')
    expect(page).to have_link('Forgot your password?')
    expect(page).to have_link("Didn't receive confirmation instructions?")
    expect(page).to have_link("Didn't receive unlock instructions?")
    expect(page).to have_button('Google')
    user = FactoryBot.create(:user, :confirmed)
    fill_in 'user_login', with: user.username
    fill_in 'user_password', with: user.password
    click_button 'Login'
    expect(page).to have_content('Signed in successfully.')
    expect(current_path).to eq('/')
    expect(page).to have_css('.dropdown-toggle i.bi-person-circle', minimum: 1)
    expect(page).to_not have_selector('a', text: 'Admin Tool')
    expect(Devise.mailer.deliveries.count).to eq 0
  end

  scenario 'with case-insensitive username credentials' do
    SiteSetting.first.update(users_can_join: true)
    expect(page).to have_selector('h1', text: 'Login')
    expect(page).to have_link('Sign up')
    expect(page).to have_link('Forgot your password?')
    expect(page).to have_link("Didn't receive confirmation instructions?")
    expect(page).to have_link("Didn't receive unlock instructions?")
    expect(page).to have_button('Google')
    user = FactoryBot.create(:user, :confirmed)
    fill_in 'user_login', with: user.username.upcase
    fill_in 'user_password', with: user.password
    click_button 'Login'
    expect(page).to have_content('Invalid Login')
    expect(current_path).to eq('/users/sign_in')
    expect(page).to_not have_selector('a', text: 'My Account')
  end

  scenario 'with unconfirmed email account' do
    user = FactoryBot.create(:user)
    expect(Devise.mailer.deliveries.count).to eq 1
    fill_in 'user_login', with: user.email
    fill_in 'user_password', with: password
    click_button 'Login'
    expect(page).to have_content('Invalid Login')
    expect(current_path).to eq('/users/sign_in')
    expect(page).to_not have_css('.dropdown-toggle i.bi-person-circle')
    expect(Devise.mailer.deliveries.count).to eq 1
  end

  scenario 'invalid password' do
    user = FactoryBot.create(:user, :confirmed)
    fill_in 'user_login', with: user.email
    fill_in 'user_password', with: password
    click_button 'Login'
    expect(page).to have_content('Invalid Login')
    expect(current_path).to eq('/users/sign_in')
    expect(page).to_not have_css('.dropdown-toggle i.bi-person-circle')
  end

  scenario 'invalid email' do
    user = FactoryBot.create(:user, :confirmed)
    fill_in 'user_login', with: email
    fill_in 'user_password', with: password
    click_button 'Login'
    expect(page).to have_content('Invalid Login')
    expect(current_path).to eq('/users/sign_in')
    expect(page).to_not have_css('.dropdown-toggle i.bi-person-circle')
  end

  scenario 'user is already signed in' do
    user = FactoryBot.create(:user, :confirmed)
    sign_in user
    visit new_user_session_path
    expect(page).to have_content('You are already signed in.')
    expect(current_path).to eq('/')
    expect(page).to have_css('.dropdown-toggle i.bi-person-circle', minimum: 1)
    expect(Devise.mailer.deliveries.count).to eq 0
  end

  scenario 'makes password visible', js: true do
    user = FactoryBot.create(:user, :confirmed)
    visit new_user_session_path
    expect(page).to_not have_css('.bi-eye-slash', visible: true)
    expect(page).to have_css('.bi-eye', visible: true)
    find(".bi-eye", match: :first).click
    expect(page).to_not have_css('.bi-eye', visible: true)
    expect(page).to have_css('.bi-eye-slash', visible: true)
    find(".bi-eye-slash", match: :first).click
    expect(page).to_not have_css('.bi-eye-slash', visible: true)
    expect(page).to have_css('.bi-eye', visible: true)
  end
end
