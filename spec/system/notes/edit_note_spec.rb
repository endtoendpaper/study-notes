require 'rails_helper'

describe 'User edits note', type: :system do
  let(:title) { Faker::Lorem.sentence(word_count: 6) }
  let(:text) { Faker::Lorem.paragraphs(number: 4) }

  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @user1 = FactoryBot.create(:user, :confirmed)
    @note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
    @group_member = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group)
    @note_private1.collaborate(@collaborator, @note_private1.user)
    @note_private1.add_group(@group, @note_private1.user)
    @group.add_member(@group_member, @group.user)
  end

  scenario 'as owner with valid data', js: true do
    sign_in @user1
    visit note_path(@note_private1.id)
    click_link 'Edit note'
    expect(page).to have_button('Delete Note')
    fill_in 'note_title', with: title
    fill_in_trix_editor 'note_text_trix_input_note_' + @note_private1.id.to_s, with: 'my edited note'
    choose('note_visibility_1')
    click_button 'Submit'
    expect(current_path).to include('/notes/' + @note_private1.id.to_s)
    expect(page).to have_content('Note was successfully updated.')
    expect(page).to_not have_content(@note_private1.title)
    expect(page).to_not have_content(@note_private1.text)
    expect(page).to_not have_content('Private')
    expect(page).to have_content(title)
    expect(page).to have_content('my edited note')
    expect(page).to have_content('Internal')
  end

  scenario "as admin editing user's note with valid data", js: true do
    sign_in @admin
    visit note_path(@note_private1.id)
    click_link 'Edit note'
    expect(page).to have_button('Delete Note')
    fill_in 'note_title', with: title
    fill_in_trix_editor 'note_text_trix_input_note_' + @note_private1.id.to_s, with: 'my edited note'
    choose('note_visibility_1')
    click_button 'Submit'
    expect(current_path).to include('/notes/' + @note_private1.id.to_s)
    expect(page).to have_content('Note was successfully updated.')
    expect(page).to_not have_content(@note_private1.title)
    expect(page).to_not have_content(@note_private1.text)
    expect(page).to_not have_content('Private')
    expect(page).to have_content(title)
    expect(page).to have_content('my edited note')
    expect(page).to have_content('Internal')
  end

  scenario 'as collaborator with valid data', js: true do
    sign_in @collaborator
    visit note_path(@note_private1.id)
    click_link 'Edit note'
    expect(page).to_not have_button('Delete Note')
    fill_in 'note_title', with: title
    fill_in_trix_editor 'note_text_trix_input_note_' + @note_private1.id.to_s, with: 'my edited note'
    choose('note_visibility_1')
    click_button 'Submit'
    expect(current_path).to include('/notes/' + @note_private1.id.to_s)
    expect(page).to have_content('Note was successfully updated.')
    expect(page).to_not have_content(@note_private1.title)
    expect(page).to_not have_content(@note_private1.text)
    expect(page).to_not have_content('Private')
    expect(page).to have_content(title)
    expect(page).to have_content('my edited note')
    expect(page).to have_content('Internal')
  end

  scenario 'as group member with valid data', js: true do
    sign_in @group_member
    visit note_path(@note_private1.id)
    click_link 'Edit note'
    expect(page).to_not have_button('Delete Note')
    fill_in 'note_title', with: title
    fill_in_trix_editor 'note_text_trix_input_note_' + @note_private1.id.to_s, with: 'my edited note'
    choose('note_visibility_1')
    click_button 'Submit'
    expect(current_path).to include('/notes/' + @note_private1.id.to_s)
    expect(page).to have_content('Note was successfully updated.')
    expect(page).to_not have_content(@note_private1.title)
    expect(page).to_not have_content(@note_private1.text)
    expect(page).to_not have_content('Private')
    expect(page).to have_content(title)
    expect(page).to have_content('my edited note')
    expect(page).to have_content('Internal')
  end

  scenario 'as owner with invalid data', js: true do
    sign_in @user1
    visit note_path(@note_private1.id)
    click_link 'Edit note'
    fill_in 'note_title', with: ''
    fill_in_trix_editor 'note_text_trix_input_note_' + @note_private1.id.to_s, with: 'my edited note'
    choose('note_visibility_1')
    click_button 'Submit'
    expect(current_path).to include('/notes/' + @note_private1.id.to_s + '/edit')
    expect(page).to have_content('1 error prohibited this note from being saved:')
    expect(page).to have_content("Title can't be blank")
  end

  scenario "as admin editing user's note with invalid data", js: true do
    sign_in @admin
    visit note_path(@note_private1.id)
    click_link 'Edit note'
    fill_in 'note_title', with: ''
    fill_in_trix_editor 'note_text_trix_input_note_' + @note_private1.id.to_s, with: 'my edited note'
    choose('note_visibility_1')
    click_button 'Submit'
    expect(current_path).to include('/notes/' + @note_private1.id.to_s + '/edit')
    expect(page).to have_content('1 error prohibited this note from being saved:')
    expect(page).to have_content("Title can't be blank")
  end
end
