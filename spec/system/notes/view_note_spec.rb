require 'rails_helper'

describe 'Views note', type: :system do
  before(:each) do
    reset_elasticsearch_indices(['notes'])

    @user1 = FactoryBot.create(:user, :confirmed, time_zone: 'UTC')
    @user2 = FactoryBot.create(:user, :confirmed, time_zone: 'UTC')
    @user3 = FactoryBot.create(:user, :confirmed, time_zone: 'UTC')
    @admin = FactoryBot.create(:user, :confirmed, :admin, time_zone: 'UTC')
    @note_public1 = FactoryBot.create(:note, :public, user_id: @user1.id, text: 'my rich text 1 user1')
    @note_internal1 = FactoryBot.create(:note, :internal, user_id: @user1.id, text: 'my rich text 2 user1')
    @note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id, text: 'my rich text 3 user1')
    @note_public2 = FactoryBot.create(:note, :public, user_id: @user2.id, text: 'my rich text 1 user2')
    @note_internal2 = FactoryBot.create(:note, :internal, user_id: @user2.id, text: 'my rich text 2 user2')
    @note_private2 = FactoryBot.create(:note, :private, user_id: @user2.id, text: 'my rich text 3 user2')
    @group_member = FactoryBot.create(:user, :confirmed, time_zone: 'UTC')
    @collaborator = FactoryBot.create(:user, :confirmed, time_zone: 'UTC')
    @group = FactoryBot.create(:group)
    @note_private1.collaborate(@collaborator, @note_private1.user)
    @note_private1.add_group(@group, @note_private1.user)
    @group.add_member(@group_member, @group.user)

    reindex_elasticsearch_data(['notes'])
  end

  scenario 'on index page there is pagination as admin' do
    13.times do
      FactoryBot.create(:note, :public, user_id: @user1.id)
    end
    Note.import
    Note.__elasticsearch__.refresh_index!
    sign_in @admin
    visit notes_path
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end

  scenario 'on user index page there is pagination as admin' do
    13.times do
      FactoryBot.create(:note, :public, user_id: @user1.id)
    end
    Note.import
    Note.__elasticsearch__.refresh_index!
    sign_in @admin
    visit '/users/' + @user1.username + '/notes'
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end

  scenario 'on index page there is pagination as user' do
    13.times do
      FactoryBot.create(:note, :public, user_id: @user1.id)
    end
    Note.import
    Note.__elasticsearch__.refresh_index!
    sign_in @user2
    visit notes_path
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end

  scenario 'on user index page there is pagination as user' do
    13.times do
      FactoryBot.create(:note, :public, user_id: @user1.id)
    end
    Note.import
    Note.__elasticsearch__.refresh_index!
    sign_in @user2
    visit '/users/' + @user1.username + '/notes'
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end

  scenario 'on index page there is pagination not logged in' do
    13.times do
      FactoryBot.create(:note, :public, user_id: @user1.id)
    end
    Note.import
    Note.__elasticsearch__.refresh_index!
    visit notes_path
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end

  scenario 'on user index page there is pagination not logged in' do
    13.times do
      FactoryBot.create(:note, :public, user_id: @user1.id)
    end
    Note.import
    Note.__elasticsearch__.refresh_index!
    visit '/users/' + @user1.username + '/notes'
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end

  scenario 'on index page as admin' do
    sign_in @admin
    visit notes_path
    expect(page).to have_content(@user1.handle)
    expect(page).to have_link('Show note')
    expect(page).to have_content(@note_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to have_content('Public')
    expect(page).to have_content(@note_internal1.title)
    expect(page).to have_content('my rich text 2 user1')
    expect(page).to have_content('Internal')
    expect(page).to have_content(@note_private1.title)
    expect(page).to have_content('my rich text 3 user1')
    expect(page).to have_content('Private')
    expect(page).to_not have_link('Edit collaborators')
    expect(page).to_not have_link('Edit tags')
    expect(page).to_not have_link('Add tags')
    expect(page).to_not have_link('Edit note')
  end

  scenario 'on index page as user who has private note' do
    sign_in @user1
    visit notes_path
    expect(page).to have_content(@user1.handle)
    expect(page).to have_link('Show note')
    expect(page).to have_content(@note_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to have_content('Public')
    expect(page).to have_content(@note_internal1.title)
    expect(page).to have_content('my rich text 2 user1')
    expect(page).to have_content('Internal')
    expect(page).to have_content(@note_private1.title)
    expect(page).to have_content('my rich text 3 user1')
    expect(page).to have_content('Private')
    expect(page).to_not have_link('Edit collaborators')
    expect(page).to_not have_link('Edit tags')
    expect(page).to_not have_link('Add tags')
    expect(page).to_not have_link('Edit note')
  end

  scenario 'on index page as collaborator of private note' do
    sign_in @collaborator
    visit notes_path
    expect(page).to have_content(@user1.handle)
    expect(page).to have_link('Show note')
    expect(page).to have_content(@note_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to_not have_content('Public')
    expect(page).to have_content(@note_internal1.title)
    expect(page).to have_content('my rich text 2 user1')
    expect(page).to_not have_content('Internal')
    expect(page).to have_content(@note_private1.title)
    expect(page).to have_content('my rich text 3 user1')
    expect(page).to have_content('Private')
    expect(page).to_not have_link('Edit collaborators')
    expect(page).to_not have_link('Edit tags')
    expect(page).to_not have_link('Add tags')
    expect(page).to_not have_link('Edit note')
  end

  scenario 'on index page as group member of private note' do
    sign_in @group_member
    visit notes_path
    expect(page).to have_content(@user1.handle)
    expect(page).to have_link('Show note')
    expect(page).to have_content(@note_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to_not have_content('Public')
    expect(page).to have_content(@note_internal1.title)
    expect(page).to have_content('my rich text 2 user1')
    expect(page).to_not have_content('Internal')
    expect(page).to have_content(@note_private1.title)
    expect(page).to have_content('my rich text 3 user1')
    expect(page).to have_content('Private')
    expect(page).to_not have_link('Edit collaborators')
    expect(page).to_not have_link('Edit tags')
    expect(page).to_not have_link('Add tags')
    expect(page).to_not have_link('Edit note')
  end

  scenario "on index page as user who shouldn't see other's private notes" do
    sign_in @user3
    visit notes_path
    expect(page).to have_content(@user1.handle)
    expect(page).to have_link('Show note')
    expect(page).to have_content(@note_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to_not have_content('Public')
    expect(page).to have_content(@note_internal1.title)
    expect(page).to have_content('my rich text 2 user1')
    expect(page).to_not have_content('Internal')
    expect(page).to_not have_content(@note_private1.title)
    expect(page).to_not have_content('my rich text 3 user1')
    expect(page).to_not have_content('Private')
    expect(page).to_not have_link('Edit tags')
    expect(page).to_not have_link('Add tags')
    expect(page).to_not have_link('Edit note')
  end

  scenario 'on index page not logged in' do
    visit notes_path
    expect(page).to have_content(@user1.handle)
    expect(page).to have_link('Show note')
    expect(page).to have_content(@note_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to_not have_content('Public')
    expect(page).to_not have_content(@note_internal1.title)
    expect(page).to_not have_content('my rich text 2 user1')
    expect(page).to_not have_content('Internal')
    expect(page).to_not have_content(@note_private1.title)
    expect(page).to_not have_content('my rich text 3 user1')
    expect(page).to_not have_content('Private')
    expect(page).to_not have_link('Edit tags')
    expect(page).to_not have_link('Add tags')
    expect(page).to_not have_link('Edit note')
  end

  scenario 'on user index page as admin' do
    sign_in @admin
    visit '/users/' + @user1.username + '/notes'
    expect(page).to have_content(@user1.handle)
    expect(page).to have_link('Show note')
    expect(page).to have_content(@note_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to have_content('Public')
    expect(page).to have_content(@note_internal1.title)
    expect(page).to have_content('my rich text 2 user1')
    expect(page).to have_content('Internal')
    expect(page).to have_content(@note_private1.title)
    expect(page).to have_content('my rich text 3 user1')
    expect(page).to have_content('Private')
    expect(page).to_not have_content('my rich text 1 user2')
    expect(page).to_not have_content('my rich text 2 user2')
    expect(page).to_not have_content('my rich text 3 user2')
    expect(page).to_not have_link('Edit tags')
    expect(page).to_not have_link('Add tags')
    expect(page).to_not have_link('Edit note')
  end

  scenario 'on user index page as user who has private note' do
    sign_in @user1
    visit '/users/' + @user1.username + '/notes'
    expect(page).to have_content(@user1.handle)
    expect(page).to have_link('Show note')
    expect(page).to have_content(@note_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to have_content('Public')
    expect(page).to have_content(@note_internal1.title)
    expect(page).to have_content('my rich text 2 user1')
    expect(page).to have_content('Internal')
    expect(page).to have_content(@note_private1.title)
    expect(page).to have_content('my rich text 3 user1')
    expect(page).to have_content('Private')
    expect(page).to_not have_content('my rich text 1 user2')
    expect(page).to_not have_content('my rich text 2 user2')
    expect(page).to_not have_content('my rich text 3 user2')
    expect(page).to_not have_link('Edit tags')
    expect(page).to_not have_link('Add tags')
    expect(page).to_not have_link('Edit note')
  end

  scenario 'on user index page as collaborator of private note' do
    sign_in @collaborator
    visit '/users/' + @user1.username + '/notes'
    expect(page).to have_content(@user1.handle)
    expect(page).to have_link('Show note')
    expect(page).to have_content(@note_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to_not have_content('Public')
    expect(page).to have_content(@note_internal1.title)
    expect(page).to have_content('my rich text 2 user1')
    expect(page).to_not have_content('Internal')
    expect(page).to have_content(@note_private1.title)
    expect(page).to have_content('my rich text 3 user1')
    expect(page).to have_content('Private')
    expect(page).to_not have_content('my rich text 1 user2')
    expect(page).to_not have_content('my rich text 2 user2')
    expect(page).to_not have_content('my rich text 3 user2')
    expect(page).to_not have_link('Edit tags')
    expect(page).to_not have_link('Add tags')
    expect(page).to_not have_link('Edit note')
  end

  scenario 'on user index page as group member of private note' do
    sign_in @group_member
    visit '/users/' + @user1.username + '/notes'
    expect(page).to have_content(@user1.handle)
    expect(page).to have_link('Show note')
    expect(page).to have_content(@note_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to_not have_content('Public')
    expect(page).to have_content(@note_internal1.title)
    expect(page).to have_content('my rich text 2 user1')
    expect(page).to_not have_content('Internal')
    expect(page).to have_content(@note_private1.title)
    expect(page).to have_content('my rich text 3 user1')
    expect(page).to have_content('Private')
    expect(page).to_not have_content('my rich text 1 user2')
    expect(page).to_not have_content('my rich text 2 user2')
    expect(page).to_not have_content('my rich text 3 user2')
    expect(page).to_not have_link('Edit tags')
    expect(page).to_not have_link('Add tags')
    expect(page).to_not have_link('Edit note')
  end

  scenario "on user index page as user who shouldn't see other's private notes" do
    sign_in @user2
    visit '/users/' + @user1.username + '/notes'
    expect(page).to have_content(@user1.handle)
    expect(page).to have_link('Show note')
    expect(page).to have_content(@note_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to_not have_content('Public')
    expect(page).to have_content(@note_internal1.title)
    expect(page).to have_content('my rich text 2 user1')
    expect(page).to_not have_content('Internal')
    expect(page).to_not have_content(@note_private1.title)
    expect(page).to_not have_content('my rich text 3 user1')
    expect(page).to_not have_content('Private')
    expect(page).to_not have_content('my rich text 1 user2')
    expect(page).to_not have_content('my rich text 2 user2')
    expect(page).to_not have_content('my rich text 3 user2')
    expect(page).to_not have_link('Edit tags')
    expect(page).to_not have_link('Add tags')
    expect(page).to_not have_link('Edit note')
  end

  scenario 'on user index page not logged in' do
    visit '/users/' + @user1.username + '/notes'
    expect(page).to have_content(@user1.handle)
    expect(page).to have_link('Show note')
    expect(page).to have_content(@note_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to_not have_content('Public')
    expect(page).to_not have_content(@note_internal1.title)
    expect(page).to_not have_content('my rich text 2 user1')
    expect(page).to_not have_content('Internal')
    expect(page).to_not have_content(@note_private1.title)
    expect(page).to_not have_content('my rich text 3 user1')
    expect(page).to_not have_content('Private')
    expect(page).to_not have_content('my rich text 1 user2')
    expect(page).to_not have_content('my rich text 2 user2')
    expect(page).to_not have_content('my rich text 3 user2')
    expect(page).to_not have_link('Edit tags')
    expect(page).to_not have_link('Add tags')
    expect(page).to_not have_link('Edit note')
  end

  scenario "on show page as admin for user's private note" do
    sign_in @admin
    visit note_path(@note_private1.id)
    expect(page).to_not have_link('Show note')
    expect(page).to have_content(@user1.handle)
    expect(page).to have_content('Private')
    expect(page).to have_content(@note_private1.title)
    expect(page).to have_content('my rich text 3 user1')
    expect(page).to have_link('Edit note')
    expect(page).to have_content(@note_private1.last_activity_at.strftime('%b %d, %Y %I:%M%p %Z'))
  end

  scenario "on show page as admin for user's internal note" do
    sign_in @admin
    visit note_path(@note_internal1.id)
    expect(page).to_not have_link('Show note')
    expect(page).to have_content(@user1.handle)
    expect(page).to have_content('Internal')
    expect(page).to have_content(@note_internal1.title)
    expect(page).to have_content('my rich text 2 user1')
    expect(page).to have_link('Edit note')
    expect(page).to have_content(@note_internal1.last_activity_at.strftime('%b %d, %Y %I:%M%p %Z'))
  end

  scenario "on show page as admin for user's public note" do
    sign_in @admin
    visit note_path(@note_public1.id)
    expect(page).to_not have_link('Show note')
    expect(page).to have_content(@user1.handle)
    expect(page).to have_content('Public')
    expect(page).to have_content(@note_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to have_link('Edit note')
    expect(page).to have_content(@note_public1.last_activity_at.strftime('%b %d, %Y %I:%M%p %Z'))
  end

  scenario 'on show page as user for own private note' do
    sign_in @user1
    visit note_path(@note_private1.id)
    expect(page).to_not have_link('Show note')
    expect(page).to have_content(@user1.handle)
    expect(page).to have_content('Private')
    expect(page).to have_content(@note_private1.title)
    expect(page).to have_content('my rich text 3 user1')
    expect(page).to have_link('Edit note')
    expect(page).to have_content(@note_private1.last_activity_at.strftime('%b %d, %Y %I:%M%p %Z'))
  end

  scenario 'on show page as collaborator of private note' do
    sign_in @collaborator
    visit note_path(@note_private1.id)
    expect(page).to_not have_link('Show note')
    expect(page).to have_content(@user1.handle)
    expect(page).to have_content('Private')
    expect(page).to have_content(@note_private1.title)
    expect(page).to have_content('my rich text 3 user1')
    expect(page).to have_link('Edit note')
    expect(page).to have_content(@note_private1.last_activity_at.strftime('%b %d, %Y %I:%M%p %Z'))
  end

  scenario 'on show page as group member of private note' do
    sign_in @group_member
    visit note_path(@note_private1.id)
    expect(page).to_not have_link('Show note')
    expect(page).to have_content(@user1.handle)
    expect(page).to have_content('Private')
    expect(page).to have_content(@note_private1.title)
    expect(page).to have_content('my rich text 3 user1')
    expect(page).to have_link('Edit note')
    expect(page).to have_content(@note_private1.last_activity_at.strftime('%b %d, %Y %I:%M%p %Z'))
  end

  scenario 'on show page as user for own internal note' do
    sign_in @user1
    visit note_path(@note_internal1.id)
    expect(page).to_not have_link('Show note')
    expect(page).to have_content(@user1.handle)
    expect(page).to have_content('Internal')
    expect(page).to have_content(@note_internal1.title)
    expect(page).to have_content('my rich text 2 user1')
    expect(page).to have_link('Edit note')
    expect(page).to have_content(@note_internal1.last_activity_at.strftime('%b %d, %Y %I:%M%p %Z'))
  end

  scenario 'on show page as user for own public note' do
    sign_in @user1
    visit note_path(@note_public1.id)
    expect(page).to_not have_link('Show note')
    expect(page).to have_content(@user1.handle)
    expect(page).to have_content('Public')
    expect(page).to have_content(@note_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to have_link('Edit note')
    expect(page).to have_content(@note_public1.last_activity_at.strftime('%b %d, %Y %I:%M%p %Z'))
  end

  scenario "on show page as user for another user's internal note" do
    sign_in @user2
    visit note_path(@note_internal1.id)
    expect(page).to_not have_link('Show note')
    expect(page).to have_content(@user1.handle)
    expect(page).to_not have_content('Internal')
    expect(page).to have_content(@note_internal1.title)
    expect(page).to have_content('my rich text 2 user1')
    expect(page).to_not have_link('Edit note')
    expect(page).to have_content(@note_internal1.last_activity_at.strftime('%b %d, %Y %I:%M%p %Z'))
  end

  scenario "on show page as user for another user's public note" do
    sign_in @user2
    visit note_path(@note_public1.id)
    expect(page).to_not have_link('Show note')
    expect(page).to have_content(@user1.handle)
    expect(page).to_not have_content('Public')
    expect(page).to have_content(@note_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to_not have_link('Edit note')
    expect(page).to have_content(@note_public1.last_activity_at.strftime('%b %d, %Y %I:%M%p %Z'))
  end

  scenario 'on show page as for public note not logged in' do
    visit note_path(@note_public1.id)
    expect(page).to_not have_link('Show note')
    expect(page).to have_content(@user1.handle)
    expect(page).to_not have_content('Internal')
    expect(page).to have_content(@note_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to_not have_link('Edit note')
    expect(page).to have_content(@note_public1.last_activity_at.strftime('%b %d, %Y %I:%M%p %Z'))
  end
end
