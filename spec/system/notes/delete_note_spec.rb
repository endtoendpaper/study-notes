require 'rails_helper'

describe 'Deletes note', type: :system do
  before(:each) do
    @user1 = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
  end

  scenario 'as user deleting own note', js: true do
    sign_in @user1
    visit note_path(@note_private1.id)
    click_link 'Edit note'
    click_button 'Delete Note'
    page.accept_alert
    expect(current_path).to include('/notes')
    expect(page).to have_content('Note was successfully deleted.')
    visit notes_path
    expect(page).to_not have_content(@note_private1.title)
  end

  scenario "as admin deleting user's note", js: true do
    sign_in @admin
    visit note_path(@note_private1.id)
    click_link 'Edit note'
    click_button 'Delete Note'
    page.accept_alert
    expect(current_path).to include('/notes')
    expect(page).to have_content('Note was successfully deleted.')
    visit notes_path
    expect(page).to_not have_content(@note_private1.title)
  end
end
