require 'rails_helper'

describe 'User creates a note', type: :system do
  let(:title) { Faker::Lorem.sentence(word_count: 6) }
  let(:text) { Faker::Lorem.paragraphs(number: 4) }

  before(:each) do
    @user = FactoryBot.create(:user, :confirmed)
  end

  scenario 'with valid data', js: true do
    sign_in @user
    visit new_note_path
    fill_in 'note_title', with: title
    fill_in_trix_editor 'note_text_trix_input_note', with: 'my new note'
    choose('note_visibility_1')
    click_button 'Submit'
    expect(page).to have_content('Note was successfully created.')
    expect(current_path).to include('/notes/')
    expect(page).to have_content(title)
    expect(page).to have_content('my new note')
    expect(page).to have_content('Internal')
  end

  scenario 'with invalid data', js: true do
    sign_in @user
    visit new_note_path
    fill_in 'note_title', with: title
    choose('note_visibility_1')
    click_button 'Submit'
    expect(page).to have_content('1 error prohibited this note from being saved:')
    expect(page).to have_content("Text can't be blank")
    expect(current_path).to eq('/notes/new')
  end
end
