require 'rails_helper'

describe 'Reply to issue', type: :system do
  let(:comment) { Faker::Lorem.sentence(word_count: 25) }
  let(:reply) { Faker::Lorem.sentence(word_count: 25) }
  let(:note) { Faker::Lorem.sentence(word_count: 25) }

  before(:each) do
    SiteSetting.first.update(email_contact_form_to_admins: 'true')
    SiteSetting.first.update(email_issue_status_to_users: 'true')
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @admin2 = FactoryBot.create(:user, :confirmed, :admin)
    @user = FactoryBot.create(:user, :confirmed)
    @support_issue = FactoryBot.create(:support_issue, :rich_text, user_id: @user.id)
    @support_issue2 = FactoryBot.create(:support_issue, :rich_text, :addressed, user_id: @user.id)
  end

  scenario 'as admin with rich text', js: true do
    sign_in @admin
    expect(ActionMailer::Base.deliveries.count).to eq 0
    visit support_issue_path(@support_issue)
    expect(page).to_not have_link('Submit New Issue')
    expect(page).to_not have_content('REPLY TO ADMINS')
    expect(page).to have_content('REPLY TO USER')
    expect(page).to have_content('MAKE INTERNAL NOTE')
    fill_in_trix_editor 'support_reply_rich_text_body_trix_input_support_reply', with: reply
    find(:xpath, '//*[@id="new_support_reply"]/div[2]/input').click
    expect(page).to have_content('User reply successfully submitted.')
    expect(page).to have_content(reply)
    expect(ActionMailer::Base.deliveries.count).to eq 2
    visit support_issue_path(@support_issue)
    click_button 'MAKE INTERNAL NOTE'
    fill_in_trix_editor 'support_note_rich_text_body_trix_input_support_note', with: note
    click_button 'Add Note'
    expect(page).to have_content('Internal note successfully submitted.')
    expect(page).to have_content(note)
    expect(ActionMailer::Base.deliveries.count).to eq 3
  end

  scenario 'as admin with plain text' do
    SiteSetting.first.update(rich_text_support_replies: 'false')
    sign_in @admin
    expect(ActionMailer::Base.deliveries.count).to eq 0
    visit support_issue_path(@support_issue)
    expect(page).to_not have_link('Submit New Issue')
    expect(page).to_not have_content('REPLY TO ADMINS')
    expect(page).to have_content('REPLY TO USER')
    expect(page).to have_content('MAKE INTERNAL NOTE')
    fill_in 'support_reply_body', with: reply
    find(:xpath, '//*[@id="new_support_reply"]/div[2]/input').click
    expect(page).to have_content('User reply successfully submitted.')
    expect(page).to have_content(reply)
    expect(ActionMailer::Base.deliveries.count).to eq 2
    visit support_issue_path(@support_issue)
    click_button 'MAKE INTERNAL NOTE'
    fill_in 'support_note_body', with: note
    click_button 'Add Note'
    expect(page).to have_content('Internal note successfully submitted.')
    expect(page).to have_content(note)
    expect(ActionMailer::Base.deliveries.count).to eq 3
  end

  scenario 'as admin for resolved issue', js: true do
    sign_in @admin
    expect(ActionMailer::Base.deliveries.count).to eq 0
    visit support_issue_path(@support_issue2)
    expect(page).to_not have_link('Submit New Issue')
    expect(page).to_not have_content('REPLY TO ADMINS')
    expect(page).to_not have_content('REPLY TO USER')
    expect(page).to have_content('MAKE INTERNAL NOTE')
    fill_in_trix_editor 'support_note_rich_text_body_trix_input_support_note', with: note
    click_button 'Add Note'
    expect(page).to have_content('Internal note successfully submitted.')
    expect(page).to have_content(note)
    expect(ActionMailer::Base.deliveries.count).to eq 1
  end

  scenario 'as admin with invalid data', js: true do
    sign_in @admin
    expect(ActionMailer::Base.deliveries.count).to eq 0
    visit support_issue_path(@support_issue)
    expect(page).to_not have_link('Submit New Issue')
    expect(page).to_not have_content('REPLY TO ADMINS')
    expect(page).to have_content('REPLY TO USER')
    expect(page).to have_content('MAKE INTERNAL NOTE')
    find(:xpath, '//*[@id="new_support_reply"]/div[2]/input').click
    expect(page).to have_content('2 errors prohibited this reply from being saved:')
    expect(page).to have_content("Details can't blank")
    expect(ActionMailer::Base.deliveries.count).to eq 0
    visit support_issue_path(@support_issue)
    click_button 'MAKE INTERNAL NOTE'
    click_button 'Add Note'
    expect(page).to have_content('2 errors prohibited this note from being saved:')
    expect(page).to have_content("Details can't blank")
    expect(ActionMailer::Base.deliveries.count).to eq 0
  end

  scenario 'as user for open issue with rich text', js: true do
    sign_in @user
    expect(ActionMailer::Base.deliveries.count).to eq 0
    visit support_issue_path(@support_issue)
    expect(page).to_not have_link('Submit New Issue')
    expect(page).to_not have_content('REPLY TO USER')
    expect(page).to_not have_content('MAKE INTERNAL NOTE')
    expect(page).to have_content('REPLY TO ADMINS')
    fill_in_trix_editor 'support_comment_rich_text_body_trix_input_support_comment', with: comment
    click_button 'Add Comment'
    expect(page).to have_content('Comment successfully submitted.')
    expect(page).to have_content(comment)
    expect(ActionMailer::Base.deliveries.count).to eq 1
  end

  scenario 'as user for open issue with plain text' do
    SiteSetting.first.update(rich_text_support_issues: 'false')
    sign_in @user
    expect(ActionMailer::Base.deliveries.count).to eq 0
    visit support_issue_path(@support_issue)
    expect(page).to_not have_content('REPLY TO USER')
    expect(page).to_not have_content('MAKE INTERNAL NOTE')
    expect(page).to_not have_link('Submit New Issue')
    expect(page).to have_content('REPLY TO ADMINS')
    fill_in 'support_comment_body', with: comment
    click_button 'Add Comment'
    expect(page).to have_content('Comment successfully submitted.')
    expect(page).to have_content(comment)
    expect(ActionMailer::Base.deliveries.count).to eq 1
  end

  scenario 'as user for resolved issue' do
    sign_in @user
    visit support_issue_path(@support_issue2)
    expect(page).to have_content('This issue has been resolved.')
    expect(page).to have_link('Submit New Issue')
    expect(page).to have_content('REPLY TO ADMINS')
    expect(page).to_not have_content('REPLY TO USER')
    expect(page).to_not have_content('MAKE INTERNAL NOTE')
  end

  scenario 'as user with invalid data', js: true do
    sign_in @user
    expect(ActionMailer::Base.deliveries.count).to eq 0
    visit support_issue_path(@support_issue)
    click_button 'Add Comment'
    expect(page).to_not have_content('Comment successfully submitted.')
    expect(page).to have_content('2 errors prohibited this comment from being saved:')
    expect(page).to have_content("Details can't blank")
    expect(ActionMailer::Base.deliveries.count).to eq 0
  end
end
