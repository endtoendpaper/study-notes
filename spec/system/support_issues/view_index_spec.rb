require 'rails_helper'

describe 'View index', type: :system do

  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @user1 = FactoryBot.create(:user, :confirmed)
    @user2 = FactoryBot.create(:user, :confirmed)
    @support_issue1 = FactoryBot.create(:support_issue, :rich_text, user_id: @user1.id)
    @support_issue2 = FactoryBot.create(:support_issue, :rich_text, :addressed, user_id: @user1.id)
    @support_issue3 = FactoryBot.create(:support_issue, :rich_text, user_id: @user2.id)
    @support_issue4 = FactoryBot.create(:support_issue, :rich_text, :addressed, user_id: @user2.id)
    @support_issue5 = FactoryBot.create(:support_issue, :rich_text, user_id: @admin.id)
    @support_issue6 = FactoryBot.create(:support_issue, :rich_text, :addressed, user_id: @admin.id)
  end

  scenario 'as admin' do
    sign_in @admin
    visit support_issues_path
    expect(page).to have_content(@support_issue1.summary)
    expect(page).to_not have_content(@support_issue2.summary)
    expect(page).to have_content(@support_issue3.summary)
    expect(page).to have_content(@support_issue4.summary)
    expect(page).to have_content(@support_issue5.summary)
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end

  scenario 'as user' do
    sign_in @user1
    visit '/users/' + @user1.reload.username + '/support'
    expect(page).to have_content(@support_issue1.summary)
    expect(page).to have_content(@support_issue2.summary)
    expect(page).to_not have_content(@support_issue3.summary)
    expect(page).to_not have_content(@support_issue4.summary)
    expect(page).to_not have_content(@support_issue5.summary)
    expect(page.body).to_not include('pagination justify-content-center')
  end
end
