require 'rails_helper'

describe 'Views group item counts', type: :system do
  before(:each) do
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group_owner = FactoryBot.create(:user, :confirmed)
    @group_member = FactoryBot.create(:user, :confirmed)

    @group = FactoryBot.create(:group, user_id: @group_owner.id)
    @group.add_member(@group_member, @group.user)

    reset_elasticsearch_indices

    UserItems.item_list.each do |item|
      private = FactoryBot.create(item.singularize, :private, user_id: @owner.id)
      internal = FactoryBot.create(item.singularize, :internal, user_id: @owner.id)
      public = FactoryBot.create(item.singularize, :public, user_id: @owner.id)
      private.collaborate(@collaborator, @admin)
      internal.collaborate(@collaborator, @admin)
      public.collaborate(@collaborator, @admin)

      private.add_group(@group, @group.user)
      internal.add_group(@group, @group.user)
      public.add_group(@group, @group.user)
    end

    reindex_elasticsearch_data
  end

  scenario 'as admin' do
    sign_in @admin
    visit user_path(@owner)
    UserItems.item_list.each do |item|
      expect(page).to have_link("3 " + item.capitalize)
    end
  end

  scenario 'as owner' do
    sign_in @owner
    visit group_path(@group)
    UserItems.item_list.each do |item|
      expect(page).to have_link("3 " + item.capitalize)
    end
  end

  scenario 'as item collaborator' do
    sign_in @collaborator
    visit group_path(@group)
    UserItems.item_list.each do |item|
      expect(page).to have_link("3 " + item.capitalize)
    end
  end

  scenario 'as item group owner' do
    sign_in @group_owner
    visit group_path(@group)
    UserItems.item_list.each do |item|
      expect(page).to have_link("3 " + item.capitalize)
    end
  end

  scenario 'as item group member' do
    sign_in @group_member
    visit group_path(@group)
    UserItems.item_list.each do |item|
      expect(page).to have_link("3 " + item.capitalize)
    end
  end

  scenario 'as user' do
    sign_in @user
    visit group_path(@group)
    UserItems.item_list.each do |item|
      expect(page).to have_link("2 " + item.capitalize)
    end
  end
end
