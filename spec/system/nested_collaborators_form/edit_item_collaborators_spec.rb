require 'rails_helper'

describe 'Edit item collaborations in nested form', type: :system do
  before(:each) do
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @group_member = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group)
    @group.add_member(@group_member, @group.user)
    @new_collaborator1 = FactoryBot.create(:user, :confirmed)
    @new_collaborator2 = FactoryBot.create(:user, :confirmed)
    @old_collaborator = FactoryBot.create(:user, :confirmed)
    @new_group1 = FactoryBot.create(:group)
    @new_group2 = FactoryBot.create(:group)
    @old_group = FactoryBot.create(:group)

    UserItems.item_list.each do |item|
      item = instance_variable_set("@#{item.singularize}", FactoryBot.create(item.singularize, :private, user_id: @owner.id))
      item.collaborate(@collaborator, item.user)
      item.add_group(@group, item.user)
      item.add_group(@old_group, item.user)
      item.collaborate(@old_collaborator, item.user)
    end
  end


  scenario 'add collaborators, remove collaborators as admin', js: true do
    sign_in @admin
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      expect(page).to have_button(@old_collaborator.handle)
      fill_in 'collaborator_username', with: @new_collaborator1.username
      click_button 'Add Collaborator'
      expect(page).to have_button(@old_collaborator.handle)
      expect(page).to have_button(@new_collaborator1.handle)
      fill_in 'collaborator_username', with: @new_collaborator2.handle
      click_button 'Add Collaborator'
      expect(page).to have_button(@old_collaborator.handle)
      expect(page).to have_button(@new_collaborator1.handle)
      expect(page).to have_button(@new_collaborator2.handle)
      click_button @old_collaborator.handle
      expect(page).to_not have_button(@old_collaborator.handle)
      click_button 'Submit'
      expect(page).to_not have_link(@old_collaborator.handle)
      expect(page).to have_link(@new_collaborator1.handle)
      expect(page).to have_link(@new_collaborator2.handle)
    end
  end

  scenario 'add collaborator that does not exist as admin', js: true do
    sign_in @admin
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      fill_in 'collaborator_username', with: '@notauser'
      click_button 'Add Collaborator'
      expect(page).to_not have_button('@notauser')
      expect(page).to have_content('Username does not exist.')
    end
  end

  scenario 'add collaborator already associated as admin', js: true do
    sign_in @admin
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      fill_in 'collaborator_username', with: @old_collaborator.handle
      click_button 'Add Collaborator'
      expect(page).to have_content('User is already a collaborater.')
    end
  end

  scenario 'add groups, remove groups as admin', js: true do
    sign_in @admin
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      expect(page).to have_button(@old_group.name)
      fill_in 'group_name', with: @new_group1.name
      click_button 'Add Group'
      expect(page).to have_button(@old_group.name)
      expect(page).to have_button(@new_group1.name)
      fill_in 'group_name', with: @new_group2.name
      click_button 'Add Group'
      expect(page).to have_button(@old_group.name)
      expect(page).to have_button(@new_group1.name)
      expect(page).to have_button(@new_group2.name)
      click_button @old_group.name
      expect(page).to_not have_button(@old_group.name)
      click_button 'Submit'
      expect(page).to_not have_link(@old_group.name)
      expect(page).to have_link(@new_group1.name)
      expect(page).to have_link(@new_group2.name)
    end
  end

  scenario 'add group that does not exists as admin', js: true do
    sign_in @admin
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      fill_in 'group_name', with: 'notagroup'
      click_button 'Add Group'
      expect(page).to_not have_button('notagroup')
      expect(page).to have_content('Group does not exist.')
    end
  end

  scenario 'add group already associated as admin', js: true do
    sign_in @admin
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      fill_in 'group_name', with: @old_group.name
      click_button 'Add Group'
      expect(page).to have_content('Group is already added.')
    end
  end

   scenario 'add collaborators, remove collaborators as owner', js: true do
    sign_in @owner
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      expect(page).to have_button(@old_collaborator.handle)
      fill_in 'collaborator_username', with: @new_collaborator1.username
      click_button 'Add Collaborator'
      expect(page).to have_button(@old_collaborator.handle)
      expect(page).to have_button(@new_collaborator1.handle)
      fill_in 'collaborator_username', with: @new_collaborator2.handle
      click_button 'Add Collaborator'
      expect(page).to have_button(@old_collaborator.handle)
      expect(page).to have_button(@new_collaborator1.handle)
      expect(page).to have_button(@new_collaborator2.handle)
      click_button @old_collaborator.handle
      expect(page).to_not have_button(@old_collaborator.handle)
      click_button 'Submit'
      expect(page).to_not have_link(@old_collaborator.handle)
      expect(page).to have_link(@new_collaborator1.handle)
      expect(page).to have_link(@new_collaborator2.handle)
    end
   end

   scenario 'add collaborator that does not exist as owner', js: true do
    sign_in @owner
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      fill_in 'collaborator_username', with: '@notauser'
      click_button 'Add Collaborator'
      expect(page).to_not have_button('@notauser')
      expect(page).to have_content('Username does not exist.')
    end
  end

  scenario 'add collaborator already associated as owner', js: true do
    sign_in @owner
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      fill_in 'collaborator_username', with: @old_collaborator.handle
      click_button 'Add Collaborator'
      expect(page).to have_content('User is already a collaborater.')
    end
  end

  scenario 'add groups, remove groups as owner', js: true do
    sign_in @owner
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      expect(page).to have_button(@old_group.name)
      fill_in 'group_name', with: @new_group1.name
      click_button 'Add Group'
      expect(page).to have_button(@old_group.name)
      expect(page).to have_button(@new_group1.name)
      fill_in 'group_name', with: @new_group2.name
      click_button 'Add Group'
      expect(page).to have_button(@old_group.name)
      expect(page).to have_button(@new_group1.name)
      expect(page).to have_button(@new_group2.name)
      click_button @old_group.name
      expect(page).to_not have_button(@old_group.name)
      click_button 'Submit'
      expect(page).to_not have_link(@old_group.name)
      expect(page).to have_link(@new_group1.name)
      expect(page).to have_link(@new_group2.name)
    end
  end

  scenario 'add group that does not exists as owner', js: true do
    sign_in @owner
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      fill_in 'group_name', with: 'notagroup'
      click_button 'Add Group'
      expect(page).to_not have_button('notagroup')
      expect(page).to have_content('Group does not exist.')
    end
  end

  scenario 'add group already associated as owner', js: true do
    sign_in @owner
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      fill_in 'group_name', with: @old_group.name
      click_button 'Add Group'
      expect(page).to have_content('Group is already added.')
    end
  end

  scenario 'add collaborators, remove collaborators as collaborator', js: true do
    sign_in @collaborator
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      expect(page).to have_button(@old_collaborator.handle)
      fill_in 'collaborator_username', with: @new_collaborator1.username
      click_button 'Add Collaborator'
      expect(page).to have_button(@old_collaborator.handle)
      expect(page).to have_button(@new_collaborator1.handle)
      fill_in 'collaborator_username', with: @new_collaborator2.username
      click_button 'Add Collaborator'
      expect(page).to have_button(@old_collaborator.handle)
      expect(page).to have_button(@new_collaborator1.handle)
      expect(page).to have_button(@new_collaborator2.handle)
      click_button @old_collaborator.handle
      expect(page).to_not have_button(@old_collaborator.handle)
      click_button 'Submit'
      expect(page).to_not have_link(@old_collaborator.handle)
      expect(page).to have_link(@new_collaborator1.handle)
      expect(page).to have_link(@new_collaborator2.handle)
    end
  end

  scenario 'add collaborator that does not exist as collaborator', js: true do
    sign_in @collaborator
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      fill_in 'collaborator_username', with: '@notauser'
      click_button 'Add Collaborator'
      expect(page).to_not have_button('@notauser')
      expect(page).to have_content('Username does not exist.')
    end
  end

  scenario 'add collaborator already associated as collaborator', js: true do
    sign_in @collaborator
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      fill_in 'collaborator_username', with: @old_collaborator.handle
      click_button 'Add Collaborator'
      expect(page).to have_content('User is already a collaborater.')
    end
  end

  scenario 'add groups, remove groups as collaborator', js: true do
    sign_in @collaborator
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      expect(page).to have_button(@old_group.name)
      fill_in 'group_name', with: @new_group1.name
      click_button 'Add Group'
      expect(page).to have_button(@old_group.name)
      expect(page).to have_button(@new_group1.name)
      fill_in 'group_name', with: @new_group2.name
      click_button 'Add Group'
      expect(page).to have_button(@old_group.name)
      expect(page).to have_button(@new_group1.name)
      expect(page).to have_button(@new_group2.name)
      click_button @old_group.name
      expect(page).to_not have_button(@old_group.name)
      click_button 'Submit'
      expect(page).to_not have_link(@old_group.name)
      expect(page).to have_link(@new_group1.name)
      expect(page).to have_link(@new_group2.name)
    end
  end

  scenario 'add group that does not exists as collaborator', js: true do
    sign_in @collaborator
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      fill_in 'group_name', with: 'notagroup'
      click_button 'Add Group'
      expect(page).to_not have_button('notagroup')
      expect(page).to have_content('Group does not exist.')
    end
  end

  scenario 'add group already associated as collaborator', js: true do
    sign_in @collaborator
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      fill_in 'group_name', with: @old_group.name
      click_button 'Add Group'
      expect(page).to have_content('Group is already added.')
    end
  end

  scenario 'add collaborators, remove collaborators as group member', js: true do
    sign_in @group_member
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      expect(page).to have_button(@old_collaborator.handle)
      fill_in 'collaborator_username', with: @new_collaborator1.username
      click_button 'Add Collaborator'
      expect(page).to have_button(@old_collaborator.handle)
      expect(page).to have_button(@new_collaborator1.handle)
      fill_in 'collaborator_username', with: @new_collaborator2.username
      click_button 'Add Collaborator'
      expect(page).to have_button(@old_collaborator.handle)
      expect(page).to have_button(@new_collaborator1.handle)
      expect(page).to have_button(@new_collaborator2.handle)
      click_button @old_collaborator.handle
      expect(page).to_not have_button(@old_collaborator.handle)
      click_button 'Submit'
      expect(page).to_not have_link(@old_collaborator.handle)
      expect(page).to have_link(@new_collaborator1.handle)
      expect(page).to have_link(@new_collaborator2.handle)
    end
  end

  scenario 'add collaborator that does not exist as group member', js: true do
    sign_in @group_member
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      fill_in 'collaborator_username', with: '@notauser'
      click_button 'Add Collaborator'
      expect(page).to_not have_button('@notauser')
      expect(page).to have_content('Username does not exist.')
    end
  end

  scenario 'add collaborator already associated as group member', js: true do
    sign_in @group_member
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      fill_in 'collaborator_username', with: @old_collaborator.handle
      click_button 'Add Collaborator'
      expect(page).to have_content('User is already a collaborater.')
    end
  end

  scenario 'add groups, remove groups as group member', js: true do
    sign_in @group_member
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      expect(page).to have_button(@old_group.name)
      fill_in 'group_name', with: @new_group1.name
      click_button 'Add Group'
      expect(page).to have_button(@old_group.name)
      expect(page).to have_button(@new_group1.name)
      fill_in 'group_name', with: @new_group2.name
      click_button 'Add Group'
      expect(page).to have_button(@old_group.name)
      expect(page).to have_button(@new_group1.name)
      expect(page).to have_button(@new_group2.name)
      click_button @old_group.name
      expect(page).to_not have_button(@old_group.name)
      click_button 'Submit'
      expect(page).to_not have_link(@old_group.name)
      expect(page).to have_link(@new_group1.name)
      expect(page).to have_link(@new_group2.name)
    end
  end

  scenario 'add group that does not exists as group member', js: true do
    sign_in @group_member
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      fill_in 'group_name', with: 'notagroup'
      click_button 'Add Group'
      expect(page).to_not have_button('notagroup')
      expect(page).to have_content('Group does not exist.')
    end
  end

  scenario 'add group already associated as group member', js: true do
    sign_in @group_member
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      fill_in 'group_name', with: @old_group.name
      click_button 'Add Group'
      expect(page).to have_content('Group is already added.')
    end
  end
end
