require 'rails_helper'

describe 'Creates calendar', type: :system do
  let(:title) { Faker::Lorem.sentence(word_count: 6) }
  let(:description) { Faker::Lorem.sentence(word_count: 25) }
  let(:background_color) { Faker::Color.hex_color }

  before(:each) do
    @owner = FactoryBot.create(:user, :confirmed)
  end

  scenario 'with valid data', js: true do
    sign_in @owner
    visit new_calendar_path
    fill_in 'calendar_title', with: title
    fill_in 'calendar_description', with: description
    choose('calendar_visibility_1')
    find('.pcr-button', match: :first).click
    find('.pcr-result', match: :first).set(background_color)
    find('.pcr-save', match: :first).click
    click_button 'Submit'
    expect(page).to have_content('Calendar was successfully created.')
    expect(current_path).to include('/calendars/')
    expect(page).to have_content(title)
    expect(page).to have_content(description)
    expect(page).to have_content('Internal')
    expect(Calendar.last.background_color).to eq(background_color.upcase)
  end

  scenario 'with invalid data', js: true do
    sign_in @owner
    visit new_calendar_path
    fill_in 'calendar_description', with: description
    choose('calendar_visibility_1')
    click_button 'Submit'
    expect(page).to have_content('1 error prohibited this calendar from being saved:')
    expect(page).to have_content("Title can't be blank")
    expect(current_path).to include('/calendars/new')
  end
end
