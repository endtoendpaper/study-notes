require 'rails_helper'

describe 'Edits calendar', type: :system do
  let(:title) { Faker::Lorem.sentence(word_count: 6) }
  let(:description) { Faker::Lorem.sentence(word_count: 25) }
  let(:background_color) { Faker::Color.hex_color }

  before(:each) do
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group_owner = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group, user_id: @group_owner.id)
    @group.add_member(@group_member, @group.user)

    @calendar_private = FactoryBot.create(:calendar, :private, user_id: @owner.id)
    @calendar_private.collaborate(@collaborator, @calendar_private.user)
    @calendar_private.add_group(@group, @calendar_private.user)
  end

  scenario 'with valid data as admin', js: true do
    sign_in @admin
    visit calendar_path(@calendar_private.id)
    click_link 'Edit calendar'
    expect(page).to have_button('Delete Calendar')
    fill_in 'calendar_title', with: title
    fill_in 'calendar_description', with: description
    choose('calendar_visibility_1')
    find('.pcr-button', match: :first).click
    find('.pcr-result', match: :first).set(background_color)
    find('.pcr-save', match: :first).click
    click_button 'Submit'
    expect(current_path).to include('/calendars/' + @calendar_private.id.to_s)
    expect(page).to have_content('Calendar was successfully updated.')
    expect(page).to_not have_content(@calendar_private.title)
    expect(page).to_not have_content(@calendar_private.description)
    expect(page).to_not have_content('Private')
    expect(page).to have_content(title)
    expect(page).to have_content(description)
    expect(page).to have_content('Internal')
    expect(Calendar.last.background_color).to eq(background_color.upcase)
  end

  scenario 'with valid data as owner', js: true do
    sign_in @owner
    visit calendar_path(@calendar_private.id)
    click_link 'Edit calendar'
    expect(page).to have_button('Delete Calendar')
    fill_in 'calendar_title', with: title
    fill_in 'calendar_description', with: description
    choose('calendar_visibility_1')
    find('.pcr-button', match: :first).click
    find('.pcr-result', match: :first).set(background_color)
    find('.pcr-save', match: :first).click
    click_button 'Submit'
    expect(current_path).to include('/calendars/' + @calendar_private.id.to_s)
    expect(page).to have_content('Calendar was successfully updated.')
    expect(page).to_not have_content(@calendar_private.title)
    expect(page).to_not have_content(@calendar_private.description)
    expect(page).to_not have_content('Private')
    expect(page).to have_content(title)
    expect(page).to have_content(description)
    expect(page).to have_content('Internal')
    expect(Calendar.last.background_color).to eq(background_color.upcase)
  end

  scenario 'with valid data as collaborator', js: true do
    sign_in @collaborator
    visit calendar_path(@calendar_private.id)
    click_link 'Edit calendar'
    expect(page).to_not have_button('Delete Calendar')
    fill_in 'calendar_title', with: title
    fill_in 'calendar_description', with: description
    choose('calendar_visibility_1')
    find('.pcr-button', match: :first).click
    find('.pcr-result', match: :first).set(background_color)
    find('.pcr-save', match: :first).click
    click_button 'Submit'
    expect(current_path).to include('/calendars/' + @calendar_private.id.to_s)
    expect(page).to have_content('Calendar was successfully updated.')
    expect(page).to_not have_content(@calendar_private.title)
    expect(page).to_not have_content(@calendar_private.description)
    expect(page).to_not have_content('Private')
    expect(page).to have_content(title)
    expect(page).to have_content(description)
    expect(page).to have_content('Internal')
    expect(Calendar.last.background_color).to eq(background_color.upcase)
  end

  scenario 'with valid data as group member', js: true do
    sign_in @group_member
    visit calendar_path(@calendar_private.id)
    click_link 'Edit calendar'
    expect(page).to_not have_button('Delete Calendar')
    fill_in 'calendar_title', with: title
    fill_in 'calendar_description', with: description
    choose('calendar_visibility_1')
    find('.pcr-button', match: :first).click
    find('.pcr-result', match: :first).set(background_color)
    find('.pcr-save', match: :first).click
    click_button 'Submit'
    expect(current_path).to include('/calendars/' + @calendar_private.id.to_s)
    expect(page).to have_content('Calendar was successfully updated.')
    expect(page).to_not have_content(@calendar_private.title)
    expect(page).to_not have_content(@calendar_private.description)
    expect(page).to_not have_content('Private')
    expect(page).to have_content(title)
    expect(page).to have_content(description)
    expect(page).to have_content('Internal')
    expect(Calendar.last.background_color).to eq(background_color.upcase)
  end

  scenario 'with valid data as group owner', js: true do
    sign_in @group_owner
    visit calendar_path(@calendar_private.id)
    click_link 'Edit calendar'
    expect(page).to_not have_button('Delete Calendar')
    fill_in 'calendar_title', with: title
    fill_in 'calendar_description', with: description
    choose('calendar_visibility_1')
    find('.pcr-button', match: :first).click
    find('.pcr-result', match: :first).set(background_color)
    find('.pcr-save', match: :first).click
    click_button 'Submit'
    expect(current_path).to include('/calendars/' + @calendar_private.id.to_s)
    expect(page).to have_content('Calendar was successfully updated.')
    expect(page).to_not have_content(@calendar_private.title)
    expect(page).to_not have_content(@calendar_private.description)
    expect(page).to_not have_content('Private')
    expect(page).to have_content(title)
    expect(page).to have_content(description)
    expect(page).to have_content('Internal')
    expect(Calendar.last.background_color).to eq(background_color.upcase)
  end

  scenario 'with invalid data as admin', js: true do
    sign_in @admin
    visit calendar_path(@calendar_private.id)
    click_link 'Edit calendar'
    fill_in 'calendar_title', with: ''
    fill_in 'calendar_description', with: description
    choose('calendar_visibility_1')
    click_button 'Submit'
    expect(current_path).to include('/calendars/' + @calendar_private.id.to_s + '/edit')
    expect(page).to have_content('1 error prohibited this calendar from being saved:')
    expect(page).to have_content("Title can't be blank")
  end

  scenario 'with invalid data as owner', js: true do
    sign_in @owner
    visit calendar_path(@calendar_private.id)
    click_link 'Edit calendar'
    fill_in 'calendar_title', with: ''
    fill_in 'calendar_description', with: description
    choose('calendar_visibility_1')
    click_button 'Submit'
    expect(current_path).to include('/calendars/' + @calendar_private.id.to_s + '/edit')
    expect(page).to have_content('1 error prohibited this calendar from being saved:')
    expect(page).to have_content("Title can't be blank")
  end

  scenario 'with invalid data as collaborator', js: true do
    sign_in @collaborator
    visit calendar_path(@calendar_private.id)
    click_link 'Edit calendar'
    fill_in 'calendar_title', with: ''
    fill_in 'calendar_description', with: description
    choose('calendar_visibility_1')
    click_button 'Submit'
    expect(current_path).to include('/calendars/' + @calendar_private.id.to_s + '/edit')
    expect(page).to have_content('1 error prohibited this calendar from being saved:')
    expect(page).to have_content("Title can't be blank")
  end

  scenario 'with invalid data as group member', js: true do
    sign_in @group_member
    visit calendar_path(@calendar_private.id)
    click_link 'Edit calendar'
    fill_in 'calendar_title', with: ''
    fill_in 'calendar_description', with: description
    choose('calendar_visibility_1')
    click_button 'Submit'
    expect(current_path).to include('/calendars/' + @calendar_private.id.to_s + '/edit')
    expect(page).to have_content('1 error prohibited this calendar from being saved:')
    expect(page).to have_content("Title can't be blank")
  end

  scenario 'with invalid data as group owner', js: true do
    sign_in @group_owner
    visit calendar_path(@calendar_private.id)
    click_link 'Edit calendar'
    fill_in 'calendar_title', with: ''
    fill_in 'calendar_description', with: description
    choose('calendar_visibility_1')
    click_button 'Submit'
    expect(current_path).to include('/calendars/' + @calendar_private.id.to_s + '/edit')
    expect(page).to have_content('1 error prohibited this calendar from being saved:')
    expect(page).to have_content("Title can't be blank")
  end
end
