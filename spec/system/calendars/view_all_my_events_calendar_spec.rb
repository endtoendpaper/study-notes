require 'rails_helper'

describe 'Views all my events', type: :system do
  before(:each) do
    reset_elasticsearch_indices

    @owner = FactoryBot.create(:user, :confirmed)
    @group1 = FactoryBot.create(:group, user_id: @owner.id)
    @group2 = FactoryBot.create(:group)
    @group2.add_member(@owner, @group2.user)

    @calendar1 = FactoryBot.create(:calendar, :private, description: 'my text 1')
    @calendar2 = FactoryBot.create(:calendar, :internal, description: 'my text 2')
    @calendar3 = FactoryBot.create(:calendar, :public, description: 'my text 3')
    @calendar4 = FactoryBot.create(:calendar, :public, description: 'my text 3')

    @calendar1.collaborate(@owner, @calendar1.user)
    @calendar2.add_group(@group1, @calendar2.user)
    @calendar3.add_group(@group2, @calendar3.user)

    @date = DateTime.current.in_time_zone(@owner.time_zone)
    @prev_date = @date - 1.month
    @next_date = @date + 1.month
    event_start = DateTime.new(@date.year, @date.month, 15, 7, 10, 0, @owner.time_zone)

    @private_multiday_event = @calendar1.events.create(
        summary: "private_multiday_event-#{Time.current.to_i}",
        start: event_start,
        end: event_start + 3.days
      )
    @private_all_day_multiday_event = @calendar2.events.create(
        summary: "private_all_day_multiday_event-#{Time.current.to_i}",
        start: event_start,
        end: event_start + 3.days,
        all_day: true
      )
    @private_event = @calendar3.events.create(
        summary: "private_event-#{Time.current.to_i}",
        start: event_start,
        end: event_start + 30.minutes
      )

    @event_for_calendar4 = @calendar4.events.create(
        summary: "event_for_calendar4-#{Time.current.to_i}",
        start: event_start,
        end: event_start + 3.days
      )

    @private_prev_month_multiday_event = @calendar1.events.create(
        summary: "private_prev_month_multiday_event-#{Time.current.to_i}",
        start: event_start - 1.month,
        end: event_start - 1.month + 3.days
      )
    @private_prev_month_all_day_multiday_event = @calendar2.events.create(
        summary: "private_prev_month_all_day_multiday_event-#{Time.current.to_i}",
        start: event_start - 1.month,
        end: event_start - 1.month + 3.days,
        all_day: true
      )
    @private_prev_month_event = @calendar3.events.create(
        summary: "private_prev_month_event-#{Time.current.to_i}",
        start: event_start - 1.month,
        end: event_start - 1.month + 30.minutes
      )

    @private_next_month_multiday_event = @calendar1.events.create(
        summary: "private_next_month_multiday_event-#{Time.current.to_i}",
        start: event_start + 1.month,
        end: event_start + 1.month + 3.days
      )
    @private_next_month_all_day_multiday_event = @calendar2.events.create(
        summary: "private_next_month_all_day_multiday_event-#{Time.current.to_i}",
        start: event_start + 1.month,
        end: event_start + 1.month + 3.days, all_day: true
      )
    @private_next_month_event = @calendar3.events.create(
        summary: "private_next_month_event-#{Time.current.to_i}",
        start: event_start + 1.month,
        end: event_start + 1.month + 30.minutes
      )

    reindex_elasticsearch_data
  end

  scenario 'as user' do
    sign_in @owner
    visit user_calendar_path(year: @date.year, month: @date.month)
    expect(page).to_not have_link('Show calendar')
    expect(page).to_not have_content(@owner.handle)
    expect(page).to_not have_content(@group1.name)
    expect(page).to_not have_content('Private')
    expect(page).to_not have_content(@calendar1.title)
    expect(page).to_not have_content('my text 1')
    expect(page).to_not have_link('Edit calendar')
    expect(page).to_not have_link('Export ICS')
    expect(page).to_not have_link('Edit collaborators')
    expect(page).to_not have_link('Collaboration Log')
    expect(page).to have_link(@owner.time_zone)
    expect(page).to_not have_content(@calendar2.updated_at .in_time_zone(@owner.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to_not have_link('Add event')
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    expect(page).to have_link('Previous')
    expect(page).to have_link('Next')
    expect(page).to have_link('Today')
    expect(page).to have_link('1')
    expect(page).to have_link('28')
    expect(html).to include("background-color:" + @calendar1.background_color + "; border-color: darken(" +  + @calendar1.background_color + "); color:"  + @calendar1.text_color)
    expect(html).to include("background-color:" + @calendar2.background_color + "; border-color: darken(" +  + @calendar2.background_color + "); color:"  + @calendar2.text_color)
    expect(html).to include("background-color:" + @calendar3.background_color + "; border-color: darken(" +  + @calendar3.background_color + "); color:"  + @calendar3.text_color)
    expect(html).to_not include("background-color:" + @calendar4.background_color + "; border-color: darken(" +  + @calendar4.background_color + "); color:"  + @calendar4.text_color)
    expect(page).to_not have_link('View all events')
    expect(page).to have_link(@private_multiday_event.summary)
    expect(page).to have_link(@private_all_day_multiday_event.summary)
    expect(page).to have_link(@private_event.summary)
    expect(page).to_not have_link(@private_prev_month_multiday_event.summary)
    expect(page).to_not have_link(@private_prev_month_all_day_multiday_event.summary)
    expect(page).to_not have_link(@private_prev_month_event.summary)
    expect(page).to_not have_link(@private_next_month_multiday_event.summary)
    expect(page).to_not have_link(@private_next_month_all_day_multiday_event.summary)
    expect(page).to_not have_link(@private_next_month_event.summary)
    expect(page).to_not have_link(@event_for_calendar4.summary)
    click_link 'Previous'
    expect(page).to have_content(@prev_date.strftime("%B") + " " + @prev_date.strftime("%Y"))
    expect(page).to have_link('Previous')
    expect(page).to have_link('Next')
    expect(page).to have_link('Today')
    expect(page).to have_link('1')
    expect(page).to have_link('28')
    expect(html).to include("background-color:" + @calendar1.background_color + "; border-color: darken(" +  + @calendar1.background_color + "); color:"  + @calendar1.text_color)
    expect(html).to include("background-color:" + @calendar2.background_color + "; border-color: darken(" +  + @calendar2.background_color + "); color:"  + @calendar2.text_color)
    expect(html).to include("background-color:" + @calendar3.background_color + "; border-color: darken(" +  + @calendar3.background_color + "); color:"  + @calendar3.text_color)
    expect(html).to_not include("background-color:" + @calendar4.background_color + "; border-color: darken(" +  + @calendar4.background_color + "); color:"  + @calendar4.text_color)
    expect(page).to_not have_link(@private_multiday_event.summary)
    expect(page).to_not have_link(@private_all_day_multiday_event.summary)
    expect(page).to_not have_link(@private_event.summary)
    expect(page).to have_link(@private_prev_month_multiday_event.summary)
    expect(page).to have_link(@private_prev_month_all_day_multiday_event.summary)
    expect(page).to have_link(@private_prev_month_event.summary)
    expect(page).to_not have_link(@private_next_month_multiday_event.summary)
    expect(page).to_not have_link(@private_next_month_all_day_multiday_event.summary)
    expect(page).to_not have_link(@private_next_month_event.summary)
    expect(page).to_not have_link(@event_for_calendar4.summary)
    click_link 'Today'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link 'Next'
    expect(page).to have_content(@next_date.strftime("%B") + " " + @next_date.strftime("%Y"))
    expect(page).to have_link('Previous')
    expect(page).to have_link('Next')
    expect(page).to have_link('Today')
    expect(page).to have_link('1')
    expect(page).to have_link('28')
    expect(html).to include("background-color:" + @calendar1.background_color + "; border-color: darken(" +  + @calendar1.background_color + "); color:"  + @calendar1.text_color)
    expect(html).to include("background-color:" + @calendar2.background_color + "; border-color: darken(" +  + @calendar2.background_color + "); color:"  + @calendar2.text_color)
    expect(html).to include("background-color:" + @calendar3.background_color + "; border-color: darken(" +  + @calendar3.background_color + "); color:"  + @calendar3.text_color)
    expect(html).to_not include("background-color:" + @calendar4.background_color + "; border-color: darken(" +  + @calendar4.background_color + "); color:"  + @calendar4.text_color)
    expect(page).to_not have_link(@private_multiday_event.summary)
    expect(page).to_not have_link(@private_all_day_multiday_event.summary)
    expect(page).to_not have_link(@private_event.summary)
    expect(page).to_not have_link(@private_prev_month_multiday_event.summary)
    expect(page).to_not have_link(@private_prev_month_all_day_multiday_event.summary)
    expect(page).to_not have_link(@private_prev_month_event.summary)
    expect(page).to_not have_link(@event_for_calendar4.summary)
    expect(page).to have_link(@private_next_month_multiday_event.summary)
    expect(page).to have_link(@private_next_month_all_day_multiday_event.summary)
    expect(page).to have_link(@private_next_month_event.summary)
  end
end
