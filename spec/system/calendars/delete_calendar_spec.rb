require 'rails_helper'

describe 'Deletes calendar', type: :system do
  before(:each) do
    @owner = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
  end

  scenario 'as admin', js: true do
    sign_in @admin
    visit calendar_path(@calendar_public.id)
    click_link 'Edit calendar'
    click_button 'Delete Calendar'
    page.accept_alert
    expect(current_path).to include('/calendars')
    expect(page).to have_content('Calendar was successfully deleted.')
    visit calendars_path
    expect(page).to_not have_content(@calendar_public.title)
  end

  scenario 'as owner', js: true do
    sign_in @owner
    visit calendar_path(@calendar_public.id)
    click_link 'Edit calendar'
    click_button 'Delete Calendar'
    page.accept_alert
    expect(current_path).to include('/calendars')
    expect(page).to have_content('Calendar was successfully deleted.')
    visit calendars_path
    expect(page).to_not have_content(@calendar_public.title)
  end
end
