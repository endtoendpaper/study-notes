require 'rails_helper'

describe 'Views calendars index page', type: :system do
  before(:each) do
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group_owner = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group, user_id: @group_owner.id)
    @group.add_member(@group_member, @group.user)

    reset_elasticsearch_indices(['calendars'])

    @calendar_private = FactoryBot.create(:calendar, :private, user_id: @owner.id, description: 'my text 1')
    @calendar_internal = FactoryBot.create(:calendar, :internal, user_id: @owner.id, description: 'my text 2')
    @calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id, description: 'my text 3')
    @calendar_private.collaborate(@collaborator, @calendar_private.user)
    @calendar_private.add_group(@group, @calendar_private.user)
    @calendar_internal.collaborate(@collaborator, @calendar_private.user)
    @calendar_internal.add_group(@group, @calendar_private.user)
    @calendar_public.collaborate(@collaborator, @calendar_private.user)
    @calendar_public.add_group(@group, @calendar_private.user)

    reindex_elasticsearch_data(['calendars'])
  end

  scenario 'there is pagination' do
    12.times do
      FactoryBot.create(:calendar, :private)
    end
    reindex_elasticsearch_data(['calendars'])

    sign_in @admin
    visit calendars_path
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end

  scenario 'as admin' do
    sign_in @admin
    visit calendars_path
    expect(page).to have_content(@owner.handle)
    expect(page).to_not have_content(@collaborator.handle)
    expect(page).to_not have_content(@group.name)
    expect(page).to have_link('Show calendar')
    expect(page).to have_content(@calendar_private.title)
    expect(page).to have_content('my text 1')
    expect(page).to have_content('Public')
    expect(page).to have_content(@calendar_internal.title)
    expect(page).to have_content('my text 2')
    expect(page).to have_content('Internal')
    expect(page).to have_content(@calendar_public.title)
    expect(page).to have_content('my text 3')
    expect(page).to have_content('Private')
    expect(page).to_not have_link('Edit calendar')
    expect(page).to_not have_link('Add event')
  end

  scenario 'as owner' do
    sign_in @owner
    visit calendars_path
    expect(page).to have_content(@owner.handle)
    expect(page).to_not have_content(@collaborator.handle)
    expect(page).to_not have_content(@group.name)
    expect(page).to have_link('Show calendar')
    expect(page).to have_content(@calendar_private.title)
    expect(page).to have_content('my text 1')
    expect(page).to have_content('Public')
    expect(page).to have_content(@calendar_internal.title)
    expect(page).to have_content('my text 2')
    expect(page).to have_content('Internal')
    expect(page).to have_content(@calendar_public.title)
    expect(page).to have_content('my text 3')
    expect(page).to have_content('Private')
    expect(page).to_not have_link('Edit calendar')
    expect(page).to_not have_link('Add event')
  end

  scenario 'as collaborator' do
    sign_in @collaborator
    visit calendars_path
    expect(page).to have_content(@owner.handle)
    expect(page).to_not have_content(@collaborator.handle)
    expect(page).to_not have_content(@group.name)
    expect(page).to have_link('Show calendar')
    expect(page).to have_content(@calendar_private.title)
    expect(page).to have_content('my text 1')
    expect(page).to have_content('Public')
    expect(page).to have_content(@calendar_internal.title)
    expect(page).to have_content('my text 2')
    expect(page).to have_content('Internal')
    expect(page).to have_content(@calendar_public.title)
    expect(page).to have_content('my text 3')
    expect(page).to have_content('Private')
    expect(page).to_not have_link('Edit calendar')
    expect(page).to_not have_link('Add event')
  end

  scenario 'as group member' do
    sign_in @group_member
    visit calendars_path
    expect(page).to have_content(@owner.handle)
    expect(page).to_not have_content(@collaborator.handle)
    expect(page).to_not have_content(@group.name)
    expect(page).to have_link('Show calendar')
    expect(page).to have_content(@calendar_private.title)
    expect(page).to have_content('my text 1')
    expect(page).to have_content('Public')
    expect(page).to have_content(@calendar_internal.title)
    expect(page).to have_content('my text 2')
    expect(page).to have_content('Internal')
    expect(page).to have_content(@calendar_public.title)
    expect(page).to have_content('my text 3')
    expect(page).to have_content('Private')
    expect(page).to_not have_link('Edit calendar')
    expect(page).to_not have_link('Add event')
  end

  scenario 'as group owner' do
    sign_in @group_owner
    visit calendars_path
    expect(page).to have_content(@owner.handle)
    expect(page).to_not have_content(@collaborator.handle)
    expect(page).to_not have_content(@group.name)
    expect(page).to have_link('Show calendar')
    expect(page).to have_content(@calendar_private.title)
    expect(page).to have_content('my text 1')
    expect(page).to have_content('Public')
    expect(page).to have_content(@calendar_internal.title)
    expect(page).to have_content('my text 2')
    expect(page).to have_content('Internal')
    expect(page).to have_content(@calendar_public.title)
    expect(page).to have_content('my text 3')
    expect(page).to have_content('Private')
    expect(page).to_not have_link('Edit calendar')
    expect(page).to_not have_link('Add event')
  end

  scenario 'as other user' do
    sign_in @user
    visit calendars_path
    expect(page).to have_content(@owner.handle)
    expect(page).to_not have_content(@collaborator.handle)
    expect(page).to_not have_content(@group.name)
    expect(page).to have_link('Show calendar')
    expect(page).to_not have_content(@calendar_private.title)
    expect(page).to_not have_content('my text 1')
    expect(page).to_not have_content('Public')
    expect(page).to have_content(@calendar_internal.title)
    expect(page).to have_content('my text 2')
    expect(page).to_not have_content('Internal')
    expect(page).to have_content(@calendar_public.title)
    expect(page).to have_content('my text 3')
    expect(page).to_not have_content('Private')
    expect(page).to_not have_link('Edit calendar')
    expect(page).to_not have_link('Add event')
  end

  scenario 'as not logged in' do
    visit calendars_path
    expect(page).to have_content(@owner.handle)
    expect(page).to_not have_content(@collaborator.handle)
    expect(page).to_not have_content(@group.name)
    expect(page).to have_link('Show calendar')
    expect(page).to_not have_content(@calendar_private.title)
    expect(page).to_not have_content('my text 1')
    expect(page).to_not have_content('Public')
    expect(page).to_not have_content(@calendar_internal.title)
    expect(page).to_not have_content('my text 2')
    expect(page).to_not have_content('Internal')
    expect(page).to have_content(@calendar_public.title)
    expect(page).to have_content('my text 3')
    expect(page).to_not have_content('Private')
    expect(page).to_not have_link('Edit calendar')
    expect(page).to_not have_link('Add event')
  end
end
