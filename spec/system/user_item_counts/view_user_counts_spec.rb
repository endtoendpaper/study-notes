require 'rails_helper'

describe 'Views user item counts', type: :system do
  before(:each) do
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group_owner = FactoryBot.create(:user, :confirmed)
    @group_member = FactoryBot.create(:user, :confirmed)

    users_group1 = FactoryBot.create(:group, user_id: @owner.id)
    users_group2 = FactoryBot.create(:group)
    users_group2.add_member(@owner, users_group2.user)

    group = FactoryBot.create(:group, user_id: @group_owner.id)
    group.add_member(@group_member, group.user)

    reset_elasticsearch_indices

    UserItems.item_list.each do |item|
      private = FactoryBot.create(item.singularize, :private, user_id: @owner.id)
      internal = FactoryBot.create(item.singularize, :internal, user_id: @owner.id)
      public = FactoryBot.create(item.singularize, :public, user_id: @owner.id)

      private.collaborate(@collaborator, @admin)
      internal.collaborate(@collaborator, @admin)
      public.collaborate(@collaborator, @admin)

      private.add_group(group, group.user)
      internal.add_group(group, group.user)
      public.add_group(group, group.user)

      private.add_star(@owner)
      internal.add_star(@owner)
      public.add_star(@owner)
    end

    reindex_elasticsearch_data
  end

  scenario 'as admin' do
    sign_in @admin
    visit user_path(@owner)
    UserItems.item_list.each do |item|
      expect(page).to have_link("3 " + item.capitalize)
    end
    expect(page).to have_link("#{UserItems.item_list.count * 3} Stars")
    expect(page).to have_link("2 Groups")
    visit user_path(@user)
    UserItems.item_list.each do |item|
      expect(page).to have_link("0 " + item.capitalize)
    end
    expect(page).to have_link("0 Stars")
    expect(page).to have_link("0 Groups")
  end

  scenario 'as owner' do
    sign_in @owner
    visit user_path(@owner)
    UserItems.item_list.each do |item|
      expect(page).to have_link("3 " + item.capitalize)
    end
    expect(page).to have_link("#{UserItems.item_list.count * 3} Stars")
    expect(page).to have_link("2 Groups")
    visit user_path(@user)
    UserItems.item_list.each do |item|
      expect(page).to have_link("0 " + item.capitalize)
    end
    expect(page).to have_link("0 Stars")
    expect(page).to have_link("0 Groups")
  end

  scenario 'as item collaborator' do
    sign_in @collaborator
    visit user_path(@owner)
    visit user_path(@owner)
    UserItems.item_list.each do |item|
      expect(page).to have_link("3 " + item.capitalize)
    end
    expect(page).to have_link("#{UserItems.item_list.count * 3} Stars")
    expect(page).to have_link("2 Groups")
    visit user_path(@user)
    UserItems.item_list.each do |item|
      expect(page).to have_link("0 " + item.capitalize)
    end
    expect(page).to have_link("0 Stars")
    expect(page).to have_link("0 Groups")
  end

  scenario 'as item group owner' do
    sign_in @group_owner
    visit user_path(@owner)
    visit user_path(@owner)
    UserItems.item_list.each do |item|
      expect(page).to have_link("3 " + item.capitalize)
    end
    expect(page).to have_link("#{UserItems.item_list.count * 3} Stars")
    expect(page).to have_link("2 Groups")
    visit user_path(@user)
    UserItems.item_list.each do |item|
      expect(page).to have_link("0 " + item.capitalize)
    end
    expect(page).to have_link("0 Stars")
    expect(page).to have_link("0 Groups")
  end

  scenario 'as item group member' do
    sign_in @group_member
    visit user_path(@owner)
    visit user_path(@owner)
    UserItems.item_list.each do |item|
      expect(page).to have_link("3 " + item.capitalize)
    end
    expect(page).to have_link("#{UserItems.item_list.count * 3} Stars")
    expect(page).to have_link("2 Groups")
    visit user_path(@user)
    UserItems.item_list.each do |item|
      expect(page).to have_link("0 " + item.capitalize)
    end
    expect(page).to have_link("0 Stars")
    expect(page).to have_link("0 Groups")
  end

  scenario 'as user' do
    sign_in @user
    visit user_path(@owner)
    visit user_path(@owner)
    visit user_path(@owner)
    UserItems.item_list.each do |item|
      expect(page).to have_link("2 " + item.capitalize)
    end
    expect(page).to have_link("#{UserItems.item_list.count * 2} Stars")
    expect(page).to have_link("2 Groups")
    visit user_path(@user)
    UserItems.item_list.each do |item|
      expect(page).to have_link("0 " + item.capitalize)
    end
    expect(page).to have_link("0 Stars")
    expect(page).to have_link("0 Groups")
  end

  scenario 'not logged in' do
    visit user_path(@owner)
    visit user_path(@owner)
    visit user_path(@owner)
    UserItems.item_list.each do |item|
      expect(page).to have_link("1 " + item.capitalize.singularize)
    end
    expect(page).to have_link("#{UserItems.item_list.count} Stars")
    expect(page).to_not have_link("2 Groups")
    visit user_path(@user)
    UserItems.item_list.each do |item|
      expect(page).to have_link("0 " + item.capitalize)
    end
    expect(page).to have_link("0 Stars")
    expect(page).to_not have_link("0 Groups")
  end

  scenario 'as admin when user hides stars' do
    @owner.update(hide_stars: true)
    sign_in @admin
    visit user_path(@owner)
    expect(page).to have_link("#{UserItems.item_list.count * 3} Stars")
  end

  scenario 'as owner when user hides stars' do
    @owner.update(hide_stars: true)
    sign_in @owner
    visit user_path(@owner)
    expect(page).to have_link("#{UserItems.item_list.count * 3} Stars")
  end

  scenario 'as item collaborator when user hides stars' do
    @owner.update(hide_stars: true)
    sign_in @collaborator
    visit user_path(@owner)
    expect(page).to_not have_link("#{UserItems.item_list.count * 3} Stars")
  end

  scenario 'as item group owner when user hides stars' do
    @owner.update(hide_stars: true)
    sign_in @group_owner
    visit user_path(@owner)
    expect(page).to_not have_link("#{UserItems.item_list.count * 3} Stars")
  end

  scenario 'as item group member when user hides stars' do
    @owner.update(hide_stars: true)
    sign_in @group_member
    visit user_path(@owner)
    expect(page).to_not have_link("#{UserItems.item_list.count * 3} Stars")
  end

  scenario 'as user when user hides stars' do
    @owner.update(hide_stars: true)
    sign_in @user
    visit user_path(@owner)
    expect(page).to_not have_link("#{UserItems.item_list.count * 2} Stars")
  end

  scenario 'not logged in user hides stars' do
    @owner.update(hide_stars: true)
    visit user_path(@owner)
    expect(page).to_not have_link("#{UserItems.item_list.count} Stars")
  end
end
