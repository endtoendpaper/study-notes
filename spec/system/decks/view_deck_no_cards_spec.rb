require 'rails_helper'

describe 'Views deck', type: :system do
  before(:each) do
    reset_elasticsearch_indices(['decks'])

    @user1 = FactoryBot.create(:user, :confirmed, time_zone: 'UTC')
    @user2 = FactoryBot.create(:user, :confirmed, time_zone: 'UTC')
    @user3 = FactoryBot.create(:user, :confirmed, time_zone: 'UTC')
    @admin = FactoryBot.create(:user, :confirmed, :admin, time_zone: 'UTC')
    @deck_public1 = FactoryBot.create(:deck, :public, user_id: @user1.id, description: 'my rich text 1 user1')
    @deck_internal1 = FactoryBot.create(:deck, :internal, user_id: @user1.id, description: 'my rich text 2 user1')
    @deck_private1 = FactoryBot.create(:deck, :private, user_id: @user1.id, description: 'my rich text 3 user1')
    @deck_public2 = FactoryBot.create(:deck, :public, user_id: @user2.id, description: 'my rich text 1 user2')
    @deck_internal2 = FactoryBot.create(:deck, :internal, user_id: @user2.id, description: 'my rich text 2 user2')
    @deck_private2 = FactoryBot.create(:deck, :private, user_id: @user2.id, description: 'my rich text 3 user2')
    @group_member = FactoryBot.create(:user, :confirmed, time_zone: 'UTC')
    @collaborator = FactoryBot.create(:user, :confirmed, time_zone: 'UTC')
    @group = FactoryBot.create(:group)
    @deck_private1.collaborate(@collaborator, @deck_private1.user)
    @deck_private1.add_group(@group, @deck_private1.user)
    @group.add_member(@group_member, @group.user)

    reindex_elasticsearch_data(['decks'])
  end

  scenario 'on index page there is pagination as admin' do
    13.times do
      FactoryBot.create(:deck, :public, user_id: @user1.id)
    end
    reindex_elasticsearch_data(['decks'])
    sign_in @admin
    visit decks_path
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end

  scenario 'on user index page there is pagination as admin' do
    13.times do
      FactoryBot.create(:deck, :public, user_id: @user1.id)
    end
    reindex_elasticsearch_data(['decks'])
    sign_in @admin
    visit '/users/' + @user1.username + '/decks'
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end

  scenario 'on index page there is pagination as user' do
    13.times do
      FactoryBot.create(:deck, :public, user_id: @user1.id)
    end
    reindex_elasticsearch_data(['decks'])
    sign_in @user2
    visit decks_path
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end

  scenario 'on user index page there is pagination as user' do
    13.times do
      FactoryBot.create(:deck, :public, user_id: @user1.id)
    end
    reindex_elasticsearch_data(['decks'])
    sign_in @user2
    visit '/users/' + @user1.username + '/decks'
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end

  scenario 'on index page there is pagination not logged in' do
    13.times do
      FactoryBot.create(:deck, :public, user_id: @user1.id)
    end
    reindex_elasticsearch_data(['decks'])
    visit decks_path
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end

  scenario 'on user index page there is pagination not logged in' do
    13.times do
      FactoryBot.create(:deck, :public, user_id: @user1.id)
    end
    reindex_elasticsearch_data(['decks'])
    visit '/users/' + @user1.username + '/decks'
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end

  scenario 'on index page as admin' do
    sign_in @admin
    visit decks_path
    expect(page).to have_content(@user1.handle)
    expect(page).to have_link('Show deck')
    expect(page).to have_content(@deck_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to have_content('Public')
    expect(page).to have_content(@deck_internal1.title)
    expect(page).to have_content('my rich text 2 user2')
    expect(page).to have_content('Internal')
    expect(page).to have_content(@deck_private1.title)
    expect(page).to have_content('my rich text 3')
    expect(page).to have_content('Private')
    expect(html).to_not include('<i class="bi bi-book"></i>Study')
    expect(page).to_not have_link('Edit deck')
    expect(page).to_not have_link('Add card')
    expect(page).to_not have_link('List cards')
    expect(page).to_not have_link('Edit collaborators')
    expect(page).to_not have_link('Edit tags')
    expect(page).to_not have_link('Add tags')
  end

  scenario 'on index page as user who has private deck' do
    sign_in @user1
    visit decks_path
    expect(page).to have_content(@user1.handle)
    expect(page).to have_link('Show deck')
    expect(page).to have_content(@deck_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to have_content('Public')
    expect(page).to have_content(@deck_internal1.title)
    expect(page).to have_content('my rich text 2 user2')
    expect(page).to have_content('Internal')
    expect(page).to have_content(@deck_private1.title)
    expect(page).to have_content('my rich text 3')
    expect(page).to have_content('Private')
    expect(html).to_not include('<i class="bi bi-book"></i>Study')
    expect(page).to_not have_link('Edit deck')
    expect(page).to_not have_link('Add card')
    expect(page).to_not have_link('List cards')
    expect(page).to_not have_link('Edit collaborators')
    expect(page).to_not have_link('Edit tags')
    expect(page).to_not have_link('Add tags')
  end

  scenario 'on index page as collaborator of private deck' do
    sign_in @collaborator
    visit decks_path
    expect(page).to have_content(@user1.handle)
    expect(page).to have_link('Show deck')
    expect(page).to have_content(@deck_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to_not have_content('Public')
    expect(page).to have_content(@deck_internal1.title)
    expect(page).to have_content('my rich text 2 user2')
    expect(page).to_not have_content('Internal')
    expect(page).to have_content(@deck_private1.title)
    expect(page).to have_content('my rich text 3')
    expect(page).to have_content('Private')
    expect(html).to_not include('<i class="bi bi-book"></i>Study')
    expect(page).to_not have_link('Edit deck')
    expect(page).to_not have_link('Add card')
    expect(page).to_not have_link('List cards')
    expect(page).to_not have_link('Edit collaborators')
    expect(page).to_not have_link('Edit tags')
    expect(page).to_not have_link('Add tags')
  end

  scenario 'on index page as group member of private deck' do
    sign_in @collaborator
    visit decks_path
    expect(page).to have_content(@user1.handle)
    expect(page).to have_link('Show deck')
    expect(page).to have_content(@deck_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to_not have_content('Public')
    expect(page).to have_content(@deck_internal1.title)
    expect(page).to have_content('my rich text 2 user2')
    expect(page).to_not have_content('Internal')
    expect(page).to have_content(@deck_private1.title)
    expect(page).to have_content('my rich text 3')
    expect(page).to have_content('Private')
    expect(html).to_not include('<i class="bi bi-book"></i>Study')
    expect(page).to_not have_link('Edit deck')
    expect(page).to_not have_link('Add card')
    expect(page).to_not have_link('List cards')
    expect(page).to_not have_link('Edit collaborators')
    expect(page).to_not have_link('Edit tags')
    expect(page).to_not have_link('Add tags')
  end

  scenario "on index page as user who shouldn't see other's private decks" do
    sign_in @user3
    visit decks_path
    expect(page).to have_content(@user1.handle)
    expect(page).to have_link('Show deck')
    expect(page).to have_content(@deck_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to_not have_content('Public')
    expect(page).to have_content(@deck_internal1.title)
    expect(page).to have_content('my rich text 2 user2')
    expect(page).to_not have_content('Internal')
    expect(page).to_not have_content(@deck_private1.title)
    expect(page).to_not have_content('my rich text 3')
    expect(page).to_not have_content('Private')
    expect(html).to_not include('<i class="bi bi-book"></i>Study')
    expect(page).to_not have_link('Edit deck')
    expect(page).to_not have_link('Add card')
    expect(page).to_not have_link('List cards')
    expect(page).to_not have_link('Edit collaborators')
    expect(page).to_not have_link('Edit tags')
    expect(page).to_not have_link('Add tags')
  end

  scenario 'on index page not logged in' do
    visit decks_path
    expect(page).to have_content(@user1.handle)
    expect(page).to have_link('Show deck')
    expect(page).to have_content(@deck_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to_not have_content('Public')
    expect(page).to_not have_content(@deck_internal1.title)
    expect(page).to_not have_content('my rich text 2 user1')
    expect(page).to_not have_content('Internal')
    expect(page).to_not have_content(@deck_private1.title)
    expect(page).to_not have_content('my rich text 3')
    expect(page).to_not have_content('Private')
    expect(html).to_not include('<i class="bi bi-book"></i>Study')
    expect(page).to_not have_link('Edit deck')
    expect(page).to_not have_link('Add card')
    expect(page).to_not have_link('List cards')
    expect(page).to_not have_link('Edit collaborators')
    expect(page).to_not have_link('Edit tags')
    expect(page).to_not have_link('Add tags')
  end

  scenario 'on user index page as admin' do
    sign_in @admin
    visit '/users/' + @user1.username + '/decks'
    expect(page).to have_content(@user1.handle)
    expect(page).to have_link('Show deck')
    expect(page).to have_content(@deck_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to have_content('Public')
    expect(page).to have_content(@deck_internal1.title)
    expect(page).to have_content('my rich text 2 user1')
    expect(page).to have_content('Internal')
    expect(page).to have_content(@deck_private1.title)
    expect(page).to have_content('my rich text 3')
    expect(page).to have_content('Private')
    expect(page).to_not have_content('my rich text 1 user2')
    expect(page).to_not have_content('my rich text 2 user2')
    expect(page).to_not have_content('my rich text 3 user2')
    expect(html).to_not include('<i class="bi bi-book"></i>Study')
    expect(page).to_not have_link('Edit deck')
    expect(page).to_not have_link('Add card')
    expect(page).to_not have_link('List cards')
    expect(page).to_not have_link('Edit collaborators')
    expect(page).to_not have_link('Edit tags')
    expect(page).to_not have_link('Add tags')
  end

  scenario 'on user index page as user who has private deck' do
    sign_in @user1
    visit '/users/' + @user1.username + '/decks'
    expect(page).to have_content(@user1.handle)
    expect(page).to have_link('Show deck')
    expect(page).to have_content(@deck_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to have_content('Public')
    expect(page).to have_content(@deck_internal1.title)
    expect(page).to have_content('my rich text 2 user1')
    expect(page).to have_content('Internal')
    expect(page).to have_content(@deck_private1.title)
    expect(page).to have_content('my rich text 3')
    expect(page).to have_content('Private')
    expect(page).to_not have_content('my rich text 1 user2')
    expect(page).to_not have_content('my rich text 2 user2')
    expect(page).to_not have_content('my rich text 3 user2')
    expect(html).to_not include('<i class="bi bi-book"></i>Study')
    expect(page).to_not have_link('Edit deck')
    expect(page).to_not have_link('Add card')
    expect(page).to_not have_link('List cards')
    expect(page).to_not have_link('Edit collaborators')
    expect(page).to_not have_link('Edit tags')
    expect(page).to_not have_link('Add tags')
  end

  scenario 'on user index page as collaborator of their private deck' do
    sign_in @collaborator
    visit '/users/' + @user1.username + '/decks'
    expect(page).to have_content(@user1.handle)
    expect(page).to have_link('Show deck')
    expect(page).to have_content(@deck_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to have_content(@deck_internal1.title)
    expect(page).to have_content('my rich text 2 user1')
    expect(page).to have_content(@deck_private1.title)
    expect(page).to have_content('my rich text 3')
    expect(page).to have_content('Private')
    expect(page).to_not have_content('my rich text 1 user2')
    expect(page).to_not have_content('my rich text 2 user2')
    expect(page).to_not have_content('my rich text 3 user2')
    expect(html).to_not include('<i class="bi bi-book"></i>Study')
    expect(page).to_not have_link('Edit deck')
    expect(page).to_not have_link('Add card')
    expect(page).to_not have_link('List cards')
    expect(page).to_not have_link('Edit collaborators')
    expect(page).to_not have_link('Edit tags')
    expect(page).to_not have_link('Add tags')
  end

  scenario 'on user index page as group member of their private deck' do
    sign_in @group_member
    visit '/users/' + @user1.username + '/decks'
    expect(page).to have_content(@user1.handle)
    expect(page).to have_link('Show deck')
    expect(page).to have_content(@deck_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to have_content(@deck_internal1.title)
    expect(page).to have_content('my rich text 2 user1')
    expect(page).to have_content(@deck_private1.title)
    expect(page).to have_content('my rich text 3')
    expect(page).to have_content('Private')
    expect(page).to_not have_content('my rich text 1 user2')
    expect(page).to_not have_content('my rich text 2 user2')
    expect(page).to_not have_content('my rich text 3 user2')
    expect(html).to_not include('<i class="bi bi-book"></i>Study')
    expect(page).to_not have_link('Edit deck')
    expect(page).to_not have_link('Add card')
    expect(page).to_not have_link('List cards')
    expect(page).to_not have_link('Edit collaborators')
    expect(page).to_not have_link('Edit tags')
    expect(page).to_not have_link('Add tags')
  end

  scenario 'on user index page as user who sees other users internal/public' do
    sign_in @user2
    visit '/users/' + @user1.username + '/decks'
    expect(page).to have_content(@user1.handle)
    expect(page).to have_link('Show deck')
    expect(page).to have_content(@deck_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to_not have_content('Public')
    expect(page).to have_content(@deck_internal1.title)
    expect(page).to have_content('my rich text 2 user1')
    expect(page).to_not have_content('Internal')
    expect(page).to_not have_content(@deck_private1.title)
    expect(page).to_not have_content('my rich text 3')
    expect(page).to_not have_content('Private')
    expect(page).to_not have_content('my rich text 1 user2')
    expect(page).to_not have_content('my rich text 2 user2')
    expect(page).to_not have_content('my rich text 3 user2')
    expect(html).to_not include('<i class="bi bi-book"></i>Study')
    expect(page).to_not have_link('Edit deck')
    expect(page).to_not have_link('Add card')
    expect(page).to_not have_link('List cards')
    expect(page).to_not have_link('Edit collaborators')
    expect(page).to_not have_link('Edit tags')
    expect(page).to_not have_link('Add tags')
  end

  scenario 'on user index page not logged in' do
    visit '/users/' + @user1.username + '/decks'
    expect(page).to have_content(@user1.handle)
    expect(page).to have_link('Show deck')
    expect(page).to have_content(@deck_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to_not have_content('Public')
    expect(page).to_not have_content(@deck_internal1.title)
    expect(page).to_not have_content('my rich text 2 user1')
    expect(page).to_not have_content('Internal')
    expect(page).to_not have_content(@deck_private1.title)
    expect(page).to_not have_content('my rich text 3')
    expect(page).to_not have_content('Private')
    expect(page).to_not have_content('my rich text 1 user2')
    expect(page).to_not have_content('my rich text 2 user2')
    expect(page).to_not have_content('my rich text 3 user2')
    expect(html).to_not include('<i class="bi bi-book"></i>Study')
    expect(page).to_not have_link('Edit deck')
    expect(page).to_not have_link('Add card')
    expect(page).to_not have_link('List cards')
    expect(page).to_not have_link('Edit collaborators')
    expect(page).to_not have_link('Edit tags')
    expect(page).to_not have_link('Add tags')
  end

  scenario "on show page as admin for user's private deck" do
    sign_in @admin
    visit deck_path(@deck_private1.id)
    expect(page).to_not have_link('Show deck')
    expect(page).to have_content(@user1.handle)
    expect(page).to have_content('Private')
    expect(page).to have_content(@deck_private1.title)
    expect(page).to have_content('my rich text 3')
    expect(page).to have_link('Edit deck')
    expect(page).to have_content(@deck_private1.last_activity_at.strftime('%b %d, %Y %I:%M%p %Z'))
    expect(html).to_not include('<i class="bi bi-book"></i>Study')
    expect(page).to have_link('Add card')
    expect(page).to_not have_link('List cards')
  end

  scenario "on show page as admin for user's internal deck" do
    sign_in @admin
    visit deck_path(@deck_internal1.id)
    expect(page).to_not have_link('Show deck')
    expect(page).to have_content(@user1.handle)
    expect(page).to have_content('Internal')
    expect(page).to have_content(@deck_internal1.title)
    expect(page).to have_content('my rich text 2 user1')
    expect(page).to have_link('Edit deck')
    expect(page).to have_content(@deck_internal1.last_activity_at.strftime('%b %d, %Y %I:%M%p %Z'))
    expect(html).to_not include('<i class="bi bi-book"></i>Study')
    expect(page).to have_link('Add card')
    expect(page).to_not have_link('List cards')
  end

  scenario "on show page as admin for user's public deck" do
    sign_in @admin
    visit deck_path(@deck_public1.id)
    expect(page).to_not have_link('Show deck')
    expect(page).to have_content(@user1.handle)
    expect(page).to have_content('Public')
    expect(page).to have_content(@deck_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to have_link('Edit deck')
    expect(page).to have_content(@deck_public1.last_activity_at.strftime('%b %d, %Y %I:%M%p %Z'))
    expect(html).to_not include('<i class="bi bi-book"></i>Study')
    expect(page).to have_link('Add card')
    expect(page).to_not have_link('List cards')
  end

  scenario 'on show page as user for own private deck' do
    sign_in @user1
    visit deck_path(@deck_private1.id)
    expect(page).to_not have_link('Show deck')
    expect(page).to have_content(@user1.handle)
    expect(page).to have_content('Private')
    expect(page).to have_content(@deck_private1.title)
    expect(page).to have_content('my rich text 3')
    expect(page).to have_link('Edit deck')
    expect(page).to have_content(@deck_private1.last_activity_at.strftime('%b %d, %Y %I:%M%p %Z'))
    expect(html).to_not include('<i class="bi bi-book"></i>Study')
    expect(page).to have_link('Add card')
    expect(page).to_not have_link('List cards')
  end

  scenario 'on show page as collaborator for deck' do
    sign_in @collaborator
    visit deck_path(@deck_private1.id)
    expect(page).to_not have_link('Show deck')
    expect(page).to have_content(@user1.handle)
    expect(page).to have_content('Private')
    expect(page).to have_content(@deck_private1.title)
    expect(page).to have_content('my rich text 3')
    expect(page).to have_link('Edit deck')
    expect(page).to have_content(@deck_private1.last_activity_at.strftime('%b %d, %Y %I:%M%p %Z'))
    expect(html).to_not include('<i class="bi bi-book"></i>Study')
    expect(page).to have_link('Add card')
    expect(page).to_not have_link('List cards')
  end

  scenario 'on show page as group member for deck' do
    sign_in @group_member
    visit deck_path(@deck_private1.id)
    expect(page).to_not have_link('Show deck')
    expect(page).to have_content(@user1.handle)
    expect(page).to have_content('Private')
    expect(page).to have_content(@deck_private1.title)
    expect(page).to have_content('my rich text 3')
    expect(page).to have_link('Edit deck')
    expect(page).to have_content(@deck_private1.last_activity_at.strftime('%b %d, %Y %I:%M%p %Z'))
    expect(html).to_not include('<i class="bi bi-book"></i>Study')
    expect(page).to have_link('Add card')
    expect(page).to_not have_link('List cards')
  end

  scenario 'on show page as user for own internal deck' do
    sign_in @user1
    visit deck_path(@deck_internal1.id)
    expect(page).to_not have_link('Show deck')
    expect(page).to have_content(@user1.handle)
    expect(page).to have_content('Internal')
    expect(page).to have_content(@deck_internal1.title)
    expect(page).to have_content('my rich text 2 user1')
    expect(page).to have_link('Edit deck')
    expect(html).to_not include('<i class="bi bi-book"></i>Study')
    expect(page).to have_content(@deck_internal1.last_activity_at.strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_link('Add card')
    expect(page).to_not have_link('List cards')
  end

  scenario 'on show page as user for own public deck' do
    sign_in @user1
    visit deck_path(@deck_public1.id)
    expect(page).to_not have_link('Show deck')
    expect(page).to have_content(@user1.handle)
    expect(page).to have_content('Public')
    expect(page).to have_content(@deck_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to have_link('Edit deck')
    expect(page).to have_content(@deck_public1.last_activity_at.strftime('%b %d, %Y %I:%M%p %Z'))
    expect(html).to_not include('<i class="bi bi-book"></i>Study')
    expect(page).to have_link('Add card')
    expect(page).to_not have_link('List cards')
  end

  scenario "on show page as user for another user's internal deck" do
    sign_in @user2
    visit deck_path(@deck_internal1.id)
    expect(page).to_not have_link('Show deck')
    expect(page).to have_content(@user1.handle)
    expect(page).to_not have_content('Internal')
    expect(page).to have_content(@deck_internal1.title)
    expect(page).to have_content('my rich text 2 user1')
    expect(page).to_not have_link('Edit deck')
    expect(page).to have_content(@deck_internal1.last_activity_at.strftime('%b %d, %Y %I:%M%p %Z'))
    expect(html).to_not include('<i class="bi bi-book"></i>Study')
    expect(page).to_not have_link('Add card')
    expect(page).to_not have_link('List cards')
  end

  scenario "on show page as user for another user's public deck" do
    sign_in @user2
    visit deck_path(@deck_public1.id)
    expect(page).to_not have_link('Show deck')
    expect(page).to have_content(@user1.handle)
    expect(page).to_not have_content('Internal')
    expect(page).to have_content(@deck_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to_not have_link('Edit deck')
    expect(page).to have_content(@deck_internal1.last_activity_at.strftime('%b %d, %Y %I:%M%p %Z'))
    expect(html).to_not include('<i class="bi bi-book"></i>Study')
    expect(page).to_not have_link('Add card')
    expect(page).to_not have_link('List cards')
  end

  scenario 'on show page as for public deck not logged in' do
    visit deck_path(@deck_public1.id)
    expect(page).to_not have_link('Show deck')
    expect(page).to have_content(@user1.handle)
    expect(page).to_not have_content('Internal')
    expect(page).to have_content(@deck_public1.title)
    expect(page).to have_content('my rich text 1 user1')
    expect(page).to_not have_link('Edit deck')
    expect(page).to have_content(@deck_public1.last_activity_at.strftime('%b %d, %Y %I:%M%p %Z'))
    expect(html).to_not include('<i class="bi bi-book"></i>Study')
    expect(page).to_not have_link('Add card')
    expect(page).to_not have_link('List cards')
  end
end
