require 'rails_helper'

describe 'Deletes deck', type: :system do
  before(:each) do
    @user1 = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @deck_private1 = FactoryBot.create(:deck, :private, user_id: @user1.id)
  end

  scenario 'as user deleting own deck', js: true do
    sign_in @user1
    visit deck_path(@deck_private1.id)
    click_link 'Edit deck'
    click_button 'Delete Deck'
    page.accept_alert
    expect(current_path).to include('/decks')
    expect(page).to have_content('Deck was successfully deleted.')
    visit decks_path
    expect(page).to_not have_content(@deck_private1.title)
  end

  scenario "as admin deleting user's deck", js: true do
    sign_in @admin
    visit deck_path(@deck_private1.id)
    click_link 'Edit deck'
    click_button 'Delete Deck'
    page.accept_alert
    expect(current_path).to include('/decks')
    expect(page).to have_content('Deck was successfully deleted.')
    visit decks_path
    expect(page).to_not have_content(@deck_private1.title)
  end
end
