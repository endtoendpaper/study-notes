require 'rails_helper'

describe 'User edits deck', type: :system do
  let(:title) { Faker::Lorem.sentence(word_count: 6) }
  let(:description) { Faker::Lorem.sentence(word_count: 25) }

  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @user1 = FactoryBot.create(:user, :confirmed)
    @deck_private1 = FactoryBot.create(:deck, :private, user_id: @user1.id)
    @group_member = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group)
    @deck_private1.collaborate(@collaborator, @deck_private1.user)
    @deck_private1.add_group(@group, @deck_private1.user)
    @group.add_member(@group_member, @group.user)
  end

  scenario 'as user with valid data' do
    sign_in @user1
    visit deck_path(@deck_private1.id)
    click_link 'Edit deck'
    expect(page).to have_button('Delete Deck')
    fill_in 'deck_title', with: title
    fill_in 'deck_description', with: description
    choose('deck_visibility_1')
    click_button 'Submit'
    expect(current_path).to include('/decks/' + @deck_private1.id.to_s)
    expect(page).to have_content('Deck was successfully updated.')
    expect(page).to_not have_content(@deck_private1.title)
    expect(page).to_not have_content(@deck_private1.description)
    expect(page).to_not have_content('Private')
    expect(page).to have_content(title)
    expect(page).to have_content(description)
    expect(page).to have_content('Internal')
  end

  scenario "as admin editing user's deck with valid data" do
    sign_in @admin
    visit deck_path(@deck_private1.id)
    click_link 'Edit deck'
    expect(page).to have_button('Delete Deck')
    fill_in 'deck_title', with: title
    fill_in 'deck_description', with: description
    choose('deck_visibility_1')
    click_button 'Submit'
    expect(current_path).to include('/decks/' + @deck_private1.id.to_s)
    expect(page).to have_content('Deck was successfully updated.')
    expect(page).to_not have_content(@deck_private1.title)
    expect(page).to_not have_content(@deck_private1.description)
    expect(page).to_not have_content('Private')
    expect(page).to have_content(title)
    expect(page).to have_content(description)
    expect(page).to have_content('Internal')
  end

  scenario 'as user with invalid data', js: true do
    sign_in @user1
    visit deck_path(@deck_private1.id)
    click_link 'Edit deck'
    fill_in 'deck_title', with: ''
    choose('deck_visibility_1')
    click_button 'Submit'
    expect(current_path).to include('/decks/' + @deck_private1.id.to_s + '/edit')
    expect(page).to have_content('1 error prohibited this deck from being saved:')
    expect(page).to have_content("Title can't be blank")
  end

  scenario "as admin editing user's deck with invalid data", js: true do
    sign_in @admin
    visit deck_path(@deck_private1.id)
    click_link 'Edit deck'
    fill_in 'deck_title', with: ''
    choose('deck_visibility_1')
    click_button 'Submit'
    expect(current_path).to include('/decks/' + @deck_private1.id.to_s + '/edit')
    expect(page).to have_content('1 error prohibited this deck from being saved:')
    expect(page).to have_content("Title can't be blank")
  end

  scenario 'as collaborator with valid data' do
    sign_in @collaborator
    visit deck_path(@deck_private1.id)
    click_link 'Edit deck'
    expect(page).to_not have_button('Delete Deck')
    fill_in 'deck_title', with: title
    fill_in 'deck_description', with: description
    choose('deck_visibility_1')
    click_button 'Submit'
    expect(current_path).to include('/decks/' + @deck_private1.id.to_s)
    expect(page).to have_content('Deck was successfully updated.')
    expect(page).to_not have_content(@deck_private1.title)
    expect(page).to_not have_content(@deck_private1.description)
    expect(page).to_not have_content('Private')
    expect(page).to have_content(title)
    expect(page).to have_content(description)
    expect(page).to have_content('Internal')
  end

  scenario 'as group member with valid data' do
    sign_in @group_member
    visit deck_path(@deck_private1.id)
    click_link 'Edit deck'
    expect(page).to_not have_button('Delete Deck')
    fill_in 'deck_title', with: title
    fill_in 'deck_description', with: description
    choose('deck_visibility_1')
    click_button 'Submit'
    expect(current_path).to include('/decks/' + @deck_private1.id.to_s)
    expect(page).to have_content('Deck was successfully updated.')
    expect(page).to_not have_content(@deck_private1.title)
    expect(page).to_not have_content(@deck_private1.description)
    expect(page).to_not have_content('Private')
    expect(page).to have_content(title)
    expect(page).to have_content(description)
    expect(page).to have_content('Internal')
  end
end
