require 'rails_helper'

describe 'User creates a deck', type: :system do
  let(:title) { Faker::Lorem.sentence(word_count: 6) }
  let(:description) { Faker::Lorem.sentence(word_count: 25) }

  before(:each) do
    @user = FactoryBot.create(:user, :confirmed)
  end

  scenario 'with valid data' do
    sign_in @user
    visit new_deck_path
    fill_in 'deck_title', with: title
    fill_in 'deck_description', with: description
    choose('deck_visibility_1')
    click_button 'Submit'
    expect(page).to have_content('Deck was successfully created.')
    expect(current_path).to include('/decks/')
    expect(page).to have_content(title)
    expect(page).to have_content(description)
    expect(page).to have_content('Internal')
  end

  scenario 'with invalid data', js: true do
    sign_in @user
    visit new_deck_path
    fill_in 'deck_description', with: description
    choose('deck_visibility_2')
    click_button 'Submit'
    expect(page).to have_content('1 error prohibited this deck from being saved:')
    expect(page).to have_content("Title can't be blank")
    expect(current_path).to eq('/decks/new')
  end
end
