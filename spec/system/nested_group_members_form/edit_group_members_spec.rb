require 'rails_helper'

describe 'User edits group members in nested form', type: :system do
  let(:name) { Faker::Lorem.sentence(word_count: 6) }
  let(:description) { Faker::Lorem.sentence(word_count: 5) }

  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @owner = FactoryBot.create(:user, :confirmed)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group, user_id: @owner.id)
    @old_group_member = FactoryBot.create(:user, :confirmed)
    @new_group_member1 = FactoryBot.create(:user, :confirmed)
    @new_group_member2 = FactoryBot.create(:user, :confirmed)
    @group.add_member(@group_member, @group.user)
    @group.add_member(@old_group_member, @group.user)
  end

  scenario 'add members, remove members as admin', js: true do
    sign_in @admin
    visit edit_group_path(@group)
    expect(page).to have_button(@old_group_member.handle)
    fill_in 'member_username', with: @new_group_member1.username
    click_button 'Add Member'
    expect(page).to have_button(@old_group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    fill_in 'member_username', with: @new_group_member2.handle
    click_button 'Add Member'
    expect(page).to have_button(@old_group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    expect(page).to have_button(@new_group_member2.handle)
    click_button @old_group_member.handle
    expect(page).to_not have_button(@old_group_member.handle)
    click_button 'Submit'
    expect(page).to have_content("Group was successfully updated.")
    expect(@group.reload.group_members.count).to eq(3)
  end

  scenario 'add member that does not exist as admin', js: true do
    sign_in @admin
    visit edit_group_path(@group)
    fill_in 'member_username', with: '@notauser'
    click_button 'Add Member'
    expect(page).to_not have_button('@notauser')
    expect(page).to have_content('Username does not exist.')
  end

  scenario 'add member already associated as admin', js: true do
    sign_in @admin
    visit edit_group_path(@group)
    fill_in 'member_username', with: @old_group_member.handle
    click_button 'Add Member'
    expect(page).to have_content('User is already a member.')
  end

  scenario 'add members, remove members as owner', js: true do
    sign_in @owner
    visit edit_group_path(@group)
    expect(page).to have_button(@old_group_member.handle)
    fill_in 'member_username', with: @new_group_member1.username
    click_button 'Add Member'
    expect(page).to have_button(@old_group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    fill_in 'member_username', with: @new_group_member2.handle
    click_button 'Add Member'
    expect(page).to have_button(@old_group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    expect(page).to have_button(@new_group_member2.handle)
    click_button @old_group_member.handle
    expect(page).to_not have_button(@old_group_member.handle)
    click_button 'Submit'
    expect(page).to have_content("Group was successfully updated.")
    expect(@group.reload.group_members.count).to eq(3)
  end

  scenario 'add member that does not exist as owner', js: true do
    sign_in @owner
    visit edit_group_path(@group)
    fill_in 'member_username', with: '@notauser'
    click_button 'Add Member'
    expect(page).to_not have_button('@notauser')
    expect(page).to have_content('Username does not exist.')
  end

  scenario 'add member already associated as owner', js: true do
    sign_in @owner
    visit edit_group_path(@group)
    fill_in 'member_username', with: @old_group_member.handle
    click_button 'Add Member'
    expect(page).to have_content('User is already a member.')
  end

  scenario 'add members, remove members as group member', js: true do
    sign_in @group_member
    visit edit_group_path(@group)
    expect(page).to have_button(@old_group_member.handle)
    fill_in 'member_username', with: @new_group_member1.username
    click_button 'Add Member'
    expect(page).to have_button(@old_group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    fill_in 'member_username', with: @new_group_member2.handle
    click_button 'Add Member'
    expect(page).to have_button(@old_group_member.handle)
    expect(page).to have_button(@new_group_member1.handle)
    expect(page).to have_button(@new_group_member2.handle)
    click_button @old_group_member.handle
    expect(page).to_not have_button(@old_group_member.handle)
    click_button 'Submit'
    expect(page).to have_content("Group was successfully updated.")
    expect(@group.reload.group_members.count).to eq(3)
  end

  scenario 'add member that does not exist as group member', js: true do
    sign_in @group_member
    visit edit_group_path(@group)
    fill_in 'member_username', with: '@notauser'
    click_button 'Add Member'
    expect(page).to_not have_button('@notauser')
    expect(page).to have_content('Username does not exist.')
  end

  scenario 'add member already associated as group member', js: true do
    sign_in @group_member
    visit edit_group_path(@group)
    fill_in 'member_username', with: @old_group_member.handle
    click_button 'Add Member'
    expect(page).to have_content('User is already a member.')
  end
end
