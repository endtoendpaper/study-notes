require 'rails_helper'

describe 'Insert emoji into note', type: :system do
  before(:each) do
    @user = FactoryBot.create(:user, :confirmed)
    @note = FactoryBot.create(:note, :public, user: @user)
  end

  scenario 'using emoji picker', js: true do
    sign_in @user
    visit note_path(@note)
    click_link 'Edit note'
    expect(page).to have_css('emoji-picker', visible: false)
    find(:css, 'button.trix-button--icon-smiley').click
    expect(page).to have_css('emoji-picker', visible: true)
    find(:css, 'button.trix-button--icon-smiley').click
    expect(page).to have_css('emoji-picker', visible: false)
  end
end
