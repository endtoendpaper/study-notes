require 'rails_helper'

describe 'Add and remove tables', type: :system do
  before(:each) do
    @user = FactoryBot.create(:user, :confirmed)
    @note = FactoryBot.create(:note, :public, user: @user)
  end

  scenario 'to note', js: true do
    sign_in @user
    visit note_path(@note)
    click_link 'Edit note'
    find(:css, 'button.trix-button--icon-table').click
    expect(page).to have_button('Add Row')
    expect(page).to have_button('Add Column')
    expect(page).to have_button('Bold First Row')
    expect(page).to_not have_button('Remove Row')
    expect(page).to_not have_button('Remove Column')
    expect(page).to_not have_button('Unbold First Row')
    page.find_button('Add Row', visible: :all).click
    page.find_button('Add Row', visible: :all).click
    expect(page).to have_button('Remove Row')
    page.find_button('Add Column', visible: :all).click
    page.find_button('Add Column', visible: :all).click
    expect(page).to have_button('Remove Column')
    page.find_button('Bold First Row', visible: :all).click
    page.find_button('Unbold First Row', visible: :all).click
    click_button 'Submit'
    expect(page).to have_css('table')
  end
end
