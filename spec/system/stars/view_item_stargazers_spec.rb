require 'rails_helper'

describe 'Views item stargazers', type: :system do
  before(:each) do
    reset_elasticsearch_indices

    @user1 = FactoryBot.create(:user, :confirmed)
    @user2 = FactoryBot.create(:user, :confirmed, hide_stars: 'true')
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    UserItems.item_list.each do |item|
      instance_variable_set("@#{item.singularize}", FactoryBot.create(item.singularize, :public))
      instance_variable_get("@#{item.singularize}").add_star(@user1)
      instance_variable_get("@#{item.singularize}").add_star(@user2)
    end

    reindex_elasticsearch_data
  end

  scenario 'as admin' do
    sign_in @admin
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      find(".stargazers", match: :first).click
      expect(page).to have_selector('h1', text: instance_variable_get("@#{item.singularize}").title + ': Stargazers')
      expect(page).to have_link(@user1.handle)
      expect(page).to have_link(@user2.handle)
    end
  end

  scenario 'as user' do
    sign_in @user1
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      find(".stargazers", match: :first).click
      expect(page).to have_selector('h1', text: instance_variable_get("@#{item.singularize}").title + ': Stargazers')
      expect(page).to have_link(@user1.handle)
      expect(page).to_not have_link(@user2.handle)
    end
  end

  scenario 'not logged in' do
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      expect(page).to_not have_button('Star')
      find(".stargazers", match: :first).click
      expect(page).to have_selector('h1', text: instance_variable_get("@#{item.singularize}").title + ': Stargazers')
      expect(page).to have_link(@user1.handle)
      expect(page).to_not have_link(@user2.handle)
    end
  end

  scenario 'not logged in from home' do
    visit root_path
    expect(page).to_not have_css('.bi-star')
    expect(page).to_not have_css('.bi-star-fill')
  end

  scenario 'logged in from home' do
    sign_in @user1
    visit root_path
    expect(page).to have_css('.bi-star-fill')
  end

  scenario 'see pagination when 26 stargazers' do
    26.times do
      user = FactoryBot.create(:user, :confirmed)
      @deck.add_star(user)
    end

    reindex_elasticsearch_data

    visit stargazers_path(@deck.route_type, @deck.id)
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end
end

