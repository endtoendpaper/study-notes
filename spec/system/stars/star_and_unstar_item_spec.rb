require 'rails_helper'

describe 'Change stars', type: :system do
  before(:each) do
    @user = FactoryBot.create(:user, :confirmed)
    UserItems.item_list.each do |item|
      instance_variable_set("@#{item.singularize}", FactoryBot.create(item.singularize, :public))
      26.times do
        user = FactoryBot.create(:user, :confirmed)
        instance_variable_get("@#{item.singularize}").add_star(user)
      end
    end
  end

  scenario 'starring and unstarring item on show page', js: true do
    sign_in @user
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      expect(page).to have_button('Star')
      expect(page).to have_content('26')
      find(:css, 'i.bi-star').click
      expect(page).to have_content('27')
      find(:css, 'i.bi-star-fill').click
      expect(page).to have_content('26')
    end
  end

  scenario 'starring and unstarring item on index page', js: true do
    sign_in @user
    UserItems.item_list.each do |item|
      visit send("#{item}_path")
      expect(page).to_not have_button('Star')
      expect(page).to_not have_button('26')
      find(:css, 'i.bi-star').click
      expect(page).to_not have_button('Star')
      expect(page).to_not have_button('27')
      find(:css, 'i.bi-star-fill').click
      expect(page).to_not have_button('Star')
      expect(page).to_not have_button('26')
    end
  end
end
