require 'rails_helper'

describe 'Views user stars', type: :system do
  before(:each) do
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group_owner = FactoryBot.create(:user, :confirmed)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group, user_id: @group_owner.id)
    @group.add_member(@group_member, @group.user)
  end

  scenario 'as admin', js: true do
    sign_in @admin

    UserItems.item_list.each do |item|
      reset_elasticsearch_indices([item])

      public1 = FactoryBot.create(item.singularize, :public, user_id: @owner.id)
      internal1 = FactoryBot.create(item.singularize, :internal, user_id: @owner.id)
      private1 = FactoryBot.create(item.singularize, :private, user_id: @owner.id)

      public2 = FactoryBot.create(item.singularize, :public, user_id: @owner.id)
      internal2 = FactoryBot.create(item.singularize, :internal, user_id: @owner.id)
      private2 = FactoryBot.create(item.singularize, :private, user_id: @owner.id)

      public1.collaborate(@collaborator, public1.user)
      internal1.collaborate(@collaborator, internal1.user)
      private1.collaborate(@collaborator, private1.user)
      public1.add_group(@group, public1.user)
      internal1.add_group(@group, internal1.user)
      private1.add_group(@group, private1.user)
      public2.collaborate(@collaborator, public2.user)
      internal2.collaborate(@collaborator, internal2.user)
      private2.collaborate(@collaborator, private2.user)
      public2.add_group(@group, public2.user)
      internal2.add_group(@group, internal2.user)
      private2.add_group(@group, private2.user)

      public1.add_star(@owner)
      internal1.add_star(@owner)
      private1.add_star(@owner)
      public2.add_star(@owner)
      internal2.add_star(@owner)
      private2.add_star(@owner)

      reindex_elasticsearch_data([item])

      visit show_stars_user_path(@owner)

      expect(page).to have_link(public1.title)
      expect(page).to have_link(internal1.title)
      expect(page).to have_link(private1.title)
      expect(page).to have_link(public2.title)
      expect(page).to have_link(internal2.title)
      expect(page).to have_link(private2.title)

      item.capitalize.singularize.constantize.destroy_all
    end
  end

  scenario 'as owner', js: true do
    sign_in @owner

    UserItems.item_list.each do |item|
      reset_elasticsearch_indices([item])

      public1 = FactoryBot.create(item.singularize, :public, user_id: @owner.id)
      internal1 = FactoryBot.create(item.singularize, :internal, user_id: @owner.id)
      private1 = FactoryBot.create(item.singularize, :private, user_id: @owner.id)

      public2 = FactoryBot.create(item.singularize, :public, user_id: @owner.id)
      internal2 = FactoryBot.create(item.singularize, :internal, user_id: @owner.id)
      private2 = FactoryBot.create(item.singularize, :private, user_id: @owner.id)

      public1.collaborate(@collaborator, public1.user)
      internal1.collaborate(@collaborator, internal1.user)
      private1.collaborate(@collaborator, private1.user)
      public1.add_group(@group, public1.user)
      internal1.add_group(@group, internal1.user)
      private1.add_group(@group, private1.user)
      public2.collaborate(@collaborator, public2.user)
      internal2.collaborate(@collaborator, internal2.user)
      private2.collaborate(@collaborator, private2.user)
      public2.add_group(@group, public2.user)
      internal2.add_group(@group, internal2.user)
      private2.add_group(@group, private2.user)

      public1.add_star(@owner)
      internal1.add_star(@owner)
      private1.add_star(@owner)
      public2.add_star(@owner)
      internal2.add_star(@owner)
      private2.add_star(@owner)

      reindex_elasticsearch_data([item])

      visit show_stars_user_path(@owner)

      expect(page).to have_link(public1.title)
      expect(page).to have_link(internal1.title)
      expect(page).to have_link(private1.title)
      expect(page).to have_link(public2.title)
      expect(page).to have_link(internal2.title)
      expect(page).to have_link(private2.title)

      item.capitalize.singularize.constantize.destroy_all
    end
  end

  scenario 'as collaborator', js: true do
    sign_in @collaborator

    UserItems.item_list.each do |item|
      reset_elasticsearch_indices([item])

      public1 = FactoryBot.create(item.singularize, :public, user_id: @owner.id)
      internal1 = FactoryBot.create(item.singularize, :internal, user_id: @owner.id)
      private1 = FactoryBot.create(item.singularize, :private, user_id: @owner.id)

      public2 = FactoryBot.create(item.singularize, :public, user_id: @owner.id)
      internal2 = FactoryBot.create(item.singularize, :internal, user_id: @owner.id)
      private2 = FactoryBot.create(item.singularize, :private, user_id: @owner.id)

      public1.collaborate(@collaborator, public1.user)
      internal1.collaborate(@collaborator, internal1.user)
      private1.collaborate(@collaborator, private1.user)
      public1.add_group(@group, public1.user)
      internal1.add_group(@group, internal1.user)
      private1.add_group(@group, private1.user)
      public2.collaborate(@collaborator, public2.user)
      internal2.collaborate(@collaborator, internal2.user)
      private2.collaborate(@collaborator, private2.user)
      public2.add_group(@group, public2.user)
      internal2.add_group(@group, internal2.user)
      private2.add_group(@group, private2.user)

      public1.add_star(@owner)
      internal1.add_star(@owner)
      private1.add_star(@owner)
      public2.add_star(@owner)
      internal2.add_star(@owner)
      private2.add_star(@owner)

      reindex_elasticsearch_data([item])

      visit show_stars_user_path(@owner)

      expect(page).to have_link(public1.title)
      expect(page).to have_link(internal1.title)
      expect(page).to have_link(private1.title)
      expect(page).to have_link(public2.title)
      expect(page).to have_link(internal2.title)
      expect(page).to have_link(private2.title)

      item.capitalize.singularize.constantize.destroy_all
    end
  end

  scenario 'as group member', js: true do
    sign_in @group_member

    UserItems.item_list.each do |item|
      reset_elasticsearch_indices([item])

      public1 = FactoryBot.create(item.singularize, :public, user_id: @owner.id)
      internal1 = FactoryBot.create(item.singularize, :internal, user_id: @owner.id)
      private1 = FactoryBot.create(item.singularize, :private, user_id: @owner.id)

      public2 = FactoryBot.create(item.singularize, :public, user_id: @owner.id)
      internal2 = FactoryBot.create(item.singularize, :internal, user_id: @owner.id)
      private2 = FactoryBot.create(item.singularize, :private, user_id: @owner.id)

      public1.collaborate(@collaborator, public1.user)
      internal1.collaborate(@collaborator, internal1.user)
      private1.collaborate(@collaborator, private1.user)
      public1.add_group(@group, public1.user)
      internal1.add_group(@group, internal1.user)
      private1.add_group(@group, private1.user)
      public2.collaborate(@collaborator, public2.user)
      internal2.collaborate(@collaborator, internal2.user)
      private2.collaborate(@collaborator, private2.user)
      public2.add_group(@group, public2.user)
      internal2.add_group(@group, internal2.user)
      private2.add_group(@group, private2.user)

      public1.add_star(@owner)
      internal1.add_star(@owner)
      private1.add_star(@owner)
      public2.add_star(@owner)
      internal2.add_star(@owner)
      private2.add_star(@owner)

      reindex_elasticsearch_data([item])

      visit show_stars_user_path(@owner)

      expect(page).to have_link(public1.title)
      expect(page).to have_link(internal1.title)
      expect(page).to have_link(private1.title)
      expect(page).to have_link(public2.title)
      expect(page).to have_link(internal2.title)
      expect(page).to have_link(private2.title)

      item.capitalize.singularize.constantize.destroy_all
    end
  end

  scenario 'as group owner', js: true do
    sign_in @group_owner
    UserItems.item_list.each do |item|
      reset_elasticsearch_indices([item])

      public1 = FactoryBot.create(item.singularize, :public, user_id: @owner.id)
      internal1 = FactoryBot.create(item.singularize, :internal, user_id: @owner.id)
      private1 = FactoryBot.create(item.singularize, :private, user_id: @owner.id)

      public2 = FactoryBot.create(item.singularize, :public, user_id: @owner.id)
      internal2 = FactoryBot.create(item.singularize, :internal, user_id: @owner.id)
      private2 = FactoryBot.create(item.singularize, :private, user_id: @owner.id)

      public1.collaborate(@collaborator, public1.user)
      internal1.collaborate(@collaborator, internal1.user)
      private1.collaborate(@collaborator, private1.user)
      public1.add_group(@group, public1.user)
      internal1.add_group(@group, internal1.user)
      private1.add_group(@group, private1.user)
      public2.collaborate(@collaborator, public2.user)
      internal2.collaborate(@collaborator, internal2.user)
      private2.collaborate(@collaborator, private2.user)
      public2.add_group(@group, public2.user)
      internal2.add_group(@group, internal2.user)
      private2.add_group(@group, private2.user)

      public1.add_star(@owner)
      internal1.add_star(@owner)
      private1.add_star(@owner)
      public2.add_star(@owner)
      internal2.add_star(@owner)
      private2.add_star(@owner)

      reindex_elasticsearch_data([item])

      visit show_stars_user_path(@owner)

      expect(page).to have_link(public1.title)
      expect(page).to have_link(internal1.title)
      expect(page).to have_link(private1.title)
      expect(page).to have_link(public2.title)
      expect(page).to have_link(internal2.title)
      expect(page).to have_link(private2.title)

      item.capitalize.singularize.constantize.destroy_all
    end
  end

  scenario 'as another user', js: true do
    sign_in @user
    UserItems.item_list.each do |item|
      reset_elasticsearch_indices([item])

      public1 = FactoryBot.create(item.singularize, :public, user_id: @owner.id)
      internal1 = FactoryBot.create(item.singularize, :internal, user_id: @owner.id)
      private1 = FactoryBot.create(item.singularize, :private, user_id: @owner.id)

      public2 = FactoryBot.create(item.singularize, :public, user_id: @owner.id)
      internal2 = FactoryBot.create(item.singularize, :internal, user_id: @owner.id)
      private2 = FactoryBot.create(item.singularize, :private, user_id: @owner.id)

      public1.collaborate(@collaborator, public1.user)
      internal1.collaborate(@collaborator, internal1.user)
      private1.collaborate(@collaborator, private1.user)
      public1.add_group(@group, public1.user)
      internal1.add_group(@group, internal1.user)
      private1.add_group(@group, private1.user)
      public2.collaborate(@collaborator, public2.user)
      internal2.collaborate(@collaborator, internal2.user)
      private2.collaborate(@collaborator, private2.user)
      public2.add_group(@group, public2.user)
      internal2.add_group(@group, internal2.user)
      private2.add_group(@group, private2.user)

      public1.add_star(@owner)
      internal1.add_star(@owner)
      private1.add_star(@owner)
      public2.add_star(@owner)
      internal2.add_star(@owner)
      private2.add_star(@owner)

      reindex_elasticsearch_data([item])

      visit show_stars_user_path(@owner)

      expect(page).to have_link(public1.title)
      expect(page).to have_link(internal1.title)
      expect(page).to_not have_link(private1.title)
      expect(page).to have_link(public2.title)
      expect(page).to have_link(internal2.title)
      expect(page).to_not have_link(private2.title)

      item.capitalize.singularize.constantize.destroy_all
    end
  end

  scenario 'not logged in' do
    UserItems.item_list.each do |item|
      reset_elasticsearch_indices([item])

      public1 = FactoryBot.create(item.singularize, :public, user_id: @owner.id)
      internal1 = FactoryBot.create(item.singularize, :internal, user_id: @owner.id)
      private1 = FactoryBot.create(item.singularize, :private, user_id: @owner.id)

      public2 = FactoryBot.create(item.singularize, :public, user_id: @owner.id)
      internal2 = FactoryBot.create(item.singularize, :internal, user_id: @owner.id)
      private2 = FactoryBot.create(item.singularize, :private, user_id: @owner.id)

      public1.collaborate(@collaborator, public1.user)
      internal1.collaborate(@collaborator, internal1.user)
      private1.collaborate(@collaborator, private1.user)
      public1.add_group(@group, public1.user)
      internal1.add_group(@group, internal1.user)
      private1.add_group(@group, private1.user)
      public2.collaborate(@collaborator, public2.user)
      internal2.collaborate(@collaborator, internal2.user)
      private2.collaborate(@collaborator, private2.user)
      public2.add_group(@group, public2.user)
      internal2.add_group(@group, internal2.user)
      private2.add_group(@group, private2.user)

      public1.add_star(@owner)
      internal1.add_star(@owner)
      private1.add_star(@owner)
      public2.add_star(@owner)
      internal2.add_star(@owner)
      private2.add_star(@owner)

      reindex_elasticsearch_data([item])

      visit show_stars_user_path(@owner)

      expect(page).to have_link(public1.title)
      expect(page).to_not have_link(internal1.title)
      expect(page).to_not have_link(private1.title)
      expect(page).to have_link(public2.title)
      expect(page).to_not have_link(internal2.title)
      expect(page).to_not have_link(private2.title)

      item.capitalize.singularize.constantize.destroy_all
    end
  end
end

