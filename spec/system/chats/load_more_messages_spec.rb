require 'rails_helper'

describe 'Load more messages', type: :system do
  before(:each) do
    @user = FactoryBot.create(:user, :confirmed)
    FactoryBot.create(:chat, :dm_with_first_user, :two_hundred_msgs)
  end

  scenario 'as user', js: true do
    sign_in @user
    visit users_path
    click_link("Join Chat")
    expect(page).to have_css('.message', minimum: 50)
    page.execute_script "document.getElementById('messages').scrollTo(0,0)"
    expect(page).to have_css('.message', minimum: 100)
  end
end

