require 'rails_helper'

describe 'Join group chat', type: :system do
  before(:each) do
    @user = FactoryBot.create(:user, :confirmed)
  end

  scenario 'when chat is already created' do
    chat = FactoryBot.create(:chat, :group_with_first_user)
    group = Group.first
    group.add_member(@user, group.user)
    sign_in @user
    visit group_path(group)
    click_link("Join Chat")
    expect(page).to have_selector('h5', text: group.name)
  end

  scenario 'when chat is not already created' do
    group = FactoryBot.create(:group)
    group.add_member(@user, group.user)
    sign_in @user
    visit group_path(group)
    click_link("Join Chat")
    expect(page).to have_selector('h5', text: group.name)
  end
end

