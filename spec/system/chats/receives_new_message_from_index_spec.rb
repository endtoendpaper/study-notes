require 'rails_helper'
include ActiveJob::TestHelper

describe 'Recieves new message from index', type: :system do
  before(:each) do
    @chat = FactoryBot.create(:chat)
    FactoryBot.create(:user, :confirmed)
    @user1 = User.ordered_by_id.first
    @user2 = FactoryBot.create(:user, :confirmed)
    @chat.collaborate(@user1)
    @chat.collaborate(@user2)
    @cp1 = ChatPreference.create(user: @user1, chat: @chat)
    @cp2 = ChatPreference.create(user: @user2, chat: @chat)
  end

  scenario 'as a user', js: true, flaky: true do
    sign_in @user1
    visit root_path
    click_link("Chat")
    expect(page).to have_css('.chat-link')
    expect(page).to_not have_css('.badge')
    message1 = nil
    message2 = nil
    using_wait_time 5 do
      perform_enqueued_jobs do
        message1 = FactoryBot.create(:message, user_id: @user2.id, user_username: @user2.username, chat_id: @chat.id)
        message2 = FactoryBot.create(:message, user_id: @user2.id, user_username: @user2.username, chat_id: @chat.id)
      end
    end
    expect(page).to have_css('.message-content')
    expect(page).to have_css('.chat-link')
    expect(page).to have_css('.badge', visible: :all, wait: 5)
    expect(page).to have_content('2')
    page.find("#chat_index_item_#{@chat.id}", visible: :all).click
    expect(page).to_not have_css('.badge')
    expect(page).to have_content(ActionController::Base.helpers.strip_tags(Kramdown::Document.new(message1.reload.content).to_html))
    expect(page).to have_content(ActionController::Base.helpers.strip_tags(Kramdown::Document.new(message2.reload.content).to_html))
    expect(page).to have_content('UNREAD MESSAGES', wait: 10)
  end
end
