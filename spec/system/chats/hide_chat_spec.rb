require 'rails_helper'

describe 'Hides a chat', type: :system do
  before(:each) do
    @user = FactoryBot.create(:user, :confirmed)
    @chat = FactoryBot.create(:chat, :dm_with_first_user)
  end

  scenario 'as a user', js: true do
    sign_in @user
    visit root_path
    click_link("Chat")
    expect(page).to have_css('.chat-link')
    page.find("#chat_index_item_#{@chat.id}", visible: :all).click
    click_link("Hide")
    expect(page).to_not have_link('Back')
    expect(page).to_not have_css('.chat-link')
  end
end
