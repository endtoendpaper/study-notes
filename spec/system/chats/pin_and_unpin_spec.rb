require 'rails_helper'

describe 'Pin and unpin chat', type: :system do
  before(:each) do
    @user = FactoryBot.create(:user, :confirmed)
    @chat = FactoryBot.create(:chat, :dm_with_first_user)
  end

  scenario 'as a user', js: true do
    sign_in @user
    visit root_path
    click_link("Chat")
    expect(page).to have_css('.chat-link')
    expect(page).to_not have_css('.pinned')
    page.find("#chat_index_item_#{@chat.id}", visible: :all).click
    click_link("Pin")
    expect(page).to have_link('Unpin')
    click_link("Back")
    expect(page).to have_css('.pinned')
    expect(page).to have_css('.chat-link')
    page.find("#chat_index_item_#{@chat.id}", visible: :all).click
    click_link("Unpin")
    expect(page).to have_link('Pin')
    click_link("Back")
    expect(page).to_not have_css('.pinned')
  end
end
