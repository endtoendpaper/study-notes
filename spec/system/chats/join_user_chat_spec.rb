# when chat already created
# when chat not created yet

require 'rails_helper'

describe 'Join user chat', type: :system do
  before(:each) do
    @user = FactoryBot.create(:user, :confirmed)
  end

  scenario 'when chat is already created', js: true do
    chat = FactoryBot.create(:chat, :dm_with_first_user)
    sign_in @user
    visit users_path
    click_link("Join Chat")
    expect(page).to have_selector('h5', text: User.first.username)
  end

  scenario 'when chat is not already created', js: true do
    user = FactoryBot.create(:user, :confirmed)
    sign_in @user
    visit users_path
    click_link("Join Chat")
    expect(page).to have_selector('h5', text: user.username)
  end
end

