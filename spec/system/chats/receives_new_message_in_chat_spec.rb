require 'rails_helper'
include ActiveJob::TestHelper

describe 'Recieves new message in chat', type: :system do
  before(:each) do
    @chat = FactoryBot.create(:chat)
    FactoryBot.create(:user, :confirmed)
    @user1 = User.ordered_by_id.first
    @user2 = FactoryBot.create(:user, :confirmed)
    @chat.collaborate(@user1)
    @chat.collaborate(@user2)
    ChatPreference.create(user: @user1, chat: @chat)
    ChatPreference.create(user: @user2, chat: @chat)
  end

  scenario 'as a user', js: true do
    sign_in @user1
    visit root_path
    click_link("Chat")
    expect(page).to have_css('.chat-link')
    expect(page).to_not have_css('.badge')
    page.find("#chat_index_item_#{@chat.id}", visible: :all).click
    message0 = nil
    message1 = nil
    message2 = nil
    expect(page).to_not have_css('.message-content')
    perform_enqueued_jobs do
      message0 = FactoryBot.create(:message, user_id: @user2.id, user_username: @user2.username, chat_id: @chat.id)
      message1 = FactoryBot.create(:message, user_id: @user2.id, user_username: @user2.username, chat_id: @chat.id)
      message2 = FactoryBot.create(:message, user_id: @user2.id, user_username: @user2.username, chat_id: @chat.id)
    end
    expect(page).to have_link('Back')
    expect(page).to have_link('Hide')
    expect(page).to have_link('Pin')
    expect(page).to have_content(@user2.username, wait: 3)
    expect(page).to have_css('.message-content', count: 3)
    expect(page).to have_content(ActionController::Base.helpers.strip_tags(Kramdown::Document.new(message0.reload.content).to_html))
    expect(page).to have_content(ActionController::Base.helpers.strip_tags(Kramdown::Document.new(message1.reload.content).to_html))
    expect(page).to have_content(ActionController::Base.helpers.strip_tags(Kramdown::Document.new(message2.reload.content).to_html))
  end
end
