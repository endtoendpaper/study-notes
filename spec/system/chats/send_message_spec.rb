require 'rails_helper'

describe 'Send a message', type: :system do
  before(:each) do
    @dm = FactoryBot.create(:chat, :dm)
    @group_chat = FactoryBot.create(:chat, :group)
    @owner = FactoryBot.create(:user, :confirmed)
    @group_member1 = FactoryBot.create(:user, :confirmed)
    @group_member2 = FactoryBot.create(:user, :confirmed)
    @non_member = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group, user_id: @owner.id)
    @group.add_member(@group_member1, @group.user)
    @group.add_member(@group_member2, @group.user)
  end

  scenario 'to dm', js: true do
    sign_in @non_member
    visit root_path
    click_link("Chat")
    expect(page).to_not have_css('.chat-link') # doesn't see other chats not involved in
    fill_in 'participant', with: @group_member1.username
    click_button("User")
    expect(page).to have_selector('h5', text: @group_member1.handle)
    fill_in('message_content', with: "my new message!!!\n").send_keys(:return)
    expect(page).to have_css('.message-content')
    expect(page).to_not have_css('.updated_date_text_class')
    expect(page).to have_selector('p', text: "my new message!!!")
    click_link("Back")
    expect(page).to have_css('.chat-link')
  end

  scenario 'to group as group member', js: true do
    sign_in @group_member1
    visit root_path
    click_link("Chat")
    expect(page).to_not have_css('.chat-link') # doesn't see other chats not involved in
    fill_in 'participant', with: @group.name
    click_button("Group")
    expect(page).to have_selector('h5', text: @group.name)
    fill_in('message_content', with: "my new message!!!\n").send_keys(:return)
    expect(page).to have_css('.message-content')
    expect(page).to_not have_css('.updated_date_text_class')
    expect(page).to have_selector('p', text: "my new message!!!")
    click_link("Back")
    expect(page).to have_css('.chat-link')
  end
end
