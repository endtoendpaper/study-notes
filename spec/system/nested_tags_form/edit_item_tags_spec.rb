require 'rails_helper'

describe "Updates item's tags in nested form", type: :system do
  let(:name1) { Faker::Lorem.characters(number: 25) }
  let(:name2) { 'another-worD+++121' }
  let(:name3) { Faker::Lorem.characters(number: 45) }
  let(:name4) { '___--+++9999' }

  before(:each) do
    @owner = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @group_member = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group)
    @group.add_member(@group_member, @group.user)
    @old_tag = FactoryBot.create(:tag)
    @new_tag = FactoryBot.create(:tag)
    UserItems.item_list.each do |item|
      item = instance_variable_set("@#{item.singularize}", FactoryBot.create(item.singularize, :public, user_id: @owner.id))
      item.collaborate(@collaborator, item.user)
      item.add_group(@group, item.user)
      item.tags << @old_tag
    end
  end

  scenario 'add and remove tags as admin', js: true do
    sign_in @admin
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      expect(page).to have_button(@old_tag.name)
      fill_in 'tag_name', with: @new_tag.name
      click_button 'Add Tag'
      expect(page).to have_button(@old_tag.name)
      expect(page).to have_button(@new_tag.name)
      fill_in 'tag_name', with: "newtaghere"
      click_button 'Add Tag'
      expect(page).to have_button(@old_tag.name)
      expect(page).to have_button(@new_tag.name)
      expect(page).to have_button("newtaghere")
      click_button @old_tag.name
      expect(page).to_not have_button(@old_tag.name)
      find('body').send_keys(:escape)
      find('body').click
      page.execute_script("document.querySelector('input[type=submit]').click()")
      using_wait_time 3 do
        expect(page).to_not have_link(@old_tag.name)
        expect(page).to have_link(@new_tag.name)
        expect(page).to have_link("newtaghere")
      end
    end
  end

  scenario 'add invalid tag as admin', js: true do
    sign_in @admin
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      fill_in 'tag_name', with: 'spac es'
      click_button 'Add Tag'
      expect(page).to have_button('spac es')
      find('body').send_keys(:escape)
      find('body').click
      page.execute_script("document.querySelector('input[type=submit]').click()")
      using_wait_time 3 do
        expect(page).to have_content('1 error prohibited this')
        expect(page).to have_content('Taggings - spac es is invalid. Tags can only contain letters, numbers, dashes, underscores, or pluses and be less than 100 characters.')
      end
    end
  end

  scenario 'add tag already associated as admin', js: true do
    sign_in @admin
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      fill_in 'tag_name', with: @old_tag.name
      click_button 'Add Tag'
      expect(page).to have_content('Tag is already added.')
      instance_variable_get("@#{item.singularize}").tags << @new_tag
      fill_in 'tag_name', with: @new_tag.name
      click_button 'Add Tag'
      find('body').send_keys(:escape)
      find('body').click
      page.execute_script("document.querySelector('input[type=submit]').click()")
      using_wait_time 3 do
        expect(page).to have_content("Taggings - #{@new_tag.name} is already associated.")
      end
    end
  end

  scenario 'add and remove tags as item owner', js: true do
    sign_in @owner
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      expect(page).to have_button(@old_tag.name)
      fill_in 'tag_name', with: @new_tag.name
      click_button 'Add Tag'
      expect(page).to have_button(@old_tag.name)
      expect(page).to have_button(@new_tag.name)
      fill_in 'tag_name', with: "newtaghere"
      click_button 'Add Tag'
      expect(page).to have_button(@old_tag.name)
      expect(page).to have_button(@new_tag.name)
      expect(page).to have_button("newtaghere")
      click_button @old_tag.name
      expect(page).to_not have_button(@old_tag.name)
      find('body').send_keys(:escape)
      find('body').click
      page.execute_script("document.querySelector('input[type=submit]').click()")
      using_wait_time 3 do
        expect(page).to_not have_link(@old_tag.name)
        expect(page).to have_link(@new_tag.name)
        expect(page).to have_link("newtaghere")
      end
    end
  end

  scenario 'add invalid tag as owner', js: true do
    sign_in @owner
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      fill_in 'tag_name', with: 'spac es'
      click_button 'Add Tag'
      expect(page).to have_button('spac es')
      find('body').send_keys(:escape)
      find('body').click
      page.execute_script("document.querySelector('input[type=submit]').click()")
      using_wait_time 3 do
        expect(page).to have_content('1 error prohibited this')
        expect(page).to have_content('Taggings - spac es is invalid. Tags can only contain letters, numbers, dashes, underscores, or pluses and be less than 100 characters.')
      end
    end
  end

  scenario 'add tag already associated as admin', js: true do
    sign_in @owner
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      fill_in 'tag_name', with: @old_tag.name
      click_button 'Add Tag'
      expect(page).to have_button(@old_tag.name)
      expect(page).to have_content('Tag is already added.')
      instance_variable_get("@#{item.singularize}").tags << @new_tag
      fill_in 'tag_name', with: @new_tag.name
      click_button 'Add Tag'
      find('body').send_keys(:escape)
      find('body').click
      page.execute_script("document.querySelector('input[type=submit]').click()")
      using_wait_time 3 do
        expect(page).to have_content("Taggings - #{@new_tag.name} is already associated.")
      end
    end
  end

  scenario 'add and remove tags as collaborator', js: true do
    sign_in @collaborator
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      expect(page).to have_button(@old_tag.name)
      fill_in 'tag_name', with: @new_tag.name
      click_button 'Add Tag'
      expect(page).to have_button(@old_tag.name)
      expect(page).to have_button(@new_tag.name)
      fill_in 'tag_name', with: "newtaghere"
      click_button 'Add Tag'
      expect(page).to have_button(@old_tag.name)
      expect(page).to have_button(@new_tag.name)
      expect(page).to have_button("newtaghere")
      click_button @old_tag.name
      expect(page).to_not have_button(@old_tag.name)
      find('body').send_keys(:escape)
      find('body').click
      page.execute_script("document.querySelector('input[type=submit]').click()")
      using_wait_time 3 do
        expect(page).to_not have_link(@old_tag.name)
        expect(page).to have_link(@new_tag.name)
        expect(page).to have_link("newtaghere")
      end
    end
  end

  scenario 'add invalid tag as collaborator', js: true do
    sign_in @collaborator
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      fill_in 'tag_name', with: 'spac es'
      click_button 'Add Tag'
      expect(page).to have_button('spac es')
      find('body').send_keys(:escape)
      find('body').click
      page.execute_script("document.querySelector('input[type=submit]').click()")
      using_wait_time 3 do
        expect(page).to have_content('1 error prohibited this')
        expect(page).to have_content('Taggings - spac es is invalid. Tags can only contain letters, numbers, dashes, underscores, or pluses and be less than 100 characters.')
      end
    end
  end

  scenario 'add tag already associated as collaborator', js: true, flakey: true do
    sign_in @collaborator
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      fill_in 'tag_name', with: @old_tag.name
      click_button 'Add Tag'
      expect(page).to have_button(@old_tag.name)
      expect(page).to have_content('Tag is already added.')
      instance_variable_get("@#{item.singularize}").tags << @new_tag
      fill_in 'tag_name', with: @new_tag.name
      click_button 'Add Tag'
      find('body').send_keys(:escape)
      find('body').click
      page.execute_script("document.querySelector('input[type=submit]').click()")
      using_wait_time 3 do
        expect(page).to have_content("Taggings - #{@new_tag.name} is already associated.")
      end
    end
  end

  scenario 'add and remove tags as group member', js: true do
    sign_in @group_member
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      expect(page).to have_button(@old_tag.name)
      fill_in 'tag_name', with: @new_tag.name
      click_button 'Add Tag'
      expect(page).to have_button(@old_tag.name)
      expect(page).to have_button(@new_tag.name)
      fill_in 'tag_name', with: "newtaghere"
      click_button 'Add Tag'
      expect(page).to have_button(@old_tag.name)
      expect(page).to have_button(@new_tag.name)
      expect(page).to have_button("newtaghere")
      click_button @old_tag.name
      expect(page).to_not have_button(@old_tag.name)
      find('body').send_keys(:escape)
      find('body').click
      page.execute_script("document.querySelector('input[type=submit]').click()")
      using_wait_time 3 do
        expect(page).to_not have_link(@old_tag.name)
        expect(page).to have_link(@new_tag.name)
        expect(page).to have_link("newtaghere")
      end
    end
  end

  scenario 'add invalid tag as group member', js: true do
    sign_in @group_member
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      fill_in 'tag_name', with: 'spac es'
      click_button 'Add Tag'
      expect(page).to have_button('spac es')
      find('body').send_keys(:escape)
      find('body').click
      page.execute_script("document.querySelector('input[type=submit]').click()")
      using_wait_time 3 do
        expect(page).to have_content('1 error prohibited this')
        expect(page).to have_content('Taggings - spac es is invalid. Tags can only contain letters, numbers, dashes, underscores, or pluses and be less than 100 characters.')
      end
    end
  end

  scenario 'add tag already associated as group member', js: true do
    sign_in @group_member
    UserItems.item_list.each do |item|
      visit send("edit_#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      fill_in 'tag_name', with: @old_tag.name
      click_button 'Add Tag'
      expect(page).to have_button(@old_tag.name)
      expect(page).to have_content('Tag is already added.')
      instance_variable_get("@#{item.singularize}").tags << @new_tag
      fill_in 'tag_name', with: @new_tag.name
      click_button 'Add Tag'
      find('body').send_keys(:escape)
      find('body').click
      page.execute_script("document.querySelector('input[type=submit]').click()")
      using_wait_time 3 do
        expect(page).to have_content("Taggings - #{@new_tag.name} is already associated.")
      end
    end
  end
end
