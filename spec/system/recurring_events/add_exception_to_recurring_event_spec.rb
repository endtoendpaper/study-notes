require 'rails_helper'

describe 'Adds exception to recurring event', type: :system do
  before(:each) do
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group_owner = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group, user_id: @group_owner.id)
    @group.add_member(@group_member, @group.user)

    @calendar_private = FactoryBot.create(:calendar, :private, user_id: @owner.id)
    @calendar_private.collaborate(@collaborator, @calendar_private.user)
    @calendar_private.add_group(@group, @calendar_private.user)

    @date = DateTime.current - 2.months
    @date = DateTime.new(@date.year, @date.month, 15, 7, 10, 0, '+0')

    @event = @calendar_private.recurring_events.create(
        rrule: "FREQ=DAILY;INTERVAL=1;COUNT=3",
        summary: "Recurring-#{Time.current.to_i}",
        start: @date,
        end: @date + 30.minutes,
        location: Faker::Address.full_address,
        description: Faker::Lorem.paragraph(sentence_count: 1, supplemental: false, random_sentences_to_add: 5)
      )
  end

  scenario 'from monthly calendar as admin', js: true do
    sign_in @admin
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@event.summary, minimum: 3)
    click_link(@event.summary, match: :first)
    find_button('Delete Occurrence', match: :first).click
    page.accept_alert
    expect(page).to have_content('Recurring event exception was successfully added.')
    expect(page).to have_content(@event.summary, maximum: 2)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
  end

  scenario 'from by_day calendar as admin', js: true do
    sign_in @admin
    date = @date.in_time_zone(@admin.time_zone)
    visit calendar_by_day_path(@calendar_private.id, year: date.year, month: date.month, day: date.day)
    expect(page).to have_content(@event.summary)
    find_button('Delete Occurrence', match: :first).click
    page.accept_alert
    using_wait_time 5 do
      expect(page).to have_content('Recurring event exception was successfully added.')
      expect(page).to_not have_content(@event.summary)
      expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    end
  end

  scenario 'from monthly calendar as owner', js: true do
    sign_in @owner
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@event.summary, minimum: 3)
    click_link(@event.summary, match: :first)
    find_button('Delete Occurrence', match: :first).click
    page.accept_alert
    using_wait_time 5 do
      expect(page).to have_content('Recurring event exception was successfully added.')
      expect(page).to have_content(@event.summary, maximum: 2)
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    end
  end

  scenario 'from by_day calendar as owner', js: true do
    sign_in @owner
    date = @date.in_time_zone(@owner.time_zone)
    visit calendar_by_day_path(@calendar_private.id, year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content(@event.summary)
    find_button('Delete Occurrence', match: :first).click
    page.accept_alert
    using_wait_time 5 do
      expect(page).to have_content('Recurring event exception was successfully added.')
      expect(page).to_not have_content(@event.summary)
      expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    end
  end

  scenario 'from monthly calendar as collaborator', js: true do
    sign_in @collaborator
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@event.summary, minimum: 3)
    click_link(@event.summary, match: :first)
    find_button('Delete Occurrence', match: :first).click
    page.accept_alert
    using_wait_time 5 do
      expect(page).to have_content('Recurring event exception was successfully added.')
      expect(page).to have_content(@event.summary, maximum: 2)
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    end
  end

  scenario 'from by_day calendar as collaborator', js: true do
    sign_in @collaborator
    date = @date.in_time_zone(@collaborator.time_zone)
    visit calendar_by_day_path(@calendar_private.id, year: date.year, month: date.month, day: date.day)
    expect(page).to have_content(@event.summary)
    find_button('Delete Occurrence', match: :first).click
    page.accept_alert
    using_wait_time 5 do
      expect(page).to have_content('Recurring event exception was successfully added.')
      expect(page).to_not have_content(@event.summary)
      expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    end
  end

  scenario 'from monthly calendar as group member', js: true do
    sign_in @group_member
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@event.summary, minimum: 3)
    click_link(@event.summary, match: :first)
    find_button('Delete Occurrence', match: :first).click
    page.accept_alert
    using_wait_time 5 do
      expect(page).to have_content('Recurring event exception was successfully added.')
      expect(page).to have_content(@event.summary, maximum: 2)
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    end
  end

  scenario 'from by_day calendar as group member', js: true do
    sign_in @group_member
    date = @date.in_time_zone(@group_member.time_zone)
    visit calendar_by_day_path(@calendar_private.id, year: date.year, month: date.month, day: date.day)
    expect(page).to have_content(@event.summary)
    find_button('Delete Occurrence', match: :first).click
    page.accept_alert
    using_wait_time 5 do
      expect(page).to have_content('Recurring event exception was successfully added.')
      expect(page).to_not have_content(@event.summary)
      expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    end
  end

  scenario 'from monthly calendar as group owner', js: true do
    sign_in @group_owner
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@event.summary, minimum: 3)
    click_link(@event.summary, match: :first)
    find_button('Delete Occurrence', match: :first).click
    page.accept_alert
    using_wait_time 5 do
      expect(page).to have_content('Recurring event exception was successfully added.')
      expect(page).to have_content(@event.summary, maximum: 2)
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    end
  end

  scenario 'from by_day calendar as group owner', js: true do
    sign_in @group_owner
    date = @date.in_time_zone(@group_owner.time_zone)
    visit calendar_by_day_path(@calendar_private.id, year: date.year, month: date.month, day: date.day)
    expect(page).to have_content(@event.summary)
    find_button('Delete Occurrence', match: :first).click
    page.accept_alert
    using_wait_time 5 do
      expect(page).to have_content('Recurring event exception was successfully added.')
      expect(page).to_not have_content(@event.summary)
      expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    end
  end

  scenario 'from monthly calendar as group owner', js: true do
    sign_in @group_owner
    visit user_calendar_path(year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content(@event.summary, minimum: 3)
    click_link(@event.summary, match: :first)
    find_button('Delete Occurrence', match: :first).click
    page.accept_alert
    using_wait_time 5 do
      expect(page).to have_content('Recurring event exception was successfully added.')
      expect(page).to have_content(@event.summary, maximum: 2)
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    end
  end
end
