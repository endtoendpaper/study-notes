require 'rails_helper'

describe 'Edits recurring event', type: :system do
  before(:each) do
    reset_elasticsearch_indices

    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group_owner = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group, user_id: @group_owner.id)
    @group.add_member(@group_member, @group.user)

    @calendar_private = FactoryBot.create(:calendar, :private, user_id: @owner.id)
    @calendar_private.collaborate(@collaborator, @calendar_private.user)
    @calendar_private.add_group(@group, @calendar_private.user)

    @date = (DateTime.new(DateTime.current.year, DateTime.current.month, 15, 7, 10, 0, @owner.time_zone)) - 2.months
    @date = DateTime.new(@date.year, @date.month, 15, 7, 10, 0, '+0')

    @event = @calendar_private.recurring_events.create(
        rrule: "FREQ=MONTHLY;BYMONTHDAY=#{@date.day};INTERVAL=1;COUNT=4",
        summary: "Recurring-#{Time.current.to_i}",
        start: @date,
        end: @date + 30.minutes,
        location: Faker::Address.full_address,
        description: Faker::Lorem.paragraph(sentence_count: 1, supplemental: false, random_sentences_to_add: 5)
      )

    reindex_elasticsearch_data
  end

  scenario 'with valid data as admin', js: true do
    sign_in @admin
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link @event.summary, match: :first
    click_link 'Edit recurring event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link 'Show recurring event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link 'Edit recurring event'
    fill_in 'recurring_event_summary', with: "My new summary!"
    click_button 'Submit'
    using_wait_time 20 do
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
      expect(page).to have_content("Recurring event was successfully updated.")
      expect(page).to have_content("My new summary!", minimum: 2)
      expect(page).to_not have_content("(all day)")
    end
  end

  scenario 'with valid data as owner', js: true do
    sign_in @owner
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link @event.summary, match: :first
    click_link 'Edit recurring event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link 'Show recurring event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link 'Edit recurring event'
    fill_in 'recurring_event_summary', with: "My new summary!"
    click_button 'Submit'
    using_wait_time 20 do
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
      expect(page).to have_content("Recurring event was successfully updated.")
      expect(page).to_not have_content("(all day)")
      expect(page).to have_content("My new summary!", minimum: 2)
    end
  end

  scenario 'with valid data as collaborator', js: true do
    sign_in @collaborator
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link @event.summary, match: :first
    click_link 'Edit recurring event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link 'Show recurring event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link 'Edit recurring event'
    fill_in 'recurring_event_summary', with: "My new summary!"
    click_button 'Submit'
    using_wait_time 20 do
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
      expect(page).to have_content("Recurring event was successfully updated.")
      expect(page).to have_content("My new summary!", minimum: 2)
      expect(page).to_not have_content("(all day)")
    end
  end

  scenario 'with valid data as group member', js: true do
    sign_in @group_member
    date = @date.in_time_zone(@group_member.time_zone)
    visit calendar_by_day_path(@calendar_private.id, year: date.year, month: date.month, day: date.day)
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    click_link 'Edit recurring event'
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    fill_in 'recurring_event_summary', with: "My new summary!"
    click_button 'Submit'
    using_wait_time 20 do
      expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
      expect(page).to have_content("My new summary!", minimum: 2)
      expect(page).to have_content("Recurring event was successfully updated.")
      expect(page).to_not have_content("(all day)")
    end
  end

  scenario 'with valid data as group owner', js: true do
    sign_in @group_owner
    date = @date.in_time_zone(@group_owner.time_zone)
    visit calendar_by_day_path(@calendar_private.id, year: date.year, month: date.month, day: date.day)
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    click_link 'Edit recurring event'
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    fill_in 'recurring_event_summary', with: "My new summary!"
    click_button 'Submit'
    using_wait_time 20 do
      expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
      expect(page).to have_content("My new summary!", minimum: 2)
      expect(page).to have_content("Recurring event was successfully updated.")
      expect(page).to_not have_content("(all day)")
    end
  end

  scenario 'all day as admin', js: true do
    sign_in @admin
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link @event.summary, match: :first
    click_link 'Edit recurring event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    page.check('recurring_event_all_day')
    click_button 'Submit'
    using_wait_time 20 do
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
      expect(page).to have_content("Recurring event was successfully updated.")
      expect(page).to have_content("(all day)")
    end
  end

  scenario 'all day as owner', js: true do
    sign_in @owner
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link @event.summary, match: :first
    click_link 'Edit recurring event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    page.check('recurring_event_all_day')
    click_button 'Submit'
    using_wait_time 20 do
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
      expect(page).to have_content("Recurring event was successfully updated.")
      expect(page).to have_content("(all day)")
    end
  end

  scenario 'all day as collaborator', js: true do
    sign_in @collaborator
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link @event.summary, match: :first
    click_link 'Edit recurring event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    page.check('recurring_event_all_day')
    click_button 'Submit'
    using_wait_time 20 do
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
      expect(page).to have_content("Recurring event was successfully updated.")
      expect(page).to have_content("(all day)")
    end
  end

  scenario 'all day as group member', js: true do
    sign_in @group_member
    date = @date.in_time_zone(@group_member.time_zone)
    visit calendar_by_day_path(@calendar_private.id, year: date.year, month: date.month, day: date.day)
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    expect(page).to_not have_content("(all day)")
    click_link 'Edit recurring event'
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    page.check('recurring_event_all_day')
    click_button 'Submit'
    using_wait_time 20 do
      expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
      expect(page).to have_content("Recurring event was successfully updated.")
      expect(page).to have_content("(all day)")
    end
  end

  scenario 'all day as group owner', js: true do
    sign_in @group_owner
    date = @date.in_time_zone(@group_owner.time_zone)
    visit calendar_by_day_path(@calendar_private.id, year: date.year, month: date.month, day: date.day)
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    expect(page).to_not have_content("(all day)")
    click_link 'Edit recurring event'
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    page.check('recurring_event_all_day')
    click_button 'Submit'
    using_wait_time 20 do
      expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
      expect(page).to have_content("Recurring event was successfully updated.")
      expect(page).to have_content("(all day)")
    end
  end

  scenario 'with invalid data as admin', js: true do
    sign_in @admin
    date = @date.in_time_zone(@admin.time_zone)
    visit calendar_by_day_path(@calendar_private.id, year: date.year, month: date.month, day: date.day)
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    expect(page).to_not have_content("(all day)")
    click_link 'Edit recurring event'
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    fill_in 'recurring_event_summary', with: ''
    click_button 'Submit'
    using_wait_time 20 do
      expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
      expect(page).to have_content("1 error prohibited this recurring event from being saved:")
      expect(page).to have_content("Summary can't be blank")
    end
  end

  scenario 'with invalid data as owner', js: true do
    sign_in @owner
    date = @date.in_time_zone(@owner.time_zone)
    visit calendar_by_day_path(@calendar_private.id, year: date.year, month: date.month, day: date.day)
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    expect(page).to_not have_content("(all day)")
    click_link 'Edit recurring event'
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    fill_in 'recurring_event_summary', with: ''
    click_button 'Submit'
    using_wait_time 20 do
      expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
      expect(page).to have_content("1 error prohibited this recurring event from being saved:")
      expect(page).to have_content("Summary can't be blank")
    end
  end

  scenario 'with invalid data as collaborator', js: true do
    sign_in @collaborator
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link @event.summary, match: :first
    click_link 'Edit recurring event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    fill_in 'recurring_event_summary', with: ''
    click_button 'Submit'
    using_wait_time 20 do
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
      expect(page).to have_content("1 error prohibited this recurring event from being saved:")
      expect(page).to have_content("Summary can't be blank")
    end
  end

  scenario 'with invalid data as group member', js: true do
    sign_in @group_member
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link @event.summary, match: :first
    click_link 'Edit recurring event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    fill_in 'recurring_event_summary', with: ''
    click_button 'Submit'
    using_wait_time 20 do
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
      expect(page).to have_content("1 error prohibited this recurring event from being saved:")
      expect(page).to have_content("Summary can't be blank")
    end
  end

  scenario 'with invalid data as group owner', js: true do
    sign_in @group_owner
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link @event.summary, match: :first
    click_link 'Edit recurring event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    fill_in 'recurring_event_summary', with: ''
    click_button 'Submit'
    using_wait_time 20 do
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
      expect(page).to have_content("1 error prohibited this recurring event from being saved:")
      expect(page).to have_content("Summary can't be blank")
    end
  end

  scenario 'with invalid data as owner', js: true do
    sign_in @owner
    visit user_calendar_path(year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    expect(page).to_not have_content("(all day)")
    expect(page).to have_content('All Calendar Events')
    click_link @event.summary, match: :first
    click_link 'Edit recurring event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    fill_in 'recurring_event_summary', with: ''
    click_button 'Submit'
    using_wait_time 20 do
      expect(page).to have_content('All Calendar Events')
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
      expect(page).to have_content("1 error prohibited this recurring event from being saved:")
      expect(page).to have_content("Summary can't be blank")
    end
  end

  scenario 'with invalid data as collaborator', js: true do
    sign_in @collaborator
    visit user_calendar_path(year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    expect(page).to have_content('All Calendar Events')
    click_link @event.summary, match: :first
    click_link 'Edit recurring event'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    fill_in 'recurring_event_summary', with: ''
    click_button 'Submit'
    using_wait_time 20 do
      expect(page).to have_content('All Calendar Events')
      expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
      expect(page).to have_content("1 error prohibited this recurring event from being saved:")
      expect(page).to have_content("Summary can't be blank")
    end
  end
end
