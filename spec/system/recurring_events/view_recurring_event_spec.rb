require 'rails_helper'

describe 'Views recurring event', type: :system do
  before(:each) do
    reset_elasticsearch_indices

    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group_owner = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group, user_id: @group_owner.id)
    @group.add_member(@group_member, @group.user)

    @calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
    @calendar_public.collaborate(@collaborator, @calendar_public.user)
    @calendar_public.add_group(@group, @calendar_public.user)

    @date = DateTime.current + 4.months
    @date = DateTime.new(@date.year, @date.month, 15, 7, 10, 0, '+0')

    @event = @calendar_public.recurring_events.create(
        rrule: "FREQ=DAILY;INTERVAL=1;COUNT=10",
        summary: "My-event-#{Time.current.to_i}",
        start: @date,
        end: @date + 30.minutes,
        location: "#{Faker::Address.full_address} #{Time.current.to_i}",
        description: Faker::Lorem.paragraph(sentence_count: 1, supplemental: false, random_sentences_to_add: 5)
      )
    @all_day_event = @calendar_public.recurring_events.create(
        rrule: "FREQ=DAILY;INTERVAL=1;COUNT=10",
        all_day: true,
        summary: "All-Day-#{Time.current.to_i}",
        start: @date,
        end: @date + 30.minutes,
        location: "#{Faker::Address.full_address} #{Time.current.to_i}",
        description: Faker::Lorem.paragraph(sentence_count: 1, supplemental: false, random_sentences_to_add: 5)
      )

    reindex_elasticsearch_data
  end

  scenario 'as admin', js: true do
    sign_in @admin
    visit calendar_path(@calendar_public.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@calendar_public.updated_at.in_time_zone(@admin.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    click_link(@event.summary, :match => :first)
    expect(page).to have_content(@calendar_public.updated_at.in_time_zone(@admin.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.summary, minimum: 11)
    expect(page).to have_content(@event.location)
    expect(page).to have_content(@event.start.in_time_zone(@admin.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.end.in_time_zone(@admin.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.description.plain_text_body)
    expect(page).to have_content(@event.updated_at.in_time_zone(@admin.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to_not have_content("(all day)")
    expect(page).to have_content('Public')
    expect(page).to have_link('Edit recurring event')
    expect(page).to have_button('Delete Occurrence')
    find('.btn-close').click
    click_link(@all_day_event.summary, :match => :first)
    expect(page).to have_content(@all_day_event.summary, minimum: 11)
    expect(page).to have_content(@all_day_event.location)
    expect(page).to have_content(@all_day_event.start.strftime("%b") + " " + @all_day_event.start.strftime("%d") + ", " + @all_day_event.start.strftime("%Y") + " (all day)")
    expect(page).to have_content(@all_day_event.end.strftime("%b") + " " + @all_day_event.end.strftime("%d") + ", " + @all_day_event.end.strftime("%Y") + " (all day)")
    expect(page).to have_content(@all_day_event.description.plain_text_body)
    expect(page).to have_content(@all_day_event.updated_at.in_time_zone(@admin.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content('Public')
    expect(page).to have_link('Edit recurring event')
    expect(page).to have_button('Delete Occurrence')
    find('.btn-close').click
    expect(page).to have_content(@calendar_public.updated_at.in_time_zone(@admin.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
  end

  scenario 'as owner', js: true do
    sign_in @owner
    visit calendar_path(@calendar_public.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@calendar_public.updated_at.in_time_zone(@owner.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    click_link(@event.summary, :match => :first)
    expect(page).to have_content(@calendar_public.updated_at.in_time_zone(@owner.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.summary, minimum: 11)
    expect(page).to have_content(@event.location)
    expect(page).to have_content(@event.start.in_time_zone(@owner.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.end.in_time_zone(@owner.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.description.plain_text_body)
    expect(page).to have_content(@event.updated_at.in_time_zone(@owner.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content('Public')
    expect(page).to have_link('Edit recurring event')
    expect(page).to have_button('Delete Occurrence')
    find('.btn-close').click
    click_link(@all_day_event.summary, :match => :first)
    expect(page).to have_content(@all_day_event.summary, minimum: 11)
    expect(page).to have_content(@all_day_event.location)
    expect(page).to have_content(@all_day_event.start.strftime("%b") + " " + @all_day_event.start.strftime("%d") + ", " + @all_day_event.start.strftime("%Y") + " (all day)")
    expect(page).to have_content(@all_day_event.end.strftime("%b") + " " + @all_day_event.end.strftime("%d") + ", " + @all_day_event.end.strftime("%Y") + " (all day)")
    expect(page).to have_content(@all_day_event.description.plain_text_body)
    expect(page).to have_content(@all_day_event.updated_at.in_time_zone(@owner.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content('Public')
    expect(page).to have_link('Edit recurring event')
    expect(page).to have_button('Delete Occurrence')
    find('.btn-close').click
    expect(page).to have_content(@calendar_public.updated_at.in_time_zone(@owner.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
  end

  scenario 'as collaborator', js: true do
    sign_in @collaborator
    visit calendar_path(@calendar_public.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@calendar_public.updated_at.in_time_zone(@collaborator.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    click_link(@event.summary, :match => :first)
    expect(page).to have_content(@calendar_public.updated_at.in_time_zone(@collaborator.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.summary, minimum: 11)
    expect(page).to have_content(@event.location)
    expect(page).to have_content(@event.start.in_time_zone(@collaborator.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.end.in_time_zone(@collaborator.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.description.plain_text_body)
    expect(page).to have_content(@event.updated_at.in_time_zone(@collaborator.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content('Public')
    expect(page).to have_link('Edit recurring event')
    expect(page).to have_button('Delete Occurrence')
    find('.btn-close').click
    click_link(@all_day_event.summary, :match => :first)
    expect(page).to have_content(@all_day_event.summary, minimum: 11)
    expect(page).to have_content(@all_day_event.location)
    expect(page).to have_content(@all_day_event.start.strftime("%b") + " " + @all_day_event.start.strftime("%d") + ", " + @all_day_event.start.strftime("%Y") + " (all day)")
    expect(page).to have_content(@all_day_event.end.strftime("%b") + " " + @all_day_event.end.strftime("%d") + ", " + @all_day_event.end.strftime("%Y") + " (all day)")
    expect(page).to have_content(@all_day_event.description.plain_text_body)
    expect(page).to have_content(@all_day_event.updated_at.in_time_zone(@collaborator.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content('Public')
    expect(page).to have_link('Edit recurring event')
    expect(page).to have_button('Delete Occurrence')
    find('.btn-close').click
    expect(page).to have_content(@calendar_public.updated_at.in_time_zone(@collaborator.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
  end

  scenario 'as group member', js: true do
    sign_in @group_member
    visit calendar_path(@calendar_public.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@calendar_public.updated_at.in_time_zone(@group_member.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    click_link(@event.summary, :match => :first)
    expect(page).to have_content(@calendar_public.updated_at.in_time_zone(@group_member.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.summary, minimum: 11)
    expect(page).to have_content(@event.location)
    expect(page).to have_content(@event.start.in_time_zone(@group_member.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.end.in_time_zone(@group_member.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.description.plain_text_body)
    expect(page).to have_content(@event.updated_at.in_time_zone(@group_member.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content('Public')
    expect(page).to have_link('Edit recurring event')
    expect(page).to have_button('Delete Occurrence')
    find('.btn-close').click
    click_link(@all_day_event.summary, :match => :first)
    expect(page).to have_content(@all_day_event.summary, minimum: 11)
    expect(page).to have_content(@all_day_event.location)
    expect(page).to have_content(@all_day_event.start.strftime("%b") + " " + @all_day_event.start.strftime("%d") + ", " + @all_day_event.start.strftime("%Y") + " (all day)")
    expect(page).to have_content(@all_day_event.end.strftime("%b") + " " + @all_day_event.end.strftime("%d") + ", " + @all_day_event.end.strftime("%Y") + " (all day)")
    expect(page).to have_content(@all_day_event.description.plain_text_body)
    expect(page).to have_content(@all_day_event.updated_at.in_time_zone(@group_member.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content('Public')
    expect(page).to have_link('Edit recurring event')
    expect(page).to have_button('Delete Occurrence')
    find('.btn-close').click
    expect(page).to have_content(@calendar_public.updated_at.in_time_zone(@group_member.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
  end

  scenario 'as group owner', js: true do
    sign_in @group_owner
    visit calendar_path(@calendar_public.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@calendar_public.updated_at.in_time_zone(@group_owner.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    click_link(@event.summary, :match => :first)
    expect(page).to have_content(@calendar_public.updated_at.in_time_zone(@group_owner.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.summary, minimum: 11)
    expect(page).to have_content(@event.location)
    expect(page).to have_content(@event.start.in_time_zone(@group_owner.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.end.in_time_zone(@group_owner.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.description.plain_text_body)
    expect(page).to have_content(@event.updated_at.in_time_zone(@group_owner.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content('Public')
    expect(page).to have_link('Edit recurring event')
    expect(page).to have_button('Delete Occurrence')
    find('.btn-close').click
    click_link(@all_day_event.summary, :match => :first)
    expect(page).to have_content(@all_day_event.summary, minimum: 11)
    expect(page).to have_content(@all_day_event.location)
    expect(page).to have_content(@all_day_event.start.strftime("%b") + " " + @all_day_event.start.strftime("%d") + ", " + @all_day_event.start.strftime("%Y") + " (all day)")
    expect(page).to have_content(@all_day_event.end.strftime("%b") + " " + @all_day_event.end.strftime("%d") + ", " + @all_day_event.end.strftime("%Y") + " (all day)")
    expect(page).to have_content(@all_day_event.description.plain_text_body)
    expect(page).to have_content(@all_day_event.updated_at.in_time_zone(@group_owner.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content('Public')
    expect(page).to have_link('Edit recurring event')
    expect(page).to have_button('Delete Occurrence')
    find('.btn-close').click
    expect(page).to have_content(@calendar_public.updated_at.in_time_zone(@group_owner.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
  end

  scenario 'as other user', js: true do
    sign_in @user
    visit calendar_path(@calendar_public.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@calendar_public.updated_at.in_time_zone(@user.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    click_link(@event.summary, :match => :first)
    expect(page).to have_content(@calendar_public.updated_at.in_time_zone(@user.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.summary, minimum: 11)
    expect(page).to have_content(@event.location)
    expect(page).to have_content(@event.start.in_time_zone(@user.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.end.in_time_zone(@user.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.description.plain_text_body)
    expect(page).to have_content(@event.updated_at.in_time_zone(@user.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to_not have_content('Public')
    expect(page).to_not have_link('Edit recurring event')
    expect(page).to_not have_button('Delete Occurrence')
    find('.btn-close').click
    click_link(@all_day_event.summary, :match => :first)
    expect(page).to have_content(@all_day_event.summary, minimum: 11)
    expect(page).to have_content(@all_day_event.location)
    expect(page).to have_content(@all_day_event.start.strftime("%b") + " " + @all_day_event.start.strftime("%d") + ", " + @all_day_event.start.strftime("%Y") + " (all day)")
    expect(page).to have_content(@all_day_event.end.strftime("%b") + " " + @all_day_event.end.strftime("%d") + ", " + @all_day_event.end.strftime("%Y") + " (all day)")
    expect(page).to have_content(@all_day_event.description.plain_text_body)
    expect(page).to have_content(@all_day_event.updated_at.in_time_zone(@user.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to_not have_content('Public')
    expect(page).to_not have_link('Edit recurring event')
    expect(page).to_not have_button('Delete Occurrence')
    find('.btn-close').click
    expect(page).to have_content(@calendar_public.updated_at.in_time_zone(@user.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
  end

  scenario 'for public as not logged in', js: true do
    visit calendar_path(@calendar_public.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@calendar_public.updated_at.in_time_zone('UTC').strftime('%b %d, %Y %I:%M%p %Z'))
    click_link(@event.summary, :match => :first)
    expect(page).to have_content(@calendar_public.updated_at.in_time_zone('UTC').strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.summary, minimum: 11)
    expect(page).to have_content(@event.location)
    expect(page).to have_content(@event.start.in_time_zone('UTC').strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.end.in_time_zone('UTC').strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.description.plain_text_body)
    expect(page).to have_content(@event.updated_at.in_time_zone('UTC').strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to_not have_content('Public')
    expect(page).to_not have_link('Edit recurring event')
    expect(page).to_not have_button('Delete Occurrence')
    find('.btn-close').click
    click_link(@all_day_event.summary, :match => :first)
    expect(page).to have_content(@all_day_event.summary, minimum: 11)
    expect(page).to have_content(@all_day_event.location)
    expect(page).to have_content(@all_day_event.start.strftime("%b") + " " + @all_day_event.start.strftime("%d") + ", " + @all_day_event.start.strftime("%Y") + " (all day)")
    expect(page).to have_content(@all_day_event.end.strftime("%b") + " " + @all_day_event.end.strftime("%d") + ", " + @all_day_event.end.strftime("%Y") + " (all day)")
    expect(page).to have_content(@all_day_event.description.plain_text_body)
    expect(page).to have_content(@all_day_event.updated_at.in_time_zone('UTC').strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to_not have_content('Public')
    expect(page).to_not have_link('Edit recurring event')
    expect(page).to_not have_button('Delete Occurrence')
    find('.btn-close').click
    expect(page).to have_content(@calendar_public.updated_at.in_time_zone('UTC').strftime('%b %d, %Y %I:%M%p %Z'))
  end

  scenario 'on all my events page as owner', js: true do
    sign_in @owner
    visit user_calendar_path(year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content("All Calendar Events")
    click_link(@event.summary, :match => :first)
    expect(page).to have_content(@event.summary, minimum: 11)
    expect(page).to have_content(@event.parent.title)
    expect(page).to have_content(@event.location)
    expect(page).to have_content(@event.start.in_time_zone(@owner.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.end.in_time_zone(@owner.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.description.plain_text_body)
    expect(page).to have_content(@event.updated_at.in_time_zone(@owner.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content('Public')
    expect(page).to have_link('Edit recurring event')
    find('.btn-close').click
    click_link(@all_day_event.summary, :match => :first)
    expect(page).to have_content("All Calendar Events")
    expect(page).to have_content(@all_day_event.summary, minimum: 11)
    expect(page).to have_content(@event.parent.title)
    expect(page).to have_content(@all_day_event.location)
    expect(page).to have_content(@all_day_event.start.strftime("%b") + " " + @all_day_event.start.strftime("%d") + ", " + @all_day_event.start.strftime("%Y") + " (all day)")
    expect(page).to have_content(@all_day_event.end.strftime("%b") + " " + @all_day_event.end.strftime("%d") + ", " + @all_day_event.end.strftime("%Y") + " (all day)")
    expect(page).to have_content(@all_day_event.description.plain_text_body)
    expect(page).to have_content(@all_day_event.updated_at.in_time_zone(@owner.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content('Public')
    expect(page).to have_link('Edit recurring event')
    expect(page).to have_button('Delete Occurrence')
    find('.btn-close').click
  end

  scenario 'as all my events collaborator', js: true do
    sign_in @collaborator
    visit user_calendar_path(year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content("All Calendar Events")
    click_link(@event.summary, :match => :first)
    expect(page).to have_content(@event.summary, minimum: 11)
    expect(page).to have_content(@event.parent.title)
    expect(page).to have_content(@event.location)
    expect(page).to have_content(@event.start.in_time_zone(@collaborator.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.end.in_time_zone(@collaborator.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.description.plain_text_body)
    expect(page).to have_content(@event.updated_at.in_time_zone(@collaborator.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content('Public')
    expect(page).to have_link('Edit recurring event')
    find('.btn-close').click
    click_link(@all_day_event.summary, :match => :first)
    expect(page).to have_content("All Calendar Events")
    expect(page).to have_content(@all_day_event.summary, minimum: 11)
    expect(page).to have_content(@event.parent.title)
    expect(page).to have_content(@all_day_event.location)
    expect(page).to have_content(@all_day_event.start.strftime("%b") + " " + @all_day_event.start.strftime("%d") + ", " + @all_day_event.start.strftime("%Y") + " (all day)")
    expect(page).to have_content(@all_day_event.end.strftime("%b") + " " + @all_day_event.end.strftime("%d") + ", " + @all_day_event.end.strftime("%Y") + " (all day)")
    expect(page).to have_content(@all_day_event.description.plain_text_body)
    expect(page).to have_content(@all_day_event.updated_at.in_time_zone(@collaborator.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content('Public')
    expect(page).to have_link('Edit recurring event')
    expect(page).to have_button('Delete Occurrence')
    find('.btn-close').click
  end

  scenario 'as all my events group member', js: true do
    sign_in @group_member
    visit user_calendar_path(year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content("All Calendar Events")
    click_link(@event.summary, :match => :first)
    expect(page).to have_content(@event.summary, minimum: 11)
    expect(page).to have_content(@event.parent.title)
    expect(page).to have_content(@event.location)
    expect(page).to have_content(@event.start.in_time_zone(@group_member.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.end.in_time_zone(@group_member.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.description.plain_text_body)
    expect(page).to have_content(@event.updated_at.in_time_zone(@group_member.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content('Public')
    expect(page).to have_link('Edit recurring event')
    find('.btn-close').click
    click_link(@all_day_event.summary, :match => :first)
    expect(page).to have_content("All Calendar Events")
    expect(page).to have_content(@all_day_event.summary, minimum: 11)
    expect(page).to have_content(@event.parent.title)
    expect(page).to have_content(@all_day_event.location)
    expect(page).to have_content(@all_day_event.start.strftime("%b") + " " + @all_day_event.start.strftime("%d") + ", " + @all_day_event.start.strftime("%Y") + " (all day)")
    expect(page).to have_content(@all_day_event.end.strftime("%b") + " " + @all_day_event.end.strftime("%d") + ", " + @all_day_event.end.strftime("%Y") + " (all day)")
    expect(page).to have_content(@all_day_event.description.plain_text_body)
    expect(page).to have_content(@all_day_event.updated_at.in_time_zone(@group_member.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content('Public')
    expect(page).to have_link('Edit recurring event')
    expect(page).to have_button('Delete Occurrence')
    find('.btn-close').click
  end

  scenario 'as all my events group owner', js: true do
    sign_in @group_owner
    visit user_calendar_path(year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content("All Calendar Events")
    click_link(@event.summary, :match => :first)
    expect(page).to have_content(@event.summary, minimum: 11)
    expect(page).to have_content(@event.parent.title)
    expect(page).to have_content(@event.location)
    expect(page).to have_content(@event.start.in_time_zone(@group_owner.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.end.in_time_zone(@group_owner.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content(@event.description.plain_text_body)
    expect(page).to have_content(@event.updated_at.in_time_zone(@group_owner.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content('Public')
    expect(page).to have_link('Edit recurring event')
    find('.btn-close').click
    click_link(@all_day_event.summary, :match => :first)
    expect(page).to have_content("All Calendar Events")
    expect(page).to have_content(@all_day_event.summary, minimum: 11)
    expect(page).to have_content(@event.parent.title)
    expect(page).to have_content(@all_day_event.location)
    expect(page).to have_content(@all_day_event.start.strftime("%b") + " " + @all_day_event.start.strftime("%d") + ", " + @all_day_event.start.strftime("%Y") + " (all day)")
    expect(page).to have_content(@all_day_event.end.strftime("%b") + " " + @all_day_event.end.strftime("%d") + ", " + @all_day_event.end.strftime("%Y") + " (all day)")
    expect(page).to have_content(@all_day_event.description.plain_text_body)
    expect(page).to have_content(@all_day_event.updated_at.in_time_zone(@group_owner.time_zone).strftime('%b %d, %Y %I:%M%p %Z'))
    expect(page).to have_content('Public')
    expect(page).to have_link('Edit recurring event')
    expect(page).to have_button('Delete Occurrence')
    find('.btn-close').click
  end

  scenario 'as all my events other user', js: true do
    sign_in @user
    visit user_calendar_path(year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content("All Calendar Events")
    expect(page).to_not have_content(@event.summary)
    expect(page).to_not have_content(@all_day_event.summary)
  end

  scenario 'as all my events admin', js: true do
    sign_in @user
    visit user_calendar_path(year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content("All Calendar Events")
    expect(page).to_not have_content(@event.summary)
    expect(page).to_not have_content(@all_day_event.summary)
  end
end
