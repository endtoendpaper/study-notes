require 'rails_helper'

describe 'Deletes recurring event', type: :system do
  before(:each) do
    reset_elasticsearch_indices

    @owner = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)

    @calendar_private = FactoryBot.create(:calendar, :private, user_id: @owner.id)

    @date = DateTime.current - 1.month
    @date = DateTime.new(@date.year, @date.month, 10, 8, 30, 0, @owner.time_zone)
    @prev_date = @date - 1.day
    @next_date = @date + 1.day

    @event = @calendar_private.recurring_events.create(
        rrule: "FREQ=MONTHLY;BYMONTHDAY=#{@date.day};INTERVAL=1;COUNT=4",
        summary: Faker::Lorem.sentence(word_count: 2, supplemental: true, random_words_to_add: 5),
        start: @date, end: @date +  30.minutes,
        location: Faker::Address.full_address,
        description: Faker::Lorem.paragraph(sentence_count: 1,
        supplemental: false, random_sentences_to_add: 5)
      )

    reindex_elasticsearch_data
  end

  scenario 'from monthly show as admin', js: true do
    sign_in @admin
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@event.summary)
    expect(page).to have_content(@next_date.strftime("%B") + " " + @next_date.strftime("%Y"))
    click_link @event.summary, match: :first
    click_link 'Edit recurring event'
    click_button 'Delete Recurring Event'
    page.accept_alert
    expect(page).to have_content('Recurring event was successfully deleted.')
    expect(page).to have_content(@next_date.strftime("%B") + " " + @next_date.strftime("%Y"))
    expect(page).to_not have_content(@event.summary)
  end

  scenario 'from monthly show as owner', js: true do
    sign_in @owner
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@event.summary)
    expect(page).to have_content(@next_date.strftime("%B") + " " + @next_date.strftime("%Y"))
    click_link @event.summary, match: :first
    click_link 'Edit recurring event'
    click_button 'Delete Recurring Event'
    page.accept_alert
    expect(page).to have_content('Recurring event was successfully deleted.')
    expect(page).to have_content(@next_date.strftime("%B") + " " + @next_date.strftime("%Y"))
    expect(page).to_not have_content(@event.summary)
  end

  scenario 'from by_day show as admin', js: true do
    sign_in @admin
    date = @date.in_time_zone(@admin.time_zone)
    visit calendar_by_day_path(@calendar_private.id, year: date.year, month: date.month, day: date.day)
    expect(page).to have_content(@event.summary)
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    click_link 'Edit recurring event'
    click_button 'Delete Recurring Event'
    page.accept_alert
    expect(page).to have_content('Recurring event was successfully deleted.')
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    expect(page).to_not have_content(@event.summary)
  end

  scenario 'from by_day show as owner', js: true do
    sign_in @owner
    date = @date.in_time_zone(@owner.time_zone)
    visit calendar_by_day_path(@calendar_private.id, year: date.year, month: date.month, day: date.day)
    expect(page).to have_content(@event.summary)
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    click_link 'Edit recurring event'
    click_button 'Delete Recurring Event'
    page.accept_alert
    expect(page).to have_content('Recurring event was successfully deleted.')
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    expect(page).to_not have_content(@event.summary)
  end

  scenario 'from all my events monthly show as owner', js: true do
    sign_in @owner
    visit user_calendar_path(year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content(@event.summary)
    expect(page).to have_content("All Calendar Events")
    expect(page).to have_content(@next_date.strftime("%B") + " " + @next_date.strftime("%Y"))
    click_link @event.summary, match: :first
    click_link 'Edit recurring event'
    click_button 'Delete Recurring Event'
    page.accept_alert
    expect(page).to have_content('Recurring event was successfully deleted.')
    expect(page).to have_content("All Calendar Events")
    expect(page).to have_content(@next_date.strftime("%B") + " " + @next_date.strftime("%Y"))
    expect(page).to_not have_content(@event.summary)
  end

  scenario 'from all my events by_day show as owner', js: true do
    sign_in @owner
    date = @date.in_time_zone(@owner.time_zone)
    visit user_calendar_by_day_path(@calendar_private.id, year: date.year, month: date.month, day: date.day)
    expect(page).to have_content(@event.summary)
    expect(page).to have_content("All Calendar Events")
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    click_link 'Edit recurring event'
    click_button 'Delete Recurring Event'
    page.accept_alert
    expect(page).to have_content('Recurring event was successfully deleted.')
    expect(page).to have_content("All Calendar Events")
    expect(page).to have_content(date.strftime("%A") + ", " + date.strftime("%B") + " " + date.strftime("%d") + ", " + date.strftime("%Y"))
    expect(page).to_not have_content(@event.summary)
  end
end
