require 'rails_helper'

describe 'Creates event', type: :system do
  let(:description) { Faker::Lorem.paragraph(sentence_count: 1, supplemental: false, random_sentences_to_add: 5) }
  let(:location) { Faker::Address.full_address }

  before(:each) do
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group_owner = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group, user_id: @group_owner.id)
    @group.add_member(@group_member, @group.user)

    @calendar_private = FactoryBot.create(:calendar, :private, user_id: @owner.id)
    @calendar_private.collaborate(@collaborator, @calendar_private.user)
    @calendar_private.add_group(@group, @calendar_private.user)

    @date = DateTime.current + 1.month
    @date = DateTime.new(@date.year, @date.month, 15, 7, 10, 0, '+0')
  end

  scenario 'with valid data as admin', js: true do
    summary = "#{Faker::Esport.event} #{Time.current.to_i}"
    sign_in @admin
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link 'Add recurring event'
    fill_in 'recurring_event_summary', with: summary
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in('recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p'))
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    click_button 'Submit'
    expect(page).to have_content('Recurring event was successfully created.')
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    expect(page).to have_content(summary)
    expect(page).to_not have_content("(all day)")
    expect(page).to have_content("FREQ=MONTHLY;INTERVAL=1;BYMONTHDAY=#{@date.day};COUNT=12")
  end


  scenario 'with valid data as owner', js: true do
    summary = "#{Faker::Esport.event} #{Time.current.to_i}"
    sign_in @owner
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link 'Add recurring event'
    fill_in 'recurring_event_summary', with: summary
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    click_button 'Submit'
    expect(page).to have_content('Recurring event was successfully created.')
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    expect(page).to have_content(summary)
    expect(page).to_not have_content("(all day)")
    expect(page).to have_content("FREQ=MONTHLY;INTERVAL=1;BYMONTHDAY=#{@date.day};COUNT=12")
  end

  scenario 'with valid data as collaborator', js: true do
    summary = "#{Faker::Esport.event} #{Time.current.to_i}"
    sign_in @collaborator
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link 'Add recurring event'
    fill_in 'recurring_event_summary', with: summary
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    click_button 'Submit'
    expect(page).to have_content('Recurring event was successfully created.')
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    expect(page).to have_content(summary)
    expect(page).to_not have_content("(all day)")
    expect(page).to have_content("FREQ=MONTHLY;INTERVAL=1;BYMONTHDAY=#{@date.day};COUNT=12")
  end

  scenario 'with valid data as group member', js: true do
    summary = "#{Faker::Esport.event} #{Time.current.to_i}"
    sign_in @group_member
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link 'Add recurring event'
    fill_in 'recurring_event_summary', with: summary
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    click_button 'Submit'
    expect(page).to have_content('Recurring event was successfully created.')
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    expect(page).to have_content(summary)
    expect(page).to_not have_content("(all day)")
    expect(page).to have_content("FREQ=MONTHLY;INTERVAL=1;BYMONTHDAY=#{@date.day};COUNT=12")
  end

  scenario 'with valid data as group owner', js: true do
    summary = "#{Faker::Esport.event} #{Time.current.to_i}"
    sign_in @group_owner
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link 'Add recurring event'
    fill_in 'recurring_event_summary', with: summary
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    click_button 'Submit'
    expect(page).to have_content('Recurring event was successfully created.')
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    expect(page).to have_content(summary)
    expect(page).to_not have_content("(all day)")
    expect(page).to have_content("FREQ=MONTHLY;INTERVAL=1;BYMONTHDAY=#{@date.day};COUNT=12")
  end

  scenario 'with valid data as admin on by_day page', js: true do
    summary = "#{Faker::Esport.event} #{Time.current.to_i}"
    sign_in @admin
    visit calendar_by_day_path(@calendar_private.id, year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    click_link 'Add recurring event'
    fill_in 'recurring_event_summary', with: summary
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    click_button 'Submit'
    expect(page).to have_content('Recurring event was successfully created.')
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    expect(page).to have_content(summary)
    expect(page).to_not have_content("(all day)")
    expect(page).to have_content("FREQ=MONTHLY;INTERVAL=1;BYMONTHDAY=#{@date.day};COUNT=12")
  end

  scenario 'with valid data as owner on by_day page', js: true do
    summary = "#{Faker::Esport.event} #{Time.current.to_i}"
    sign_in @owner
    visit calendar_by_day_path(@calendar_private.id, year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    click_link 'Add recurring event'
    fill_in 'recurring_event_summary', with: summary
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    click_button 'Submit'
    expect(page).to have_content('Recurring event was successfully created.')
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    expect(page).to have_content(summary)
    expect(page).to_not have_content("(all day)")
    expect(page).to have_content("FREQ=MONTHLY;INTERVAL=1;BYMONTHDAY=#{@date.day};COUNT=12")
  end

  scenario 'with valid data as collaborator on by_day page', js: true do
    summary = "#{Faker::Esport.event} #{Time.current.to_i}"
    sign_in @collaborator
    visit calendar_by_day_path(@calendar_private.id, year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    click_link 'Add recurring event'
    fill_in 'recurring_event_summary', with: summary
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    click_button 'Submit'
    expect(page).to have_content('Recurring event was successfully created.')
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    expect(page).to have_content(summary)
    expect(page).to_not have_content("(all day)")
    expect(page).to have_content("FREQ=MONTHLY;INTERVAL=1;BYMONTHDAY=#{@date.day};COUNT=12")
  end

  scenario 'with valid data as group member on by_day page', js: true do
    summary = "#{Faker::Esport.event} #{Time.current.to_i}"
    sign_in @group_member
    visit calendar_by_day_path(@calendar_private.id, year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    click_link 'Add recurring event'
    fill_in 'recurring_event_summary', with: summary
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    click_button 'Submit'
    expect(page).to have_content('Recurring event was successfully created.')
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    expect(page).to have_content(summary)
    expect(page).to_not have_content("(all day)")
    expect(page).to have_content("FREQ=MONTHLY;INTERVAL=1;BYMONTHDAY=#{@date.day};COUNT=12")
  end

  scenario 'with valid data as group owner on by_day page', js: true do
    summary = "#{Faker::Esport.event} #{Time.current.to_i}"
    sign_in @group_owner
    visit calendar_by_day_path(@calendar_private.id, year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    click_link 'Add recurring event'
    fill_in 'recurring_event_summary', with: summary
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    click_button 'Submit'
    expect(page).to have_content('Recurring event was successfully created.')
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    expect(page).to have_content(summary)
    expect(page).to_not have_content("(all day)")
    expect(page).to have_content("FREQ=MONTHLY;INTERVAL=1;BYMONTHDAY=#{@date.day};COUNT=12")
  end

  scenario 'all day as admin', js: true do
    summary = "#{Faker::Esport.event} #{Time.current.to_i}"
    sign_in @admin
    visit calendar_by_day_path(@calendar_private.id, year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    click_link 'Add recurring event'
    fill_in 'recurring_event_summary', with: summary
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    page.check('recurring_event_all_day')
    click_button 'Submit'
    expect(page).to have_content('Recurring event was successfully created.')
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    expect(page).to have_content(summary)
    expect(page).to have_content(RecurringEvent.last.start.strftime('%b %d, %Y') + " (all day)")
    expect(page).to have_content("FREQ=MONTHLY;INTERVAL=1;BYMONTHDAY=#{@date.day};COUNT=12")
  end

  scenario 'all day as owner', js: true do
    summary = "#{Faker::Esport.event} #{Time.current.to_i}"
    sign_in @owner
    visit calendar_by_day_path(@calendar_private.id, year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    click_link 'Add recurring event'
    fill_in 'recurring_event_summary', with: summary
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: @date + 2.hours
    page.check('recurring_event_all_day')
    click_button 'Submit'
    expect(page).to have_content('Recurring event was successfully created.')
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    expect(page).to have_content(summary)
    expect(page).to have_content(RecurringEvent.last.start.strftime('%b %d, %Y') + " (all day)")
    expect(page).to have_content("FREQ=MONTHLY;INTERVAL=1;BYMONTHDAY=#{@date.day};COUNT=12")
  end

  scenario 'all day as collaborator', js: true do
    summary = "#{Faker::Esport.event} #{Time.current.to_i}"
    sign_in @collaborator
    visit calendar_by_day_path(@calendar_private.id, year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    click_link 'Add recurring event'
    fill_in 'recurring_event_summary', with: summary
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    page.check('recurring_event_all_day')
    click_button 'Submit'
    expect(page).to have_content('Recurring event was successfully created.')
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    expect(page).to have_content(summary)
    expect(page).to have_content(RecurringEvent.last.start.strftime('%b %d, %Y') + " (all day)")
    expect(page).to have_content("FREQ=MONTHLY;INTERVAL=1;BYMONTHDAY=#{@date.day};COUNT=12")
  end

  scenario 'all day as group member', js: true do
    summary = "#{Faker::Esport.event} #{Time.current.to_i}"
    sign_in @group_member
    visit calendar_by_day_path(@calendar_private.id, year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    click_link 'Add recurring event'
    fill_in 'recurring_event_summary', with: summary
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    page.check('recurring_event_all_day')
    click_button 'Submit'
    expect(page).to have_content('Recurring event was successfully created.')
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    expect(page).to have_content(summary)
    expect(page).to have_content(RecurringEvent.last.start.strftime('%b %d, %Y') + " (all day)")
    expect(page).to have_content("FREQ=MONTHLY;INTERVAL=1;BYMONTHDAY=#{@date.day};COUNT=12")
  end

  scenario 'all day as group owner', js: true do
    summary = "#{Faker::Esport.event} #{Time.current.to_i}"
    sign_in @group_owner
    visit calendar_by_day_path(@calendar_private.id, year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    click_link 'Add recurring event'
    fill_in 'recurring_event_summary', with: summary
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    page.check('recurring_event_all_day')
    click_button 'Submit'
    expect(page).to have_content('Recurring event was successfully created.')
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    expect(page).to have_content(summary)
    expect(page).to have_content(RecurringEvent.last.start.strftime('%b %d, %Y') + " (all day)")
    expect(page).to have_content("FREQ=MONTHLY;INTERVAL=1;BYMONTHDAY=#{@date.day};COUNT=12")
  end

  scenario 'and creates another as admin', js: true do
    summary1 = "#{Faker::Esport.event} #{Time.current.to_i}"
    summary2 = "#{Faker::Esport.event} #{Time.current.to_i}"
    sign_in @admin
    visit calendar_by_day_path(@calendar_private.id, year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    click_link 'Add recurring event'
    fill_in 'recurring_event_summary', with: summary1
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    page.check('recurring_event_create_another')
    click_button 'Submit'
    expect(page).to have_content('Recurring event was successfully created.')
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    expect(page).to have_content(summary1)
    expect(page).to_not have_content("(all day)")
    expect(page).to have_content('New Recurring Event')
    fill_in 'recurring_event_summary', with: summary2
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    click_button 'Submit'
    expect(page).to have_content('Recurring event was successfully created.')
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    expect(page).to have_content(summary2)
    expect(page).to_not have_content("(all day)")
    expect(page).to_not have_content('New Event')
  end

  scenario 'and creates another as owner', js: true do
    summary1 = "#{Faker::Esport.event} #{Time.current.to_i}"
    summary2 = "#{Faker::Esport.event} #{Time.current.to_i}"
    sign_in @owner
    visit calendar_by_day_path(@calendar_private.id, year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    click_link 'Add recurring event'
    fill_in 'recurring_event_summary', with: summary1
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    page.check('recurring_event_create_another')
    click_button 'Submit'
    expect(page).to have_content('Recurring event was successfully created.')
    select('MONTHLY', from: 'frequency')
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    expect(page).to have_content(summary1)
    expect(page).to_not have_content("(all day)")
    expect(page).to have_content('New Recurring Event')
    fill_in 'recurring_event_summary', with: summary2
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    click_button 'Submit'
    expect(page).to have_content('Recurring event was successfully created.')
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    expect(page).to have_content(summary2)
    expect(page).to_not have_content("(all day)")
    expect(page).to_not have_content('New Event')
  end

  scenario 'and creates another as collaborator', js: true do
    summary1 = "#{Faker::Esport.event} #{Time.current.to_i}"
    summary2 = "#{Faker::Esport.event} #{Time.current.to_i}"
    sign_in @collaborator
    visit calendar_by_day_path(@calendar_private.id, year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    click_link 'Add recurring event'
    fill_in 'recurring_event_summary', with: summary1
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    page.check('recurring_event_create_another')
    click_button 'Submit'
    expect(page).to have_content('Recurring event was successfully created.')
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    expect(page).to have_content(summary1)
    expect(page).to_not have_content("(all day)")
    expect(page).to have_content('New Recurring Event')
    fill_in 'recurring_event_summary', with: summary2
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    click_button 'Submit'
    expect(page).to have_content('Recurring event was successfully created.')
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    expect(page).to have_content(summary2)
    expect(page).to_not have_content("(all day)")
    expect(page).to_not have_content('New Event')
  end

  scenario 'and creates another as group member', js: true do
    summary1 = "#{Faker::Esport.event} #{Time.current.to_i}"
    summary2 = "#{Faker::Esport.event} #{Time.current.to_i}"
    sign_in @group_member
    visit calendar_by_day_path(@calendar_private.id, year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    click_link 'Add recurring event'
    fill_in 'recurring_event_summary', with: summary1
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    page.check('recurring_event_create_another')
    click_button 'Submit'
    expect(page).to have_content('Recurring event was successfully created.')
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    expect(page).to have_content(summary1)
    expect(page).to_not have_content("(all day)")
    expect(page).to have_content('New Recurring Event')
    fill_in 'recurring_event_summary', with: summary2
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    click_button 'Submit'
    expect(page).to have_content('Recurring event was successfully created.')
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    expect(page).to have_content(summary2)
    expect(page).to_not have_content("(all day)")
    expect(page).to_not have_content('New Event')
  end

  scenario 'and creates another as group owner', js: true do
    summary1 = "#{Faker::Esport.event} #{Time.current.to_i}"
    summary2 = "#{Faker::Esport.event} #{Time.current.to_i}"
    sign_in @group_owner
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link 'Add recurring event'
    fill_in 'recurring_event_summary', with: summary1
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    page.check('recurring_event_create_another')
    click_button 'Submit'
    expect(page).to have_content('Recurring event was successfully created.')
    expect(page).to have_content(summary1)
    expect(page).to_not have_content("(all day)")
    expect(page).to have_content('New Recurring Event')
    fill_in 'recurring_event_summary', with: summary2
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    click_button 'Submit'
    expect(page).to have_content('Recurring event was successfully created.')
    expect(page).to have_content(summary2)
    expect(page).to_not have_content("(all day)")
    expect(page).to_not have_content('New Event')
  end

  scenario 'with invalid data as admin', js: true do
    sign_in @admin
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link 'Add recurring event'
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    click_button 'Submit'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    expect(page).to have_content('1 error prohibited this recurring event from being saved:')
    expect(page).to have_content("Summary can't be blank")
  end

  scenario 'with invalid data as owner', js: true do
    sign_in @owner
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link 'Add recurring event'
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    click_button 'Submit'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    expect(page).to have_content('1 error prohibited this recurring event from being saved:')
    expect(page).to have_content("Summary can't be blank")
  end

  scenario 'with invalid data as collaborator', js: true do
    sign_in @collaborator
    visit calendar_path(@calendar_private.id, year: @date.year, month: @date.month)
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    click_link 'Add recurring event'
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    click_button 'Submit'
    expect(page).to have_content(@date.strftime("%B") + " " + @date.strftime("%Y"))
    expect(page).to have_content('1 error prohibited this recurring event from being saved:')
    expect(page).to have_content("Summary can't be blank")
  end

  scenario 'with invalid data as group member', js: true do
    sign_in @group_member
    visit calendar_by_day_path(@calendar_private.id, year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    click_link 'Add recurring event'
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    click_button 'Submit'
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    expect(page).to have_content('1 error prohibited this recurring event from being saved:')
    expect(page).to have_content("Summary can't be blank")
  end

  scenario 'with invalid data as group owner', js: true do
    sign_in @group_owner
    visit calendar_by_day_path(@calendar_private.id, year: @date.year, month: @date.month, day: @date.day)
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    click_link 'Add recurring event'
    fill_in 'recurring_event_location', with: location
    fill_in_trix_editor 'recurring_event_description_trix_input_recurring_event', with: description
    fill_in 'recurring_event_start', with: @date.strftime('%b %d, %Y %I:%M%p')
    select('MONTHLY', from: 'frequency')
    fill_in 'recurring_event_end', with: (@date + 2.hours).strftime('%b %d, %Y %I:%M%p')
    click_button 'Submit'
    expect(page).to have_content(@date.strftime("%A") + ", " + @date.strftime("%B") + " " + @date.strftime("%d") + ", " + @date.strftime("%Y"))
    expect(page).to have_content('1 error prohibited this recurring event from being saved:')
    expect(page).to have_content("Summary can't be blank")
  end
end
