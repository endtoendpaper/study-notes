require 'rails_helper'

describe "User views tags on item", type: :system do
  before(:each) do
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)

    UserItems.item_list.each do |item|
      instance_variable_set("@#{item.singularize}", FactoryBot.create(item.singularize, :public, user_id: @owner.id))
    end
  end

  scenario 'as user' do
    sign_in @user
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      expect(page).to_not have_link('Add Tags')
      tag = FactoryBot.create(:tag)
      instance_variable_get("@#{item.singularize}").tags << tag
      visit current_path
      expect(page).to_not have_link('Edit tags')
      expect(page).to have_link(tag.name)
    end
  end

  scenario 'not logged in' do
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      expect(page).to_not have_link('Add Tags')
      tag = FactoryBot.create(:tag)
      instance_variable_get("@#{item.singularize}").tags << tag
      visit current_path
      expect(page).to_not have_link('Edit tags')
      expect(page).to have_link(tag.name)
    end
  end
end
