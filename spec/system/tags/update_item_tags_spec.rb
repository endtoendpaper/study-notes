require 'rails_helper'

describe "Updates item's tags'", type: :system do
  let(:name1) { Faker::Lorem.characters(number: 25) }
  let(:name2) { 'another-worD+++121' }
  let(:name3) { Faker::Lorem.characters(number: 45) }
  let(:name4) { '___--+++9999' }

  before(:each) do
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @group_member = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group)
    @group.add_member(@group_member, @group.user)

    UserItems.item_list.each do |item|
      item = instance_variable_set("@#{item.singularize}", FactoryBot.create(item.singularize, :public, user_id: @owner.id))
      item.collaborate(@collaborator, item.user)
      item.add_group(@group, item.user)
    end
  end

  scenario 'as admin with valid data', js: true do
    sign_in @admin
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      click_link 'Add Tags'
      expect(page).to have_selector('h5', text: "#{item.singularize.capitalize} Tags")
      fill_in 'tag_tag_name', with: name1, wait: 10
      page.find(:xpath, '/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form/div[1]/input[2]').click
      expect(page).to have_button(name1)
      fill_in 'tag_tag_name', with: name2, wait: 10
      page.find(:xpath, '/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form/div[1]/input[2]').click
      expect(page).to have_button(name1)
      expect(page).to have_button(name2)
      fill_in 'tag_tag_name', with: name3, wait: 10
      page.find(:xpath, '/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form/div[1]/input[2]').click
      expect(page).to have_button(name1)
      expect(page).to have_button(name2)
      expect(page).to have_button(name3)
      fill_in 'tag_tag_name', with: name4, wait: 10
      page.find(:xpath, '/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form/div[1]/input[2]').click
      expect(page).to have_button(name1)
      expect(page).to have_button(name2)
      expect(page).to have_button(name3)
      expect(page).to have_button(name4)
      click_button name3, wait: 10
      expect(page).to have_button(name1)
      expect(page).to have_button(name2)
      expect(page).to_not have_button(name3)
      expect(page).to have_button(name4)
      page.find(:xpath, '/html/body/main/div/div/turbo-frame[1]/div/div/div/div[1]/button').click
      expect(page).to_not have_selector('h5', text: "#{item.singularize.capitalize} Tags")
      expect(page).to have_link(name1)
      expect(page).to have_link(name2)
      expect(page).to_not have_link(name3)
      expect(page).to have_link(name4)
      expect(page).to have_link('Edit tags')
    end
  end

  scenario 'as admin with invalid data', js: true do
    sign_in @admin
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      click_link 'Add Tags'
      expect(page).to have_selector('h5', text: "#{item.singularize.capitalize} Tags")
      fill_in 'tag_tag_name', with: "spac es", wait: 10
      page.find(:xpath, '/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form/div[1]/input[2]').click
      expect(page).to have_selector('h4', text: '1 error prohibited this tag from being saved:')
      expect(page).to have_selector('li', text: 'can only contain letters, numbers, dashes, underscores, or pluses')
      fill_in 'tag_tag_name', with: "spaces", wait: 10
      click_button 'Add Tag', wait: 10
      expect(page).to_not have_selector('h4', text: '1 error prohibited this tag from being saved:')
      expect(page).to_not have_selector('li', text: 'can only contain letters, numbers, dashes, underscores, or pluses')
      expect(page).to have_button('spaces')
    end
  end

  scenario 'as item owner with valid data', js: true do
    sign_in @owner
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      click_link 'Add Tags'
      expect(page).to have_selector('h5', text: "#{item.singularize.capitalize} Tags")
      fill_in 'tag_tag_name', with: name1, wait: 10
      page.find(:xpath, '/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form/div[1]/input[2]').click
      expect(page).to have_button(name1)
      fill_in 'tag_tag_name', with: name2, wait: 10
      page.find(:xpath, '/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form/div[1]/input[2]').click
      expect(page).to have_button(name1)
      expect(page).to have_button(name2)
      fill_in 'tag_tag_name', with: name3, wait: 10
      page.find(:xpath, '/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form/div[1]/input[2]').click
      expect(page).to have_button(name1)
      expect(page).to have_button(name2)
      expect(page).to have_button(name3)
      fill_in 'tag_tag_name', with: name4, wait: 10
      page.find(:xpath, '/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form/div[1]/input[2]').click
      expect(page).to have_button(name1)
      expect(page).to have_button(name2)
      expect(page).to have_button(name3)
      expect(page).to have_button(name4)
      click_button name3, wait: 10
      expect(page).to have_button(name1)
      expect(page).to have_button(name2)
      expect(page).to_not have_button(name3)
      expect(page).to have_button(name4)
      page.find(:xpath, '/html/body/main/div/div/turbo-frame[1]/div/div/div/div[1]/button').click
      expect(page).to_not have_selector('h5', text: "#{item.singularize.capitalize} Tags")
      expect(page).to have_link(name1)
      expect(page).to have_link(name2)
      expect(page).to_not have_link(name3)
      expect(page).to have_link(name4)
      expect(page).to have_link('Edit tags')
    end
  end

  scenario 'as item owner with invalid data', js: true do
    sign_in @owner
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      click_link 'Add Tags'
      expect(page).to have_selector('h5', text: "#{item.singularize.capitalize} Tags")
      fill_in 'tag_tag_name', with: "spac es", wait: 10
      page.find(:xpath, '/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form/div[1]/input[2]').click
      expect(page).to have_selector('h4', text: '1 error prohibited this tag from being saved:')
      expect(page).to have_selector('li', text: 'can only contain letters, numbers, dashes, underscores, or pluses')
      fill_in 'tag_tag_name', with: "spaces", wait: 10
      click_button 'Add Tag', wait: 10
      expect(page).to_not have_selector('h4', text: '1 error prohibited this tag from being saved:')
      expect(page).to_not have_selector('li', text: 'can only contain letters, numbers, dashes, underscores, or pluses')
      expect(page).to have_button('spaces')
    end
  end

  scenario 'as collaborator with valid data', js: true do
    sign_in @collaborator
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      click_link 'Add Tags'
      expect(page).to have_selector('h5', text: "#{item.singularize.capitalize} Tags")
      fill_in 'tag_tag_name', with: name1, wait: 10
      page.find(:xpath, '/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form/div[1]/input[2]').click
      expect(page).to have_button(name1)
      fill_in 'tag_tag_name', with: name2, wait: 10
      page.find(:xpath, '/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form/div[1]/input[2]').click
      expect(page).to have_button(name1)
      expect(page).to have_button(name2)
      fill_in 'tag_tag_name', with: name3, wait: 10
      page.find(:xpath, '/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form/div[1]/input[2]').click
      expect(page).to have_button(name1)
      expect(page).to have_button(name2)
      expect(page).to have_button(name3)
      fill_in 'tag_tag_name', with: name4, wait: 10
      page.find(:xpath, '/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form/div[1]/input[2]').click
      expect(page).to have_button(name1)
      expect(page).to have_button(name2)
      expect(page).to have_button(name3)
      expect(page).to have_button(name4)
      click_button name3, wait: 10
      expect(page).to have_button(name1)
      expect(page).to have_button(name2)
      expect(page).to_not have_button(name3)
      expect(page).to have_button(name4)
      page.find(:xpath, '/html/body/main/div/div/turbo-frame[1]/div/div/div/div[1]/button').click
      expect(page).to_not have_selector('h5', text: "#{item.singularize.capitalize} Tags")
      expect(page).to have_link(name1)
      expect(page).to have_link(name2)
      expect(page).to_not have_link(name3)
      expect(page).to have_link(name4)
      expect(page).to have_link('Edit tags')
    end
  end

  scenario 'as group member with valid data', js: true do
    sign_in @group_member
    UserItems.item_list.each do |item|
      visit send("#{item.singularize}_path", instance_variable_get("@#{item.singularize}"))
      click_link 'Add Tags'
      expect(page).to have_selector('h5', text: "#{item.singularize.capitalize} Tags")
      fill_in 'tag_tag_name', with: name1, wait: 10
      page.find(:xpath, '/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form/div[1]/input[2]').click
      expect(page).to have_button(name1)
      fill_in 'tag_tag_name', with: name2, wait: 10
      page.find(:xpath, '/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form/div[1]/input[2]').click
      expect(page).to have_button(name1)
      expect(page).to have_button(name2)
      fill_in 'tag_tag_name', with: name3, wait: 10
      page.find(:xpath, '/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form/div[1]/input[2]').click
      expect(page).to have_button(name1)
      expect(page).to have_button(name2)
      expect(page).to have_button(name3)
      fill_in 'tag_tag_name', with: name4, wait: 10
      page.find(:xpath, '/html/body/main/div/div/turbo-frame[1]/div/div/div/div[2]/form/div[1]/input[2]').click
      expect(page).to have_button(name1)
      expect(page).to have_button(name2)
      expect(page).to have_button(name3)
      expect(page).to have_button(name4)
      click_button name3, wait: 10
      expect(page).to have_button(name1)
      expect(page).to have_button(name2)
      expect(page).to_not have_button(name3)
      expect(page).to have_button(name4)
      page.find(:xpath, '/html/body/main/div/div/turbo-frame[1]/div/div/div/div[1]/button').click
      expect(page).to_not have_selector('h5', text: "#{item.singularize.capitalize} Tags")
      expect(page).to have_link(name1)
      expect(page).to have_link(name2)
      expect(page).to_not have_link(name3)
      expect(page).to have_link(name4)
      expect(page).to have_link('Edit tags')
    end
  end
end
