require 'rails_helper'

describe 'View help page', type: :system do

  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @user = FactoryBot.create(:user, :confirmed)
    @help_section1 = FactoryBot.create(:help_section, content: "help_section1")
    @help_section2 = FactoryBot.create(:help_section, content: "help_section2")
  end

  scenario 'as admin' do
    sign_in @admin
    visit root_path
    click_link 'Help'
    expect(page).to have_selector('h1', text: 'Help')
    expect(page).to have_link('Get Support')
    expect(page).to have_selector('h2', text: @help_section1.title)
    expect(page).to have_selector('li', text: @help_section1.title)
    expect(page).to have_content('help_section1')
    expect(page).to have_selector('h2', text: @help_section2.title)
    expect(page).to have_selector('li', text: @help_section2.title)
    expect(page).to have_content('help_section2')
    expect(page).to have_link('Add help section', minimum: 2)
    expect(page).to have_link('Edit', minimum: 2)
    expect(page).to have_content('Updated', minimum: 2)
  end

  scenario 'as user when users can contact admins' do
    sign_in @user
    visit root_path
    click_link 'Help'
    expect(page).to have_selector('h1', text: 'Help')
    expect(page).to have_link('Get Support')
    expect(page).to have_selector('h2', text: @help_section1.title)
    expect(page).to have_selector('li', text: @help_section1.title)
    expect(page).to have_content('help_section1')
    expect(page).to have_selector('h2', text: @help_section2.title)
    expect(page).to have_selector('li', text: @help_section2.title)
    expect(page).to have_content('help_section2')
    expect(page).to_not have_link('Add help section')
    expect(page).to_not have_link('Edit')
    expect(page).to_not have_content('Updated', minimum: 2)
  end

  scenario 'as user when users cannot contact admins' do
    SiteSetting.first.update(contact_form: false)
    sign_in @user
    visit help_path
    expect(page).to have_selector('h1', text: 'Help')
    expect(page).to_not have_link('Get Support')
    expect(page).to have_selector('h2', text: @help_section1.title)
    expect(page).to have_selector('li', text: @help_section1.title)
    expect(page).to have_content('help_section1')
    expect(page).to have_selector('h2', text: @help_section2.title)
    expect(page).to have_selector('li', text: @help_section2.title)
    expect(page).to have_content('help_section2')
    expect(page).to_not have_link('Add help section')
    expect(page).to_not have_link('Edit')
    expect(page).to_not have_content('Updated', minimum: 2)
  end

  scenario 'not logged in' do
    visit root_path
    expect(page).to_not have_link('Help')
  end
end
