require 'rails_helper'

describe 'Views home', type: :system do
  before(:each) do
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @collaborator = FactoryBot.create(:user, :confirmed, :admin)
    @group_owner = FactoryBot.create(:user, :confirmed)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group, user_id: @group_owner.id)
    @group.add_member(@group_member, @group.user)
  end

  scenario 'on home page there is pagination' do
    reset_elasticsearch_indices

    13.times do
      FactoryBot.create(:note, :public, user_id: @owner.id)
    end

    reindex_elasticsearch_data

    sign_in @admin
    visit root_path
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end

  scenario "returns all items as admin" do
    sign_in @admin

    UserItems.item_list.each do |item|
      reset_elasticsearch_indices

      private = FactoryBot.create(item.singularize, :private, user_id: @owner.id)
      internal = FactoryBot.create(item.singularize, :internal, user_id: @owner.id)
      public = FactoryBot.create(item.singularize, :public, user_id: @owner.id)

      reindex_elasticsearch_data

      visit root_path
      expect(page).to have_content(private.title)
      expect(page).to have_content(internal.title)
      expect(page).to have_content(public.title)
      expect(page).to_not have_link('Edit collaborators')
      expect(page).to_not have_link('Edit tags')
      expect(page).to_not have_link('Add tags')
      expect(page).to_not have_link('Add card')
      expect(page).to_not have_link("Edit #{item.singularize}")
      expect(page).to have_link("Show #{item.singularize}")
      expect(page).to have_content('Public')
      expect(page).to have_content('Internal')
      expect(page).to have_content('Private')
      private.destroy
      internal.destroy
      public.destroy
    end
  end

  scenario "returns all for owner" do
    sign_in @owner
    UserItems.item_list.each do |item|
      reset_elasticsearch_indices

      private = FactoryBot.create(item.singularize, :private, user_id: @owner.id)
      internal = FactoryBot.create(item.singularize, :internal, user_id: @owner.id)
      public = FactoryBot.create(item.singularize, :public, user_id: @owner.id)

      reindex_elasticsearch_data

      visit root_path
      expect(page).to have_content(private.title)
      expect(page).to have_content(internal.title)
      expect(page).to have_content(public.title)
      expect(page).to_not have_link('Edit collaborators')
      expect(page).to_not have_link('Edit tags')
      expect(page).to_not have_link('Add tags')
      expect(page).to_not have_link('Add card')
      expect(page).to_not have_link("Edit #{item.singularize}")
      expect(page).to have_link("Show #{item.singularize}")
      expect(page).to have_content('Public')
      expect(page).to have_content('Internal')
      expect(page).to have_content('Private')
      private.destroy
      internal.destroy
      public.destroy
    end
  end

  scenario "returns all for collaborator" do
    sign_in @collaborator
    UserItems.item_list.each do |item|
      reset_elasticsearch_indices

      private = FactoryBot.create(item.singularize, :private, user_id: @owner.id)
      internal = FactoryBot.create(item.singularize, :internal, user_id: @owner.id)
      public = FactoryBot.create(item.singularize, :public, user_id: @owner.id)
      private.collaborate(@collaborator, private.user)
      internal.collaborate(@collaborator, internal.user)
      public.collaborate(@collaborator, public.user)

      reindex_elasticsearch_data

      visit root_path
      expect(page).to have_content(private.title)
      expect(page).to have_content(internal.title)
      expect(page).to have_content(public.title)
      expect(page).to_not have_link('Edit collaborators')
      expect(page).to_not have_link('Edit tags')
      expect(page).to_not have_link('Add tags')
      expect(page).to_not have_link('Add card')
      expect(page).to_not have_link("Edit #{item.singularize}")
      expect(page).to have_link("Show #{item.singularize}")
      expect(page).to have_content('Public')
      expect(page).to have_content('Internal')
      expect(page).to have_content('Private')
      expect(page).to_not have_content(@collaborator.handle)
      private.destroy
      internal.destroy
      public.destroy
    end
  end

  scenario "returns all for group owner" do
    sign_in @group_owner
    UserItems.item_list.each do |item|
      reset_elasticsearch_indices

      private = FactoryBot.create(item.singularize, :private, user_id: @owner.id)
      internal = FactoryBot.create(item.singularize, :internal, user_id: @owner.id)
      public = FactoryBot.create(item.singularize, :public, user_id: @owner.id)
      private.add_group(@group, private.user)
      internal.add_group(@group, internal.user)
      public.add_group(@group, public.user)

      reindex_elasticsearch_data

      visit root_path
      expect(page).to have_content(private.title)
      expect(page).to have_content(internal.title)
      expect(page).to have_content(public.title)
      expect(page).to_not have_link('Edit collaborators')
      expect(page).to_not have_link('Edit tags')
      expect(page).to_not have_link('Add tags')
      expect(page).to_not have_link('Add card')
      expect(page).to_not have_link("Edit #{item.singularize}")
      expect(page).to have_link("Show #{item.singularize}")
      expect(page).to have_content('Public')
      expect(page).to have_content('Internal')
      expect(page).to have_content('Private')
      expect(page).to_not have_content(@group.name)
      private.destroy
      internal.destroy
      public.destroy
    end
  end

  scenario "returns all for group member" do
    sign_in @group_member
    UserItems.item_list.each do |item|
      reset_elasticsearch_indices

      private = FactoryBot.create(item.singularize, :private, user_id: @owner.id)
      internal = FactoryBot.create(item.singularize, :internal, user_id: @owner.id)
      public = FactoryBot.create(item.singularize, :public, user_id: @owner.id)
      private.add_group(@group, private.user)
      internal.add_group(@group, internal.user)
      public.add_group(@group, public.user)

      reindex_elasticsearch_data

      visit root_path
      expect(page).to have_content(private.title)
      expect(page).to have_content(internal.title)
      expect(page).to have_content(public.title)
      expect(page).to_not have_link('Edit collaborators')
      expect(page).to_not have_link('Edit tags')
      expect(page).to_not have_link('Add tags')
      expect(page).to_not have_link('Add card')
      expect(page).to_not have_link("Edit #{item.singularize}")
      expect(page).to have_link("Show #{item.singularize}")
      expect(page).to have_content('Public')
      expect(page).to have_content('Internal')
      expect(page).to have_content('Private')
      expect(page).to_not have_content(@group.name)
      private.destroy
      internal.destroy
      public.destroy
    end
  end

  scenario "returns public and private for user" do
    sign_in @user
    UserItems.item_list.each do |item|
      reset_elasticsearch_indices

      private = FactoryBot.create(item.singularize, :private, user_id: @owner.id)
      internal = FactoryBot.create(item.singularize, :internal, user_id: @owner.id)
      public = FactoryBot.create(item.singularize, :public, user_id: @owner.id)

      reindex_elasticsearch_data

      visit root_path
      expect(page).to_not have_content(private.title)
      expect(page).to have_content(internal.title)
      expect(page).to have_content(public.title)
      expect(page).to_not have_link('Edit collaborators')
      expect(page).to_not have_link('Edit tags')
      expect(page).to_not have_link('Add tags')
      expect(page).to_not have_link('Add card')
      expect(page).to_not have_link("Edit #{item.singularize}")
      expect(page).to have_link("Show #{item.singularize}")
      expect(page).to_not have_content('Public')
      expect(page).to_not have_content('Internal')
      expect(page).to_not have_content('Private')
      private.destroy
      internal.destroy
      public.destroy
    end
  end

  scenario "returns only public items if not logged in" do
    UserItems.item_list.each do |item|
      reset_elasticsearch_indices

      private = FactoryBot.create(item.singularize, :private, user_id: @owner.id)
      internal = FactoryBot.create(item.singularize, :internal, user_id: @owner.id)
      public = FactoryBot.create(item.singularize, :public, user_id: @owner.id)

      reindex_elasticsearch_data

      visit root_path
      expect(page).to_not have_content(private.title)
      expect(page).to_not have_content(internal.title)
      expect(page).to have_content(public.title)
      expect(page).to_not have_link('Edit collaborators')
      expect(page).to_not have_link('Edit tags')
      expect(page).to_not have_link('Add tags')
      expect(page).to_not have_link('Add card')
      expect(page).to_not have_link("Edit #{item.singularize}")
      expect(page).to have_link("Show #{item.singularize}")
      expect(page).to_not have_content('Public')
      expect(page).to_not have_content('Internal')
      expect(page).to_not have_content('Private')
      private.destroy
      internal.destroy
      public.destroy
    end
  end
end
