require 'rails_helper'

describe 'Admin searches users', type: :system do
  before(:each) do
    @user = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    User.import
    User.__elasticsearch__.refresh_index!
  end

  scenario 'with email that exists', js: true do
    sign_in @admin
    visit user_management_path
    fill_in('search', with: @user.email)
    click_button 'Search'
    expect(page).to have_content(@user.email)
    expect(page).to have_content(@user.username)
    expect(page).to have_content('Edit user')
  end

  scenario 'with username that exists', js: true do
    sign_in @admin
    visit user_management_path
    fill_in('search', with: @user.username)
    click_button 'Search'
    expect(page).to have_content(@user.email)
    expect(page).to have_content(@user.username)
    expect(page).to have_content('Edit user')
  end

  scenario 'with email that does not exist', js: true do
    sign_in @admin
    visit user_management_path
    expect(page).to have_content(@user.email)
    expect(page).to have_content(@user.username)
    expect(page).to have_content('Edit user')
    fill_in('search', with: 'notausername')
    click_button 'Search'
    expect(page).to_not have_content(@user.email)
    expect(page).to_not have_content(@user.username)
    expect(page).to_not have_content('Edit user')
    click_link 'Clear'
    expect(page).to have_content(@user.email)
    expect(page).to have_content(@user.username)
    expect(page).to have_content('Edit user')
  end

  scenario 'with username that does not exist and then clears search', js: true do
    sign_in @admin
    visit user_management_path
    expect(page).to have_content(@user.email)
    expect(page).to have_content(@user.username)
    expect(page).to have_content('Edit user')
    fill_in('search', with: 'dont@exist')
    click_button 'Search'
    expect(page).to_not have_content(@user.email)
    expect(page).to_not have_content(@user.username)
    expect(page).to_not have_content('Edit user')
    click_link 'Clear'
    expect(page).to have_content(@user.email)
    expect(page).to have_content(@user.username)
    expect(page).to have_content('Edit user')
  end

  scenario 'and toggles admin only users', js: true do
    sign_in @admin
    visit user_management_path
    expect(page).to have_content(@user.email)
    expect(page).to have_content(@user.username)
    expect(page).to have_content(@admin.email)
    expect(page).to have_content(@admin.username)
    page.check('view_admins')
    expect(page).to_not have_content(@user.email)
    expect(page).to_not have_content(@user.username)
    expect(page).to have_content(@admin.email)
    expect(page).to have_content(@admin.username)
    page.uncheck('view_admins')
    expect(page).to have_content(@user.email)
    expect(page).to have_content(@user.username)
    expect(page).to have_content(@admin.email)
    expect(page).to have_content(@admin.username)
  end
end
