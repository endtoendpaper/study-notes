require 'rails_helper'

describe 'Views user management', type: :system do
  before(:each) do
    @user1 = FactoryBot.create(:user, :confirmed, :avatar, :hide_relationships)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
  end

  scenario 'as admin' do
    sign_in @admin
    visit user_management_path
    expect(html).to include('crying-cat.jpeg')
    expect(html).to include('<img src=')
    expect(page).to have_selector('h5', text: @admin.handle)
    expect(page).to have_selector('h5', text: @user1.handle)
    expect(page).to have_link('0 following', minimum: 2)
    expect(page).to have_link('0 followers', minimum: 2)
    expect(page).to_not have_button('Follow')
    expect(page).to have_link('Edit your profile', minimum: 1)
    expect(page).to have_link('Edit user', minimum: 1)
    expect(page).to have_content('Email: ' + @admin.email)
    expect(page).to have_content('Email: ' + @user1.email)
    SiteSetting.first.update(follow_users: false)
    visit user_management_path
    expect(page).to_not have_link('0 following')
    expect(page).to_not have_link('0 followers')
    visit users_path
  end

  scenario 'sees pagination with >2 users' do
    sign_in @admin
    FactoryBot.create(:user, :confirmed)
    visit user_management_path
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end
end
