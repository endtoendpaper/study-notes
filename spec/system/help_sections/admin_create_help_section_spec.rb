require 'rails_helper'

describe 'Admin creates help section', type: :system do
  let(:title) { Faker::Lorem.sentence(word_count: 3) }
  let(:content) { 'My new help content!' }

  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
  end

  scenario 'with valid data', js: true do
    sign_in @admin
    visit root_path
    click_link 'Help'
    click_link 'Add help section'
    fill_in 'help_section_title', with: title
    fill_in_trix_editor 'help_section_content_trix_input_help_section', with: content
    click_button 'Submit'
    expect(page).to have_selector('h2', text: title)
    expect(page).to have_selector('li', text: title)
    expect(page).to have_content(content)
    expect(page).to have_content('Help section was successfully created.')
  end

  scenario 'with invalid data', js: true do
    sign_in @admin
    visit root_path
    click_link 'Help'
    click_link 'Add help section'
    fill_in_trix_editor 'help_section_content_trix_input_help_section', with: "exiting new content"
    click_button 'Submit'
    expect(page).to_not have_content('exciting new content')
    expect(page).to have_content('1 error prohibited this help section from being saved:')
    expect(page).to have_content("Title can't be blank")
  end
end
