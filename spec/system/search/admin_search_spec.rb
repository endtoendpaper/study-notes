require 'rails_helper'

describe 'User search', type: :system do
  before(:each) do
    @admin = FactoryBot.create(:user, :admin, :confirmed)

    # Delete and recreate indices for all relevant models
    [Note, Deck, Card, Calendar, Event, RecurringEvent, Tag].each do |model|
      begin
        model.__elasticsearch__.delete_index!
      rescue Elasticsearch::Transport::Transport::Errors::NotFound
        # Ignore if the index does not exist
      end
      model.__elasticsearch__.create_index!
    end

    @public_note = FactoryBot.create(:note, :public, title: "note puppy 1")
    @internal_note = FactoryBot.create(:note, :internal, title: "note puppy 2")
    @private_note = FactoryBot.create(:note, :private, title: "note puppy 3")
    @public_deck = FactoryBot.create(:deck, :public, title: "deck puppy 4")
    @internal_deck = FactoryBot.create(:deck, :internal, title: "deck puppy 5")
    @private_deck = FactoryBot.create(:deck, :private, title: "deck puppy 6")
    @public_card = FactoryBot.create(:card, :public, front: "card puppy 7")
    @internal_card = FactoryBot.create(:card, :internal, front: "card puppy 8")
    @private_card = FactoryBot.create(:card, :private, front: "card puppy 9")
    @public_calendar = FactoryBot.create(:calendar, :public, title: "calendar puppy 10")
    @internal_calendar = FactoryBot.create(:calendar, :internal, title: "calendar puppy 11")
    @private_calendar = FactoryBot.create(:calendar, :private, title: "calendar puppy 12")
    @public_event = FactoryBot.create(:event, :public, summary: "event puppy 13")
    @internal_event = FactoryBot.create(:event, :internal, summary: "event puppy 14")
    @private_event = FactoryBot.create(:event, :private, summary: "event puppy 15")
    @public_recurring_event = FactoryBot.create(:recurring_event, :public, summary: "recurring event puppy 16")
    @internal_recurring_event = FactoryBot.create(:recurring_event, :internal, summary: "recurring event puppy 17")
    @private_recurring_event = FactoryBot.create(:recurring_event, :private, summary: "recurring event puppy 18")
    @tag = FactoryBot.create(:tag, name: "puppy1")

    # Reindex & refresh all records
    [Note, Deck, Card, Calendar, Event, RecurringEvent, Tag].each do |model|
      model.import
      model.__elasticsearch__.refresh_index!
    end
  end

  scenario 'for puppy calendars and events', js: true do
    sign_in @admin

    visit search_path(query: 'puppy', item_types: ['calendars','recurring_events','events'])

    expect(page).to have_content(@public_calendar.title)
    expect(page).to have_content(@public_event.summary)
    expect(page).to have_content(@public_recurring_event.summary)
    expect(page).to have_content(@private_calendar.title)
    expect(page).to have_content(@private_event.summary)
    expect(page).to have_content(@private_recurring_event.summary)
    expect(page).to have_content(@internal_calendar.title)
    expect(page).to have_content(@internal_event.summary)
    expect(page).to have_content(@internal_recurring_event.summary)

    expect(page).to_not have_content(@private_note.title)
    expect(page).to_not have_content(@private_deck.title)
    expect(page).to_not have_content(@private_card.front.to_plain_text)
    expect(page).to_not have_content(@public_note.title)
    expect(page).to_not have_content(@public_deck.title)
    expect(page).to_not have_content(@public_card.front.to_plain_text)
    expect(page).to_not have_content(@internal_note.title)
    expect(page).to_not have_content(@internal_deck.title)
    expect(page).to_not have_content(@internal_card.front.to_plain_text)

    expect(page).to have_content("LIMIT RESULT TYPE")
    expect(page).to have_content("TAGS")
    expect(page).to have_content("FILTERS")
    expect(page).to have_content("Owned Items")
    expect(page).to have_content("My Items")
    expect(page).to have_content("My Stars")
  end
end
