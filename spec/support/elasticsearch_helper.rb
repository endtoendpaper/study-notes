
def reset_elasticsearch_indices(models = nil)
  model_list = models || UserItems.site_searchable + ['users', 'groups', 'tags']

  model_list.each do |model|
    begin
      UserItems.class(model).__elasticsearch__.delete_index!
    rescue StandardError
      # Ignore if the index does not exist
    end
    UserItems.class(model).__elasticsearch__.create_index!
  end
end

def reindex_elasticsearch_data(models = nil)
  model_list = models || UserItems.site_searchable + ['users', 'groups', 'tags']

  model_list.each do |model|
    UserItems.class(model).import
    UserItems.class(model).__elasticsearch__.refresh_index!
  end
end

