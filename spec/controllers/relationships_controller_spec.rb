require 'rails_helper'

RSpec.describe RelationshipsController, type: :controller do
  before(:each) do
    @user1 = FactoryBot.create(:user, :confirmed)
    @user2 = FactoryBot.create(:user, :confirmed)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
  end

  shared_context 'when admin logged in' do
    before(:each) do
      sign_in @admin
    end
  end

  shared_context 'when user logged in' do
    before(:each) do
      sign_in @user1
    end
  end

  describe 'POST create' do
    context 'for admin' do
      include_context 'when admin logged in'

      it 'should be successful when follow is enabled in site settings' do
        SiteSetting.first.update(follow_users: true)
        expect do
          post :create, params: { followed_id: @user1.id }
        end.to change(@admin.following, :count).by(1).and change(@user1.followers, :count).by(1)
      end

      it 'should not be successful when follow is disabled in site settings' do
          SiteSetting.first.update(follow_users: false)
          expect do
            post :create, params: { followed_id: @user2.id }
          end.to change(@admin.following, :count).by(0).and change(@user2.followers, :count).by(0)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should be successful when follow is enabled in site settings' do
        SiteSetting.first.update(follow_users: true)
        expect do
          post :create, params: { followed_id: @user2.id }
        end.to change(@user1.following, :count).by(1).and change(@user2.followers, :count).by(1)
      end

      it 'should not be successful when follow is disabled in site settings' do
          SiteSetting.first.update(follow_users: false)
          expect do
            post :create, params: { followed_id: @admin.id }
          end.to change(@user1.following, :count).by(0).and change(@admin.followers, :count).by(0)
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
        SiteSetting.first.update(follow_users: true)
        expect do
          post :create, params: { followed_id: @user2.id }
        end.to change(@user2.followers, :count).by(0)
      end
    end
  end

  describe 'DELETE destroy' do
    context 'for admin' do
      include_context 'when admin logged in'

      it 'should be successful when follow is enabled in site settings' do
        SiteSetting.first.update(follow_users: true)
        @admin.follow(@user1)
        expect do
          delete :destroy, params: { id: @admin.active_relationships.find_by(followed: @user1).id }
        end.to change(@admin.following, :count).by(-1).and change(@user1.followers, :count).by(-1)
      end

      it 'should not be successful when follow is disabled in site settings' do
        SiteSetting.first.update(follow_users: true)
        @admin.follow(@user2)
        SiteSetting.first.update(follow_users: false)
        expect do
          delete :destroy, params: { id: @admin.active_relationships.find_by(followed: @user2).id }
        end.to change(@admin.following, :count).by(0).and change(@user1.followers, :count).by(0)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should be successful when follow is enabled in site settings' do
        SiteSetting.first.update(follow_users: true)
        @user1.follow(@user2)
        expect do
          delete :destroy, params: { id: @user1.active_relationships.find_by(followed: @user2).id }
        end.to change(@user1.following, :count).by(-1).and change(@user2.followers, :count).by(-1)
      end

      it 'should not be successful when follow is disabled in site settings' do
        SiteSetting.first.update(follow_users: true)
        @user1.follow(@admin)
        SiteSetting.first.update(follow_users: false)
        expect do
          delete :destroy, params: { id: @user1.active_relationships.find_by(followed: @admin).id }
        end.to change(@user1.following, :count).by(0).and change(@admin.followers, :count).by(0)
      end
    end

    context 'when not logged in' do

      it 'should not be successful when follow is enabled in site settings' do
        sign_in @user1
        SiteSetting.first.update(follow_users: true)
        @user1.follow(@admin)
        sign_out @user1
        expect do
          delete :destroy, params: { id: @user1.active_relationships.find_by(followed: @admin).id }
        end.to change(@user1.following, :count).by(0).and change(@admin.followers, :count).by(0)
      end

      it 'should not be successful when follow is disabled in site settings' do
        sign_in @user1
        SiteSetting.first.update(follow_users: true)
        @user1.follow(@admin)
        SiteSetting.first.update(follow_users: false)
        sign_out @user1
        expect do
          delete :destroy, params: { id: @user1.active_relationships.find_by(followed: @admin).id }
        end.to change(@user1.following, :count).by(0).and change(@admin.followers, :count).by(0)
      end
    end
  end
end
