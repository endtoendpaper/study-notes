require 'rails_helper'

RSpec.describe CollaborationsController, type: :controller do
  before(:each) do
    @user1 = FactoryBot.create(:user, :confirmed)
    @user2 = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group_owner = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group, user_id: @group_owner.id)
    @group2 = FactoryBot.create(:group)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group.add_member(@group_member, @group_owner)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @items = []
    UserItems.item_list.each do |item|
      item = FactoryBot.create(item.singularize, :public, user_id: @user1.id)
      item.add_group(@group, item.user)
      item.collaborate(@collaborator, item.user)
      @items << item
    end
  end

  shared_context 'when admin logged in' do
    before(:each) do
      sign_in @admin
    end
  end

  shared_context 'when item owner logged in' do
    before(:each) do
      sign_in @user1
    end
  end

  shared_context 'when collaborator logged in' do
    before(:each) do
      sign_in @collaborator
    end
  end

  shared_context 'when group member logged in' do
    before(:each) do
      sign_in @group_member
    end
  end

  shared_context 'when group owner logged in' do
    before(:each) do
      sign_in @group_owner
    end
  end

  shared_context 'when user logged in' do
    before(:each) do
      sign_in @user2
    end
  end

  describe 'GET edit_collaborations' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'is successful' do
        @items.each do |item|
          get :edit_collaborations, params: { record_type: item.type, record_id: item.id }
         expect(response).to have_http_status(:ok)
        end
      end
    end

    context 'for item owner' do
      include_context 'when item owner logged in'

      it 'is successful' do
        @items.each do |item|
          get :edit_collaborations, params: { record_type: item.type, record_id: item.id }
          expect(response).to have_http_status(:ok)
        end
      end
    end

    context 'for item collaborator' do
      include_context 'when collaborator logged in'

      it 'is successful' do
        @items.each do |item|
          get :edit_collaborations, params: { record_type: item.type, record_id: item.id }
          expect(response).to have_http_status(:ok)
        end
      end
    end

    context 'for item group member' do
      include_context 'when group member logged in'

      it 'is successful' do
        @items.each do |item|
          get :edit_collaborations, params: { record_type: item.type, record_id: item.id }
          expect(response).to have_http_status(:ok)
        end
      end
    end

    context 'for item group owner' do
      include_context 'when group owner logged in'

      it 'is successful' do
        @items.each do |item|
          get :edit_collaborations, params: { record_type: item.type, record_id: item.id }
          expect(response).to have_http_status(:ok)
        end
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'is not successful' do
        @items.each do |item|
          get :edit_collaborations, params: { record_type: item.type, record_id: item.id }
          expect(response).to have_http_status(:found)
        end
      end
    end

    context 'when not logged in' do

      it 'is not successful' do
        @items.each do |item|
          get :edit_collaborations, params: { record_type: item.type, record_id: item.id }
          expect(response).to have_http_status(:found)
        end
      end
    end
  end

  describe 'PATCH change_owner' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'is successful with valid username handle' do
        @items.each do |item|
          patch :change_owner, params: { record_type: item.type, record_id: item.id, username: @admin.handle }
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.owner).to eq(@admin)
          item.update(user: @user1)
        end
      end

      it 'is successful with valid username handle' do
        @items.each do |item|
          patch :change_owner, params: { record_type: item.type, record_id: item.id, username: @admin.username }
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.owner).to eq(@admin)
          item.update(user: @user1)
        end
      end

      it "does not change owner with invalid username handle" do
        @items.each do |item|
          patch :change_owner, params: { record_type: item.type, record_id: item.id, username: '@gibberish' }
          expect(response).to have_http_status(:unprocessable_entity)
          item.reload
          expect(item.owner).to eq(@user1)
        end
      end

      it "does not change owner with invalid username" do
        @items.each do |item|
          patch :change_owner, params: { record_type: item.type, record_id: item.id, username: 'gibberish' }
          expect(response).to have_http_status(:unprocessable_entity)
          item.reload
          expect(item.owner).to eq(@user1)
        end
      end
    end

    context 'for item owner' do
      include_context 'when item owner logged in'

      it 'is successful with valid username handle' do
        @items.each do |item|
          patch :change_owner, params: { record_type: item.type, record_id: item.id, username: @admin.handle }
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.owner).to eq(@admin)
          item.update(user: @user1)
        end
      end

      it 'is successful with valid username handle' do
        @items.each do |item|
          patch :change_owner, params: { record_type: item.type, record_id: item.id, username: @admin.username }
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.owner).to eq(@admin)
          item.update(user: @user1)
        end
      end

      it "does not change owner with invalid username handle" do
        @items.each do |item|
          patch :change_owner, params: { record_type: item.type, record_id: item.id, username: '@gibberish' }
          expect(response).to have_http_status(:unprocessable_entity)
          item.reload
          expect(item.owner).to eq(@user1)
        end
      end

      it "does not change owner with invalid username" do
        @items.each do |item|
          patch :change_owner, params: { record_type: item.type, record_id: item.id, username: 'gibberish' }
          expect(response).to have_http_status(:unprocessable_entity)
          item.reload
          expect(item.owner).to eq(@user1)
        end
      end
    end

    context 'for item collaborator' do
      include_context 'when collaborator logged in'

      it 'is not successful with valid username handle' do
        @items.each do |item|
          patch :change_owner, params: { record_type: item.type, record_id: item.id, username: @admin.handle }
          expect(response).to have_http_status(:found)
          item.reload
          expect(item.owner).to eq(@user1)
        end
      end

      it 'is not successful with valid username handle' do
        @items.each do |item|
          patch :change_owner, params: { record_type: item.type, record_id: item.id, username: @admin.username }
          expect(response).to have_http_status(:found)
          item.reload
          expect(item.owner).to eq(@user1)
        end
      end
    end

    context 'for item group member' do
      include_context 'when group member logged in'

      it 'is not successful with valid username handle' do
        @items.each do |item|
          patch :change_owner, params: { record_type: item.type, record_id: item.id, username: @admin.handle }
          expect(response).to have_http_status(:found)
          item.reload
          expect(item.owner).to eq(@user1)
        end
      end

      it 'is not successful with valid username handle' do
        @items.each do |item|
          patch :change_owner, params: { record_type: item.type, record_id: item.id, username: @admin.username }
          expect(response).to have_http_status(:found)
          item.reload
          expect(item.owner).to eq(@user1)
        end
      end
    end

    context 'for item group owner' do
      include_context 'when group owner logged in'

      it 'is not successful with valid username handle' do
        @items.each do |item|
          patch :change_owner, params: { record_type: item.type, record_id: item.id, username: @admin.handle }
          expect(response).to have_http_status(:found)
          item.reload
          expect(item.owner).to eq(@user1)
        end
      end

      it 'is not successful with valid username handle' do
        @items.each do |item|
          patch :change_owner, params: { record_type: item.type, record_id: item.id, username: @admin.username }
          expect(response).to have_http_status(:found)
          item.reload
          expect(item.owner).to eq(@user1)
        end
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'is not successful with valid username handle' do
        @items.each do |item|
          patch :change_owner, params: { record_type: item.type, record_id: item.id, username: @admin.handle }
          expect(response).to have_http_status(:found)
          item.reload
          expect(item.owner).to eq(@user1)
        end
      end

      it 'is not successful with valid username handle' do
        @items.each do |item|
          patch :change_owner, params: { record_type: item.type, record_id: item.id, username: @admin.username }
          expect(response).to have_http_status(:found)
          item.reload
          expect(item.owner).to eq(@user1)
        end
      end
    end

    context 'when not logged in' do

      it 'is not successful with valid username handle' do
        @items.each do |item|
          patch :change_owner, params: { record_type: item.type, record_id: item.id, username: @admin.handle }
          expect(response).to have_http_status(:found)
          item.reload
          expect(item.owner).to eq(@user1)
        end
      end

      it 'is not successful with valid username handle' do
        @items.each do |item|
          patch :change_owner, params: { record_type: item.type, record_id: item.id, username: @admin.username }
          expect(response).to have_http_status(:found)
          item.reload
          expect(item.owner).to eq(@user1)
        end
      end
    end
  end

  describe 'PATCH add_collaborator' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'is successful with valid username handle' do
        @items.each do |item|
          expect(item.collaborators_count).to eq(1)
          expect do
            patch :add_collaborator, params: { record_type: item.type, record_id: item.id, username: @admin.handle }
          end.to change(Collaboration, :count).by(1)
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.collaborators_count).to eq(2)
          item.uncollaborate(@admin, item.user)
        end
      end

      it 'is successful with valid username handle' do
        @items.each do |item|
          expect(item.collaborators_count).to eq(1)
          expect do
            patch :add_collaborator, params: { record_type: item.type, record_id: item.id, username: @admin.username }
          end.to change(Collaboration, :count).by(1)
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.collaborators_count).to eq(2)
          item.uncollaborate(@admin, item.user)
        end
      end

      it "does not add collaborator with invalid username handle" do
        @items.each do |item|
          expect(item.collaborators_count).to eq(1)
          expect do
            patch :add_collaborator, params: { record_type: item.type, record_id: item.id, username: '@gibberish' }
          end.to change(Collaboration, :count).by(0)
          expect(response).to have_http_status(:unprocessable_entity)
          item.reload
          expect(item.collaborators_count).to eq(1)
        end
      end

      it "does not change owner with invalid username" do
        @items.each do |item|
          expect(item.collaborators_count).to eq(1)
          expect do
            patch :add_collaborator, params: { record_type: item.type, record_id: item.id, username: 'gibberish' }
          end.to change(Collaboration, :count).by(0)
          expect(response).to have_http_status(:unprocessable_entity)
          item.reload
          expect(item.collaborators_count).to eq(1)
        end
      end
    end

    context 'for item owner' do
      include_context 'when item owner logged in'

      it 'is successful with valid username handle' do
        @items.each do |item|
          expect(item.collaborators_count).to eq(1)
          expect do
            patch :add_collaborator, params: { record_type: item.type, record_id: item.id, username: @admin.handle }
          end.to change(Collaboration, :count).by(1)
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.collaborators_count).to eq(2)
          item.uncollaborate(@admin, item.user)
        end
      end

      it 'is successful with valid username handle' do
        @items.each do |item|
          expect(item.collaborators_count).to eq(1)
          expect do
            patch :add_collaborator, params: { record_type: item.type, record_id: item.id, username: @admin.username }
          end.to change(Collaboration, :count).by(1)
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.collaborators_count).to eq(2)
          item.uncollaborate(@admin, item.user)
        end
      end

      it "does not change owner with invalid username handle" do
        @items.each do |item|
          expect(item.collaborators_count).to eq(1)
          expect do
            patch :add_collaborator, params: { record_type: item.type, record_id: item.id, username: '@gibberish' }
          end.to change(Collaboration, :count).by(0)
          expect(response).to have_http_status(:unprocessable_entity)
          item.reload
          expect(item.collaborators_count).to eq(1)
        end
      end

      it "does not change owner with invalid username" do
        @items.each do |item|
          expect(item.collaborators_count).to eq(1)
          expect do
            patch :add_collaborator, params: { record_type: item.type, record_id: item.id, username: 'gibberish' }
          end.to change(Collaboration, :count).by(0)
          expect(response).to have_http_status(:unprocessable_entity)
          item.reload
          expect(item.collaborators_count).to eq(1)
        end
      end
    end

    context 'for item collaborator' do
      include_context 'when collaborator logged in'

      it 'is successful with valid username handle' do
        @items.each do |item|
          expect(item.collaborators_count).to eq(1)
          expect do
            patch :add_collaborator, params: { record_type: item.type, record_id: item.id, username: @admin.handle }
          end.to change(Collaboration, :count).by(1)
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.collaborators_count).to eq(2)
          item.uncollaborate(@admin, item.user)
        end
      end

      it 'is successful with valid username handle' do
        @items.each do |item|
          expect(item.collaborators_count).to eq(1)
          expect do
            patch :add_collaborator, params: { record_type: item.type, record_id: item.id, username: @admin.username }
          end.to change(Collaboration, :count).by(1)
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.collaborators_count).to eq(2)
          item.uncollaborate(@admin, item.user)
        end
      end

      it "does add collborator with invalid username handle" do
        @items.each do |item|
          expect(item.collaborators_count).to eq(1)
          expect do
            patch :add_collaborator, params: { record_type: item.type, record_id: item.id, username: '@gibberish' }
          end.to change(Collaboration, :count).by(0)
          expect(response).to have_http_status(:unprocessable_entity)
          item.reload
          expect(item.collaborators_count).to eq(1)
        end
      end

      it "does not change owner with invalid username" do
        @items.each do |item|
          expect(item.collaborators_count).to eq(1)
          expect do
            patch :add_collaborator, params: { record_type: item.type, record_id: item.id, username: 'gibberish' }
          end.to change(Collaboration, :count).by(0)
          expect(response).to have_http_status(:unprocessable_entity)
          item.reload
          expect(item.collaborators_count).to eq(1)
        end
      end
    end

    context 'for item group member' do
      include_context 'when group member logged in'

      it 'is successful with valid username handle' do
        @items.each do |item|
          expect(item.collaborators_count).to eq(1)
          expect do
            patch :add_collaborator, params: { record_type: item.type, record_id: item.id, username: @admin.handle }
          end.to change(Collaboration, :count).by(1)
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.collaborators_count).to eq(2)
          item.uncollaborate(@admin, item.user)
        end
      end

      it 'is successful with valid username handle' do
        @items.each do |item|
          expect(item.collaborators_count).to eq(1)
          expect do
            patch :add_collaborator, params: { record_type: item.type, record_id: item.id, username: @admin.username }
          end.to change(Collaboration, :count).by(1)
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.collaborators_count).to eq(2)
          item.uncollaborate(@admin, item.user)
        end
      end

      it "does not add collborator with invalid username handle" do
        @items.each do |item|
          expect(item.collaborators_count).to eq(1)
          expect do
            patch :add_collaborator, params: { record_type: item.type, record_id: item.id, username: '@gibberish' }
          end.to change(Collaboration, :count).by(0)
          expect(response).to have_http_status(:unprocessable_entity)
          item.reload
          expect(item.collaborators_count).to eq(1)
        end
      end

      it "does not add collborator with invalid username" do
        @items.each do |item|
          expect(item.collaborators_count).to eq(1)
          expect do
            patch :add_collaborator, params: { record_type: item.type, record_id: item.id, username: 'gibberish' }
          end.to change(Collaboration, :count).by(0)
          expect(response).to have_http_status(:unprocessable_entity)
          item.reload
          expect(item.collaborators_count).to eq(1)
        end
      end
    end

    context 'for item group owner' do
      include_context 'when group owner logged in'

      it 'is successful with valid username handle' do
        @items.each do |item|
          expect(item.collaborators_count).to eq(1)
          expect do
            patch :add_collaborator, params: { record_type: item.type, record_id: item.id, username: @admin.handle }
          end.to change(Collaboration, :count).by(1)
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.collaborators_count).to eq(2)
          item.uncollaborate(@admin, item.user)
        end
      end

      it 'is successful with valid username handle' do
        @items.each do |item|
          expect(item.collaborators_count).to eq(1)
          expect do
            patch :add_collaborator, params: { record_type: item.type, record_id: item.id, username: @admin.username }
          end.to change(Collaboration, :count).by(1)
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.collaborators_count).to eq(2)
          item.uncollaborate(@admin, item.user)
        end
      end

      it "does not change owner with invalid username handle" do
        @items.each do |item|
          expect(item.collaborators_count).to eq(1)
          expect do
            patch :add_collaborator, params: { record_type: item.type, record_id: item.id, username: '@gibberish' }
          end.to change(Collaboration, :count).by(0)
          expect(response).to have_http_status(:unprocessable_entity)
          item.reload
          expect(item.collaborators_count).to eq(1)
        end
      end

      it "does not change owner with invalid username" do
        @items.each do |item|
          expect(item.collaborators_count).to eq(1)
          expect do
            patch :add_collaborator, params: { record_type: item.type, record_id: item.id, username: 'gibberish' }
          end.to change(Collaboration, :count).by(0)
          expect(response).to have_http_status(:unprocessable_entity)
          item.reload
          expect(item.collaborators_count).to eq(1)
        end
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'is unsuccessful with valid username handle' do
        @items.each do |item|
          expect(item.collaborators_count).to eq(1)
          expect do
            patch :add_collaborator, params: { record_type: item.type, record_id: item.id, username: @admin.handle }
          end.to change(Collaboration, :count).by(0)
          expect(response).to have_http_status(:found)
          item.reload
          expect(item.collaborators_count).to eq(1)
        end
      end

      it 'is unsuccessful with valid username handle' do
        @items.each do |item|
          expect(item.collaborators_count).to eq(1)
          expect do
            patch :add_collaborator, params: { record_type: item.type, record_id: item.id, username: @admin.username }
          end.to change(Collaboration, :count).by(0)
          expect(response).to have_http_status(:found)
          item.reload
          expect(item.collaborators_count).to eq(1)
        end
      end
    end

    context 'when not logged in' do

      it 'is unsuccessful with valid username handle' do
        @items.each do |item|
          expect(item.collaborators_count).to eq(1)
          expect do
            patch :add_collaborator, params: { record_type: item.type, record_id: item.id, username: @admin.handle }
          end.to change(Collaboration, :count).by(0)
          expect(response).to have_http_status(:found)
          item.reload
          expect(item.collaborators_count).to eq(1)
        end
      end

      it 'is unsuccessful with valid username handle' do
        @items.each do |item|
          expect(item.collaborators_count).to eq(1)
          expect do
            patch :add_collaborator, params: { record_type: item.type, record_id: item.id, username: @admin.username }
          end.to change(Collaboration, :count).by(0)
          expect(response).to have_http_status(:found)
          item.reload
          expect(item.collaborators_count).to eq(1)
        end
      end
    end
  end

  describe 'PATCH remove_collaborator' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'is successful with valid user' do
        @items.each do |item|
          item.collaborate(@group_owner, item.user)
          expect(item.collaborators_count).to eq(2)
          expect do
            patch :remove_collaborator, params: { record_type: item.type, record_id: item.id, user_id: @group_owner.id }
          end.to change(Collaboration, :count).by(-1)
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.collaborators_count).to eq(1)
        end
      end
    end

    context 'for item owner' do
      include_context 'when item owner logged in'

      it 'is successful with valid user' do
        @items.each do |item|
          item.collaborate(@group_owner, item.user)
          expect(item.collaborators_count).to eq(2)
          expect do
            patch :remove_collaborator, params: { record_type: item.type, record_id: item.id, user_id: @group_owner.id }
          end.to change(Collaboration, :count).by(-1)
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.collaborators_count).to eq(1)
        end
      end
    end

    context 'for item collaborator' do
      include_context 'when collaborator logged in'

      it 'is successful with valid user' do
        @items.each do |item|
          item.collaborate(@group_owner, item.user)
          expect(item.collaborators_count).to eq(2)
          expect do
            patch :remove_collaborator, params: { record_type: item.type, record_id: item.id, user_id: @group_owner.id }
          end.to change(Collaboration, :count).by(-1)
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.collaborators_count).to eq(1)
        end
      end
    end

    context 'for item group member' do
      include_context 'when group member logged in'

      it 'is successful with valid user' do
        @items.each do |item|
          item.collaborate(@group_owner, item.user)
          expect(item.collaborators_count).to eq(2)
          expect do
            patch :remove_collaborator, params: { record_type: item.type, record_id: item.id, user_id: @group_owner.id }
          end.to change(Collaboration, :count).by(-1)
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.collaborators_count).to eq(1)
        end
      end
    end

    context 'for item group owner' do
      include_context 'when group owner logged in'

      it 'is successful with valid user' do
        @items.each do |item|
          item.collaborate(@group_owner, item.user)
          expect(item.collaborators_count).to eq(2)
          expect do
            patch :remove_collaborator, params: { record_type: item.type, record_id: item.id, user_id: @group_owner.id }
          end.to change(Collaboration, :count).by(-1)
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.collaborators_count).to eq(1)
        end
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'is not successful with valid user' do
        @items.each do |item|
          item.collaborate(@group_owner, item.user)
          expect(item.collaborators_count).to eq(2)
          expect do
            patch :remove_collaborator, params: { record_type: item.type, record_id: item.id, user_id: @group_owner.id }
          end.to change(Collaboration, :count).by(0)
          expect(response).to have_http_status(:found)
          item.reload
          expect(item.collaborators_count).to eq(2)
          item.uncollaborate(@group_owner, item.user)
        end
      end
    end

    context 'when not logged in' do

      it 'is not successful with valid user' do
        @items.each do |item|
          item.collaborate(@group_owner, item.user)
          expect(item.collaborators_count).to eq(2)
          expect do
            patch :remove_collaborator, params: { record_type: item.type, record_id: item.id, user_id: @group_owner.id }
          end.to change(Collaboration, :count).by(0)
          expect(response).to have_http_status(:found)
          item.reload
          expect(item.collaborators_count).to eq(2)
          item.uncollaborate(@group_owner, item.user)
        end
      end
    end
  end

  describe 'PATCH add_group' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'is successful with valid group name' do
        @items.each do |item|
          expect(item.groups_count).to eq(1)
          expect do
            patch :add_group, params: { record_type: item.type, record_id: item.id, group_name: @group2.name }
          end.to change(GroupAssignment, :count).by(1)
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.groups_count).to eq(2)
          item.remove_group(@group2, item.user)
        end
      end

      it "does not add group with invalid name" do
        @items.each do |item|
          expect(item.groups_count).to eq(1)
          expect do
            patch :add_group, params: { record_type: item.type, record_id: item.id, group_name: 'not a group' }
          end.to change(GroupAssignment, :count).by(0)
          expect(response).to have_http_status(:unprocessable_entity)
          item.reload
          expect(item.groups_count).to eq(1)
        end
      end
    end

    context 'for item owner' do
      include_context 'when item owner logged in'

      it 'is successful with valid group name' do
        @items.each do |item|
          expect(item.groups_count).to eq(1)
          expect do
            patch :add_group, params: { record_type: item.type, record_id: item.id, group_name: @group2.name }
          end.to change(GroupAssignment, :count).by(1)
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.groups_count).to eq(2)
          item.remove_group(@group2, item.user)
        end
      end

      it "does not add group with invalid name" do
        @items.each do |item|
          expect(item.groups_count).to eq(1)
          expect do
            patch :add_group, params: { record_type: item.type, record_id: item.id, group_name: 'not a group' }
          end.to change(GroupAssignment, :count).by(0)
          expect(response).to have_http_status(:unprocessable_entity)
          item.reload
          expect(item.groups_count).to eq(1)
        end
      end
    end

    context 'for item collaborator' do
      include_context 'when collaborator logged in'

      it 'is successful with valid group name' do
        @items.each do |item|
          expect(item.groups_count).to eq(1)
          expect do
            patch :add_group, params: { record_type: item.type, record_id: item.id, group_name: @group2.name }
          end.to change(GroupAssignment, :count).by(1)
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.groups_count).to eq(2)
          item.remove_group(@group2, item.user)
        end
      end

      it "does not add group with invalid name" do
        @items.each do |item|
          expect(item.groups_count).to eq(1)
          expect do
            patch :add_group, params: { record_type: item.type, record_id: item.id, group_name: 'not a group' }
          end.to change(GroupAssignment, :count).by(0)
          expect(response).to have_http_status(:unprocessable_entity)
          item.reload
          expect(item.groups_count).to eq(1)
        end
      end
    end

    context 'for item group member' do
      include_context 'when group member logged in'

      it 'is successful with valid group name' do
        @items.each do |item|
          expect(item.groups_count).to eq(1)
          expect do
            patch :add_group, params: { record_type: item.type, record_id: item.id, group_name: @group2.name }
          end.to change(GroupAssignment, :count).by(1)
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.groups_count).to eq(2)
          item.remove_group(@group2, item.user)
        end
      end

      it "does not add group with invalid name" do
        @items.each do |item|
          expect(item.groups_count).to eq(1)
          expect do
            patch :add_group, params: { record_type: item.type, record_id: item.id, group_name: 'not a group' }
          end.to change(GroupAssignment, :count).by(0)
          expect(response).to have_http_status(:unprocessable_entity)
          item.reload
          expect(item.groups_count).to eq(1)
        end
      end
    end

    context 'for item group owner' do
      include_context 'when group owner logged in'

      it 'is successful with valid group name' do
        @items.each do |item|
          expect(item.groups_count).to eq(1)
          expect do
            patch :add_group, params: { record_type: item.type, record_id: item.id, group_name: @group2.name }
          end.to change(GroupAssignment, :count).by(1)
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.groups_count).to eq(2)
          item.remove_group(@group2, item.user)
        end
      end

      it "does not add group with invalid name" do
        @items.each do |item|
          expect(item.groups_count).to eq(1)
          expect do
            patch :add_group, params: { record_type: item.type, record_id: item.id, group_name: 'not a group' }
          end.to change(GroupAssignment, :count).by(0)
          expect(response).to have_http_status(:unprocessable_entity)
          item.reload
          expect(item.groups_count).to eq(1)
        end
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'is not successful with valid group name' do
        @items.each do |item|
          expect(item.groups_count).to eq(1)
          expect do
            patch :add_group, params: { record_type: item.type, record_id: item.id, group_name: @group2.name }
          end.to change(GroupAssignment, :count).by(0)
          expect(response).to have_http_status(:found)
          item.reload
          expect(item.groups_count).to eq(1)
        end
      end
    end

    context 'when not logged in' do

      it 'is not successful with valid group name' do
        @items.each do |item|
          expect(item.groups_count).to eq(1)
          expect do
            patch :add_group, params: { record_type: item.type, record_id: item.id, group_name: @group2.name }
          end.to change(GroupAssignment, :count).by(0)
          expect(response).to have_http_status(:found)
          item.reload
          expect(item.groups_count).to eq(1)
        end
      end
    end
  end

  describe 'PATCH remove_group' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'is successful with valid group' do
        @items.each do |item|
          item.add_group(@group2, item.user)
          expect(item.groups_count).to eq(2)
          expect do
            patch :remove_group, params: { record_type: item.type, record_id: item.id, group_id: @group2.id }
          end.to change(GroupAssignment, :count).by(-1)
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.groups_count).to eq(1)
        end
      end
    end

    context 'for item owner' do
      include_context 'when item owner logged in'

      it 'is successful with valid group' do
        @items.each do |item|
          item.add_group(@group2, item.user)
          expect(item.groups_count).to eq(2)
          expect do
            patch :remove_group, params: { record_type: item.type, record_id: item.id, group_id: @group2.id }
          end.to change(GroupAssignment, :count).by(-1)
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.groups_count).to eq(1)
        end
      end
    end

    context 'for item collaborator' do
      include_context 'when collaborator logged in'

      it 'is successful with valid group' do
        @items.each do |item|
          item.add_group(@group2, item.user)
          expect(item.groups_count).to eq(2)
          expect do
            patch :remove_group, params: { record_type: item.type, record_id: item.id, group_id: @group2.id }
          end.to change(GroupAssignment, :count).by(-1)
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.groups_count).to eq(1)
        end
      end
    end

    context 'for item group member' do
      include_context 'when group member logged in'

      it 'is successful with valid group' do
        @items.each do |item|
          item.add_group(@group2, item.user)
          expect(item.groups_count).to eq(2)
          expect do
            patch :remove_group, params: { record_type: item.type, record_id: item.id, group_id: @group2.id }
          end.to change(GroupAssignment, :count).by(-1)
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.groups_count).to eq(1)
        end
      end
    end

    context 'for item group owner' do
      include_context 'when group owner logged in'

      it 'is successful with valid group' do
        @items.each do |item|
          item.add_group(@group2, item.user)
          expect(item.groups_count).to eq(2)
          expect do
            patch :remove_group, params: { record_type: item.type, record_id: item.id, group_id: @group2.id }
          end.to change(GroupAssignment, :count).by(-1)
          expect(response).to have_http_status(:ok)
          item.reload
          expect(item.groups_count).to eq(1)
        end
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'is not successful with valid group' do
        @items.each do |item|
          item.add_group(@group2, item.user)
          expect(item.groups_count).to eq(2)
          expect do
            patch :remove_group, params: { record_type: item.type, record_id: item.id, group_id: @group2.id }
          end.to change(GroupAssignment, :count).by(0)
          expect(response).to have_http_status(:found)
          item.reload
          expect(item.groups_count).to eq(2)
          item.remove_group(@group2, item.user)
        end
      end
    end

    context 'when not logged in' do

      it 'is not successful with valid group' do
        @items.each do |item|
          item.add_group(@group2, item.user)
          expect(item.groups_count).to eq(2)
          expect do
            patch :remove_group, params: { record_type: item.type, record_id: item.id, group_id: @group2.id }
          end.to change(GroupAssignment, :count).by(0)
          expect(response).to have_http_status(:found)
          item.reload
          expect(item.groups_count).to eq(2)
          item.remove_group(@group2, item.user)
        end
      end
    end
  end

  describe 'GET collaboration_logs' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'is successful' do
        @items.each do |item|
          get :collaboration_logs, params: { type: item.route_type, record_id: item.id }
          expect(response).to have_http_status(:ok)
        end
      end
    end

    context 'for item owner' do
      include_context 'when item owner logged in'

      it 'is successful' do
        @items.each do |item|
          get :collaboration_logs, params: { type: item.route_type, record_id: item.id }
          expect(response).to have_http_status(:ok)
        end
      end
    end

    context 'for item collaborator' do
      include_context 'when collaborator logged in'

      it 'is successful' do
        @items.each do |item|
          get :collaboration_logs, params: { type: item.route_type, record_id: item.id }
          expect(response).to have_http_status(:ok)
        end
      end
    end

    context 'for item group member' do
      include_context 'when group member logged in'

      it 'is successful' do
        @items.each do |item|
          get :collaboration_logs, params: { type: item.route_type, record_id: item.id }
          expect(response).to have_http_status(:ok)
        end
      end
    end

    context 'for item group owner' do
      include_context 'when group owner logged in'

      it 'is successful' do
        @items.each do |item|
          get :collaboration_logs, params: { type: item.route_type, record_id: item.id }
          expect(response).to have_http_status(:ok)
        end
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'is not successful' do
        @items.each do |item|
          get :collaboration_logs, params: { type: item.route_type, record_id: item.id }
          expect(response).to have_http_status(:found)
          expect(response).to redirect_to(root_path)
          expect(flash[:alert]).to match(/Not authorized./)
        end
      end
    end

    context 'when not logged in' do

      it 'is not successful' do
        @items.each do |item|
          get :collaboration_logs, params: { type: item.route_type, record_id: item.id }
          expect(response).to have_http_status(:found)
          expect(response).to redirect_to(new_user_session_path)
          expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
        end
      end
    end
  end
end
