require 'rails_helper'

RSpec.describe RecurringEventsController, type: :controller do
  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group.add_member(@group_member, @admin)
  end

  shared_context 'when admin logged in' do
    before(:each) do
      sign_in @admin
    end
  end

  shared_context 'when calendar item owner logged in' do
    before(:each) do
      sign_in @owner
    end
  end

  shared_context 'when collaborator logged in' do
    before(:each) do
      sign_in @collaborator
    end
  end

  shared_context 'when group member logged in' do
    before(:each) do
      sign_in @group_member
    end
  end

  shared_context 'when user logged in' do
    before(:each) do
      sign_in @user
    end
  end

  describe 'GET show' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "returns 200 ok for another users private calendar's recurring_event" do
        calendar_private = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_private.id)
        get :show, params: { calendar_id: recurring_event.calendar_id, id: recurring_event.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for calendar item owner' do
      include_context 'when calendar item owner logged in'

      it "returns 200 ok for private calendar's recurring_event" do
        calendar_private = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_private.id)
        get :show, params: { calendar_id: recurring_event.calendar_id, id: recurring_event.id }
        expect(response).to have_http_status(:ok)
      end

      it "returns 200 ok for internal calendar's recurring_event" do
        calendar_internal = FactoryBot.create(:calendar, :internal, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_internal.id)
        get :show, params: { calendar_id: recurring_event.calendar_id, id: recurring_event.id }
        expect(response).to have_http_status(:ok)
      end

      it "returns 200 ok for public calendar's recurring_event" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        get :show, params: { calendar_id: recurring_event.calendar_id, id: recurring_event.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for calendar collaborator' do
      include_context 'when collaborator logged in'

      it "returns 200 ok for private calendar's recurring_event" do
        calendar_private = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        calendar_private.collaborate(@collaborator, calendar_private.user)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_private.id)
        get :show, params: { calendar_id: recurring_event.calendar_id, id: recurring_event.id }
        expect(response).to have_http_status(:ok)
      end

      it "returns 200 ok for internal calendar's recurring_event" do
        calendar_internal = FactoryBot.create(:calendar, :internal, user_id: @owner.id)
        calendar_internal.collaborate(@collaborator, calendar_internal.user)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_internal.id)
        get :show, params: { calendar_id: recurring_event.calendar_id, id: recurring_event.id }
        expect(response).to have_http_status(:ok)
      end

      it "returns 200 ok for public calendar's recurring_event" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.collaborate(@collaborator, calendar_public.user)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        get :show, params: { calendar_id: recurring_event.calendar_id, id: recurring_event.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for calendar group member' do
      include_context 'when group member logged in'

      it "returns 200 ok for private calendar's recurring_event" do
        calendar_private = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        calendar_private.add_group(@group, calendar_private.user)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_private.id)
        get :show, params: { calendar_id: recurring_event.calendar_id, id: recurring_event.id }
        expect(response).to have_http_status(:ok)
      end

      it "returns 200 ok for internal calendar's recurring_event" do
        calendar_internal = FactoryBot.create(:calendar, :internal, user_id: @owner.id)
        calendar_internal.add_group(@group, calendar_internal.user)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_internal.id)
        get :show, params: { calendar_id: recurring_event.calendar_id, id: recurring_event.id }
        expect(response).to have_http_status(:ok)
      end

      it "returns 200 ok for public calendar's recurring_event" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.add_group(@group, calendar_public.user)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        get :show, params: { calendar_id: recurring_event.calendar_id, id: recurring_event.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it "returns a 404 for a private calendar's recurring_event" do
        calendar_private = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_private.id)
        get :show, params: { calendar_id: recurring_event.calendar_id, id: recurring_event.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it "returns 200 ok for internal calendar's recurring_event" do
        calendar_internal = FactoryBot.create(:calendar, :internal, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_internal.id)
        get :show, params: { calendar_id: recurring_event.calendar_id, id: recurring_event.id }
        expect(response).to have_http_status(:ok)
      end

      it "returns 200 ok for public calendar's recurring_event" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        get :show, params: { calendar_id: recurring_event.calendar_id, id: recurring_event.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when not logged in' do
      it "returns a 404 for a private calendar's recurring_event" do
        calendar_private = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_private.id)
        get :show, params: { calendar_id: recurring_event.calendar_id, id: recurring_event.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it "returns 404 for internal calendar's recurring_event" do
        calendar_internal = FactoryBot.create(:calendar, :internal, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_internal.id)
        get :show, params: { calendar_id: recurring_event.calendar_id, id: recurring_event.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it "returns 200 ok for public calendar's recurring_event" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        get :show, params: { calendar_id: recurring_event.calendar_id, id: recurring_event.id }
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'GET new' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns 302 found redirect for calendar that user did not create' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        get :new, params: { calendar_id: calendar_public.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for calendar item owner' do
      include_context 'when calendar item owner logged in'

      it 'returns 200 ok' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        get :new, params: { calendar_id: calendar_public.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns 302 found redirect' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        get :new, params: { calendar_id: calendar_public.id }
        expect(response).to have_http_status(:found)
      end
    end

    context 'when not logged in' do
      it 'returns 302 found redirect' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        get :new, params: { calendar_id: calendar_public.id }
        expect(response).to have_http_status(:found)
      end
    end
  end

  describe 'GET edit' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns 200 ok' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        get :edit, params: { calendar_id: recurring_event.calendar_id, id: recurring_event.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for calendar item owner' do
      include_context 'when calendar item owner logged in'

      it 'returns 200 ok' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        get :edit, params: { calendar_id: recurring_event.calendar_id, id: recurring_event.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for calendar collaborator' do
      include_context 'when collaborator logged in'

      it 'returns 200 ok' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.collaborate(@collaborator, calendar_public.user)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        get :edit, params: { calendar_id: recurring_event.calendar_id, id: recurring_event.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for calendar group member' do
      include_context 'when group member logged in'

      it 'returns 200 ok' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.add_group(@group, calendar_public.user)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        get :edit, params: { calendar_id: recurring_event.calendar_id, id: recurring_event.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns 302 found redirect' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        get :edit, params: { calendar_id: recurring_event.calendar_id, id: recurring_event.id }
        expect(response).to have_http_status(:found)
      end
    end

    context 'when not logged in' do
      it 'returns 302 found redirect' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        get :edit, params: { calendar_id: recurring_event.calendar_id, id: recurring_event.id }
        expect(response).to have_http_status(:found)
      end
    end
  end

  describe 'POST create' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'creates new recurring_event with valid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        expect do
          post :create, params: {
            calendar_id: calendar_public.id, recurring_event: { summary: 'my title',
            start: DateTime.now, end: DateTime.now + 2.hours, year: DateTime.now.year,
            rrule: "FREQ=DAILY;INTERVAL=2;COUNT=20", month: DateTime.now.month, day: DateTime.now.day } }
        end.to change(RecurringEvent, :count).by(1)
      end

      it 'does not create a new recurring_event with invalid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        expect do
          post :create, params: {
            calendar_id: calendar_public.id, recurring_event: { summary: '',
            start: DateTime.now, end: DateTime.now + 2.hours, year: DateTime.now.year,
            rrule: "FREQ=DAILY;INTERVAL=2;COUNT=20", month: DateTime.now.month, day: DateTime.now.day } }
        end.to change(RecurringEvent, :count).by(0)
      end
    end

    context 'for calendar item owner' do
      include_context 'when calendar item owner logged in'

      it 'creates new recurring_event with valid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        expect do
          post :create, params: {
            calendar_id: calendar_public.id, recurring_event: { summary: 'my title',
            start: DateTime.now, end: DateTime.now + 2.hours, year: DateTime.now.year,
            rrule: "FREQ=DAILY;INTERVAL=2;COUNT=20", month: DateTime.now.month, day: DateTime.now.day } }
        end.to change(RecurringEvent, :count).by(1)
      end

      it 'does not create a new recurring_event with invalid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        expect do
          post :create, params: {
            calendar_id: calendar_public.id, recurring_event: { summary: '',
            start: DateTime.now, end: DateTime.now + 2.hours, year: DateTime.now.year,
            rrule: "FREQ=DAILY;INTERVAL=2;COUNT=20", month: DateTime.now.month, day: DateTime.now.day } }
        end.to change(RecurringEvent, :count).by(0)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'does not create a new recurring_event with valid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        expect do
          post :create, params: {
            calendar_id: calendar_public.id, recurring_event: { summary: 'my title',
            start: DateTime.now, end: DateTime.now + 2.hours, year: DateTime.now.year,
            rrule: "FREQ=DAILY;INTERVAL=2;COUNT=20", month: DateTime.now.month, day: DateTime.now.day } }
        end.to change(RecurringEvent, :count).by(0)
      end
    end

    context 'when not logged in' do
      it 'does not create a new recurring_event' do
        calendar_public = FactoryBot.create(:calendar, :public)
        expect do
          post :create, params: {
            calendar_id: calendar_public.id, recurring_event: { summary: 'my title',
            start: DateTime.now, end: DateTime.now + 2.hours, year: DateTime.now.year,
            rrule: "FREQ=DAILY;INTERVAL=2;COUNT=20", month: DateTime.now.month, day: DateTime.now.day } }
        end.to change(RecurringEvent, :count).by(0)
      end
    end
  end

  describe 'PATCH update' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "updates another user's calendar's recurring_event with valid data" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        patch :update, params: { calendar_id: calendar_public.id, id: recurring_event.id,
                                 recurring_event: { summary: 'updated_text', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to redirect_to(calendar_path(calendar_public.id))
      end

      it "does not update user's calendar's recurring_event with invalid data" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        patch :update, params: { calendar_id: calendar_public.id, id: recurring_event.id,
                                 recurring_event: { summary: '', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to have_http_status(422)
      end
    end

    context 'for calendar item owner' do
      include_context 'when calendar item owner logged in'

      it 'updates recurring_event with valid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        patch :update, params: { calendar_id: calendar_public.id, id: recurring_event.id,
                                 recurring_event: { summary: 'updated_text', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to redirect_to(calendar_path(calendar_public.id))
      end

      it 'does not update recurring_event with invalid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        patch :update, params: { calendar_id: calendar_public.id, id: recurring_event.id,
                                 recurring_event: { summary: '', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to have_http_status(422)
      end
    end

    context 'for calendar collaborator' do
      include_context 'when collaborator logged in'

      it 'updates recurring_event with valid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.collaborate(@collaborator,calendar_public.user)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        patch :update, params: { calendar_id: calendar_public.id, id: recurring_event.id,
                                 recurring_event: { summary: 'updated_text', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to redirect_to(calendar_path(calendar_public.id))
      end

      it 'does not update recurring_event with invalid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.collaborate(@collaborator, calendar_public.user)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        patch :update, params: { calendar_id: calendar_public.id, id: recurring_event.id,
                                 recurring_event: { summary: '', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to have_http_status(422)
      end
    end

    context 'for calendar group member' do
      include_context 'when group member logged in'

      it 'updates recurring_event with valid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.add_group(@group, calendar_public.user)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        patch :update, params: { calendar_id: calendar_public.id, id: recurring_event.id,
                                 recurring_event: { summary: 'updated_text', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to redirect_to(calendar_path(calendar_public.id))
      end

      it 'does not update recurring_event with invalid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.add_group(@group, calendar_public.user)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        patch :update, params: { calendar_id: calendar_public.id, id: recurring_event.id,
                                 recurring_event: { summary: '', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to have_http_status(422)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'does not update recurring_event' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        patch :update, params: { calendar_id: calendar_public.id, id: recurring_event.id,
                                 recurring_event: { front: 'updated_text', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to redirect_to(calendars_path)
      end
    end

    context 'when not logged in' do
      it 'does not update recurring_event' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        patch :update, params: { calendar_id: calendar_public.id, id: recurring_event.id,
                                 recurring_event: { summary: 'updated_text', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to redirect_to(user_session_path)
      end
    end
  end

  describe 'PUT update' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "updates another user's calendar's recurring_event with valid data" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        put :update,
            params: { calendar_id: calendar_public.id, id: recurring_event.id, recurring_event:
                { summary: 'updated_text', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to redirect_to(calendar_path(id: calendar_public.id))
      end

      it 'does not recurring_event with invalid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        put :update, params: { calendar_id: calendar_public.id, id: recurring_event.id,
                               recurring_event: { summary: '', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to have_http_status(422)
      end
    end

    context 'for calendar item owner' do
      include_context 'when calendar item owner logged in'

      it 'updates recurring_event with valid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        put :update,
            params: { calendar_id: calendar_public.id, id: recurring_event.id,
                      recurring_event: { summary: 'updated_text', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to redirect_to(calendar_path(id: calendar_public.id))
      end

      it 'does not update recurring_event with invalid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        put :update, params: { calendar_id: calendar_public.id, id: recurring_event.id,
                               recurring_event: { summary: '', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to have_http_status(422)
      end
    end

    context 'for calendar collaborator' do
      include_context 'when collaborator logged in'

      it 'updates recurring_event with valid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.collaborate(@collaborator, calendar_public.user)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        put :update,
            params: { calendar_id: calendar_public.id, id: recurring_event.id,
                      recurring_event: { summary: 'updated_text', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to redirect_to(calendar_path(calendar_public.id))
      end

      it 'does not update recurring_event with invalid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.collaborate(@collaborator, calendar_public.user)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        put :update, params: { calendar_id: calendar_public.id, id: recurring_event.id,
                               recurring_event: { summary: '', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to have_http_status(422)
      end
    end

    context 'for calendar group member' do
      include_context 'when group member logged in'

      it 'updates recurring_event with valid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.add_group(@group, calendar_public.user)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        put :update,
            params: { calendar_id: calendar_public.id, id: recurring_event.id,
                      recurring_event: { summary: 'updated_text', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to redirect_to(calendar_path(calendar_public.id))
      end

      it 'does not update recurring_event with invalid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.add_group(@group, calendar_public.user)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        put :update, params: { calendar_id: calendar_public.id, id: recurring_event.id,
                               recurring_event: { summary: '', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to have_http_status(422)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'does not update recurring_event' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        put :update,
            params: { calendar_id: calendar_public.id, id: recurring_event.id, recurring_event: {
              summary: 'updated_text', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to redirect_to(calendars_path)
      end
    end

    context 'when not logged in' do
      it 'does not update recurring_event' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        put :update,
            params: { calendar_id: calendar_public.id, id: recurring_event.id, recurring_event:
                { summary: 'updated_text', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to redirect_to(user_session_path)
      end
    end
  end

  describe 'DELETE destroy' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "destroys a user's calendar's recurring_event" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        expect do
          delete :destroy, params: { calendar_id: calendar_public.id, id: recurring_event.id }
        end.to change(RecurringEvent, :count).by(-1)
      end
    end

    context 'for calendar item owner' do
      include_context 'when calendar item owner logged in'

      it 'destroys a recurring_event' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        expect do
          delete :destroy, params: { calendar_id: calendar_public.id, id: recurring_event.id }
        end.to change(RecurringEvent, :count).by(-1)
      end
    end

    context 'for calendar collaborator' do
      include_context 'when collaborator logged in'

      it 'does not destroy recurring_event' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.collaborate(@collaborator, calendar_public.user)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        expect do
        delete :destroy, params: { calendar_id: calendar_public.id, id: recurring_event.id }
        end.to change(RecurringEvent, :count).by(0)
      end
    end

    context 'for calendar group member' do
      include_context 'when group member logged in'

      it 'does not destroy recurring_event' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.add_group(@group, calendar_public.user)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        expect do
        delete :destroy, params: { calendar_id: calendar_public.id, id: recurring_event.id }
        end.to change(RecurringEvent, :count).by(0)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'does not destroy recurring_event' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        expect do
          delete :destroy, params: { calendar_id: calendar_public.id, id: recurring_event.id }
        end.to change(RecurringEvent, :count).by(0)
      end

      it 'redirects to calendars index' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        delete :destroy, params: { calendar_id: calendar_public.id, id: recurring_event.id }
        expect(response).to redirect_to(calendars_path)
      end
    end

    context 'when not logged in' do
      it "does not destroy a user's calendar's recurring_event" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        expect do
          delete :destroy, params: { calendar_id: calendar_public.id, id: recurring_event.id }
        end.to change(RecurringEvent, :count).by(0)
      end

      it 'redirects to sign in' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id)
        delete :destroy, params: { calendar_id: calendar_public.id, id: recurring_event.id }
        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end

  describe 'PATCH add_exception' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "updates another user's calendar's recurring_event by adding an exception for event_date params" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id, exceptions: ["2024-09-03"])
        patch :add_exception, params: { calendar_id: calendar_public.id, recurring_event_id: recurring_event.id,
                                 year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day,
                                 event_year: "2024", event_month: "5", event_day: "6" }
        expect(recurring_event.reload.exceptions).to eq(["2024-09-03","2024-05-06"])
      end
    end

    context 'for calendar item owner' do
      include_context 'when calendar item owner logged in'

      it "updates recurring_event by adding an exception for event_date params" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id, exceptions: ["2024-09-03"])
        patch :add_exception, params: { calendar_id: calendar_public.id, recurring_event_id: recurring_event.id,
                                 year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day,
                                 event_year: "2024", event_month: "5", event_day: "6" }
        expect(recurring_event.reload.exceptions).to eq(["2024-09-03","2024-05-06"])
      end
    end

    context 'for calendar collaborator' do
      include_context 'when collaborator logged in'

      it "updates recurring_event by adding an exception for event_date params" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.collaborate(@collaborator, calendar_public.user)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id, exceptions: ["2024-09-03"])
        patch :add_exception, params: { calendar_id: calendar_public.id, recurring_event_id: recurring_event.id,
                                 year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day,
                                 event_year: "2024", event_month: "5", event_day: "6" }
        expect(recurring_event.reload.exceptions).to eq(["2024-09-03","2024-05-06"])
      end
    end

    context 'for calendar group member' do
      include_context 'when group member logged in'

      it "updates recurring_event by adding an exception for event_date params" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id, exceptions: ["2024-09-03"])
        calendar_public.add_group(@group, calendar_public.user)
        patch :add_exception, params: { calendar_id: calendar_public.id, recurring_event_id: recurring_event.id,
                                 year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day,
                                 event_year: "2024", event_month: "5", event_day: "6" }
        expect(recurring_event.reload.exceptions).to eq(["2024-09-03","2024-05-06"])
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it "updates recurring_event by adding an exception for event_date params" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id, exceptions: ["2024-09-03"])
        patch :add_exception, params: { calendar_id: calendar_public.id, recurring_event_id: recurring_event.id,
                                 year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day,
                                 event_year: "2024", event_month: "5", event_day: "6" }
        expect(recurring_event.reload.exceptions).to eq(["2024-09-03"])
        expect(response).to redirect_to(calendars_path)
      end
    end

    context 'when not logged in' do
      it "updates recurring_event by adding an exception for event_date params" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        recurring_event = FactoryBot.create(:recurring_event, calendar_id: calendar_public.id, exceptions: ["2024-09-03"])
        patch :add_exception, params: { calendar_id: calendar_public.id, recurring_event_id: recurring_event.id,
                                 year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day,
                                 event_year: "2024", event_month: "5", event_day: "6" }
        expect(recurring_event.reload.exceptions).to eq(["2024-09-03"])
        expect(response).to redirect_to(user_session_path)
      end
    end
  end
end

