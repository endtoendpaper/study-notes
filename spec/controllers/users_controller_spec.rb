require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  before(:each) do
    @png_file = Rack::Test::UploadedFile
                .new('spec/factories/images/rails-logo.png', 'image/jpeg')
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @user1 = FactoryBot.create(:user, :confirmed)
    @user2 = FactoryBot.create(:user, :confirmed)
    @user3 = FactoryBot.create(:user, :confirmed, :hide_relationships)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group_owner = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group, user_id: @group_owner.id)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group.add_member(@group_member, @group.user)
  end

  shared_context 'when admin logged in' do
    before(:each) do
      sign_in @admin
    end
  end

  shared_context 'when user logged in' do
    before(:each) do
      sign_in @user1
    end
  end

  shared_context 'when collaborator logged in' do
    before(:each) do
      sign_in @collaborator
    end
  end

  shared_context 'when group member logged in' do
    before(:each) do
      sign_in @group_member
    end
  end

  shared_context 'when group owner logged in' do
    before(:each) do
      sign_in @group_owner
    end
  end

  shared_context 'when another user logged in' do
    before(:each) do
      sign_in @user2
    end
  end





  describe 'GET index' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        get :index
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        get :index
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end

    end

    context 'when not logged in' do

      it 'should not be successful' do
        get :index
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end
  end

  describe 'GET show' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        get :show, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should be successful' do
        get :show, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end

    context 'when not logged in' do

      it 'should be successful' do
        get :show, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end
  end

  describe 'GET show_groups' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        get :show_groups, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should be successful' do
        get :show_groups, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
        get :show_groups, params: { id: @user2.username }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'GET show_stars' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful when hide_stars is true' do
        @user1.update(hide_stars: 'true')
        get :show_stars, params: { id: @user1.username}
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
        expect(response).to_not render_template('errors/not_found')
      end

      it 'should be successful when hide_stars is false' do
        @user1.update(hide_stars: 'false')
        get :show_stars, params: { id: @user1.username}
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
        expect(response).to_not render_template('errors/not_found')
      end
    end

    context 'for the user' do
      include_context 'when user logged in'

      it 'should be successful when hide_stars is true' do
        @user1.update(hide_stars: 'true')
        get :show_stars, params: { id: @user1.username}
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
        expect(response).to_not render_template('errors/not_found')
      end

      it 'should be successful when hide_stars is false' do
        @user1.update(hide_stars: 'false')
        get :show_stars, params: { id: @user1.username}
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
        expect(response).to_not render_template('errors/not_found')
      end
    end

    context 'for other user' do
      it 'should be successful when hide_stars is false' do
        sign_in @user2
        @user1.update(hide_stars: 'false')
        get :show_stars, params: { id: @user1.username}
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
        expect(response).to_not render_template('errors/not_found')
      end

      it 'should not be successful when hide_stars is true' do
        sign_in @user2
        @user1.update(hide_stars: 'true')
        get :show_stars, params: { id: @user1.username}
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
        expect(response).to render_template('errors/not_found')
      end
    end

    context 'when not logged in' do

      it 'should be successful when hide_stars is false' do
        @user1.update(hide_stars: 'false')
        get :show_stars, params: { id: @user1.username}
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
        expect(response).to_not render_template('errors/not_found')
      end

      it 'should not be successful when hide_stars is true' do
        @user1.update(hide_stars: 'true')
        get :show_stars, params: { id: @user1.username}
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
        expect(response).to render_template('errors/not_found')
      end
    end
  end

  describe 'GET new' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        get :new
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        get :new
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
        get :new
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'GET edit' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        get :edit, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        get :edit, params: { id: @user1.username}
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
        get :edit, params: { id: @user2.username }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'PATCH update' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful with valid data' do
        patch :update, params: { id: @user1.username, user: { username: 'meL1-me_m91e' } }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(user_path(@user1.reload))
        expect(flash[:notice]).to match(/User was successfully updated./)
        expect(@user1.reload.username).to eq("meL1-me_m91e")
      end

      it 'should not be successful invalid data' do
        patch :update, params: { id: @user1.reload.username, user: { username: 'me me/me' } }
        expect(response).to have_http_status(:unprocessable_entity)
        expect(@user1.reload.username).to_not eq("me-me/me")
      end

      it 'should update admin attribute' do
        expect(@user2.admin).to be false
        patch :update, params: { id: @user2.username, user: { admin: 'true' } }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(user_path(@user2))
        expect(flash[:notice]).to match(/User was successfully updated./)
        expect(@user2.reload.admin).to be true
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        patch :update, params: { id: @user1.username, user: { username: 'me-me/me' } }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
        patch :update, params: { id: @user1.username, user: { username: 'me-me/me' } }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'PUT update' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful with valid data' do
        put :update, params: { id: @user1.username, user: { username: 'me-me_222me' } }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(user_path(@user1.reload))
        expect(flash[:notice]).to match(/User was successfully updated./)
        expect(@user1.reload.username).to eq("me-me_222me")
      end

      it 'should not be successful invalid data' do
        put :update, params: { id: @user1.reload.username, user: { username: 'me me/me' } }
        expect(response).to have_http_status(:unprocessable_entity)
        expect(@user1.reload.username).to_not eq("me-me/me")
      end

      it 'should update admin attribute' do
        @user2.update(admin: 'false')
        expect(@user2.reload.admin).to be false
        put :update, params: { id: @user2.username, user: { admin: 'true' } }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(user_path(@user2))
        expect(flash[:notice]).to match(/User was successfully updated./)
        expect(@user2.reload.admin).to be true
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        put :update, params: { id: @user1.username, user: { username: 'me-me/me' } }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
        put :update, params: { id: @user1.username, user: { username: 'me-me/me' } }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'DELETE destroy' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        expect do
          delete :destroy, params: { id: @user1.username}
        end.to change(User, :count).by(-1)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(users_path)
        expect(flash[:notice]).to match(/User was successfully deleted./)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        expect do
          delete :destroy, params: { id: @user1.username}
        end.to change(User, :count).by(0)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
      expect do
        delete :destroy, params: { id: @user1.username}
      end.to change(User, :count).by(0)
      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(new_user_session_path)
      expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
    end

    end
  end

  describe 'DELETE delete_avatar' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        @user1.update(avatar: @png_file)
        expect(@user1.reload.avatar).to be_attached
        delete :delete_avatar, params: { id: @user1.username}
        expect(response).to have_http_status(:found)
        expect(@user1.reload.avatar).to_not be_attached
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        @user1.update(avatar: @png_file)
        expect(@user1.reload.avatar).to be_attached
        delete :delete_avatar, params: { id: @user1.username}
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
        expect(@user1.reload.avatar).to be_attached
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
        @user1.update(avatar: @png_file)
        expect(@user1.reload.avatar).to be_attached
        delete :delete_avatar, params: { id: @user1.username}
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
        expect(@user1.reload.avatar).to be_attached
      end
    end
  end

  describe 'GET following' do
    context 'for admin' do
      include_context 'when admin logged in'

      it 'should be successful when follow is enabled in site settings' do
        SiteSetting.first.update(follow_users: true)
        get :following, params: { id: @user1.username}
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end

      it 'should be successful when user selected to hide follow info' do
        SiteSetting.first.update(follow_users: true)
        get :following, params: { id: @user3.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end

      it 'should not be successful when follow is disabled in site settings' do
        SiteSetting.first.update(follow_users: false)
        get :following, params: { id: @user1.username}
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
        expect(response).to render_template('errors/not_found')
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should be successful when follow is enabled in site settings' do
        SiteSetting.first.update(follow_users: true)
        get :following, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end

      it 'should be successful when user selected to hide their follow info' do
        SiteSetting.first.update(follow_users: true)
        @user1.update(hide_relationships: true)
        get :following, params: { id: @user1.username}
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end

      it 'should not be successful for another user who selected to hide follow data' do
        SiteSetting.first.update(follow_users: true)
        get :following, params: { id: @user3.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
        expect(response).to render_template('errors/not_found')
      end

      it 'should not be successful when follow is disabled in site settings' do
        SiteSetting.first.update(follow_users: false)
        get :following, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
        expect(response).to render_template('errors/not_found')
      end
    end

    context 'when not logged in' do

      it 'should be successful when follow is enabled in site settings' do
        SiteSetting.first.update(follow_users: true)
        get :following, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end

      it 'should not be successful for user who selected to hide follow data' do
        SiteSetting.first.update(follow_users: true)
        get :following, params: { id: @user3.username }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
        expect(flash[:alert]).to be_nil
      end

      it 'should not be successful when follow is disabled in site settings' do
        SiteSetting.first.update(follow_users: false)
        get :following, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
        expect(flash[:alert]).to be_nil
      end
    end
  end

  describe 'GET followers' do
    context 'for admin' do
      include_context 'when admin logged in'

      it 'should be successful when follow is enabled in site settings' do
        SiteSetting.first.update(follow_users: true)
        get :followers, params: { id: @user1.username}
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end

      it 'should be successful when user selected to hide follow info' do
        SiteSetting.first.update(follow_users: true)
        get :followers, params: { id: @user3.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end

      it 'should not be successful when follow is disabled in site settings' do
        SiteSetting.first.update(follow_users: false)
        get :followers, params: { id: @user1.username}
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
        expect(response).to render_template('errors/not_found')
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should be successful when follow is enabled in site settings' do
        SiteSetting.first.update(follow_users: true)
        get :followers, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end

      it 'should be successful when user selected to hide their follow info' do
        SiteSetting.first.update(follow_users: true)
        @user1.update(hide_relationships: true)
        get :followers, params: { id: @user1.username}
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end

      it 'should not be successful for another user who selected to hide follow data' do
        SiteSetting.first.update(follow_users: true)
        get :followers, params: { id: @user3.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
        expect(response).to render_template('errors/not_found')
      end

      it 'should not be successful when follow is disabled in site settings' do
        SiteSetting.first.update(follow_users: false)
        get :followers, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
        expect(response).to render_template('errors/not_found')
      end
    end

    context 'when not logged in' do

      it 'should be successful when follow is enabled in site settings' do
        SiteSetting.first.update(follow_users: true)
        get :followers, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end

      it 'should not be successful for user who selected to hide follow data' do
        SiteSetting.first.update(follow_users: true)
        get :followers, params: { id: @user3.username }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
        expect(flash[:alert]).to be_nil
      end

      it 'should not be successful when follow is disabled in site settings' do
        SiteSetting.first.update(follow_users: false)
        get :followers, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
        expect(flash[:alert]).to be_nil
      end
    end
  end

  describe 'GET show_items' do
    render_views

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns public, internal, and private notes for a user' do
        UserItems.item_list.each do |item|
          reset_elasticsearch_indices

          public1 = instance_variable_set("@#{item.singularize}_public", FactoryBot.create(item.singularize, :public, user_id: @user1.id))
          internal1 = instance_variable_set("@#{item.singularize}_internal", FactoryBot.create(item.singularize, :internal, user_id: @user1.id))
          private1 = instance_variable_set("@#{item.singularize}_private", FactoryBot.create(item.singularize, :private, user_id: @user1.id))
          public2 = instance_variable_set("@#{item.singularize}_public", FactoryBot.create(item.singularize, :public, user_id: @user2.id))
          internal2 = instance_variable_set("@#{item.singularize}_internal", FactoryBot.create(item.singularize, :internal, user_id: @user2.id))
          private2 = instance_variable_set("@#{item.singularize}_private", FactoryBot.create(item.singularize, :private, user_id: @user2.id))

          reindex_elasticsearch_data

          get :show_items, params: { id: @user1.username, type: item }
          expect(response).to have_http_status(:ok)
          expect(response.body).to have_content(public1.title)
          expect(response.body).to have_content(internal1.title)
          expect(response.body).to have_content(private1.title)
          expect(response.body).to_not have_content(public2.title)
          expect(response.body).to_not have_content(internal2.title)
          expect(response.body).to_not have_content(private2.title)
        end
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns public, internal, and private notes for user' do
        UserItems.item_list.each do |item|
          UserItems.item_list.each do |model|
            begin
              UserItems.class(model).__elasticsearch__.delete_index!
            rescue
              # Ignore if the index does not exist
            end
            UserItems.class(model).__elasticsearch__.create_index!
          end
          public1 = instance_variable_set("@#{item.singularize}_public", FactoryBot.create(item.singularize, :public, user_id: @user1.id))
          internal1 = instance_variable_set("@#{item.singularize}_internal", FactoryBot.create(item.singularize, :internal, user_id: @user1.id))
          private1 = instance_variable_set("@#{item.singularize}_private", FactoryBot.create(item.singularize, :private, user_id: @user1.id))
          public2 = instance_variable_set("@#{item.singularize}_public", FactoryBot.create(item.singularize, :public, user_id: @user2.id))
          internal2 = instance_variable_set("@#{item.singularize}_internal", FactoryBot.create(item.singularize, :internal, user_id: @user2.id))
          private2 = instance_variable_set("@#{item.singularize}_private", FactoryBot.create(item.singularize, :private, user_id: @user2.id))
          # Reindex & refresh all records
          UserItems.item_list.each do |model|
            UserItems.class(model).import
            UserItems.class(model).__elasticsearch__.refresh_index!
          end
          get :show_items, params: { id: @user1.username, type: item }
          expect(response).to have_http_status(:ok)
          expect(response.body).to have_content(public1.title)
          expect(response.body).to have_content(internal1.title)
          expect(response.body).to have_content(private1.title)
          expect(response.body).to_not have_content(public2.title)
          expect(response.body).to_not have_content(internal2.title)
          expect(response.body).to_not have_content(private2.title)
        end
      end
    end

    context 'for collaborator' do
      include_context 'when collaborator logged in'

      it 'returns public, internal, collaborated notes for another user' do
        UserItems.item_list.each do |item|
          UserItems.item_list.each do |model|
            begin
              UserItems.class(model).__elasticsearch__.delete_index!
            rescue
              # Ignore if the index does not exist
            end
            UserItems.class(model).__elasticsearch__.create_index!
          end
          public1 = instance_variable_set("@#{item.singularize}_public", FactoryBot.create(item.singularize, :public, user_id: @user1.id))
          internal1 = instance_variable_set("@#{item.singularize}_internal", FactoryBot.create(item.singularize, :internal, user_id: @user1.id))
          private1 = instance_variable_set("@#{item.singularize}_private", FactoryBot.create(item.singularize, :private, user_id: @user1.id))
          public2 = instance_variable_set("@#{item.singularize}_public", FactoryBot.create(item.singularize, :public, user_id: @user2.id))
          internal2 = instance_variable_set("@#{item.singularize}_internal", FactoryBot.create(item.singularize, :internal, user_id: @user2.id))
          private2 = instance_variable_set("@#{item.singularize}_private", FactoryBot.create(item.singularize, :private, user_id: @user2.id))
          public1.collaborate(@collaborator, public1.user)
          internal1.collaborate(@collaborator, internal1.user)
          private1.collaborate(@collaborator, private1.user)
          public2.collaborate(@collaborator, public2.user)
          internal2.collaborate(@collaborator, internal2.user)
          private2.collaborate(@collaborator, private2.user)
          # Reindex & refresh all records
          UserItems.item_list.each do |model|
            UserItems.class(model).import
            UserItems.class(model).__elasticsearch__.refresh_index!
          end
          get :show_items, params: { id: @user1.username, type: item }
          expect(response).to have_http_status(:ok)
          expect(response.body).to have_content(public1.title)
          expect(response.body).to have_content(internal1.title)
          expect(response.body).to have_content(private1.title)
          expect(response.body).to_not have_content(public2.title)
          expect(response.body).to_not have_content(internal2.title)
          expect(response.body).to_not have_content(private2.title)
        end
      end
    end

    context 'for group member' do
      include_context 'when group member logged in'

      context 'for note group member' do
        include_context 'when group member logged in'

        it 'returns public, internal, collaborated notes for another user' do
          UserItems.item_list.each do |item|
            UserItems.item_list.each do |model|
              begin
                UserItems.class(model).__elasticsearch__.delete_index!
              rescue
                # Ignore if the index does not exist
              end
              UserItems.class(model).__elasticsearch__.create_index!
            end
            public1 = instance_variable_set("@#{item.singularize}_public", FactoryBot.create(item.singularize, :public, user_id: @user1.id))
            internal1 = instance_variable_set("@#{item.singularize}_internal", FactoryBot.create(item.singularize, :internal, user_id: @user1.id))
            private1 = instance_variable_set("@#{item.singularize}_private", FactoryBot.create(item.singularize, :private, user_id: @user1.id))
            public2 = instance_variable_set("@#{item.singularize}_public", FactoryBot.create(item.singularize, :public, user_id: @user2.id))
            internal2 = instance_variable_set("@#{item.singularize}_internal", FactoryBot.create(item.singularize, :internal, user_id: @user2.id))
            private2 = instance_variable_set("@#{item.singularize}_private", FactoryBot.create(item.singularize, :private, user_id: @user2.id))
            public1.add_group(@group, public1.user)
            internal1.add_group(@group, internal1.user)
            private1.add_group(@group, private1.user)
            public2.add_group(@group, public2.user)
            internal2.add_group(@group, internal2.user)
            private2.add_group(@group, private2.user)
            # Reindex & refresh all records
            UserItems.item_list.each do |model|
              UserItems.class(model).import
              UserItems.class(model).__elasticsearch__.refresh_index!
            end
            get :show_items, params: { id: @user1.username, type: item }
            expect(response).to have_http_status(:ok)
            expect(response.body).to have_content(public1.title)
            expect(response.body).to have_content(internal1.title)
            expect(response.body).to have_content(private1.title)
            expect(response.body).to_not have_content(public2.title)
            expect(response.body).to_not have_content(internal2.title)
            expect(response.body).to_not have_content(private2.title)
          end
        end
      end
    end

    context 'for group owner' do
      include_context 'when group owner logged in'

      it 'returns public, internal, collaborated notes for another user' do
        UserItems.item_list.each do |item|
          UserItems.item_list.each do |model|
            begin
              UserItems.class(model).__elasticsearch__.delete_index!
            rescue
              # Ignore if the index does not exist
            end
            UserItems.class(model).__elasticsearch__.create_index!
          end
          public1 = instance_variable_set("@#{item.singularize}_public", FactoryBot.create(item.singularize, :public, user_id: @user1.id))
          internal1 = instance_variable_set("@#{item.singularize}_internal", FactoryBot.create(item.singularize, :internal, user_id: @user1.id))
          private1 = instance_variable_set("@#{item.singularize}_private", FactoryBot.create(item.singularize, :private, user_id: @user1.id))
          public2 = instance_variable_set("@#{item.singularize}_public", FactoryBot.create(item.singularize, :public, user_id: @user2.id))
          internal2 = instance_variable_set("@#{item.singularize}_internal", FactoryBot.create(item.singularize, :internal, user_id: @user2.id))
          private2 = instance_variable_set("@#{item.singularize}_private", FactoryBot.create(item.singularize, :private, user_id: @user2.id))
          public1.add_group(@group, public1.user)
          internal1.add_group(@group, internal1.user)
          private1.add_group(@group, private1.user)
          public2.add_group(@group, public2.user)
          internal2.add_group(@group, internal2.user)
          private2.add_group(@group, private2.user)
          # Reindex & refresh all records
          UserItems.item_list.each do |model|
            UserItems.class(model).import
            UserItems.class(model).__elasticsearch__.refresh_index!
          end
          get :show_items, params: { id: @user1.username, type: item }
          expect(response).to have_http_status(:ok)
          expect(response.body).to have_content(public1.title)
          expect(response.body).to have_content(internal1.title)
          expect(response.body).to have_content(private1.title)
          expect(response.body).to_not have_content(public2.title)
          expect(response.body).to_not have_content(internal2.title)
          expect(response.body).to_not have_content(private2.title)
        end
      end
    end

    context 'for another user' do
      include_context 'when another user logged in'

      it 'returns public, internal notes for another user' do
        UserItems.item_list.each do |item|
          UserItems.item_list.each do |model|
            begin
              UserItems.class(model).__elasticsearch__.delete_index!
            rescue
              # Ignore if the index does not exist
            end
            UserItems.class(model).__elasticsearch__.create_index!
          end
          public1 = instance_variable_set("@#{item.singularize}_public", FactoryBot.create(item.singularize, :public, user_id: @user1.id))
          internal1 = instance_variable_set("@#{item.singularize}_internal", FactoryBot.create(item.singularize, :internal, user_id: @user1.id))
          private1 = instance_variable_set("@#{item.singularize}_private", FactoryBot.create(item.singularize, :private, user_id: @user1.id))
          public2 = instance_variable_set("@#{item.singularize}_public", FactoryBot.create(item.singularize, :public, user_id: @user2.id))
          internal2 = instance_variable_set("@#{item.singularize}_internal", FactoryBot.create(item.singularize, :internal, user_id: @user2.id))
          private2 = instance_variable_set("@#{item.singularize}_private", FactoryBot.create(item.singularize, :private, user_id: @user2.id))
          # Reindex & refresh all records
          UserItems.item_list.each do |model|
            UserItems.class(model).import
            UserItems.class(model).__elasticsearch__.refresh_index!
          end
          get :show_items, params: { id: @user1.username, type: item }
          expect(response).to have_http_status(:ok)
          expect(response.body).to have_content(public1.title)
          expect(response.body).to have_content(internal1.title)
          expect(response.body).to_not have_content(private1.title)
          expect(response.body).to_not have_content(public2.title)
          expect(response.body).to_not have_content(internal2.title)
          expect(response.body).to_not have_content(private2.title)
        end
      end
    end

    context 'when not logged in' do

      context 'when not logged in' do
        it 'returns only public notes' do
          UserItems.item_list.each do |item|
            UserItems.item_list.each do |model|
              begin
                UserItems.class(model).__elasticsearch__.delete_index!
              rescue
                # Ignore if the index does not exist
              end
              UserItems.class(model).__elasticsearch__.create_index!
            end
            public1 = instance_variable_set("@#{item.singularize}_public", FactoryBot.create(item.singularize, :public, user_id: @user1.id))
            internal1 = instance_variable_set("@#{item.singularize}_internal", FactoryBot.create(item.singularize, :internal, user_id: @user1.id))
            private1 = instance_variable_set("@#{item.singularize}_private", FactoryBot.create(item.singularize, :private, user_id: @user1.id))
            public2 = instance_variable_set("@#{item.singularize}_public", FactoryBot.create(item.singularize, :public, user_id: @user2.id))
            internal2 = instance_variable_set("@#{item.singularize}_internal", FactoryBot.create(item.singularize, :internal, user_id: @user2.id))
            private2 = instance_variable_set("@#{item.singularize}_private", FactoryBot.create(item.singularize, :private, user_id: @user2.id))
            # Reindex & refresh all records
            UserItems.item_list.each do |model|
              UserItems.class(model).import
              UserItems.class(model).__elasticsearch__.refresh_index!
            end
            get :show_items, params: { id: @user1.username, type: item }
            expect(response).to have_http_status(:ok)
            expect(response.body).to have_content(public1.title)
            expect(response.body).to_not have_content(internal1.title)
            expect(response.body).to_not have_content(private1.title)
            expect(response.body).to_not have_content(public2.title)
            expect(response.body).to_not have_content(internal2.title)
            expect(response.body).to_not have_content(private2.title)
          end
        end
      end
    end
  end

  describe 'GET bulk_edit_ownership' do
    render_views

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        get :bulk_edit_ownership, params: { id: @user1.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should be successful' do
        get :bulk_edit_ownership, params: { id: @user1.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end

    context 'for another user' do
      include_context 'when another user logged in'

      it 'should not be successful' do
        get :bulk_edit_ownership, params: { id: @user1.username }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
        expect(flash[:alert]).to match(/Not authorized./)
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
        get :bulk_edit_ownership, params: { id: @user1.username }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'POST bulk_transfer_ownership' do
    render_views

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should transfer groups' do
        FactoryBot.create(:group, user_id: @user1.id)
        FactoryBot.create(:group, user_id: @user1.id)
        expect(@user1.reload.groups.count).to eq(2)
        UserItems.item_list.each do |item|
          FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          expect(@user1.reload.send(item).count).to eq(1)
        end
        post :bulk_transfer_ownership, params: { id: @user1.username, new_user: @user2.username, groups: "1" }
        expect(@user1.reload.groups.count).to eq(0)
        expect(@user2.reload.groups.count).to eq(2)
        UserItems.item_list.each do |item|
          expect(@user1.reload.send(item).count).to eq(1)
          expect(@user2.reload.send(item).count).to eq(0)
        end
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(@user1)
        expect(flash[:notice]).to eq("Items were successfully transferred to #{@user2.handle}.")
      end

      it 'should be transfer items' do
        FactoryBot.create(:group, user_id: @user1.id)
        FactoryBot.create(:group, user_id: @user1.id)
        expect(@user1.reload.groups.count).to eq(2)
        UserItems.item_list.each do |item|
          FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          expect(@user1.reload.send(item).count).to eq(2)
        end
        params = Hash.new
        UserItems.item_list.each do |item|
          params[item.to_sym] = "1"
        end
        params[:id] = @user1.username
        params[:new_user] = @user2.handle
        post :bulk_transfer_ownership, params: params
        expect(@user1.reload.groups.count).to eq(2)
        expect(@user2.reload.groups.count).to eq(0)
        UserItems.item_list.each do |item|
          expect(@user1.reload.send(item).count).to eq(0)
          expect(@user2.reload.send(item).count).to eq(2)
        end
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(@user1)
        expect(flash[:notice]).to eq("Items were successfully transferred to #{@user2.handle}.")
      end

      it 'should throw error is username is invalid' do
        FactoryBot.create(:group, user_id: @user1.id)
        FactoryBot.create(:group, user_id: @user1.id)
        UserItems.item_list.each do |item|
          FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          expect(@user1.reload.send(item).count).to eq(2)
        end
        params = Hash.new
        UserItems.item_list.each do |item|
          params[item.to_sym] = "1"
        end
        params[:id] = @user1.username
        params[:new_user] = "notauser"
        post :bulk_transfer_ownership, params: params
        expect(@user1.reload.groups.count).to eq(2)
        expect(@user2.reload.groups.count).to eq(0)
        UserItems.item_list.each do |item|
          expect(@user1.reload.send(item).count).to eq(2)
          expect(@user2.reload.send(item).count).to eq(0)
        end
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'should throw error is username is user' do
        FactoryBot.create(:group, user_id: @user1.id)
        FactoryBot.create(:group, user_id: @user1.id)
        UserItems.item_list.each do |item|
          FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          expect(@user1.reload.send(item).count).to eq(2)
        end
        params = Hash.new
        UserItems.item_list.each do |item|
          params[item.to_sym] = "1"
        end
        params[:id] = @user1.username
        params[:new_user] = @user1.username
        post :bulk_transfer_ownership, params: params
        expect(@user1.reload.groups.count).to eq(2)
        expect(@user2.reload.groups.count).to eq(0)
        UserItems.item_list.each do |item|
          expect(@user1.reload.send(item).count).to eq(2)
          expect(@user2.reload.send(item).count).to eq(0)
        end
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'should throw error if no options are selected' do
        FactoryBot.create(:group, user_id: @user1.id)
        FactoryBot.create(:group, user_id: @user1.id)
        UserItems.item_list.each do |item|
          FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          expect(@user1.reload.send(item).count).to eq(2)
        end
        params = Hash.new
        params[:id] = @user1.username
        params[:new_user] = @user1.username
        post :bulk_transfer_ownership, params: params
        expect(@user1.reload.groups.count).to eq(2)
        expect(@user2.reload.groups.count).to eq(0)
        UserItems.item_list.each do |item|
          expect(@user1.reload.send(item).count).to eq(2)
          expect(@user2.reload.send(item).count).to eq(0)
        end
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should transfer groups' do
        FactoryBot.create(:group, user_id: @user1.id)
        FactoryBot.create(:group, user_id: @user1.id)
        expect(@user1.reload.groups.count).to eq(2)
        UserItems.item_list.each do |item|
          FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          expect(@user1.reload.send(item).count).to eq(1)
        end
        post :bulk_transfer_ownership, params: { id: @user1.username, new_user: @user2.username, groups: "1" }
        expect(@user1.reload.groups.count).to eq(0)
        expect(@user2.reload.groups.count).to eq(2)
        UserItems.item_list.each do |item|
          expect(@user1.reload.send(item).count).to eq(1)
          expect(@user2.reload.send(item).count).to eq(0)
        end
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(@user1)
        expect(flash[:notice]).to eq("Items were successfully transferred to #{@user2.handle}.")
      end

      it 'should be transfer items' do
        FactoryBot.create(:group, user_id: @user1.id)
        FactoryBot.create(:group, user_id: @user1.id)
        expect(@user1.reload.groups.count).to eq(2)
        UserItems.item_list.each do |item|
          FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          expect(@user1.reload.send(item).count).to eq(2)
        end
        params = Hash.new
        UserItems.item_list.each do |item|
          params[item.to_sym] = "1"
        end
        params[:id] = @user1.username
        params[:new_user] = @user2.handle
        post :bulk_transfer_ownership, params: params
        expect(@user1.reload.groups.count).to eq(2)
        expect(@user2.reload.groups.count).to eq(0)
        UserItems.item_list.each do |item|
          expect(@user1.reload.send(item).count).to eq(0)
          expect(@user2.reload.send(item).count).to eq(2)
        end
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(@user1)
        expect(flash[:notice]).to eq("Items were successfully transferred to #{@user2.handle}.")
      end

      it 'should throw error is username is invalid' do
        FactoryBot.create(:group, user_id: @user1.id)
        FactoryBot.create(:group, user_id: @user1.id)
        UserItems.item_list.each do |item|
          FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          expect(@user1.reload.send(item).count).to eq(2)
        end
        params = Hash.new
        UserItems.item_list.each do |item|
          params[item.to_sym] = "1"
        end
        params[:id] = @user1.username
        params[:new_user] = "notauser"
        post :bulk_transfer_ownership, params: params
        expect(@user1.reload.groups.count).to eq(2)
        expect(@user2.reload.groups.count).to eq(0)
        UserItems.item_list.each do |item|
          expect(@user1.reload.send(item).count).to eq(2)
          expect(@user2.reload.send(item).count).to eq(0)
        end
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'should throw error is username is user' do
        FactoryBot.create(:group, user_id: @user1.id)
        FactoryBot.create(:group, user_id: @user1.id)
        UserItems.item_list.each do |item|
          FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          expect(@user1.reload.send(item).count).to eq(2)
        end
        params = Hash.new
        UserItems.item_list.each do |item|
          params[item.to_sym] = "1"
        end
        params[:id] = @user1.username
        params[:new_user] = @user1.username
        post :bulk_transfer_ownership, params: params
        expect(@user1.reload.groups.count).to eq(2)
        expect(@user2.reload.groups.count).to eq(0)
        UserItems.item_list.each do |item|
          expect(@user1.reload.send(item).count).to eq(2)
          expect(@user2.reload.send(item).count).to eq(0)
        end
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'should throw error if no options are selected' do
        FactoryBot.create(:group, user_id: @user1.id)
        FactoryBot.create(:group, user_id: @user1.id)
        UserItems.item_list.each do |item|
          FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          expect(@user1.reload.send(item).count).to eq(2)
        end
        params = Hash.new
        params[:id] = @user1.username
        params[:new_user] = @user1.username
        post :bulk_transfer_ownership, params: params
        expect(@user1.reload.groups.count).to eq(2)
        expect(@user2.reload.groups.count).to eq(0)
        UserItems.item_list.each do |item|
          expect(@user1.reload.send(item).count).to eq(2)
          expect(@user2.reload.send(item).count).to eq(0)
        end
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context 'for another user' do
      include_context 'when another user logged in'

      it 'should not be successful' do
        FactoryBot.create(:group, user_id: @user1.id)
        FactoryBot.create(:group, user_id: @user1.id)
        expect(@user1.reload.groups.count).to eq(2)
        UserItems.item_list.each do |item|
          FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          expect(@user1.reload.send(item).count).to eq(2)
        end
        params = Hash.new
        UserItems.item_list.each do |item|
          params[item.to_sym] = "1"
        end
        params[:id] = @user1.username
        params[:new_user] = @user2.handle
        post :bulk_transfer_ownership, params: params
        expect(@user1.reload.groups.count).to eq(2)
        expect(@user2.reload.groups.count).to eq(0)
        UserItems.item_list.each do |item|
          expect(@user1.reload.send(item).count).to eq(2)
          expect(@user2.reload.send(item).count).to eq(0)
        end
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
        expect(flash[:alert]).to match(/Not authorized./)
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
        FactoryBot.create(:group, user_id: @user1.id)
        FactoryBot.create(:group, user_id: @user1.id)
        expect(@user1.reload.groups.count).to eq(2)
        UserItems.item_list.each do |item|
          FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          expect(@user1.reload.send(item).count).to eq(2)
        end
        params = Hash.new
        UserItems.item_list.each do |item|
          params[item.to_sym] = "1"
        end
        params[:id] = @user1.username
        params[:new_user] = @user2.handle
        post :bulk_transfer_ownership, params: params
        expect(@user1.reload.groups.count).to eq(2)
        expect(@user2.reload.groups.count).to eq(0)
        UserItems.item_list.each do |item|
          expect(@user1.reload.send(item).count).to eq(2)
          expect(@user2.reload.send(item).count).to eq(0)
        end
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end
end
