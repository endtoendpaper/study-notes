require 'rails_helper'

RSpec.describe CardsController, type: :controller do
  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @user1 = FactoryBot.create(:user, :confirmed)
    @user2 = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group.add_member(@group_member, @admin)
  end

  shared_context 'when admin logged in' do
    before(:each) do
      sign_in @admin
    end
  end

  shared_context 'when deck item owner logged in' do
    before(:each) do
      sign_in @user1
    end
  end

  shared_context 'when collaborator logged in' do
    before(:each) do
      sign_in @collaborator
    end
  end

  shared_context 'when group member logged in' do
    before(:each) do
      sign_in @group_member
    end
  end

  shared_context 'when user logged in' do
    before(:each) do
      sign_in @user2
    end
  end

  describe 'GET private deck cards index' do
    render_views

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns all cards' do
        deck_private = FactoryBot.create(:deck, :private, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_private.id)
        card2 = FactoryBot.create(:card, front: 'question2', back: 'answer2',
                                         deck_id: deck_private.id)
        get :index, params: { deck_id: card.deck_id, id: card.deck_id }
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content('question1')
        expect(response.body).to have_content('answer1')
        expect(response.body).to have_content('question2')
        expect(response.body).to have_content('answer2')
      end
    end

    context 'for deck item owner' do
      include_context 'when deck item owner logged in'

      it 'returns all cards' do
        deck_private = FactoryBot.create(:deck, :private, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_private.id)
        card2 = FactoryBot.create(:card, front: 'question2', back: 'answer2',
                                         deck_id: deck_private.id)
        get :index, params: { deck_id: card.deck_id, id: card.deck_id }
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content('question1')
        expect(response.body).to have_content('answer1')
        expect(response.body).to have_content('question2')
        expect(response.body).to have_content('answer2')
      end
    end

    context 'for deck collaborator' do
      include_context 'when collaborator logged in'

      it 'returns all cards' do
        deck_private = FactoryBot.create(:deck, :private, user_id: @user1.id)
        deck_private.collaborate(@collaborator, deck_private.user)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_private.id)
        card2 = FactoryBot.create(:card, front: 'question2', back: 'answer2',
                                         deck_id: deck_private.id)
        get :index, params: { deck_id: card.deck_id, id: card.deck_id }
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content('question1')
        expect(response.body).to have_content('answer1')
        expect(response.body).to have_content('question2')
        expect(response.body).to have_content('answer2')
      end
    end

    context 'for deck group member' do
      include_context 'when group member logged in'

      it 'returns all cards' do
        deck_private = FactoryBot.create(:deck, :private, user_id: @user1.id)
        deck_private.add_group(@group, deck_private.user)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_private.id)
        card2 = FactoryBot.create(:card, front: 'question2', back: 'answer2',
                                         deck_id: deck_private.id)
        get :index, params: { deck_id: card.deck_id, id: card.deck_id }
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content('question1')
        expect(response.body).to have_content('answer1')
        expect(response.body).to have_content('question2')
        expect(response.body).to have_content('answer2')
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns  404 error' do
        deck_private = FactoryBot.create(:deck, :private, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_private.id)
        card2 = FactoryBot.create(:card, front: 'question2', back: 'answer2',
                                         deck_id: deck_private.id)
        get :index, params: { deck_id: card.deck_id, id: card.deck_id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
        expect(response.body).to_not have_content('question1')
        expect(response.body).to_not have_content('answer1')
        expect(response.body).to_not have_content('question2')
        expect(response.body).to_not have_content('answer2')
      end
    end

    context 'when not logged in' do
      it 'returns  404 error' do
        deck_private = FactoryBot.create(:deck, :private, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_private.id)
        card2 = FactoryBot.create(:card, front: 'question2', back: 'answer2',
                                         deck_id: deck_private.id)
        get :index, params: { deck_id: card.deck_id, id: card.deck_id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
        expect(response.body).to_not have_content('question1')
        expect(response.body).to_not have_content('answer1')
        expect(response.body).to_not have_content('question2')
        expect(response.body).to_not have_content('answer2')
      end
    end
  end

  describe 'GET internal deck cards index' do
    render_views

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns all cards' do
        deck_internal = FactoryBot.create(:deck, :internal, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_internal.id)
        card2 = FactoryBot.create(:card, front: 'question2', back: 'answer2',
                                         deck_id: deck_internal.id)
        get :index, params: { deck_id: card.deck_id, id: card.deck_id }
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content('question1')
        expect(response.body).to have_content('answer1')
        expect(response.body).to have_content('question2')
        expect(response.body).to have_content('answer2')
      end
    end

    context 'for deck item owner' do
      include_context 'when deck item owner logged in'

      it 'returns all cards' do
        deck_internal = FactoryBot.create(:deck, :internal, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_internal.id)
        card2 = FactoryBot.create(:card, front: 'question2', back: 'answer2',
                                         deck_id: deck_internal.id)
        get :index, params: { deck_id: card.deck_id, id: card.deck_id }
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content('question1')
        expect(response.body).to have_content('answer1')
        expect(response.body).to have_content('question2')
        expect(response.body).to have_content('answer2')
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns  all cards' do
        deck_internal = FactoryBot.create(:deck, :internal, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_internal.id)
        card2 = FactoryBot.create(:card, front: 'question2', back: 'answer2',
                                         deck_id: deck_internal.id)
        get :index, params: { deck_id: card.deck_id, id: card.deck_id }
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content('question1')
        expect(response.body).to have_content('answer1')
        expect(response.body).to have_content('question2')
        expect(response.body).to have_content('answer2')
      end
    end

    context 'when not logged in' do
      it 'returns 404 template' do
        deck_internal = FactoryBot.create(:deck, :internal, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_internal.id)
        card2 = FactoryBot.create(:card, front: 'question2', back: 'answer2',
                                         deck_id: deck_internal.id)
        get :index, params: { deck_id: card.deck_id, id: card.deck_id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
        expect(response.body).to_not have_content('question1')
        expect(response.body).to_not have_content('answer1')
        expect(response.body).to_not have_content('question2')
        expect(response.body).to_not have_content('answer2')
      end
    end
  end

  describe 'GET public deck cards index' do
    render_views

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns all cards' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        card2 = FactoryBot.create(:card, front: 'question2', back: 'answer2',
                                  deck_id: deck_public.id)
        get :index, params: { deck_id: card.deck_id, id: card.deck_id }
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content('question1')
        expect(response.body).to have_content('answer1')
        expect(response.body).to have_content('question2')
        expect(response.body).to have_content('answer2')
      end
    end

    context 'for deck item owner' do
      include_context 'when deck item owner logged in'

      it 'returns all cards' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_public.id)
        card2 = FactoryBot.create(:card, front: 'question2', back: 'answer2',
                                         deck_id: deck_public.id)
        get :index, params: { deck_id: card.deck_id, id: card.deck_id }
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content('question1')
        expect(response.body).to have_content('answer1')
        expect(response.body).to have_content('question2')
        expect(response.body).to have_content('answer2')
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns  all cards' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_public.id)
        card2 = FactoryBot.create(:card, front: 'question2', back: 'answer2',
                                         deck_id: deck_public.id)
        get :index, params: { deck_id: card.deck_id, id: card.deck_id }
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content('question1')
        expect(response.body).to have_content('answer1')
        expect(response.body).to have_content('question2')
        expect(response.body).to have_content('answer2')
      end
    end

    context 'when not logged in' do
      it 'returns  all cards' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_public.id)
        card2 = FactoryBot.create(:card, front: 'question2', back: 'answer2',
                                         deck_id: deck_public.id)
        get :index, params: { deck_id: card.deck_id, id: card.deck_id }
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content('question1')
        expect(response.body).to have_content('answer1')
        expect(response.body).to have_content('question2')
        expect(response.body).to have_content('answer2')
      end
    end
  end

  describe 'GET show' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "returns 200 ok for another users private deck's card" do
        deck_private = FactoryBot.create(:deck, :private, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_private.id)
        get :show, params: { deck_id: card.deck_id, id: card.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for deck item owner' do
      include_context 'when deck item owner logged in'

      it "returns 200 ok for private deck's card" do
        deck_private = FactoryBot.create(:deck, :private, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_private.id)
        get :show, params: { deck_id: card.deck_id, id: card.id }
        expect(response).to have_http_status(:ok)
      end

      it "returns 200 ok for internal deck's card" do
        deck_internal = FactoryBot.create(:deck, :internal, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_internal.id)
        get :show, params: { deck_id: card.deck_id, id: card.id }
        expect(response).to have_http_status(:ok)
      end

      it "returns 200 ok for public deck's card" do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_public.id)
        get :show, params: { deck_id: card.deck_id, id: card.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for deck collaborator' do
      include_context 'when collaborator logged in'

      it "returns 200 ok for private deck's card" do
        deck_private = FactoryBot.create(:deck, :private, user_id: @user1.id)
        deck_private.collaborate(@collaborator, deck_private.user)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_private.id)
        get :show, params: { deck_id: card.deck_id, id: card.id }
        expect(response).to have_http_status(:ok)
      end

      it "returns 200 ok for internal deck's card" do
        deck_internal = FactoryBot.create(:deck, :internal, user_id: @user1.id)
        deck_internal.collaborate(@collaborator, deck_internal.user)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_internal.id)
        get :show, params: { deck_id: card.deck_id, id: card.id }
        expect(response).to have_http_status(:ok)
      end

      it "returns 200 ok for public deck's card" do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        deck_public.collaborate(@collaborator, deck_public.user)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_public.id)
        get :show, params: { deck_id: card.deck_id, id: card.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for deck group member' do
      include_context 'when group member logged in'

      it "returns 200 ok for private deck's card" do
        deck_private = FactoryBot.create(:deck, :private, user_id: @user1.id)
        deck_private.add_group(@group, deck_private.user)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_private.id)
        get :show, params: { deck_id: card.deck_id, id: card.id }
        expect(response).to have_http_status(:ok)
      end

      it "returns 200 ok for internal deck's card" do
        deck_internal = FactoryBot.create(:deck, :internal, user_id: @user1.id)
        deck_internal.add_group(@group, deck_internal.user)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_internal.id)
        get :show, params: { deck_id: card.deck_id, id: card.id }
        expect(response).to have_http_status(:ok)
      end

      it "returns 200 ok for public deck's card" do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        deck_public.add_group(@group, deck_public.user)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_public.id)
        get :show, params: { deck_id: card.deck_id, id: card.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it "returns a 404 for a private deck's card" do
        deck_private = FactoryBot.create(:deck, :private, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_private.id)
        get :show, params: { deck_id: card.deck_id, id: card.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it "returns 200 ok for internal deck's card" do
        deck_internal = FactoryBot.create(:deck, :internal, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_internal.id)
        get :show, params: { deck_id: card.deck_id, id: card.id }
        expect(response).to have_http_status(:ok)
      end

      it "returns 200 ok for public deck's card" do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_public.id)
        get :show, params: { deck_id: card.deck_id, id: card.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when not logged in' do
      it "returns a 404 for a private deck's card" do
        deck_private = FactoryBot.create(:deck, :private, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_private.id)
        get :show, params: { deck_id: card.deck_id, id: card.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it "returns 404 for internal deck's card" do
        deck_internal = FactoryBot.create(:deck, :internal, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_internal.id)
        get :show, params: { deck_id: card.deck_id, id: card.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it "returns 200 ok for public deck's card" do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_public.id)
        get :show, params: { deck_id: card.deck_id, id: card.id }
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'GET new' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns 302 found redirect for deck that user did not create' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        get :new, params: { deck_id: deck_public.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for deck item owner' do
      include_context 'when deck item owner logged in'

      it 'returns 200 ok' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        get :new, params: { deck_id: deck_public.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns 302 found redirect' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        get :new, params: { deck_id: deck_public.id }
        expect(response).to have_http_status(:found)
      end
    end

    context 'when not logged in' do
      it 'returns 302 found redirect' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        get :new, params: { deck_id: deck_public.id }
        expect(response).to have_http_status(:found)
      end
    end
  end

  describe 'GET edit' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns 200 ok' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_public.id)
        get :edit, params: { deck_id: card.deck_id, id: card.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for deck item owner' do
      include_context 'when deck item owner logged in'

      it 'returns 200 ok' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_public.id)
        get :edit, params: { deck_id: card.deck_id, id: card.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for deck collaborator' do
      include_context 'when collaborator logged in'

      it 'returns 200 ok' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        deck_public.collaborate(@collaborator, deck_public.user)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_public.id)
        get :edit, params: { deck_id: card.deck_id, id: card.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for deck group member' do
      include_context 'when group member logged in'

      it 'returns 200 ok' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        deck_public.add_group(@group, deck_public.user)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_public.id)
        get :edit, params: { deck_id: card.deck_id, id: card.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns 302 found redirect' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_public.id)
        get :edit, params: { deck_id: card.deck_id, id: card.id }
        expect(response).to have_http_status(:found)
      end
    end

    context 'when not logged in' do
      it 'returns 302 found redirect' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_public.id)
        get :edit, params: { deck_id: card.deck_id, id: card.id }
        expect(response).to have_http_status(:found)
      end
    end
  end

  describe 'POST create' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'creates new card with valid data' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        expect do
          post :create, params: {
              deck_id: deck_public.id, card: { front: 'my question',
              back: 'my answer' } }
        end.to change(Card, :count).by(1)
      end

      it 'does not create a new card with invalid data' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        expect do
          post :create, params: {
              deck_id: deck_public.id, card: { front: 'my question' } }
        end.to change(Card, :count).by(0)
      end
    end

    context 'for deck item owner' do
      include_context 'when deck item owner logged in'

      it 'creates new card with valid data' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        expect do
          post :create, params: {
              deck_id: deck_public.id, card: { front: 'my question',
              back: 'my answer' } }
        end.to change(Card, :count).by(1)
      end

      it 'does not create a new card with invalid data' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        expect do
          post :create, params: {
              deck_id: deck_public.id, card: { front: 'my question' } }
        end.to change(Card, :count).by(0)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'does not create a new card with valid data' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        expect do
          post :create, params: {
              deck_id: deck_public.id, card: { front: 'my question',
              back: 'my answer' } }
        end.to change(Card, :count).by(0)
      end
    end

    context 'when not logged in' do
      it 'does not create a new card' do
        deck_public = FactoryBot.create(:deck, :public)
        expect do
          post :create, params: {
              deck_id: deck_public.id, card: { front: 'my question',
              back: 'my answer' } }
        end.to change(Card, :count).by(0)
      end
    end
  end

  describe 'PATCH update' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "updates another user's deck's card with valid data" do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_public.id)
        patch :update, params: { deck_id: deck_public.id, id: card.id,
                                 card: { front: 'updated_text' } }
        expect(response).to redirect_to(deck_card_path(deck_id: deck_public.id,
                                                       id: card.id))
      end

      it "does not update user's deck's card with invalid data" do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_public.id)
        patch :update, params: { deck_id: deck_public.id, id: card.id,
                                 card: { front: '' } }
        expect(response).to have_http_status(422)
      end
    end

    context 'for deck item owner' do
      include_context 'when deck item owner logged in'

      it 'updates card with valid data' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_public.id)
        patch :update, params: { deck_id: deck_public.id, id: card.id,
                                 card: { front: 'updated_text' } }
        expect(response).to redirect_to(deck_card_path(deck_id: deck_public.id,
                                                       id: card.id))
      end

      it 'does not update card with invalid data' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        patch :update, params: { deck_id: deck_public.id, id: card.id,
                                 card: { front: '' } }
        expect(response).to have_http_status(422)
      end
    end

    context 'for deck collaborator' do
      include_context 'when collaborator logged in'

      it 'updates card with valid data' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        deck_public.collaborate(@collaborator,deck_public.user)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_public.id)
        patch :update, params: { deck_id: deck_public.id, id: card.id,
                                 card: { front: 'updated_text' } }
        expect(response).to redirect_to(deck_card_path(deck_id: deck_public.id,
                                                       id: card.id))
      end

      it 'does not update card with invalid data' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        deck_public.collaborate(@collaborator, deck_public.user)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        patch :update, params: { deck_id: deck_public.id, id: card.id,
                                 card: { front: '' } }
        expect(response).to have_http_status(422)
      end
    end

    context 'for deck group member' do
      include_context 'when group member logged in'

      it 'updates card with valid data' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        deck_public.add_group(@group, deck_public.user)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_public.id)
        patch :update, params: { deck_id: deck_public.id, id: card.id,
                                 card: { front: 'updated_text' } }
        expect(response).to redirect_to(deck_card_path(deck_id: deck_public.id,
                                                       id: card.id))
      end

      it 'does not update card with invalid data' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        deck_public.add_group(@group, deck_public.user)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        patch :update, params: { deck_id: deck_public.id, id: card.id,
                                 card: { front: '' } }
        expect(response).to have_http_status(422)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'does not update card' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        patch :update, params: { deck_id: deck_public.id, id: card.id,
                                 card: { front: 'updated_text' } }
        expect(response).to redirect_to(decks_path)
      end
    end

    context 'when not logged in' do
      it 'does not update card' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_public.id)
        patch :update, params: { deck_id: deck_public.id, id: card.id,
                                 card: { front: 'updated_text' } }
        expect(response).to redirect_to(user_session_path)
      end
    end
  end

  describe 'PUT update' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "updates another user's deck's card with valid data" do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        put :update,
            params: { deck_id: deck_public.id, id: card.id, card:
                { front: 'updated_text', back: 'updated_answer' } }
        expect(response).to redirect_to(deck_card_path(deck_id: deck_public.id,
                                                                id: card.id))
      end

      it 'does not card with invalid data' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        put :update, params: { deck_id: deck_public.id, id: card.id,
                               card: { front: '', back: 'updated_answer' } }
        expect(response).to have_http_status(422)
      end
    end

    context 'for deck item owner' do
      include_context 'when deck item owner logged in'

      it 'updates card with valid data' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        put :update,
            params: { deck_id: deck_public.id, id: card.id,
                      card: { front: 'updated_text', back: 'updated_answer' } }
        expect(response).to redirect_to(deck_card_path(deck_id: deck_public.id,
                                                       id: card.id))
      end

      it 'does not update card with invalid data' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_public.id)
        put :update, params: { deck_id: deck_public.id, id: card.id,
                               card: { front: '', back: 'updated_answer' } }
        expect(response).to have_http_status(422)
      end
    end

    context 'for deck collaborator' do
      include_context 'when collaborator logged in'

      it 'updates card with valid data' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        deck_public.collaborate(@collaborator, deck_public.user)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        put :update,
            params: { deck_id: deck_public.id, id: card.id,
                      card: { front: 'updated_text', back: 'updated_answer' } }
        expect(response).to redirect_to(deck_card_path(deck_id: deck_public.id,
                                                       id: card.id))
      end

      it 'does not update card with invalid data' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        deck_public.collaborate(@collaborator, deck_public.user)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_public.id)
        put :update, params: { deck_id: deck_public.id, id: card.id,
                               card: { front: '', back: 'updated_answer' } }
        expect(response).to have_http_status(422)
      end
    end

    context 'for deck group member' do
      include_context 'when group member logged in'

      it 'updates card with valid data' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        deck_public.add_group(@group, deck_public.user)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        put :update,
            params: { deck_id: deck_public.id, id: card.id,
                      card: { front: 'updated_text', back: 'updated_answer' } }
        expect(response).to redirect_to(deck_card_path(deck_id: deck_public.id,
                                                       id: card.id))
      end

      it 'does not update card with invalid data' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        deck_public.add_group(@group, deck_public.user)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                        deck_id: deck_public.id)
        put :update, params: { deck_id: deck_public.id, id: card.id,
                               card: { front: '', back: 'updated_answer' } }
        expect(response).to have_http_status(422)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'does not update card' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        put :update,
            params: { deck_id: deck_public.id, id: card.id, card: {
                front: 'updated_text', back: 'updated_answer' } }
        expect(response).to redirect_to(decks_path)
      end
    end

    context 'when not logged in' do
      it 'does not update card' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        put :update,
            params: { deck_id: deck_public.id, id: card.id, card:
                { front: 'updated_text', back: 'updated_answer' } }
        expect(response).to redirect_to(user_session_path)
      end
    end
  end

  describe 'DELETE destroy' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "destroys a user's deck's card" do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        expect do
          delete :destroy, params: { deck_id: deck_public.id, id: card.id }
        end.to change(Card, :count).by(-1)
      end

      it 'redirects to decks index' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        delete :destroy, params: { deck_id: deck_public.id, id: card.id }
        expect(response).to redirect_to(deck_cards_path(deck_public.id))
      end
    end

    context 'for deck item owner' do
      include_context 'when deck item owner logged in'

      it 'destroys a card' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        expect do
          delete :destroy, params: { deck_id: deck_public.id, id: card.id }
        end.to change(Card, :count).by(-1)
      end
    end

    context 'for deck collaborator' do
      include_context 'when collaborator logged in'

      it 'does not destroy card' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        deck_public.collaborate(@collaborator, deck_public.user)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                  deck_id: deck_public.id)
        expect do
        delete :destroy, params: { deck_id: deck_public.id, id: card.id }
        end.to change(Card, :count).by(0)
      end
    end

    context 'for deck group member' do
      include_context 'when group member logged in'

      it 'does not destroy card' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        deck_public.add_group(@group, deck_public.user)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                  deck_id: deck_public.id)
        expect do
        delete :destroy, params: { deck_id: deck_public.id, id: card.id }
        end.to change(Card, :count).by(0)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'does not destroy card' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        expect do
          delete :destroy, params: { deck_id: deck_public.id, id: card.id }
        end.to change(Card, :count).by(0)
      end

      it 'redirects to decks index' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        delete :destroy, params: { deck_id: deck_public.id, id: card.id }
        expect(response).to redirect_to(decks_path)
      end
    end

    context 'when not logged in' do
      it "does not destroy a user's deck's card" do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        expect do
          delete :destroy, params: { deck_id: deck_public.id, id: card.id }
        end.to change(Card, :count).by(0)
      end

      it 'redirects to sign in' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        delete :destroy, params: { deck_id: deck_public.id, id: card.id }
        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end

  describe 'PATCH update_position' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "updates a user's card position" do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card1 = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        card2 = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                  deck_id: deck_public.id)
        expect(card1.reload.display_position).to eq(0)
        expect(card2.reload.display_position).to eq(1)
        patch :update_position, params: { deck_id: deck_public.id, card_id: card1.id, display_position: '1' }
        expect(card1.reload.display_position).to eq(1)
        expect(card2.reload.display_position).to eq(0)
      end
    end

    context 'for deck item owner' do
      include_context 'when deck item owner logged in'

      it "updates a user's card position" do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        card1 = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        card2 = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                  deck_id: deck_public.id)
        expect(card1.reload.display_position).to eq(0)
        expect(card2.reload.display_position).to eq(1)
        patch :update_position, params: { deck_id: deck_public.id, card_id: card1.id, display_position: '1' }
        expect(card1.reload.display_position).to eq(1)
        expect(card2.reload.display_position).to eq(0)
      end
    end

    context 'for deck collaborator' do
      include_context 'when collaborator logged in'

      it "updates a user's card position" do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        deck_public.collaborate(@collaborator, deck_public.user)
        card1 = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        card2 = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                  deck_id: deck_public.id)
        expect(card1.reload.display_position).to eq(0)
        expect(card2.reload.display_position).to eq(1)
        patch :update_position, params: { deck_id: deck_public.id, card_id: card1.id, display_position: '1' }
        expect(card1.reload.display_position).to eq(1)
        expect(card2.reload.display_position).to eq(0)
      end
    end

    context 'for deck group member' do
      include_context 'when group member logged in'

      it "updates a user's card position" do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        deck_public.add_group(@group, deck_public.user)
        card1 = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        card2 = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                  deck_id: deck_public.id)
        expect(card1.reload.display_position).to eq(0)
        expect(card2.reload.display_position).to eq(1)
        patch :update_position, params: { deck_id: deck_public.id, card_id: card1.id, display_position: '1' }
        expect(card1.reload.display_position).to eq(1)
        expect(card2.reload.display_position).to eq(0)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it "does not update a user's card position" do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        deck_public.add_group(@group, deck_public.user)
        card1 = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        card2 = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                  deck_id: deck_public.id)
        expect(card1.reload.display_position).to eq(0)
        expect(card2.reload.display_position).to eq(1)
        patch :update_position, params: { deck_id: deck_public.id, card_id: card1.id, display_position: '1' }
        expect(card1.reload.display_position).to eq(0)
        expect(card2.reload.display_position).to eq(1)
      end

      it 'redirects to decks index' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        deck_public.add_group(@group, deck_public.user)
        card1 = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        card2 = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                  deck_id: deck_public.id)
        expect(card1.reload.display_position).to eq(0)
        expect(card2.reload.display_position).to eq(1)
        patch :update_position, params: { deck_id: deck_public.id, card_id: card1.id, display_position: '1' }
        expect(response).to redirect_to(decks_path)
        expect(flash[:alert]).to match(/Not authorized./)
      end
    end

    context 'when not logged in' do
      it "does not update a user's card position" do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        deck_public.add_group(@group, deck_public.user)
        card1 = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        card2 = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                  deck_id: deck_public.id)
        expect(card1.reload.display_position).to eq(0)
        expect(card2.reload.display_position).to eq(1)
        patch :update_position, params: { deck_id: deck_public.id, card_id: card1.id, display_position: '1' }
        expect(card1.reload.display_position).to eq(0)
        expect(card2.reload.display_position).to eq(1)
      end

      it 'redirects to decks index' do
        deck_public = FactoryBot.create(:deck, :public, user_id: @user1.id)
        deck_public.add_group(@group, deck_public.user)
        card1 = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                 deck_id: deck_public.id)
        card2 = FactoryBot.create(:card, front: 'question1', back: 'answer1',
                                  deck_id: deck_public.id)
        expect(card1.reload.display_position).to eq(0)
        expect(card2.reload.display_position).to eq(1)
        patch :update_position, params: { deck_id: deck_public.id, card_id: card1.id, display_position: '1' }
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end
end

