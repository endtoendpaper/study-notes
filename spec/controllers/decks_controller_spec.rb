require 'rails_helper'

RSpec.describe DecksController, type: :controller do
  before(:each) do
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group.add_member(@group_member, @group.user)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
  end

  shared_context 'when admin logged in' do
    before(:each) do
      sign_in @admin
    end
  end

  shared_context 'when deck item owner logged in' do
    before(:each) do
      sign_in @owner
    end
  end

  shared_context 'when collaborator logged in' do
    before(:each) do
      sign_in @collaborator
    end
  end

  shared_context 'when group member logged in' do
    before(:each) do
      sign_in @group_member
    end
  end

  shared_context 'when user logged in' do
    before(:each) do
      sign_in @user
    end
  end

  describe 'GET index' do
    render_views

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns all decks for other users' do
        reset_elasticsearch_indices(['decks'])

        deck_public1 = FactoryBot.create(:deck, :public, user_id: @owner.id)
        deck_internal1 = FactoryBot.create(:deck, :internal, user_id: @owner.id)
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        deck_public2 = FactoryBot.create(:deck, :public, user_id: @user.id)
        deck_internal2 = FactoryBot.create(:deck, :internal, user_id: @user.id)
        deck_private2 = FactoryBot.create(:deck, :private, user_id: @user.id)

        reindex_elasticsearch_data(['decks'])

        get :index
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content(deck_public1.title)
        expect(response.body).to have_content(deck_internal1.title)
        expect(response.body).to have_content(deck_private1.title)
        expect(response.body).to have_content(deck_public2.title)
        expect(response.body).to have_content(deck_internal2.title)
        expect(response.body).to have_content(deck_private2.title)
      end
    end

    context 'for deck item owner' do
      include_context 'when deck item owner logged in'

      it "returns public, internal, and only the user's private and collaborated decks" do
        reset_elasticsearch_indices(['decks'])

        deck_public1 = FactoryBot.create(:deck, :public, user_id: @owner.id)
        deck_internal1 = FactoryBot.create(:deck, :internal, user_id: @owner.id)
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        deck_public2 = FactoryBot.create(:deck, :public, user_id: @user.id)
        deck_internal2 = FactoryBot.create(:deck, :internal, user_id: @user.id)
        deck_private2 = FactoryBot.create(:deck, :private, user_id: @user.id)

        reindex_elasticsearch_data(['decks'])

        get :index
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content(deck_public1.title)
        expect(response.body).to have_content(deck_internal1.title)
        expect(response.body).to have_content(deck_private1.title)
        expect(response.body).to have_content(deck_public2.title)
        expect(response.body).to have_content(deck_internal2.title)
        expect(response.body).to_not have_content(deck_private2.title)
      end
    end

    context 'for deck collaborator' do
      include_context 'when collaborator logged in'

      it "returns public, internal, and only the user's private and collaborated decks" do
        reset_elasticsearch_indices(['decks'])

        deck_public1 = FactoryBot.create(:deck, :public, user_id: @owner.id)
        deck_internal1 = FactoryBot.create(:deck, :internal, user_id: @owner.id)
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        deck_public2 = FactoryBot.create(:deck, :public, user_id: @user.id)
        deck_internal2 = FactoryBot.create(:deck, :internal, user_id: @user.id)
        deck_private2 = FactoryBot.create(:deck, :private, user_id: @user.id)
        deck_private1.collaborate(@collaborator, deck_private1.user)

        reindex_elasticsearch_data(['decks'])

        get :index
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content(deck_public1.title)
        expect(response.body).to have_content(deck_internal1.title)
        expect(response.body).to have_content(deck_private1.title)
        expect(response.body).to have_content(deck_public2.title)
        expect(response.body).to have_content(deck_internal2.title)
        expect(response.body).to_not have_content(deck_private2.title)
      end
    end

    context 'for deck group member' do
      include_context 'when group member logged in'

      it "returns public, internal, and only the user's private and collaborated decks" do
        reset_elasticsearch_indices(['decks'])

        deck_public1 = FactoryBot.create(:deck, :public, user_id: @owner.id)
        deck_internal1 = FactoryBot.create(:deck, :internal, user_id: @owner.id)
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        deck_public2 = FactoryBot.create(:deck, :public, user_id: @user.id)
        deck_internal2 = FactoryBot.create(:deck, :internal, user_id: @user.id)
        deck_private2 = FactoryBot.create(:deck, :private, user_id: @user.id)
        deck_private1.add_group(@group, deck_private1.user)

        reindex_elasticsearch_data(['decks'])

        get :index
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content(deck_public1.title)
        expect(response.body).to have_content(deck_internal1.title)
        expect(response.body).to have_content(deck_private1.title)
        expect(response.body).to have_content(deck_public2.title)
        expect(response.body).to have_content(deck_internal2.title)
        expect(response.body).to_not have_content(deck_private2.title)
      end
    end

    context 'when not logged in' do
      it 'returns only public decks' do
        reset_elasticsearch_indices(['decks'])

        deck_public1 = FactoryBot.create(:deck, :public, user_id: @owner.id)
        deck_internal1 = FactoryBot.create(:deck, :internal, user_id: @owner.id)
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        deck_public2 = FactoryBot.create(:deck, :public, user_id: @user.id)
        deck_internal2 = FactoryBot.create(:deck, :internal, user_id: @user.id)
        deck_private2 = FactoryBot.create(:deck, :private, user_id: @user.id)

        reindex_elasticsearch_data(['decks'])

        get :index
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content(deck_public1.title)
        expect(response.body).to_not have_content(deck_internal1.title)
        expect(response.body).to_not have_content(deck_private1.title)
        expect(response.body).to have_content(deck_public2.title)
        expect(response.body).to_not have_content(deck_internal2.title)
        expect(response.body).to_not have_content(deck_private2.title)
      end
    end
  end

  describe 'GET show' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns 200 ok for another users private deck' do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        get :show, params: { id: deck_private1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for deck item owner' do
      include_context 'when deck item owner logged in'

      it 'returns 200 ok for private deck' do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        get :show, params: { id: deck_private1.id }
        expect(response).to have_http_status(:ok)
      end

      it 'returns 200 ok for internal deck' do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        get :show, params: { id: deck_private1.id }
        expect(response).to have_http_status(:ok)
      end

      it 'returns 200 ok for public deck' do
        deck_public1 = FactoryBot.create(:deck, :public, user_id: @owner.id)
        get :show, params: { id: deck_public1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for deck collaborator' do
      include_context 'when collaborator logged in'

      it 'returns 200 ok for private deck' do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        deck_private1.collaborate(@collaborator, deck_private1.user)
        get :show, params: { id: deck_private1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for deck group member' do
      include_context 'when group member logged in'

      it 'returns 200 ok for private deck' do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        deck_private1.add_group(@group, deck_private1.user)
        get :show, params: { id: deck_private1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns a 404 for a private deck' do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        get :show, params: { id: deck_private1.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it 'returns 200 ok for internal deck' do
        deck_internal1 = FactoryBot.create(:deck, :internal, user_id: @owner.id)
        get :show, params: { id: deck_internal1.id }
        expect(response).to have_http_status(:ok)
      end

      it 'returns 200 ok for public deck' do
        deck_public1 = FactoryBot.create(:deck, :public, user_id: @owner.id)
        get :show, params: { id: deck_public1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when not logged in' do
      it 'returns a 404 for a private deck' do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        get :show, params: { id: deck_private1.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it 'returns 404 for internal deck' do
        deck_internal1 = FactoryBot.create(:deck, :internal, user_id: @owner.id)
        get :show, params: { id: deck_internal1.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it 'returns 200 ok for public deck' do
        deck_public1 = FactoryBot.create(:deck, :public, user_id: @owner.id)
        get :show, params: { id: deck_public1.id }
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'GET new' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns 200 ok' do
        get :new
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for deck item owner' do
      include_context 'when deck item owner logged in'

      it 'returns 200 ok' do
        get :new
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns 200 ok' do
        get :new
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when not logged in' do
      it 'returns 302 found redirect' do
        get :new
        expect(response).to have_http_status(:found)
      end
    end
  end

  describe 'GET edit' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "returns 200 ok for another user's deck" do
        deck_public1 = FactoryBot.create(:deck, :public, user_id: @owner.id)
        get :edit, params: { id: deck_public1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for deck item owner' do
      include_context 'when deck item owner logged in'

      it 'returns 200 ok' do
        deck_public1 = FactoryBot.create(:deck, :public, user_id: @owner.id)
        get :edit, params: { id: deck_public1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for deck collaborator' do
      include_context 'when collaborator logged in'

      it 'returns 200 ok for private deck' do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        deck_private1.collaborate(@collaborator, deck_private1.user)
        get :edit, params: { id: deck_private1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for deck group member' do
      include_context 'when group member logged in'

      it 'returns 200 ok for private deck' do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        deck_private1.add_group(@group, deck_private1.user)
        get :edit, params: { id: deck_private1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns 302 found redirect' do
        deck_public1 = FactoryBot.create(:deck, :public, user_id: @owner.id)
        get :edit, params: { id: deck_public1.id }
        expect(response).to have_http_status(:found)
      end
    end

    context 'when not logged in' do
      it 'returns 302 found redirect' do
        deck_public1 = FactoryBot.create(:deck, :public, user_id: @owner.id)
        get :edit, params: { id: deck_public1.id }
        expect(response).to have_http_status(:found)
      end
    end
  end

  describe 'POST create' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'creates new deck with valid data' do
        expect do
          post :create,
               params: { deck: { title: 'my title',
                                 description: 'lots of decks', visibility: 0,
                                 user_id: @admin.id } }
        end.to change(Deck, :count).by(1)
      end

      it 'does not create a new deck with invalid data' do
        expect do
          post :create, params: { deck: { description: 'lots of deck',
                                          visibility: 0, user_id: @admin.id } }
        end.to change(Deck, :count).by(0)
      end
    end

    context 'for deck item owner' do
      include_context 'when deck item owner logged in'
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'creates new deck with valid data' do
        expect do
          post :create,
               params: { deck: { title: 'my title',
                                 description: 'lots of decks', visibility: 0,
                                 user_id: @owner.id } }
        end.to change(Deck, :count).by(1)
      end

      it 'does not create a new deck with inalid data' do
        expect do
          post :create, params: { deck: { description: 'lots of decks',
                                  visibility: 0, user_id: @owner.id } }
        end.to change(Deck, :count).by(0)
      end
    end

    context 'when not logged in' do
      it 'does not create a new deck if not logged in' do
        expect do
          post :create,
               params: { deck: { title: 'my title',
                                 description: 'lots of decks', visibility: 0,
                                 user_id: @owner.id } }
        end.to change(Deck, :count).by(0)
      end
    end
  end

  describe 'PATCH update' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "updates another user's deck with valid data" do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        patch :update, params: { id: deck_private1.id,
                                 deck: { title: 'updated_text' } }
        expect(response).to redirect_to(deck_private1)
        deck_private1.reload
        expect(deck_private1.title).to eq('updated_text')
      end

      it "does not update another user's deck with invalid data" do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        patch :update, params: { id: deck_private1.id, deck: { title: nil } }
        deck_private1.reload
        expect(deck_private1.title).to_not be_nil
      end
    end

    context 'for deck item owner' do
      include_context 'when deck item owner logged in'

      it "updates user's deck with valid data" do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        patch :update, params: { id: deck_private1.id,
                                     deck: { title: 'updated_text' } }
        expect(response).to redirect_to(deck_private1)
        deck_private1.reload
        expect(deck_private1.title).to eq('updated_text')
      end

      it "does not update user's deck with invalid data" do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        sign_in @owner
        patch :update, params: { id: deck_private1.id, deck: { title: nil } }
        deck_private1.reload
        expect(deck_private1.title).to_not be_nil
      end
    end

    context 'for deck collaborator' do
      include_context 'when collaborator logged in'

      it "updates collaborated deck with valid data" do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        deck_private1.collaborate(@collaborator, deck_private1.user)
        patch :update, params: { id: deck_private1.id,
                                     deck: { title: 'updated_text' } }
        expect(response).to redirect_to(deck_private1)
        deck_private1.reload
        expect(deck_private1.title).to eq('updated_text')
      end

      it "does not update collaborated deck with invalid data" do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        deck_private1.collaborate(@collaborator, deck_private1.user)
        patch :update, params: { id: deck_private1.id, deck: { title: nil } }
        deck_private1.reload
        expect(deck_private1.title).to_not be_nil
      end
    end

    context 'for deck group member' do
      include_context 'when group member logged in'

      it "updates collaborated deck with valid data" do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        deck_private1.add_group(@group, deck_private1.user)
        patch :update, params: { id: deck_private1.id,
                                     deck: { title: 'updated_text' } }
        expect(response).to redirect_to(deck_private1)
        deck_private1.reload
        expect(deck_private1.title).to eq('updated_text')
      end

      it "does not update collaborated deck with invalid data" do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        deck_private1.add_group(@group, deck_private1.user)
        patch :update, params: { id: deck_private1.id, deck: { title: nil } }
        deck_private1.reload
        expect(deck_private1.title).to_not be_nil
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it "does not update another user's deck" do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        patch :update, params: { id: deck_private1.id,
                                     deck: { title: 'updated_text' } }
        expect(response).to redirect_to(decks_path)
        deck_private1.reload
        expect(deck_private1.title).to_not eq('updated_text')
      end
    end

    context 'when not logged in' do
      it 'does not update another deck' do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        patch :update, params: { id: deck_private1.id,
                                 deck: { title: 'updated_text' } }
        expect(response).to redirect_to(new_user_session_path)
        deck_private1.reload
        expect(deck_private1.title).to_not eq('updated_text')
      end
    end
  end

  describe 'PUT update' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "updates another user's deck with valid data" do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        put :update, params: { id: deck_private1.id,
                               deck: { title: 'updated_text' } }
        expect(response).to redirect_to(deck_private1)
        deck_private1.reload
        expect(deck_private1.title).to eq('updated_text')
      end

      it "does not update another user's deck with invalid data" do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        put :update, params: { id: deck_private1.id, deck: { title: nil } }
        deck_private1.reload
        expect(deck_private1.title).to_not be_nil
      end
    end

    context 'for deck item owner' do
      include_context 'when deck item owner logged in'

      it "updates user's deck with valid data" do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        put :update, params: { id: deck_private1.id,
                               deck: { title: 'updated_text' } }
        expect(response).to redirect_to(deck_private1)
        deck_private1.reload
        expect(deck_private1.title).to eq('updated_text')
      end

      it "does not update user's deck with invalid data" do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        sign_in @owner
        put :update, params: { id: deck_private1.id, deck: { title: nil } }
        deck_private1.reload
        expect(deck_private1.title).to_not be_nil
      end
    end

    context 'for deck collaborator' do
      include_context 'when collaborator logged in'

      it "updates collaborated deck with valid data" do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        deck_private1.collaborate(@collaborator, deck_private1.user)
        put :update, params: { id: deck_private1.id,
                                     deck: { title: 'updated_text' } }
        expect(response).to redirect_to(deck_private1)
        deck_private1.reload
        expect(deck_private1.title).to eq('updated_text')
      end

      it "does not update collaborated deck with invalid data" do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        deck_private1.collaborate(@collaborator, deck_private1.user)
        put :update, params: { id: deck_private1.id, deck: { title: nil } }
        deck_private1.reload
        expect(deck_private1.title).to_not be_nil
      end
    end

    context 'for deck group member' do
      include_context 'when group member logged in'

      it "updates collaborated deck with valid data" do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        deck_private1.add_group(@group, deck_private1.user)
        put :update, params: { id: deck_private1.id,
                                     deck: { title: 'updated_text' } }
        expect(response).to redirect_to(deck_private1)
        deck_private1.reload
        expect(deck_private1.title).to eq('updated_text')
      end

      it "does not update collaborated deck with invalid data" do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        deck_private1.add_group(@group, deck_private1.user)
        put :update, params: { id: deck_private1.id, deck: { title: nil } }
        deck_private1.reload
        expect(deck_private1.title).to_not be_nil
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it "does not update another user's deck" do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        put :update, params: { id: deck_private1.id,
                              deck: { title: 'updated_text' } }
        expect(response).to redirect_to(decks_path)
        deck_private1.reload
        expect(deck_private1.title).to_not eq('updated_text')
      end
    end

    context 'when not logged in' do
      it "does not update user's deck" do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        put :update, params: { id: deck_private1.id,
                               deck: { title: 'updated_text' } }
        expect(response).to redirect_to(new_user_session_path)
        deck_private1.reload
        expect(deck_private1.title).to_not eq('updated_text')
      end
    end
  end

  describe 'DELETE destroy' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "destroys a user's deck" do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        expect do
          delete :destroy, params: { id: deck_private1.id }
        end.to change(Deck, :count).by(-1)
      end

      it 'redirects to decks index' do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        delete :destroy, params: { id: deck_private1.id }
        expect(response).to redirect_to(decks_path)
      end
    end

    context 'for deck item owner' do
      include_context 'when deck item owner logged in'

      it "destroys a user's deck" do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        expect do
          delete :destroy, params: { id: deck_private1.id }
        end.to change(Deck, :count).by(-1)
      end
    end

    context 'for deck collaborator' do
      include_context 'when collaborator logged in'

      it 'redirects to decks index' do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        deck_private1.collaborate(@collaborator, deck_private1.user)
        expect do
          delete :destroy, params: { id: deck_private1.id }
        end.to change(Deck, :count).by(0)
        expect(response).to redirect_to(decks_path)
      end
    end

    context 'for deck group member' do
      include_context 'when group member logged in'

      it 'redirects to decks index' do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        deck_private1.add_group(@group, deck_private1.user)
        expect do
          delete :destroy, params: { id: deck_private1.id }
        end.to change(Deck, :count).by(0)
        expect(response).to redirect_to(decks_path)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'redirects to decks index' do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        delete :destroy, params: { id: deck_private1.id }
        expect(response).to redirect_to(decks_path)
      end

      it "does not destroy another user's deck" do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        expect do
          delete :destroy, params: { id: deck_private1.id }
        end.to change(Deck, :count).by(0)
      end
    end

    context 'when not logged in' do
      it "does not destroy a user's deck" do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        expect do
          delete :destroy, params: { id: deck_private1.id }
        end.to change(Deck, :count).by(0)
      end

      it 'redirects to decks index' do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        delete :destroy, params: { id: deck_private1.id }
        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end

  describe 'POST shuffle' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "updates a user's deck" do
        deck_private1 = FactoryBot.create(:deck, :private, :two_cards, user_id: @owner.id)
        post :shuffle, params: { deck_id: deck_private1.id }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(deck_cards_path(deck_private1))
        expect(flash[:notice]).to match(/Deck was successfully shuffled./)
      end
    end

    context 'for deck item owner' do
      include_context 'when deck item owner logged in'

      it "updates a user's deck" do
        deck_private1 = FactoryBot.create(:deck, :private, :two_cards, user_id: @owner.id)
        post :shuffle, params: { deck_id: deck_private1.id }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(deck_cards_path(deck_private1))
        expect(flash[:notice]).to match(/Deck was successfully shuffled./)
      end
    end

    context 'for deck collaborator' do
      include_context 'when collaborator logged in'

      it "updates a user's deck" do
        deck_private1 = FactoryBot.create(:deck, :private, :two_cards, user_id: @owner.id)
        deck_private1.collaborate(@collaborator, deck_private1.user)
        post :shuffle, params: { deck_id: deck_private1.id }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(deck_cards_path(deck_private1))
        expect(flash[:notice]).to match(/Deck was successfully shuffled./)
      end
    end

    context 'for deck group member' do
      include_context 'when group member logged in'

      it "updates a user's deck" do
        deck_private1 = FactoryBot.create(:deck, :private, :two_cards, user_id: @owner.id)
        deck_private1.add_group(@group, deck_private1.user)
        post :shuffle, params: { deck_id: deck_private1.id }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(deck_cards_path(deck_private1))
        expect(flash[:notice]).to match(/Deck was successfully shuffled./)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'redirects to decks index' do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        post :shuffle, params: { deck_id: deck_private1.id }
        expect(response).to redirect_to(decks_path)
        expect(flash[:alert]).to match(/Not authorized./)
      end
    end

    context 'when not logged in' do
      it 'redirects to login' do
        deck_private1 = FactoryBot.create(:deck, :private, user_id: @owner.id)
        post :shuffle, params: { deck_id: deck_private1.id }
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end
end
