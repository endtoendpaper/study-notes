require 'rails_helper'

RSpec.describe NotesController, type: :controller do
  before(:each) do
    @user1 = FactoryBot.create(:user, :confirmed)
    @user2 = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group.add_member(@group_member, @group.user)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
  end

  shared_context 'when admin logged in' do
    before(:each) do
      sign_in @admin
    end
  end

  shared_context 'when note item owner logged in' do
    before(:each) do
      sign_in @user1
    end
  end

  shared_context 'when collaborator logged in' do
    before(:each) do
      sign_in @collaborator
    end
  end

  shared_context 'when group member logged in' do
    before(:each) do
      sign_in @group_member
    end
  end

  shared_context 'when user logged in' do
    before(:each) do
      sign_in @user2
    end
  end

  describe 'GET index' do
    render_views

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns public, internal, and private notes for other users' do
        reset_elasticsearch_indices(['notes'])

        note_public1 = FactoryBot.create(:note, :public, user_id: @user1.id)
        note_internal1 = FactoryBot.create(:note, :internal, user_id: @user1.id)
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        note_public2 = FactoryBot.create(:note, :public, user_id: @user2.id)
        note_internal2 = FactoryBot.create(:note, :internal, user_id: @user2.id)
        note_private2 = FactoryBot.create(:note, :private, user_id: @user2.id)

        reindex_elasticsearch_data(['notes'])

        get :index
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content(note_public1.title)
        expect(response.body).to have_content(note_internal1.title)
        expect(response.body).to have_content(note_private1.title)
        expect(response.body).to have_content(note_public2.title)
        expect(response.body).to have_content(note_internal2.title)
        expect(response.body).to have_content(note_private2.title)
      end
    end

    context 'for item owner' do
      include_context 'when note item owner logged in'

      it "returns public, internal, and only the user's private notes" do
        reset_elasticsearch_indices(['notes'])

        note_public1 = FactoryBot.create(:note, :public, user_id: @user1.id)
        note_internal1 = FactoryBot.create(:note, :internal, user_id: @user1.id)
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        note_public2 = FactoryBot.create(:note, :public, user_id: @user2.id)
        note_internal2 = FactoryBot.create(:note, :internal, user_id: @user2.id)
        note_private2 = FactoryBot.create(:note, :private, user_id: @user2.id)

        reindex_elasticsearch_data(['notes'])

        get :index
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content(note_public1.title)
        expect(response.body).to have_content(note_internal1.title)
        expect(response.body).to have_content(note_private1.title)
        expect(response.body).to have_content(note_public2.title)
        expect(response.body).to have_content(note_internal2.title)
        expect(response.body).to_not have_content(note_private2.title)
      end
    end

    context 'for note collaborator' do
      include_context 'when collaborator logged in'

      it "returns public, internal, and only the user's private and collaborated notes" do
        reset_elasticsearch_indices(['notes'])

        note_public1 = FactoryBot.create(:note, :public, user_id: @user1.id)
        note_internal1 = FactoryBot.create(:note, :internal, user_id: @user1.id)
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        note_public2 = FactoryBot.create(:note, :public, user_id: @user2.id)
        note_internal2 = FactoryBot.create(:note, :internal, user_id: @user2.id)
        note_private2 = FactoryBot.create(:note, :private, user_id: @user2.id)
        note_private1.collaborate(@collaborator, note_private1.user)

        reindex_elasticsearch_data(['notes'])

        get :index
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content(note_public1.title)
        expect(response.body).to have_content(note_internal1.title)
        expect(response.body).to have_content(note_private1.title)
        expect(response.body).to have_content(note_public2.title)
        expect(response.body).to have_content(note_internal2.title)
        expect(response.body).to_not have_content(note_private2.title)
      end
    end

    context 'for note group member' do
      include_context 'when group member logged in'

      it "returns public, internal, and only the user's private and collaborated notes" do
        reset_elasticsearch_indices(['notes'])

        note_public1 = FactoryBot.create(:note, :public, user_id: @user1.id)
        note_internal1 = FactoryBot.create(:note, :internal, user_id: @user1.id)
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        note_public2 = FactoryBot.create(:note, :public, user_id: @user2.id)
        note_internal2 = FactoryBot.create(:note, :internal, user_id: @user2.id)
        note_private2 = FactoryBot.create(:note, :private, user_id: @user2.id)
        note_private1.add_group(@group, note_private1.user)

        reindex_elasticsearch_data(['notes'])

        get :index
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content(note_public1.title)
        expect(response.body).to have_content(note_internal1.title)
        expect(response.body).to have_content(note_private1.title)
        expect(response.body).to have_content(note_public2.title)
        expect(response.body).to have_content(note_internal2.title)
        expect(response.body).to_not have_content(note_private2.title)
      end
    end

    context 'when not logged in' do
      it 'returns only public notes if not logged in' do
        reset_elasticsearch_indices(['notes'])

        note_public1 = FactoryBot.create(:note, :public, user_id: @user1.id)
        note_internal1 = FactoryBot.create(:note, :internal, user_id: @user1.id)
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        note_public2 = FactoryBot.create(:note, :public, user_id: @user2.id)
        note_internal2 = FactoryBot.create(:note, :internal, user_id: @user2.id)
        note_private2 = FactoryBot.create(:note, :private, user_id: @user2.id)

        reindex_elasticsearch_data(['notes'])

        get :index
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content(note_public1.title)
        expect(response.body).to_not have_content(note_internal1.title)
        expect(response.body).to_not have_content(note_private1.title)
        expect(response.body).to have_content(note_public2.title)
        expect(response.body).to_not have_content(note_internal2.title)
        expect(response.body).to_not have_content(note_private2.title)
      end
    end
  end

  describe 'GET show' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns 200 ok for another users private note' do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        get :show, params: { id: note_private1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for item owner' do
      include_context 'when note item owner logged in'

      it 'returns 200 ok private note' do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        get :show, params: { id: note_private1.id }
        expect(response).to have_http_status(:ok)
      end

      it 'returns 200 ok for internal note' do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        get :show, params: { id: note_private1.id }
        expect(response).to have_http_status(:ok)
      end

      it 'returns 200 ok for ublic note' do
        note_public1 = FactoryBot.create(:note, :public, user_id: @user1.id)
        get :show, params: { id: note_public1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for note collaborator' do
      include_context 'when collaborator logged in'

      it 'returns 200 ok for private note' do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        note_private1.collaborate(@collaborator, note_private1.user)
        get :show, params: { id: note_private1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for note group member' do
      include_context 'when group member logged in'

      it 'returns 200 ok for private note' do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        note_private1.add_group(@group, note_private1.user)
        get :show, params: { id: note_private1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it "returns a 404 for another user's private note" do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        get :show, params: { id: note_private1.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it "returns 200 ok for another user's internal note" do
        note_internal1 = FactoryBot.create(:note, :internal, user_id: @user1.id)
        get :show, params: { id: note_internal1.id }
        expect(response).to have_http_status(:ok)
      end

      it "returns 200 ok for another user's public note" do
        note_public1 = FactoryBot.create(:note, :public, user_id: @user1.id)
        sign_in @user2
        get :show, params: { id: note_public1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when not logged in' do
      it 'returns a 404 for a private note' do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        get :show, params: { id: note_private1.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it 'returns 404 for internal note' do
        note_internal1 = FactoryBot.create(:note, :internal, user_id: @user1.id)
        get :show, params: { id: note_internal1.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it 'returns 200 ok public note' do
        note_public1 = FactoryBot.create(:note, :public, user_id: @user1.id)
        get :show, params: { id: note_public1.id }
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'GET new' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns 200 ok' do
        get :new
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns 200 ok' do
        get :new
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when not logged in' do
      it 'returns 302 found redirect' do
        get :new
        expect(response).to have_http_status(:found)
      end
    end
  end

  describe 'GET edit' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "returns 200 ok for another user's note" do
        note_public1 = FactoryBot.create(:note, :public, user_id: @user1.id)
        get :edit, params: { id: note_public1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for item owner' do
      include_context 'when note item owner logged in'

      it 'returns 200 ok' do
        note_public1 = FactoryBot.create(:note, :public, user_id: @user1.id)
        get :edit, params: { id: note_public1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it "returns 302 found redirect for another user's note" do
        note_public1 = FactoryBot.create(:note, :public, user_id: @user1.id)
        get :edit, params: { id: note_public1.id }
        expect(response).to have_http_status(:found)
      end
    end

    context 'for note collaborator' do
      include_context 'when collaborator logged in'

      it 'returns 200 ok for private note' do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        note_private1.collaborate(@collaborator, note_private1.user)
        get :edit, params: { id: note_private1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for note group member' do
      include_context 'when group member logged in'

      it 'returns 200 ok for private note' do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        note_private1.add_group(@group, note_private1.user)
        get :edit, params: { id: note_private1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when not logged in' do
      it 'returns 302 found redirect' do
        note_public1 = FactoryBot.create(:note, :public, user_id: @user1.id)
        get :edit, params: { id: note_public1.id }
        expect(response).to have_http_status(:found)
      end
    end
  end

  describe 'POST notes' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'creates new note with valid data' do
        expect do
          post :create,
               params: { note: { title: 'my title', text: 'lots of notes',
                         visibility: 0, user_id: @admin.id } }
        end.to change(Note, :count).by(1)
      end

      it 'does not create a new note with invalid data' do
        expect do
          post :create, params: { note: { text: 'lots of notes',
                                  visibility: 0, user_id: @admin
            .id } }
        end.to change(Note, :count).by(0)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'creates new note with valid data' do
        expect do
          post :create,
               params: { note: { title: 'my title', text: 'lots of notes',
                                 visibility: 0, user_id: @user1.id } }
        end.to change(Note, :count).by(1)
      end

      it 'does not create a new note with inalid data' do
        sign_in @user1
        expect  do
          post :create, params: { note: { text: 'lots of notes',
                                          visibility: 0, user_id: @user1.id } }
        end.to change(Note, :count).by(0)
      end
    end

    context 'when not logged in' do
      it 'does not create a new note' do
        expect do
          post :create,
               params: { note: { title: 'my title', text: 'lots of notes',
                                 visibility: 0, user_id: @user1.id } }
        end.to change(Note, :count).by(0)
      end
    end
  end

  describe 'PATCH update' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "updates another user's note with valid data" do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        patch :update, params: { id: note_private1.id,
                                 note: { title: 'updated_text' } }
        expect(response).to redirect_to(note_private1)
        note_private1.reload
        expect(note_private1.title).to eq('updated_text')
      end

      it "does not update another user's with invalid data" do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        patch :update, params: { id: note_private1.id, note: { title: nil } }
        note_private1.reload
        expect(note_private1.title).to_not be_nil
      end
    end

    context 'for item owner' do
      include_context 'when note item owner logged in'

      it "updates user's note with valid data" do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        patch :update, params: { id: note_private1.id,
                                 note: { title: 'updated_text' } }
        expect(response).to redirect_to(note_private1)
        note_private1.reload
        expect(note_private1.title).to eq('updated_text')
      end

      it "does not update user's note with invalid data" do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        patch :update, params: { id: note_private1.id, note: { title: nil } }
        note_private1.reload
        expect(note_private1.title).to_not be_nil
      end
    end

    context 'for note collaborator' do
      include_context 'when collaborator logged in'

      it "updates collaborated note with valid data" do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        note_private1.collaborate(@collaborator, note_private1.user)
        put :update, params: { id: note_private1.id,
                                     note: { title: 'updated_text' } }
        expect(response).to redirect_to(note_private1)
        note_private1.reload
        expect(note_private1.title).to eq('updated_text')
      end

      it "does not update collaborated note with invalid data" do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        note_private1.collaborate(@collaborator, note_private1.user)
        put :update, params: { id: note_private1.id, note: { title: nil } }
        note_private1.reload
        expect(note_private1.title).to_not be_nil
      end
    end

    context 'for note group member' do
      include_context 'when group member logged in'

      it "updates collaborated note with valid data" do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        note_private1.add_group(@group, note_private1.user)
        put :update, params: { id: note_private1.id,
                                     note: { title: 'updated_text' } }
        expect(response).to redirect_to(note_private1)
        note_private1.reload
        expect(note_private1.title).to eq('updated_text')
      end

      it "does not update collaborated note with invalid data" do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        note_private1.add_group(@group, note_private1.user)
        put :update, params: { id: note_private1.id, note: { title: nil } }
        note_private1.reload
        expect(note_private1.title).to_not be_nil
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it "does not update another user's note" do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        patch :update, params: { id: note_private1.id,
                                 note: { title: 'updated_text' } }
        expect(response).to redirect_to(notes_path)
        note_private1.reload
        expect(note_private1.title).to_not eq('updated_text')
      end
    end

    context 'when not logged in' do
      it "does not update another user's note" do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        patch :update, params: { id: note_private1.id,
                                 note: { title: 'updated_text' } }
        expect(response).to redirect_to(new_user_session_path)
        note_private1.reload
        expect(note_private1.title).to_not eq('updated_text')
      end
    end
  end

  describe 'PUT update' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "updates another user's note with valid data" do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        put :update, params: { id: note_private1.id,
                               note: { title: 'updated_text' } }
        expect(response).to redirect_to(note_private1)
        note_private1.reload
        expect(note_private1.title).to eq('updated_text')
      end

      it "does not update another user's note with invalid data" do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        put :update, params: { id: note_private1.id, note: { title: nil } }
        note_private1.reload
        expect(note_private1.title).to_not be_nil
      end
    end

    context 'for item owner' do
      include_context 'when note item owner logged in'

      it "updates user's note with valid data" do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        put :update, params: { id: note_private1.id,
                               note: { title: 'updated_text' } }
        expect(response).to redirect_to(note_private1)
        note_private1.reload
        expect(note_private1.title).to eq('updated_text')
      end

      it "does not update user's note with invalid data" do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        put :update, params: { id: note_private1.id, note: { title: nil } }
        note_private1.reload
        expect(note_private1.title).to_not be_nil
      end
    end

    context 'for note collaborator' do
      include_context 'when collaborator logged in'

      it "updates collaborated note with valid data" do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        note_private1.collaborate(@collaborator, note_private1.user)
        patch :update, params: { id: note_private1.id,
                                     note: { title: 'updated_text' } }
        expect(response).to redirect_to(note_private1)
        note_private1.reload
        expect(note_private1.title).to eq('updated_text')
      end

      it "does not update collaborated note with invalid data" do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        note_private1.collaborate(@collaborator, note_private1.user)
        patch :update, params: { id: note_private1.id, note: { title: nil } }
        note_private1.reload
        expect(note_private1.title).to_not be_nil
      end
    end

    context 'for note group member' do
      include_context 'when group member logged in'

      it "updates collaborated note with valid data" do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        note_private1.add_group(@group, note_private1.user)
        patch :update, params: { id: note_private1.id,
                                     note: { title: 'updated_text' } }
        expect(response).to redirect_to(note_private1)
        note_private1.reload
        expect(note_private1.title).to eq('updated_text')
      end

      it "does not update collaborated note with invalid data" do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        note_private1.add_group(@group, note_private1.user)
        patch :update, params: { id: note_private1.id, note: { title: nil } }
        note_private1.reload
        expect(note_private1.title).to_not be_nil
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it "does not update another user's note" do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        put :update, params: { id: note_private1.id,
                               note: { title: 'updated_text' } }
        expect(response).to redirect_to(notes_path)
        note_private1.reload
        expect(note_private1.title).to_not eq('updated_text')
      end
    end

    context 'when not logged in' do
      it "does not update another user's note" do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        put :update, params: { id: note_private1.id,
                               note: { title: 'updated_text' } }
        expect(response).to redirect_to(new_user_session_path)
        note_private1.reload
        expect(note_private1.title).to_not eq('updated_text')
      end
    end
  end

  describe 'DELETE destroy' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "destroys a user's note" do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        expect do
          delete :destroy, params: { id: note_private1.id }
        end.to change(Note, :count).by(-1)
      end

      it 'redirects to notes index' do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        delete :destroy, params: { id: note_private1.id }
        expect(response).to redirect_to(notes_path)
      end
    end

    context 'for item owner' do
      include_context 'when note item owner logged in'

      it "destroys a user's note" do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        expect do
          delete :destroy, params: { id: note_private1.id }
        end.to change(Note, :count).by(-1)
      end
    end

    context 'for note collaborator' do
      include_context 'when collaborator logged in'

      it 'redirects to notes index' do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        note_private1.collaborate(@collaborator, note_private1.user)
        expect do
          delete :destroy, params: { id: note_private1.id }
        end.to change(Note, :count).by(0)
        expect(response).to redirect_to(notes_path)
      end
    end

    context 'for note group member' do
      include_context 'when group member logged in'

      it 'redirects to notes index' do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        note_private1.add_group(@group, note_private1.user)
        expect do
          delete :destroy, params: { id: note_private1.id }
        end.to change(Note, :count).by(0)
        expect(response).to redirect_to(notes_path)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'redirects to notes index' do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        delete :destroy, params: { id: note_private1.id }
        expect(response).to redirect_to(notes_path)
      end

      it "does not destroy a user's note" do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        expect do
          delete :destroy, params: { id: note_private1.id }
        end.to change(Note, :count).by(0)
      end
    end

    context 'when not logged in' do
      it "does not destroy a user's note" do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        expect do
          delete :destroy, params: { id: note_private1.id }
        end.to change(Note, :count).by(0)
      end

      it 'redirects to notes index' do
        note_private1 = FactoryBot.create(:note, :private, user_id: @user1.id)
        delete :destroy, params: { id: note_private1.id }
        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end
end
