require 'rails_helper'

RSpec.describe MessagesController, type: :controller do
  before(:each) do
    @siteSettings = SiteSetting.first
    @siteSettings.update(chat: true)
    @chat = FactoryBot.create(:chat)
    @user1 = FactoryBot.create(:user, :confirmed)
    @user2 = FactoryBot.create(:user, :confirmed)
    @user3 = FactoryBot.create(:user, :confirmed)
    @chat.collaborate(@user1)
    @chat.collaborate(@user2)
    ChatPreference.create(user: @user1, chat: @chat)
    ChatPreference.create(user: @user2, chat: @chat)
  end

  shared_context 'when chat participant logged in' do
    before(:each) do
      sign_in @user1
    end
  end

  shared_context 'when non-chat participant logged in' do
    before(:each) do
      sign_in @user3
    end
  end

  describe 'POST create' do
    context 'for chat participant user' do
      include_context 'when chat participant logged in'

      it 'is successful when chat is site enabled' do
        expect do
          post :create, params: {
              chat_id: @chat.id, message: { content: 'my new message' } }
        end.to change(@chat.messages, :count).by(1)
        expect(response).to have_http_status(:no_content)
      end

      it 'is not successful when chat is site disabled' do
        @siteSettings.update(chat: false)
        expect do
          post :create, params: {
              chat_id: @chat.id, message: { content: 'my new message' } }
        end.to change(@chat.messages, :count).by(0)
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end
    end

    context 'for non-chat participant user' do
      include_context 'when non-chat participant logged in'

      it 'is not successful' do
        expect do
          post :create, params: {
              chat_id: @chat.id, message: { content: 'my new message' } }
        end.to change(@chat.messages, :count).by(0)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not logged in' do

      it 'is not successful' do
        expect do
          post :create, params: {
              chat_id: @chat.id, message: { content: 'my new message' } }
        end.to change(@chat.messages, :count).by(0)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end
end
