require 'rails_helper'

RSpec.describe StarsController, type: :controller do
  before(:each) do
    @user1 = FactoryBot.create(:user, :confirmed)
    @user2 = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group.add_member(@group_member, @group.user)
    @admin = FactoryBot.create(:user, :confirmed, :admin)

    UserItems.item_list.each do |item|
      public = instance_variable_set("@#{item.singularize}_public", FactoryBot.create(item.singularize, :public, user_id: @user1.id))
      internal = instance_variable_set("@#{item.singularize}_internal", FactoryBot.create(item.singularize, :internal, user_id: @user1.id))
      private = instance_variable_set("@#{item.singularize}_private", FactoryBot.create(item.singularize, :private, user_id: @user1.id))
      public.collaborate(@collaborator, public.user)
      internal.collaborate(@collaborator, internal.user)
      private.collaborate(@collaborator, private.user)
      public.add_group(@group, public.user)
      internal.add_group(@group, internal.user)
      private.add_group(@group, private.user)
    end
  end

  shared_context 'when admin logged in' do
    before(:each) do
      sign_in @admin
    end
  end

  shared_context 'when item owner logged in' do
    before(:each) do
      sign_in @user1
    end
  end

  shared_context 'when collaborator logged in' do
    before(:each) do
      sign_in @collaborator
    end
  end

  shared_context 'when group member logged in' do
    before(:each) do
      sign_in @group_member
    end
  end

  shared_context 'when user logged in' do
    before(:each) do
      sign_in @user2
    end
  end

  describe 'PATCH add' do
    render_views

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'successfully stars private item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_private")
          expect(item.star_count).to eq(0)
          expect do
            patch :add, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(1)
          expect(item.reload.star_count).to eq(1)
        end
      end

      it 'successfully stars internal item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_internal")
          expect(item.star_count).to eq(0)
          expect do
            patch :add, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(1)
          expect(item.reload.star_count).to eq(1)
        end
      end

      it 'successfully stars public item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_public")
          expect(item.star_count).to eq(0)
          expect do
            patch :add, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(1)
          expect(item.reload.star_count).to eq(1)
        end
      end
    end

    context 'for item owner' do
      include_context 'when item owner logged in'

      it 'successfully stars private item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_private")
          expect(item.star_count).to eq(0)
          expect do
            patch :add, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(1)
          expect(item.reload.star_count).to eq(1)
        end
      end

      it 'successfully stars internal item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_internal")
          expect(item.star_count).to eq(0)
          expect do
            patch :add, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(1)
          expect(item.reload.star_count).to eq(1)
        end
      end

      it 'successfully stars public item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_public")
          expect(item.star_count).to eq(0)
          expect do
            patch :add, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(1)
          expect(item.reload.star_count).to eq(1)
        end
      end
    end

    context 'for item collaborator' do
      include_context 'when collaborator logged in'

      it 'successfully stars private item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_private")
          expect(item.star_count).to eq(0)
          expect do
            patch :add, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(1)
          expect(item.reload.star_count).to eq(1)
        end
      end

      it 'successfully stars internal item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_internal")
          expect(item.star_count).to eq(0)
          expect do
            patch :add, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(1)
          expect(item.reload.star_count).to eq(1)
        end
      end

      it 'successfully stars public item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_public")
          expect(item.star_count).to eq(0)
          expect do
            patch :add, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(1)
          expect(item.reload.star_count).to eq(1)
        end
      end
    end

    context 'for item group member' do
      include_context 'when group member logged in'

      it 'successfully stars private item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_private")
          expect(item.star_count).to eq(0)
          expect do
            patch :add, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(1)
          expect(item.reload.star_count).to eq(1)
        end
      end

      it 'successfully stars internal item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_internal")
          expect(item.star_count).to eq(0)
          expect do
            patch :add, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(1)
          expect(item.reload.star_count).to eq(1)
        end
      end

      it 'successfully stars public item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_public")
          expect(item.star_count).to eq(0)
          expect do
            patch :add, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(1)
          expect(item.reload.star_count).to eq(1)
        end
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'does not star private item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_private")
          expect(item.star_count).to eq(0)
          expect do
            patch :add, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(0)
          expect(item.reload.star_count).to eq(0)
        end
      end

      it 'successfully stars internal item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_internal")
          expect(item.star_count).to eq(0)
          expect do
            patch :add, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(1)
          expect(item.reload.star_count).to eq(1)
        end
      end

      it 'successfully stars public item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_public")
          expect(item.star_count).to eq(0)
          expect do
            patch :add, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(1)
          expect(item.reload.star_count).to eq(1)
        end
      end
    end

    context 'when not logged in' do
      it 'does not star private item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_private")
          expect(item.star_count).to eq(0)
          expect do
            patch :add, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(0)
          expect(item.reload.star_count).to eq(0)
        end
      end

      it 'does not star internal item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_internal")
          expect(item.star_count).to eq(0)
          expect do
            patch :add, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(0)
          expect(item.reload.star_count).to eq(0)
        end
      end

      it 'does not star public item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_public")
          expect(item.star_count).to eq(0)
          expect do
            patch :add, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(0)
          expect(item.reload.star_count).to eq(0)
        end
      end
    end
  end

  describe 'PATCH remove' do
    render_views

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'successfully unstars private item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_private")
          item.add_star(@admin)
          expect(item.star_count).to eq(1)
          expect do
            patch :remove, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(-1)
          expect(item.reload.star_count).to eq(0)
        end
      end

      it 'successfully unstars internal item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_internal")
          item.add_star(@admin)
          expect(item.star_count).to eq(1)
          expect do
            patch :remove, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(-1)
          expect(item.reload.star_count).to eq(0)
        end
      end

      it 'successfully unstars public item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_public")
          item.add_star(@admin)
          expect(item.star_count).to eq(1)
          expect do
            patch :remove, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(-1)
          expect(item.reload.star_count).to eq(0)
        end
      end
    end

    context 'for item owner' do
      include_context 'when item owner logged in'

      it 'successfully unstars private item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_private")
          item.add_star(@user1)
          expect(item.star_count).to eq(1)
          expect do
            patch :remove, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(-1)
          expect(item.reload.star_count).to eq(0)
        end
      end

      it 'successfully unstars internal item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_internal")
          item.add_star(@user1)
          expect(item.star_count).to eq(1)
          expect do
            patch :remove, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(-1)
          expect(item.reload.star_count).to eq(0)
        end
      end

      it 'successfully unstars public item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_public")
          item.add_star(@user1)
          expect(item.star_count).to eq(1)
          expect do
            patch :remove, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(-1)
          expect(item.reload.star_count).to eq(0)
        end
      end
    end

    context 'for item collaborator' do
      include_context 'when collaborator logged in'

      it 'successfully unstars private item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_private")
          item.add_star(@collaborator)
          expect(item.star_count).to eq(1)
          expect do
            patch :remove, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(-1)
          expect(item.reload.star_count).to eq(0)
        end
      end

      it 'successfully unstars internal item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_internal")
          item.add_star(@collaborator)
          expect(item.star_count).to eq(1)
          expect do
            patch :remove, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(-1)
          expect(item.reload.star_count).to eq(0)
        end
      end

      it 'successfully unstars public item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_public")
          item.add_star(@collaborator)
          expect(item.star_count).to eq(1)
          expect do
            patch :remove, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(-1)
          expect(item.reload.star_count).to eq(0)
        end
      end
    end

    context 'for item group member' do
      include_context 'when group member logged in'

      it 'successfully unstars private item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_private")
          item.add_star(@group_member)
          expect(item.star_count).to eq(1)
          expect do
            patch :remove, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(-1)
          expect(item.reload.star_count).to eq(0)
        end
      end

      it 'successfully unstars internal item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_internal")
          item.add_star(@group_member)
          expect(item.star_count).to eq(1)
          expect do
            patch :remove, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(-1)
          expect(item.reload.star_count).to eq(0)
        end
      end

      it 'successfully unstars public item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_public")
          item.add_star(@group_member)
          expect(item.star_count).to eq(1)
          expect do
            patch :remove, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(-1)
          expect(item.reload.star_count).to eq(0)
        end
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'successfully unstars internal item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_internal")
          item.add_star(@user2)
          expect(item.star_count).to eq(1)
          expect do
            patch :remove, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(-1)
          expect(item.reload.star_count).to eq(0)
        end
      end

      it 'successfully unstars public item' do
        UserItems.item_list.each do |item|
          item = instance_variable_get("@#{item.singularize}_public")
          item.add_star(@user2)
          expect(item.star_count).to eq(1)
          expect do
            patch :remove, params: { record_id: item.id, record_type: item.type }
          end.to change(Star, :count).by(-1)
          expect(item.reload.star_count).to eq(0)
        end
      end

      it 'does not unstars private item that used to be public' do
        UserItems.item_list.each do |item|
          public1 = FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          public1.add_star(@user2)
          expect(public1.star_count).to eq(1)
          public1.update(visibility: '0')
          expect do
            patch :remove, params: { record_id: public1.id, record_type: public1.type }
          end.to change(Star, :count).by(0)
          expect(public1.reload.star_count).to eq(1)
        end
      end
    end
  end

  describe 'GET stargazers' do
    render_views

    context 'for admin user' do
      include_context 'when admin logged in'

      it "is successful for private item" do
        UserItems.item_list.each do |item|
          item = FactoryBot.create(item.singularize, :private, user_id: @user1.id)
          get :stargazers, params: { type: item.route_type, record_id: item.id }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template('stars/stargazers')
        end
      end

      it "is successful for internal item" do
        UserItems.item_list.each do |item|
          item = FactoryBot.create(item.singularize, :internal, user_id: @user1.id)
          get :stargazers, params: { type: item.route_type, record_id: item.id }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template('stars/stargazers')
        end
      end

      it "is successful for public item" do
        UserItems.item_list.each do |item|
          item = FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          get :stargazers, params: { type: item.route_type, record_id: item.id }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template('stars/stargazers')
        end
      end
    end

    context 'for item owner' do
      include_context 'when item owner logged in'

      it "is successful for private item" do
        UserItems.item_list.each do |item|
          item = FactoryBot.create(item.singularize, :private, user_id: @user1.id)
          get :stargazers, params: { type: item.route_type, record_id: item.id }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template('stars/stargazers')
        end
      end

      it "is successful for internal item" do
        UserItems.item_list.each do |item|
          item = FactoryBot.create(item.singularize, :internal, user_id: @user1.id)
          get :stargazers, params: { type: item.route_type, record_id: item.id }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template('stars/stargazers')
        end
      end

      it "is successful for public item" do
        UserItems.item_list.each do |item|
          item = FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          get :stargazers, params: { type: item.route_type, record_id: item.id }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template('stars/stargazers')
        end
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it "is not successful for private item" do
        UserItems.item_list.each do |item|
          item = FactoryBot.create(item.singularize, :private, user_id: @user1.id)
          get :stargazers, params: { type: item.route_type, record_id: item.id }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template('errors/not_found')
        end
      end

      it "is successful for internal item" do
        UserItems.item_list.each do |item|
          item = FactoryBot.create(item.singularize, :internal, user_id: @user1.id)
          get :stargazers, params: { type: item.route_type, record_id: item.id }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template('stars/stargazers')
        end
      end

      it "is successful for public item" do
        UserItems.item_list.each do |item|
          item = FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          get :stargazers, params: { type: item.route_type, record_id: item.id }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template('stars/stargazers')
        end
      end
    end

    context 'when not logged in' do
      it "is not successful for private item" do
        UserItems.item_list.each do |item|
          item = FactoryBot.create(item.singularize, :private, user_id: @user1.id)
          get :stargazers, params: { type: item.route_type, record_id: item.id }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template('errors/not_found')
        end
      end

      it "is not successful for internal item" do
        UserItems.item_list.each do |item|
          item = FactoryBot.create(item.singularize, :internal, user_id: @user1.id)
          get :stargazers, params: { type: item.route_type, record_id: item.id }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template('errors/not_found')
        end
      end

      it "is successful for public item" do
        UserItems.item_list.each do |item|
          item = FactoryBot.create(item.singularize, :public, user_id: @user1.id)
          get :stargazers, params: { type: item.route_type, record_id: item.id }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template('stars/stargazers')
        end
      end
    end
  end
end
