require 'rails_helper'

RSpec.describe RegistrationsController, type: :controller do
  before(:each) do
    @siteSettings = SiteSetting.first
    @user1 = FactoryBot.create(:user, :confirmed, :avatar)
    @admin = FactoryBot.create(:user, :confirmed, :admin, :avatar)
  end

  before(:each) do
    request.env['devise.mapping'] = Devise.mappings[:user]
  end

  shared_context 'when admin logged in' do
    before(:each) do
      sign_in @admin
    end
  end

  shared_context 'when user logged in' do
    before(:each) do
      sign_in @user1
    end
  end

  describe 'GET new' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should redirect when users_can_join = true' do
        @siteSettings.update(users_can_join: true)
        get :new
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
        expect(flash[:alert]).to match(/You are already signed in./)
      end

      it 'should redirect when users_can_join = false' do
        @siteSettings.update(users_can_join: false)
        get :new
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
        expect(flash[:alert]).to match(/You are already signed in./)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should redirect when users_can_join = true' do
        @siteSettings.update(users_can_join: true)
        get :new
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
        expect(flash[:alert]).to match(/You are already signed in./)
      end

      it 'should redirect when users_can_join = false' do
        @siteSettings.update(users_can_join: false)
        get :new
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
        expect(flash[:alert]).to match(/You are already signed in./)
      end
    end

    context 'when not logged in' do

      it 'should be successful when users_can_join = true' do
        @siteSettings.update(users_can_join: true)
        get :new
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end

      it 'should redirect when users_can_join = false' do
        @siteSettings.update(users_can_join: false)
        get :new
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end
    end
  end

  describe 'POST create' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should create new user when users_can_join = true' do
        @siteSettings.update(users_can_join: true)
        expect do
          post :create, params: { user: FactoryBot.attributes_for(:user) }
        end.to change(User, :count).by(1).and change(Devise.mailer.deliveries, :count).by(1)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(User.ordered_by_id.last)
        expect(flash[:notice]).to match(/User created and confirmation email sent to their email address./)
        expect(Devise.mailer.deliveries.last.subject).to eq('Confirmation instructions')
        expect(User.ordered_by_id.last.admin).to be false
      end

      it 'should create new user when users_can_join = false' do
        @siteSettings.update(users_can_join: false)
        expect do
          post :create, params: { user: FactoryBot.attributes_for(:user) }
        end.to change(User, :count).by(1).and change(Devise.mailer.deliveries, :count).by(1)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(User.ordered_by_id.last)
        expect(flash[:notice]).to match(/User created and confirmation email sent to their email address./)
        expect(Devise.mailer.deliveries.last.subject).to eq('Confirmation instructions')
        expect(User.ordered_by_id.last.admin).to be false
      end

      it 'should not create user with invalid data' do
        expect do
          post :create, params: { user: FactoryBot.attributes_for(:user, username: "spac es") }
        end.to change(User, :count).by(0).and change(Devise.mailer.deliveries, :count).by(0)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_path)
        expect(flash[:alert]).to match(/Username can only contain letters, numbers, dashes, or underscores/)
      end

      it 'should create new user with admin attribute' do
        expect do
          post :create, params: { user: FactoryBot.attributes_for(:user, :admin) }
        end.to change(User, :count).by(1).and change(Devise.mailer.deliveries, :count).by(1)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(User.ordered_by_id.last)
        expect(flash[:notice]).to match(/User created and confirmation email sent to their email address./)
        expect(Devise.mailer.deliveries.last.subject).to eq('Confirmation instructions')
        expect(User.ordered_by_id.last.admin).to be true
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not create new user when users_can_join = true' do
        @siteSettings.update(users_can_join: true)
        expect do
          post :create, params: { user: FactoryBot.attributes_for(:user) }
        end.to change(User, :count).by(0).and change(Devise.mailer.deliveries, :count).by(0)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end

      it 'should not create new user when users_can_join = false' do
        @siteSettings.update(users_can_join: false)
        expect do
          post :create, params: { user: FactoryBot.attributes_for(:user) }
        end.to change(User, :count).by(0).and change(Devise.mailer.deliveries, :count).by(0)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not logged in' do

      it 'should create new user when users_can_join = true' do
        @siteSettings.update(users_can_join: true)
        expect do
          post :create, params: { user: FactoryBot.attributes_for(:user) }
        end.to change(User, :count).by(1).and change(Devise.mailer.deliveries, :count).by(1)
        expect(response).to have_http_status(:see_other)
        expect(response).to redirect_to(root_path)
        expect(flash[:notice]).to match(/A message with a confirmation link has been sent to your email address. Please follow the link to activate your account./)
        expect(Devise.mailer.deliveries.last.subject).to eq('Confirmation instructions')
        expect(User.ordered_by_id.last.admin).to be false
      end

      it 'should not create new user with admin attribute when users_can_join = true' do
        @siteSettings.update(users_can_join: true)
        expect do
          post :create, params: { user: FactoryBot.attributes_for(:user, :admin) }
        end.to change(User, :count).by(1).and change(Devise.mailer.deliveries, :count).by(1)
        expect(response).to have_http_status(:see_other)
        expect(response).to redirect_to(root_path)
        expect(flash[:notice]).to match(/A message with a confirmation link has been sent to your email address. Please follow the link to activate your account./)
        expect(Devise.mailer.deliveries.last.subject).to eq('Confirmation instructions')
        expect(User.ordered_by_id.last.admin).to be false
      end

      it 'should not create user with invalid data' do
        @siteSettings.update(users_can_join: true)
        expect do
          post :create, params: { user: FactoryBot.attributes_for(:user, username: "spac es") }
        end.to change(User, :count).by(0).and change(Devise.mailer.deliveries, :count).by(0)
      end

      it 'should not create new user when users_can_join = false' do
        @siteSettings.update(users_can_join: false)
        expect do
          post :create, params: { user: FactoryBot.attributes_for(:user) }
        end.to change(User, :count).by(0).and change(Devise.mailer.deliveries, :count).by(0)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end
    end
  end

  describe 'DELETE delete_avatar' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "deletes own avatar" do
        expect(@admin.avatar).to be_attached
        delete :delete_avatar
        expect(@admin.reload.avatar).to_not be_attached
        expect(response).to have_http_status(:found)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it "deletes own avatar" do
        expect(@user1.avatar).to be_attached
        delete :delete_avatar
        expect(@user1.reload.avatar).to_not be_attached
        expect(response).to have_http_status(:found)
      end

    end

    context 'when not logged in' do

      it "deletes own avatar" do
        delete :delete_avatar
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end

    end
  end
end
