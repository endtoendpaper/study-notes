require 'rails_helper'

RSpec.describe TablesController, type: :controller do
  before(:each) do
    @user1 = FactoryBot.create(:user, :confirmed)
    @user2 = FactoryBot.create(:user, :confirmed)
    @note = FactoryBot.create(:note, :public, user_id: @user1.id)
    @table = FactoryBot.create(:table, :record_saved, record_id: @note.id, record_type: 'Note')
  end

  shared_context 'when record owner logged in' do
    before(:each) do
      sign_in @user1
    end
  end

  shared_context 'when user logged in' do
    before(:each) do
      sign_in @user2
    end
  end

  describe 'GET show' do

    context 'for record owner' do
      include_context 'when record owner logged in'

      it 'is successful' do
        get :show, params: { id: @table.attachable_sgid }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'is not successful' do
        get :show, params: { id: @table.attachable_sgid }
        expect(response).to have_http_status(:found)
      end
    end

    context 'when not logged in' do

      it 'is not successful' do
        get :show, params: { id: @table.attachable_sgid }
        expect(response).to have_http_status(:found)
      end
    end
  end

  describe 'POST create' do

    context 'for record owner' do
      include_context 'when record owner logged in'

      it 'is successful' do
        post :create
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'is successful if it is a new record' do
        post :create
        expect(response).to have_http_status(:ok)
      end

      it 'is not successful if record is already created' do
        post :create,
        params: { record_id: @note.id, record_type: 'Note' }
        expect(response).to have_http_status(:found)
      end
    end

    context 'when not logged in' do

      it 'is not successful' do
        post :create
        expect(response).to have_http_status(:found)
      end
    end
  end

  describe 'PATCH update' do

    context 'for record owner' do
      include_context 'when record owner logged in'

      it 'is successful' do
        patch :update,
        params: { id: @table.attachable_sgid }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'is successful if it is a new record' do
        table = FactoryBot.create(:table)
        patch :update,
        params: { id: table.attachable_sgid }
        expect(response).to have_http_status(:ok)
      end

      it 'is not successful if record is already created' do
        patch :update,
        params: { id: @table.attachable_sgid }
        expect(response).to have_http_status(:found)
      end
    end

    context 'when not logged in' do

      it 'is not successful' do
        patch :update,
        params: { id: @table.attachable_sgid }
        expect(response).to have_http_status(:found)
      end
    end
  end
end
