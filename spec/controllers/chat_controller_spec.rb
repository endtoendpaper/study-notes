require 'rails_helper'

RSpec.describe ChatsController, type: :controller do
  before(:each) do
    @siteSettings = SiteSetting.first
    @siteSettings.update(chat: true)
    @chat = FactoryBot.create(:chat)
    @group = FactoryBot.create(:group)
    @user1 = FactoryBot.create(:user, :confirmed)
    @user2 = FactoryBot.create(:user, :confirmed)
    @user3 = FactoryBot.create(:user, :confirmed)
    @group.add_member(@user1, @group.user)
    @group.add_member(@user2, @group.user)
    @chat.add_group(@group)
    @group.all_group_members.each do |member|
      ChatPreference.create(user: member, chat: @chat)
    end
  end

  shared_context 'when chat participant logged in' do
    before(:each) do
      sign_in @user1
    end
  end

  shared_context 'when non-chat participant logged in' do
    before(:each) do
      sign_in @user3
    end
  end

  describe 'GET index' do
    render_views

    context 'for chat participant user' do
      include_context 'when chat participant logged in'

      it 'is successful when chat is site enabled' do
        get :index
        expect(response).to have_http_status(:ok)
      end

      it 'is not successful when chat is site disabled' do
        @siteSettings.update(chat: false)
        get :index
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it 'is shows chat' do
        get :index
        expect(response.body).to have_content(@group.name)
      end
    end

    context 'for non-chat participant user' do
      include_context 'when non-chat participant logged in'

      it 'does not show chat' do
        get :index
        expect(response.body).to_not have_content(@group.name)
      end
    end

    context 'when not logged in' do

      it 'is not successful' do
        get :index
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end

  describe 'POST update_index' do
    context 'for chat participant user' do
      include_context 'when chat participant logged in'

      it 'is successful when chat is site enabled' do
        post :update_index
        expect(response).to have_http_status(:no_content)
      end

      it 'is not successful when chat is site disabled' do
        @siteSettings.update(chat: false)
        post :update_index
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end
    end

    context 'when not logged in' do

      it 'is not successful' do
        post :update_index, params: { chat_id: @chat.id }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end

  describe 'POST update_show' do
    context 'for chat participant user' do
      include_context 'when chat participant logged in'

      it 'is successful when chat is site enabled' do
        post :update_show, params: { id: @chat.id }
        expect(response).to have_http_status(:no_content)
      end

      it 'is not successful when chat is site disabled' do
        @siteSettings.update(chat: false)
        post :update_show, params: { id: @chat.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end
    end

    context 'for non-chat participant user' do
      include_context 'when non-chat participant logged in'

      it 'is not successful when chat is site enabled' do
        post :update_show, params: { id: @chat.id }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
        expect(flash[:alert]).to match(/Not authorized./)
      end
    end

    context 'when not logged in' do

      it 'is not successful' do
        post :update_show, params: { id: @chat.id }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end

  describe 'POST load_more_messages' do
    context 'for chat participant user' do
      include_context 'when chat participant logged in'

      it 'is successful when chat is site enabled' do
        post :load_more_messages, params: { id: @chat.id }
        expect(response).to have_http_status(:no_content)
      end

      it 'is not successful when chat is site disabled' do
        @siteSettings.update(chat: false)
        post :load_more_messages, params: { id: @chat.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end
    end

    context 'for non-chat participant user' do
      include_context 'when non-chat participant logged in'

      it 'is not successful when chat is site enabled' do
        post :load_more_messages, params: { id: @chat.id }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
        expect(flash[:alert]).to match(/Not authorized./)
      end
    end

    context 'when not logged in' do

      it 'is not successful' do
        post :load_more_messages, params: { id: @chat.id }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end

  describe 'GET join_user_chat' do
    context 'for chat participant user' do
      include_context 'when chat participant logged in'

      it 'is successful when chat is site enabled' do
        get :join_user_chat, params: { user_id: @user2.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('chats/join_user_chat')
      end

      it 'is not successful when chat is site disabled' do
        @siteSettings.update(chat: false)
        get :join_user_chat, params: { user_id: @user2.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it 'is not successful when user_id is invalid' do
        get :join_user_chat, params: { user_id: 'not an id' }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
        expect(flash[:alert]).to match(/Not a valid username./)
      end
    end

    context 'when not logged in' do

      it 'is not successful' do
        get :join_user_chat, params: { user_id: @user2.id }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end

  describe 'GET join_group_chat' do
    context 'for chat participant user' do
      include_context 'when chat participant logged in'

      it 'is successful when chat is site enabled' do
        get :join_group_chat, params: { group_id: @group.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('chats/join_group_chat')
      end

      it 'is not successful when chat is site disabled' do
        @siteSettings.update(chat: false)
        get :join_group_chat, params: { group_id: @group.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it 'is not successful when user_id is invalid' do
        get :join_group_chat, params: { group_id: "not an id" }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
        expect(flash[:alert]).to match(/Not a member of this group./)
      end
    end

    context 'for non-chat participant user' do
      include_context 'when non-chat participant logged in'

      it 'is not successful when chat is site enabled' do
        get :join_group_chat, params: { group_id: @group.id }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
        expect(flash[:alert]).to match(/Not a member of this group./)
      end
    end

    context 'when not logged in' do

      it 'is not successful' do
        get :join_group_chat, params: { group_id: @group.id }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end

  describe 'POST create' do
    context 'for chat participant user' do
      include_context 'when chat participant logged in'

      it 'is successful for dm when chat is site enabled' do
        expect(Chat.count).to eq(1)
        expect do
          post :create, params: { commit: '@User', participant: @user2.username }
        end.to change(Chat, :count).by(1) and change(ChatPreference, :count).by(2)
        expect(response).to have_http_status(:no_content)
      end

      it 'is successful for dm when using user handle' do
        expect(Chat.count).to eq(1)
        expect do
          post :create, params: { commit: '@User', participant: "@" + @user2.username }
        end.to change(Chat, :count).by(1) and change(ChatPreference, :count).by(2)
        expect(response).to have_http_status(:no_content)
      end

      it 'is successful for group when chat is site enabled' do
        group = FactoryBot.create(:group)
        group.add_member(@user1, group.user)
        expect(Chat.count).to eq(1)
        expect do
          post :create, params: { commit: 'Group', participant: group.name }
        end.to change(Chat, :count).by(1) and change(ChatPreference, :count).by(2)
        expect(response).to have_http_status(:no_content)
      end

      it 'is does not create new chat if dm already exists' do
        post :create, params: { commit: '@User', participant: @user2.username }
        expect(Chat.count).to eq(2)
        expect do
          post :create, params: { commit: '@User', participant: @user2.username }
        end.to change(Chat, :count).by(0) and change(ChatPreference, :count).by(0)
        expect(response).to have_http_status(:no_content)
      end

      it 'is does not create new chat if group chat already exists' do
        expect(Chat.count).to eq(1)
        expect do
          post :create, params: { commit: 'Group', participant: @group.name }
        end.to change(Chat, :count).by(0) and change(ChatPreference, :count).by(0)
        expect(response).to have_http_status(:no_content)
      end

      it 'unhides dm chat for current user' do
        post :create, params: { commit: '@User', participant: @user2.username }
        cp = ChatPreference.find_by(user: @user1, chat: Chat.last)
        cp.update(hide: true)
        expect(cp.hide).to eq(true)
        expect do
          post :create, params: { commit: '@User', participant: @user2.username }
        end.to change(Chat, :count).by(0) and change(ChatPreference, :count).by(0)
        expect(response).to have_http_status(:no_content)
        expect(cp.reload.hide).to eq(false)
      end

      it 'unhides group chat for current user' do
        cp = ChatPreference.find_by(user: @user1, chat: @chat)
        cp.update(hide: true)
        expect(cp.hide).to eq(true)
        expect do
          post :create, params: { commit: 'Group', participant: @group.name }
        end.to change(Chat, :count).by(0) and change(ChatPreference, :count).by(0)
        expect(response).to have_http_status(:no_content)
        expect(cp.reload.hide).to eq(false)
      end

      it 'is not successful when chat is site disabled' do
        @siteSettings.update(chat: false)
        group = FactoryBot.create(:group)
        group.add_member(@user1, group.user)
        expect(Chat.count).to eq(1)
        expect do
          post :create, params: { commit: 'Group', participant: group.name }
        end.to change(Chat, :count).by(0) and change(ChatPreference, :count).by(0)
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it 'is not successful when username is invalid' do
        expect(Chat.count).to eq(1)
        expect do
          post :create, params: { commit: '@User', participant: "notausername" }
        end.to change(Chat, :count).by(0) and change(ChatPreference, :count).by(0)
        expect(response).to have_http_status(:no_content)
      end

      it 'is not successful when group name is invalid' do
        expect(Chat.count).to eq(1)
        expect do
          post :create, params: { commit: 'Group', participant: "not a group" }
        end.to change(Chat, :count).by(0) and change(ChatPreference, :count).by(0)
        expect(response).to have_http_status(:no_content)
      end
    end

    context 'for non-chat participant user' do
      include_context 'when non-chat participant logged in'

      it 'is not successful for group' do
        group = FactoryBot.create(:group)
        expect(Chat.count).to eq(1)
        expect do
          post :create, params: { commit: 'Group', participant: group.name }
        end.to change(Chat, :count).by(0) and change(ChatPreference, :count).by(0)
        expect(response).to have_http_status(:no_content)
      end
    end

    context 'when not logged in' do

      it 'is not successful for group' do
        expect(Chat.count).to eq(1)
        expect do
          post :create, params: { commit: '@User', participant: @user2.username }
        end.to change(Chat, :count).by(0) and change(ChatPreference, :count).by(0)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
      end

      it 'is not successful for dm' do
        group = FactoryBot.create(:group)
        group.add_member(@user1, group.user)
        expect(Chat.count).to eq(1)
        expect do
          post :create, params: { commit: 'Group', participant: group.name }
        end.to change(Chat, :count).by(0) and change(ChatPreference, :count).by(0)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end

  describe 'POST hide' do
    context 'for chat participant user' do
      include_context 'when chat participant logged in'

      it 'is successful when chat is site enabled' do
        cp = ChatPreference.where(chat: @chat, user: @user1).first
        expect(cp.hide).to eq(false)
        post :hide, params: { id: @chat.id }
        expect(response).to have_http_status(:no_content)
        expect(cp.reload.hide).to eq(true)
      end

      it 'is not successful when chat is site disabled' do
        @siteSettings.update(chat: false)
        cp = ChatPreference.where(chat: @chat, user: @user1).first
        expect(cp.hide).to eq(false)
        post :hide, params: { id: @chat.id }
        expect(cp.reload.hide).to eq(false)
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it 'is not successful when chat_id is invalid' do
        cp = ChatPreference.where(chat: @chat, user: @user1).first
        expect(cp.hide).to eq(false)
        post :hide, params: { id: "not an id" }
        expect(cp.reload.hide).to eq(false)
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end
    end

    context 'for non-chat participant user' do
      include_context 'when non-chat participant logged in'

      it 'is not successful when chat is site enabled' do
        post :hide, params: { id: @chat.id }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
        expect(flash[:alert]).to match(/Not authorized./)
      end
    end

    context 'when not logged in' do

      it 'is not successful' do
        post :hide, params: { id: @chat.id }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end

  describe 'POST pin' do
    context 'for chat participant user' do
      include_context 'when chat participant logged in'

      it 'is successful when chat is site enabled' do
        cp = ChatPreference.where(chat: @chat, user: @user1).first
        expect(cp.pin).to eq(false)
        post :pin, params: { id: @chat.id }
        expect(response).to have_http_status(:no_content)
        expect(cp.reload.pin).to eq(true)
      end

      it 'is not successful when chat is site disabled' do
        @siteSettings.update(chat: false)
        cp = ChatPreference.where(chat: @chat, user: @user1).first
        expect(cp.pin).to eq(false)
        post :pin, params: { id: @chat.id }
        expect(cp.reload.pin).to eq(false)
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it 'is not successful when chat_id is invalid' do
        cp = ChatPreference.where(chat: @chat, user: @user1).first
        expect(cp.pin).to eq(false)
        post :pin, params: { id: "not an id" }
        expect(cp.reload.pin).to eq(false)
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end
    end

    context 'for non-chat participant user' do
      include_context 'when non-chat participant logged in'

      it 'is not successful when chat is site enabled' do
        post :pin, params: { id: @chat.id }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
        expect(flash[:alert]).to match(/Not authorized./)
      end
    end

    context 'when not logged in' do

      it 'is not successful' do
        post :pin, params: { id: @chat.id }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end

  describe 'POST unpin' do
    context 'for chat participant user' do
      include_context 'when chat participant logged in'

      it 'is successful when chat is site enabled' do
        cp = ChatPreference.where(chat: @chat, user: @user1).first
        cp.update(pin: true)
        expect(cp.pin).to eq(true)
        post :unpin, params: { id: @chat.id }
        expect(response).to have_http_status(:no_content)
        expect(cp.reload.pin).to eq(false)
      end

      it 'is not successful when chat is site disabled' do
        @siteSettings.update(chat: false)
        cp = ChatPreference.where(chat: @chat, user: @user1).first
        cp.update(pin: true)
        expect(cp.pin).to eq(true)
        post :unpin, params: { id: @chat.id }
        expect(cp.reload.pin).to eq(true)
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it 'is not successful when chat_id is invalid' do
        cp = ChatPreference.where(chat: @chat, user: @user1).first
        cp.update(pin: true)
        expect(cp.pin).to eq(true)
        post :unpin, params: { id: "not an id" }
        expect(cp.reload.pin).to eq(true)
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end
    end

    context 'for non-chat participant user' do
      include_context 'when non-chat participant logged in'

      it 'is not successful when chat is site enabled' do
        post :unpin, params: { id: @chat.id }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
        expect(flash[:alert]).to match(/Not authorized./)
      end
    end

    context 'when not logged in' do

      it 'is not successful' do
        post :unpin, params: { id: @chat.id }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end

  describe 'POST clear_unread_messages' do
    context 'for chat participant user' do
      include_context 'when chat participant logged in'

      it 'is successful when chat is site enabled' do
        cp = ChatPreference.where(chat: @chat, user: @user1).first
        cp.update(unread_messages: 3)
        expect(cp.unread_messages).to eq(3)
        post :clear_unread_messages, params: { id: @chat.id }
        expect(response).to have_http_status(:no_content)
        expect(cp.reload.unread_messages).to eq(0)
      end

      it 'is not successful when chat is site disabled' do
        @siteSettings.update(chat: false)
        cp = ChatPreference.where(chat: @chat, user: @user1).first
        cp.update(unread_messages: 3)
        expect(cp.unread_messages).to eq(3)
        post :clear_unread_messages, params: { id: @chat.id }
        expect(cp.reload.unread_messages).to eq(3)
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it 'is not successful when chat_id is invalid' do
        post :clear_unread_messages, params: { id: "not an id" }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end
    end

    context 'for non-chat participant user' do
      include_context 'when non-chat participant logged in'

      it 'is not successful when chat is site enabled' do
        post :clear_unread_messages, params: { id: @chat.id }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
        expect(flash[:alert]).to match(/Not authorized./)
      end
    end

    context 'when not logged in' do

      it 'is not successful' do
        post :clear_unread_messages, params: { id: @chat.id }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end
end
