require 'rails_helper'

RSpec.describe TagsController, type: :controller do
  let(:tag_name) { Faker::Lorem.word }

  before(:each) do
    reset_elasticsearch_indices

    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @user1 = FactoryBot.create(:user, :confirmed)
    @user2 = FactoryBot.create(:user, :confirmed)
    @group_owner = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group, user_id: @group_owner.id)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group.add_member(@group_member, @group.user)
    @items = []
    UserItems.item_list.each do |item|
      item = FactoryBot.create(item.singularize, :public, user_id: @user1.id)
      item.add_group(@group, item.user)
      item.collaborate(@collaborator, item.user)
      @items << item
    end

    reindex_elasticsearch_data
  end

  shared_context 'when admin logged in' do
    before(:each) do
      sign_in @admin
    end
  end

  shared_context 'when item owner logged in' do
    before(:each) do
      sign_in @user1
    end
  end

  shared_context 'when user logged in' do
    before(:each) do
      sign_in @user2
    end
  end

  shared_context 'when collaborator logged in' do
    before(:each) do
      sign_in @collaborator
    end
  end

  shared_context 'when group member logged in' do
    before(:each) do
      sign_in @group_member
    end
  end

  shared_context 'when group owner logged in' do
    before(:each) do
      sign_in @group_owner
    end
  end

  shared_context 'when tag exists' do
    before(:each) do
      @tag = FactoryBot.create(:tag)
    end
  end

  describe 'GET show' do
    render_views

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns all public, internal, and private items' do
        @items.each do |item|
          public = FactoryBot.create(item.member_route_type.to_sym, :public, user_id: @user1.id)
          internal = FactoryBot.create(item.member_route_type.to_sym, :internal, user_id: @user1.id)
          private = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
          not_tagged = FactoryBot.create(item.member_route_type.to_sym, :public, user_id: @user1.id)
          tag = FactoryBot.create(:tag)
          public.tags << tag
          internal.tags << tag
          private.tags << tag
          tag2 = FactoryBot.create(:tag)
          not_tagged.tags << tag2
          # Reindex & refresh all records
          (UserItems.item_list + ['Tag']).each do |model|
            UserItems.class(model).import
            UserItems.class(model).__elasticsearch__.refresh_index!
          end
          get :show, params: { id: tag.name }
          expect(response).to have_http_status(:ok)
          expect(response.body).to have_content(public.title)
          expect(response.body).to have_content(internal.title)
          expect(response.body).to have_content(private.title)
          expect(response.body).to_not have_content(not_tagged.title)
        end
      end
    end

    context 'for item owner' do
      include_context 'when item owner logged in'

      it "returns public, internal, and user's private tagged items" do
        @items.each do |item|
          public = FactoryBot.create(item.member_route_type.to_sym, :public, user_id: @user1.id)
          internal = FactoryBot.create(item.member_route_type.to_sym, :internal, user_id: @user1.id)
          private = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
          not_tagged = FactoryBot.create(item.member_route_type.to_sym, :public, user_id: @user1.id)
          tag = FactoryBot.create(:tag)
          public.tags << tag
          internal.tags << tag
          private.tags << tag
          tag2 = FactoryBot.create(:tag)
          not_tagged.tags << tag2
          # Reindex & refresh all records
          (UserItems.item_list + ['Tag']).each do |model|
            UserItems.class(model).import
            UserItems.class(model).__elasticsearch__.refresh_index!
          end
          get :show, params: { id: tag.name }
          expect(response).to have_http_status(:ok)
          expect(response.body).to have_content(public.title)
          expect(response.body).to have_content(internal.title)
          expect(response.body).to have_content(private.title)
          expect(response.body).to_not have_content(not_tagged.title)
        end
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it "returns other user's public and internal tagged items" do
        @items.each do |item|
          public = FactoryBot.create(item.member_route_type.to_sym, :public, user_id: @user1.id)
          internal = FactoryBot.create(item.member_route_type.to_sym, :internal, user_id: @user1.id)
          private = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
          not_tagged = FactoryBot.create(item.member_route_type.to_sym, :public, user_id: @user1.id)
          tag = FactoryBot.create(:tag)
          public.tags << tag
          internal.tags << tag
          private.tags << tag
          tag2 = FactoryBot.create(:tag)
          not_tagged.tags << tag2
          # Reindex & refresh all records
          (UserItems.item_list + ['Tag']).each do |model|
            UserItems.class(model).import
            UserItems.class(model).__elasticsearch__.refresh_index!
          end
          get :show, params: { id: tag.name }
          expect(response).to have_http_status(:ok)
          expect(response.body).to have_content(public.title)
          expect(response.body).to have_content(internal.title)
          expect(response.body).to_not have_content(private.title)
          expect(response.body).to_not have_content(not_tagged.title)
        end
      end
    end

    context 'when not logged in' do
      it 'returns only public tagged items' do
        @items.each do |item|
          public = FactoryBot.create(item.member_route_type.to_sym, :public, user_id: @user1.id)
          internal = FactoryBot.create(item.member_route_type.to_sym, :internal, user_id: @user1.id)
          private = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
          not_tagged = FactoryBot.create(item.member_route_type.to_sym, :public, user_id: @user1.id)
          tag = FactoryBot.create(:tag)
          public.tags << tag
          internal.tags << tag
          private.tags << tag
          tag2 = FactoryBot.create(:tag)
          not_tagged.tags << tag2
          # Reindex & refresh all records
          (UserItems.item_list + ['Tag']).each do |model|
            UserItems.class(model).import
            UserItems.class(model).__elasticsearch__.refresh_index!
          end
          get :show, params: { id: tag.name }
          expect(response).to have_http_status(:ok)
          expect(response.body).to have_content(public.title)
          expect(response.body).to_not have_content(internal.title)
          expect(response.body).to_not have_content(private.title)
          expect(response.body).to_not have_content(not_tagged.title)
        end
      end
    end
  end

  describe 'GET edit_item_list' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns 200 ok for item' do
        @items.each do |item|
          get :edit_item_list, params: { record_id: item.id, record_type: item.type }
          expect(response).to have_http_status(:ok)
        end
      end

      it 'returns 200 ok for item' do
        @items.each do |item|
          get :edit_item_list, params: { record_id: item.id, record_type: item.type }
          expect(response).to have_http_status(:ok)
        end
      end
    end

    context 'for item owner' do
      include_context 'when item owner logged in'

      it 'returns 200 ok for item' do
        @items.each do |item|
          get :edit_item_list, params: { record_id: item.id, record_type: item.type }
          expect(response).to have_http_status(:ok)
        end
      end

      it 'returns 200 ok for item' do
        @items.each do |item|
          get :edit_item_list, params: { record_id: item.id, record_type: item.type }
          expect(response).to have_http_status(:ok)
        end
      end
    end

    context 'for item collaborator' do
      include_context 'when collaborator logged in'

      it 'returns 200 ok for item' do
        @items.each do |item|
          get :edit_item_list, params: { record_id: item.id, record_type: item.type }
          expect(response).to have_http_status(:ok)
        end
      end

      it 'returns 200 ok for item' do
        @items.each do |item|
          get :edit_item_list, params: { record_id: item.id, record_type: item.type }
          expect(response).to have_http_status(:ok)
        end
      end
    end

    context 'for item group member' do
      include_context 'when group member logged in'

      it 'returns 200 ok for item' do
        @items.each do |item|
          get :edit_item_list, params: { record_id: item.id, record_type: item.type }
          expect(response).to have_http_status(:ok)
        end
      end

      it 'returns 200 ok for item' do
        @items.each do |item|
          get :edit_item_list, params: { record_id: item.id, record_type: item.type }
          expect(response).to have_http_status(:ok)
        end
      end
    end

    context 'for item group owner' do
      include_context 'when group owner logged in'

      it 'returns 200 ok for item' do
        @items.each do |item|
          get :edit_item_list, params: { record_id: item.id, record_type: item.type }
          expect(response).to have_http_status(:ok)
        end
      end

      it 'returns 200 ok for item' do
        @items.each do |item|
          get :edit_item_list, params: { record_id: item.id, record_type: item.type }
          expect(response).to have_http_status(:ok)
        end
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns found redirect for item' do
        @items.each do |item|
          get :edit_item_list, params: { record_id: item.id, record_type: item.type }
          expect(response).to have_http_status(:found)
        end
      end

      it 'returns found redirect for item' do
        @items.each do |item|
          get :edit_item_list, params: { record_id: item.id, record_type: item.type }
          expect(response).to have_http_status(:found)
        end
      end
    end

    context 'when not logged in for item' do
      it 'returns found redirect for item' do
        @items.each do |item|
          get :edit_item_list, params: { record_id: item.id, record_type: item.type }
          expect(response).to have_http_status(:found)
        end
      end

      it 'returns found redirect for item' do
        @items.each do |item|
          get :edit_item_list, params: { record_id: item.id, record_type: item.type }
          expect(response).to have_http_status(:found)
        end
      end
    end
  end

  describe 'PATCH add' do
    context 'for admin user' do
      include_context 'when admin logged in'

      context 'when tag exists' do
        include_context 'when tag exists'

        it 'creates an association to item' do
          @items.each do |item|
            expect(item.tags.count).to eq(0)
            expect do
              patch :add, params: { record_id: item.id, record_type: item.type, tag: { tag_name: @tag.name } }
            end.to change(Tag, :count).by(0)
            expect(item.reload.tags.count).to eq(1)
          end
        end

        it 'does not create an association to item if already associated' do
          @items.each do |item|
            item.tags << @tag
            expect(item.tags.count).to eq(1)
            expect do
              patch :add, params: { record_id: item.id, record_type: item.type, tag: { tag_name: @tag.name } }
            end.to change(Tag, :count).by(0)
            expect(item.reload.tags.count).to eq(1)
          end
        end
      end

      context 'when tag does not exist' do

      	it 'creates a tag and an association to item with valid data' do
          @items.each do |item|
            item = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
            expect(item.tags.count).to eq(0)
            expect do
              patch :add, params: { record_id: item.id, record_type: item.type, tag: { tag_name: 'new_tag' + item.type } }
            end.to change(Tag, :count).by(1)
            expect(item.reload.tags.count).to eq(1)
          end
        end

      	it 'does not create a tag and an association to item with invalid data' do
          @items.each do |item|
            item = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
            expect(item.tags.count).to eq(0)
            expect do
              patch :add, params: { record_id: item.id, record_type: item.type, tag: { tag_name: 'spa ces' } }
            end.to change(Tag, :count).by(0)
            expect(item.reload.tags.count).to eq(0)
          end
        end
      end
    end

    context 'for item owner' do
      include_context 'when item owner logged in'

      context 'when tag exists' do
        include_context 'when tag exists'

        it 'creates an association to item' do
          @items.each do |item|
            expect(item.tags.count).to eq(0)
            expect do
              patch :add, params: { record_id: item.id, record_type: item.type, tag: { tag_name: @tag.name } }
            end.to change(Tag, :count).by(0)
            expect(item.reload.tags.count).to eq(1)
          end
        end

        it 'does not create an association to item if already associated' do
          @items.each do |item|
            item.tags << @tag
            expect(item.tags.count).to eq(1)
            expect do
              patch :add, params: { record_id: item.id, record_type: item.type, tag: { tag_name: @tag.name } }
            end.to change(Tag, :count).by(0)
            expect(item.reload.tags.count).to eq(1)
          end
        end
      end

      context 'when tag does not exist' do

      	it 'creates a tag and an association to item with valid data' do
          @items.each do |item|
            item = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
            expect(item.tags.count).to eq(0)
            expect do
              patch :add, params: { record_id: item.id, record_type: item.type, tag: { tag_name: 'new_tag' + item.type } }
            end.to change(Tag, :count).by(1)
            expect(item.reload.tags.count).to eq(1)
          end
        end

      	it 'does not create a tag and an association to item with invalid data' do
          @items.each do |item|
            item = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
            expect(item.tags.count).to eq(0)
            expect do
              patch :add, params: { record_id: item.id, record_type: item.type, tag: { tag_name: 'spa ces' } }
            end.to change(Tag, :count).by(0)
            expect(item.reload.tags.count).to eq(0)
          end
        end
      end
    end

    context 'for item collaborator' do
      include_context 'when collaborator logged in'

      context 'when tag exists' do
        include_context 'when tag exists'

        it 'creates an association to item' do
          @items.each do |item|
            expect(item.tags.count).to eq(0)
            expect do
              patch :add, params: { record_id: item.id, record_type: item.type, tag: { tag_name: @tag.name } }
            end.to change(Tag, :count).by(0)
            expect(item.reload.tags.count).to eq(1)
          end
        end

        it 'does not create an association to item if already associated' do
          @items.each do |item|
            item.tags << @tag
            expect(item.tags.count).to eq(1)
            expect do
              patch :add, params: { record_id: item.id, record_type: item.type, tag: { tag_name: @tag.name } }
            end.to change(Tag, :count).by(0)
            expect(item.reload.tags.count).to eq(1)
          end
        end
      end

      context 'when tag does not exist' do

      	it 'creates a tag and an association to item with valid data' do
          @items.each do |item|
            item = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
            item.collaborate(@collaborator, item.user)
            expect(item.tags.count).to eq(0)
            expect do
              patch :add, params: { record_id: item.id, record_type: item.type, tag: { tag_name: 'new_tag' + item.type } }
            end.to change(Tag, :count).by(1)
            expect(item.reload.tags.count).to eq(1)
          end
        end

      	it 'does not create a tag and an association to item with invalid data' do
          @items.each do |item|
            item = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
            item.collaborate(@collaborator, item.user)
            expect(item.tags.count).to eq(0)
            expect do
              patch :add, params: { record_id: item.id, record_type: item.type, tag: { tag_name: 'spa ces' } }
            end.to change(Tag, :count).by(0)
            expect(item.reload.tags.count).to eq(0)
          end
        end
      end
    end

    context 'for item group member' do
      include_context 'when group member logged in'

      context 'when tag exists' do
        include_context 'when tag exists'

        it 'creates an association to item' do
          @items.each do |item|
            expect(item.tags.count).to eq(0)
            expect do
              patch :add, params: { record_id: item.id, record_type: item.type, tag: { tag_name: @tag.name } }
            end.to change(Tag, :count).by(0)
            expect(item.reload.tags.count).to eq(1)
          end
        end

        it 'does not create an association to item if already associated' do
          @items.each do |item|
            item.tags << @tag
            expect(item.tags.count).to eq(1)
            expect do
              patch :add, params: { record_id: item.id, record_type: item.type, tag: { tag_name: @tag.name } }
            end.to change(Tag, :count).by(0)
            expect(item.reload.tags.count).to eq(1)
          end
        end
      end

      context 'when tag does not exist' do

      	it 'creates a tag and an association to item with valid data' do
          @items.each do |item|
            item = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
            item.add_group(@group, item.user)
            expect(item.tags.count).to eq(0)
            expect do
              patch :add, params: { record_id: item.id, record_type: item.type, tag: { tag_name: 'new_tag' + item.type } }
            end.to change(Tag, :count).by(1)
            expect(item.reload.tags.count).to eq(1)
          end
        end

      	it 'does not create a tag and an association to item with invalid data' do
          @items.each do |item|
            item = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
            item.add_group(@group, item.user)
            expect(item.tags.count).to eq(0)
            expect do
              patch :add, params: { record_id: item.id, record_type: item.type, tag: { tag_name: 'spa ces' } }
            end.to change(Tag, :count).by(0)
            expect(item.reload.tags.count).to eq(0)
          end
        end
      end
    end

    context 'for item group owner' do
      include_context 'when group owner logged in'

      context 'when tag exists' do
        include_context 'when tag exists'

        it 'creates an association to item' do
          @items.each do |item|
            expect(item.tags.count).to eq(0)
            expect do
              patch :add, params: { record_id: item.id, record_type: item.type, tag: { tag_name: @tag.name } }
            end.to change(Tag, :count).by(0)
            expect(item.reload.tags.count).to eq(1)
          end
        end

        it 'does not create an association to item if already associated' do
          @items.each do |item|
            item.tags << @tag
            expect(item.tags.count).to eq(1)
            expect do
              patch :add, params: { record_id: item.id, record_type: item.type, tag: { tag_name: @tag.name } }
            end.to change(Tag, :count).by(0)
            expect(item.reload.tags.count).to eq(1)
          end
        end
      end

      context 'when tag does not exist' do

      	it 'creates a tag and an association to item with valid data' do
          @items.each do |item|
            item = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
            item.add_group(@group, item.user)
            expect(item.tags.count).to eq(0)
            expect do
              patch :add, params: { record_id: item.id, record_type: item.type, tag: { tag_name: 'new_tag' + item.type } }
            end.to change(Tag, :count).by(1)
            expect(item.reload.tags.count).to eq(1)
          end
        end

      	it 'does not create a tag and an association to item with invalid data' do
          @items.each do |item|
            item = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
            item.add_group(@group, item.user)
            expect(item.tags.count).to eq(0)
            expect do
              patch :add, params: { record_id: item.id, record_type: item.type, tag: { tag_name: 'spa ces' } }
            end.to change(Tag, :count).by(0)
            expect(item.reload.tags.count).to eq(0)
          end
        end
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      context 'when tag exists' do
        include_context 'when tag exists'

        it 'creates an association to item' do
          @items.each do |item|
            expect(item.tags.count).to eq(0)
            expect do
              patch :add, params: { record_id: item.id, record_type: item.type, tag: { tag_name: @tag.name } }
            end.to change(Tag, :count).by(0)
            expect(item.reload.tags.count).to eq(0)
          end
        end
      end

      context 'when tag does not exist' do

      	it 'creates a tag and an association to item with valid data' do
          @items.each do |item|
            item = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
            expect(item.tags.count).to eq(0)
            expect do
              patch :add, params: { record_id: item.id, record_type: item.type, tag: { tag_name: 'new_tag' } }
            end.to change(Tag, :count).by(0)
            expect(item.reload.tags.count).to eq(0)
          end
        end
      end
    end

    context 'when not logged in for item' do
      context 'when tag exists' do
        include_context 'when tag exists'

        it 'creates an association to item' do
          @items.each do |item|
            expect(item.tags.count).to eq(0)
            expect do
              patch :add, params: { record_id: item.id, record_type: item.type, tag: { tag_name: @tag.name } }
            end.to change(Tag, :count).by(0)
            expect(item.reload.tags.count).to eq(0)
          end
        end
      end

      context 'when tag does not exist' do

      	it 'creates a tag and an association to item with valid data' do
          @items.each do |item|
            item = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
            expect(item.tags.count).to eq(0)
            expect do
              patch :add, params: { record_id: item.id, record_type: item.type, tag: { tag_name: 'new_tag' } }
            end.to change(Tag, :count).by(0)
            expect(item.reload.tags.count).to eq(0)
          end
        end
      end
    end
  end

  describe 'PATCH remove' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'removes assoication to item if tag associated to other items' do
        @items.each do |item|
          item_private = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
          item = FactoryBot.create(item.member_route_type.to_sym, :private)
          tag = FactoryBot.create(:tag)
          item_private.tags << tag
          item.tags << tag
          expect(item_private.tags.count).to eq(1)
          expect do
            patch :remove, params: { record_id: item_private.id, record_type: item_private.type, tag: { tag_id: tag.id } }
          end.to change(Tag, :count).by(0)
          expect(item_private.reload.tags.count).to eq(0)
        end
      end

      it 'removes assoication to item and destroys if tag has no other associations' do
        @items.each do |item|
          item_private = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
          tag = FactoryBot.create(:tag)
          item_private.tags << tag
          expect(item_private.tags.count).to eq(1)
          expect do
            patch :remove, params: { record_id: item_private.id, record_type: item.type, tag: { tag_id: tag.id } }
          end.to change(Tag, :count).by(-1)
          expect(item_private.reload.tags.count).to eq(0)
        end
      end
    end

    context 'for item owner' do
      include_context 'when item owner logged in'

      it 'removes assoication to item if tag associated to other items' do
        @items.each do |item|
          item_private = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
          item = FactoryBot.create(item.member_route_type.to_sym, :private)
          tag = FactoryBot.create(:tag)
          item_private.tags << tag
          item.tags << tag
          expect(item_private.tags.count).to eq(1)
          expect do
            patch :remove, params: { record_id: item_private.id, record_type: item_private.type, tag: { tag_id: tag.id } }
          end.to change(Tag, :count).by(0)
          expect(item_private.reload.tags.count).to eq(0)
        end
      end

      it 'removes assoication to item and destroys if tag has no other associations' do
        @items.each do |item|
          item_private = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
          tag = FactoryBot.create(:tag)
          item_private.tags << tag
          expect(item_private.tags.count).to eq(1)
          expect do
            patch :remove, params: { record_id: item_private.id, record_type: item_private.type, tag: { tag_id: tag.id } }
          end.to change(Tag, :count).by(-1)
          expect(item_private.reload.tags.count).to eq(0)
        end
      end
    end

    context 'for item collaborator' do
      include_context 'when collaborator logged in'

      it 'removes assoication to item if tag associated to other items' do
        @items.each do |item|
          item_private = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
          item_private.collaborate(@collaborator, item_private.user)
          item = FactoryBot.create(item.member_route_type.to_sym, :private)
          tag = FactoryBot.create(:tag)
          item_private.tags << tag
          item.tags << tag
          expect(item_private.tags.count).to eq(1)
          expect do
            patch :remove, params: { record_id: item_private.id, record_type: item_private.type, tag: { tag_id: tag.id } }
          end.to change(Tag, :count).by(0)
          expect(item_private.reload.tags.count).to eq(0)
        end
      end

      it 'removes assoication to item and destroys if tag has no other associations' do
        @items.each do |item|
          item_private = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
          item_private.collaborate(@collaborator, item_private.user)
          tag = FactoryBot.create(:tag)
          item_private.tags << tag
          expect(item_private.tags.count).to eq(1)
          expect do
            patch :remove, params: { record_id: item_private.id, record_type: item_private.type, tag: { tag_id: tag.id } }
          end.to change(Tag, :count).by(-1)
          expect(item_private.reload.tags.count).to eq(0)
        end
      end
    end

    context 'for item group member' do
      include_context 'when group member logged in'

      it 'removes assoication to item if tag associated to other items' do
        @items.each do |item|
          item_private = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
          item = FactoryBot.create(item.member_route_type.to_sym, :private)
          item_private.add_group(@group, item_private.user)
          tag = FactoryBot.create(:tag)
          item_private.tags << tag
          item.tags << tag
          expect(item_private.tags.count).to eq(1)
          expect do
            patch :remove, params: { record_id: item_private.id, record_type: item_private.type, tag: { tag_id: tag.id } }
          end.to change(Tag, :count).by(0)
          expect(item_private.reload.tags.count).to eq(0)
        end
      end

      it 'removes assoication to item and destroys if tag has no other associations' do
        @items.each do |item|
          item_private = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
          item_private.add_group(@group, item_private.user)
          tag = FactoryBot.create(:tag)
          item_private.tags << tag
          expect(item_private.tags.count).to eq(1)
          expect do
            patch :remove, params: { record_id: item_private.id, record_type: item_private.type, tag: { tag_id: tag.id } }
          end.to change(Tag, :count).by(-1)
          expect(item_private.reload.tags.count).to eq(0)
        end
      end
    end

    context 'for item group owner' do
      include_context 'when group owner logged in'

      it 'removes assoication to item if tag associated to other items' do
        @items.each do |item|
          item_private = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
          item_private.add_group(@group, item_private.user)
          item = FactoryBot.create(item.member_route_type.to_sym, :private)
          tag = FactoryBot.create(:tag)
          item_private.tags << tag
          item.tags << tag
          expect(item_private.tags.count).to eq(1)
          expect do
            patch :remove, params: { record_id: item_private.id, record_type: item_private.type, tag: { tag_id: tag.id } }
          end.to change(Tag, :count).by(0)
          expect(item_private.reload.tags.count).to eq(0)
        end
      end

      it 'removes assoication to item and destroys if tag has no other associations' do
        @items.each do |item|
          item_private = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
          item_private.add_group(@group, item_private.user)
          tag = FactoryBot.create(:tag)
          item_private.tags << tag
          expect(item_private.tags.count).to eq(1)
          expect do
            patch :remove, params: { record_id: item_private.id, record_type: item_private.type, tag: { tag_id: tag.id } }
          end.to change(Tag, :count).by(-1)
          expect(item_private.reload.tags.count).to eq(0)
        end
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'removes assoication to item if tag associated to other items' do
        @items.each do |item|
          item_private = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
          item = FactoryBot.create(item.member_route_type.to_sym, :private)
          tag = FactoryBot.create(:tag)
          item_private.tags << tag
          item.tags << tag
          expect(item_private.tags.count).to eq(1)
          expect do
            patch :remove, params: { record_id: item_private.id, record_type: item_private.type, tag: { tag_id: tag.id } }
          end.to change(Tag, :count).by(0)
          expect(item_private.reload.tags.count).to eq(1)
        end
      end
    end

    context 'when not logged in for item' do
      it 'removes assoication to item if tag associated to other items' do
        @items.each do |item|
          item_private = FactoryBot.create(item.member_route_type.to_sym, :private, user_id: @user1.id)
          item = FactoryBot.create(item.member_route_type.to_sym, :private)
          tag = FactoryBot.create(:tag)
          item_private.tags << tag
          item.tags << tag
          expect(item_private.tags.count).to eq(1)
          expect do
            patch :remove, params: { record_id: item_private.id, record_type: 'Note', tag: { tag_id: tag.id } }
          end.to change(Tag, :count).by(0)
          expect(item_private.reload.tags.count).to eq(1)
        end
      end
    end
  end
end
