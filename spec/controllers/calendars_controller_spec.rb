require 'rails_helper'

RSpec.describe CalendarsController, type: :controller do
  before(:each) do
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group.add_member(@group_member, @group.user)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
  end

  shared_context 'when admin logged in' do
    before(:each) do
      sign_in @admin
    end
  end

  shared_context 'when calendar item owner logged in' do
    before(:each) do
      sign_in @owner
    end
  end

  shared_context 'when collaborator logged in' do
    before(:each) do
      sign_in @collaborator
    end
  end

  shared_context 'when group member logged in' do
    before(:each) do
      sign_in @group_member
    end
  end

  shared_context 'when user logged in' do
    before(:each) do
      sign_in @user
    end
  end

  describe 'GET index' do
    render_views

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns all calendars for other users' do
        reset_elasticsearch_indices(['calendars'])

        calendar_public1 = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_internal1 = FactoryBot.create(:calendar, :internal, user_id: @owner.id)
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        calendar_public2 = FactoryBot.create(:calendar, :public, user_id: @user.id)
        calendar_internal2 = FactoryBot.create(:calendar, :internal, user_id: @user.id)
        calendar_private2 = FactoryBot.create(:calendar, :private, user_id: @user.id)

        reindex_elasticsearch_data(['calendars'])

        get :index
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content(calendar_public1.title)
        expect(response.body).to have_content(calendar_internal1.title)
        expect(response.body).to have_content(calendar_private1.title)
        expect(response.body).to have_content(calendar_public2.title)
        expect(response.body).to have_content(calendar_internal2.title)
        expect(response.body).to have_content(calendar_private2.title)
      end
    end

    context 'for calendar item owner' do
      include_context 'when calendar item owner logged in'

      it "returns public, internal, and only the user's private and collaborated calendars" do
        reset_elasticsearch_indices(['calendars'])

        calendar_public1 = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_internal1 = FactoryBot.create(:calendar, :internal, user_id: @owner.id)
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        calendar_public2 = FactoryBot.create(:calendar, :public, user_id: @user.id)
        calendar_internal2 = FactoryBot.create(:calendar, :internal, user_id: @user.id)
        calendar_private2 = FactoryBot.create(:calendar, :private, user_id: @user.id)

        reindex_elasticsearch_data(['calendars'])

        get :index
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content(calendar_public1.title)
        expect(response.body).to have_content(calendar_internal1.title)
        expect(response.body).to have_content(calendar_private1.title)
        expect(response.body).to have_content(calendar_public2.title)
        expect(response.body).to have_content(calendar_internal2.title)
        expect(response.body).to_not have_content(calendar_private2.title)
      end
    end

    context 'for calendar collaborator' do
      include_context 'when collaborator logged in'

      it "returns public, internal, and only the user's private and collaborated calendars" do
        reset_elasticsearch_indices(['calendars'])

        calendar_public1 = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_internal1 = FactoryBot.create(:calendar, :internal, user_id: @owner.id)
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        calendar_public2 = FactoryBot.create(:calendar, :public, user_id: @user.id)
        calendar_internal2 = FactoryBot.create(:calendar, :internal, user_id: @user.id)
        calendar_private2 = FactoryBot.create(:calendar, :private, user_id: @user.id)
        calendar_private1.collaborate(@collaborator, calendar_private1.user)

        reindex_elasticsearch_data(['calendars'])

        get :index
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content(calendar_public1.title)
        expect(response.body).to have_content(calendar_internal1.title)
        expect(response.body).to have_content(calendar_private1.title)
        expect(response.body).to have_content(calendar_public2.title)
        expect(response.body).to have_content(calendar_internal2.title)
        expect(response.body).to_not have_content(calendar_private2.title)
      end
    end

    context 'for calendar group member' do
      include_context 'when group member logged in'

      it "returns public, internal, and only the user's private and collaborated calendars" do
        reset_elasticsearch_indices(['calendars'])

        calendar_public1 = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_internal1 = FactoryBot.create(:calendar, :internal, user_id: @owner.id)
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        calendar_public2 = FactoryBot.create(:calendar, :public, user_id: @user.id)
        calendar_internal2 = FactoryBot.create(:calendar, :internal, user_id: @user.id)
        calendar_private2 = FactoryBot.create(:calendar, :private, user_id: @user.id)
        calendar_private1.add_group(@group, calendar_private1.user)

        reindex_elasticsearch_data(['calendars'])

        get :index
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content(calendar_public1.title)
        expect(response.body).to have_content(calendar_internal1.title)
        expect(response.body).to have_content(calendar_private1.title)
        expect(response.body).to have_content(calendar_public2.title)
        expect(response.body).to have_content(calendar_internal2.title)
        expect(response.body).to_not have_content(calendar_private2.title)
      end
    end

    context 'when not logged in' do
      it 'returns only public calendars' do
        reset_elasticsearch_indices(['calendars'])

        calendar_public1 = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_internal1 = FactoryBot.create(:calendar, :internal, user_id: @owner.id)
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        calendar_public2 = FactoryBot.create(:calendar, :public, user_id: @user.id)
        calendar_internal2 = FactoryBot.create(:calendar, :internal, user_id: @user.id)
        calendar_private2 = FactoryBot.create(:calendar, :private, user_id: @user.id)

        reindex_elasticsearch_data(['calendars'])

        get :index
        expect(response).to have_http_status(:ok)
        expect(response.body).to have_content(calendar_public1.title)
        expect(response.body).to_not have_content(calendar_internal1.title)
        expect(response.body).to_not have_content(calendar_private1.title)
        expect(response.body).to have_content(calendar_public2.title)
        expect(response.body).to_not have_content(calendar_internal2.title)
        expect(response.body).to_not have_content(calendar_private2.title)
      end
    end
  end

  describe 'GET show' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns 200 ok for another users private calendar' do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        get :show, params: { id: calendar_private1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for calendar item owner' do
      include_context 'when calendar item owner logged in'

      it 'returns 200 ok for private calendar' do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        get :show, params: { id: calendar_private1.id }
        expect(response).to have_http_status(:ok)
      end

      it 'returns 200 ok for internal calendar' do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        get :show, params: { id: calendar_private1.id }
        expect(response).to have_http_status(:ok)
      end

      it 'returns 200 ok for public calendar' do
        calendar_public1 = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        get :show, params: { id: calendar_public1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for calendar collaborator' do
      include_context 'when collaborator logged in'

      it 'returns 200 ok for private calendar' do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        calendar_private1.collaborate(@collaborator, calendar_private1.user)
        get :show, params: { id: calendar_private1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for calendar group member' do
      include_context 'when group member logged in'

      it 'returns 200 ok for private calendar' do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        calendar_private1.add_group(@group, calendar_private1.user)
        get :show, params: { id: calendar_private1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns a 404 for a private calendar' do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        get :show, params: { id: calendar_private1.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it 'returns 200 ok for internal calendar' do
        calendar_internal1 = FactoryBot.create(:calendar, :internal, user_id: @owner.id)
        get :show, params: { id: calendar_internal1.id }
        expect(response).to have_http_status(:ok)
      end

      it 'returns 200 ok for public calendar' do
        calendar_public1 = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        get :show, params: { id: calendar_public1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when not logged in' do
      it 'returns a 404 for a private calendar' do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        get :show, params: { id: calendar_private1.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it 'returns 404 for internal calendar' do
        calendar_internal1 = FactoryBot.create(:calendar, :internal, user_id: @owner.id)
        get :show, params: { id: calendar_internal1.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it 'returns 200 ok for public calendar' do
        calendar_public1 = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        get :show, params: { id: calendar_public1.id }
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'GET new' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns 200 ok' do
        get :new
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for calendar item owner' do
      include_context 'when calendar item owner logged in'

      it 'returns 200 ok' do
        get :new
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns 200 ok' do
        get :new
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when not logged in' do
      it 'returns 302 found redirect' do
        get :new
        expect(response).to have_http_status(:found)
      end
    end
  end

  describe 'GET edit' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "returns 200 ok for another user's calendar" do
        calendar_public1 = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        get :edit, params: { id: calendar_public1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for calendar item owner' do
      include_context 'when calendar item owner logged in'

      it 'returns 200 ok' do
        calendar_public1 = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        get :edit, params: { id: calendar_public1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for calendar collaborator' do
      include_context 'when collaborator logged in'

      it 'returns 200 ok for private calendar' do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        calendar_private1.collaborate(@collaborator, calendar_private1.user)
        get :edit, params: { id: calendar_private1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for calendar group member' do
      include_context 'when group member logged in'

      it 'returns 200 ok for private calendar' do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        calendar_private1.add_group(@group, calendar_private1.user)
        get :edit, params: { id: calendar_private1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns 302 found redirect' do
        calendar_public1 = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        get :edit, params: { id: calendar_public1.id }
        expect(response).to have_http_status(:found)
      end
    end

    context 'when not logged in' do
      it 'returns 302 found redirect' do
        calendar_public1 = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        get :edit, params: { id: calendar_public1.id }
        expect(response).to have_http_status(:found)
      end
    end
  end

  describe 'POST create' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'creates new calendar with valid data' do
        expect do
          post :create,
               params: { calendar: { title: 'my title',
                                 description: 'lots of calendars', visibility: 0,
                                 user_id: @admin.id } }
        end.to change(Calendar, :count).by(1)
      end

      it 'does not create a new calendar with invalid data' do
        expect do
          post :create, params: { calendar: { description: 'lots of calendar',
                                          visibility: 0, user_id: @admin.id } }
        end.to change(Calendar, :count).by(0)
      end
    end

    context 'for calendar item owner' do
      include_context 'when calendar item owner logged in'
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'creates new calendar with valid data' do
        expect do
          post :create,
               params: { calendar: { title: 'my title',
                                 description: 'lots of calendars', visibility: 0,
                                 user_id: @owner.id } }
        end.to change(Calendar, :count).by(1)
      end

      it 'does not create a new calendar with inalid data' do
        expect do
          post :create, params: { calendar: { description: 'lots of calendars',
                                  visibility: 0, user_id: @owner.id } }
        end.to change(Calendar, :count).by(0)
      end
    end

    context 'when not logged in' do
      it 'does not create a new calendar if not logged in' do
        expect do
          post :create,
               params: { calendar: { title: 'my title',
                                 description: 'lots of calendars', visibility: 0,
                                 user_id: @owner.id } }
        end.to change(Calendar, :count).by(0)
      end
    end
  end

  describe 'PATCH update' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "updates another user's calendar with valid data" do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        patch :update, params: { id: calendar_private1.id,
                                 calendar: { title: 'updated_text' } }
        expect(response).to redirect_to(calendar_private1)
        calendar_private1.reload
        expect(calendar_private1.title).to eq('updated_text')
      end

      it "does not update another user's calendar with invalid data" do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        patch :update, params: { id: calendar_private1.id, calendar: { title: nil } }
        calendar_private1.reload
        expect(calendar_private1.title).to_not be_nil
      end
    end

    context 'for calendar item owner' do
      include_context 'when calendar item owner logged in'

      it "updates user's calendar with valid data" do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        patch :update, params: { id: calendar_private1.id,
                                     calendar: { title: 'updated_text' } }
        expect(response).to redirect_to(calendar_private1)
        calendar_private1.reload
        expect(calendar_private1.title).to eq('updated_text')
      end

      it "does not update user's calendar with invalid data" do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        sign_in @owner
        patch :update, params: { id: calendar_private1.id, calendar: { title: nil } }
        calendar_private1.reload
        expect(calendar_private1.title).to_not be_nil
      end
    end

    context 'for calendar collaborator' do
      include_context 'when collaborator logged in'

      it "updates collaborated calendar with valid data" do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        calendar_private1.collaborate(@collaborator, calendar_private1.user)
        patch :update, params: { id: calendar_private1.id,
                                     calendar: { title: 'updated_text' } }
        expect(response).to redirect_to(calendar_private1)
        calendar_private1.reload
        expect(calendar_private1.title).to eq('updated_text')
      end

      it "does not update collaborated calendar with invalid data" do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        calendar_private1.collaborate(@collaborator, calendar_private1.user)
        patch :update, params: { id: calendar_private1.id, calendar: { title: nil } }
        calendar_private1.reload
        expect(calendar_private1.title).to_not be_nil
      end
    end

    context 'for calendar group member' do
      include_context 'when group member logged in'

      it "updates collaborated calendar with valid data" do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        calendar_private1.add_group(@group, calendar_private1.user)
        patch :update, params: { id: calendar_private1.id,
                                     calendar: { title: 'updated_text' } }
        expect(response).to redirect_to(calendar_private1)
        calendar_private1.reload
        expect(calendar_private1.title).to eq('updated_text')
      end

      it "does not update collaborated calendar with invalid data" do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        calendar_private1.add_group(@group, calendar_private1.user)
        patch :update, params: { id: calendar_private1.id, calendar: { title: nil } }
        calendar_private1.reload
        expect(calendar_private1.title).to_not be_nil
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it "does not update another user's calendar" do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        patch :update, params: { id: calendar_private1.id,
                                     calendar: { title: 'updated_text' } }
        expect(response).to redirect_to(calendars_path)
        calendar_private1.reload
        expect(calendar_private1.title).to_not eq('updated_text')
      end
    end

    context 'when not logged in' do
      it 'does not update another calendar' do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        patch :update, params: { id: calendar_private1.id,
                                 calendar: { title: 'updated_text' } }
        expect(response).to redirect_to(new_user_session_path)
        calendar_private1.reload
        expect(calendar_private1.title).to_not eq('updated_text')
      end
    end
  end

  describe 'PUT update' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "updates another user's calendar with valid data" do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        put :update, params: { id: calendar_private1.id,
                               calendar: { title: 'updated_text' } }
        expect(response).to redirect_to(calendar_private1)
        calendar_private1.reload
        expect(calendar_private1.title).to eq('updated_text')
      end

      it "does not update another user's calendar with invalid data" do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        put :update, params: { id: calendar_private1.id, calendar: { title: nil } }
        calendar_private1.reload
        expect(calendar_private1.title).to_not be_nil
      end
    end

    context 'for calendar item owner' do
      include_context 'when calendar item owner logged in'

      it "updates user's calendar with valid data" do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        put :update, params: { id: calendar_private1.id,
                               calendar: { title: 'updated_text' } }
        expect(response).to redirect_to(calendar_private1)
        calendar_private1.reload
        expect(calendar_private1.title).to eq('updated_text')
      end

      it "does not update user's calendar with invalid data" do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        sign_in @owner
        put :update, params: { id: calendar_private1.id, calendar: { title: nil } }
        calendar_private1.reload
        expect(calendar_private1.title).to_not be_nil
      end
    end

    context 'for calendar collaborator' do
      include_context 'when collaborator logged in'

      it "updates collaborated calendar with valid data" do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        calendar_private1.collaborate(@collaborator, calendar_private1.user)
        put :update, params: { id: calendar_private1.id,
                                     calendar: { title: 'updated_text' } }
        expect(response).to redirect_to(calendar_private1)
        calendar_private1.reload
        expect(calendar_private1.title).to eq('updated_text')
      end

      it "does not update collaborated calendar with invalid data" do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        calendar_private1.collaborate(@collaborator, calendar_private1.user)
        put :update, params: { id: calendar_private1.id, calendar: { title: nil } }
        calendar_private1.reload
        expect(calendar_private1.title).to_not be_nil
      end
    end

    context 'for calendar group member' do
      include_context 'when group member logged in'

      it "updates collaborated calendar with valid data" do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        calendar_private1.add_group(@group, calendar_private1.user)
        put :update, params: { id: calendar_private1.id,
                                     calendar: { title: 'updated_text' } }
        expect(response).to redirect_to(calendar_private1)
        calendar_private1.reload
        expect(calendar_private1.title).to eq('updated_text')
      end

      it "does not update collaborated calendar with invalid data" do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        calendar_private1.add_group(@group, calendar_private1.user)
        put :update, params: { id: calendar_private1.id, calendar: { title: nil } }
        calendar_private1.reload
        expect(calendar_private1.title).to_not be_nil
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it "does not update another user's calendar" do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        put :update, params: { id: calendar_private1.id,
                              calendar: { title: 'updated_text' } }
        expect(response).to redirect_to(calendars_path)
        calendar_private1.reload
        expect(calendar_private1.title).to_not eq('updated_text')
      end
    end

    context 'when not logged in' do
      it "does not update user's calendar" do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        put :update, params: { id: calendar_private1.id,
                               calendar: { title: 'updated_text' } }
        expect(response).to redirect_to(new_user_session_path)
        calendar_private1.reload
        expect(calendar_private1.title).to_not eq('updated_text')
      end
    end
  end

  describe 'DELETE destroy' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "destroys a user's calendar" do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        expect do
          delete :destroy, params: { id: calendar_private1.id }
        end.to change(Calendar, :count).by(-1)
      end

      it 'redirects to calendars index' do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        delete :destroy, params: { id: calendar_private1.id }
        expect(response).to redirect_to(calendars_path)
      end
    end

    context 'for calendar item owner' do
      include_context 'when calendar item owner logged in'

      it "destroys a user's calendar" do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        expect do
          delete :destroy, params: { id: calendar_private1.id }
        end.to change(Calendar, :count).by(-1)
      end
    end

    context 'for calendar collaborator' do
      include_context 'when collaborator logged in'

      it 'redirects to calendars index' do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        calendar_private1.collaborate(@collaborator, calendar_private1.user)
        expect do
          delete :destroy, params: { id: calendar_private1.id }
        end.to change(Calendar, :count).by(0)
        expect(response).to redirect_to(calendars_path)
      end
    end

    context 'for calendar group member' do
      include_context 'when group member logged in'

      it 'redirects to calendars index' do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        calendar_private1.add_group(@group, calendar_private1.user)
        expect do
          delete :destroy, params: { id: calendar_private1.id }
        end.to change(Calendar, :count).by(0)
        expect(response).to redirect_to(calendars_path)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'redirects to calendars index' do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        delete :destroy, params: { id: calendar_private1.id }
        expect(response).to redirect_to(calendars_path)
      end

      it "does not destroy another user's calendar" do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        expect do
          delete :destroy, params: { id: calendar_private1.id }
        end.to change(Calendar, :count).by(0)
      end
    end

    context 'when not logged in' do
      it "does not destroy a user's calendar" do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        expect do
          delete :destroy, params: { id: calendar_private1.id }
        end.to change(Calendar, :count).by(0)
      end

      it 'redirects to calendars index' do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        delete :destroy, params: { id: calendar_private1.id }
        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end

  describe 'GET by_day' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns 200 ok for another users private calendar' do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        get :by_day, params: { calendar_id: calendar_private1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for calendar item owner' do
      include_context 'when calendar item owner logged in'

      it 'returns 200 ok for private calendar' do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        get :by_day, params: { calendar_id: calendar_private1.id }
        expect(response).to have_http_status(:ok)
      end

      it 'returns 200 ok for internal calendar' do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        get :by_day, params: { calendar_id: calendar_private1.id }
        expect(response).to have_http_status(:ok)
      end

      it 'returns 200 ok for public calendar' do
        calendar_public1 = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        get :by_day, params: { calendar_id: calendar_public1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for calendar collaborator' do
      include_context 'when collaborator logged in'

      it 'returns 200 ok for private calendar' do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        calendar_private1.collaborate(@collaborator, calendar_private1.user)
        get :by_day, params: { calendar_id: calendar_private1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for calendar group member' do
      include_context 'when group member logged in'

      it 'returns 200 ok for private calendar' do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        calendar_private1.add_group(@group, calendar_private1.user)
        get :by_day, params: { calendar_id: calendar_private1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns a 404 for a private calendar' do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        get :by_day, params: { calendar_id: calendar_private1.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it 'returns 200 ok for internal calendar' do
        calendar_internal1 = FactoryBot.create(:calendar, :internal, user_id: @owner.id)
        get :by_day, params: { calendar_id: calendar_internal1.id }
        expect(response).to have_http_status(:ok)
      end

      it 'returns 200 ok for public calendar' do
        calendar_public1 = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        get :by_day, params: { calendar_id: calendar_public1.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when not logged in' do
      it 'returns a 404 for a private calendar' do
        calendar_private1 = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        get :by_day, params: { calendar_id: calendar_private1.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it 'returns 404 for internal calendar' do
        calendar_internal1 = FactoryBot.create(:calendar, :internal, user_id: @owner.id)
        get :by_day, params: { calendar_id: calendar_internal1.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it 'returns 200 ok for public calendar' do
        calendar_public1 = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        post :by_day, params: { calendar_id: calendar_public1.id }
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'GET user_calendar' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns 200 ok' do
        get :user_calendar
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when calendar item owner logged in'

      it 'returns 200 ok' do
        get :user_calendar
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when not logged in' do
      it 'returns 302 found redirect' do
        get :user_calendar
        expect(response).to have_http_status(:found)
      end
    end
  end
end
