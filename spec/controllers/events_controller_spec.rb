require 'rails_helper'

RSpec.describe EventsController, type: :controller do
  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @collaborator = FactoryBot.create(:user, :confirmed)
    @group = FactoryBot.create(:group)
    @group_member = FactoryBot.create(:user, :confirmed)
    @group.add_member(@group_member, @admin)
  end

  shared_context 'when admin logged in' do
    before(:each) do
      sign_in @admin
    end
  end

  shared_context 'when calendar item owner logged in' do
    before(:each) do
      sign_in @owner
    end
  end

  shared_context 'when collaborator logged in' do
    before(:each) do
      sign_in @collaborator
    end
  end

  shared_context 'when group member logged in' do
    before(:each) do
      sign_in @group_member
    end
  end

  shared_context 'when user logged in' do
    before(:each) do
      sign_in @user
    end
  end

  describe 'GET show' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "returns 200 ok for another users private calendar's event" do
        calendar_private = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_private.id)
        get :show, params: { calendar_id: event.calendar_id, id: event.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for calendar item owner' do
      include_context 'when calendar item owner logged in'

      it "returns 200 ok for private calendar's event" do
        calendar_private = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_private.id)
        get :show, params: { calendar_id: event.calendar_id, id: event.id }
        expect(response).to have_http_status(:ok)
      end

      it "returns 200 ok for internal calendar's event" do
        calendar_internal = FactoryBot.create(:calendar, :internal, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_internal.id)
        get :show, params: { calendar_id: event.calendar_id, id: event.id }
        expect(response).to have_http_status(:ok)
      end

      it "returns 200 ok for public calendar's event" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        get :show, params: { calendar_id: event.calendar_id, id: event.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for calendar collaborator' do
      include_context 'when collaborator logged in'

      it "returns 200 ok for private calendar's event" do
        calendar_private = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        calendar_private.collaborate(@collaborator, calendar_private.user)
        event = FactoryBot.create(:event, calendar_id: calendar_private.id)
        get :show, params: { calendar_id: event.calendar_id, id: event.id }
        expect(response).to have_http_status(:ok)
      end

      it "returns 200 ok for internal calendar's event" do
        calendar_internal = FactoryBot.create(:calendar, :internal, user_id: @owner.id)
        calendar_internal.collaborate(@collaborator, calendar_internal.user)
        event = FactoryBot.create(:event, calendar_id: calendar_internal.id)
        get :show, params: { calendar_id: event.calendar_id, id: event.id }
        expect(response).to have_http_status(:ok)
      end

      it "returns 200 ok for public calendar's event" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.collaborate(@collaborator, calendar_public.user)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        get :show, params: { calendar_id: event.calendar_id, id: event.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for calendar group member' do
      include_context 'when group member logged in'

      it "returns 200 ok for private calendar's event" do
        calendar_private = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        calendar_private.add_group(@group, calendar_private.user)
        event = FactoryBot.create(:event, calendar_id: calendar_private.id)
        get :show, params: { calendar_id: event.calendar_id, id: event.id }
        expect(response).to have_http_status(:ok)
      end

      it "returns 200 ok for internal calendar's event" do
        calendar_internal = FactoryBot.create(:calendar, :internal, user_id: @owner.id)
        calendar_internal.add_group(@group, calendar_internal.user)
        event = FactoryBot.create(:event, calendar_id: calendar_internal.id)
        get :show, params: { calendar_id: event.calendar_id, id: event.id }
        expect(response).to have_http_status(:ok)
      end

      it "returns 200 ok for public calendar's event" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.add_group(@group, calendar_public.user)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        get :show, params: { calendar_id: event.calendar_id, id: event.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it "returns a 404 for a private calendar's event" do
        calendar_private = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_private.id)
        get :show, params: { calendar_id: event.calendar_id, id: event.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it "returns 200 ok for internal calendar's event" do
        calendar_internal = FactoryBot.create(:calendar, :internal, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_internal.id)
        get :show, params: { calendar_id: event.calendar_id, id: event.id }
        expect(response).to have_http_status(:ok)
      end

      it "returns 200 ok for public calendar's event" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        get :show, params: { calendar_id: event.calendar_id, id: event.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when not logged in' do
      it "returns a 404 for a private calendar's event" do
        calendar_private = FactoryBot.create(:calendar, :private, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_private.id)
        get :show, params: { calendar_id: event.calendar_id, id: event.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it "returns 404 for internal calendar's event" do
        calendar_internal = FactoryBot.create(:calendar, :internal, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_internal.id)
        get :show, params: { calendar_id: event.calendar_id, id: event.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end

      it "returns 200 ok for public calendar's event" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        get :show, params: { calendar_id: event.calendar_id, id: event.id }
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'GET new' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns 302 found redirect for calendar that user did not create' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        get :new, params: { calendar_id: calendar_public.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for calendar item owner' do
      include_context 'when calendar item owner logged in'

      it 'returns 200 ok' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        get :new, params: { calendar_id: calendar_public.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns 302 found redirect' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        get :new, params: { calendar_id: calendar_public.id }
        expect(response).to have_http_status(:found)
      end
    end

    context 'when not logged in' do
      it 'returns 302 found redirect' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        get :new, params: { calendar_id: calendar_public.id }
        expect(response).to have_http_status(:found)
      end
    end
  end

  describe 'GET edit' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns 200 ok' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        get :edit, params: { calendar_id: event.calendar_id, id: event.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for calendar item owner' do
      include_context 'when calendar item owner logged in'

      it 'returns 200 ok' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        get :edit, params: { calendar_id: event.calendar_id, id: event.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for calendar collaborator' do
      include_context 'when collaborator logged in'

      it 'returns 200 ok' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.collaborate(@collaborator, calendar_public.user)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        get :edit, params: { calendar_id: event.calendar_id, id: event.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for calendar group member' do
      include_context 'when group member logged in'

      it 'returns 200 ok' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.add_group(@group, calendar_public.user)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        get :edit, params: { calendar_id: event.calendar_id, id: event.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns 302 found redirect' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        get :edit, params: { calendar_id: event.calendar_id, id: event.id }
        expect(response).to have_http_status(:found)
      end
    end

    context 'when not logged in' do
      it 'returns 302 found redirect' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        get :edit, params: { calendar_id: event.calendar_id, id: event.id }
        expect(response).to have_http_status(:found)
      end
    end
  end

  describe 'POST create' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'creates new event with valid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        expect do
          post :create, params: {
            calendar_id: calendar_public.id, event: { summary: 'my title',
            start: DateTime.now, end: DateTime.now + 2.hours, year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        end.to change(Event, :count).by(1)
      end

      it 'does not create a new event with invalid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        expect do
          post :create, params: {
            calendar_id: calendar_public.id, event: { summary: '',
            start: DateTime.now, end: DateTime.now + 2.hours, year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        end.to change(Event, :count).by(0)
      end
    end

    context 'for calendar item owner' do
      include_context 'when calendar item owner logged in'

      it 'creates new event with valid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        expect do
          post :create, params: {
            calendar_id: calendar_public.id, event: { summary: 'my title',
            start: DateTime.now, end: DateTime.now + 2.hours, year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        end.to change(Event, :count).by(1)
      end

      it 'does not create a new event with invalid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        expect do
          post :create, params: {
            calendar_id: calendar_public.id, event: { summary: '',
            start: DateTime.now, end: DateTime.now + 2.hours, year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        end.to change(Event, :count).by(0)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'does not create a new event with valid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        expect do
          post :create, params: {
            calendar_id: calendar_public.id, event: { summary: 'my title',
            start: DateTime.now, end: DateTime.now + 2.hours, year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        end.to change(Event, :count).by(0)
      end
    end

    context 'when not logged in' do
      it 'does not create a new event' do
        calendar_public = FactoryBot.create(:calendar, :public)
        expect do
          post :create, params: {
            calendar_id: calendar_public.id, event: { summary: 'my title',
            start: DateTime.now, end: DateTime.now + 2.hours, year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        end.to change(Event, :count).by(0)
      end
    end
  end

  describe 'PATCH update' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "updates another user's calendar's event with valid data" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        patch :update, params: { calendar_id: calendar_public.id, id: event.id,
                                 event: { summary: 'updated_text', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to redirect_to(calendar_path(calendar_public.id))
      end

      it "does not update user's calendar's event with invalid data" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        patch :update, params: { calendar_id: calendar_public.id, id: event.id,
                                 event: { summary: '', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to have_http_status(422)
      end
    end

    context 'for calendar item owner' do
      include_context 'when calendar item owner logged in'

      it 'updates event with valid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        patch :update, params: { calendar_id: calendar_public.id, id: event.id,
                                 event: { summary: 'updated_text', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to redirect_to(calendar_path(calendar_public.id))
      end

      it 'does not update event with invalid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        patch :update, params: { calendar_id: calendar_public.id, id: event.id,
                                 event: { summary: '', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to have_http_status(422)
      end
    end

    context 'for calendar collaborator' do
      include_context 'when collaborator logged in'

      it 'updates event with valid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.collaborate(@collaborator,calendar_public.user)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        patch :update, params: { calendar_id: calendar_public.id, id: event.id,
                                 event: { summary: 'updated_text', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to redirect_to(calendar_path(calendar_public.id))
      end

      it 'does not update event with invalid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.collaborate(@collaborator, calendar_public.user)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        patch :update, params: { calendar_id: calendar_public.id, id: event.id,
                                 event: { summary: '', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to have_http_status(422)
      end
    end

    context 'for calendar group member' do
      include_context 'when group member logged in'

      it 'updates event with valid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.add_group(@group, calendar_public.user)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        patch :update, params: { calendar_id: calendar_public.id, id: event.id,
                                 event: { summary: 'updated_text', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to redirect_to(calendar_path(calendar_public.id))
      end

      it 'does not update event with invalid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.add_group(@group, calendar_public.user)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        patch :update, params: { calendar_id: calendar_public.id, id: event.id,
                                 event: { summary: '', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to have_http_status(422)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'does not update event' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        patch :update, params: { calendar_id: calendar_public.id, id: event.id,
                                 event: { front: 'updated_text', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to redirect_to(calendars_path)
      end
    end

    context 'when not logged in' do
      it 'does not update event' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        patch :update, params: { calendar_id: calendar_public.id, id: event.id,
                                 event: { summary: 'updated_text', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to redirect_to(user_session_path)
      end
    end
  end

  describe 'PUT update' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "updates another user's calendar's event with valid data" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        put :update,
            params: { calendar_id: calendar_public.id, id: event.id, event:
                { summary: 'updated_text', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to redirect_to(calendar_path(id: calendar_public.id))
      end

      it 'does not event with invalid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        put :update, params: { calendar_id: calendar_public.id, id: event.id,
                               event: { summary: '', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to have_http_status(422)
      end
    end

    context 'for calendar item owner' do
      include_context 'when calendar item owner logged in'

      it 'updates event with valid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        put :update,
            params: { calendar_id: calendar_public.id, id: event.id,
                      event: { summary: 'updated_text', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to redirect_to(calendar_path(id: calendar_public.id))
      end

      it 'does not update event with invalid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        put :update, params: { calendar_id: calendar_public.id, id: event.id,
                               event: { summary: '', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to have_http_status(422)
      end
    end

    context 'for calendar collaborator' do
      include_context 'when collaborator logged in'

      it 'updates event with valid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.collaborate(@collaborator, calendar_public.user)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        put :update,
            params: { calendar_id: calendar_public.id, id: event.id,
                      event: { summary: 'updated_text', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to redirect_to(calendar_path(calendar_public.id))
      end

      it 'does not update event with invalid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.collaborate(@collaborator, calendar_public.user)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        put :update, params: { calendar_id: calendar_public.id, id: event.id,
                               event: { summary: '', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to have_http_status(422)
      end
    end

    context 'for calendar group member' do
      include_context 'when group member logged in'

      it 'updates event with valid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.add_group(@group, calendar_public.user)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        put :update,
            params: { calendar_id: calendar_public.id, id: event.id,
                      event: { summary: 'updated_text', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to redirect_to(calendar_path(calendar_public.id))
      end

      it 'does not update event with invalid data' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.add_group(@group, calendar_public.user)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        put :update, params: { calendar_id: calendar_public.id, id: event.id,
                               event: { summary: '', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to have_http_status(422)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'does not update event' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        put :update,
            params: { calendar_id: calendar_public.id, id: event.id, event: {
              summary: 'updated_text', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to redirect_to(calendars_path)
      end
    end

    context 'when not logged in' do
      it 'does not update event' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        put :update,
            params: { calendar_id: calendar_public.id, id: event.id, event:
                { summary: 'updated_text', year: DateTime.now.year, month: DateTime.now.month, day: DateTime.now.day } }
        expect(response).to redirect_to(user_session_path)
      end
    end
  end

  describe 'DELETE destroy' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it "destroys a user's calendar's event" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        expect do
          delete :destroy, params: { calendar_id: calendar_public.id, id: event.id }
        end.to change(Event, :count).by(-1)
      end
    end

    context 'for calendar item owner' do
      include_context 'when calendar item owner logged in'

      it 'destroys a event' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        expect do
          delete :destroy, params: { calendar_id: calendar_public.id, id: event.id }
        end.to change(Event, :count).by(-1)
      end
    end

    context 'for calendar collaborator' do
      include_context 'when collaborator logged in'

      it 'does not destroy event' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.collaborate(@collaborator, calendar_public.user)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        expect do
        delete :destroy, params: { calendar_id: calendar_public.id, id: event.id }
        end.to change(Event, :count).by(0)
      end
    end

    context 'for calendar group member' do
      include_context 'when group member logged in'

      it 'does not destroy event' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        calendar_public.add_group(@group, calendar_public.user)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        expect do
        delete :destroy, params: { calendar_id: calendar_public.id, id: event.id }
        end.to change(Event, :count).by(0)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'does not destroy event' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        expect do
          delete :destroy, params: { calendar_id: calendar_public.id, id: event.id }
        end.to change(Event, :count).by(0)
      end

      it 'redirects to calendars index' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        delete :destroy, params: { calendar_id: calendar_public.id, id: event.id }
        expect(response).to redirect_to(calendars_path)
      end
    end

    context 'when not logged in' do
      it "does not destroy a user's calendar's event" do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        expect do
          delete :destroy, params: { calendar_id: calendar_public.id, id: event.id }
        end.to change(Event, :count).by(0)
      end

      it 'redirects to sign in' do
        calendar_public = FactoryBot.create(:calendar, :public, user_id: @owner.id)
        event = FactoryBot.create(:event, calendar_id: calendar_public.id)
        delete :destroy, params: { calendar_id: calendar_public.id, id: event.id }
        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end
end

