require 'rails_helper'

RSpec.describe GroupsController, type: :controller do
  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @owner = FactoryBot.create(:user, :confirmed)
    @user = FactoryBot.create(:user, :confirmed)
    @group_member = FactoryBot.create(:user, :confirmed)
  end

  shared_context 'when admin logged in' do
    before(:each) do
      sign_in @admin
    end
  end

  shared_context 'when owner logged in' do
    before(:each) do
      sign_in @owner
    end
  end

  shared_context 'when group member logged in' do
    before(:each) do
      sign_in @group_member
    end
  end

  shared_context 'when user logged in' do
    before(:each) do
      sign_in @user
    end
  end

  describe 'GET index' do

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns 200 ok' do
        get :index
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns 200 ok' do
        get :index
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when not logged in' do

      it 'returns found redirect' do
        get :index
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'GET show' do

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns 200 ok' do
        group = FactoryBot.create(:group)
        get :show, params: { id: group.name }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns 200 ok' do
        group = FactoryBot.create(:group)
        get :show, params: { id: group.name }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when not logged in' do

      it 'returns found redirect' do
        group = FactoryBot.create(:group)
        get :show, params: { id: group.name }
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'GET show_members' do

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns 200 ok' do
        group = FactoryBot.create(:group)
        get :show_members, params: { group_id: group.name }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns 200 ok' do
        group = FactoryBot.create(:group)
        get :show_members, params: { group_id: group.name }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when not logged in' do

      it 'returns found redirect' do
        group = FactoryBot.create(:group)
        get :show_members, params: { group_id: group.name }
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'GET show_items' do

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns 200 ok' do
        group = FactoryBot.create(:group)
        get :show_items, params: { group_id: group.name, type: 'notes' }
        expect(response).to have_http_status(:ok)
      end

      it 'returns 200 ok' do
        group = FactoryBot.create(:group)
        get :show_items, params: { group_id: group.name, type: 'decks' }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns 200 ok' do
        group = FactoryBot.create(:group)
        get :show_items, params: { group_id: group.name, type: 'notes' }
        expect(response).to have_http_status(:ok)
      end

      it 'returns 200 ok' do
        group = FactoryBot.create(:group)
        get :show_items, params: { group_id: group.name, type: 'decks' }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when not logged in' do

      it 'returns found redirect' do
        group = FactoryBot.create(:group)
        get :show_items, params: { group_id: group.name, type: 'notes' }
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end

      it 'returns found redirect' do
        group = FactoryBot.create(:group)
        get :show_items, params: { group_id: group.name, type: 'decks' }
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'GET new' do

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns 200 ok' do
        get :new
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns 200 ok' do
        get :new
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when not logged in' do

      it 'returns found redirect' do
        get :new
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'POST create' do

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'is successful with valid data' do
        expect do
          post :create,
               params: { group: { name: 'my new group',
                                 description: 'very important team' } }
        end.to change(Group, :count).by(1)
      end

      it 'is not successful with invalid data' do
        expect do
          post :create,
                params: { group: { description: 'very important team' } }
        end.to change(Group, :count).by(0)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'is successful with valid data' do
        expect do
          post :create,
               params: { group: { name: 'my new group',
                                 description: 'very important team' } }
        end.to change(Group, :count).by(1)
      end

      it 'is not successful with invalid data' do
        expect do
          post :create,
                params: { group: { description: 'very important team' } }
        end.to change(Group, :count).by(0)
      end
    end

    context 'when not logged in' do

      it 'returns found redirect' do
        expect do
          post :create,
               params: { group: { name: 'my new group',
                                 description: 'very important team' } }
        end.to change(Group, :count).by(0)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'GET edit' do

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns 200 ok' do
        group = FactoryBot.create(:group)
        get :edit, params: { id: group.name }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for group owner' do
      include_context 'when owner logged in'

      it 'returns 200 ok' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        get :edit, params: { id: group.name }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for group member' do
      include_context 'when group member logged in'

      it 'returns 200 ok' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        group.add_member(@group_member, group.user)
        get :edit, params: { id: group.name }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns found redirect' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        get :edit, params: { id: group.name }
        expect(response).to redirect_to(groups_path)
        expect(flash[:alert]).to match(/Not authorized./)
      end
    end

    context 'when not logged in' do

      it 'returns found redirect' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        get :edit, params: { id: group.name }
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'PATCH update' do

    context 'when admin logged in' do
      include_context 'when admin logged in'

      it 'is successful with valid data' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        patch :update, params: { id: group.name, group: { name: "new title" } }
        expect(response).to redirect_to(group.reload)
        expect(group.reload.name).to eq('new title')
      end

      it 'is not successful with invalid data' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        patch :update, params: { id: group.name, group: { name: "" } }
        expect(group.reload.name).to_not be_nil
      end
    end

    context 'for group owner' do
      include_context 'when owner logged in'

      it 'is successful with valid data' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        patch :update, params: { id: group.name, group: { name: "new title" } }
        expect(response).to redirect_to(group.reload)
        expect(group.reload.name).to eq('new title')
      end

      it 'is not successful with invalid data' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        patch :update, params: { id: group.name, group: { name: "" } }
        expect(group.reload.name).to_not be_nil
      end
    end

    context 'for group member' do
      include_context 'when group member logged in'

      it 'is successful with valid data' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        group.add_member(@group_member, group.user)
        patch :update, params: { id: group.name, group: { name: "new title" } }
        expect(response).to redirect_to(group.reload)
        expect(group.reload.name).to eq('new title')
      end

      it 'is not successful with invalid data' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        group.add_member(@group_member, group.user)
        patch :update, params: { id: group.name, group: { name: "" } }
        expect(group.reload.name).to_not be_nil
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns found redirect' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        patch :update, params: { id: group.name, group: { name: "new title" } }
        expect(response).to redirect_to(groups_path)
        expect(group.reload.name).to_not eq('new title')
      end
    end

    context 'when not logged in' do

      it 'returns found redirect' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        patch :update, params: { id: group.name, group: { name: "new title" } }
        expect(group.reload.name).to_not eq('new title')
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'DELETE destroy' do

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'is successful' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        expect do
          delete :destroy, params: { id: group.name }
        end.to change(Group, :count).by(-1)
        expect(response).to redirect_to(groups_path)
        expect(flash[:notice]).to match(/Group was successfully deleted./)
      end
    end

    context 'for group owner' do
      include_context 'when owner logged in'

      it 'is successful' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        expect do
          delete :destroy, params: { id: group.name }
        end.to change(Group, :count).by(-1)
        expect(response).to redirect_to(groups_path)
        expect(flash[:notice]).to match(/Group was successfully deleted./)
      end
    end

    context 'for group member' do
      include_context 'when group member logged in'

      it 'is not successful' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        group.add_member(@group_member, group.user)
        expect do
          delete :destroy, params: { id: group.name }
        end.to change(Group, :count).by(0)
        expect(response).to redirect_to(groups_path)
        expect(flash[:alert]).to match(/Not authorized./)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'is not successful' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        expect do
          delete :destroy, params: { id: group.name }
        end.to change(Group, :count).by(0)
        expect(response).to redirect_to(groups_path)
        expect(flash[:alert]).to match(/Not authorized./)
      end
    end

    context 'when not logged in' do

      it 'is not successful' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        expect do
          delete :destroy, params: { id: group.name }
        end.to change(Group, :count).by(0)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'GET edit_members' do

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns 200 ok' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        get :edit_members, params: { group_id: group.name }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for group owner' do
      include_context 'when owner logged in'

      it 'returns 200 ok' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        get :edit_members, params: { group_id: group.name }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for group member' do
      include_context 'when group member logged in'

      it 'returns 200 ok' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        group.add_member(@group_member, group.user)
        get :edit_members, params: { group_id: group.name }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns found redirect' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        get :edit_members, params: { group_id: group.name }
        expect(response).to redirect_to(groups_path)
        expect(flash[:alert]).to match(/Not authorized./)
      end
    end

    context 'when not logged in' do

      it 'returns found redirect' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        get :edit_members, params: { group_id: group.name }
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'PATCH change_owner' do

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'is successful with valid username' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        user = FactoryBot.create(:user, :confirmed)
        expect(group.user_id).to eq(@owner.id)
        patch :change_owner, params: { group_id: group.name, group: { username: user.username } }
        expect(response).to redirect_to(group)
        expect(group.reload.user_id).to eq(user.id)
      end

      it 'is successful with valid handle' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        user = FactoryBot.create(:user, :confirmed)
        expect(group.user_id).to eq(@owner.id)
        patch :change_owner, params: { group_id: group.name, group: { username: user.handle } }
        expect(response).to redirect_to(group)
        expect(group.reload.user_id).to eq(user.id)
      end

      it 'is not successful with invalid username' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        user = FactoryBot.create(:user, :confirmed)
        expect(group.user_id).to eq(@owner.id)
        patch :change_owner, params: { group_id: group.name, group: { username: "spa ces" } }
        expect(response).to have_http_status(:unprocessable_entity)
        expect(group.reload.user_id).to eq(@owner.id)
      end
    end

    context 'for group owner' do
      include_context 'when owner logged in'

      it 'is successful with valid username' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        user = FactoryBot.create(:user, :confirmed)
        expect(group.user_id).to eq(@owner.id)
        patch :change_owner, params: { group_id: group.name, group: { username: user.username } }
        expect(response).to redirect_to(group)
        expect(group.reload.user_id).to eq(user.id)
      end

      it 'is successful with valid handle' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        user = FactoryBot.create(:user, :confirmed)
        expect(group.user_id).to eq(@owner.id)
        patch :change_owner, params: { group_id: group.name, group: { username: user.handle } }
        expect(response).to redirect_to(group)
        expect(group.reload.user_id).to eq(user.id)
      end

      it 'is not successful with invalid username' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        user = FactoryBot.create(:user, :confirmed)
        expect(group.user_id).to eq(@owner.id)
        patch :change_owner, params: { group_id: group.name, group: { username: "spa ces" } }
        expect(response).to have_http_status(:unprocessable_entity)
        expect(group.reload.user_id).to eq(@owner.id)
      end
    end

    context 'for group member' do
      include_context 'when group member logged in'

      it 'is not successful' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        user = FactoryBot.create(:user, :confirmed)
        group.add_member(@group_member, group.user)
        expect(group.user_id).to eq(@owner.id)
        patch :change_owner, params: { group_id: group.name, group: { username: user.username } }
        expect(response).to redirect_to(groups_path)
        expect(group.reload.user_id).to eq(@owner.id)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'is not successful' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        user = FactoryBot.create(:user, :confirmed)
        expect(group.user_id).to eq(@owner.id)
        patch :change_owner, params: { group_id: group.name, group: { username: user.username } }
        expect(response).to redirect_to(groups_path)
        expect(group.reload.user_id).to eq(@owner.id)
      end
    end

    context 'when not logged in' do

      it 'is not successful' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        user = FactoryBot.create(:user, :confirmed)
        expect(group.user_id).to eq(@owner.id)
        patch :change_owner, params: { group_id: group.name, group: { username: user.username } }
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'PATCH add_member' do

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'is successful with valid username' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        user = FactoryBot.create(:user, :confirmed)
        expect(group.members.count).to eq(0)
        patch :add_member, params: { group_id: group.name, group: { username: user.username } }
        expect(response).to redirect_to(group)
        expect(group.members.count).to eq(1)
      end

      it 'is successful with valid handle' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        user = FactoryBot.create(:user, :confirmed)
        expect(group.members.count).to eq(0)
        patch :add_member, params: { group_id: group.name, group: { username: user.handle } }
        expect(response).to redirect_to(group)
        expect(group.members.count).to eq(1)
      end

      it 'is not successful with invalid username' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        user = FactoryBot.create(:user, :confirmed)
        expect(group.members.count).to eq(0)
        patch :add_member, params: { group_id: group.name, group: { username: "spa ces" } }
        expect(response).to have_http_status(:unprocessable_entity)
        expect(group.members.count).to eq(0)
      end
    end

    context 'for group owner' do
      include_context 'when owner logged in'

      it 'is successful with valid username' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        user = FactoryBot.create(:user, :confirmed)
        expect(group.members.count).to eq(0)
        patch :add_member, params: { group_id: group.name, group: { username: user.username } }
        expect(response).to redirect_to(group)
        expect(group.members.count).to eq(1)
      end

      it 'is successful with valid handle' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        user = FactoryBot.create(:user, :confirmed)
        expect(group.members.count).to eq(0)
        patch :add_member, params: { group_id: group.name, group: { username: user.handle } }
        expect(response).to redirect_to(group)
        expect(group.members.count).to eq(1)
      end

      it 'is not successful with invalid username' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        user = FactoryBot.create(:user, :confirmed)
        expect(group.members.count).to eq(0)
        patch :add_member, params: { group_id: group.name, group: { username: "spa ces" } }
        expect(response).to have_http_status(:unprocessable_entity)
        expect(group.members.count).to eq(0)
      end
    end

    context 'for group member' do
      include_context 'when group member logged in'

      it 'is successful with valid username' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        user = FactoryBot.create(:user, :confirmed)
        group.add_member(@group_member, group.user)
        expect(group.members.count).to eq(1)
        patch :add_member, params: { group_id: group.name, group: { username: user.username } }
        expect(response).to redirect_to(group)
        expect(group.members.count).to eq(2)
      end

      it 'is successful with valid handle' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        user = FactoryBot.create(:user, :confirmed)
        group.add_member(@group_member, group.user)
        expect(group.members.count).to eq(1)
        patch :add_member, params: { group_id: group.name, group: { username: user.handle } }
        expect(response).to redirect_to(group)
        expect(group.members.count).to eq(2)
      end

      it 'is not successful with invalid username' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        user = FactoryBot.create(:user, :confirmed)
        group.add_member(@group_member, group.user)
        expect(group.members.count).to eq(1)
        patch :add_member, params: { group_id: group.name, group: { username: "spa ces" } }
        expect(response).to have_http_status(:unprocessable_entity)
        expect(group.members.count).to eq(1)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'is not successful' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        user = FactoryBot.create(:user, :confirmed)
        expect(group.members.count).to eq(0)
        patch :add_member, params: { group_id: group.name, group: { username: user.username } }
        expect(response).to redirect_to(groups_path)
        expect(flash[:alert]).to match(/Not authorized./)
        expect(group.members.count).to eq(0)
      end
    end

    context 'when not logged in' do

      it 'is not successful' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        user = FactoryBot.create(:user, :confirmed)
        expect(group.members.count).to eq(0)
        patch :add_member, params: { group_id: group.name, group: { username: user.username } }
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'PATCH remove_member' do

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'is successful' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        user = FactoryBot.create(:user, :confirmed)
        group.add_member(user, group.user)
        expect(group.members.count).to eq(1)
        patch :remove_member, params: { group_id: group.name, group: { user_id: user.id } }
        expect(response).to redirect_to(group)
        expect(group.members.count).to eq(0)
      end
    end

    context 'for group owner' do
      include_context 'when owner logged in'

      it 'is successful' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        user = FactoryBot.create(:user, :confirmed)
        group.add_member(user, group.user)
        expect(group.members.count).to eq(1)
        patch :remove_member, params: { group_id: group.name, group: { user_id: user.id } }
        expect(response).to redirect_to(group)
        expect(group.members.count).to eq(0)
      end
    end

    context 'for group member' do
      include_context 'when group member logged in'

      it 'is successful' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        user = FactoryBot.create(:user, :confirmed)
        group.add_member(@group_member, group.user)
        group.add_member(user, group.user)
        expect(group.members.count).to eq(2)
        patch :remove_member, params: { group_id: group.name, group: { user_id: user.id } }
        expect(group.members.count).to eq(1)
        expect(response).to redirect_to(group_path(group))
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'is not successful' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        user = FactoryBot.create(:user, :confirmed)
        group.add_member(user, group.user)
        expect(group.members.count).to eq(1)
        patch :remove_member, params: { group_id: group.name, group: { user_id: user.id } }
        expect(response).to redirect_to(groups_path)
        expect(flash[:alert]).to match(/Not authorized./)
        expect(group.members.count).to eq(1)
      end
    end

    context 'when not logged in' do

      it 'is not successful' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        user = FactoryBot.create(:user, :confirmed)
        group.add_member(user, group.user)
        expect(group.members.count).to eq(1)
        patch :remove_member, params: { group_id: group.name, group: { user_id: user.id } }
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'DELETE destroy' do

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'is successful' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        expect do
          delete :destroy, params: { id: group.name }
        end.to change(Group, :count).by(-1)
        expect(response).to redirect_to(groups_path)
        expect(flash[:notice]).to match(/Group was successfully deleted./)
      end
    end

    context 'for group owner' do
      include_context 'when owner logged in'

      it 'is successful' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        expect do
          delete :destroy, params: { id: group.name }
        end.to change(Group, :count).by(-1)
        expect(response).to redirect_to(groups_path)
        expect(flash[:notice]).to match(/Group was successfully deleted./)
      end
    end

    context 'for group member' do
      include_context 'when group member logged in'

      it 'is not successful' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        group.add_member(@group_member, group.user)
        expect do
          delete :destroy, params: { id: group.name }
        end.to change(Group, :count).by(0)
        expect(response).to redirect_to(groups_path)
        expect(flash[:alert]).to match(/Not authorized./)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'is not successful' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        expect do
          delete :destroy, params: { id: group.name }
        end.to change(Group, :count).by(0)
        expect(response).to redirect_to(groups_path)
        expect(flash[:alert]).to match(/Not authorized./)
      end
    end

    context 'when not logged in' do

      it 'is not successful' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        expect do
          delete :destroy, params: { id: group.name }
        end.to change(Group, :count).by(0)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'DELETE delete_avatar' do

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'is successful' do
        group = FactoryBot.create(:group, :avatar, user_id: @owner.id)
        expect(group.avatar).to be_attached
        delete :delete_avatar, params: { group_id: group.name }
        expect(group.reload.avatar).to_not be_attached
        expect(response).to redirect_to(edit_group_path(group))
      end
    end

    context 'for group owner' do
      include_context 'when owner logged in'

      it 'is successful' do
        group = FactoryBot.create(:group, :avatar, user_id: @owner.id)
        expect(group.avatar).to be_attached
        delete :delete_avatar, params: { group_id: group.name }
        expect(group.reload.avatar).to_not be_attached
        expect(response).to redirect_to(edit_group_path(group))
      end
    end

    context 'for group member' do
      include_context 'when group member logged in'

      it 'is successful' do
        group = FactoryBot.create(:group, :avatar, user_id: @owner.id)
        group.add_member(@group_member, group.user)
        expect(group.avatar).to be_attached
        delete :delete_avatar, params: { group_id: group.name }
        expect(group.reload.avatar).to_not be_attached
        expect(response).to redirect_to(edit_group_path(group))
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'is not successful' do
        group = FactoryBot.create(:group, :avatar, user_id: @owner.id)
        expect(group.avatar).to be_attached
        delete :delete_avatar, params: { group_id: group.name }
        expect(group.reload.avatar).to be_attached
        expect(response).to redirect_to(groups_path)
        expect(flash[:alert]).to match(/Not authorized./)
      end
    end

    context 'when not logged in' do

      it 'is not successful' do
        group = FactoryBot.create(:group, :avatar, user_id: @owner.id)
        expect(group.avatar).to be_attached
        delete :delete_avatar, params: { group_id: group.name }
        expect(group.reload.avatar).to be_attached
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'GET membership_logs' do

    context 'for admin user' do
      include_context 'when admin logged in'

      it 'returns 200 ok' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        get :membership_logs, params: { group_id: group.name }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for group owner' do
      include_context 'when owner logged in'

      it 'returns 200 ok' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        get :membership_logs, params: { group_id: group.name }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for group member' do
      include_context 'when group member logged in'

      it 'returns 200 ok' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        group.add_member(@group_member, group.user)
        get :membership_logs, params: { group_id: group.name }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'returns found redirect' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        get :membership_logs, params: { group_id: group.name }
        expect(response).to redirect_to(groups_path)
        expect(flash[:alert]).to match(/Not authorized./)
      end
    end

    context 'when not logged in' do

      it 'returns found redirect' do
        group = FactoryBot.create(:group, user_id: @owner.id)
        get :membership_logs, params: { group_id: group.name }
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end
end
