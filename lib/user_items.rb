module UserItems
  def self.item_list
    ['calendars','notes','decks']
  end

  def self.subitem_list
    ['cards','events','recurring_events']
  end

  def self.site_searchable
    ['calendars','notes','decks','cards','events','recurring_events']
  end

  def self.item_and_subitem_list
    self.item_list + self.subitem_list
  end

  def self.class item
    item.classify.constantize
  end
end
