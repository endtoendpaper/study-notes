module SiteSearch
  def self.model_list
    UserItems.site_searchable
  end

  def self.search(page: 1, per_page: 50, admin: false, user_id: nil, item_types: nil, items_user_id: nil, owner_user_id: nil, stars_user_id: nil, group_id: nil, tag: nil)
    page = page.to_i <= 0 ? 1 : page.to_i

    # Initialize search_body with match_all query (fetch all records)
    search_body = { query: { bool: { must: { match_all: {} } } } }

    # Ensure bool key exists and initialize the filter array
    search_body[:query][:bool][:filter] ||= []

    # Add filter logic if no current_user
    if user_id.nil?
      search_body[:query][:bool][:filter] = [
        {
          bool: {
            must: [
              { term: { visibility: 2 } }
            ]
          }
        }
      ]
    # Add filter logic for current_user
    else
      unless admin
        search_body[:query][:bool][:filter] = [
          # Match (all_collaborator_ids OR visibility)
          {
            bool: {
              should: [
                { term: { all_collaborator_ids: user_id } },
                { terms: { visibility: [1, 2] } }
              ],
              minimum_should_match: 1
            }
          }
        ]
      end
    end

    # Apply additional filters
    search_body[:query][:bool][:filter] << { term: { all_collaborator_ids: items_user_id } } if items_user_id
    search_body[:query][:bool][:filter] << { term: { user_id: owner_user_id } } if owner_user_id
    search_body[:query][:bool][:filter] << { term: { stargazers: stars_user_id } } if stars_user_id
    search_body[:query][:bool][:filter] << { term: { group_ids: group_id } } if group_id
    search_body[:query][:bool][:filter] << { term: { tags: tag } } if tag

    # Pagination
    if per_page
      search_body.merge!({
        from: (page - 1) * per_page,
        size: per_page
      })
    end

    # Filter by item types if provided
    if item_types.present?
      valid_item_types = item_types.select { |item| self.model_list.include?(item) }
      indices = valid_item_types.map { |item| Object.const_get(item.classify).index_name }
    else
      indices = self.model_list.map { |item| Object.const_get(item.classify).index_name }
    end

    # Sorting by last_activity_at in descending order (most recent activity first)
    search_body[:sort] = [
      { last_activity_at: { order: 'desc' } }
    ]

    # Elasticsearch query
    search_response = Elasticsearch::Model.client.search(
      index: indices,
      body: search_body
    )

    # Extract hits
    hits = search_response['hits']['hits']
    total_hits = search_response['hits']['total']['value']

    # Fetch records from the database dynamically
    records = []
    hits.each do |hit|
      model_name = self.model_list.find { |item| Object.const_get(item.classify).index_name == hit['_index'] }
      next unless model_name # Skip if model_name is nil

      record = Object.const_get(model_name.classify).find_by(id: hit['_id'])
      next unless record # Skip if record is nil

      records << record
    end

    # Return paginated collection
    WillPaginate::Collection.create(page, per_page, total_hits) do |pager|
      pager.replace(records)
    end
  end

  def self.search_total_hits(admin: false, user_id: nil, item_types: nil, items_user_id: nil, owner_user_id: nil, stars_user_id: nil, group_id: nil, tag: nil)
    # Initialize search_body with match_all query (fetch all records)
    search_body = { query: { bool: { must: { match_all: {} } } } }

    # Ensure bool key exists and initialize the filter array
    search_body[:query][:bool][:filter] ||= []

    # Add filter logic if no current_user
    if user_id.nil?
      search_body[:query][:bool][:filter] = [
        {
          bool: {
            must: [
              { term: { visibility: 2 } }
            ]
          }
        }
      ]
    # Add filter logic for current_user
    else
      unless admin
        search_body[:query][:bool][:filter] = [
          {
            bool: {
              should: [
                { term: { all_collaborator_ids: user_id } },
                { terms: { visibility: [1, 2] } }
              ],
              minimum_should_match: 1
            }
          }
        ]
      end
    end

    # Apply additional filters
    search_body[:query][:bool][:filter] << { term: { all_collaborator_ids: items_user_id } } if items_user_id
    search_body[:query][:bool][:filter] << { term: { user_id: owner_user_id } } if owner_user_id
    search_body[:query][:bool][:filter] << { term: { stargazers: stars_user_id } } if stars_user_id
    search_body[:query][:bool][:filter] << { term: { group_ids: group_id } } if group_id
    search_body[:query][:bool][:filter] << { term: { tags: tag } } if tag

    # Filter by item types if provided
    if item_types.present?
      valid_item_types = item_types.select { |item| self.model_list.include?(item) }
      indices = valid_item_types.map { |item| Object.const_get(item.classify).index_name }
    else
      indices = self.model_list.map { |item| Object.const_get(item.classify).index_name }
    end

    # Elasticsearch query (without pagination)
    search_response = Elasticsearch::Model.client.search(
      index: indices,
      body: search_body
    )

    # Return only the total number of hits
    search_response['hits']['total']['value']
  end

  def self.query(query, page: 1, per_page: 50, admin: false, user_id: nil, item_types: nil, my_items: false, my_owned: false, my_stars: false)
    return WillPaginate::Collection.create(1, per_page, 0) { |pager| pager.replace([]) } if query.nil? || query.strip.empty?

    page = page.to_i <= 0 ? 1 : page.to_i

    # Base query
    search_body = {
      query: {
        bool: {
          must: [
            {
              multi_match: {
                query: query,
                fields: [
                          'title^2',
                          'description',
                          'tags^2',
                          'text',
                          'front',
                          'back',
                          'summary^2',
                          'name^2'
                        ],
                fuzziness: 'AUTO'
              }
            }
          ]
        }
      }
    }

    # Ensure bool key exists and initialize the filter array
    search_body[:query][:bool][:filter] ||= []

    # Add filter logic if no current_user
    if user_id.nil?
      search_body[:query][:bool][:filter] = [
        {
          bool: {
            must: [
              { term: { visibility: 2 } }
            ]
          }
        }
      ]
    # Add filter logic for current_user
    else
      unless admin
        search_body[:query][:bool][:filter] = [
          {
            bool: {
              should: [
                { term: { all_collaborator_ids: user_id } },
                { terms: { visibility: [1, 2] } }
              ],
              minimum_should_match: 1
            }
          }
        ]
      end

      # Apply additional filters
      search_body[:query][:bool][:filter] << { term: { all_collaborator_ids: user_id } } if my_items
      search_body[:query][:bool][:filter] << { term: { user_id: user_id } } if my_owned
      search_body[:query][:bool][:filter] << { term: { stargazers: user_id } } if my_stars
    end

    # Pagination
    if per_page
      search_body.merge!({
        from: (page - 1) * per_page,
        size: per_page
      })
    end

    # Filter by item types if provided
    if item_types.present?
      valid_item_types = item_types.select { |item| self.model_list.include?(item) }
      indices = valid_item_types.map { |item| Object.const_get(item.classify).index_name }
    else
      indices = self.model_list.map { |item| Object.const_get(item.classify).index_name }
    end

    # Elasticsearch query
    search_response = Elasticsearch::Model.client.search(
      index: indices,
      body: search_body
    )

    # Extract hits
    hits = search_response['hits']['hits']
    total_hits = search_response['hits']['total']['value']

    # Fetch records from the database dynamically
    records = []
    hits.each do |hit|
      model_name = self.model_list.find { |item| Object.const_get(item.classify).index_name == hit['_index'] }
      next unless model_name # Skip if model_name is nil

      record = Object.const_get(model_name.classify).find_by(id: hit['_id'])
      next unless record # Skip if record is nil

      records << record
    end

    # Return paginated collection
    WillPaginate::Collection.create(page, per_page, total_hits) do |pager|
      pager.replace(records)
    end
  end
end
