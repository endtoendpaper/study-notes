namespace :update_tables do
  desc 'Deletes tables that are not attached to an ActionText Record updated between 24-48 hours ago'
  task :delete_unmatched_tables => :environment do
    Table.delete_unmatched_tables
  end

  desc 'Deletes tables that are not attached to an ActionText Record'
  task :delete_all_unmatched_tables => :environment do
    Table.delete_all_unmatched_tables
  end
end
