namespace :elasticsearch do
  desc "Create elasticsearch indices"
  task create_indices: :environment do
    (UserItems.site_searchable + ['users','groups','tags']).each do |model|
      model_class = UserItems.class(model)
      model_class.__elasticsearch__.create_index!
      model_class = UserItems.class(model)
      model_class.import
      model_class.__elasticsearch__.refresh_index!
    end
  end
end
