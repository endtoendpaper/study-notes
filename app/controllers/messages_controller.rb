class MessagesController < ApplicationController
  before_action :chat_allowed
  before_action :authenticate_user!
  before_action :set_chat
  before_action :is_participant

  def create
    @message = @chat.messages.build(msg_params)
    @message.user_username = current_user.handle
    @message.user = current_user
    @message.save
  end

  private

    def chat_allowed
      render 'errors/not_found' unless SiteSetting.chat?
    end

    def set_chat
      @chat = Chat.find(params[:chat_id])
      render 'errors/not_found' unless @chat
    end

    def msg_params
      params.require(:message).permit(:content)
    end

    def is_participant
      redirect_to root_path, alert: 'Not authorized.' unless @chat.participant? current_user
    end

    def render_message(message)
      render(partial: 'message', locals: { message: @message })
    end
end
