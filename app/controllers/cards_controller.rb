class CardsController < ApplicationController
  before_action :set_deck
  before_action :set_card, only: %i[show edit update destroy update_position]
  before_action :authenticate_user!, except: %i[index show]
  before_action :has_owner_privileges, only: %i[ destroy ]
  before_action :has_edit_privileges, only: %i[edit update new create destroy update_position]
  before_action :has_view_privileges, only: %i[index show]

  # GET /cards or /cards.json
  def index
    @cards = @deck.cards.all.paginate(page: params[:page], per_page: 12)
    respond_to do |format|
      format.html
      format.csv { send_data @deck.cards_to_csv, filename: "cards-#{Time.now.utc.to_formatted_s(:number)}.csv" }
    end
  end

  # GET /cards/1 or /cards/1.json
  def show; end

  # GET /cards/new
  def new
    @card = @deck.cards.build
  end

  # GET /cards/1/edit
  def edit; end

  # POST /cards or /cards.json
  def create
    @card = @deck.cards.build(card_params)
    respond_to do |format|
      if @card.save && card_params[:create_another] == '1'
        format.html { redirect_to new_deck_card_path,
                                  notice: 'Card was successfully created.' }
        format.json { render :show, status: :created,
                              location: deck_card_url(@card.deck_id, @card) }
      elsif @card.save
        format.html { redirect_to deck_card_url(@card.deck_id, @card),
                  notice: 'Card was successfully created.' }
        format.json { render :show, status: :created,
              location: deck_card_url(@card.deck_id, @card) }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @card.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cards/1 or /cards/1.json
  def update
    respond_to do |format|
      if @card.update(card_params)
        format.html { redirect_to deck_card_url(@card.deck_id, @card),
                                  notice: 'Card was successfully updated.' }
        format.json { render :show, status: :ok,
                             location: deck_card_url(@card.deck_id, @card) }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @card.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cards/1 or /cards/1.json
  def destroy
    @card.destroy

    respond_to do |format|
      format.html { redirect_to deck_cards_url,
                                notice: 'Card was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  def update_position
    @card.update_display_position(params[:display_position].to_i)
    if params[:pagination] && params[:pagination][:update]
      @cards = @deck.cards.paginate(page: params[:pagination][:page], per_page: 12)
      respond_to do |format|
        format.html { head :no_content }
        format.turbo_stream { render :update_card_display_order }
      end
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_deck
      @deck = Deck.find_by_id(params[:deck_id])
      render 'errors/not_found' unless @deck
    end

    def set_card
      @card = @deck.cards.find_by_id(params[:id])
      @card ||= @deck.cards.find_by_id(params[:card_id])
      render 'errors/not_found' unless @card
    end

    # Only allow a list of trusted parameters through.
    def card_params
      params.require(:card).permit(:front, :back, :deck_id, :create_another, :whodunnit)
    end

    def has_edit_privileges
      redirect_to decks_path, alert: 'Not authorized.' unless @deck.has_edit_privileges current_user
    end

    def has_view_privileges
      render 'errors/not_found' unless @deck.has_view_privileges(current_user)
    end

    def has_owner_privileges
      redirect_to decks_path, alert: 'Not authorized.' unless @deck.has_owner_privileges current_user
    end
end
