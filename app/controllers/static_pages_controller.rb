class StaticPagesController < ApplicationController
  before_action :authenticate_user!, only: [:help, :user_management]
  before_action :admin_user, only: [:user_management]

  def home
    item_types = ['calendars','decks','notes']
    if current_user
      @items = SiteSearch.search(
        page: params[:page],
        per_page: 12,
        admin: current_user.admin?,
        user_id: current_user.id,
        item_types: item_types
      )
    else
      @items = SiteSearch.search(
        page: params[:page],
        per_page: 12,
        item_types: item_types
      )
    end
  end

  def about; end

  def help
    @help_sections = HelpSection.all
  end

  def user_management
    if params[:view_admins].present? && params[:view_admins] == '1'
      @users = User.admin_search(params[:search], admin: true, page: params[:page], per_page: 2)
    else
      @users = User.admin_search(params[:search], page: params[:page], per_page: 2)
    end
  end

  def search
    if current_user
      @items = SiteSearch.query(
        params[:query],
        page: params[:page],
        per_page: 12,
        admin: current_user.admin?,
        user_id: current_user.id,
        item_types: params[:item_types],
        my_items: params[:my_items].present?,
        my_owned: params[:my_owned].present?,
        my_stars: params[:my_stars].present?
      )
    else
      @items = SiteSearch.query(
        params[:query],
        page: params[:page],
        per_page: 12,
        item_types: params[:item_types]
      )
    end
    @tags = @items.select { |item| item.respond_to?(:tags) && item.tags.present? }
              .flat_map(&:tags)
              .uniq
              .sort_by(&:last_activity_at)
              .reverse
    @start_result = ((@items.current_page - 1) * @items.per_page) + 1
    @end_result = [@items.current_page * @items.per_page, @items.total_entries].min
  end

  private

    def admin_user
      redirect_to root_path, alert: 'Not authorized.' unless current_user.admin?
    end
end
