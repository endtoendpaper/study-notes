class CalendarsController < ApplicationController
  require 'will_paginate/array'
  include ApplicationHelper

  before_action :set_calendar, only: %i[ by_day show edit update destroy update_events_for_tz ]
  before_action :authenticate_user!, except: %i[ by_day index show update_events_for_tz ]
  before_action :has_edit_privileges, only: %i[ edit update ]
  before_action :has_owner_privileges, only: %i[ destroy ]
  before_action :has_view_privileges, only: %i[ show by_day update_events_for_tz ]
  before_action :set_page
  before_action :set_tz

  # GET /calendars or /calendars.json
  def index
    @title = 'Calendars'
    if current_user
      @calendars = SiteSearch.search(
        page: params[:page],
        per_page: 12,
        admin: current_user.admin?,
        user_id: current_user.id,
        item_types: ['calendars']
      )
    else
      @calendars = SiteSearch.search(
        page: params[:page],
        per_page: 12,
        item_types: ['calendars']
      )
    end
    @tags = @calendars.select { |item| item.respond_to?(:tags) && item.tags.present? }
      .flat_map(&:tags)
      .uniq
      .sort_by(&:last_activity_at)
      .reverse
  end

  # GET /calendars/1 or /calendars/1.json
  def show
    if params[:year].present? && params[:month].present?
      year = params[:year].to_i
      month = params[:month].to_i
    else
      year = current_timezone(current_user) ? Time.find_zone!(current_timezone(current_user)).today.year : DateTime.current.year
      month = current_timezone(current_user) ? Time.find_zone!(current_timezone(current_user)).today.month : DateTime.current.month
    end
    begin
      @current_date = Date.new(year, month, 1)
      @prev_month = @current_date - 1.month
      @next_month = @current_date + 1.month
      @weeks = Calendar.generate_calendar_grid(year, month)
      if current_user
        @monthly_events = @calendar.get_events_for(@weeks.flatten, current_timezone(current_user), 5)
      else
        @monthly_events = Hash.new
      end
      respond_to do |format|
        format.html
        format.ics { send_data @calendar.events_to_ics, filename: "#{@calendar.title.underscore}-#{Time.now.utc.to_formatted_s(:number)}.ics" }
      end
    rescue ArgumentError => e
      render 'errors/not_found'
      return
    end
  end

  def by_day

    if params[:year].present? && params[:month].present? && params[:day].present?
      year = params[:year].to_i
      month = params[:month].to_i
      day = params[:day].to_i
    else
      year = current_timezone(current_user) ? Time.find_zone!(current_timezone(current_user)).today.year : DateTime.current.year
      month = current_timezone(current_user) ? Time.find_zone!(current_timezone(current_user)).today.month : DateTime.current.month
      day = current_timezone(current_user) ? Time.find_zone!(current_timezone(current_user)).today.day : DateTime.current.day
    end
    begin
      @current_date = Date.new(year, month, day)
      @prev_day = @current_date - 1.day
      @next_day = @current_date + 1.day
    rescue ArgumentError => e
      render 'errors/not_found'
      return
    end
    @page ||= 1
    if current_user
      @daily_events = Array.wrap(@calendar.get_events_for([@current_date], current_timezone(current_user))[@current_date]).paginate(page: params[:page], per_page: 12) if current_user
    else
      @daily_events = [].paginate(page: params[:page], per_page: 12)
    end
  end

  def update_events_for_tz
    if params[:view].present? && params[:view] == "monthly"
      year = params[:year].to_i
      month = params[:month].to_i
      @current_date = Date.new(year, month, 1)
      @prev_month = @current_date - 1.month
      @next_month = @current_date + 1.month
      @weeks = Calendar.generate_calendar_grid(year, month)
      @monthly_events = @calendar.get_events_for(@weeks.flatten, @tz, 5)
      respond_to do |format|
        format.turbo_stream { render :update_monthly_events_for_tz }
        format.html { head :no_content }
      end
    elsif params[:view].present? && params[:view] == "daily"
      year = params[:year].to_i
      month = params[:month].to_i
      day = params[:day].to_i
      @current_date = Date.new(year, month, day)
      @prev_day = @current_date - 1.day
      @next_day = @current_date + 1.day
      @daily_events = Array.wrap(@calendar.get_events_for([@current_date], @tz)[@current_date]).paginate(page: params[:page], per_page: 12)
      respond_to do |format|
        format.turbo_stream { render :update_daily_events_for_tz }
        format.html { head :no_content }
      end
    end
  end

  # GET /calendars/new
  def new
    @calendar = current_user.calendars.build
  end

  # GET /calendars/1/edit
  def edit; end

  # POST /calendars or /calendars.json
  def create
    @calendar = current_user.calendars.build(calendar_params)

    respond_to do |format|
      if @calendar.save
        format.html { redirect_to calendar_url(@calendar),
                                  notice: 'Calendar was successfully created.' }
        format.json { render :show, status: :created, location: @calendar }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @calendar.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /calendars/1 or /calendars/1.json
  def update
    respond_to do |format|
      if @calendar.update(calendar_params)
        format.html { redirect_to calendar_url(@calendar),
                                  notice: 'Calendar was successfully updated.' }
        format.json { render :show, status: :ok, location: @calendar }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @calendar.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /calendars/1 or /calendars/1.json
  def destroy
    @calendar.destroy

    respond_to do |format|
      format.html { redirect_to calendars_url,
                                notice: 'Calendar was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  def user_calendar
    @title = "All Calendar Events"
    if params[:year].present? && params[:month].present?
      year = params[:year].to_i
      month = params[:month].to_i
    else
      year = current_timezone(current_user) ? Time.find_zone!(current_timezone(current_user)).today.year : DateTime.current.year
      month = current_timezone(current_user) ? Time.find_zone!(current_timezone(current_user)).today.month : DateTime.current.month
    end
    begin
      @current_date = Date.new(year, month, 1)
      @prev_month = @current_date - 1.month
      @next_month = @current_date + 1.month
      @user_calendar = 1
      @weeks = Calendar.generate_calendar_grid(year, month)
      @monthly_events = current_user.get_events_for(@weeks.flatten, current_timezone(current_user), 5)
    rescue ArgumentError => e
      render 'errors/not_found'
      return
    end
  end

  def user_calendar_by_day
    @title = "All Calendar Events"
    if params[:year].present? && params[:month].present? && params[:day].present?
      year = params[:year].to_i
      month = params[:month].to_i
      day = params[:day].to_i
    else
      year = current_timezone(current_user) ? Time.find_zone!(current_timezone(current_user)).today.year : DateTime.current.year
      month = current_timezone(current_user) ? Time.find_zone!(current_timezone(current_user)).today.month : DateTime.current.month
      day = current_timezone(current_user) ? Time.find_zone!(current_timezone(current_user)).today.day : DateTime.current.day
    end
    begin
      @current_date = Date.new(year, month, day)
      @prev_day = @current_date - 1.day
      @next_day = @current_date + 1.day
    rescue ArgumentError => e
      render 'errors/not_found'
      return
    end
    @user_calendar = 1
    @page ||= 1
    @daily_events = Array.wrap(current_user.get_events_for([@current_date], current_timezone(current_user))[@current_date]).paginate(page: params[:page], per_page: 12)
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_calendar
      if params[:id].present?
        @calendar = Calendar.find_by_id(params[:id])
      else
        @calendar = Calendar.find_by_id(params[:calendar_id])
      end
      render 'errors/not_found' unless @calendar
    end

    def set_page
      if params[:pagination] && params[:pagination][:page]
        @page = params[:pagination][:page]
      else
        @page = params[:page]
      end
    end

    def set_tz
      if params[:tz]
        @tz = params[:tz]
      end
    end

    # Only allow a list of trusted parameters through.
    def calendar_params
      params.require(:calendar).permit(:title, :description, :visibility, :background_color,
        :text_color, :create_another, :whodunnit,
        collaborations_attributes: [:id, :record_type, :record_id, :user_id, :_destroy],
        group_assignments_attributes: [:id, :record_type, :record_id, :group_id, :_destroy],
        taggings_attributes: [:id, :record_type, :record_id, :tag_id, :tag_name, :_destroy]
      )
    end

    def has_edit_privileges
      redirect_to calendars_path, alert: 'Not authorized.' unless @calendar.has_edit_privileges current_user
    end

    def has_view_privileges
      render 'errors/not_found' unless @calendar.has_view_privileges(current_user)
    end

    def has_owner_privileges
      redirect_to calendars_path, alert: 'Not authorized.' unless @calendar.has_owner_privileges current_user
    end
end
