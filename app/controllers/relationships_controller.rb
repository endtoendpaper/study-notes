class RelationshipsController < ApplicationController
  before_action :authenticate_user!
  before_action :follow_enabled

  def create
    @user = User.find(params[:followed_id])
    current_user.follow(@user)
    respond_to do |format|
      format.html { redirect_to @user }
      format.turbo_stream
    end
  end

  def destroy
    @user = Relationship.find(params[:id]).followed
    current_user.unfollow(@user)
    respond_to do |format|
      format.html { redirect_to @user, status: :see_other }
      format.turbo_stream
    end
  end

  private

    def follow_enabled
      render 'errors/not_found' unless SiteSetting.follow_users?
    end
end
