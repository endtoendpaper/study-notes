class EventsController < ApplicationController
  include ApplicationHelper

  before_action :set_calendar
  before_action :set_event, only: %i[ show edit update destroy ]
  before_action :authenticate_user!, except: %i[ show ]
  before_action :has_owner_privileges, only: %i[ destroy ]
  before_action :has_edit_privileges, only: %i[ edit update new create destroy ]
  before_action :has_view_privileges, only: %i[ show ]
  before_action :set_current_user_for_turbo, except: %i[ destroy ]
  before_action :set_dates_from_params, only: %i[ show new edit destroy ]
  before_action :set_dates_from_form_params, only: %i[ update create ]
  before_action :set_page
  before_action :set_tz

  # GET /events/1 or /events/1.json
  def show

  end

  # GET /events/new
  def new
    @event = @calendar.events.build
  end

  # GET /events/1/edit
  def edit
  end

  # POST /events or /events.json
  def create
    @event = @calendar.events.build(event_params)
    respond_to do |format|
      if @event.save
        @notice = 'Event was successfully created.'
        if event_params[:create_another] == '1'
          @event = @calendar.events.build
          @new = true
          format.turbo_stream { render :new }
          format.html { redirect_to calendar_url(@calendar),
                                    notice: 'Event was successfully created.' }
        else
          format.turbo_stream { render :show }
          format.html { redirect_to calendar_url(@calendar),
                  notice: 'Event was successfully created.' }
        end
      else
        format.turbo_stream { render :new,
                          status: :unprocessable_entity }
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1 or /events/1.json
  def update
    respond_to do |format|
      if @event.update(event_params)
        @notice = 'Event was successfully updated.'
        format.turbo_stream { render :show }
        format.html { redirect_to calendar_url(@calendar),
                                  notice: 'Event was successfully updated.' }
      else
        format.turbo_stream { render :edit,
                          status: :unprocessable_entity }
        format.html { render :edit, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1 or /events/1.json
  def destroy
    @event.destroy
    @weeks = Calendar.generate_calendar_grid(@current_date.year, @current_date.month)
    respond_to do |format|
      if @page and @user_calendar
        @daily_events = Array.wrap(current_user.get_events_for([@current_date], current_timezone(current_user))[@current_date]).paginate(page: params[:page], per_page: 12)
        format.html { redirect_to user_calendar_by_day_url(month: @current_date.month, year: @current_date.year, day: @current_date.day, page: @page),
                                  notice: 'Event was successfully deleted.' }
        format.json { head :no_content }
      elsif @user_calendar
        @monthly_events = current_user.get_events_for(@weeks.flatten, current_timezone(current_user), 5)
        format.html { redirect_to user_calendar_url(month: @current_date.month, year: @current_date.year, day: @current_date.day),
                                   notice: 'Event was successfully deleted.' }
        format.json { head :no_content }
      elsif @page
        @daily_events = Array.wrap(@calendar.get_events_for([@current_date], current_timezone(current_user))[@current_date]).paginate(page: @page, per_page: 12) if current_user
        format.html { redirect_to calendar_by_day_url(@calendar, month: @current_date.month, year: @current_date.year, day: @current_date.day, page: @page),
                                  notice: 'Event was successfully deleted.' }
        format.json { head :no_content }
      else
        @monthly_events = @calendar.get_events_for(@weeks.flatten, current_timezone(current_user), 5) if current_user
        format.html { redirect_to calendar_url(@calendar, month: @current_date.month, year: @current_date.year, day: @current_date.day),
                                   notice: 'Event was successfully deleted.' }
        format.json { head :no_content }
      end
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_calendar
      @calendar = Calendar.find_by_id(params[:calendar_id])
      render 'errors/not_found' unless @calendar
    end

    def set_event
      @event = @calendar.events.find_by_id(params[:id])
      @event ||= @calendar.events.find_by_id(params[:event_id])
      render 'errors/not_found' unless @event
    end

    # Only allow a list of trusted parameters through.
    def event_params
      event_params = params.require(:event).permit(:summary, :location, :start, :end, :description, :create_another, :day, :month, :year, :all_day, :whodunnit, :user_calendar)
      # Add correct time_zone, account for daylight savings time
      if event_params['all_day'].present? && event_params['all_day'] == "1"
        if event_params['start'].present? and event_params['start'].is_a? String
          event_params['start'] = event_params['start'] + " UTC"
        end
        if event_params['end'].present? and event_params['end'].is_a? String
          event_params['end'] = event_params['end'] + " UTC"
        end
      else
        if event_params['start'].present? and event_params['start'].is_a? String
          start_tz = event_params['start'].to_date.to_datetime + 23.hours
          start_tz = start_tz.in_time_zone(current_timezone(current_user)).strftime('%Z')
          event_params['start'] = event_params['start'] + " " + start_tz
        end
        if event_params['end'].present? and event_params['end'].is_a? String
          end_tz = event_params['end'].to_date.to_datetime + 23.hours
          end_tz = end_tz.in_time_zone(current_timezone(current_user)).strftime('%Z')
          event_params['end'] = event_params['end'] + " " + end_tz
        end
      end
      event_params
    end

    def set_dates_from_params
      if params[:year].present? && params[:month].present?
        year = params[:year].to_i
        month = params[:month].to_i
        if params[:day].present?
          day = params[:day].to_i
        else
          day = 1
        end
      else
        year = current_timezone(current_user) ? Time.find_zone!(current_timezone(current_user)).today.year : DateTime.current.year
        month = current_timezone(current_user) ? Time.find_zone!(current_timezone(current_user)).today.month : DateTime.current.month
        day = 1
      end
      begin
        @current_date = Date.new(year, month, day)
      rescue ArgumentError => e
        render 'errors/not_found'
        return
      end
      @prev_month = @current_date - 1.month
      @next_month = @current_date + 1.month
      @prev_day = @current_date - 1.day
      @next_day = @current_date + 1.day
      if params[:user_calendar].present? and params[:user_calendar] == "1"
        @user_calendar = params[:user_calendar].to_i
      end
    end

    def set_dates_from_form_params
      year = event_params['year'].to_i
      month = event_params['month'].to_i
      day = event_params['day'].to_i
      begin
        @current_date = Date.new(year, month, day)
      rescue ArgumentError => e
        render 'errors/not_found'
        return
      end
      @prev_month = @current_date - 1.month
      @next_month = @current_date + 1.month
      @prev_day = @current_date - 1.day
      @next_day = @current_date + 1.day
      if event_params['user_calendar'] and event_params['user_calendar'] == "1"
        @user_calendar = event_params['user_calendar'].to_i
      end
    end

    def set_current_user_for_turbo
      @current_user = current_user
    end

    def set_page
      if params[:pagination] && params[:pagination][:page]
        @page = params[:pagination][:page]
      else
        @page = params[:page]
      end
    end

    def set_tz
      if params[:tz]
        @tz = params[:tz]
      end
    end

    def has_edit_privileges
      redirect_to calendars_path, alert: 'Not authorized.' unless @calendar.has_edit_privileges current_user
    end

    def has_view_privileges
      render 'errors/not_found' unless @calendar.has_view_privileges(current_user)
    end

    def has_owner_privileges
      redirect_to calendars_path, alert: 'Not authorized.' unless @calendar.has_owner_privileges current_user
    end
end
