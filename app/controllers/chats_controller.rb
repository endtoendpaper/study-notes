class ChatsController < ApplicationController
  before_action :chat_allowed
  before_action :authenticate_user!
  before_action :set_chat, only: %i[ update_show load_more_messages hide pin unpin clear_unread_messages ]
  before_action :is_participant, only: %i[ update_show load_more_messages hide pin unpin clear_unread_messages ]
  before_action :set_participant, only: %i[ join_user_chat ]
  before_action :set_group, only: %i[ join_group_chat ]

  def index
    require 'will_paginate/array'
    all_chats = current_user.all_chats(page: params[:page], per_page: 50)
    @chats = all_chats[0]
    @all_preferences = all_chats[1]
  end

  def update_index
    require 'will_paginate/array'
    all_chats = current_user.all_chats(page: params[:page], per_page: 50)
    @chats = all_chats[0]
    @all_preferences = all_chats[1]
    respond_to do |format|
      format.turbo_stream { render :update_index }
      format.html { head :no_content }
    end
  end

  def update_show
    @message = @chat.messages.build
    @preferences.reload
    respond_to do |format|
      format.turbo_stream { render :update_show }
      format.html { head :no_content }
    end
  end

  def load_more_messages
    @message = @chat.messages.build
    respond_to do |format|
      format.turbo_stream { render :load_more_messages }
      format.html { head :no_content }
    end
  end

  def join_user_chat
    @chat = current_user.find_or_create_user_chat(@participant)
    @messages = @chat.messages.for_display.paginate(page: params[:page], per_page: 50)
    @message = @chat.messages.build
    @preferences = ChatPreference.find_by(user: current_user, chat: @chat)
    @preferences.update(hide: false)
    @preferences.reload
  end

  def join_group_chat
    @chat = @group.find_or_create_chat
    @messages = @chat.messages.for_display.paginate(page: params[:page], per_page: 50)
    @message = @chat.messages.build
    @preferences = ChatPreference.find_by(user: current_user, chat: @chat)
    @preferences.update(hide: false)
    @preferences.reload
  end

  def create
    if params[:commit] == '@User'
      username = params[:participant]
      username = username[1..-1] if username.slice(0) == '@'
      user = User.find_by(username: username)
      if user
        if user == current_user
          @alert = "Please enter another user's username."
          respond_to do |format|
            format.turbo_stream { render :display_create_errors }
            format.html { head :no_content }
          end
        else
          @chat = current_user.find_or_create_user_chat(User.find_by(username: username))
          @messages = @chat.messages.for_display.paginate(page: params[:page], per_page: 50)
          @message = @chat.messages.build
          @preferences = ChatPreference.find_by(user: current_user, chat: @chat)
          @preferences.update(hide: false)
          @preferences.reload
          respond_to do |format|
            format.turbo_stream { render :update_show }
            format.html { head :no_content }
          end
        end
      else
        @alert = 'Username does not exist.'
        respond_to do |format|
          format.turbo_stream { render :display_create_errors }
          format.html { head :no_content }
        end
      end
    elsif params[:commit] == 'Group'
      if Group.find_by(name: params[:participant])
        group = Group.find_by(name: params[:participant])
        if group.member?(current_user)
          @chat = group.find_or_create_chat
          @messages = @chat.messages.for_display.paginate(page: params[:page], per_page: 50)
          @message = @chat.messages.build
          @preferences = ChatPreference.find_by(user: current_user, chat: @chat)
          @preferences.update(hide: false)
          @preferences.reload
          respond_to do |format|
            format.turbo_stream { render :update_show }
            format.html { head :no_content }
          end
        else
          @alert = 'Not a group member.'
          respond_to do |format|
            format.turbo_stream { render :display_create_errors }
            format.html { head :no_content }
          end
        end
      else
        @alert = 'Group does not exist.'
        respond_to do |format|
          format.turbo_stream { render :display_create_errors }
          format.html { head :no_content }
        end
      end
    end
  end

  def hide
    require 'will_paginate/array'
    @preferences.update(hide: true)
    all_chats = current_user.all_chats(page: params[:page], per_page: 50)
    @chats = all_chats[0]
    @all_preferences = all_chats[1]
    respond_to do |format|
      format.turbo_stream { render :update_index }
      format.html { head :no_content }
    end
  end

  def pin
    @preferences.update(pin: true)
    @preferences.reload
    respond_to do |format|
      format.turbo_stream { render :update_chat_action_links }
      format.html { head :no_content }
    end
  end

  def unpin
    @preferences.update(pin: false)
    @preferences.reload
    respond_to do |format|
      format.turbo_stream { render :update_chat_action_links }
      format.html { head :no_content }
    end
  end

  def clear_unread_messages
    @preferences.update(unread_messages: 0)
    current_user.update_unread_msg_count
    current_user.broadcast_unread_msg_update
  end

  def search
    query = params[:query]
    if query.present?
      chats = Chat.search(query, per_page: 10).take(10)
      render json: chats
    else
      render json: []
    end
  end

  private

    def chat_allowed
      render 'errors/not_found' unless SiteSetting.chat?
    end

    def set_chat
      @chat = Chat.find_by(id: params[:id])
      render 'errors/not_found' if @chat.nil?
      @preferences = ChatPreference.find_by(user: current_user, chat: @chat) unless @chat.nil?
      @messages = @chat.messages.for_display.paginate(page: params[:page], per_page: 50) unless @chat.nil?
    end

    def chat_params
      params.require(:group).permit(:participant)
    end

    def is_participant
      redirect_to root_path, alert: 'Not authorized.' unless @chat.participant? current_user
    end

    def set_participant
      @participant = User.find_by(id: params[:user_id])
      if (@participant && @participant == current_user) || @participant.nil?
        redirect_to root_path, alert: "Not a valid username."
      end
    end

    def set_group
      @group = Group.find_by(id: params[:group_id])
      if @group.nil? || !@group.member?(current_user)
        redirect_to root_path, alert: "Not a member of this group."
      end
    end
end
