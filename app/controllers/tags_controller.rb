class TagsController < ApplicationController
  before_action :authenticate_user!, except: %i[show]
  before_action :set_tag, only: %i[show]
  before_action :set_record, only: %i[edit_item_list add remove]
  before_action :has_edit_privileges, only: %i[edit_item_list add remove]

  # GET /tags/1 or /tags/1.json
  def show
    if @tag
      if current_user
        @items = SiteSearch.search(
          page: params[:page],
          per_page: 12,
          admin: current_user.admin?,
          user_id: current_user.id,
          item_types: [params[:type]],
          tag: @tag.name
        )
      else
        @items = SiteSearch.search(
          page: params[:page],
          per_page: 12,
          item_types: [params[:type]],
          tag: @tag.name
        )
      end
    else
      @items = []
    end
  end

  def edit_item_list
    @tag = Tag.new
  end

  def add
    # if tag name exists
    if Tag.where(name: tag_params[:tag_name]).first
      @tag = Tag.where(name: tag_params[:tag_name]).first
      if @record.tag?(@tag)
        @tag = Tag.new
        respond_to do |format|
          format.turbo_stream { render :form_update,
                                  locals: {
                                    notice: "Tag already assigned to #{@record.type.downcase}."
                                  } }
          format.html { render @record,
            notice: "Tag already assigned to #{@record.type.downcase}." }
        end
      else
        @record.tag(@tag, current_user)
        @tag = Tag.new
        respond_to do |format|
          format.turbo_stream { render :form_update }
          format.html { render @record,
            notice: 'Tag was successfully added.'}
        end
      end
    # if tag name does not exist
    else
      @tag = Tag.new({ name: tag_params[:tag_name] })
      if @tag.save
        @record.tag(@tag, current_user)
        @tag = Tag.new
        respond_to do |format|
          format.turbo_stream { render :form_update }
          format.html { render @record }
        end
      else
        respond_to do |format|
          format.turbo_stream { render :form_update,
                                       status: :unprocessable_entity }
          format.html { render @record, status: :unprocessable_entity }
        end
      end
    end
  end

  def remove
    @tag = Tag.find(tag_params[:tag_id])
    @record.untag(@tag, current_user)
    @tag.destroy if @tag.should_delete?
    @tag = Tag.new
    respond_to do |format|
      format.turbo_stream { render :form_update }
      format.html { render @record }
    end
  end

  def get_id
    tag_name = params[:tag_name]
    id = Tag.find_by(name: tag_name)&.id
    if id
      render json: { id: id }
    else
      render json: { id: 0 }
    end
  end

  def search
    query = params[:query]
    if query.present?
      tags = Tag.search(query, page: params[:page], per_page: 10)&.pluck(:name)
      render json: tags
    else
      render json: []
    end
  end

  private

    def set_tag
      @tag = Tag.find_by(name: params[:id])
      @tag_name = @tag&.name || params[:id]
    end

    def set_record
      @record ||= params[:record_type].capitalize.constantize.find(params[:record_id])
    end

    def tag_params
      params.require(:tag).permit(:tag_id, :tag_name)
    end

    def has_edit_privileges
      redirect_to root_path, alert: 'Not authorized.' unless @record.has_edit_privileges current_user
    end
end
