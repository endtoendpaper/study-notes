class UsersController < ApplicationController
  before_action :set_user, only: %i[show edit update destroy delete_avatar following followers show_groups show_stars show_items bulk_edit_ownership bulk_transfer_ownership]
  before_action :authenticate_user!, except: [:show, :index, :following, :followers, :show_stars, :show_items]
  before_action :follow_enabled, only: [:following, :followers]
  before_action :hide_followers, only: [:following, :followers]
  before_action :hide_stars, only: [:show_stars]
  before_action :admin_user, except: [:show, :index, :following, :followers, :show_groups, :show_stars, :show_items, :get_id, :bulk_edit_ownership, :bulk_transfer_ownership, :search]
  before_action :has_edit_privileges, only: [:bulk_edit_ownership, :bulk_transfer_ownership]

  # GET /users or /users.json
  def index
    @users = User.search(params[:search], page: params[:page], per_page: 5)
  end

  # GET /users/1 or /users/1.json
  def show; end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit; end

  # PATCH/PUT /users/1 or /users/1.json
  def update
    respond_to do |format|
      if @user.update_without_password(user_params)
        format.html { redirect_to user_path(@user),
                      notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1 or /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_path,
                                notice: 'User was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  def delete_avatar
    @user.avatar.purge if @user.avatar.attached?
    respond_to do |format|
      format.html { redirect_to edit_user_path(@user) }
      format.turbo_stream
    end
  end

  def following
    @title = "Following"
    @users = @user.following.paginate(page: params[:page], per_page: 5)
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    if current_user&.admin
      @users = @user.followers.paginate(page: params[:page], per_page: 5)
    else
      @users = @user.followers.where(hide_relationships: false).paginate(page: params[:page], per_page: 5)
    end
    render 'show_follow'
  end

  def show_groups
    if params[:view_owned].present? && params[:view_owned] == '1'
      @groups = Group.search(params[:search], owner_to_filter: @user).paginate(page: params[:page], per_page: 5) if Group.search(params[:search])
    else
      @groups = Group.search(params[:search], member_to_filter: @user).paginate(page: params[:page], per_page: 5) if Group.search(params[:search])
    end
  end

  def show_stars
    item_types = ['calendars','decks','notes']
    if current_user
      @items = SiteSearch.search(
        page: params[:page],
        per_page: 12,
        admin: current_user.admin?,
        user_id: current_user.id,
        item_types: item_types,
        stars_user_id: @user.id
      )
    else
      @items = SiteSearch.search(
        page: params[:page],
        per_page: 12,
        item_types: item_types,
        stars_user_id: @user.id
      )
    end
    @tags = @items.select { |item| item.respond_to?(:tags) && item.tags.present? }
      .flat_map(&:tags)
      .uniq
      .sort_by(&:last_activity_at)
      .reverse
  end

  def show_items
    @title = @user.handle + ": " + params[:type].capitalize
    if params[:view_owned].present? && params[:view_owned] == '1' && (current_user&.id = @user.id || current_user&.admin?)
      @items = SiteSearch.search(
        page: params[:page],
        per_page: 12,
        admin: current_user.admin?,
        user_id: current_user.id,
        item_types: [params[:type]],
        items_user_id: @user.id,
        owner_user_id: @user.id
      )
    elsif current_user
      @items = SiteSearch.search(
        page: params[:page],
        per_page: 12,
        admin: current_user.admin?,
        user_id: current_user.id,
        item_types: [params[:type]],
        items_user_id: @user.id
      )
    else
      @items = SiteSearch.search(
        page: params[:page],
        per_page: 12,
        item_types: [params[:type]],
        items_user_id: @user.id
      )
    end
    @tags = @items.select { |item| item.respond_to?(:tags) && item.tags.present? }
      .flat_map(&:tags)
      .uniq
      .sort_by(&:last_activity_at)
      .reverse
  end

  def get_id
    username = params[:username]
    username = params[:username]
    username = username[1..-1] if username.slice(0) == '@'
    id = User.find_by(username: username)&.id
    if id
      render json: { id: id }
    else
      render json: { id: 0 }
    end
  end

  def search
    query = params[:query]
    if query.present?
      query = query[1..-1] if query.slice(0) == '@'
      users = User.search(query, per_page: 10)&.pluck(:username)&.map { |username| "@#{username}" }
      render json: users
    else
      render json: []
    end
  end

  def bulk_edit_ownership
  end

  def bulk_transfer_ownership
    username = params[:new_user]
    username = username[1..-1] if username.slice(0) == '@'
    new_user = User.find_by(username: username)
    item_selected = false
    UserItems.item_list.each do |item|
      if params["#{item}".to_sym] == '1'
        item_selected = true
      end
    end
    respond_to do |format|
      if !item_selected && params[:groups] != '1'
        @user.errors.add(:base, 'No options selected.')
        format.html { render :bulk_edit_ownership, status: :unprocessable_entity }
        format.json { render json: 'No options selected', status: :unprocessable_entity }
      elsif new_user == @user
        @user.errors.add(:base, 'User already owns items.')
        format.html { render :bulk_edit_ownership, status: :unprocessable_entity }
        format.json { render json: 'User already owns items.', status: :unprocessable_entity }
      elsif new_user
        if params[:groups] == '1'
          @user.groups.in_batches.each { |x| x.update(user: new_user) }
        end
        UserItems.item_list.each do |item|
          if params["#{item}".to_sym] == '1'
            @user.send("#{item}").in_batches.each { |x| x.update(user: new_user) }
          end
        end
        format.html { redirect_to user_path(@user),
          notice: "Items were successfully transferred to #{new_user.handle}." }
        format.json { render :show, status: :ok }
      else
        @user.errors.add(:base, 'Username does not exist.')
        format.html { render :bulk_edit_ownership, status: :unprocessable_entity }
        format.json { render json: 'Username does not exist.', status: :unprocessable_entity }
      end
    end
  end

  private

    def hide_stars
      if @user.hide_stars && (!current_user || (!current_user.admin && current_user.id != @user.id))
        render 'errors/not_found'
      end
    end

    def follow_enabled
      render 'errors/not_found' unless SiteSetting.follow_users?
    end

    def hide_followers
      if @user.hide_relationships && (!current_user || (!current_user.admin && current_user.id != @user.id))
        render 'errors/not_found'
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user =User.find_by(username: params[:id])
      render 'errors/not_found' unless @user
    end

    # Only allow a list of trusted parameters through.
    def user_params
      params.require(:user).permit(:avatar, :password, :password_confirmation,
                                  :email, :username, :admin, :darkmode,
                                  :time_zone, :hide_relationships, :hide_stars)
    end

    def admin_user
      redirect_to root_path, alert: 'Not authorized.' unless current_user.admin?
    end

    def has_edit_privileges
      redirect_to root_path, alert: 'Not authorized.' unless (current_user.admin? || current_user.id == @user.id)
    end
end
