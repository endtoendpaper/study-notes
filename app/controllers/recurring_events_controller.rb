class RecurringEventsController < ApplicationController
  include ApplicationHelper

  before_action :set_calendar
  before_action :set_recurring_event, only: %i[ show edit update destroy add_exception ]
  before_action :authenticate_user!, except: %i[ show ]
  before_action :has_owner_privileges, only: %i[ destroy ]
  before_action :has_edit_privileges, only: %i[ edit update new create destroy add_exception ]
  before_action :has_view_privileges, only: %i[ show ]
  before_action :set_current_user_for_turbo, except: %i[ destroy add_exception ]
  before_action :set_dates_from_params, only: %i[ show new edit destroy add_exception ]
  before_action :set_dates_from_form_params, only: %i[ update create ]
  before_action :set_event_dates_from_params, only: %i[ show edit destroy add_exception ]
  before_action :set_event_dates_from_form_params, only: %i[ update ]
  before_action :set_page
  before_action :set_tz

  # GET /recurring_events/1 or /recurring_events/1.json
  def show
  end

  # GET /recurring_events/new
  def new
    @recurring_event = @calendar.recurring_events.build
  end

  # GET /recurring_events/1/edit
  def edit
  end

  # POST /recurring_events or /recurring_events.json
  def create
    @recurring_event = @calendar.recurring_events.build(recurring_event_params)
    respond_to do |format|
      if @recurring_event.save
        @notice = 'Recurring event was successfully created.'
        if recurring_event_params[:create_another] == '1'
          @recurring_event = @calendar.recurring_events.build
          @new = true
          format.turbo_stream { render :new }
          format.html { redirect_to calendar_url(@calendar),
                                    notice: 'Recurring event was successfully created.' }
        else
          format.turbo_stream { render :show }
          format.html { redirect_to calendar_url(@calendar),
                  notice: 'Recurring event was successfully created.' }
        end
      else
        format.turbo_stream { render :new,
                          status: :unprocessable_entity }
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /recurring_events/1 or /recurring_events/1.json
  def update
    respond_to do |format|
      if @recurring_event.update(recurring_event_params)
        @notice = 'Recurring event was successfully updated.'
        format.turbo_stream { render :show }
        format.html { redirect_to calendar_url(@calendar),
                                  notice: 'Recurring event was successfully updated.' }
      else
        format.turbo_stream { render :edit,
                          status: :unprocessable_entity }
        format.html { render :edit, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /recurring_events/1 or /recurring_events/1.json
  def destroy
    @recurring_event.destroy
    @weeks = Calendar.generate_calendar_grid(@current_date.year, @current_date.month)
    respond_to do |format|
      if @page and @user_calendar
        @daily_events = Array.wrap(current_user.get_events_for([@current_date], current_timezone(current_user))[@current_date]).paginate(page: params[:page], per_page: 12)
        format.html { redirect_to user_calendar_by_day_url(month: @current_date.month, year: @current_date.year, day: @current_date.day, page: @page),
                                  notice: 'Recurring event was successfully deleted.' }
        format.json { head :no_content }
      elsif @user_calendar
        @monthly_events = current_user.get_events_for(@weeks.flatten, current_timezone(current_user), 5)
        format.html { redirect_to user_calendar_url(month: @current_date.month, year: @current_date.year, day: @current_date.day),
                                   notice: 'Recurring event was successfully deleted.' }
        format.json { head :no_content }
      elsif @page
        @daily_events = Array.wrap(@calendar.get_events_for([@current_date], current_timezone(current_user))[@current_date]).paginate(page: @page, per_page: 12) if current_user
        format.html { redirect_to calendar_by_day_url(@calendar, month: @current_date.month, year: @current_date.year, day: @current_date.day, page: @page),
                                  notice: 'Recurring event was successfully deleted.' }
        format.json { head :no_content }
      else
        @monthly_events = @calendar.get_events_for(@weeks.flatten, current_timezone(current_user), 5) if current_user
        format.html { redirect_to calendar_url(@calendar, month: @current_date.month, year: @current_date.year, day: @current_date.day),
                                   notice: 'Recurring event was successfully deleted.' }
        format.json { head :no_content }
      end
    end
  end

  def add_exception
    exception = @event_start.in_time_zone('UTC').to_date.to_s
    @recurring_event.exceptions << exception unless @recurring_event.exceptions.include? exception
    @recurring_event.save
    @weeks = Calendar.generate_calendar_grid(@current_date.year, @current_date.month)
    respond_to do |format|
      if @page and @user_calendar
        @daily_events = Array.wrap(current_user.get_events_for([@current_date], current_timezone(current_user))[@current_date]).paginate(page: params[:page], per_page: 12)
        format.html { redirect_to user_calendar_by_day_url(month: @current_date.month, year: @current_date.year, day: @current_date.day, page: @page),
                                  notice: 'Recurring event exception was successfully added.' }
        format.json { head :no_content }
      elsif @user_calendar
        @monthly_events = current_user.get_events_for(@weeks.flatten, current_timezone(current_user), 5)
        format.html { redirect_to user_calendar_url(month: @current_date.month, year: @current_date.year, day: @current_date.day),
                                   notice: 'Recurring event exception was successfully added.' }
        format.json { head :no_content }
      elsif @page
        @daily_events = Array.wrap(@calendar.get_events_for([@current_date], current_timezone(current_user))[@current_date]).paginate(page: @page, per_page: 12) if current_user
        format.html { redirect_to calendar_by_day_url(@calendar, month: @current_date.month, year: @current_date.year, day: @current_date.day, page: @page),
                                  notice: 'Recurring event exception was successfully added.' }
        format.json { head :no_content }
      else
        @monthly_events = @calendar.get_events_for(@weeks.flatten, current_timezone(current_user), 5) if current_user
        format.html { redirect_to calendar_url(@calendar, month: @current_date.month, year: @current_date.year, day: @current_date.day),
                                   notice: 'Recurring event exception was successfully added.' }
        format.json { head :no_content }
      end
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_calendar
      @calendar = Calendar.find_by_id(params[:calendar_id])
      render 'errors/not_found' unless @calendar
    end

    def set_recurring_event
      @recurring_event = @calendar.recurring_events.find_by_id(params[:id])
      @recurring_event ||= @calendar.recurring_events.find_by_id(params[:recurring_event_id])
      render 'errors/not_found' unless @recurring_event
    end

    # Only allow a list of trusted parameters through.
    def recurring_event_params
      recurring_event_params = params.require(:recurring_event).permit(:rrule, :summary, :location, :start, :end, :description, :create_another, :day, :month, :year, :all_day, :whodunnit, :event_day, :event_month, :user_calendar, :event_year, exceptions: [])
      # Add correct time_zone, account for daylight savings time
      if recurring_event_params['all_day'].present? && recurring_event_params['all_day'] == "1"
        if recurring_event_params['start'].present? and recurring_event_params['start'].is_a? String
          recurring_event_params['start'] = recurring_event_params['start'] + " UTC"
        end
        if recurring_event_params['end'].present? and recurring_event_params['end'].is_a? String
          recurring_event_params['end'] = recurring_event_params['end'] + " UTC"
        end
      else
        if recurring_event_params['rrule'] and recurring_event_params['start'].present? and recurring_event_params['start'].is_a? String
          start_tz = recurring_event_params['start'].to_date.to_datetime + 23.hours
          start_tz = start_tz.in_time_zone(current_timezone(current_user)).strftime('%Z')
          recurring_event_params['start'] = recurring_event_params['start'] + " " + start_tz
          recurring_event_params['rrule'] = RecurringEvent.convert_rrule_to_tz recurring_event_params['rrule'], current_timezone(current_user), 'UTC', recurring_event_params['start'].to_datetime
        elsif recurring_event_params['rrule'] and recurring_event_params['start'].present?
          recurring_event_params['rrule'] = RecurringEvent.convert_rrule_to_tz recurring_event_params['rrule'], current_timezone(current_user), 'UTC', recurring_event_params['start']
        elsif recurring_event_params['rrule'] and @recurring_event.start
          recurring_event_params['rrule'] = RecurringEvent.convert_rrule_to_tz recurring_event_params['rrule'], current_timezone(current_user), 'UTC', @recurring_event.start
        end
        if recurring_event_params['end'].present? and recurring_event_params['end'].is_a? String
          end_tz = recurring_event_params['end'].to_date.to_datetime + 23.hours
          end_tz = end_tz.in_time_zone(current_timezone(current_user)).strftime('%Z')
          recurring_event_params['end'] = recurring_event_params['end'] + " " + end_tz
        end
      end
      recurring_event_params
    end

    def set_dates_from_params
      if params[:year].present? && params[:month].present?
        year = params[:year].to_i
        month = params[:month].to_i
        if params[:day].present?
          day = params[:day].to_i
        else
          day = 1
        end
      else
        year = current_timezone(current_user) ? Time.find_zone!(current_timezone(current_user)).today.year : DateTime.current.year
        month = current_timezone(current_user) ? Time.find_zone!(current_timezone(current_user)).today.month : DateTime.current.month
        day = 1
      end
      begin
        @current_date = Date.new(year, month, day)
      rescue ArgumentError => e
        render 'errors/not_found'
        return
      end
      @prev_month = @current_date - 1.month
      @next_month = @current_date + 1.month
      @prev_day = @current_date - 1.day
      @next_day = @current_date + 1.day
      if params[:user_calendar].present? and params[:user_calendar] == "1"
        @user_calendar = params[:user_calendar].to_i
      end
    end

    def set_dates_from_form_params
      year = recurring_event_params['year'].to_i
      month = recurring_event_params['month'].to_i
      day = recurring_event_params['day'].to_i
      begin
        @current_date = Date.new(year, month, day)
      rescue ArgumentError => e
        render 'errors/not_found'
        return
      end
      @prev_month = @current_date - 1.month
      @next_month = @current_date + 1.month
      @prev_day = @current_date - 1.day
      @next_day = @current_date + 1.day
      if recurring_event_params['user_calendar'] and recurring_event_params['user_calendar'] == "1"
        @user_calendar = recurring_event_params['user_calendar'].to_i
      end
    end

    def set_current_user_for_turbo
      @current_user = current_user
    end

    def set_page
      if params[:pagination] && params[:pagination][:page]
        @page = params[:pagination][:page]
      else
        @page = params[:page]
      end
    end

    def set_tz
      if params[:tz]
        @tz = params[:tz]
      end
    end

    def set_event_dates_from_params
      year = params[:event_year].to_i
      month = params[:event_month].to_i
      if params[:event_day].present?
        day = params[:event_day].to_i
      else
        day = 1
      end
      begin
        @event_start = DateTime.new(year, month, day, @recurring_event.start.hour, @recurring_event.start.min)
        @event_end = @event_start + (@recurring_event.end - @recurring_event.start).to_i.seconds
      rescue ArgumentError => e
        @event_start = @recurring_event.start
        @event_end = @recurring_event.end
        return
      end
    end

    def set_event_dates_from_form_params
      year = recurring_event_params['event_year'].to_i
      month = recurring_event_params['event_month'].to_i
      day = recurring_event_params['event_day'].to_i
      begin
        @event_start = DateTime.new(year, month, day, @recurring_event.start.hour, @recurring_event.start.min)
        @event_end = @event_start + (@recurring_event.end - @recurring_event.start).to_i.seconds
      rescue ArgumentError => e
        @event_start = @recurring_event.start
        @event_end = @recurring_event.end
        return
      end
    end

    def has_edit_privileges
      redirect_to calendars_path, alert: 'Not authorized.' unless @calendar.has_edit_privileges current_user
    end

    def has_view_privileges
      render 'errors/not_found' unless @calendar.has_view_privileges(current_user)
    end

    def has_owner_privileges
      redirect_to calendars_path, alert: 'Not authorized.' unless @calendar.has_owner_privileges current_user
    end
end
