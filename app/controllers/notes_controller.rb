class NotesController < ApplicationController
  before_action :set_note, only: %i[ show edit update destroy ]
  before_action :authenticate_user!, except: %i[ index show ]
  before_action :has_edit_privileges, only: %i[ edit update ]
  before_action :has_owner_privileges, only: %i[ destroy ]
  before_action :has_view_privileges, only: [ :show ]

  # GET /notes or /notes.json
  def index
    @title = 'Notes'
    if current_user
      @notes = SiteSearch.search(
        page: params[:page],
        per_page: 12,
        admin: current_user.admin?,
        user_id: current_user.id,
        item_types: ['notes']
      )
    else
      @notes = SiteSearch.search(
        page: params[:page],
        per_page: 12,
        item_types: ['notes']
      )
    end
    @tags = @notes.select { |item| item.respond_to?(:tags) && item.tags.present? }
      .flat_map(&:tags)
      .uniq
      .sort_by(&:last_activity_at)
      .reverse
  end

  # GET /notes/1 or /notes/1.json
  def show
  end

  # GET /notes/new
  def new
    @note = current_user.notes.build
  end

  # GET /notes/1/edit
  def edit; end

  # POST /notes or /notes.json
  def create
    @note = current_user.notes.build(note_params)

    respond_to do |format|
      if @note.save && note_params[:create_another] == '1'
        format.html { redirect_to new_note_path,
                  notice: 'Note was successfully created.' }
        format.json { render :show, status: :created, location: @note }
      elsif @note.save
        format.html { redirect_to note_url(@note),
                                  notice: 'Note was successfully created.' }
        format.json { render :show, status: :created, location: @note }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @note.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /notes/1 or /notes/1.json
  def update
    respond_to do |format|
      if @note.update(note_params)
        format.html { redirect_to note_url(@note),
                                  notice: 'Note was successfully updated.' }
        format.json { render :show, status: :ok, location: @note }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @note.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /notes/1 or /notes/1.json
  def destroy
    @note.destroy

    respond_to do |format|
      format.html { redirect_to notes_url,
                                notice: 'Note was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_note
      if params[:id].present?
        @note = Note.find_by_id(params[:id])
      else
        @note = Note.find_by_id(params[:note_id])
      end
      render 'errors/not_found' unless @note
    end

    # Only allow a list of trusted parameters through.
    def note_params
      params.require(:note).permit(:title, :text, :visibility, :create_another,  :whodunnit,
        collaborations_attributes: [:id, :record_type, :record_id, :user_id, :_destroy],
        group_assignments_attributes: [:id, :record_type, :record_id, :group_id, :_destroy],
        taggings_attributes: [:id, :record_type, :record_id, :tag_id, :tag_name, :_destroy]
      )
    end

    def has_edit_privileges
      redirect_to notes_path, alert: 'Not authorized.' unless @note.has_edit_privileges current_user
    end

    def has_view_privileges
      render 'errors/not_found' unless @note.has_view_privileges(current_user)
    end

    def has_owner_privileges
      redirect_to notes_path, alert: 'Not authorized.' unless @note.has_owner_privileges current_user
    end
end
