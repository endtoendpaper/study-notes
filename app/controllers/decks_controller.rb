class DecksController < ApplicationController
  before_action :set_deck, only: %i[ show edit update destroy shuffle ]
  before_action :authenticate_user!, except: %i[ index show ]
  before_action :has_edit_privileges, only: %i[ edit update shuffle ]
  before_action :has_owner_privileges, only: %i[ destroy ]
  before_action :has_view_privileges, only: [ :show ]

  # GET /decks or /decks.json
  def index
    @title = 'Decks'
    if current_user
      @decks = SiteSearch.search(
        page: params[:page],
        per_page: 12,
        admin: current_user.admin?,
        user_id: current_user.id,
        item_types: ['decks']
      )
    else
      @decks = SiteSearch.search(
        page: params[:page],
        per_page: 12,
        item_types: ['decks']
      )
    end
    @tags = @decks.select { |item| item.respond_to?(:tags) && item.tags.present? }
      .flat_map(&:tags)
      .uniq
      .sort_by(&:last_activity_at)
      .reverse
  end

  # GET /decks/1 or /decks/1.json
  def show
  end

  # GET /decks/new
  def new
    @deck = current_user.decks.build
  end

  # GET /decks/1/edit
  def edit; end

  # POST /decks or /decks.json
  def create
    @deck = current_user.decks.build(deck_params)

    respond_to do |format|
      if @deck.save && deck_params[:create_another] == '1'
        format.html { redirect_to new_deck_card_path(@deck),
                  notice: 'Deck was successfully created.' }
        format.json { render :show, status: :created, location: @deck }
      elsif @deck.save
        format.html { redirect_to deck_url(@deck),
                                  notice: 'Deck was successfully created.' }
        format.json { render :show, status: :created, location: @deck }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @deck.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /decks/1 or /decks/1.json
  def update
    respond_to do |format|
      if @deck.update(deck_params)
        format.html { redirect_to deck_url(@deck),
                                  notice: 'Deck was successfully updated.' }
        format.json { render :show, status: :ok, location: @deck }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @deck.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /decks/1 or /decks/1.json
  def destroy
    @deck.destroy

    respond_to do |format|
      format.html { redirect_to decks_url,
                                notice: 'Deck was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  def shuffle
    @deck.shuffle
    respond_to do |format|
      format.html { redirect_to deck_cards_path(@deck),
                                notice: 'Deck was successfully shuffled.' }
      format.json { head :no_content }
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_deck
      if params[:id].present?
        @deck = Deck.find_by_id(params[:id])
      else
        @deck = Deck.find_by_id(params[:deck_id])
      end
      render 'errors/not_found' unless @deck
    end

    # Only allow a list of trusted parameters through.
    def deck_params
      params.require(:deck).permit(:title, :description, :visibility, :create_another, :whodunnit,
        collaborations_attributes: [:id, :record_type, :record_id, :user_id, :_destroy],
        group_assignments_attributes: [:id, :record_type, :record_id, :group_id, :_destroy],
        taggings_attributes: [:id, :record_type, :record_id, :tag_id, :tag_name, :_destroy]
      )
    end

    def has_edit_privileges
      redirect_to decks_path, alert: 'Not authorized.' unless @deck.has_edit_privileges current_user
    end

    def has_view_privileges
      render 'errors/not_found' unless @deck.has_view_privileges(current_user)
    end

    def has_owner_privileges
      redirect_to decks_path, alert: 'Not authorized.' unless @deck.has_owner_privileges current_user
    end
end
