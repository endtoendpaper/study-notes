class StarsController < ApplicationController
  before_action :authenticate_user!, except: %i[stargazers]
  before_action :set_record
  before_action :has_view_privileges

  def add
    @record.add_star(current_user)
    respond_to do |format|
      format.html { render @record,
                        notice: 'Star added.' }
      format.json { render :show, status: :ok, location: @record }
      format.turbo_stream { render :change_star }
    end
  end

  def remove
    @record.unstar(current_user)
    respond_to do |format|
      format.html { render @record,
                        notice: 'Star removed.' }
      format.json { render :show, status: :ok, location: @record }
      format.turbo_stream { render :change_star }
    end
  end

  def stargazers
    if current_user&.admin
      @stargazers = @record.stargazers.paginate(page: params[:page], per_page: 15)
    else
      @stargazers = @record.stargazers.where(hide_stars: false).paginate(page: params[:page], per_page: 15)
    end
  end

  private

    def set_record
      @record ||= (params[:record_type]&.capitalize&.constantize&.find(params[:record_id]))
      @record ||= (params[:type]&.capitalize&.singularize&.constantize&.find(params[:record_id]))
    end

    def has_view_privileges
      render 'errors/not_found' unless @record.has_view_privileges(current_user)
    end
end
