class GroupsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_group, except: %i[ index new create get_id search ]
  before_action :has_edit_privileges, only: %i[ edit update add_member remove_member edit_members delete_avatar membership_logs ]
  before_action :has_owner_privileges, only: %i[ change_owner destroy ]

  def index
    if params[:view_my_groups].present? && params[:view_my_groups] == '1'
      @groups = Group.search(params[:search], member_to_filter: current_user, page: params[:page], per_page: 5)
    else
      @groups = Group.search(params[:search], page: params[:page], per_page: 5)
    end
  end

  def show
  end

  def show_members
    @members = @group.all_group_members.paginate(page: params[:page], per_page: 5)
  end

  def show_items
    @title = @group.name + ": " + params[:type].capitalize
    @items = SiteSearch.search(
        page: params[:page],
        per_page: 12,
        admin: current_user.admin?,
        user_id: current_user.id,
        item_types: [params[:type]],
        group_id: @group.id
      )
    @tags = @items.select { |item| item.respond_to?(:tags) && item.tags.present? }
      .flat_map(&:tags)
      .uniq
      .sort_by(&:last_activity_at)
      .reverse
  end

  def membership_logs
    @logs = @group.group_member_logs.for_display.paginate(page: params[:page], per_page: 12)
    respond_to do |format|
      format.html
      format.csv { send_data @group.membership_logs_to_csv, filename: "#{@group.name.parameterize.underscore}_membership_logs-#{Time.now.utc.to_formatted_s(:number)}.csv" }
    end
  end

  def new
    @group = Group.new
  end

  def create
    @group = current_user.groups.build(group_params)

    respond_to do |format|
      if @group.save
        format.html { redirect_to group_url(@group),
                                  notice: 'Group was successfully created.' }
        format.json { render :show, status: :created, location: @group }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
  end

  def update
    respond_to do |format|
      if @group.update(group_params)
        format.html { redirect_to group_url(@group),
                                  notice: 'Group was successfully updated.' }
        format.json { render :show, status: :ok, location: @group }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @group.destroy

    respond_to do |format|
      format.html { redirect_to groups_url,
                                notice: 'Group was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  def edit_members
  end

  def change_owner
    username = group_params[:username]
    username = username[1..-1] if username.slice(0) == '@'
    new_owner = User.find_by(username: username)
    respond_to do |format|
      if new_owner == @group.user
        @alert = 'User is already owner.'
        @members = @group.all_group_members.paginate(page: params[:page], per_page: 5)
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @group.errors, status: :unprocessable_entity }
        format.turbo_stream { render :change_owner_form_update,
                          notice: 'User is already owner.' }
      elsif new_owner
        @group.change_owner(new_owner, current_user)
        @notice = 'Owner was successfully updated.'
        @members = @group.all_group_members.paginate(page: params[:page], per_page: 5)
        format.html { redirect_to group_url(@group),
                          notice: 'Owner was successfully updated.' }
        format.json { render :show, status: :ok, location: @group }
        format.turbo_stream { render :change_owner_form_update }
      else
        @alert = 'Username does not exist.'
        @members = @group.all_group_members.paginate(page: params[:page], per_page: 5)
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @group.errors, status: :unprocessable_entity }
        format.turbo_stream { render :change_owner_form_update,
                          notice: 'Username does not exist.' }
      end
    end
  end

  def add_member
    username = group_params[:username]
    username = username[1..-1] if username.slice(0) == '@'
    respond_to do |format|
      if User.find_by(username: username)
        if @group.members.include?(User.find_by(username: username))
          @notice = 'User is already a member.'
          @members = @group.all_group_members.paginate(page: params[:page], per_page: 5)
          format.html { redirect_to group_url(@group),
                            notice: 'User is already a member.' }
          format.json { render :show, status: :ok, location: @group }
          format.turbo_stream { render :form_update_for_members }
        else
          @group.add_member(User.find_by(username: username), current_user)
          @notice = 'Member was successfully added.'
          @members = @group.all_group_members.paginate(page: params[:page], per_page: 5)
          format.html { redirect_to group_url(@group),
                            notice: 'Member was successfully added.' }
          format.json { render :show, status: :ok, location: @group }
          format.turbo_stream { render :form_update_for_members }
        end
      else
        @alert = 'Username does not exist.'
        @members = @group.all_group_members.paginate(page: params[:page], per_page: 5)
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @group.errors, status: :unprocessable_entity }
        format.turbo_stream { render :form_update_for_members,
                          notice: 'Username does not exist.' }
      end
    end
  end

  def remove_member
    user = User.find(group_params[:user_id])
    @group.remove_member(user, current_user)
    @members = @group.all_group_members.paginate(page: params[:page], per_page: 5)
    respond_to do |format|
      format.html { redirect_to group_path(@group),
                                notice: 'User was successfully removed.' }
      format.json { head :no_content }
      format.turbo_stream { render :form_update_for_members }
    end
  end

  def delete_avatar
    @group.avatar.purge if @group.avatar.attached?
    respond_to do |format|
      format.html { redirect_to edit_group_path(@group) }
      format.turbo_stream
    end
  end

  def get_id
    group_name = params[:group_name]
    id = Group.find_by(name: group_name)&.id
    if id
      render json: { id: id }
    else
      render json: { id: 0 }
    end
  end

  def search
    query = params[:query]
    if query.present?
      groups = Group.name_search(query, page: params[:page], per_page: 10)&.pluck(:name)
      render json: groups
    else
      render json: []
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_group
      if params[:id].present?
        @group = Group.find_by(name: params[:id])
      else
        @group = Group.find_by(name: params[:group_id])
      end
      render 'errors/not_found' unless @group
    end

    # Only allow a list of trusted parameters through.
    def group_params
      params.require(:group).permit(:name, :username, :description, :avatar, :user_id, :whodunnit,
        group_members_attributes: [:id, :group_id, :user_id, :_destroy]
      )
    end

    def has_edit_privileges
      redirect_to groups_path, alert: 'Not authorized.' unless @group.has_edit_privileges current_user
    end

    def has_owner_privileges
      redirect_to groups_path, alert: 'Not authorized.' unless @group.has_owner_privileges current_user
    end
end
