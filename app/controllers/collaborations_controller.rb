class CollaborationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_record
  before_action :set_current_day_from_params
  before_action :has_owner_privileges, only: %i[ change_owner ]
  before_action :has_edit_privileges, only: %i[ edit_collaborations add_collaborator remove_collaborator add_group remove_group collaboration_logs ]

  def edit_collaborations
  end

  def change_owner
    username = params[:username]
    username = username[1..-1] if username.slice(0) == '@'
    new_owner = User.find_by(username: username)
    respond_to do |format|
      if new_owner == @record.user
        @alert = 'User is already owner.'
        format.html { render @record, status: :unprocessable_entity }
        format.json { render json: @record.errors, status: :unprocessable_entity }
        format.turbo_stream { render :owner_form_update,
                          notice: 'User is already owner.' }
      elsif new_owner
        @record.change_owner(new_owner, current_user)
        @notice = 'Owner was successfully updated.'
        format.html { render @record,
                          notice: 'Owner was successfully updated.' }
        format.json { render :show, status: :ok, location: @record }
        format.turbo_stream { render :owner_form_update }
      else
        @alert = 'Username does not exist.'
        format.html { render @record, status: :unprocessable_entity }
        format.json { render json: @record.errors, status: :unprocessable_entity }
        format.turbo_stream { render :owner_form_update,
                          notice: 'Username does not exist.' }
      end
    end
  end

  def add_collaborator
    username = params[:username]
    username = username[1..-1] if username.slice(0) == '@'
    respond_to do |format|
      if User.find_by(username: username)
        if @record.collaborater?(User.find_by(username: username))
          @notice = 'User is already a collaborater.'
          format.html { render @record,
                            notice: 'User is already a collaborater.' }
          format.json { render :show, status: :ok, location: @record }
          format.turbo_stream { render :collaborators_form_update }
        else
          @record.collaborate(User.find_by(username: username), current_user)
          @notice = 'Collaborater was successfully added.'
          format.html { render @record,
                            notice: 'Collaborater was successfully added.' }
          format.json { render @record, status: :ok, location: @record }
          format.turbo_stream { render :collaborators_form_update }
        end
      else
        @alert = 'Username does not exist.'
        format.html { render @record, status: :unprocessable_entity }
        format.json { render json: @record.errors, status: :unprocessable_entity }
        format.turbo_stream { render :collaborators_form_update,
                          notice: 'Username does not exist.' }
      end
    end
  end

  def remove_collaborator
    user = User.find(params[:user_id])
    @record.uncollaborate(user, current_user)
    respond_to do |format|
      format.html { render @record,
                                notice: 'User was successfully removed.' }
      format.json { head :no_content }
      format.turbo_stream { render :collaborators_form_update }
    end
  end

  def add_group
    group_name = params[:group_name]
    respond_to do |format|
      if Group.find_by(name: group_name)
        if @record.group?(Group.find_by(name: group_name))
          @notice = 'Group is already added.'
          format.html { render @record,
                            notice: 'Group is already added.' }
          format.json { render :show, status: :ok, location: @record }
          format.turbo_stream { render :groups_form_update }
        else
          @record.add_group(Group.find_by(name: group_name), current_user)
          @notice = 'Group was successfully added.'
          format.html { render @record,
                            notice: 'Group was successfully added.' }
          format.json { render :show, status: :ok, location: @record }
          format.turbo_stream { render :groups_form_update }
        end
      else
        @alert = 'Group does not exist.'
        format.html { render @record, status: :unprocessable_entity }
        format.json { render json: @record.errors, status: :unprocessable_entity }
        format.turbo_stream { render :groups_form_update,
                          notice: 'Group does not exist.' }
      end
    end
  end

  def remove_group
    group = Group.find(params[:group_id])
    @record.remove_group(group, current_user)
    respond_to do |format|
      format.html { render @record,
                                notice: 'Group was successfully removed.' }
      format.json { head :no_content }
      format.turbo_stream { render :groups_form_update }
    end
  end

  def collaboration_logs
    @logs = @record.collaboration_logs.for_display.paginate(page: params[:page], per_page: 12)
    respond_to do |format|
      format.html
      format.csv { send_data @record.collaboration_logs_to_csv, filename: "collaboration_logs-#{Time.now.utc.to_formatted_s(:number)}.csv" }
    end
  end

  private

    def set_record
      @record ||= (params[:record_type]&.capitalize&.constantize&.find(params[:record_id]))
      @record ||= (params[:type]&.capitalize&.singularize&.constantize&.find(params[:record_id]))
    end

    def has_edit_privileges
      redirect_to root_path, alert: 'Not authorized.' unless @record.has_edit_privileges current_user
    end

    def has_owner_privileges
      redirect_to root_path, alert: 'Not authorized.' unless @record.has_owner_privileges current_user
    end

    def set_current_day_from_params
      if params[:year].present? && params[:month].present?
        year = params[:year].to_i
        month = params[:month].to_i
        if params[:day].present?
          day = params[:day].to_i
        else
          day = 1
        end
      else
        year = Date.today.year
        month = Date.today.month
        day = 1
      end
      begin
        @current_date = Date.new(year, month, day)
      rescue ArgumentError => e
        render 'errors/not_found'
      end
    end
end
