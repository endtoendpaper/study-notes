class ElasticsearchReindexJob
  include Sidekiq::Job

  queue_as :default

  def perform(record_class, record_id, method_name)
    record = record_class.constantize.find_by(id: record_id)

    return unless record&.persisted?

    if record.respond_to?(method_name)
      record.send(method_name)
    end
  rescue StandardError => e
    Rails.logger.error("Failed to perform #{method_name} for #{record_class} with ID #{record_id}: #{e.message}")
    raise e
  end
end
