module CssClassHelper
  def active_link_class(path)
    if current_page?(path)
      ' active '
    else
      ''
    end
  end

  def navbar_class
    if user_signed_in? and current_user.darkmode
      #' bg-dark '
      ' bg-primary '
    else
      ' bg-primary '
    end
  end

  def dropdown_class
    if user_signed_in? and current_user.darkmode
      ' dropdown-menu-dark '
    else
      ' '
    end
  end

  def link_class(record = nil?)
    return if record&.is_a?(Calendar) && ((controller_name != 'calendars') || (action_name == 'index' && controller_name == 'calendars'))
    if user_signed_in? and current_user.darkmode
      ' link-light '
    else
      ''
    end
  end

  def input_class
    if user_signed_in? and current_user.darkmode
      ' text-white bg-dark '
    else
      ''
    end
  end

  def btn_close_class
    if user_signed_in? and current_user.darkmode
      ' btn-close-white '
    else
      ''
    end
  end

  def light_text_class
    if user_signed_in? and current_user.darkmode
      ' text-white '
    else
      ''
    end
  end

  def updated_date_text_class
    if user_signed_in? and current_user.darkmode
      ' text-white '
    else
      ' text-muted '
    end
  end

  def modal_class
    if user_signed_in? and current_user.darkmode
      ' bg-dark text-white bg-opacity-80 '
    else
      ''
    end
  end

  def button_close_class(record = nil)
    if record&.is_a? Calendar
      unless is_hex_light?  record.background_color
        ' btn-close-white '
      end
    elsif user_signed_in? and current_user.darkmode
      ' btn-close-white '
    else
      ''
    end
  end

  def table_class
    if user_signed_in? and current_user.darkmode
      ' table-dark text-white '
    else
      ''
    end
  end

  def calendar_extra_day_class
    if user_signed_in? and current_user.darkmode
      ' extra-day-dark '
    else
      ' extra-day '
    end
  end

  def calendar_day_class day
    today = current_timezone(current_user) ? Time.find_zone!(current_timezone(current_user)).today : local_time(Date.current).to_date
    if user_signed_in? and current_user.darkmode
      if day.to_date == today
        ' btn btn-primary btn-sm day '
      else
        ' btn btn-outline-light btn-sm day '
      end
    else
      if day.to_date == today
        ' btn btn-primary btn-sm day '
      else
        ' btn btn-outline-primary btn-sm day '
      end
    end
  end

  def card_class
    if user_signed_in? and current_user.darkmode
      ' text-white '
    else
      ''
    end
  end

  def outline_button_class
    if user_signed_in? and current_user.darkmode
      ' btn-outline-light '
    else
      ' btn-outline-primary '
    end
  end

  def star_button_class(record = nil)
    if user_signed_in? and current_user.darkmode
      ' btn-outline-light '
    else
      ' btn-outline-dark'
    end
  end

  def star_style(record = nil)
    if record&.is_a? Calendar
      "color: #{record.text_color}; border-color: #{record.text_color};"
    end
  end

  def is_hex_light? hex
    hex.slice! "#"
    hex = hex.to_i(16)
    hex > 0xffffff/2 ? true : false
  end

  def masquerade_button_class
    if user_signed_in? and current_user.darkmode
      ' btn-outline-light '
    else
      ' btn-outline-secondary '
    end
  end

  def offcanvas_class
    if user_signed_in? and current_user.darkmode
      'text-bg-dark '
    end
  end

  def follow_btn_float_class
    if action_name != 'stargazers'
      " float-start me-1 "
    end
  end

  def sort_class(record)
    if record.has_edit_privileges(current_user)
      ' sort '
    else
      ' '
    end
  end

  def list_group_item_class
    if user_signed_in? and current_user.darkmode
      ' list-group-item-dark '
    else
      ' list-group-item-secondary '
    end
  end

  def list_group_item_log_class
    if user_signed_in? and current_user.darkmode
      ' list-group-item-dark '
    else
      ' ps-0 '
    end
  end

  def chat_index_item_class user
    if user.darkmode
      ' bg-dark text-white '
    end
  end

  def item_icon_class object
    if object.class == Deck
      ' bi bi-paperclip '
    elsif object.class == Note
      ' bi bi-pin-angle-fill '
    elsif object.class == Calendar
      ' bi bi-calendar3 '
    end
  end

  def message_class user
    if user.darkmode
      ' message-content bg-secondary bg-gradient p-2 text-white bg-opacity-25 rounded '
    else
      ' message-content bg-secondary p-2 text-dark bg-opacity-25 rounded '
    end
  end

  def search_btn_class
    if current_user&.darkmode
      ' btn-primary '
    else
      ' btn-outline-primary '
    end
  end
end
