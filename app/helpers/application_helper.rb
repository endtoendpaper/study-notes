module ApplicationHelper
  def full_title(title)
    if title.empty?
      SiteSetting.site_name
    else
      title + ' | ' + SiteSetting.site_name
    end
  end

  def img_asset_path(image, size)
    object = SiteSetting.send("#{image}")
    file_ext = File.extname(object.blob.filename.to_s)
    asset_path = "#{image}/#{image}-" + size.to_s + "-" + SiteSetting.updated_at.to_i.to_s + file_ext
    path = Rails.root.join('public', asset_path).to_s
    if File.exist?(path)
      "/" + asset_path
    else
      begin
        dir_path = Rails.root.join("public/#{image}", "#{image}-#{size.to_s}-*" ).to_s
        FileUtils.rm_rf(Dir[dir_path])
        object.variant(size).processed
        File.open(path, 'wb') do |file|
          file.write(object.variant(size).download)
        end
        "/" + asset_path
      rescue
        object.variant(size)
      end
    end
  end

  def logo_navbar_path
    img_asset_path("logo", :navbar)
  end

  def favicon_path(size)
    img_asset_path("favicon", size)
  end

  def apple_icon_path(size)
    img_asset_path("apple_touch_icon", size)
  end

  def default_avatar_path(size)
    img_asset_path("default_avatar", size)
  end

  def route_type_for type
    if type == 'groups'
      'groups'
    else
      'items'
    end
  end

  def current_timezone(user = nil)
    if user
      user.time_zone
    elsif current_user
      current_user.time_zone
    end
  end

  def item_updated(item)
    if current_timezone
      item.last_activity_at.in_time_zone(current_timezone).strftime('%b %d, %Y %I:%M%p %Z')
    else
      local_time(item.last_activity_at, '%b %d, %Y %I:%M%p %Z')
    end
  end

  def record_created(resource, user = nil)
    if current_timezone(user)
      resource.created_at.in_time_zone(current_timezone(user)).strftime('%b %d, %Y %I:%M%p %Z')
    else
      local_time(resource.created_at, '%b %d, %Y %I:%M%p %Z')
    end
  end

  def record_updated(resource, user = nil)
    if current_timezone(user)
      resource.updated_at.in_time_zone(current_timezone(user)).strftime('%b %d, %Y %I:%M%p %Z')
    else
      local_time(resource.updated_at, '%b %d, %Y %I:%M%p %Z')
    end
  end

  def pretty_time(time, user = nil)
    if current_timezone(user)
      time.in_time_zone(current_timezone(user)).strftime('%b %d, %Y %I:%M%p %Z') unless time.nil?
    else
      local_time(time, '%b %d, %Y %I:%M%p %Z') unless time.nil?
    end
  end

  def pretty_event_start_time(event, user = nil)
    if event.all_day
      event.start.strftime('%b %d, %Y') + " (all day)"
    else
      if current_timezone(user)
        event.start.in_time_zone(current_timezone(user)).strftime('%b %d, %Y %I:%M%p %Z')
      else
        local_time(event.start, '%b %d, %Y %I:%M%p %Z')
      end
    end
  end

  def pretty_event_end_time(event, user = nil)
    if event.all_day
      event.end.strftime('%b %d, %Y') + " (all day)"
    else
      if current_timezone(user)
        event.end.time.in_time_zone(current_timezone(user)).strftime('%b %d, %Y %I:%M%p %Z')
      else
        local_time(event.end, '%b %d, %Y %I:%M%p %Z')
      end
    end
  end

  def pretty_recurring_event_start_time(event, new_time, user = nil)
    if event.all_day
      new_time.strftime('%b %d, %Y') + " (all day)"
    else
      if current_timezone(user)
        new_time.in_time_zone(current_timezone(user)).strftime('%b %d, %Y %I:%M%p %Z')
      else
        local_time(new_time, '%b %d, %Y %I:%M%p %Z')
      end
    end
  end

  def pretty_recurring_event_end_time(event, new_time, user = nil)
    if event.all_day
      new_time.strftime('%b %d, %Y') + " (all day)"
    else
      if current_timezone(user)
        new_time.in_time_zone(current_timezone(user)).strftime('%b %d, %Y %I:%M%p %Z')
      else
        local_time(new_time, '%b %d, %Y %I:%M%p %Z')
      end
    end
  end

  def pretty_picker_time(time, user = nil)
    if current_timezone(user)
      time.in_time_zone(current_timezone(user)).strftime('%b %d, %Y %I:%M%p') unless time.nil?
    else
      local_time(time, '%b %d, %Y %I:%M%p') unless time.nil?
    end
  end

  def pretty_new_start_picker_time(time, user = nil)
    return if time.nil?
    if current_timezone(user)
      start_time = time.in_time_zone(current_timezone(user)) + 6.hours
      start_time.strftime('%b %d, %Y %I:%M%p')
    else
      local_time(time, '%b %d, %Y %I:%M%p')
    end
  end

  def pretty_picker_start_time(event, user, current_date)
    if event.all_day
      event.start? ? event.start.strftime('%b %d, %Y') : (Date.current.to_datetime + 6.hours).strftime('%b %d, %Y')
    else
      event.start? ? pretty_picker_time(event.start, user) : pretty_new_start_picker_time(current_date, user)
    end
  end

  def pretty_picker_end_date(user, current_date)
    current_date.strftime('%Y%m%d')
  end

  def pretty_picker_end_time(event, user, current_date)
    if event.all_day
      event.end? ? event.end.strftime('%b %d, %Y') : ""
    else
      event.end? ? pretty_picker_time(event.end, user) : ""
    end
  end

  def pretty_exceptions(exceptions, start, tz)
    return "None" if exceptions.empty?
    pretty_exceptions = exceptions.map do |exception|
      if tz
        DateTime.new(exception.to_date.year, exception.to_date.month, exception.to_date.day, start.hour, start.min).in_time_zone(tz).strftime('%b %d, %Y')
      else
        local_time(DateTime.new(exception.to_date.year, exception.to_date.month, exception.to_date.day, start.hour, start.min), '%b %d, %Y')
      end
    end
    pretty_exceptions.join("; ")
  end
end
