module UsersHelper
  def user_left_col_classes
    if action_name == 'show' || action_name == 'index' || action_name == 'user_management'
      " col-sm-6 col-md-4 col-lg-3 col-xl-3 col-xxl-2 mt-3 "
    else
      " col-xl-3 "
    end
  end

  def user_right_col_classes
    if action_name == 'show' || action_name == 'index' || action_name == 'user_management'
      " col-sm-6 col-md-8 col-lg-9 col-xl-9 col-xxl-10 mt-3 "
    else
      " col-xl-9 "
    end
  end

  def can_view_follower_stats(user)
    !user.hide_relationships || (current_user&.admin || current_user&.id == user.id)
  end
end
