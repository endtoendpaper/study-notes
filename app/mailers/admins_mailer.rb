class AdminsMailer < ApplicationMailer
  include MailerAttachments

  def support_issue_created(support_issue, current_user)
    @support_issue = support_issue
    add_attachments
    if !User.where(admin: true).where.not(id: current_user.id).empty?
      mail(to: User.where(admin: true).where.not(id: current_user.id).pluck(:email),
        subject: SiteSetting.site_name + " " + @support_issue.display_name + ': ' + @support_issue.summary)
    end
  end

  def support_comment(support_comment, current_user)
    @support_comment = support_comment
    @support_issue = SupportIssue.find(@support_comment.support_issue_id)
    add_attachments
    if !User.where(admin: true).where.not(id: current_user.id).empty?
      mail(to: User.where(admin: true).where.not(id: current_user.id).pluck(:email),
        subject: SiteSetting.site_name + " " + @support_issue.display_name + ': ' + @support_issue.summary)
    end
  end

  def support_reply(support_reply, current_user)
    @support_reply = support_reply
    @support_issue = SupportIssue.find(@support_reply.support_issue_id)
    add_attachments
    if !User.where(admin: true).where.not(id: current_user.id).empty?
      mail(to: User.where(admin: true).where.not(id: current_user.id).pluck(:email),
        subject: SiteSetting.site_name + " " + @support_issue.display_name + ': ' + @support_issue.summary)
    end
  end

  def support_note(support_note, current_user)
    @support_note = support_note
    @support_issue = SupportIssue.find(@support_note.support_issue_id)
    add_attachments
    if !User.where(admin: true).where.not(id: current_user.id).empty?
      mail(to: User.where(admin: true).where.not(id: current_user.id).pluck(:email),
        subject: SiteSetting.site_name + " " + @support_issue.display_name + ': ' + @support_issue.summary)
    end
  end

  def support_issue_reopened(support_issue, current_user)
    @support_issue = support_issue
    if !User.where(admin: true).where.not(id: current_user.id).empty?
      mail(to: User.where(admin: true).where.not(id: current_user.id).pluck(:email),
        subject: SiteSetting.site_name + " " + @support_issue.display_name + ': ' + @support_issue.summary)
    end
  end

  def support_issue_resolved(support_issue, current_user)
    @support_issue = support_issue
    if !User.where(admin: true).where.not(id: current_user.id).empty?
      mail(to: User.where(admin: true).where.not(id: current_user.id).pluck(:email),
        subject: SiteSetting.site_name + " " + @support_issue.display_name + ': ' + @support_issue.summary)
    end
  end
end
