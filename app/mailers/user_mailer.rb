class UserMailer < ApplicationMailer
  include MailerAttachments

  def support_reply(support_reply)
    @support_reply = support_reply
    @support_issue = SupportIssue.find(@support_reply.support_issue_id)
    add_attachments
    if !User.where(id: @support_issue.user_id).empty?
      @user = User.find(@support_issue.user_id)
      @user_email = @user.email
      mail(to: @user_email, subject: SiteSetting.site_name + " " + @support_issue.display_name + ': ' + @support_issue.summary)
    end
  end

  def support_issue_created(support_issue)
    @support_issue = support_issue
    if !User.where(id: @support_issue.user_id).empty?
      @user = User.find(@support_issue.user_id)
      @user_email = @user.email
      mail(to: @user_email, subject: SiteSetting.site_name + " " + @support_issue.display_name + ': ' + @support_issue.summary)
    end
  end

  def support_issue_reopened(support_issue)
    @support_issue = support_issue
    if !User.where(id: @support_issue.user_id).empty?
      @user = User.find(@support_issue.user_id)
      @user_email = @user.email
      mail(to: @user_email, subject: SiteSetting.site_name + " " + @support_issue.display_name + ': ' + @support_issue.summary)
    end
  end

  def support_issue_resolved(support_issue)
    @support_issue = support_issue
    if !User.where(id: @support_issue.user_id).empty?
      @user = User.find(@support_issue.user_id)
      @user_email = @user.email
      mail(to: @user_email, subject: SiteSetting.site_name + " " + @support_issue.display_name + ': ' + @support_issue.summary)
    end
  end
end
