module MailerAttachments

  private

    def add_attachments
      @resource = @support_issue ||= @support_comment ||= @support_reply ||= @support_note
      @resource.rich_text_body.embeds_attachments.each do |file|
        if attachment_size_ok(file) && attachment_extension_ok(file)
          attachments[file.blob.filename.to_s] = {
            mime_type: file.blob.content_type,
            content: file.blob.download
          }
        end
      end
    end

    def attachment_size_ok(file)
      # Less than 19 MB for google mail server
      ((file.byte_size.to_f / 1000000).round(2) < 19)
    end

    def attachment_extension_ok(file)
      # Don't send files with extensions google blocks or zip/gzip files
      blocked_formats = ['.ade', '.adp', '.apk', '.appx', '.appxbundle', '.bat',
        '.cab', '.chm', '.cmd', '.com', '.cpl', '.diagcab', '.diagcfg',
        '.diagpack', '.dll', '.dmg', '.ex', '.ex_', '.exe', '.hta', '.img',
        '.ins', '.iso', '.isp', '.jar', '.jnlp', '.js', '.jse', '.lib', '.lnk',
        '.mde', '.msc', '.msi', '.msix', '.msixbundle', '.msp', '.mst', '.nsh',
        '.pif', '.ps1', '.scr', '.sct', '.shb', '.sys', '.vb', '.vbe', '.vbs',
        '.vhd', '.vxd', '.wsc', '.wsf', '.wsh', '.xll', '.zip', '.gzip']
      !blocked_formats.include? File.extname(file.filename.to_s)
    end
end
