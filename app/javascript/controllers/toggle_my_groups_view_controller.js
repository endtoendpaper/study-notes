import { Controller } from "@hotwired/stimulus"
import Rails from "@rails/ujs"

// Connects to data-controller="toggle-my-groups-view"
export default class extends Controller {
  static targets = ["toggle", "submit", "query", "mine"];

  connect() {
    const params = new URLSearchParams(window.location.search);

    if (params.get("view_my_groups") === "1") {
      this.toggleTarget.checked = true;
      this.mineTarget.value = "1";
    }

    const searchQuery = params.get("search");
    if (searchQuery) {
      this.queryTarget.value = searchQuery;
    }
  }

  toggle() {
    Rails.fire(this.submitTarget, 'submit');
  }
}
