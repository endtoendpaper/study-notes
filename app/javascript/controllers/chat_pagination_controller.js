import { Controller } from "@hotwired/stimulus"

// Connects to data-controller="chat-pagination"
export default class extends Controller {
  static targets = [ "pagination" ];

  connect() {
    var pageLinks = this.paginationTarget.querySelectorAll("a");
    pageLinks.forEach((link) => {
      var href = link.href
      link.setAttribute("href", "/chats/update_index?" + href.slice(href.lastIndexOf('?')+1));
      link.setAttribute("data-turbo-method", "post");
    });
  }
}
