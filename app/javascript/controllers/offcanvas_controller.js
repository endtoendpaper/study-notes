import { Controller } from "@hotwired/stimulus"
import { Offcanvas } from "bootstrap"

export default class extends Controller {
  connect() {
    this.offcanvas = new Offcanvas(this.element)
    this.offcanvas.show()
  }
}
