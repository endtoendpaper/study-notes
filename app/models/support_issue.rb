class SupportIssue < ApplicationRecord
  include IsSupportUserCommunication

  has_many :support_replies, dependent: :destroy
  has_many :support_comments, dependent: :destroy
  has_many :support_notes, dependent: :destroy

  validates_length_of :summary, minimum: 3, maximum: 255, allow_blank: false

  def to_param
    self.display_name
  end

  def display_name
    "ISSUE-#{self.id}"
  end
end
