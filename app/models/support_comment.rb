class SupportComment < ApplicationRecord
  include IsSupportUserCommunication

  belongs_to :support_issue
end
