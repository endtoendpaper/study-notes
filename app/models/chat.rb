class Chat < ApplicationRecord
  include Collaborative

  has_many :messages, dependent: :destroy
  has_many :chat_preferences, dependent: :destroy

  validate :one_group_or_direct_msg

  before_create :update_show_last_message
  before_destroy :update_participants_unread_msgs, prepend: true

  def self.search(query, page: 1, per_page: 50)
    return [] if query.nil? || query.strip.empty?

    page = page.to_i <= 0 ? 1 : page.to_i

    search_body = {
      query: {
        bool: {
          should: [
            {
              multi_match: {
                query: query,
                fields: ['username'],
                "fuzziness": "AUTO"
              }
            },
            {
              multi_match: {
                query: query,
                fields: ['name'],
                "fuzziness": "AUTO"
              }
            }
          ]
        }
      }
    }

    if per_page
      search_body.merge!({
        from: (page.to_i - 1) * per_page.to_i,
        size: per_page.to_i
      })
    end

    user_index = "#{User.index_name}"
    group_index = "#{Group.index_name}"

    response = Elasticsearch::Model.client.search(
      index: [user_index, group_index], # Dynamically constructed index names
      body: search_body
    )

    # Parse results to return relevant fields
    response['hits']['hits'].map do |hit|
      if hit['_source']['username']
        "@#{hit['_source']['username']}"  # Append '@' to username
      else
        hit['_source']['name']  # Return name if no username
      end
    end
  end

  def participants
    all_collaborators
  end

  def participant?(user)
    participants.include?(user)
  end

  def participant_display(user = nil)
    if groups.first
      groups.first.name
    elsif user.nil?
      participants.pluck(:username).map { |p| '@' + p }.join(" ")
    elsif (participants.pluck(:username) - [user.username]).first.nil?
      "[Deleted User Account]"
    else
      '@' + (participants.pluck(:username) - [user.username]).first
    end
  end

  private

    def update_show_last_message
      self.last_message = DateTime.now
    end

    def update_participants_unread_msgs
      chat_preferences.each { |p| p.update(unread_messages: 0)}
      participants.each { |p| p.update_unread_msg_count }
    end

    def one_group_or_direct_msg
      if groups_count > 1 || collaborators_count > 2 || (groups_count > 0 && collaborators_count > 0)
        errors.add(:base, "Direct or group messages only.")
      end
    end
end
