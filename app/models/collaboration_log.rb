class CollaborationLog < ApplicationRecord
  belongs_to :user
  belongs_to :item, polymorphic: true
  belongs_to :collaboration, polymorphic: true

  enum :action, [ :changed_owner_to, :added_collaborator, :removed_collaborator, :added_group, :removed_group ]

  validates_presence_of :action

  scope :for_display, -> { order("created_at DESC") }

  before_create :update_columns

  def update_columns
    self.user_handle = self.user.handle
    if self.collaboration.class.to_s == "User"
      self.collaboration_name = self.collaboration.handle
    else
      self.collaboration_name = self.collaboration.name
    end
  end

  def action_string
    self.action.to_s.humanize.downcase
  end
end
