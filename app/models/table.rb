class Table < ApplicationRecord
  include GlobalID::Identification
  include ActionText::Attachable

  belongs_to :record, polymorphic: true, optional: true

  def to_trix_content_attachment_partial_path
	  "tables/editor"
  end

  def self.delete_unmatched_tables
    # Delete tables updated older than 24 hours if their record was never saved
    Table.where(record_saved: false).and(Table.where(updated_at: ..1.day.ago)).destroy_all
  end

  def self.delete_all_unmatched_tables
    # Delete tables older than 24 hours if their record was never saved
    Table.where(record_saved: false).destroy_all
  end
end
