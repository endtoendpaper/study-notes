class ChatPreference < ApplicationRecord
  belongs_to :chat
  belongs_to :user

  validates :user, presence: true
  validates :chat, presence: true
  validates :user, uniqueness: { scope: :chat }
end
