class SupportNote < ApplicationRecord
  include IsSupportAdminCommunication

  belongs_to :support_issue
end
