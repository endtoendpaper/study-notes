class Group < ApplicationRecord
  include HasEditNotice
  include Searchable

  before_save { name.strip! }

  belongs_to :user

  attr_accessor :whodunnit

  has_one_attached :avatar do |attachable|
    attachable.variant :tiny, resize_to_limit: [15, 15]
    attachable.variant :thumb, resize_to_limit: [100, 100]
    attachable.variant :medium, resize_to_limit: [200, 200]
  end

  has_many :group_members, dependent: :destroy
  has_many :members, :through => :group_members, source: :user
  has_many :group_assignments, dependent: :destroy

  has_many :group_member_logs, dependent: :destroy
  has_many :passive_collaboration_logs, class_name:  "CollaborationLog",
            foreign_key: "collaboration_id",
            dependent:   :nullify

  has_many :chats, :through => :group_assignments, source: :record, source_type: 'Chat'

  accepts_nested_attributes_for :group_members, allow_destroy: true

  UserItems.item_list.each do |item|
    has_many "#{item}".to_sym, :through => :group_assignments, source: :record, source_type: "#{UserItems.class(item)}"
  end

  VALID_NAME_REGEX = /\A[a-z\d\-_\s]*\z/i
  validates :name, presence: true, length: { maximum: 100 },
                    format: { with: VALID_NAME_REGEX, message: 'can only contain letters, numbers, spaces, dashes, or underscores' },
                    uniqueness: { case_sensitive: false }
  validates :description, length: { maximum: 500 }

  default_scope { order('lower(groups.name)')}
  scope :ordered_by_id, -> { reorder(id: :asc) }

  after_create :log_owner
  after_commit :queue_reindex_all_group_members_ids, if: :saved_change_to_user_id?
  after_commit :queue_reindex_group_records, if: :saved_change_to_user_id?
  after_destroy :queue_reindex_group_records

  settings do
    mappings dynamic: false do
      indexes :name, type: :text
      indexes :description, type: :text
      indexes :user_id, type: :integer
      indexes :member_ids, type: :integer
    end
  end

  def as_indexed_json(options = {})
    as_json(only: %i[name description user_id]).merge(
      member_ids: all_group_members.pluck(:id)
    )
  end

  def to_param
    self.name_was
  end

  def self.search(query, owner_to_filter: nil, member_to_filter: nil, page: 1, per_page: 50)
    page = page.to_i <= 0 ? 1 : page.to_i

    if query.present?
      search_body = {
        query: {
          bool: {
            must: [
              {
                multi_match: {
                  query: query,
                  fields: ['name^2', 'description'],
                  "fuzziness": "AUTO"
                }
              }
            ],
            filter: []
          }
        }
      }

      search_body[:query][:bool][:filter] << { term: { user_id: owner_to_filter.id } } if owner_to_filter
      search_body[:query][:bool][:filter] << { terms: { member_ids: [member_to_filter.id] } } if member_to_filter

      if per_page
        search_body.merge!({
          from: (page.to_i - 1) * per_page.to_i,
          size: per_page.to_i
        })
      end

      search_response = Group.__elasticsearch__.search(search_body)

      WillPaginate::Collection.create(page.to_i, per_page.to_i, search_response.response['hits']['total']['value']) do |pager|
        pager.replace(search_response.records.to_a)
      end
    elsif owner_to_filter.present?
      owner_to_filter.groups.order('lower(name)').paginate(page: page, per_page: per_page)
    elsif member_to_filter.present?
      member_to_filter.all_groups.order('lower(name)').paginate(page: page, per_page: per_page)
    else
      Group.all.order('lower(name)').paginate(page: page, per_page: per_page)
    end
  end

  def self.name_search(query, page: 1, per_page: 50)
    page = page.to_i <= 0 ? 1 : page.to_i

    search_body = {
      query: {
        bool: {
          must: [
            {
              multi_match: {
                query: query,
                fields: ['name'],
                "fuzziness": "AUTO"
              }
            }
          ]
        }
      }
    }

    if per_page
      search_body.merge!({
        from: (page.to_i - 1) * per_page.to_i,
        size: per_page.to_i
      })
    end

    search_response = Group.__elasticsearch__.search(search_body)

    WillPaginate::Collection.create(page.to_i, per_page.to_i, search_response.response['hits']['total']['value']) do |pager|
      pager.replace(search_response.records.to_a)
    end
  end

  def log_owner
    GroupMemberLog.create(group: self, user: self.user, action: :changed_owner_to, member: self.user)
  end

  def owner
    user
  end

  def owner_handle
    '@' + owner.username unless owner.nil?
  end

  def change_owner(user, whodunnit)
    unless self.user == user
      self.update({ user: user })
      GroupMemberLog.create(group: self, user: whodunnit, action: :changed_owner_to, member: user)
    end
  end

  def add_member(user, whodunnit)
    if !members.include?(user)
      members << user
      GroupMemberLog.create(group: self, user: whodunnit, action: :added_member, member: user)
      if chats.first
        ChatPreference.create(user: user, chat: chats.first) unless ChatPreference.where(user: user, chat: chats.first).first
      end
    end
  end

  def remove_member(user, whodunnit)
    if members.include?(user)
      members.destroy(user)
      GroupMemberLog.create(group: self, user: whodunnit, action: :removed_member, member: user)
      if chats.first
        ChatPreference.where(user: user, chat: chats.first).destroy_all
      end
    end
  end

  def members_count
    if members.include?(owner)
      members.count
    else
      members.count + 1
    end
  end

  def member?(user)
    (members.include?(user) || owner == user)
  end

  def all_group_members
    User.where(id: [self.user_id] | self.members.pluck(:id)).order('username ASC')
  end

  def has_owner_privileges user
    user && (user.id == owner.id or user.admin)
  end

  def has_edit_privileges user
    user && (member?(user) || has_owner_privileges(user))
  end

  def has_view_privileges user
    user
  end

  def find_or_create_chat
    chat = chats.first
    if !chat
      chat = Chat.create
      chat.add_group(self)
      all_group_members.each do |member|
        ChatPreference.create(user: member, chat: chat)
      end
    end
    chat
  end

  def membership_logs_to_csv
    CSV.generate(headers: true) do |csv|
      csv << %w(user action member datetime)
      group_member_logs.each do |log|
        csv << [log.user_handle, log.action, log.member_handle, log.created_at]
      end
    end
  end

  def reindex_all_group_members_ids
    return unless Elasticsearch::Model.client.exists?(index: self.class.index_name, id: id)

    Elasticsearch::Model.client.update(
      index: self.class.index_name,
      id: id,
      retry_on_conflict: 3,
      body: {
        doc: {
          member_ids: all_group_members.pluck(:id)
        }
      }
    )
  end

  def queue_reindex_all_group_members_ids
    ElasticsearchReindexJob.perform_async(self.class.name, self.id, 'reindex_all_group_members_ids')
  end

  def queue_reindex_group_records
    UserItems.item_list.each do |item|
      self.send(item).each do |record|
        ElasticsearchReindexJob.perform_async(record.class.name, record.id, 'reindex_all_collaborator_ids')
        ElasticsearchReindexJob.perform_async(record.class.name, record.id, 'reindex_subitems_collaborators')
      end
    end
  end
end
