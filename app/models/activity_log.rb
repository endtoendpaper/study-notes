class ActivityLog < ApplicationRecord
  belongs_to :user
  belongs_to :record, polymorphic: true

  enum :action, [ :followed, :unfollowed, :starred, :unstarred, :tagged, :untagged, :created, :edited ]

  scope :for_display, -> { order("created_at DESC") }

  after_create :delete_old_data

  def action_string
    self.action.to_s.humanize.downcase
  end

  private

    def delete_old_data
      ActivityLog.where(user: self.user).for_display.offset(50).destroy_all
    end
end
