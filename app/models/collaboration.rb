class Collaboration < ApplicationRecord
  include UpdatesRecordLastActivity

  belongs_to :user
  belongs_to :record, polymorphic: true

  validates :user, presence: true
  validates :record, presence: true
  validate :unique_join

  after_create :log_create_changes
  after_commit :queue_reindex_record
  before_destroy :destroy_dm_chats
  before_destroy :log_destroy_changes
  after_destroy :queue_reindex_record

  scope :order_by_username, -> { sort_by{ |collaboration| collaboration.user.username } }

  private

    def unique_join
      errors.add(:base, "- #{user.handle} is already associated.") unless self.persisted? if record&.collaborater?(user)
    end

    def destroy_dm_chats
      record.destroy if record.class.to_s == "Chat"
    end

    def log_create_changes
      CollaborationLog.create(item: self.record, user: User.find(self.record.whodunnit), action: :added_collaborator, collaboration: self.user) if self.record.class.to_s != "Chat" and self.record.whodunnit
    end

    def log_destroy_changes
      CollaborationLog.create(item: self.record, user: User.find(self.record.whodunnit), action: :removed_collaborator, collaboration: self.user) if self.record.class.to_s != "Chat" and self.record.whodunnit
    end

    def queue_reindex_record
      if self.record.class.to_s != "Chat" && self.record.persisted?
        ElasticsearchReindexJob.perform_async(record.class.name, record.id, 'reindex_all_collaborator_ids')
        ElasticsearchReindexJob.perform_async(record.class.name, record.id, 'reindex_subitems_collaborators')
      end
    end
end
