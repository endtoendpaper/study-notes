class GroupMember < ApplicationRecord
  belongs_to :group
  belongs_to :user

  validates :group, presence: true
  validates :user, presence: true
  validate :unique_join

  scope :order_by_username, -> { sort_by{ |member| member.user.username } }

  before_create :log_create_changes
  after_commit :queue_reindex_group_records
  before_destroy :log_destroy_changes
  after_destroy :queue_reindex_group_records

  private

    def log_create_changes
      GroupMemberLog.create(group: self.group, user: User.find(self.group.whodunnit), action: :added_member, member: self.user) if self.group.whodunnit
    end

    def log_destroy_changes
      GroupMemberLog.create(group: self.group, user: User.find(self.group.whodunnit), action: :removed_member, member: self.user) if self.group.whodunnit
    end

    def unique_join
      errors.add(:base, "- #{user.handle} is already a member.") unless self.persisted? if self.group&.members&.include?(self.user)
    end

    def queue_reindex_group_records
      ElasticsearchReindexJob.perform_async(group.class.name, group.id, 'reindex_all_group_members_ids') if group.persisted?
      UserItems.item_list.each do |item|
        group.send(item).each do |record|
          ElasticsearchReindexJob.perform_async(record.class.name, record.id, 'reindex_all_collaborator_ids')
          ElasticsearchReindexJob.perform_async(record.class.name, record.id, 'reindex_subitems_collaborators')
        end
      end
    end
end
