class Note < ApplicationRecord
  include HasRichTextExtensions
  include IsItem

  belongs_to :user
  has_rich_text :text

  validates :title, presence: true, length: { maximum: 255 }
  validates_presence_of :text

  settings do
    mappings dynamic: false do
      indexes :title, type: :text
      indexes :text, type: :text
      indexes :user_id, type: :integer
      indexes :visibility, type: :integer
      indexes :group_ids, type: :integer
      indexes :all_collaborator_ids, type: :integer
      indexes :stargazers, type: :integer
      indexes :last_activity_at, type: :date
      indexes :tags, type: :keyword
    end
  end

  def as_indexed_json(options = {})
    as_json(only: %i[title user_id visibility last_activity_at])
      .merge(group_ids: groups.pluck(:id))
      .merge(all_collaborator_ids: (collaborators.pluck(:id) + group_members.pluck(:id) + group_owners.pluck(:id) + [user_id]).uniq)
      .merge(stargazers: stargazers.pluck(:id))
      .merge(tags: tags.pluck(:name))
      .merge(text: text.to_plain_text)
  end
end
