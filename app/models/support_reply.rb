class SupportReply < ApplicationRecord
  include IsSupportAdminCommunication

  belongs_to :support_issue
end
