class Message < ApplicationRecord
  belongs_to :chat
  belongs_to :user

  validates :content, presence: true

  scope :for_display, -> { order("created_at DESC") }

  after_create :update_show_last_message
  after_create :broadcast_msg_to_participants

  private

    def update_show_last_message
      chat.update(last_message: created_at)
    end

    def broadcast_msg_to_participants
      chat.participants.each do |u|
        broadcast_append_to [chat, u, "chat_messages"],
          action: :append,
          target: "messages",
          locals: { recieved_real_time: true, chat: chat, user: u }
        preference = ChatPreference.find_by(user: u, chat: chat)
        preference.update(unread_messages: preference.unread_messages += 1) unless user == u
        u.update_unread_msg_count
        broadcast_prepend_to [u, "chat_index_item"],
          action: :prepend,
          target: "chats",
          partial: "chats/chat_index_item",
          locals: { chat: chat, recieved_real_time: true, user: u, preference: preference.reload }
        broadcast_update_to [u, "unread_msg_count_footer"],
          target: "unread_msg_count_footer",
          partial: "user_unread_msg_count",
          locals: { user: u }
        broadcast_update_to [u, "unread_msg_count_header"],
          target: "unread_msg_count_header",
          partial: "user_unread_msg_count",
          locals: { user: u }
      end
    end
end

