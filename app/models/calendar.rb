class Calendar < ApplicationRecord
  require 'active_support/all'
  require 'icalendar'
  require 'icalendar/tzinfo'

  include HasSearchableSubItems
  include IsItem

  has_many :events, dependent: :destroy
  has_many :recurring_events, dependent: :destroy

  validates :title, presence: true, length: { maximum: 255 }

  settings do
    mappings dynamic: false do
      indexes :title, type: :text
      indexes :description, type: :text
      indexes :user_id, type: :integer
      indexes :visibility, type: :integer
      indexes :group_ids, type: :integer
      indexes :all_collaborator_ids, type: :integer
      indexes :stargazers, type: :integer
      indexes :last_activity_at, type: :date
      indexes :tags, type: :keyword
    end
  end

  def as_indexed_json(options = {})
    as_json(only: %i[title description user_id visibility last_activity_at])
      .merge(group_ids: groups.pluck(:id))
      .merge(all_collaborator_ids: (collaborators.pluck(:id) + group_members.pluck(:id) + group_owners.pluck(:id) + [user_id]).uniq)
      .merge(stargazers: stargazers.pluck(:id))
      .merge(tags: tags.pluck(:name))
  end

  #Buffer of days to query events to account for tx differences in view
  DATE_ADJUSTMENT = 2

  def self.generate_calendar_grid year, month
    begin
      current_month = Date.new(year, month, 1)
    rescue ArgumentError
      raise ArgumentError, "Invalid year or month provided."
    end

    start_wday = current_month.wday
    start_date = current_month - start_wday

    end_of_month = current_month.next_month.prev_day
    end_wday = end_of_month.wday
    days_to_add = 6 - end_wday
    end_date = end_of_month + days_to_add

    all_dates = (start_date..end_date).to_a

    calendar_grid = all_dates.each_slice(7).to_a
    calendar_grid
  end

  def self.get_user_events_for(user, dates, tz, limit = nil)
    search_start = dates.first - DATE_ADJUSTMENT
    search_end = dates.last + DATE_ADJUSTMENT
    all_events = []
    recurring_events_instances = []

    # gets events from user's 1000 most recently updated calendars
    SiteSearch.search(
        per_page: 1000,
        user_id: user.id,
        item_types: ['calendars'],
        items_user_id: user.id
      )
      .each do |calendar|
        calendar_events = calendar.events
                                  .where(end: search_start.., start: ..search_end)
                                  .order(start: :asc)
        all_events.concat(calendar_events)

        recurring_events = calendar.recurring_events
                                  .where(recurrence_end: search_start.., start: ..search_end)
                                  .or(calendar.recurring_events.where(recurrence_end: nil, start: ..search_end))
                                  .order(start: :asc)

        recurring_events.each do |event|
          recurring_events_instances.concat(event.events(search_start, search_end))
        end
    end

    all_events.concat(recurring_events_instances)
    Calendar.create_month_events_hash(all_events, search_end, tz, limit)
  end

  def self.create_month_events_hash(events_for_month, search_end, tz, limit = nil)
    events_by_day = Hash.new { |hash, key| hash[key] = [] }

    events_for_month.each do |event|
      start_date, end_date = event_date_range(event, tz)

      (start_date..end_date).each do |date|
        break if date >= search_end

        next if limit && events_by_day[date].count >= limit

        events_by_day[date] << event
      end
    end

    events_by_day
  end

  def subitems
    recurring_events + events
  end

  def searchable_subitems
    self.subitems
  end

  def get_events_for(dates, tz, limit = nil)
    search_start = dates.first - DATE_ADJUSTMENT
    search_end = dates.last + DATE_ADJUSTMENT

    events_for_month = events
                         .where(end: search_start.., start: ..search_end)
                         .order(start: :asc)

    recurring_events_for_month = recurring_events
                                    .where(recurrence_end: search_start.., start: ..search_end)
                                    .or(recurring_events.where(recurrence_end: nil, start: ..search_end))
                                    .order(start: :asc)

    recurring_events_instances = recurring_events_for_month.flat_map do |event|
      event.events(search_start, search_end)
    end

    all_events = events_for_month + recurring_events_instances

    Calendar.create_month_events_hash(all_events, search_end, tz, limit)
  end

  def events_to_ics
    cal = Icalendar::Calendar.new
    tz = TZInfo::Timezone.get('UTC')
    cal.add_timezone tz.ical_timezone(DateTime.current)

    add_event_to_calendar = lambda do |event, rrule = nil|
      cal.event do |e|
        e.dtstart = format_date(event.start, event.all_day?)
        e.dtend = format_date(event.end, event.all_day?)
        e.summary     = event.summary
        e.description = event.description.to_plain_text
        e.location    = event.location
        e.rrule       = rrule if rrule
      end
    end

    events.each { |event| add_event_to_calendar.call(event) }
    recurring_events.each { |event| add_event_to_calendar.call(event, event.rrule) }

    cal.to_ical
  end

  private

    def self.event_date_range(event, tz)
      if event.all_day
        start_date = event.start.to_date
        end_date = event.end.to_date
      else
        start_date = event.start.in_time_zone(tz).to_date
        end_date = event.end.in_time_zone(tz).to_date
      end
      [start_date, end_date]
    end

    def format_date(date, all_day)
      all_day ? Icalendar::Values::Date.new(date.to_date) : Icalendar::Values::DateTime.new(date, 'tzid' => 'UTC')
    end
end
