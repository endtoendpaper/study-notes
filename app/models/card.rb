class Card < ApplicationRecord
  include HasRichTextExtensions
  include IsSubitem

  belongs_to :parent, class_name: 'Deck', foreign_key: 'deck_id'
  has_rich_text :front
  has_rich_text :back

  validates_presence_of :front
  validates_presence_of :back

  before_create :set_display_position
  before_destroy :reset_deck_positions

  default_scope { order(:display_position) }

  settings do
    mappings dynamic: false do
      indexes :front, type: :text
      indexes :back, type: :text
      indexes :user_id, type: :integer
      indexes :visibility, type: :integer
      indexes :all_collaborator_ids, type: :integer
    end
  end

  def as_indexed_json(options = {})
    as_json
      .merge(front: front.to_plain_text)
      .merge(back: back.to_plain_text)
      .merge(user_id: parent.user_id)
      .merge(visibility: parent.visibility)
      .merge(all_collaborator_ids: (parent.collaborators.pluck(:id) + parent.group_members.pluck(:id) + parent.group_owners.pluck(:id) + [parent.user_id]).uniq)
  end

  def set_display_position
    self.display_position = self.parent.cards.count
  end

  def reset_deck_positions
    cards = self.parent.cards - [self]
    cards.each_with_index do | card, i |
      card.update(display_position: i)
    end
  end

  def prev_card
    parent.cards.where('display_position < ?', display_position).last
  end

  def next_card
    parent.cards.where('display_position > ?', display_position).first
  end

  def update_display_position(position)
    return unless position.is_a?(Integer)
    return if position == self.display_position

    cards_to_update = self.parent.cards - [self]
    end_index = cards_to_update.length
    position = end_index if position < 0
    position = 0 if position >= self.parent.cards.count

    # Update the current card's position
    self.update(display_position: position)

    # Update the positions of the other cards
    cards_to_update.each_with_index do |card, i|
      # Adjust the position depending on the current position of the card
      new_position = if position == 0
                       i + 1
                     elsif position == end_index
                       i
                     elsif i < position
                       i
                     else
                       i + 1
                     end
      card.update(display_position: new_position)
    end
  end
end
