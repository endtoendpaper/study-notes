class Deck < ApplicationRecord
  require 'csv'

  include HasSearchableSubItems
  include IsItem

  has_many :cards, dependent: :destroy
  alias_method :subitems, :cards

  validates :title, presence: true, length: { maximum: 255 }

  settings do
    mappings dynamic: false do
      indexes :title, type: :text
      indexes :description, type: :text
      indexes :user_id, type: :integer
      indexes :visibility, type: :integer
      indexes :group_ids, type: :integer
      indexes :all_collaborator_ids, type: :integer
      indexes :stargazers, type: :integer
      indexes :last_activity_at, type: :date
      indexes :tags, type: :keyword
    end
  end

  def as_indexed_json(options = {})
    as_json(only: %i[title description user_id visibility last_activity_at])
      .merge(group_ids: groups.pluck(:id))
      .merge(all_collaborator_ids: (collaborators.pluck(:id) + group_members.pluck(:id) + group_owners.pluck(:id) + [user_id]).uniq)
      .merge(stargazers: stargazers.pluck(:id))
      .merge(tags: tags.pluck(:name))
  end

  def searchable_subitems
    self.subitems
  end

  def first_card
    cards.first
  end

  def shuffle
    reorder = self.cards.shuffle
    reorder.each_with_index do | card, i |
      card.update(display_position: i)
    end
  end

  def cards_to_csv
    CSV.generate(headers: true) do |csv|
      csv << %w(front back)
      cards.each do |card|
        csv << [card.front.plain_text_body, card.back.plain_text_body]
      end
    end
  end
end
