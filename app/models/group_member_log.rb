class GroupMemberLog < ApplicationRecord
  belongs_to :group
  belongs_to :user, optional: true
  belongs_to :member, class_name: "User"

  enum :action, [ :changed_owner_to, :added_member, :removed_member ]

  validates_presence_of :action

  scope :for_display, -> { order("created_at DESC") }

  before_create :update_columns

  def update_columns
    self.user_handle = self.user.handle
    self.member_handle = self.member.handle
  end

  def action_string
    self.action.to_s.humanize.downcase
  end
end
