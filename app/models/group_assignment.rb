class GroupAssignment < ApplicationRecord
  include UpdatesRecordLastActivity

  belongs_to :group
  belongs_to :record, polymorphic: true

  validates :group, presence: true
  validates :record, presence: true
  validate :unique_join

  after_create :log_create_changes
  after_commit :queue_reindex_records
  before_destroy :destroy_group_chats
  before_destroy :log_destroy_changes
  after_destroy :queue_reindex_records

  scope :order_by_group_name, -> { sort_by{ |assignment| assignment.group.name } }

  private

    def unique_join
      errors.add(:base, "- #{group.name} is already associated.") unless self.persisted? if record&.group?(group)
    end

    def destroy_group_chats
      record.destroy if record.class.to_s == "Chat"
    end

    def log_create_changes
      CollaborationLog.create(item: self.record, user: User.find(self.record.whodunnit), action: :added_group, collaboration: self.group) if self.record.class.to_s != "Chat" and self.record.whodunnit
    end

    def log_destroy_changes
      CollaborationLog.create(item: self.record, user: User.find(self.record.whodunnit), action: :removed_group, collaboration: self.group) if self.record.class.to_s != "Chat" and self.record.whodunnit
    end

    def queue_reindex_records
      if self.record.class.to_s != "Chat" && self.record.persisted?
        ElasticsearchReindexJob.perform_async(record.class.name, record.id, 'reindex_group_ids')
        ElasticsearchReindexJob.perform_async(record.class.name, record.id, 'reindex_all_collaborator_ids')
        ElasticsearchReindexJob.perform_async(record.class.name, record.id, 'reindex_subitems_collaborators')
      end
    end
end
