class HelpSection < ApplicationRecord
  include HasEditNotice
  include HasRichTextExtensions

  has_rich_text :content

  validates :title, presence: true, length: { maximum: 100 }
  validates_presence_of :content

  default_scope { order('lower(help_sections.title)') }

  def has_edit_privileges user
    user && user.admin
  end
end
