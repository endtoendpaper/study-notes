# shared methods for items owned by users (e.g. Decks, Notes)
module IsItem
  extend ActiveSupport::Concern

  included do
    include Collaborative
    include HasCollaborationLogs
    include HasEditNotice
    include HasItemRoutes
    include HasUserActivityLogs
    include Searchable
    include Starrable
    include Taggable

    attr_accessor :create_another, :whodunnit

    belongs_to :user

    validates_presence_of :visibility
    validates :visibility, inclusion: 0..2

    before_save :update_last_activity_at
    after_commit :queue_reindex_all_collaborator_ids, if: :saved_change_to_user_id?

    default_scope { order(last_activity_at: :desc) }
    scope :ordered_by_id, -> { reorder(id: :asc) }
    scope :public_items, -> { where(visibility: 2) }
    scope :internal_items, -> { where("visibility > 0") }
    scope :private_items, -> { where(visibility: 0) }

    def owner
      user if user
    end

    def owner_handle
      '@' + owner.username unless owner.nil?
    end

    def editors
      (all_collaborators + [user]).uniq
    end

    def has_owner_privileges user
      user && (user.id == owner.id or user.admin)
    end

    def has_edit_privileges user
      user && (has_owner_privileges(user) || all_collaborators?(user))
    end

    def has_view_privileges user
      (!user && visibility == 2) || (user && (visibility >= 1)) || (user && has_edit_privileges(user))
    end

    def status
      return 'Private' if visibility == 0
      return 'Internal' if visibility == 1
      return 'Public' if visibility == 2
    end

    def reindex_all_collaborator_ids
      return unless Elasticsearch::Model.client.exists?(index: self.class.index_name, id: id)

      Elasticsearch::Model.client.update(
        index: self.class.index_name,
        id: id,
        retry_on_conflict: 3,
        body: {
          doc: {
            all_collaborator_ids: (collaborators.pluck(:id) + group_members.pluck(:id) + group_owners.pluck(:id) + [user_id]).uniq
          }
        }
      )
    end

    def reindex_stargazers
      return unless Elasticsearch::Model.client.exists?(index: self.class.index_name, id: id)

      Elasticsearch::Model.client.update(
        index: self.class.index_name,
        id: id,
        retry_on_conflict: 3,
        body: {
          doc: {
            stargazers: stargazers.pluck(:id)
          }
        }
      )
    end

    def reindex_group_ids
      return unless Elasticsearch::Model.client.exists?(index: self.class.index_name, id: id)

      Elasticsearch::Model.client.update(
        index: self.class.index_name,
        id: id,
        retry_on_conflict: 3,
        body: {
          doc: {
            group_ids: groups.pluck(:id)
          }
        }
      )
    end

    def reindex_tags
      return unless Elasticsearch::Model.client.exists?(index: self.class.index_name, id: id)

      Elasticsearch::Model.client.update(
        index: self.class.index_name,
        id: id,
        retry_on_conflict: 3,
        body: {
          doc: {
            tags: tags.pluck(:name)
          }
        }
      )
    end

    private

      def queue_reindex_all_collaborator_ids
        ElasticsearchReindexJob.perform_async(self.class.name, self.id, 'reindex_all_collaborator_ids')
      end

      def update_last_activity_at
        self.last_activity_at = Time.current
      end
  end
end
