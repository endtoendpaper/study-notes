module HasSubitemRoutes
  extend ActiveSupport::Concern

  included do
    def type
      @type ||= self.class.name
    end

    def route_type
      @route_type ||= self.type.tableize
    end

    def member_route_type
      @member_route_type ||= "#{parent.member_route_type}_#{self.type.singularize.underscore}"
    end

    def card_template_path
      "#{self.route_type}/#{self.route_type.singularize}_card"
    end
  end
end
