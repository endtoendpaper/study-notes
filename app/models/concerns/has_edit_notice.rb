module HasEditNotice
  extend ActiveSupport::Concern

  included do
    after_update :broadcast_edit_notice

    private

      def broadcast_edit_notice
        broadcast_update_to [self, "edit_notice"],
          target: "edit-notice",
          partial: "edit_notice"
      end
  end
end
