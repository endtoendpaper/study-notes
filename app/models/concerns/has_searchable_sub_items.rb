# shared methods for items owned by users (e.g. Decks, Notes)
module HasSearchableSubItems
  extend ActiveSupport::Concern

  included do
    after_commit :queue_reindex_subitems_collaborators, if: :saved_change_to_user_id?
    after_commit :queue_reindex_subitems_visibility, if: :saved_change_to_visibility?

    def reindex_subitems_collaborators
      searchable_subitems.each do |item|
        return unless Elasticsearch::Model.client.exists?(index: item.class.index_name, id: item.id)

        Elasticsearch::Model.client.update(
          index: item.class.index_name,
          id: item.id,
          retry_on_conflict: 3,
          body: {
            doc: {
              user_id: user_id,
              all_collaborator_ids: (collaborators.pluck(:id) + group_members.pluck(:id) + group_owners.pluck(:id) + [user_id]).uniq
            }
          }
        )
      end
    end

    def reindex_subitems_visibility
      searchable_subitems.each do |item|
        return unless Elasticsearch::Model.client.exists?(index: item.class.index_name, id: item.id)

        Elasticsearch::Model.client.update(
          index: item.class.index_name,
          id: item.id,
          retry_on_conflict: 3,
          body: {
            doc: {
              visibility: visibility
            }
          }
        )
      end
    end

    private

      def queue_reindex_subitems_collaborators
        ElasticsearchReindexJob.perform_async(self.class.name, self.id, 'reindex_subitems_collaborators')
      end

      def queue_reindex_subitems_visibility
        ElasticsearchReindexJob.perform_async(self.class.name, self.id, 'reindex_subitems_visibility')
      end
  end
end
