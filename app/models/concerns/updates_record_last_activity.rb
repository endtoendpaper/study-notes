module UpdatesRecordLastActivity
  extend ActiveSupport::Concern

  included do
    after_create :touch_record_last_activity, prepend: true
    before_destroy :touch_record_last_activity, prepend: true

    private

      def touch_record_last_activity
        self.record.update_column(:last_activity_at, Time.current) if self.record.respond_to?(:last_activity_at)
      end
  end
end
