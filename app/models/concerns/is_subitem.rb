# shared methods for subitems of items owned by users (e.g. Decks, Notes)
module IsSubitem
  extend ActiveSupport::Concern

  included do
    include HasEditNotice
    include HasSubitemRoutes
    include HasUserActivityLogs
    include Searchable

    attr_accessor :create_another, :whodunnit

    before_save :touch_parent_last_activity
    before_destroy :touch_parent_last_activity

    scope :ordered_by_updated, -> { reorder(updated_at: :desc)}

    def owner
      parent.owner
    end

    def owner_handle
      parent.owner_handle
    end

    def editors
      parent.editors
    end

    def has_owner_privileges user
      parent.has_owner_privileges(user)
    end

    def has_edit_privileges user
      parent.has_edit_privileges(user)
    end

    def has_view_privileges user
      parent.has_view_privileges(user)
    end

    def status
      parent.status
    end

    private

      def touch_parent_last_activity
        self.parent.update_column(:last_activity_at, Time.current) if self.parent.respond_to?(:last_activity_at)
      end
  end
end
