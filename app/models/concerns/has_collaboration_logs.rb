module HasCollaborationLogs
  extend ActiveSupport::Concern

  included do
    require 'csv'

    has_many :collaboration_logs, as: :item, dependent: :destroy

    after_commit :log_owner, on: [:create]

    def log_owner
      CollaborationLog.create(item: self, user: self.user, action: :changed_owner_to, collaboration: self.user)
    end

    def collaboration_logs_to_csv
      CSV.generate(headers: true) do |csv|
        csv << %w(user action name datetime)
        collaboration_logs.each do |log|
          csv << [log.user_handle, log.action, log.collaboration_name, log.created_at]
        end
      end
    end
  end
end
