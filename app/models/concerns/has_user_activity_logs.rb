module HasUserActivityLogs
  extend ActiveSupport::Concern

  included do
    has_many :passive_activity_logs, class_name:  "ActivityLog",
      foreign_key: "record_id",
      dependent:   :destroy

    attr_accessor :is_new_record

    before_create :set_new_record
    after_commit :log_create_activity, on: [:create]
    after_commit :log_edit_activity, on: [:update]

    private

      def set_new_record
        self.is_new_record = true
      end

      def log_create_activity
        ActivityLog.create(user: User.find(whodunnit), action: :created, record: self) if self.whodunnit
      end

      def log_edit_activity
        ActivityLog.create(user: User.find(whodunnit), action: :edited, record: self) if self.whodunnit unless self.is_new_record
      end
  end
end
