module IsSupportUserCommunication
  extend ActiveSupport::Concern

  included do
    include IsSupport

    private

      def rich_or_plain_text
        if rich_text_body.blank? && body.blank?
          errors.add(:base, "Details can't blank")
        elsif !rich_text_body.blank? && !SiteSetting.rich_text_support_issues?
          errors.add(:base, "Only plain text submissions")
        elsif !rich_text_body.blank? && !body.blank?
          errors.add(:base, "Only plain text or rich text")
        end
      end

      def rich_text_length
        if SiteSetting.rich_text_support_issues? && body.blank?
          if ActionController::Base.helpers.strip_tags(self.rich_text_body.to_plain_text).length > 2000
            errors.add(:base, "Rich text body is too long (maximum is 2000 characters)")
          elsif ActionController::Base.helpers.strip_tags(self.rich_text_body.to_plain_text).length < 10
            errors.add(:base, "Rich text body is too short (minimum is 10 characters)")
          end
        end
      end
  end
end
