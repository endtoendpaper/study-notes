module Collaborative
  extend ActiveSupport::Concern

  included do
    has_many :collaborations, as: :record, dependent: :destroy
    has_many :collaborators, :through => :collaborations, source: :user

    has_many :group_assignments, as: :record, dependent: :destroy
    has_many :groups, :through => :group_assignments
    has_many :group_members, :through => :groups, source: :members
    has_many :group_owners, :through => :groups, source: :user

    has_many :collaboration_logs, as: :item, dependent: :destroy

    accepts_nested_attributes_for :collaborations, allow_destroy: true
    accepts_nested_attributes_for :group_assignments, allow_destroy: true

    def change_owner(user, whodunnit)
      unless self.user == user
        self.update({ user: user })
        CollaborationLog.create(item: self, user: whodunnit, action: :changed_owner_to, collaboration: user)
      end
    end

    def collaborate(user, whodunnit = nil)
      unless collaborater?(user)
        collaborators << user
        CollaborationLog.create(item: self, user: whodunnit, action: :added_collaborator, collaboration: user) unless self.class.to_s == "Chat"
      end
    end

    def uncollaborate(user, whodunnit = nil)
      if self.collaborater?(user)
        collaborators.destroy(user)
        CollaborationLog.create(item: self, user: whodunnit, action: :removed_collaborator, collaboration: user) unless self.class.to_s == "Chat"
      end
    end

    def collaborators_count
      collaborators.count
    end

    def collaborater?(user)
      return false if !user
      collaborators.include?(user)
    end

    def add_group(group, whodunnit = nil)
      unless self.group?(group)
        groups << group
        CollaborationLog.create(item: self, user: whodunnit, action: :added_group, collaboration: group) unless self.class.to_s == "Chat"
      end
    end

    def remove_group(group, whodunnit = nil)
      if self.group?(group)
        groups.destroy(group)
        CollaborationLog.create(item: self, user: whodunnit, action: :removed_group, collaboration: group) unless self.class.to_s == "Chat"
      end
    end

    def groups_count
      groups.count
    end

    def group?(group)
      groups.include?(group)
    end

    def all_collaborators
      (collaborators + group_members + group_owners).uniq
    end

    def all_collaborators?(user)
      return false if !user
      all_collaborators.include?(user)
    end
  end
end
