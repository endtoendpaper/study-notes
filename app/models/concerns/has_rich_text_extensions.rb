module HasRichTextExtensions
  extend ActiveSupport::Concern

  included do
    has_many :tables, as: :record, dependent: :destroy

    before_save :update_rich_text_plain_text

    after_save :update_table_associations

    private

      def update_table_associations
        Table.where(record: self).each do |table|
          table.update(record_saved: false)
        end

        rich_text_associations.each do |field|
          if self.send("#{field}")&.body
            self.send("#{field}").body.attachables.each do |attachment|
              obj = ActionText::Attachable.from_attachable_sgid(attachment.attachable_sgid)
              if obj.is_a? Table
                obj.update(record: self)
                obj.update(record_saved: true)
              end
            end
          end
        end
      end

      def update_rich_text_plain_text
        rich_text_associations.each do |field|
          self.send("#{field}").plain_text_body = self.send("#{field}").body.to_plain_text unless self.send("#{field}")&.body.nil?
        end
      end

      def rich_text_associations
        _reflections.keys.select{ |a| a.to_s.include? "rich_text_" }.map{ |a| a.to_s.delete_prefix("rich_text_") }
      end
  end
end
