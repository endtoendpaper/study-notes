module Starrable
  extend ActiveSupport::Concern

  included do
    has_many :stars, as: :record, dependent: :destroy
    has_many :stargazers, :through => :stars, source: :user

    def add_star(user)
      stargazers << user unless self.stargazer?(user)
      ActivityLog.create(user: user, action: :starred, record: self)
    end

    def unstar(user)
      stargazers.destroy(user) if self.stargazer?(user)
      ActivityLog.create(user: user, action: :unstarred, record: self)
    end

    def star_count
      stargazers.count
    end

    def stargazer?(user)
      return false if !user
      stargazers.include?(user)
    end
  end
end
