module IsSupport
  extend ActiveSupport::Concern

  included do
    include HasRichTextExtensions

    belongs_to :user, optional: true
    has_rich_text :rich_text_body

    validate :rich_or_plain_text
    validates_length_of :body, minimum: 10, maximum: 2000, allow_blank: true
    validate :rich_text_length

    def creator
      user
    end

    def creator_handle
      '@' + creator.username unless creator.nil?
    end

    def has_edit_privileges user
      user && user.id == creator.id
    end
  end
end
