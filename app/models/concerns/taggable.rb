module Taggable
  extend ActiveSupport::Concern

  included do
    has_many :taggings, as: :record, dependent: :destroy
    has_many :tags, :through => :taggings, source: :tag

    accepts_nested_attributes_for :taggings, allow_destroy: true

    def tag(tag, whodunnit = nil)
      tags << tag unless self.tag?(tag)
      ActivityLog.create(user: whodunnit, action: :tagged, record: self, details: "with " + tag.name) if whodunnit
    end

    def untag(tag, whodunnit = nil)
      tags.destroy(tag) if self.tag?(tag)
      ActivityLog.create(user: whodunnit, action: :untagged, record: self, details: "with " + tag.name) if whodunnit
    end

    def tags_count
      tags.count
    end

    def tag?(tag)
      tags.include?(tag)
    end
  end
end
