module HasItemRoutes
  extend ActiveSupport::Concern

  included do
    def type
      @type ||= self.class.name
    end

    def route_type
      @route_type ||= self.type.tableize
    end

    def member_route_type
      @member_route_type ||= type.singularize.underscore
    end

    def show_template_path
      "#{route_type}/#{type.underscore}"
    end

    def card_template_path
      "#{route_type}/#{route_type.singularize}_card"
    end
  end
end
