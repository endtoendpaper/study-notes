class Event < ApplicationRecord
  include HasRichTextExtensions
  include IsSubitem

  belongs_to :parent, class_name: 'Calendar', foreign_key: 'calendar_id'

  has_rich_text :description

  validates :summary, presence: true, length: { maximum: 255 }
  validates_presence_of :start
  validates_presence_of :end
  validate :start_before_end

  attr_accessor :month, :year, :day, :user_calendar

  default_scope { order(:start) }

  settings do
    mappings dynamic: false do
      indexes :summary, type: :text
      indexes :location, type: :text
      indexes :description, type: :text
      indexes :user_id, type: :integer
      indexes :visibility, type: :integer
      indexes :all_collaborator_ids, type: :integer
    end
  end

  def as_indexed_json(options = {})
    as_json(only: %i[summary location])
      .merge(description: description.to_plain_text)
      .merge(user_id: parent.user_id)
      .merge(visibility: parent.visibility)
      .merge(all_collaborator_ids: (parent.collaborators.pluck(:id) + parent.group_members.pluck(:id) + parent.group_owners.pluck(:id) + [parent.user_id]).uniq)
  end

  private
    def start_before_end
      if self.all_day
        if (self.start && self.end) && (self.start > self.end)
          errors.add(:start, "must begin before end")
        end
      else
        if (self.start && self.end) && (self.start >= self.end)
          errors.add(:start, "must begin before end")
        end
      end
    end
end
