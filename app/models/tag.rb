class Tag < ApplicationRecord
  include Searchable

  has_many :taggings, dependent: :destroy

  before_save :update_last_activity_at

  UserItems.item_list.each do |item|
    has_many "#{item}".to_sym, :through => :taggings, source: :record, source_type: "#{UserItems.class(item)}"
  end

  VALID_TAG_REGEX = /\A[a-z\d\-_+]*\z/i
  validates :name, presence: true, length: { maximum: 100 },
                    format: { with: VALID_TAG_REGEX, message: 'can only contain letters, numbers, dashes, underscores, or pluses' },
                    uniqueness: { case_sensitive: false }

  default_scope { order(updated_at: :desc) }
  scope :ordered_by_id, -> { reorder(id: :asc) }

  settings do
    mappings dynamic: false do
      indexes :name, type: :text
    end
  end

  def as_indexed_json(options = {})
    as_json(only: [:name])
  end

  def to_param
    self.name_was
  end

  def self.search(query, page: 1, per_page: 50)
    return unless query.present?

    page = page.to_i <= 0 ? 1 : page.to_i

    search_body = {
      query: {
        bool: {
          must: [
            {
              multi_match: {
                query: query,
                fields: ['name'],
                "fuzziness": "AUTO"
              }
            }
          ]
        }
      }
    }

    if per_page
      search_body.merge!({
        from: (page.to_i - 1) * per_page.to_i,
        size: per_page.to_i
      })
    end

    search_response = Tag.__elasticsearch__.search(search_body)

    WillPaginate::Collection.create(page.to_i, per_page.to_i, search_response.response['hits']['total']['value']) do |pager|
      pager.replace(search_response.records.to_a)
    end
  end

  def card_template_path
    "tags/tag_card"
  end

  def should_delete?
    UserItems.item_list.reduce(0) { |sum, item| sum + self.send(item).count } == 0
  end

  def public_items
    UserItems.item_list.map { |item| self.send(item).public_items }.flatten.uniq.sort_by(&:updated_at).reverse
  end

  def self.public_tags
    UserItems.public_items.map(&:tags).flatten.uniq.sort_by(&:updated_at).reverse
  end

  private

    def update_last_activity_at
      self.last_activity_at = Time.current
    end
end
