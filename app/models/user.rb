class User < ApplicationRecord
  include HasEditNotice
  include Searchable

  attr_writer :login

  devise :database_authenticatable, :registerable, :trackable,
         :confirmable, :lockable, :timeoutable,
         :recoverable, :rememberable, :validatable,
         :omniauthable, omniauth_providers: [:google_oauth2]

  if Rails.env.development?
    devise :masqueradable
  end

  has_one_attached :avatar do |attachable|
    attachable.variant :tiny, resize_to_fit: [15, 15]
    attachable.variant :thumb, resize_to_fit: [100, 100]
    attachable.variant :medium, resize_to_fit: [200, 200]
  end

  has_many :support_issues, dependent: :nullify
  has_many :support_replies, dependent: :nullify
  has_many :support_comments, dependent: :nullify
  has_many :support_notes, dependent: :nullify

  has_many :active_relationships, class_name:  "Relationship",
            foreign_key: "follower_id",
            dependent:   :destroy
  has_many :passive_relationships, class_name:  "Relationship",
            foreign_key: "followed_id",
            dependent:   :destroy
  has_many :following, through: :active_relationships, source: :followed
  has_many :followers, through: :passive_relationships, source: :follower

  has_many :collaborations, dependent: :destroy

  has_many :group_members, dependent: :destroy
  has_many :group_memberships, through: :group_members, source: :group
  has_many :groups, dependent: :destroy

  has_many :group_member_logs, dependent: :nullify
  has_many :passive_group_member_logs, class_name:  "GroupMemberLog",
            foreign_key: "member_id",
            dependent:   :nullify
  has_many :collaboration_logs, dependent: :nullify
  has_many :passive_collaboration_logs, class_name:  "CollaborationLog",
            foreign_key: "collaboration_id",
            dependent:   :nullify

  has_many :chats, through: :collaborations, source: :record, source_type: 'Chat'
  has_many :group_chats, through: :group_memberships, source: :chats
  has_many :owned_group_chats, through: :groups, source: :chats

  has_many :chat_preferences, dependent: :destroy
  has_many :messages, dependent: :nullify

  has_many :stars, dependent: :destroy

  UserItems.item_list.each do |item|
    has_many "#{item}".to_sym, dependent: :destroy
    has_many "collaborated_#{item}".to_sym, :through => :collaborations, source: :record, source_type: "#{UserItems.class(item)}"
    has_many "owned_group_#{item}".to_sym, :through => :groups, source: "#{item}".to_sym
    has_many "group_collaborated_#{item}".to_sym, :through => :group_memberships, source: "#{item}".to_sym
    has_many "starred_#{item}".to_sym, :through => :stars, source: :record, source_type: "#{UserItems.class(item)}"
  end

  has_many :activity_logs, dependent: :destroy
  has_many :passive_activity_logs, class_name:  "ActivityLog",
            foreign_key: "record_id",
            dependent:   :destroy

  VALID_USERNAME_REGEX = /\A[a-z\d\-_]*\z/i
  validates :username, presence: true, length: { maximum: 100 },
                    format: { with: VALID_USERNAME_REGEX, message: 'can only contain letters, numbers, dashes, or underscores' },
                    uniqueness: { case_sensitive: false }
  validates :avatar, blob: { content_type: :image, size_range: 1..(5.megabytes) }

  default_scope { order('lower(users.username)') }
  scope :ordered_by_id, -> { reorder(id: :asc) }
  scope :admins, -> { where(admin: true) }

  settings do
    mappings dynamic: false do
      indexes :username, type: :text
      indexes :email, type: :keyword
      indexes :admin, type: :boolean
    end
  end

  def as_indexed_json(options = {})
    as_json(only: [:username, :email, :admin])
  end

  def to_param
    self.username_was
  end

  def self.from_omniauth(access_token)
    data = access_token.info
    user = User.where(email: data['email']).first

    # Create users if they don't exist
    if SiteSetting.allow_users_to_join? && !user
      email_username = data['email'].split("@").first
      email_username = email_username.gsub!(/\W/, '') if email_username.gsub!(/\W/, '').present?
      new_username = email_username
      inc = 1
      while User.where("lower(username) = ?", new_username.downcase).count > 0
        new_username = email_username + inc.to_s
        inc = inc + 1
      end
      user = User.new(username: new_username,
                         email: data['email'],
                         password: Devise.friendly_token[0, 20])
      user.skip_confirmation!
      user.save
    elsif user && !user.confirmed?
      user.skip_confirmation!
      user.save
    end

    user
  end

  def login
    @login || self.username || self.email
  end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    login = conditions.delete(:login)
    if (login.include?('@'))
      where(conditions.to_h).where(["lower(email) = :value", { :value => login.downcase }]).first
    else
      where(conditions.to_h).where(["username = :value", { :value => login }]).first
    end
  end

  def self.search(query, page: 1, per_page: 50)
    page = page.to_i <= 0 ? 1 : page.to_i

    if query.present?
      search_body = {
        query: {
          multi_match: {
            query: query,
            fields: ['username'],
            fuzziness: "AUTO"
          }
        }
      }

      if per_page
        search_body.merge!({
          from: (page.to_i - 1) * per_page.to_i,
          size: per_page.to_i
        })
      end

      search_response = User.__elasticsearch__.search(search_body)

      WillPaginate::Collection.create(page.to_i, per_page.to_i, search_response.response['hits']['total']['value']) do |pager|
        pager.replace(search_response.records.to_a)
      end
    else
      all.order('username ASC').paginate(page: page, per_page: per_page)
    end
  end

  def self.admin_search(query, admin: false, page: 1, per_page: 50)
    page = page.to_i <= 0 ? 1 : page.to_i

    if query.present?
      search_body = {
        query: {
          bool: {
            must: [
              {
                multi_match: {
                  query: query,
                  fields: ['username^2', 'email'],
                  "fuzziness": "AUTO"
                }
              }
            ]
          }
        }
      }

      if admin
        search_body[:query][:bool][:filter] = [
          { term: { admin: true } }
        ]
      end

      if per_page
        search_body.merge!({
          from: (page.to_i - 1) * per_page.to_i,
          size: per_page.to_i
        })
      end

      search_response = User.__elasticsearch__.search(search_body)

      WillPaginate::Collection.create(page.to_i, per_page.to_i, search_response.response['hits']['total']['value']) do |pager|
        pager.replace(search_response.records.to_a)
      end
    elsif admin
      admins.order('username ASC').paginate(page: page, per_page: per_page)
    else
      all.order('username ASC').paginate(page: page, per_page: per_page)
    end
  end

  def follow(other_user)
    unless self == other_user || !SiteSetting.follow_users? || self.following?(other_user)
      following << other_user
      ActivityLog.create(user: self, action: :followed, record: other_user)
    end
  end

  def unfollow(other_user)
    if self.following?(other_user) && SiteSetting.follow_users?
      following.destroy(other_user)
      ActivityLog.create(user: self, action: :unfollowed, record: other_user)
    end
  end

  def following?(other_user)
    following.include?(other_user)
  end

  def handle
    '@' + username
  end

  def all_groups
    Group.select('groups.*, LOWER(groups.name) AS lower_name').where(id: group_memberships.select(:id)).or(Group.where(user_id: id)).distinct.order('lower_name')
  end

  def groups_count
    all_groups.length
  end

  def all_chats(page: 1, per_page: 50)
    page = page.to_i <= 0 ? 1 : page.to_i

    all_chat_ids = chats.pluck(:id) + group_chats.pluck(:id) + owned_group_chats.pluck(:id)

    all_chats = Chat.where(id: all_chat_ids).joins(:chat_preferences)
                  .where(chat_preferences: { user_id: self.id, hide: false })

    pinned_chats_with_unread = all_chats.joins(:chat_preferences)
                                .where(chat_preferences: { user_id: self.id, pin: true })
                                .where.not(chat_preferences: { unread_messages: 0 })
                                .order(last_message: :desc)
                                .includes(:chat_preferences)

    pinned_chats_without_unread = all_chats.joins(:chat_preferences)
                                .where(chat_preferences: { user_id: self.id, pin: true, unread_messages: 0 } )
                                .order(last_message: :desc)
                                .includes(:chat_preferences)

    unpinned_chats_with_unread = all_chats.joins(:chat_preferences)
                                  .where(chat_preferences: { user_id: self.id, pin: false })
                                  .where.not(chat_preferences: { unread_messages: 0 })
                                  .order(last_message: :desc)
                                  .includes(:chat_preferences)

    unpinned_chats_without_unread = all_chats.joins(:chat_preferences)
                                      .where(chat_preferences: { user_id: self.id, pin: false, unread_messages: 0 } )
                                      .order(last_message: :desc)
                                      .includes(:chat_preferences)

    all_chats_in_order = (pinned_chats_with_unread + pinned_chats_without_unread + unpinned_chats_with_unread + unpinned_chats_without_unread).paginate(page: page, per_page: per_page)
    all_preferences = all_chats_in_order.map { |chat| chat.chat_preferences.find_by(user_id: self.id) }
    [all_chats_in_order, all_preferences]
  end

  def find_or_create_user_chat(user)
    chat = chats.joins(:collaborators).where(collaborators: { id: user.id } ).first
    if !chat
      chat = Chat.create
      chat.collaborate(self)
      chat.collaborate(user)
      ChatPreference.create(user: self, chat: chat)
      ChatPreference.create(user: user, chat: chat)
    end
    chat
  end

  def update_unread_msg_count
    unread_msgs = chat_preferences.sum(:unread_messages)
    self.update(unread_messages: unread_msgs)
  end

  def broadcast_unread_msg_update
    broadcast_update_to [self, "unread_msg_count_footer"],
        target: "unread_msg_count_footer",
        partial: "user_unread_msg_count",
        locals: { user: self }
      broadcast_update_to [self, "unread_msg_count_header"],
        target: "unread_msg_count_header",
        partial: "user_unread_msg_count",
        locals: { user: self }
  end

  def viewable_activity_logs_for(other_user)
    if other_user == self || admin?
      other_user.activity_logs.for_display.limit(10)
    else
      logs = other_user.activity_logs.for_display.to_a
      hide_stars = other_user.hide_stars
      hide_relationships = other_user.hide_relationships || !SiteSetting.follow_users?

      filtered_logs = []
      logs.each do |log|
        next if hide_stars && log.action.to_s.include?('star')
        next if hide_relationships && log.action.to_s.include?('follow')
        next if log.record.methods.include?(:visibility) && !log.record.has_view_privileges(self)
        next if log.record.methods.include?(:parent) && !log.record.has_view_privileges(self)

        filtered_logs << log
        break if filtered_logs.size >= 10
      end

      filtered_logs
    end
  end

  def public_activity_logs
    logs = self.activity_logs.for_display.to_a
    hide_stars = self.hide_stars
    hide_relationships = self.hide_relationships || !SiteSetting.follow_users?

    filtered_logs = []
    logs.each do |log|
      next if hide_stars && log.action.to_s.include?('star')
      next if hide_relationships && log.action.to_s.include?('follow')
      next if log.record.methods.include?(:visibility) && log.record.visibility != 2
      next if log.record.methods.include?(:parent) && log.record.parent.visibility != 2

      filtered_logs << log
      break if filtered_logs.size >= 10
    end

    filtered_logs
  end

  def viewable_group_activity_logs_for(group)
    group.all_group_members.map { |user| viewable_activity_logs_for(user) }.flatten.sort_by(&:created_at).reverse.first(10)
  end

  def get_events_for dates, tz, limit = nil
    Calendar.get_user_events_for self, dates, tz, limit
  end
end
