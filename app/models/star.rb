class Star < ApplicationRecord
  include UpdatesRecordLastActivity

  belongs_to :user
  belongs_to :record, polymorphic: true

  validates :user, presence: true
  validates :record, presence: true
  validates :user, uniqueness: { scope: :record }

  after_commit :queue_reindex_record
  after_destroy :queue_reindex_record

  def queue_reindex_record
    return unless record.persisted?
    ElasticsearchReindexJob.perform_async(record.class.name, record.id, 'reindex_stargazers')
  end
end
