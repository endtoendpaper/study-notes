class Tagging < ApplicationRecord
  include UpdatesRecordLastActivity

  belongs_to :tag
  belongs_to :record, polymorphic: true

  attr_accessor :tag_name

  validates :tag, presence: true
  validates :record, presence: true
  validate :unique_join

  before_validation :create_missing_tag
  after_create :log_create_activity
  after_create :touch_tag_last_activity, prepend: true
  after_commit :queue_reindex_record
  before_destroy :log_destroy_activity
  before_destroy :destroy_tag_if_unused
  before_destroy :touch_tag_last_activity, prepend: true
  after_destroy :queue_reindex_record

  scope :order_tag_name, -> { sort_by{ |tagging| tagging.tag.name } }

  private

    def create_missing_tag
      if self.tag.nil? && self.tag_name
        self.tag = Tag.find_or_create_by(name: self.tag_name)
      end
      errors.add(:base, "- #{self.tag_name} is invalid. Tags can only contain letters, numbers, dashes, underscores, or pluses and be less than 100 characters.") unless self.tag&.persisted?
    end

    def unique_join
      errors.add(:base, "- #{tag.name} is already associated.") unless self.persisted? if self.tag&.persisted? && self.record&.tag?(self.tag)
    end

    def destroy_tag_if_unused
      tag.destroy if UserItems.item_list.reduce(0) { |sum, item| sum + tag.send(item).count } <= 1
    end

    def log_create_activity
      ActivityLog.create(user: User.find(record.whodunnit), action: :tagged, record: self.record, details: "with " + self.tag.name) if self.record.whodunnit
    end

    def log_destroy_activity
      ActivityLog.create(user: User.find(record.whodunnit), action: :untagged, record: self.record, details: "with " + self.tag.name) if self.record.whodunnit
    end

    def touch_tag_last_activity
      self.tag.update_column(:last_activity_at, Time.current) if self.tag
    end

    def queue_reindex_record
      return unless record.persisted?
      ElasticsearchReindexJob.perform_async(record.class.name, record.id, 'reindex_tags')
    end
end
