class RecurringEvent < ApplicationRecord
  include HasRichTextExtensions
  include IsSubitem

  belongs_to :parent, class_name: 'Calendar', foreign_key: 'calendar_id'

  has_rich_text :description

  serialize :exceptions, coder: YAML

  validates :summary, presence: true, length: { maximum: 255 }
  validates_presence_of :start
  validates_presence_of :end
  validates_presence_of :rrule
  validate :start_before_end

  before_save :set_recurrence_end, if: :should_set_recurrence_end?

  attr_accessor :month, :year, :day, :event_month, :event_year, :event_day, :user_calendar

  default_scope { order(:start) }

  settings do
    mappings dynamic: false do
      indexes :summary, type: :text
      indexes :location, type: :text
      indexes :description, type: :text
      indexes :user_id, type: :integer
      indexes :visibility, type: :integer
      indexes :all_collaborator_ids, type: :integer
    end
  end

  def as_indexed_json(options = {})
    as_json(only: %i[summary location])
      .merge(description: description.to_plain_text)
      .merge(user_id: parent.user_id)
      .merge(visibility: parent.visibility)
      .merge(all_collaborator_ids: (parent.collaborators.pluck(:id) + parent.group_members.pluck(:id) + parent.group_owners.pluck(:id) + [parent.user_id]).uniq)
  end

  def self.convert_rrule_to_tz rrule_string, original_tz, new_tz, startdate
    offset = 0
    new_date = startdate.to_date
    if (startdate.in_time_zone(new_tz).to_date > startdate.in_time_zone(original_tz).to_date)
      new_date = new_date + 1.day
      offset = 1
    elsif (startdate.in_time_zone(new_tz).to_date < startdate.in_time_zone(original_tz).to_date)
      new_date = new_date - 1.day
      offset = -1
    else
      return rrule_string
    end
    rules = rrule_string.split(";")
    rules.map! do |rule|
      rule_arr = rule.split("=")
      if rule_arr[0] == "BYMONTH"
        rule = "BYMONTH=#{new_date.month}"
      elsif rule_arr[0] == "BYMONTHDAY"
        rule = "BYMONTHDAY=#{new_date.day}"
      elsif rule_arr[0] == "BYDAY"
        days = rule_arr[1].split(",")
        new_days = []
        weekdays = ["SU", "MO", "TU", "WE", "TH", "FR", "SA"]
        days.each do |day|
          index = weekdays.each_index.select{ |i| weekdays[i] == day }.first
          new_index = index + offset
          new_index = 0 if new_index == 7
          new_index = 6 if new_index == -1
          new_days << weekdays[new_index]
        end
        rule = "BYDAY=#{new_days.join(',')}"
      elsif rule_arr[0] == "BYSETPOS"
        if rule_arr[1] != "-1"
          nth = ['1', '2', '3', '4', '5', '6'];
          month_start_day_offset = Date.new(new_date.year, new_date.month, 1).wday
          if (((new_date.day + month_start_day_offset) % 7) == 0)
            nthDay = nth[((new_date.day + month_start_day_offset)/7)-1];
          else
            nthDay = nth[((new_date.day + month_start_day_offset)/7).to_i];
          end
          rule = "BYSETPOS=#{nthDay}"
        end
      end
      rule
    end
    rules.join(";")
  end

  def events search_start, search_end
    exceptions_datetimes = []
    self.exceptions.each do |exception|
      exceptions_datetimes << exception.to_datetime
    end
    event_dates = RRule::Rule.new(self.rrule, dtstart: self.start.to_date, exdate: exceptions_datetimes).between(Time.new(search_start.year, search_start.month, search_start.day), Time.new(search_end.year, search_end.month, search_end.day))
    events = []
    event_dates.each do |start_date|
      new_start = DateTime.new(start_date.year, start_date.month, start_date.day, self.start.hour, self.start.min)
      new_end = new_start + (self.end - self.start).to_i.seconds
      events << RecurringEvent.new(id: self.id, created_at: self.created_at, updated_at: self.updated_at, summary: self.summary, location: self.location, calendar_id: self.calendar_id, rrule: self.rrule, exceptions: self.exceptions, all_day: self.all_day, start: new_start, end: new_end)
    end
    events
  end

  def first_event_start
    exceptions_datetimes = []
    self.exceptions.each do |exception|
      exceptions_datetimes << exception.to_datetime
    end
    first_event_date = RRule::Rule.new(self.rrule, dtstart: self.start.to_date, exdate: exceptions_datetimes).all(limit: 1).first
    DateTime.new(first_event_date.year, first_event_date.month, first_event_date.day, self.start.hour, self.start.min)
  end

  private

    def start_before_end
      if self.all_day
        if (self.start && self.end) && (self.start > self.end)
          errors.add(:start, "must begin before end")
        end
      else
        if (self.start && self.end) && (self.start >= self.end)
          errors.add(:start, "must begin before end")
        end
      end
    end

    def set_recurrence_end
      return unless self.rrule.include? "COUNT" or self.rrule.include? "UNTIL"
      self.exceptions = self.exceptions || []
      exceptions_datetimes = []
      self.exceptions.each do |exception|
        exceptions_datetimes << exception.to_datetime
      end
      begin
        recurrence_end_date = RRule::Rule.new(self.rrule, dtstart: self.start.to_date, exdate: exceptions_datetimes).all.last
        self.recurrence_end = DateTime.new(recurrence_end_date.year, recurrence_end_date.month, recurrence_end_date.day, self.start.hour, self.start.min)
        self.recurrence_end = self.recurrence_end + (self.end - self.start).to_i.seconds
      rescue => e
        errors.add(:rrule, "must be valid")
      end
    end

    def should_set_recurrence_end?
      self.new_record? || self.saved_change_to_rrule? || self.saved_change_to_start?
    end
end
