json.extract! card, :id, :front, :back, :deck_id, :created_at, :updated_at
json.url card_url(card, format: :json)
json.front card.front.to_s
json.back card.back.to_s
