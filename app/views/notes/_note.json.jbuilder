json.extract! note, :id, :user_id, :title, :text, :created_at, :updated_at
json.url note_url(note, format: :json)
json.text note.text.to_s
