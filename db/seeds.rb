if SiteSetting.count < 1
  SiteSetting.create! :users_can_join => 'true', :site_name => 'My New Site', :display_site_name => 'true'
  user = User.new(:username => ENV['ADMIN_USERNAME'], :admin => true, :email => ENV['ADMIN_EMAIL'], :password => ENV['ADMIN_PW'], :password_confirmation => ENV['ADMIN_PW'])
  user.skip_confirmation!
  user.save!
else
  puts "Already seeded database."
end
