class AddHideRelationshipsToUsers < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :hide_relationships, :boolean, default: false
    add_index :users, :hide_relationships
  end
end
