class AddFollowUsersToSiteSettings < ActiveRecord::Migration[7.0]
  def change
    add_column :site_settings, :follow_users, :boolean, default: true
  end
end
