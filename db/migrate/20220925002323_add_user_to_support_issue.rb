class AddUserToSupportIssue < ActiveRecord::Migration[7.0]
  def change
    add_reference :support_issues, :user, null: true, foreign_key: true
  end
end
