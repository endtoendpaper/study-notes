class AddAdminIndexToUsers < ActiveRecord::Migration[7.0]
  def change
    add_index :users, :admin
  end
end
