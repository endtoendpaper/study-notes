class AddTagsIndex < ActiveRecord::Migration[7.0]
  def change
    add_index :notes_tags, [:tag_id, :note_id], unique: true
    add_index :decks_tags, [:tag_id, :deck_id], unique: true
  end
end
