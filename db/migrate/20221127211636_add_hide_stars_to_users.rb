class AddHideStarsToUsers < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :hide_stars, :boolean, default: false
    add_index :users, :hide_stars
  end
end
