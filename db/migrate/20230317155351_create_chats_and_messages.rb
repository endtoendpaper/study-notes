class CreateChatsAndMessages < ActiveRecord::Migration[7.0]
  def change
    create_table :chats do |t|
      t.datetime :last_message

      t.timestamps
    end
    create_table :messages do |t|
      t.text :content
      t.string :user_username
      t.references :user, null: true, foreign_key: true
      t.references :chat, null: false, foreign_key: true

      t.timestamps
    end
    create_table :chat_preferences do |t|
      t.boolean :hide, default: false
      t.boolean :pin, default: false
      t.integer :unread_messages, default: 0
      t.references :user, null: false, foreign_key: true
      t.references :chat, null: false, foreign_key: true

      t.timestamps
    end
    add_index :chat_preferences, [:user_id, :chat_id], unique: true
    add_column :users, :unread_messages, :integer, default: 0
  end
end
