class AddIndexesToTagsDecksNotesCards < ActiveRecord::Migration[7.0]
  def change
    add_index :cards, :created_at
    add_index :decks, :created_at
    add_index :notes, :created_at
    add_index :tags, :created_at

    add_index :cards, :updated_at
    add_index :decks, :updated_at
    add_index :notes, :updated_at
    add_index :tags, :updated_at

    add_index :decks,"to_tsvector('english', title)", using: :gin, name: "tsvector_decks_title_idx"
    add_index :decks,"to_tsvector('english', description)", using: :gin, name: "tsvector_decks_description_idx"
    add_index :decks, :visibility

    add_index :notes,"to_tsvector('english', title)", using: :gin, name: "tsvector_notes_title_idx"
    add_index :notes, :visibility

    add_index :tags, :name
    add_index :tags,"to_tsvector('english', name)", using: :gin, name: "tsvector_tags_name_idx"
  end
end
