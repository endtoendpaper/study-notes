class AddRichTextSupportIssuesToSiteSettings < ActiveRecord::Migration[7.0]
  def change
    add_column :site_settings, :rich_text_support_issues, :boolean, default: true
  end
end
