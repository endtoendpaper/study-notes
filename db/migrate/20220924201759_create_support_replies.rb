class CreateSupportReplies < ActiveRecord::Migration[7.0]
  def change
    create_table :support_replies do |t|
      t.text :body
      t.references :support_issue, null: false, foreign_key: true

      t.timestamps
    end
  end
end
