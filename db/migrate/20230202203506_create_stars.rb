class CreateStars < ActiveRecord::Migration[7.0]
  def change
    drop_table :note_stars
    drop_table :deck_stars

    create_table :stars do |t|
      t.string :record_type
      t.bigint :record_id
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
    add_index :stars, [:record_type, :record_id], unique: false
    add_index :stars, [:record_type, :record_id, :user_id], unique: true
  end
end
