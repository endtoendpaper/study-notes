class CreateSupportNotes < ActiveRecord::Migration[7.0]
  def change
    create_table :support_notes do |t|
      t.text :body
      t.string :user_email
      t.integer :user_id

      t.timestamps
    end
  end
end
