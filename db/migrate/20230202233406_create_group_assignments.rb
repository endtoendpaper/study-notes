class CreateGroupAssignments < ActiveRecord::Migration[7.0]
  def change
    drop_table :deck_groups
    drop_table :note_groups

    create_table :group_assignments do |t|
      t.string :record_type
      t.bigint :record_id
      t.references :group, null: false, foreign_key: true

      t.timestamps
    end
    add_index :group_assignments, [:record_type, :record_id], unique: false
    add_index :group_assignments, [:record_type, :record_id, :group_id], unique: true, name: 'index_group_assignments_on_record_and_group_id'
  end
end
