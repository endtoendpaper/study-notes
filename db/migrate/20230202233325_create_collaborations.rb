class CreateCollaborations < ActiveRecord::Migration[7.0]
  def change
    drop_table :deck_collaborations
    drop_table :note_collaborations

    create_table :collaborations do |t|
      t.string :record_type
      t.bigint :record_id
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
    add_index :collaborations, [:record_type, :record_id], unique: false
    add_index :collaborations, [:record_type, :record_id, :user_id], unique: true
  end
end
