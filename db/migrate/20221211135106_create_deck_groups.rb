class CreateDeckGroups < ActiveRecord::Migration[7.0]
  def change
    create_table :deck_groups do |t|
      t.references :group, null: false, foreign_key: true
      t.references :deck, null: false, foreign_key: true

      t.timestamps
    end
    add_index :deck_groups, [:group_id, :deck_id], unique: true
  end
end
