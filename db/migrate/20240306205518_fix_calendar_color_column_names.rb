class FixCalendarColorColumnNames < ActiveRecord::Migration[7.1]
  def change
    rename_column :calendars, :color, :background_color
    add_column :calendars, :text_color, :string
  end
end
