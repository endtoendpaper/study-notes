class CreateEvents < ActiveRecord::Migration[7.1]
  def change
    create_table :events do |t|
      t.datetime :start
      t.datetime :end
      t.string :summary
      t.text :location
      t.references :calendar, null: false, foreign_key: true

      t.timestamps
    end

    add_index :events, :start
    add_index :events, [:end, :end]
  end
end
