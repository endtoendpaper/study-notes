class AddDisplayPositionToCards < ActiveRecord::Migration[7.1]
  def change
    add_column :cards, :display_position, :integer
  end
end
