class AddChatToSiteSettings < ActiveRecord::Migration[7.0]
  def change
    add_column :site_settings, :chat, :boolean, default: true
  end
end
