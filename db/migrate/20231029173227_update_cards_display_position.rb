class UpdateCardsDisplayPosition < ActiveRecord::Migration[7.1]
  def change
    Deck.find_each do |deck|
      deck.cards.each_with_index do | card, i |
        card.update(display_position: i)
      end
    end
  end
end
