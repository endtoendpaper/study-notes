class CreateTaggings < ActiveRecord::Migration[7.0]
  def change
    drop_table :decks_tags
    drop_table :notes_tags

    create_table :taggings do |t|
      t.string :record_type
      t.bigint :record_id
      t.references :tag, null: false, foreign_key: true

      t.timestamps
    end

    add_index :taggings, [:record_type, :record_id], unique: false
    add_index :taggings, [:record_type, :record_id, :tag_id], unique: true
  end
end
