class AddStatusLogToSupportIssues < ActiveRecord::Migration[7.0]
  def change
    add_column :support_issues, :status_log, :text
  end
end
