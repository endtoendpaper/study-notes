class ChangeUserIdToBigintFromSupportNotes < ActiveRecord::Migration[7.0]
  def change
    change_column(:support_notes, :user_id, :bigint)
  end
end
