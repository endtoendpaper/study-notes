class UpdateAddressedDefaultToFalseSupportIssues < ActiveRecord::Migration[7.0]
  def change
    change_column_default :support_issues, :addressed, from: nil, to: 'false'
  end
end
