class DropSgidTable < ActiveRecord::Migration[7.0]
  def change
    drop_table :table_sgids
    add_column :tables, :record_saved, :boolean, default: false
    add_index :tables, [:record_saved]
  end
end
