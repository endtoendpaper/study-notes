class CreateCalendars < ActiveRecord::Migration[7.1]
  def change
    create_table :calendars do |t|
      t.string :title
      t.text :description
      t.integer :visibility
      t.string :color
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end

    add_index :calendars, :visibility
  end
end
