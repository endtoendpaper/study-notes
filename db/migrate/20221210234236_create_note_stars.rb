class CreateNoteStars < ActiveRecord::Migration[7.0]
  def change
    create_table :note_stars do |t|
      t.references :user, null: false, foreign_key: true
      t.references :note, null: false, foreign_key: true

      t.timestamps
    end
    add_index :note_stars, [:user_id, :note_id], unique: true
  end
end
