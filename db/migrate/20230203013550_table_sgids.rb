class TableSgids < ActiveRecord::Migration[7.0]
  def change
    create_table :table_sgids do |t|
      t.string :record_type
      t.bigint :record_id
      t.text :sgid, null: false

      t.timestamps
    end
    add_index :table_sgids, [:sgid], unique: true
    add_index :table_sgids, [:record_type, :record_id], unique: false
    add_index :table_sgids, [:record_type, :record_id, :sgid], unique: true
  end
end
