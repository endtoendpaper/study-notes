class AddUserIdIndexToSupportNotes < ActiveRecord::Migration[7.0]
  def change
    add_index :support_notes, :user_id
  end
end
