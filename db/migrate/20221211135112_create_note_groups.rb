class CreateNoteGroups < ActiveRecord::Migration[7.0]
  def change
    create_table :note_groups do |t|
      t.references :group, null: false, foreign_key: true
      t.references :note, null: false, foreign_key: true

      t.timestamps
    end
    add_index :note_groups, [:group_id, :note_id], unique: true
  end
end
