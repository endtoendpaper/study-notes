class CreateTables < ActiveRecord::Migration[7.0]
  def change
    create_table :tables do |t|
      t.integer :columns, default: 1
      t.integer :rows, default: 1
      t.json :data, default: {}
      t.string :record_type
      t.bigint :record_id
      t.boolean :header_row, default: false

      t.timestamps
    end
  end
end
