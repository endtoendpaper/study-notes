class AddIndexesForDecksAndNotesTags < ActiveRecord::Migration[7.0]
  def change
    add_index :decks_tags, :tag_id
    add_index :decks_tags, :deck_id
    add_index :notes_tags, :tag_id
    add_index :notes_tags, :note_id
  end
end
