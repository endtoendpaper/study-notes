class CreateLogTables < ActiveRecord::Migration[7.1]
  def change
    add_index :cards, :display_position

    create_table :collaboration_logs do |t|
      t.string :item_type
      t.bigint :item_id
      t.references :user, null: true, foreign_key: true
      t.string :user_handle
      t.integer :action
      t.string :collaboration_type
      t.bigint :collaboration_id
      t.string :collaboration_name

      t.timestamps
    end
    add_index :collaboration_logs, [:item_type, :item_id], unique: false

    create_table :group_member_logs do |t|
      t.references :group, null: false, foreign_key: true
      t.references :user, null: true, foreign_key: true
      t.string :user_handle
      t.integer :action
      t.bigint :member_id
      t.string :member_handle

      t.timestamps
    end

    create_table :activity_logs do |t|
      t.references :user, null: false, foreign_key: true
      t.integer :action
      t.string :record_type
      t.bigint :record_id
      t.string :details

      t.timestamps
    end
    add_index :activity_logs, [:record_type, :record_id], unique: false
    add_index :activity_logs, :action
  end
end
