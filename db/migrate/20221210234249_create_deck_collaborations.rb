class CreateDeckCollaborations < ActiveRecord::Migration[7.0]
  def change
    create_table :deck_collaborations do |t|
      t.references :user, null: false, foreign_key: true
      t.references :deck, null: false, foreign_key: true

      t.timestamps
    end
    add_index :deck_collaborations, [:user_id, :deck_id], unique: true
  end
end
