class CreateRecurringEvents < ActiveRecord::Migration[7.1]
  def change
    create_table :recurring_events do |t|
      t.datetime :start
      t.datetime :end
      t.string :summary
      t.text :location
      t.references :calendar, null: false, foreign_key: true
      t.string :rrule
      t.text :exceptions
      t.boolean :all_day
      t.datetime :recurrence_end

      t.timestamps
    end

    add_index :recurring_events, :start
    add_index :recurring_events, :end
    add_index :recurring_events, [:start, :end]
    remove_index :events, column: [:end], name: "index_events_on_end_and_end"
    add_index :events, :end
    add_index :events, [:start, :end]
  end
end
