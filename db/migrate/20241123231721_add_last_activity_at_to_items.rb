class AddLastActivityAtToItems < ActiveRecord::Migration[7.2]
  def change
    add_column :calendars, :last_activity_at, :datetime
    add_column :decks, :last_activity_at, :datetime
    add_column :notes, :last_activity_at, :datetime
    add_column :tags, :last_activity_at, :datetime

    reversible do |dir|
      dir.up do
        Calendar.reset_column_information
        Calendar.update_all('last_activity_at = updated_at')
        Deck.reset_column_information
        Deck.update_all('last_activity_at = updated_at')
        Note.reset_column_information
        Note.update_all('last_activity_at = updated_at')
        Tag.reset_column_information
        Tag.update_all('last_activity_at = updated_at')
      end
    end
  end
end
