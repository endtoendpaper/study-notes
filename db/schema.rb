# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[8.0].define(version: 2024_11_23_231721) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "pg_catalog.plpgsql"

  create_table "action_text_rich_texts", force: :cascade do |t|
    t.string "name", null: false
    t.text "body"
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "plain_text_body"
    t.index "to_tsvector('english'::regconfig, plain_text_body)", name: "tsvector_rich_text_idx", using: :gin
    t.index ["record_type", "record_id", "name"], name: "index_action_text_rich_texts_uniqueness", unique: true
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum"
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "activity_logs", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.integer "action"
    t.string "record_type"
    t.bigint "record_id"
    t.string "details"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["action"], name: "index_activity_logs_on_action"
    t.index ["record_type", "record_id"], name: "index_activity_logs_on_record_type_and_record_id"
    t.index ["user_id"], name: "index_activity_logs_on_user_id"
  end

  create_table "calendars", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.integer "visibility"
    t.string "background_color"
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "text_color"
    t.datetime "last_activity_at"
    t.index ["user_id"], name: "index_calendars_on_user_id"
    t.index ["visibility"], name: "index_calendars_on_visibility"
  end

  create_table "cards", force: :cascade do |t|
    t.bigint "deck_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "display_position"
    t.index ["created_at"], name: "index_cards_on_created_at"
    t.index ["deck_id"], name: "index_cards_on_deck_id"
    t.index ["display_position"], name: "index_cards_on_display_position"
    t.index ["updated_at"], name: "index_cards_on_updated_at"
  end

  create_table "chat_preferences", force: :cascade do |t|
    t.boolean "hide", default: false
    t.boolean "pin", default: false
    t.integer "unread_messages", default: 0
    t.bigint "user_id", null: false
    t.bigint "chat_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chat_id"], name: "index_chat_preferences_on_chat_id"
    t.index ["user_id", "chat_id"], name: "index_chat_preferences_on_user_id_and_chat_id", unique: true
    t.index ["user_id"], name: "index_chat_preferences_on_user_id"
  end

  create_table "chats", force: :cascade do |t|
    t.datetime "last_message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "collaboration_logs", force: :cascade do |t|
    t.string "item_type"
    t.bigint "item_id"
    t.bigint "user_id"
    t.string "user_handle"
    t.integer "action"
    t.string "collaboration_type"
    t.bigint "collaboration_id"
    t.string "collaboration_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["item_type", "item_id"], name: "index_collaboration_logs_on_item_type_and_item_id"
    t.index ["user_id"], name: "index_collaboration_logs_on_user_id"
  end

  create_table "collaborations", force: :cascade do |t|
    t.string "record_type"
    t.bigint "record_id"
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["record_type", "record_id", "user_id"], name: "index_collaborations_on_record_type_and_record_id_and_user_id", unique: true
    t.index ["record_type", "record_id"], name: "index_collaborations_on_record_type_and_record_id"
    t.index ["user_id"], name: "index_collaborations_on_user_id"
  end

  create_table "decks", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.integer "visibility"
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "last_activity_at"
    t.index "to_tsvector('english'::regconfig, (title)::text)", name: "tsvector_decks_title_idx", using: :gin
    t.index "to_tsvector('english'::regconfig, description)", name: "tsvector_decks_description_idx", using: :gin
    t.index ["created_at"], name: "index_decks_on_created_at"
    t.index ["updated_at"], name: "index_decks_on_updated_at"
    t.index ["user_id"], name: "index_decks_on_user_id"
    t.index ["visibility"], name: "index_decks_on_visibility"
  end

  create_table "events", force: :cascade do |t|
    t.datetime "start"
    t.datetime "end"
    t.string "summary"
    t.text "location"
    t.bigint "calendar_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "all_day"
    t.index ["calendar_id"], name: "index_events_on_calendar_id"
    t.index ["end"], name: "index_events_on_end"
    t.index ["start", "end"], name: "index_events_on_start_and_end"
    t.index ["start"], name: "index_events_on_start"
  end

  create_table "group_assignments", force: :cascade do |t|
    t.string "record_type"
    t.bigint "record_id"
    t.bigint "group_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id"], name: "index_group_assignments_on_group_id"
    t.index ["record_type", "record_id", "group_id"], name: "index_group_assignments_on_record_and_group_id", unique: true
    t.index ["record_type", "record_id"], name: "index_group_assignments_on_record_type_and_record_id"
  end

  create_table "group_member_logs", force: :cascade do |t|
    t.bigint "group_id", null: false
    t.bigint "user_id"
    t.string "user_handle"
    t.integer "action"
    t.bigint "member_id"
    t.string "member_handle"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id"], name: "index_group_member_logs_on_group_id"
    t.index ["user_id"], name: "index_group_member_logs_on_user_id"
  end

  create_table "group_members", force: :cascade do |t|
    t.bigint "group_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id", "user_id"], name: "index_group_members_on_group_id_and_user_id", unique: true
    t.index ["group_id"], name: "index_group_members_on_group_id"
    t.index ["user_id"], name: "index_group_members_on_user_id"
  end

  create_table "groups", force: :cascade do |t|
    t.string "name"
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "description"
    t.index ["name"], name: "index_groups_on_name", unique: true
    t.index ["user_id"], name: "index_groups_on_user_id"
  end

  create_table "help_sections", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "messages", force: :cascade do |t|
    t.text "content"
    t.string "user_username"
    t.bigint "user_id"
    t.bigint "chat_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chat_id"], name: "index_messages_on_chat_id"
    t.index ["user_id"], name: "index_messages_on_user_id"
  end

  create_table "notes", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "title"
    t.integer "visibility"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "last_activity_at"
    t.index "to_tsvector('english'::regconfig, (title)::text)", name: "tsvector_notes_title_idx", using: :gin
    t.index ["created_at"], name: "index_notes_on_created_at"
    t.index ["updated_at"], name: "index_notes_on_updated_at"
    t.index ["user_id"], name: "index_notes_on_user_id"
    t.index ["visibility"], name: "index_notes_on_visibility"
  end

  create_table "recurring_events", force: :cascade do |t|
    t.datetime "start"
    t.datetime "end"
    t.string "summary"
    t.text "location"
    t.bigint "calendar_id", null: false
    t.string "rrule"
    t.text "exceptions"
    t.boolean "all_day"
    t.datetime "recurrence_end"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["calendar_id"], name: "index_recurring_events_on_calendar_id"
    t.index ["end"], name: "index_recurring_events_on_end"
    t.index ["start", "end"], name: "index_recurring_events_on_start_and_end"
    t.index ["start"], name: "index_recurring_events_on_start"
  end

  create_table "relationships", force: :cascade do |t|
    t.bigint "follower_id"
    t.bigint "followed_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["followed_id"], name: "index_relationships_on_followed_id"
    t.index ["follower_id", "followed_id"], name: "index_relationships_on_follower_id_and_followed_id", unique: true
    t.index ["follower_id"], name: "index_relationships_on_follower_id"
  end

  create_table "site_settings", force: :cascade do |t|
    t.boolean "users_can_join", default: true
    t.string "site_name", default: "My New Site"
    t.boolean "display_site_name", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "contact_form", default: true
    t.boolean "rich_text_support_replies", default: true
    t.boolean "rich_text_support_issues", default: true
    t.boolean "email_contact_form_to_admins", default: true
    t.boolean "email_issue_status_to_users", default: true
    t.boolean "follow_users", default: true
    t.boolean "chat", default: true
  end

  create_table "stars", force: :cascade do |t|
    t.string "record_type"
    t.bigint "record_id"
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["record_type", "record_id", "user_id"], name: "index_stars_on_record_type_and_record_id_and_user_id", unique: true
    t.index ["record_type", "record_id"], name: "index_stars_on_record_type_and_record_id"
    t.index ["user_id"], name: "index_stars_on_user_id"
  end

  create_table "support_comments", force: :cascade do |t|
    t.text "body"
    t.string "user_email"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "support_issue_id", null: false
    t.index ["support_issue_id"], name: "index_support_comments_on_support_issue_id"
    t.index ["user_id"], name: "index_support_comments_on_user_id"
  end

  create_table "support_issues", force: :cascade do |t|
    t.string "summary"
    t.text "body"
    t.boolean "addressed", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "user_email"
    t.bigint "user_id"
    t.text "status_log"
    t.index ["user_id"], name: "index_support_issues_on_user_id"
  end

  create_table "support_notes", force: :cascade do |t|
    t.text "body"
    t.string "user_email"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "support_issue_id", null: false
    t.index ["support_issue_id"], name: "index_support_notes_on_support_issue_id"
    t.index ["user_id"], name: "index_support_notes_on_user_id"
  end

  create_table "support_replies", force: :cascade do |t|
    t.text "body"
    t.bigint "support_issue_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "user_email"
    t.bigint "user_id"
    t.index ["support_issue_id"], name: "index_support_replies_on_support_issue_id"
    t.index ["user_id"], name: "index_support_replies_on_user_id"
  end

  create_table "tables", force: :cascade do |t|
    t.integer "columns", default: 1
    t.integer "rows", default: 1
    t.json "data", default: {}
    t.string "record_type"
    t.bigint "record_id"
    t.boolean "header_row", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "record_saved", default: false
    t.index ["record_saved"], name: "index_tables_on_record_saved"
  end

  create_table "taggings", force: :cascade do |t|
    t.string "record_type"
    t.bigint "record_id"
    t.bigint "tag_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["record_type", "record_id", "tag_id"], name: "index_taggings_on_record_type_and_record_id_and_tag_id", unique: true
    t.index ["record_type", "record_id"], name: "index_taggings_on_record_type_and_record_id"
    t.index ["tag_id"], name: "index_taggings_on_tag_id"
  end

  create_table "tags", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "last_activity_at"
    t.index "to_tsvector('english'::regconfig, (name)::text)", name: "tsvector_tags_name_idx", using: :gin
    t.index ["created_at"], name: "index_tags_on_created_at"
    t.index ["name"], name: "index_tags_on_name"
    t.index ["updated_at"], name: "index_tags_on_updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "username", default: "", null: false
    t.boolean "admin", default: false, null: false
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "darkmode", default: false
    t.string "time_zone", default: "UTC"
    t.boolean "hide_relationships", default: false
    t.boolean "hide_stars", default: false
    t.integer "unread_messages", default: 0
    t.index ["admin"], name: "index_users_on_admin"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["hide_relationships"], name: "index_users_on_hide_relationships"
    t.index ["hide_stars"], name: "index_users_on_hide_stars"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "activity_logs", "users"
  add_foreign_key "calendars", "users"
  add_foreign_key "cards", "decks"
  add_foreign_key "chat_preferences", "chats"
  add_foreign_key "chat_preferences", "users"
  add_foreign_key "collaboration_logs", "users"
  add_foreign_key "collaborations", "users"
  add_foreign_key "decks", "users"
  add_foreign_key "events", "calendars"
  add_foreign_key "group_assignments", "groups"
  add_foreign_key "group_member_logs", "groups"
  add_foreign_key "group_member_logs", "users"
  add_foreign_key "group_members", "groups"
  add_foreign_key "group_members", "users"
  add_foreign_key "groups", "users"
  add_foreign_key "messages", "chats"
  add_foreign_key "messages", "users"
  add_foreign_key "notes", "users"
  add_foreign_key "recurring_events", "calendars"
  add_foreign_key "stars", "users"
  add_foreign_key "support_comments", "support_issues"
  add_foreign_key "support_comments", "users"
  add_foreign_key "support_issues", "users"
  add_foreign_key "support_notes", "support_issues"
  add_foreign_key "support_notes", "users"
  add_foreign_key "support_replies", "support_issues"
  add_foreign_key "support_replies", "users"
  add_foreign_key "taggings", "tags"
end
