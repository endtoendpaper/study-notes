abort("The Rails environment is running in production mode!") if Rails.env.production?

User.where.not(id: User.ordered_by_id.first.id).destroy_all
SupportIssue.destroy_all
SupportReply.destroy_all
SupportNote.destroy_all
SupportComment.destroy_all
HelpSection.destroy_all
Group.where.not(user_id: User.ordered_by_id.first.id).destroy_all
Table.delete_all_unmatched_tables
Chat.destroy_all

tag0 = FactoryBot.create(:tag)
tag1 = FactoryBot.create(:tag)
tag2 = FactoryBot.create(:tag)
tag3 = FactoryBot.create(:tag)

8.times do |i|
  FactoryBot.create(:help_section)
end

12.times do |i|
  if i == 0
    user = FactoryBot.create(:user, :admin, :confirmed).id
  elsif i.even?
    user = FactoryBot.create(:user, :confirmed).id
  else
    user = FactoryBot.create(:user, :confirmed, :avatar).id
  end
  tag4 = FactoryBot.create(:tag)
  tag5 = FactoryBot.create(:tag)
  tag6 = FactoryBot.create(:tag)
  tag7 = FactoryBot.create(:tag)
  FactoryBot.create(:calendar, :public, :thirty_events, :six_recurring_events, user_id: user).tags << tag1
  FactoryBot.create(:note, :internal, user_id: user).tags << tag0
  FactoryBot.create(:deck, :public, :thirty_cards, user_id: user).tags << tag1
  FactoryBot.create(:note, :private, user_id: user).tags << tag1
  FactoryBot.create(:deck, :internal, :two_cards, user_id: user).tags << tag0
  FactoryBot.create(:calendar, :private, user_id: user).tags << tag0
  note1 = FactoryBot.create(:note, :public, user_id: user)
  deck1 = FactoryBot.create(:deck, :private, user_id: user)
  calendar1 = FactoryBot.create(:calendar, :internal, :one_event, :two_recurring_events, user_id: user)
  note1.tags << tag0
  note1.tags << tag1
  note1.tags << tag2
  note1.tags << tag3
  note1.tags << tag4
  note1.tags << tag5
  note1.tags << tag6
  note1.tags << tag7
  deck1.tags << tag0
  deck1.tags << tag1
  deck1.tags << tag2
  deck1.tags << tag3
  deck1.tags << tag4
  deck1.tags << tag5
  deck1.tags << tag6
  deck1.tags << tag7
  calendar1.tags << tag0
  calendar1.tags << tag1
  calendar1.tags << tag2
  calendar1.tags << tag3
  calendar1.tags << tag4
  calendar1.tags << tag5
  calendar1.tags << tag6
  calendar1.tags << tag7
end

deleted_user = FactoryBot.create(:user, :confirmed).id
deleted_admin = FactoryBot.create(:user, :confirmed, :admin).id

8.times do |i|
  user = User.all[i+1]
  if i == 1
    admin = User.second
  else
    admin = User.find(deleted_admin)
  end

  if i.even?
    SiteSetting.first.update(rich_text_support_issues: 'true')
    SiteSetting.first.update(rich_text_support_replies: 'false')
    support_issue = FactoryBot.create(:support_issue, :addressed, :rich_text,
                                       user_id: user.id, user_email: user.email)
    support_issue.update(status_log:
            'Issue Created at ' +
            support_issue.created_at.strftime('%b %d, %Y %I:%M%p %Z') +
            ' by ' + user.username + ' (' +
            user.email + ")\r\n")
    support_issue.update(status_log: support_issue.status_log +
                  'Issue Resolved at ' +
                  support_issue.updated_at.strftime('%b %d, %Y %I:%M%p %Z') +
                  ' by ' + user.username + ' (' +
                  user.email + ")\r\n")
    FactoryBot.create(:support_reply, user_id: admin.id,
                      user_email: admin.email,
                      support_issue_id: support_issue.id)
    FactoryBot.create(:support_comment, :rich_text, user_id: user.id,
                      user_email: user.email,
                      support_issue_id: support_issue.id)
    FactoryBot.create(:support_note, user_id: admin.id, user_email: admin.email,
                      support_issue_id: support_issue.id)
    FactoryBot.create(:support_reply, user_id: admin.id,
                      user_email: admin.email,
                      support_issue_id: support_issue.id)
    support_issue2 = FactoryBot.create(:support_issue, :addressed, :rich_text,
                      user_id: user.id, user_email: user.email)
    support_issue2.update(status_log:
                        'Issue Created at ' +
                        support_issue2.created_at.strftime('%b %d, %Y %I:%M%p %Z') +
                        ' by ' + user.username + ' (' +
                        user.email + ")\r\n")
    support_issue2.update(status_log: support_issue2.status_log +
                        'Issue Resolved at ' +
                        support_issue2.updated_at.strftime('%b %d, %Y %I:%M%p %Z') +
                        ' by ' + user.username + ' (' +
                        user.email + ")\r\n")
  else
    SiteSetting.first.update(rich_text_support_issues: 'false')
    SiteSetting.first.update(rich_text_support_replies: 'true')
    support_issue = FactoryBot.create(:support_issue, user_id: user.id,
                                      user_email: user.email)
    support_issue.update(status_log:
                  'Issue Created at ' +
                  support_issue.created_at.strftime('%b %d, %Y %I:%M%p %Z') +
                  ' by ' + user.username + ' (' +
                  user.email + ")\r\n")
    FactoryBot.create(:support_reply, :rich_text, user_id: admin.id,
                      user_email: admin.email,
                      support_issue_id: support_issue.id)
    FactoryBot.create(:support_comment, user_id: user.id,
                      user_email: user.email,
                      support_issue_id: support_issue.id)
    FactoryBot.create(:support_note, :rich_text, user_id: admin.id,
                      user_email: admin.email,
                      support_issue_id: support_issue.id)
    FactoryBot.create(:support_reply, :rich_text, user_id: admin.id,
                      user_email: admin.email,
                      support_issue_id: support_issue.id)
  end
end

User.find(deleted_user).destroy
User.find(deleted_admin).destroy

4.times do |i|
  FactoryBot.create(:user, :confirmed)
  FactoryBot.create(:user)
end

SiteSetting.first.update(rich_text_support_issues: 'true')
SiteSetting.first.update(rich_text_support_replies: 'true')

users = User.all
decks = Deck.all
notes = Note.all
calendars = Calendar.all
3.times do |i|
  user  = users[i]
  following = users[i+1..15]
  followers = users[i+3..15]
  following.each { |followed| user.follow(followed) }
  followers.each { |follower| follower.follow(user) }
end

6.times do |i|
  if i.even?
    group = FactoryBot.create(:group)
  else
    group = FactoryBot.create(:group, :avatar)
  end
  members = users[i+2..11]
  members.each { |member| group.add_member(member, group.user) }
  decks_to_add = decks[i+3..11]
  decks_to_add.each { |deck| deck.add_group(group, deck.user) }
  notes_to_add = notes[i..8]
  notes_to_add.each { |note| note.add_group(group, note.user) }
  calendars_to_add = calendars[i..8]
  calendars_to_add.each { |calendar| calendar.add_group(group, calendar.user) }
end

group = FactoryBot.create(:group)

8.times do |i|
  user  = users[i+3]
  decks_to_add = decks[i..11]
  decks_to_add.each { |deck| deck.collaborate(user, deck.user) }
  decks_to_add.each { |deck| deck.add_star(user) }
  notes_to_add = notes[i+2..15]
  notes_to_add.each { |note| note.collaborate(user, note.user) }
  notes_to_add.each { |note| note.add_star(user) }
  calendars_to_add = calendars[i+2..15]
  calendars_to_add.each { |calendar| calendar.collaborate(user, calendar.user) }
  calendars_to_add.each { |calendar| calendar.add_star(user) }
end

2.times do |i|
  FactoryBot.create(:chat, :dm_with_first_user_pinned, :two_hundred_msgs)
  FactoryBot.create(:chat, :group_with_first_user_pinned, :two_hundred_msgs)
end

20.times do |i|
  FactoryBot.create(:chat, :dm_with_first_user, :one_msg)
  FactoryBot.create(:chat, :group_with_first_user, :one_msg)
end

6.times do |i|
  FactoryBot.create(:chat, :dm_with_first_user, :two_hundred_msgs)
  FactoryBot.create(:chat, :group_with_first_user, :two_hundred_msgs)
end

User.__elasticsearch__.delete_index! rescue nil
User.__elasticsearch__.create_index!

Group.__elasticsearch__.delete_index! rescue nil
Group.__elasticsearch__.create_index!

SiteSearch.model_list.each do |item|
  model_class = UserItems.class(item)
  model_class.__elasticsearch__.delete_index! rescue nil
  model_class.__elasticsearch__.create_index!
end

User.import
Group.import

SiteSearch.model_list.each do |item|
  model_class = UserItems.class(item)
  model_class.import
end

Elasticsearch::Model.client.indices.refresh(index: '_all')
